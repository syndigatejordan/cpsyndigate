<?php
require_once '/usr/local/syndigate/conf/pheanstalk.conf.php';

abstract class Worker {
 
	private $pheanstalk;
	
	protected $sleepInterval 		= PHEANSTALK_SLEEP_INTERVAL;
	protected $host 		 		= PHEANSTALK_HOST;
	protected $tube 		 		= PHEANSTALK_TUBE;
	protected $ignore 		 		= PHEANSTALK_IGNORE;
	protected $maxConcurrentJobs 	= 1;
	
	public $job;
	
	public function __construct($conf =array()) {		
		
		$this->log('Starting Worker ' . get_class($this) . ' constructor');  			
				
		if(isset($conf['host'])) {
			$this->host = $conf['host'];
		}
		
		if(isset($conf['tube'])) {
			$this->tube = $conf['tube'];
		}
		
		if(isset($conf['ignore'])) {
			$this->ignore = $conf['ignore'];
		}
		
		if(isset($conf['sleep_interval'])) {
			$this->sleepInterval = $conf['sleep_interval'];
		}
		
		if(isset($conf['max_concurrent_jobs'])) {
			$this->maxConcurrentJobs = $conf['max_concurrent_jobs'];
		}
		$this->pheanstalk = new Pheanstalk($this->host . ':11300');				
	}
	
	abstract public function processJob($job);
	
   
	public function __destruct() {
		$this->log('Worker ' . get_class($this) . ' finished');
	}
  
  	public function run() {
  		
  		$this->log('Running worker ' . get_class($this));  
  		 		
  		
    	while(true) {    	
    		try {
    			
    			$tubeStats = $this->pheanstalk->statsTube($this->tube);			
    		
    			if($tubeStats['current-jobs-reserved'] >= $this->maxConcurrentJobs) {
    				continue;
    			}
    		
    			$this->log('Listening for new ' . get_class($this) . ' Jobs');
      		
    			$job 		= $this->pheanstalk->watch($this->tube)->ignore($this->ignore)->reserve();       		
      		
	      		if($job) {      			
    	  			$this->log('Getting new job for ' . get_class($this));
      			      			
      				if($this->processJob($job->getData())) {
      					
      					$this->log("Deleting job from tube " . $this->tube);
      					$this->pheanstalk->delete($job);
      					unset($job);
      				}
      				
      			} else {
      				$this->logCritical("Job was not a real job");
      				sleep($this->sleepInterval); 
      			}
    		} catch (Exception $e) {
    			$this->logCritical($e->getMessage());
    			sleep($this->sleepInterval);
    		}
    	}
  	}
  	  	 	
  
  	protected function log($logMsg) {    
	    	echo $logMsg . PHP_EOL;
		
		$dir = '/home/badmin/yousef/log/' . __FILE__ ;
		if(!is_dir($dir)) {
			mkdir($dir, 0777, true);
		}
		$file = $dir . date('/Y-m-d.log');
		file_put_contents($file, $logMsg . PHP_EOL, FILE_APPEND );		
		
  	}
  	
  	protected function logCritical($logMsg) {

		if('NOT_FOUND' != $logMsg || 'Server reported NOT_FOUND' != $logMsg) {
	  		$this->log($logMsg);
  			//mail('khaled1@corp.albawaba.com', "Error in worker" , $logMsg);
		}
  	}
}
