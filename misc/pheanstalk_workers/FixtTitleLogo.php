<?php 
require_once 'worker.php';
require_once '/usr/local/syndigate/scripts/propel_include_config.php';


class FixtTitleLogo extends Worker {
	
	public function __construct($conf) {				
		parent::__construct($conf);
	}
	
	
	public function processJob($job) {	
		
		$this->log('Processing job');
		$this->job = (array)json_decode($job);
				
		$this->log('getting Title with id ' .  $this->job['title_id']);		
		$title 		= TitlePeer::retrieveByPK($this->job['title_id']);		
		
		if(!$title) {
			$this->log('No title with id ' . $this->job['title_id']);
			return true;
		}
		
		$logoPath 	= $title->getLogoPath();
		
		if($logoPath) {
			
			$logoPath 		= explode('/', $logoPath);
			$logo 			= $logoPath[count($logoPath)-1];
			$logoPath 		= sfConfig::get('sf_upload_dir') . '/' . $logo;						
			$newLogoPath 	= '/syndigate/pub/logos/' . $title->getId() . '.png';			
			
			if(is_file($logoPath) && $logo!= $title->getId() . '.png') {												
				copy($logoPath, $newLogoPath);
				//$title->setLogoPath( '/uploads/' . $title->getId() . '.png');
				//$title->save();
				$this->saveResizedImage($newLogoPath, $newLogoPath);
				chmod($newLogoPath, 0777);
			}			
			
			$smallDir = '/syndigate/pub/logos/small/'; 
			
			if(!is_dir( $smallDir )) {				
				mkdir($smallDir, 0777);
				chmod($smallDir, 0777);
			}
						
			$this->saveResizedImage($newLogoPath, $smallDir . $title->getId() . '.png', 400, 30);
			chmod($smallDir.$title->getId() . '.png', 0777);
			
			return true;			
		} else {
			return true;
		}
		
	}
	
	function saveResizedImage($srcfile, $dstfile, $maxw='', $maxh='') {
		
        $settings = new ezcImageConverterSettings( 
    		array(
        		new ezcImageHandlerSettings( 'GD',          'ezcImageGdHandler' ),
        		//new ezcImageHandlerSettings( 'ImageMagick', 'ezcImageImagemagickHandler' ),
    		)
		);
		$converter = new ezcImageConverter( $settings );

		if($maxh || $maxw) {
			$scaleFilters = array(
            	new ezcImageFilter(
                	'scale',
                	array( 'width'    => $maxw,
                    	   'height'   => $maxh,
                       	'direction' => ezcImageGeometryFilters::SCALE_DOWN )
                	)
            	);
		} else {
			$scaleFilters = array();
		}
		
        // Which MIME types the conversion may output        
        $mimeTypes = array('image/png' );
        
        // Create the transformation inside the manager, this can be re-used
        $converter->createTransformation( 'png', $scaleFilters, $mimeTypes );

        try {
                $converter->transform( 'png', $srcfile, $dstfile);
        } catch ( ezcImageTransformationException $e) {
                trigger_error("Exception: {$e->getMessage()} File:'$srcfile' ", E_USER_WARNING);
                return true;
        }

        return true;
	}

} // End class




$conf = array(		
	'tube'=> 'title-fix-logo',
	'sleep_interval'=>1,		
);

$worker = new FixtTitleLogo($conf);
$worker->run();
