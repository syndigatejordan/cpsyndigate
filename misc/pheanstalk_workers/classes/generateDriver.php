<?php 
require_once '/usr/local/syndigate/misc/pheanstalk_workers/worker.php';
require_once '/usr/local/syndigate/scripts/propel_include_config.php';


class GenerateDriver extends Worker {

	public function __construct($conf) {
		parent::__construct($conf);
	}


	public function processJob($job) {

		$this->log('Processing job');
		$this->job = (array)json_decode($job);

		$titleId  = $this->job['title_id'];
		$revision = $this->job['revision'];
		if($this->job['report_id']) {
			$report_id = $this->job['report_id'];
		} else {
			$report_id = '';
		}

		//$cmd = '/usr/bin/php /usr/local/syndigate/classes/syndGenerateRevDriver.php ' . $titleId . ' ' . $revision  . ' ' . $report_id ;
                $cmd = '/usr/bin/php /usr/local/syndigate/classes/syndGenerateRevDriver.php title_id=' . $titleId . ' revision=' . $revision . ' report_id=' .  $report_id;
		exec($cmd);
		return true;		
		
	}
	
} // End class




$conf = array(		
	'tube'=> 'generate_driver',
	'sleep_interval'=>1,
	'max_concurrent_jobs' => 4,		
);

print_r($conf);

$worker = new GenerateDriver($conf);
$worker->run();
