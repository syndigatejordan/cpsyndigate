<?php 
require_once '/usr/local/syndigate/scripts/propel_include_config.php';

class FixtTitleLogoNew {
	
	public function processJob($title_id) {	
		
		$title 		= TitlePeer::retrieveByPK($title_id);		
		if(!$title) {
			return true;
		}
		
		$logoPath 	= $title->getLogoPath();
		if(!$logoPath) {
			return true;
		}
		if($logoPath) {
			$logoPath 		= explode('/', $logoPath);
			$logo 			= $logoPath[count($logoPath)-1];
			$logoPath 		= sfConfig::get('sf_upload_dir') . '/' . $logo;						
			$newLogoPath 	= '/syndigate/pub/logos/' . $title->getId() . '.png';	
			if(is_file($logoPath)) {		
				copy($logoPath, $newLogoPath);
				$this->saveResizedImage($newLogoPath, $newLogoPath);
				chmod($newLogoPath, 0777);
			}			
			
			$smallDir = '/syndigate/pub/logos/small/'; 
			$lnDir    = '/syndigate/pub/logos/ln/'; // Lexis Nexis standards
			
			if(!is_dir( $smallDir )) {				
				mkdir($smallDir, 0777);
				chmod($smallDir, 0777);
			}
			if(!is_dir( $lnDir )) {				
				mkdir($lnDir, 0777);
				chmod($lnDir, 0777);
			}
			
			
			try {
				$this->saveResizedImage($newLogoPath, $smallDir . $title->getId() . '.png', 400, 30);
				
				//// Lexis Nexis directory
				$this->saveResizedImage($newLogoPath, $lnDir . $title->getId() . '.png', 225, 75);
				
			} catch (Exception $e) {
				return true;
			}
			
			chmod($smallDir.$title->getId() . '.png', 0777);
			chmod($lnDir.$title->getId() . '.png', 0777);
			return true;			
		} else {
			return true;
		}
		
	}
	
	function saveResizedImage($srcfile, $dstfile, $maxw='', $maxh='') {
        $settings = new ezcImageConverterSettings( 
    		array(
        		new ezcImageHandlerSettings( 'GD',          'ezcImageGdHandler' ),
        		//new ezcImageHandlerSettings( 'ImageMagick', 'ezcImageImagemagickHandler' ),
    		)
		);
		$converter = new ezcImageConverter( $settings );

		if($maxh || $maxw) {
			$scaleFilters = array(
            	new ezcImageFilter(
                	'scale',
                	array( 'width'    => $maxw,
                    	   'height'   => $maxh,
                       	'direction' => ezcImageGeometryFilters::SCALE_DOWN )
                	)
            	);
		} else {
			$scaleFilters = array();
		}
        // Which MIME types the conversion may output        
        $mimeTypes = array('image/png' );
        
        // Create the transformation inside the manager, this can be re-used
        $converter->createTransformation( 'png', $scaleFilters, $mimeTypes );

        try {
                $converter->transform( 'png', $srcfile, $dstfile);
        } catch ( ezcImageTransformationException $e) {
die('sdfsdfsdfdsfsdfsdfsdfsdffssfsf');
                trigger_error("Exception: {$e->getMessage()} File:'$srcfile' ", E_USER_WARNING);
                return false;
        }

        return true;
	}

} // End class

$title_id = $argv[1];;
$worker = new FixtTitleLogoNew();
$worker->processJob($title_id);
