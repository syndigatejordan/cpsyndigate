#!/usr/bin/env php
<?php
//Set Beginning of php code:
header("Content-Type: text/html; charset=UTF-8");
mysql_query("SET NAMES 'utf8'");
mysql_query('SET CHARACTER SET utf8');
require_once '/usr/local/syndigate/misc/pheanstalk_workers/worker.php';

define('ALBAWABA_HOST', '10.200.200.196');
define('ALBAWABA_DB_NAME', 'elpis_syndigate');
define('ALBAWABA_DB_USER', 'synd02');
define('ALBAWABA_DB_PASSWORD', 'vgdjvJF6Lhq8');

class PostProcessArticles extends Worker {

  public function __construct($conf) {

    parent::__construct($conf);

    //get Accespted titles 
    $acceptedTitlesIds = array();
    $acceptedTitles = DrupalIdsPeer::doselect(new Criteria());
    foreach ($acceptedTitles as $title) {
      $acceptedTitlesIds[] = $title->getTitleId();
    }

    $this->acceptedTitlesIds = $acceptedTitlesIds;
  }

  public function processJob($job) {

    $this->log('Processing job');
    $this->job = (array) json_decode($job);

    if (!in_array((int) $this->job['title_id'], $this->acceptedTitlesIds)) {
      $this->log('Title id is not in the accepted ids ... exiting job');
      return true;
    }


    $this->log('title : ' . $this->job['title_id'] . ' getting articles with ids ' . implode(',', $this->job['articles_ids']));
    $insertArticles = $this->insertArticles($this->job['articles_ids'], ALBAWABA_HOST, ALBAWABA_DB_USER, ALBAWABA_DB_PASSWORD, ALBAWABA_DB_NAME);
    return true;
  }

  public function insertArticles($article_id, $ALBAWABA_HOST, $ALBAWABA_DB_USER, $ALBAWABA_DB_PASSWORD, $ALBAWABA_DB_NAME) {
    $this->log("ALBAWABA_DB_NAME  " . $ALBAWABA_DB_NAME);
    $c = new Criteria();
    $c->add(ArticlePeer::ID, $article_id, Criteria::IN);
    $articles = ArticlePeer::doSelect($c);
    if (count($articles) > 0) {

      $albawabaConn = mysql_connect($ALBAWABA_HOST, $ALBAWABA_DB_USER, $ALBAWABA_DB_PASSWORD) OR $this->log("Can not connect to albawaba mysql $ALBAWABA_DB_NAME");
      $albawabaDb = mysql_select_db($ALBAWABA_DB_NAME) Or $this->log("Can not select albawaba database $ALBAWABA_DB_NAME");

      $sql = "REPLACE INTO article     (id, title_id, language_id, headline, summary, body, author, date, parsed_at, updated_at, canonical_link) VALUES ";
      $sql2 = "REPLACE INTO utf_article (id, title_id, language_id, headline, summary, body, author, date, parsed_at, updated_at, canonical_link) VALUES ";

      foreach ($articles as $article) {
        //if (strlen($article->getBody()) <= 1000) {				
        $headline = mysql_escape_string($article->getHeadline());
        $summary = mysql_escape_string($article->getSummary());
        $body = mysql_escape_string($article->getBody());
        $author = mysql_escape_string($article->getAuthor());
        $maybe = "";
        $maybe_close = "";

        /* 	if (in_array($article->getTitleId(),array(87)))
          {
          $maybe="maybe_utf8_decode(";
          $maybe_close=")";
          } */
        //$sqlParams[] = "( {$article->getId()}, {$article->getTitleId()}, {$article->getLanguageId()}, maybe_utf8_decode('$headline'), '$summary', maybe_utf8_decode('$body'), '$author', '{$article->getDate()}', '{$article->getParsedAt()}', '{$article->getUpdatedAt()}')";
        $sqlParams[] = "( {$article->getId()}, {$article->getTitleId()}, {$article->getLanguageId()}, $maybe '$headline' $maybe_close, '$summary', $maybe '$body' $maybe_close, '$author', '{$article->getDate()}', '{$article->getParsedAt()}', '{$article->getUpdatedAt()}', '{$article->getArticleOriginalData()->getReference()}')";

        $images = $article->getImages();
        $imgsSql = "REPLACE INTO image (id, article_id, img_name, image_caption, is_headline) VALUES ";

        if (count($images) > 0) {
          foreach ($images as $img) {
            $imgName = mysql_escape_string($img->getImgName());
            $imgCaption = mysql_escape_string($img->getImageCaption());
            $imgSqlParams[] = "( {$img->getId()}, {$img->getArticleId()}, '$imgName', '{$imgCaption}', '{$img->getIsHeadline()}' )";
          }
        }
        //	}
      }
      //set utf-8 to insert all articles into utf_article as utf8
      mysql_query("SET NAMES 'utf8'", $albawabaConn);

      $sqlParams = implode(', ', $sqlParams);
      //$this->log("Quering : $sql  $sqlParams");
      if (!mysql_query($sql . $sqlParams, $albawabaConn)) {
        echo mysql_error() . "\nQUERY: $sql $sqlParams\n";
      }



      $articlesUpdated = mysql_affected_rows($albawabaConn) > 0 ? true : false;
      $this->log("articles updated ? " . $articlesUpdated);

      if (is_array($imgSqlParams) && count($imgSqlParams) > 0) {
        $this->log('articles has images');

        $imgSqlParams = implode(', ', $imgSqlParams);
        mysql_query($imgsSql . $imgSqlParams, $albawabaConn);
        //$this->log("Quering : $imgsSql $imgSqlParams");
        $imagesUpdated = mysql_affected_rows($albawabaConn) > 0 ? true : false;
        $this->log("Images updated :" . $imagesUpdated);
      } else {
        $this->log("No Images for articles");
        $imagesUpdated = true;
      }


      if ($articlesUpdated && $imagesUpdated) {
        $this->log('successfully exiting job');
        mysql_close($albawabaConn);
        return true;
      } else {
        $this->log('Unsuccessfully exiting job ... returning false');
        mysql_close($albawabaConn);
        return true; // to avoid stucking 
      }
    } else {

      $this->log('No articles to insert into database');
      mysql_close($albawabaConn);
      return true;
    }
  }

}

$conf = array(
    'tube' => 'post-process-articles',
    'sleep_interval' => 5,
);

$worker = new PostProcessArticles($conf);
$worker->run();
