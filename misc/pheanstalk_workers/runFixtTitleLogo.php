#!/usr/bin/php
<?php

/**
* Handling SIG handlers
*/
$sigterm = false;
$sighup  = false;

/**
* SIG handler
*/
function sig_handler($signo) {
	global $sigterm, $sigup;
	
	if($signo == SIGTERM) {
		$sigterm = true;
	} else if($signo == SIGHUP) {
		$sighup = true;
	}
}


global $sigterm, $sighup;

ini_set("max_execution_time", "0");
ini_set("max_input_time", "0");
set_time_limit(0);

pcntl_signal(SIGTERM, "sig_handler");
pcntl_signal(SIGHUP, "sig_handler");


$pid = pcntl_fork();

if($pid == -1) {
	die("Error : Forking failed ... !");
}

if($pid) {
	echo($pid);
	return 0;
}

while(!$sigterm) {
	exec('php /usr/local/syndigate/misc/pheanstalk_workers/classes/fixtTitleLogo.php');
	sleep(2);
}

return 0;