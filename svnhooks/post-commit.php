#!/usr/bin/env php
<?php
	/**
	 * SVN post-commit hook that notifes "shepherd" of new content 
	 * that needs to be parsed
	 */
	
	require_once dirname(__FILE__) . '/../ez_autoload.php';

	$log = ezcLog::getInstance();
	$log->source = 'svn-hook';
	
	
	$repo     = $_SERVER['argv'][1];
	$revision = $_SERVER['argv'][2];
	$log->log("Hook Called: Repo [$repo] Revision [$revision]", ezcLog::DEBUG);
        
        // Getting title id from the repo path 
        $parts = explode('/', $repo);
        $full_parts = array();
        // make sure to exclude empty array values since you many encounter double slash in the repo path
        foreach ($parts as $part) {
            if($part) {
                $full_parts[] = $part;
            }
        }
        $titleId = $full_parts[4];
         
        
	//get the title ID using repo path $repo
	$c = new Criteria();	
        $title = TitlePeer::retrieveByPK($titleId);
        
        if (!is_object($title)) {
            trigger_error('Title not found, wrong repo path. '.$repo,E_USER_ERROR);
        }   
    
	$log->log("Resolved Title [$titleId] Revision [$revision]", ezcLog::DEBUG);
	$row = new ParsingState();
	$row->setTitleId($titleId);
	$row->setRevisionNum($revision);
	$row->save();
