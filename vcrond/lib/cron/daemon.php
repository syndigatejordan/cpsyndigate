<?php
/**
*
*       CronDaemon, v1.0
*
*       By Robin Speekenbrink
*
*       Kingsquare Information Services, 1-10-2007
*
*  Based on the original PHP_FORK class by Luca Mariano
* 
*  This program is free software. You can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License.
* @author Robin Speekenbrink <info@kingsquare.nl>
* @version 1.0
*/

class CronDaemon extends PHP_Fork {
	/**
     * The time (seconds) the process checks for new jobs or finished workers
     *
     * @var string
     * @access private
     */
    var $_sleepInt = 60;

    /**
     * The time (seconds) a worker can be left `idle`
     *
     * @var integer
     * @access private
     */
    var $_maxIdleTime = 60;
	
    /**
	 * An array of workers currently executing code
	 *
	 * @var array
     * @access private
	 */
	var $_workers = array();
	
	/**
	 * Log object instance
	 *
	 * @var ezcLog
	 */
	var $log;
	
	/**
	 * Pointer to db connection
	 */
	var $dbLink;
	
	/**
	 * log params 
	 */
	var $logParams;
    
    /**
     * create a new Daemon
     *
     * @param string $name
     * @param integer $sleepInterval
     * @param integer $maxIdletime
     */
    function cronDaemon($name, $sleepInterval, $maxIdletime) {
    	$this->log = ezcLog::getInstance();
    	$this->log->source = 'vcrond';
    	$this->logParams = array('source'=> $this->log->source);
    	
    	$this->PHP_Fork($name);
        $this->_sleepInt = $sleepInterval;
        $this->_maxIdleTime = $maxIdletime;
    }

    /**
     * The actual code that runs continously
     *
     */
    function run() {
    	
    	while (true) {						
			$this->dbLink = newDb();
     		$this->_processCronTabs();
       		$this->_startWorkers();
            $this->_detectDeadWorkers();
			$this->_kill();
			mysqli_close($this->dbLink);
		    sleep($this->_sleepInt);
        }
    }
    
    /**
     * kill the deaemon itself. Remove any open pointer it can find
     *
     */
    function stop() {
    	global $fp;
    	if (is_resource($fp)) fclose($fp);
    }
    
    /**
     * Kill all the running workers we can find
     *
     */
    function stopAllWorkers() {
        $this->log->log($this->getName().': stopping workers...', ezcLog::DEBUG, $this->logParams );
        foreach($this->_workers as $implementationId=>$workers) {
        	foreach($workers as $worker) {
        		$worker->stop();
        		$this->log->log($worker->getName().' ('.$worker->getPid().'): stopped', ezcLog::DEBUG, $this->logParams );
        	}
        }
        unset($this->_workers);
        $this->_workers = array();
        $this->log->log($this->getName().': All workers stopped', ezcLog::DEBUG, $this->logParams );
        
    }

	/**
	 * Retrieve all the jobs that have to be executed
	 *
	 * @return array CronJob
	 */
	function _getJobs() {    	
		$rows = array();
    		$jobs = array();

		$sql  = 'SELECT `id` FROM `'.DB_NAME.'`.`'.TABLE_CRONJOB.'` WHERE `pid` = 0 AND `end_timestamp` = "0000-00-00 00:00:00" ';
		$res  = mysqli_query($this->dbLink, $sql);
		while ($row = mysqli_fetch_assoc($res)) {
			$rows[] = $row['id'];
		}		

		
		if (mysqli_error($this->dbLink)) {
			$this->log->log('MySQL error: '.mysqli_error($this->dbLink).' when _getJobs is called '.$sql, ezcLog::FATAL, $this->logParams );
			$this->_kill(true);
		}

		foreach ($rows as $cronJobId) { 
			$jobs[] = new CronJob($cronJobId);
		}
		
		if (!empty($jobs)) {
			$this->log->log('found jobs ('.count($jobs).'): '.implode(',',$rows), ezcLog::DEBUG, $this->logParams);
		} else {
			$this->log->log('no jobs found', ezcLog::DEBUG, $this->logParams );
		}
		
		return $jobs;
	}

    /**
     * get all the crontabs and process them
     *
     * @access private
     */
    function _processCronTabs() {		
    	$sql = 'SELECT `id` FROM `'.DB_NAME.'`.`'.TABLE_CRONTAB.'`';
    	$res = mysqli_query($this->dbLink, $sql);
    	while ($row = mysqli_fetch_assoc($res)) {			
    		$crontab = new Crontab($row['id']); 
    		
    		//Check to see if this client is active
    		$model = new syndModel();
    		$titleId = $crontab->implementationId;
    		if (!$model->isTitleActive($titleId)) {
    			//this title is not active, don't create a job for it
    			continue;
    		}
    		   		
    		if ($crontab instanceof Crontab) 
    			$crontab->process();
    	}
    }
 
    /**
     * Start workers for the jobs found
     * 
     * @access private
     */
    function _startWorkers() {

    	if(count($this->_workers) <= MAX_WORKERS) {
	        $this->log->log('checking for jobs', ezcLog::DEBUG, $this->logParams );      		
   		$jobs = $this->_getJobs();

    		foreach($jobs as $job) {
				
    			if ((key_exists($job->implementationId, $this->_workers) && $job->concurrent) ||  (!key_exists($job->implementationId, $this->_workers))) {
					if ($job->concurrent) {
						foreach((array)$this->_workers[$job->implementationId] as $worker) {
							if (!$worker->concurrent) break 2;
						}
					}
					
					mysqli_close($this->dbLink);
    				$newWorker = new worker ('worker implementation '.$job->implementationId.' job '.$job->id);
					
					//start new DB connection
					$this->dbLink = newDb();
					
					
					if ($newWorker->_ipc_is_ok) {
    					$newWorker->jobId = $job->id;
						$newWorker->script = $job->script;						
						$newWorker->concurrent = $job->concurrent;
						$newWorker->implementationId = $job->implementationId;
						$newWorker->crontabId = $job->crontabId;
						$newWorker->start();
						
	                	$job->pid = $newWorker->getPid();
	                	
	                	if(!$newWorker->getPid()) {
	                		$this->log->log('Failed to get pid for worker [cronjob id =' . $job->id . ' ] ', ezcLog::FATAL, $this->logParams );
	                	}
						$job->update();
	                	$this->_workers[$job->implementationId][] = $newWorker;
	                	$this->log->log('started new Woker '.$newWorker->getName().' ('. $newWorker->getPid().')', ezcLog::DEBUG, $this->logParams );
	                } else {
	                	$this->log->log($newWorker->getName().': Unable to create IPC segment...', ezcLog::ERROR, $this->logParams );  
	                }
    			} else {
	                $this->log->log('Unable to start job due to concurrency', ezcLog::NOTICE, $this->logParams );    				
    			}
   			}
    	}
    }

	/**
	 * Detect dead / finished workers and kill them
	 *
	 * @access private
	 */
	function _detectDeadWorkers() {
		foreach (array_keys($this->_workers) as $implementationId) {
			foreach(array_keys($this->_workers[$implementationId]) as $workerKey) {
				$worker = $this->_workers[$implementationId][$workerKey];
	            $this->log->log('getKill '.$worker->getVariable('_kill').' : '.$worker->getPid(), ezcLog::DEBUG, $this->logParams );
				if ($worker->getVariable('_kill')) {
					$this->log->log($worker->getName().' ('.$worker->getPid().') seems to be finished...', ezcLog::DEBUG, $this->logParams );
					$worker->stop();
					array_splice($this->_workers[$implementationId],$workerKey,1);
					break;
				} else {
					//$this->log->log(print_r($worker,true), ezcLog::DEBUG );
				}
			}
			if (empty($this->_workers[$implementationId])) unset($this->_workers[$implementationId]);
		}
	}
    
	/**
	 * The ultimate kill command for the deaemon / workers
	 * If force is called the existance of the real FS_KILL-file is ignored (but cleaned if available)
	 * 
	 * @param boolean $force
	 */
	function _kill($force = false) {
		if (is_readable(FS_KILL) || $force) {
			$contents = ((is_readable(FS_KILL))? file_get_contents(FS_KILL): 1 );
			if (!empty($contents)) {
				if ($force) {
					$this->log->log($this->getName().' was killed due to error or otherwise forced', ezcLog::DEBUG, $this->logParams );					
				} else {
					$this->log->log($this->getName().' found kill file and contents', ezcLog::DEBUG, $this->logParams );					
				}
				$this->stopAllWorkers();
				
				if (is_file(FS_KILL)) {
					unlink(FS_KILL);
				}
				
				$this->log->log($this->getName().' fully stopped', ezcLog::DEBUG, $this->logParams );
				$this->stop();
				exit;
			} else {
				$this->log->log($this->getName().' is still alive muhaha', ezcLog::DEBUG, $this->logParams );
			}
		}
	}
}
?>
