<?php
/**
*
*       CronJob, v1.0
*
*       By Robin Speekenbrink
*
*       Kingsquare Information Services, 1-10-2007
*
*       More info, bugs or requests, contact info@kingsquare.nl
* @author Robin Speekenbrink <info@kingsquare.nl>
* @version 1.0
*/

class CronJob {
	/**
	* The identifier for this job
	*
	* @var integer
	* @access public
	*/
	var $id = 0;
	
	/**
	* The identifier for the creator of this job (the crontab definition)
	*
	* @var integer
	* @access public
	*/
	var $crontabId = 0;

	/**
	* The actual UNIX timestamp of the start of this job
	*
	* @var integer
	* @access public
	*/
	var $startTimestamp = 0;

	/**
	* The UNIX timestamp for when this job claimed to be finished
	*
	* @var integer
	* @access public
	*/
	var $endTimestamp = 0;

	/**
	* The actual code that should be executed on running this job
	*
	* @var integer
	* @access public
	*/
	var $script = '';
	

	/**
	* Can a job run along side another running job (together with a similar implementationId)
	*
	* @var integer
	* @access public
	*/
	var $concurrent = 0;

	/**
	* The identifier for concurrent environments
	*
	* @var integer
	* @access public
	*/
	var $implementationId = 0;

	/**
	* The final output returned by the system
	*
	* @var integer
	* @access public
	*/
	var $results = '';

	/**
	* The PID as given by the OS to the job as its being handeld.
	*
	* @var integer
	* @access public
	*/
	var $pid = 0;
	
	
	
	/**
	 * Define if the job existed normally, by default 0, 
	 *
	 * @var bool
	 * @access public
	 */
	var $isExitedNormally = 0;
	
	/**
	 * Log object instance
	 *
	 * @var ezcLog
	 */
	var $log;
	
	var $dbLink;
		
	/**
	 * Create a new blank (id=false) or existing cronjob as found in DB
	 *
	 * @param integer $id
	 * @return CronJob
	 */
	function CronJob($id = false) {
		$this->dbLink = newDb();
		
		$this->log = ezcLog::getInstance();
		$this->log->source = 'vcrond';
		
		$this->id = $id;
		if ($this->id) $this->_setData();
	}
		
	/**
	 * A method to easily create a job from a crontab definition
	 *
	 * @param Crontab $crontab
	 */
	function createFromCrontab(& $crontab) {
		$this->crontabId = $crontab->getId();
		$this->startTimestamp = time();		
		$this->script = $crontab->getScript();		
		$this->concurrent = $crontab->getConcurrent();
		$this->implementationId = $crontab->getImplementationId();
	}
	
	/**
	 * Update the record in the DB
	 *
	 * @return boolean
	 */
	function update() {		
		$sql = 'REPLACE `'.DB_NAME.'`.`'.TABLE_CRONJOB.'` SET 
				`id`				= "'.mysqli_escape_string($this->dbLink, $this->id).'",
				`crontab_id`		= '.mysqli_escape_string($this->dbLink, $this->crontabId).',				
				`start_timestamp`	= FROM_UNIXTIME('.mysqli_escape_string($this->dbLink, $this->startTimestamp).'),
				`end_timestamp`		= FROM_UNIXTIME('.mysqli_escape_string($this->dbLink, $this->endTimestamp).'),
				`script`			= "'.mysqli_escape_string($this->dbLink, $this->script).'",
				`concurrent`		= "'.mysqli_escape_string($this->dbLink, $this->concurrent).'",
				`implementation_id`	= '.mysqli_escape_string($this->dbLink, $this->implementationId).',
				`results`			= "'.mysqli_escape_string($this->dbLink, $this->results).'",
				`is_exited_normally`= "'.mysqli_escape_string($this->dbLink, $this->isExitedNormally).'",
				`pid`				= '.mysqli_escape_string($this->dbLink, $this->pid);		
		if (!mysqli_query($this->dbLink, $sql)) {
			$this->log->log(mysqli_error($this->dbLink).$sql, ezcLog::ERROR );
			return false;
		}
		if (!$this->id) {
			$this->id = mysqli_insert_id($this->dbLink);
			$action = 'created';
		} else {
			$action = 'updated';
		}
		
		$this->log->log(ucfirst($action).' job with ID: '.$this->id, ezcLog::DEBUG  );

		return true;
	}
	
	/**
	 * retrieve thea actual data from the DB
	 *
	 * @access private
	 */
	function _setData() {		
		$sql = 'SELECT * FROM `'.DB_NAME.'`.`'.TABLE_CRONJOB.'` WHERE `id` = ' . mysqli_escape_string($this->dbLink,$this->id);
		$res = mysqli_query($this->dbLink, $sql);
		$jobData = mysqli_fetch_assoc($res);		
				
		$this->id 	  			= $jobData['id'];
		$this->crontabId		= $jobData['crontab_id'];
		$this->startTimestamp  	= strtotime($jobData['start_timestamp']);
		$this->endTimestamp 	= strtotime($jobData['end_timestamp']);
		$this->script 		    = $jobData['script'];
		$this->concurrent    	= $jobData['concurrent'];
		$this->implementationId = $jobData['implementation_id'];
		$this->results 			= $jobData['results'];
		$this->pid              = $jobData['pid'];		
		$this->isExitedNormally = $jobData['is_exited_normally'];
	}
}