<?php

class Crontab {
	/**
	 * identifier
	 *
	 * @var integer
	 */
	var $id 				= 0;
	
	/**
	 * small title for the crontab
	 *
	 * @var string (255)
	 */
	var $title 				= '';
	
	
	/**
	 * Extended description for longer descriptions
	 *
	 * @var string
	 */
	var $description 		= '';
	
	/**
	 * the actual code (PHP)
	 *
	 * @var string
	 */
	var $script				= '';
	
	/**
	 * Can this script run along side an already running script for this implmementation / environment?
	 *
	 * @var boolean
	 */
	var $concurrent 		= false;
	
	/**
	 * The implementation / environment identifier
	 *
	 * @var integer
	 */
	var $implementationId	= 0;
	
	/**
	 * The actual cron statement as used by the original cronDaemon on *NIX
	 *
	 * @var string
	 */
	var $cronDefinition		= '';
	
	/**
	 * UNIX Timestamp of the last actual run for this cronTab
	 *
	 * @var timestamp
	 */
	var $lastActualTimestamp= 0;
	
	/**
	 * a small var to keep messages generated on update / setters etc
	 *
	 * @var string
	 */
	var $_message = '';
	
	/**
	 * Log object instance
	 *
	 * @var ezcLog
	 */
	var $log;
		
	/**
	 * Whether to run this tab only once or not?
	 *
	 * @var int
	 */
	var $runOnce = 0;
	
	var $dbLink;
	
	
	/**
	 * Constructor for this package
	 *
	 * @param integer $id
	 * @return Crontab
	 */
	function Crontab($id = false) {
		
		$this->dbLink = newDb();
		
		$this->log = ezcLog::getInstance();
		$this->log->source = 'vcrond';	
		
		$this->id = $id;
		if ($this->id) $this->_setData();
	}

	/**
	 * Delete this crontab
	 *
	 * @return boolean
	 */
	function delete() {		
		$sql = 'DELETE FROM `'.DB_NAME.'`.`'.TABLE_CRONTAB.'` WHERE id = ' . mysqli_escape_string($this->dbLink,$this->id);		
		
		if (!mysqli_query($this->dbLink, $sql)) {
			$this->log->log(mysqli_error($this->dbLink), ezcLog::ERROR );
			return false;
		}
		
		$this->id = 0;
		return true;
	}
	
	/**
	 * update the DB
	 *
	 * @return boolean
	 */
	function update() {		
		$sql = 'REPLACE `'.DB_NAME.'`.`'.TABLE_CRONTAB.'` SET 
				`id`					= "'.mysqli_escape_string($this->dbLink, $this->id).'",
				`title`					= "'.mysqli_escape_string($this->dbLink, $this->title).'",				
				`description`			= "'.mysqli_escape_string($this->dbLink, $this->description).'",
				`script`				= "'.mysqli_escape_string($this->dbLink, $this->script).'",
				`concurrent`			= "'.mysqli_escape_string($this->dbLink, $this->concurrent).'",
				`implementation_id`		= '.mysqli_escape_string($this->dbLink, $this->implementationId).',
				`cron_definition`		= "'.mysqli_escape_string($this->dbLink, $this->cronDefinition).'",
				`run_once`				= '.mysqli_escape_string($this->dbLink, $this->runOnce).',
				`last_actual_timestamp`	= '.mysqli_escape_string($this->dbLink, $this->lastActualTimestamp);
		
		if (!mysqli_query($this->dbLink, $sql)) {
			$this->log->log(mysqli_error($this->dbLink), ezcLog::ERROR );
			return false;
		}
		if (!$this->id)  $this->id = mysqli_insert_id($this->dbLink);
		return true;
	}
	
	/**
	 * Create a cronParser and check to see if we need to create new jobs
	 *
	 * @return void
	 */
	function process() {		
		$cronParser = new CronParser($this->cronDefinition);
		if ($this->getId() && ($cronParser->getLastRanUnix() > $this->getLastActualTimestamp())) {
			$this->log->log('creating job because '.date('d-m-Y H:i', $cronParser->getLastRanUnix()).' > '.date('d-m-Y H:i', $this->getLastActualTimestamp()).' ('.$cronParser->getLastRanUnix().' > '.$this->getLastActualTimestamp().')', ezcLog::DEBUG );
			
			$this->lastActualTimestamp = time();
			$sql = 'UPDATE `'.DB_NAME.'`.`'.TABLE_CRONTAB.'` SET `last_actual_timestamp`= '.mysqli_escape_string($this->dbLink, $this->lastActualTimestamp).' WHERE `id`= '.mysqli_escape_string($this->dbLink, $this->getId());
			$res = mysqli_query($this->dbLink, $sql);
			$job = new CronJob();
			$job->createFromCrontab($this);
			$job->update();
		}
		return;
	}

	/***********************
	 * PRIVATE FUNCTIONS
	 */
	
	/**
	 * get all the data from the DB
	 *
	 */
	function _setData() {		
		$sql = 'SELECT * FROM `'.DB_NAME.'`.`'.TABLE_CRONTAB.'` WHERE `id` = '.mysqli_escape_string($this->dbLink,$this->id);	
		$res = mysqli_query($this->dbLink, $sql);
		$tabData = mysqli_fetch_assoc($res);
		
		$this->id 	  			   = $tabData['id'];
		$this->title  			   = $tabData['title'];
		$this->description  	   = $tabData['description'];
		$this->script 		       = $tabData['script'];
		$this->concurrent 		   = $tabData['concurrent'];
		$this->implementationId    = $tabData['implementation_id'];
		$this->cronDefinition 	   = $tabData['cron_definition'];
		$this->lastActualTimestamp = $tabData['last_actual_timestamp'];
		$this->runOnce             = $tabData['run_once'];	
	}
	
	/***********************
	 * SETTERS
	 */
	
	/**
	 * simple setter of titles
	 *
	 * @param string $var
	 * @return boolean
	 */
	function setTitle($var) {
		$var = trim(strip_tags($var));
		if ($var) {
			$this->title = $var;
			return true;
		} else {
			$this->_message .= ' Title is mandatory!';
			return false;
		}
	}
	
		
	/**

	 * Enter description here...
	 *
	 * @param unknown_type $var
	 * @return boolean
	 */
	function setDescription($var = false) {
		$description = strip_tags(trim($var));
		if (empty($description)){
			$this->_message .= 'Description was left empty';
			return false;
		} else {
			$this->description = $description;
			return true;
		}
	}

	/**
	 * Set the actual code that shoudl be executed. PHP tags will be striped to be on the safe side
	 *
	 * @param string $var
	 * @return void
	 */
	function setScript($var) {
		if ($var) {
			$this->script = $var;
			return true;
		} else {
			$this->_message .= ' Script is mandatory!';
			return false;
		}
		
		
	}
	
	/**
	 * Set the concurrency
	 *
	 * @param boolean $var
	 * @return void
	 */
	function setConcurrent($var = true) {
		$this->concurrent = (boolean) ($var);
		return true;
	}
	
	/**
	 * Set the environment / implementation Identifier
	 *
	 * @param integer $var
	 * @return void
	 */
	function setImplementationId($var = 0) {
		$this->implementationId = (int) $var;
		return true;
	}
	
	/**
	 * The cron definition
	 * currently supported is comma seperated per field. No dividers (* / 5 instead use 5,10,15 etc)
	 *
	 * @param string $var
	 * @return void
	 */
	function setCronDefinition($var) {
		$this->cronDefinition = trim(strip_tags($var));
		return true;
	}
	
	/**
	 * Last run UNIX Timestamp
	 *
	 * @param integer $var
	 * @return void
	 */
	function setLastActualTimestamp($var = 0) {
		//if we get 1970, this is the UNIX epoch, which should e converted to 0
		if ($var == 1970) {
			$var = 0;
		}
		$this->lastActualTimestamp = (int) $var;
		return true;
	}

	/**
	 * Set if this tab should be run only once
	 *
	 * @param boolean $var
	 * @return void
	 */
	function setRunOnce($var = false) {
		$this->runOnce = (boolean) ($var);
		return true;
	}
	
	/***********************
	 * GETTERS
	 */
	
	/**
	 * Simple getter
	 *
	 * @return int
	 */
	function getId() { return $this->id; }
	
	/**
	* Simple getter
	 *
	 * @return string
	 */
	function getTitle() { return $this->title; }
	
	
	/**
	 * Simple getter
	 *
	 * @return string
	 */
	function getDescription() { return $this->description; }
	
	/**
	 * Simple getter
	 *
	 * @return string
	 */
	function getScript() { return $this->script; }
	
	/**
	 * Simple getter
	 *
	 * @return string
	 */
	function getImplementationId() { return $this->implementationId; }
	
	/**
	 * Simple getter
	 *
	 * @return boolean
	 */
	function getConcurrent() { return (boolean) $this->concurrent; }
	
	/**
	 * Simple getter
	 *
	 * @return string
	 */
	function getCronDefinition() { return $this->cronDefinition; }

	/**
	 * Simple getter
	 *
	 * @return bool
	 */
	function getRunOnce() { return $this->runOnce; }	
	
	/**
	 * Get the last run timestamp
	 *
	 * @param string $format
	 * @return string
	 */
	function getLastActualTimestamp($format = false) { 
		if (!$format) return $this->lastActualTimestamp;
		else return date($format, $this->lastActualTimestamp);
	}
}
?>
