<?php
/**
 * class Worker is the actual processor of jobs.
 *
 */
class Worker extends PHP_Fork {
	/**
	 * jobIdentifier
	 *
	 * @var integer
	 * @access public
	 */
	var $jobId 				= 0;
    
	/**
	 * the absolute path of the script that should be run
	 *
	 * @var string
	 * @access public
	 */
	var $script				= '';
	
	
	/**
	 * concurrency control
	 *
	 * @var boolean
	 * @access public
	 */
	var $concurrent			= false;
    
	/**
	 * implementation / environment identifier
	 *
	 * @var integer
	 * @access public
	 */
	var $implementationId	= 0;
    
	/**
	 * jobIdentifier
	 *
	 * @var boolean
	 * @access private
	 */
    var $_finished			= false;
    
    var $crontabId;
    
    var $dbLink;
	
    /**
     * construct a new worker
     *
     * @param string $name
     * @return worker
     */
    function worker($name) {
    	$this->PHP_Fork($name);
    }

    /**
     * Enter description here...
     *
     */
    function run() {
    	$this->dbLink = newDb();
        while (true) {
            $this->execute();
			$this->setAlive();
			sleep(1);
        }
    }

    /**
     * Get the local finished var
     *
     * @return boolean
     * @access public
     */
    function getFinished(){
    	return $this->getVariable('_kill');
    }
    
    /**
     * Set this worker to finish. Tells the daemon that the work is done!
     *
     * @return boolean
     */
    function setFinished(){
    	
    	
    	$log = ezcLog::getInstance();
		$log->log("Cronjob is run from crontab [{$this->crontabId}]", ezcLog::DEBUG, array('source' => 'vcrond') );
    	
    	//Check the crontab first
    	$crontab =  new Crontab($this->crontabId);
    	if ($crontab->getRunOnce()) {
			$log->log("Deleting crontab [{$this->crontabId}]. Run once", ezcLog::DEBUG, array('source' => 'vcrond') );
    		
    		//delete crontab
    		$crontab->delete();
    	} else {
			$log->log("Cronjob is not Run once", ezcLog::DEBUG, array('source' => 'vcrond') );    		
    	}
    	
    	
		$this->_finished = true;
    	$this->setVariable('_kill', 1);
    	mysqli_close($this->dbLink);
    	
    	return true;
    }
    
    /**
     * The actual execution happens right here
     *
     * @return void
     */
    function execute() {
    	global $db;    	
    	unset($output);
    	if (!$this->_finished) {
	    	if (!empty($this->script)) {	    		
				exec($this->script, $output);
			}
			
			$log = ezcLog::getInstance();
			$log->log("Executing cronjob [{$this->jobId}]", ezcLog::DEBUG, array('source' => 'vcrond') );
			
			$job = new CronJob($this->jobId);
			$job->endTimestamp = time();
			$job->isExitedNormally = 1;
			$job->results = (empty($results) ? '' : $results);
			$job->update();

			$this->setFinished();
			
    	}
	}
}
?>
