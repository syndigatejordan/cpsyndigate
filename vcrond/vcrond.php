#!/usr/bin/env php
<?php

/**
 *  This file is released under the GPL, any version you like
 * 
 * 
 * PHP CronDaemon v1.0
 * 
 * By Robin Speekenbrink
 * Kingsquare Information Services, 1-10-2007
 * 
 * 
 * 
 * 
 * 
 * Based on the original PHP_FORK class by Luca Mariano
 * This program is free software. You can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 * @author Robin Speekenbrink <info@kingsquare.nl>
 * @version 1.0
 * 
 * I juse ADODB classes from www.phplens.com/adodb but for clearity reasons i use the db_functions from this file.
 * Changing to another DB shouldn't pose any problems.
 */
/**
 *  @todo when preccess killed do not kill the childern till the childern finished working
*/

die('vcrond is stopped from the file /usr/local/syndigate/vcrond/vcrond.php'. PHP_EOL);
require_once dirname(__FILE__) . '/common.php';
$fp = false;


$controller = new cronDaemon('cronDaemon', SLEEP_INTERVAL, MAX_IDLE);
$controller->start();

$log = ezcLog::getInstance();
$log->log('starting deamon: '.$controller->getName().' ('.$controller->getPid().')', ezcLog::INFO, array('source' => 'vcrond'));
