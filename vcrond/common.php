<?php

//Load Syndigate commons
require_once dirname(__FILE__) . '/../ez_autoload.php';
//Load configuration
require_once APP_PATH . 'conf/vcrond.conf.php';

require_once dirname(__FILE__) . '/lib/miniDb.php';
require_once dirname(__FILE__) . '/lib/phpfork.php';
require_once dirname(__FILE__) . '/lib/worker.php';
require_once dirname(__FILE__) . '/lib/cron/tab.php';
require_once dirname(__FILE__) . '/lib/cron/daemon.php';
require_once dirname(__FILE__) . '/lib/cron/job.php';
require_once dirname(__FILE__) . '/lib/cron/parser.php';


/**
 * A central function for each sub process to create a new instance of the DB handle.
 * My version uses ADODB
 *
 * @return miniDb
 */
function newDb() {
	$log = ezcLog::getInstance();
	$log->source = 'vcrond';
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASS);
	if (!$link) {
		$log->log("Couldn't open connection to DB", ezcLog::FATAL);
		exit;
	}
	
	if (!mysqli_select_db($link, DB_NAME)) {
		$log->log("Couldn't select DB", ezcLog::FATAL);
		exit;		
	}
	return $link;
	
}
