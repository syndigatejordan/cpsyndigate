<?php 
$mailTo = array('alaa@corp.albawaba.com', 'mark@syndigate.info','khaled@syndigate.info','eyad@syndigate.info','majdi@syndigate.info','safaa@syndigate.info');


require_once '/usr/local/syndigate/ez_autoload.php';
define('SAVING_PATH', '/tmp/mark_report_' . date('M-d-Y', time()) .'/');	

//------ Script body -------//

require_once LINUX_SCRIPTS_PATH . "excel.php";
require_once LINUX_SCRIPTS_PATH . 'database_helper.php';
require_once SF_ROOT_DIR . '../classes/syndModel.php';
require_once SF_ROOT_DIR . '../classes/syndGenerate.php';



$c = new Criteria();
$c->add(ClientPeer::IS_ACTIVE ,1);
$allClients = ClientPeer::doSelect($c);


// Titles SUPPOSED to be finished (receiving, parsing. sending) with all thier clients
$c = new Criteria();
$c->add(TitlePeer::IS_ACTIVE , 1);
$c->add(TitlePeer::IS_RECEIVING,1);
$c->add(TitlePeer::IS_PARSING,1);
$c->add(TitlePeer::RECEIVING_STATUS, 0);
$parsingAndDistributing = TitlePeer::doSelect($c);


// Receiving Titles but not parsed yet
$c = new Criteria();
$c->add(TitlePeer::IS_RECEIVING,1);
$c->add(TitlePeer::IS_PARSING,0);
//$c->add(TitlePeer::RECEIVING_STATUS,1);
$receivingNotParsing = TitlePeer::doSelect($c);

// Parsing but they stopped sending
$c = new Criteria();
$c->add(TitlePeer::IS_ACTIVE , 1);
$c->add(TitlePeer::IS_RECEIVING, 1);
$c->add(TitlePeer::IS_PARSING,1);
$c->add(TitlePeer::RECEIVING_STATUS, 1);
$supposedToBeFinihed = TitlePeer::doSelect($c);

//Inactive titles
$c = new Criteria();
$c->add(TitlePeer::IS_ACTIVE,0);
$inactiveTitles = TitlePeer::doSelect($c);


mkdir(SAVING_PATH, 0777, true);
$files   = array();
$files[] = arrayToExcle($parsingAndDistributing,'Parsing_and_distributing');
$files[] = arrayToExcle($receivingNotParsing, 'Receiving_but_not_parsing yet');
$files[] = arrayToExcle($supposedToBeFinihed, 'Finished_but_stopped_sending');
$files[] = arrayToExcle($inactiveTitles, 'Inactive_titles');


$sendResult = sendEmail($files);

shell_exec("rm -r ". SAVING_PATH);

//------ End script body -------//





// ---------------- functions ----------------//

/**
 * Convert a array to excle file
 *
 * @param array $data : The data to fill	
 * @param array $headers : the headers names [ must equal the data columns ]
 * @param array $leftCol : 
 * @return unknown
 */
function arrayToExcle($titles, $filename ='', $allClients=false)
{	
	$db = new dataBaseHelper();	
	$excle;
	
	$c=new Criteria();
	$c->add(ClientPeer::IS_ACTIVE, 1);
	$allClients = ClientPeer::doSelect($c);
		
	foreach ($titles as $title) {
		$record['Title Id']   	 = $title->getId();
		$record['Title Name'] 	 = $title->getUniqueName();
		$record['Publisher Name']= $title->getPublisher()->getName();
		$record['Has parser']    = $title->getIsParsing() ? "Yes": "No";
		$record['Active']        = $title->getIsActive()? "Yes": "No";
		$record['Must be receiving']	 = $title->getIsReceiving()? "Yes": "No";
		
		if($title->getIsActive()) {
			$record['Currently receiving']  = $title->getReceivingStatus()? "No": "Yes";
		} else {
			$record['Currently receiving']  = $title->getIsReceiving()? "Sample received": "No";
		}
		
		
		if(is_array($allClients)) {
			
			foreach ($allClients as $client) {
				
				$name = $client->getClientName();
				$clientsTakingFromTitle = $db->getClientDealingTitlesIds($client->getId());
				
				if(count($clientsTakingFromTitle)==0) {
					$record[$name] = 'X';
				} else {
					if(!in_array($title->getId(), $clientsTakingFromTitle)) {
						$record[$name] = '';
					} else {
						$record[$name] = 'X';
					}
		
				} //End if(count($clientsTakingFromTitle)==0)
				
			} //End foreach
		}//End if
		
		$excle[] = $record;
	}//End foreach Titles
	
	$exportFile = 'xlsfile:/' . SAVING_PATH .$filename. " " . date('M-d-Y', time()) . ".xls";
	@$FP = file_put_contents($exportFile, serialize($excle));
	return str_replace('xlsfile:/', '', $exportFile);
}



function sendEmail($files)
{
	global $mailTo;

	$mail = new ezcMailComposer();
	
	$mail->from = new ezcMailAddress( 'report@syndigate.info', 'No reply ' );
	
	foreach($mailTo as $to) {
	 	$mail->addTo( new ezcMailAddress($to) );
	 }

	$mail->subject   = "Syndigate report";
	$mail->plainText = "Good day commander \n\r Kindly refer to the attachments to get the updated syndigate reports.";
	
	foreach($files as $file) {
		$mail->addAttachment($file);
	}	
	$mail->build();
	
	$transport = new ezcMailMtaTransport();
	$transport->send( $mail ); 
}
?>
