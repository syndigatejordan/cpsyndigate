<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
$publishers = explode(",", $argv[1]);
foreach ($publishers as $publisher) {
  $qp = mysql_query("select id,publisher_id from title where publisher_id=$publisher");
  while ($p = mysql_fetch_array($qp)) {
    $publisher_id = $p["publisher_id"];
    $title_id = $p["id"];
    $backuppath = "/syndigate/imgs/$publisher_id/$title_id";
    if (!is_dir($backuppath)) {
      mkdir($backuppath, 0777, true);
    }
    $q = mysql_query("select image.* from image 
  inner join article on image.article_id=article.id 
  and article.title_id=$title_id and img_name like '%syndigate.info%'");
    $original_old = "/syndigate/imgs/$publisher_id/$title_id";
    $cmd = "mv $original_old $original_old" . "_old";
    echo $cmd . chr(10);
    exec($cmd);
    while ($row = mysql_fetch_assoc($q)) {
      $img_name = $row['img_name'];
      if (!empty($img_name)) {
        $checker = explode('?', $img_name);
        if (count($checker) == 1) {
          $img_path = explode('/', $img_name);

          $original_image = "/syndigate/imgs/$publisher_id/$title_id" . "_old/" . $img_path[5] . '/' . $img_path[6];
          $img_path = $backuppath . '/' . $img_path[5] . '/';
          if (!is_dir($img_path)) {
            mkdir($img_path, 0777, true);
          }
          $command = "mv $original_image $img_path";
          echo $row['id'] . ' ' . $command . chr(10);
          exec($command);
        }
      }
    }

    $q = mysql_query("select image_tmp.* from image_tmp 
  inner join article_tmp on image_tmp.article_id=article_tmp.id 
  and article_tmp.title_id=$title_id and img_name like '%syndigate.info%'");

    while ($row = mysql_fetch_assoc($q)) {
      $img_name = $row['img_name'];
      if (!empty($img_name)) {
        $checker = explode('?', $img_name);
        if (count($checker) == 1) {
          $img_path = explode('/', $img_name);

          $original_image = "/syndigate/imgs/$publisher_id/$title_id" . "_old/" . $img_path[5] . '/' . $img_path[6];
          $img_path = $backuppath . '/' . $img_path[5] . '/';
          if (!is_dir($img_path)) {
            mkdir($img_path, 0777, true);
          }
          $command = "mv $original_image $img_path";
          echo $row['id'] . ' ' . $command . chr(10);
          exec($command);
        }
      }
    }

    $rm_cmd = "rm -r /syndigate/imgs/$publisher_id/$title_id" . "_old";
    //echo $rm_cmd . chr(10);
    //exec($rm_cmd);
    echo "Sleep 10 Seconds" . chr(10);
    sleep(10);
  }
}