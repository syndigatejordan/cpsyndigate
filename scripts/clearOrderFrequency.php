<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
$date = strtotime('1 weeks ago');
$of = new Criteria();
$of->add(OrdersFrequencyPeer::END, $date, Criteria::LESS_EQUAL);
$order_frequency = OrdersFrequencyPeer::doSelect($of);
$ids = array();
if (!empty($order_frequency)) {
  foreach ($order_frequency as $record) {
    $ids[] = $record->getOId();
  }
}
foreach ($ids as $id) {
  $c = new Criteria();
  $c->add(OrdersFrequencyPeer::O_ID, $id);
  OrdersFrequencyPeer::doDelete($c);
}
?>