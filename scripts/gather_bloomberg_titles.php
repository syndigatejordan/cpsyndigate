<?php

// Check if an instance exists from the same scripts 
$avilable_instances = 0;
$output = shell_exec('ps aux | grep gather_bloomberg_titles.php | grep -v "/bin/sh -c" | grep -v "sh -c" | grep -v "grep" ');
$output = explode(PHP_EOL, $output);
foreach ($output as $o) {
  if ($o) {
    $avilable_instances++;
  }
}

if ($avilable_instances > 1) {
  die();
}

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

$cURLConnection = curl_init();
curl_setopt($cURLConnection, CURLOPT_URL, 'http://cms.syndigate.info/rest/cms-titles');
curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
$phoneList = curl_exec($cURLConnection);
curl_close($cURLConnection);
$ids= json_decode($phoneList,true);
$ids=$ids["ids"];

$q = mysql_query("select t.id from titles_clients_rules r inner join title t on r.title_id=t.id where client_id in (124, 134, 138) and title_id IN (10361,10418,10382,10372,10370,10385,10386,10817,11194,10388,10828,10371,11934,10420,12383,12420,12421,12422,12423,12424,12425,12426,12427,12428,12429,12430,12431,12432,12433,12434,12435,12436,12437,12438,12439,12440,12441,12442,12443,12444,12445,12446,12447,12448,12449,12450,12451,12452,12453,12454,12455,12456,12457,10400,10441,10442,10515,10585,10759,10788,11706,12416,12627,12628,12629, 10817, 12636, 12637, 12638, 12304, 12703) and is_active =1 order by directory_layout asc , t.id asc;");
//$mailTo = array('eyad@syndigate.info , khaled@syndigate.info');
$mailTo = array('eyad@syndigate.info');
$titles = "";
while ($title = mysql_fetch_array($q)) {
  $titles.=$title["id"] . ",";
}
$titles = trim($titles, ",");
$result = mysql_query("select PUBLISHER_ID,title.id
  ,title_frequency_id
  ,(select sum(f.frequency) from orders_frequency f where title.id=f.title_id group by title_id limit 1) frq 
  from title 
  where 
  title.is_active=1 and title.id in ($titles) and title.id not in ($ids) 
  order by title_frequency_id asc, frq asc , title.id asc;");
while ($row = mysql_fetch_assoc($result)) {
  if ($row['id'] == 1655 || $row['id'] == 1652 || $row['id'] == 1656) { // CountryWatch IDs
    echo "Title ID:" . $row['id'] . ' CountryWatch ID Skipped' . chr(10);
    continue;
  }
  $homePath = HOMEPATH . "/" . $row["PUBLISHER_ID"] . "/" . $row["id"];
  $wcPath = WCPATH . "/" . $row["PUBLISHER_ID"] . "/" . $row["id"];
  $pwcPath = PWCPATH . "/" . $row["PUBLISHER_ID"] . "/" . $row["id"];

  if (is_dir($homePath) && is_dir($wcPath) && is_dir($pwcPath)) {
    $d = date("[Y-m-d H:i:s]");
    $cmd = "/usr/local/syndigate/classes/gather/syndGather.php " . $row['id'];
    echo "$d  Title ID: " . $row['id'] . ' title_frequency_id: ' . $row['title_frequency_id'] . chr(10);
    $start = date('Y-m-d H:i:s');
    shell_exec($cmd);
    $end = date('Y-m-d H:i:s');
    $id = $row['id'];
    $frequency = strtotime($end) - strtotime($start);
    if ($frequency >= 60) {
      $OrdersFrequency = new OrdersFrequency();
      $OrdersFrequency->setTitleId($id);
      $OrdersFrequency->setStart($start);
      $OrdersFrequency->setEnd($end);
      $OrdersFrequency->setFrequency($frequency);
      $OrdersFrequency->save();
    }
    //sleep(2);
  } else {
    sendEmail($row["id"], $mailTo);
    echo "Title ID:" . $row['id'] . ' have problem on directory' . chr(10);
  }
}

function sendEmail($id, $mailTo) {
  $body = "Title id $id have problem on directory ,This issue needs to be fixed ASAP please.";
  $mail = new ezcMailComposer();
  $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply');
  foreach ($mailTo as $to) {
    $mail->addTo(new ezcMailAddress($to));
  }
  $mail->subject = "Title id $id have problem [URGENT],";
  //$mail->htmlText  = $body;
  $mail->plainText = $body;
  $mail->build();
  $transport = new ezcMailMtaTransport();
  $transport->send($mail);
}

?>
