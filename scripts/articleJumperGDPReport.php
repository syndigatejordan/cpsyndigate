<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
//$mailTo = array('khaled@syndigate.info','eyad@syndigate.info','carla@syndigate.info','safaa@syndigate.info');
$mailTo = array( 'khaled@syndigate.info', 'majdi@syndigate.info', 'eyad@syndigate.info');
//$mailTo = array('eyad@syndigate.info');
$mailTo_2 = array('khaled@syndigate.info', 'majdi@syndigate.info', 'eyad@syndigate.info');
//-------------------------------------- Main--------------------------------------------------
$slave = connect_slave();
$date = date('Y-m-d', strtotime('-1 day'));
//$date = "2018-03-13";
$title_id = getTitleId($slave);
$mapped_title = getMappedTitle();
$data = array();
$data = getCountInfo($mapped_title, $title_id, $date, $slave);



// Sort the data with mid descending
// Add $data as the last parameter, to sort by the common key
array_multisort($data["total"], SORT_DESC);
$msg = "Global Data Point Articles count for date $date. , (Date is based on the date tag in the XML file, not the date when they were sent/updated on the XML file) \n\n\n";

sendEmail($msg, $mailTo, $data, $title_id);

$msg = "Global DataPoint Articles Map for Titles don't have articles  for date $date. , (Date is based on the date tag in the XML file, not the date when they were sent/updated on the XML file) \n\n\n";

sendEmailForTotal($msg, $mailTo_2, $data, $title_id);

function getMappedTitle() {
  $titles_id = array(5, 6, 9, 11, 21, 23, 25, 35, 44, 55, 62, 89, 159, 163, 164, 167, 174, 183, 185, 200, 217, 230, 243, 363, 364, 365, 366, 367, 368, 369, 386,
      387, 389, 391, 392, 393, 398, 399, 401, 425, 496, 598, 600, 701, 713, 775, 780, 784, 786, 787, 788, 789, 793, 814, 878, 894, 896, 897, 898,
      899, 900, 901, 902, 903, 904, 908, 1005, 1007, 1048, 1056, 1058, 1059, 1075, 1084, 1143, 1217, 1228, 1244, 1286, 1310, 1312, 1349,
      1386, 1387, 1388, 1390, 1436, 1442, 1505, 1526, 1609, 1633, 1673, 1674, 1795, 1800, 1801, 1803, 1808, 1809, 1811, 2709, 2724, 2732, 2745, 2746, 2747,
      2749, 2754, 2756, 2757, 2758, 2759, 2760, 2761, 2762, 2764, 2765, 2766, 2767, 2768, 2769, 2771
  );
  $mapped_title = array();
  $i = 0;
  foreach ($titles_id as $id) {
    $mapped_title[$i]['mapped_title'] = $id;
    $i++;
  }
  return $mapped_title;
}

function getTitleId($slave) {
  $query = mysql_query("select title_id from article_jumperGDP group by title_id;", $slave);
  $title_id = array();
  $i = 0;
  while ($row = mysql_fetch_row($query)) {
    $title_id[$i]['title_id'] = $row[0];
    $i++;
  }
  return $title_id;
}

function getCountInfo($mapped_title, $title_id, $date, $slave) {
  $count_article = 0;
  $data = array();
  $i = 0;
  foreach ($mapped_title as $title) {
    $result = mysql_query("select name from title where id={$title['mapped_title']};", $slave);
    $name = mysql_fetch_assoc($result);
    $data["{$title['mapped_title']}"]["name"] = "<th>{$name["name"]} ({$title['mapped_title']})</th>";


    foreach ($title_id as $id) {
      $data["{$title['mapped_title']}"]["{$id['title_id']}"] = "0";
    }
    $result = mysql_query("select title_id,count(*) cnt from article_jumperGDP where  is_sent=1 and mapped_title={$title['mapped_title']} and  date(send_date)='{$date}' group by title_id;", $slave);
    while ($row = mysql_fetch_row($result)) {
      foreach ($title_id as $id) {
        if ($row[0] == $id['title_id']) {
          $data["{$title['mapped_title']}"]["{$id['title_id']}"] = $row[1];
        }
      }
    }

    $result = mysql_query("SELECT COUNT(*) as sum FROM article INNER JOIN article_original_data ON article.article_original_data_id = article_original_data.id WHERE article.title_id = {$title['mapped_title']} AND article.date = '$date'", $slave);
    $row = mysql_fetch_assoc($result);
    if (is_null($row["sum"])) {
      $row["sum"] = '0';
    }
    $data["{$title['mapped_title']}"]["total"] = "{$row["sum"]}";
    $data["total"]["{$title['mapped_title']}"]["count"] = "{$row["sum"]}";
    $data["total"]["{$title['mapped_title']}"]["title_id"] = $title['mapped_title'];
    $i++;
  }
  $data["GDP"]["name"] = "<th>Total</th>";
  foreach ($title_id as $id) {
    $result = mysql_query("select count(*) sum from article_jumperGDP where title_id ={$id['title_id']} and date(send_date)='{$date}';", $slave);
    $row = mysql_fetch_assoc($result);
    if (is_null($row["sum"])) {
      $row["sum"] = '0';
    }
    $data["GDP"]["{$id['title_id']}"] = "{$row["sum"]}";
  }
  return $data;
}

function sendEmail($body, $mailTo, $data, $title_id) {
  $table = "";
  $total = 0;
  $table .= "<table border='1'><tr><th>Mapped Title</th>";
  foreach ($title_id as $id) {
    $result = mysql_query("select name from title where id={$id['title_id']};");
    $name = mysql_fetch_assoc($result);
    $table .= "<th>{$name["name"]} ({$id['title_id']})</th>";
  }
  $table .= "<th>Count articles from publisher</th></tr>";
  foreach ($data["total"] as $valueTotal) {
    $table .= "<tr>" . $data["{$valueTotal["title_id"]}"]["name"];
    foreach ($data["{$valueTotal["title_id"]}"] as $keyCount => $count) {
      if ($keyCount == "name" || $keyCount == "total") {
        continue;
      }
      $table .="<td align='center'>$count</td>";
    }
    $total +=intval($data["{$valueTotal["title_id"]}"]["total"]);
    if (!empty($data["{$valueTotal["title_id"]}"]["total"])) {
      $table .="<td align='center'>{$data["{$valueTotal["title_id"]}"]["total"]}</td></tr>";
    }
  }
  $table .= "<tr>" . $data["GDP"]["name"];
  foreach ($data["GDP"] as $keyGDP => $valueGDP) {
    if ($keyGDP == "name") {
      continue;
    }
    $table .="<td align='center'>$valueGDP</td>";
  }
  $table .= "<td align='center'>{$total}</td></tr>";
  $table .="</table>";
  $body = $body . $table;
  $body = wordwrap($body, 75, "\r\n");

  $mail = new ezcMailComposer();
  $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply');


  foreach ($mailTo as $to) {
    $mail->addTo(new ezcMailAddress($to));
  }
  $mail->subject = "Global Data Point Custom Sent Report,";
  $mail->htmlText = $body;
  $mail->build();

  $transport = new ezcMailMtaTransport();
  $transport->send($mail);
}

function sendEmailForTotal($body, $mailTo, $data, $title_id) {
  $table = "";
  $total = 0;
  $table .= "<table border='1'><tr>"
          . "<th>Title</th>"
          . "<th>Count from Title</th>"
          . "<th>Count from Map</th>"
          . "</tr>";
  $totalMap = 0;
  foreach ($data as $key => $values) {
    if ($key == "GDP" || $key == "total") {
      continue;
    }
    if ($values["total"] <= 0) {
      $table .= "<tr>";
      $valueTotal = 0;
      foreach ($values as $keyCount => $value) {
        if ($keyCount == "name") {
          $table .=$value;
        } elseif ($keyCount == "total") {
          $table .="<td align='center'>$value</td>";
        } else {
          $valueTotal+=intval($value);
        }
      }
      $totalMap+=$valueTotal;
      $table .="<td align='center'>$valueTotal</td>";
      $table .= "</tr>";
    }
  }
  $table .= "<tr>"
          . "<th>Total</th>"
          . "<th>0</th>"
          . "<th>$totalMap</th>"
          . "</tr>";
  $table .="</table>";
  $body = $body . $table;
  $body = wordwrap($body, 75, "\r\n");

  $mail = new ezcMailComposer();
  $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply');


  foreach ($mailTo as $to) {
    $mail->addTo(new ezcMailAddress($to));
  }
  $mail->subject = "Global Data Point Custom Sent Report for Empty titles,";
  $mail->htmlText = $body;
  $mail->build();

  $transport = new ezcMailMtaTransport();
  $transport->send($mail);
}

function connect_slave() {
  $conn = @mysql_connect("10.200.200.66", "syndigate", "bQAuA4DuhtX6xyaE");
  //$conn = @mysql_connect("192.168.1.200", "root", "root");
  if (!$conn) {
    die("Error connecting to syndigate : " . mysql_error());
  }
  mysql_select_db("syndigate", $conn);
  return $conn;
}
