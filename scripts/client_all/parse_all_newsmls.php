<?php 
require_once '/usr/local/syndigate/scripts/propel_include_config.php';
require_once dirname(__FILE__ ) . '/newsMlParser.php';

$date = date('Y/m/d', strtotime('-1 day'));
if(isset($argv[1]) && !empty($argv[1])) {
	$date = str_replace('-', '/', $argv[1]);
}

$dir   = '/syndigate/clients/24/' . $date . '/';
$files = scandir($dir);
$newsMlParser = new NewsMlParser();

logStr("checking dir $dir");
logStr("Files found :");
logStr($files, true);

foreach ($files as $f) {
	if('.' == $f || '..' == $f) {
		continue;
	}
	$file     = $dir . $f;
	$titleId  = explode('_', $f);
	$titleId  = $titleId[0];
	
	logStr("starting with file $file");
	logStr("title id : $titleId");
	
	$articles = $newsMlParser->parseNewsMlString(file_get_contents($file));
				
	if(!empty($articles)) {
		logStr("count of articles is " . count($articles));
		foreach ($articles as $article) {
			insertNewsMlParsedArticleInDb($titleId, $article);
		}
	} else {
		logStr("no articles found");
	}
}
logStr("script finished.");




// --- functions -------------------------------------------------------------//
function logStr($str, $print = false) {
	//return ;
	if($print) {
		print_r($str);
	} else {
		echo $str . PHP_EOL;
	}
}

function insertNewsMlParsedArticleInDb($titleId, $article) {
	
	global  $conn, $date;
	
	$sql = "INSERT INTO article_cl_24 SET ";
	$sql.= "id       	="  . (int)$article['id'] . ', '; 
	$sql.= "title_id 	="  . (int)$titleId . ', '; 
	$sql.= "headline 	= " . "'" . mysql_escape_string($article['headline']) . "', ";
	$sql.= "summary  	= " . "'" . mysql_escape_string($article['abstract']) . "', ";
	$sql.= "date     	= " . "'" . mysql_escape_string($article['date']    ) . "', ";
	$sql.= "category 	= " . "'" . mysql_escape_string($article['category']) . "', ";
	$sql.= "report_date = " . "'" . mysql_escape_string($date               ) . "', ";
	$sql.= "body     	= " . "'" . mysql_escape_string($article['story']   ) . "' ";
	
	mysql_query($sql, $conn);
} 