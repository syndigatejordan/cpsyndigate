<?php 

require_once '/usr/local/syndigate/classes/parse/syndParseXmlContent.php';

class NewsMlParser extends syndParseXmlContent{
	
	public function __construct() {
		// overriding upper constructor
	}
	/**
	 * Parse NewsMl String to articles array
	 *
	 * @param String $XMLstring
	 * @return array
	 */
	public  function parseNewsMlString($XMLstring) {
	
		$articles = array();
		
		$rawArticles = $this->getRawArticles($XMLstring);
		$this->logStr("count of articles in file " . count($rawArticles));
		
		foreach($rawArticles as $rawArticle) {
			$article = $this->getArticle($rawArticle);
					
			if(trim(strip_tags($article['story']))) {
				$articles[] = $article;
			}
		}
		
		return $articles;
	}
	
	public function getArticle(&$text) {
		try {
			$this->logStr("getting aricle");
			
			$article['id']			  = $this->getId($text);
			$article['headline']      = strip_tags($this->getHeadline($text));
			$article['abstract']      = strip_tags($this->getAbstract($text));
			$article['date']          = $this->getArticleDate($text);
			$article['category']        = $this->getOriginalCategory($text);
			$article['original_data'] = $this->getOriginalData($text);
			$article['story']         = $this->getStory($text);
						
		} catch (Exception $e) {
			// Nothing ...
		}
		
		return $article;
	}
	
	
	public function getRawArticles(&$text) {		
		$rows = $this->getElementsByName('NewsComponent', $text);
		if(!$rows || empty($rows)) {
			return array();
		}
		
		$combinedRows = array();
		$tmp = '';
		foreach ($rows as $row) {
			if(!$tmp) {
				$tmp = $row;
			} else {
				$combinedRows[] = $tmp . $row;
				unset($tmp);
			}
		}
		return $combinedRows;
	}
	
	public function getHeadline(&$text) {
		return $this->getElementByName('HeadLine', $text);
	}
	
	public function getAbstract(&$text) {
		$rez = $this->getElementsByName('DataContent', $text);
		return $rez[0]; 
	}
	
	public function getArticleDate(&$text) {
		preg_match('/<Property FormalName="articleDate" Value="[0-9]{4}-[0-9]{2}-[0-9]{2}" \/>/', $text, $matches);
		$dateTag = $matches[0];
		$date = preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', $dateTag, $matches);
		return $this->dateFormater($matches[0]);
	}
	
	public function getOriginalCategory(&$text) {
		preg_match('/<Property FormalName="originalCategory" Value="[a-zA-Z0-9]*" \/>/', $text, $matches);
		$tag = $matches[0];
		preg_match('/Value="[a-zA-Z0-9]*"/', $tag, $matches);
		$tag = $matches[0];
		$tag = substr($tag, strpos($tag, '="')+2, strlen($tag)-8);
		return $tag;
	}
	
	public function getOriginalData(&$text) {
		
	}
	
	public function getStory(&$text) {
		$rez = $this->getElementsByName('DataContent', $text);
		return $rez[1]; 
	}
	
	public function getId(&$text) {
		
		preg_match('/<Property FormalName="articleId" Value="[0-9]*" \/>/', $text, $matches);
		$tag = $matches[0];
		
		$tag = preg_match('/"[0-9]*"/', $tag, $matches);
		$tag = $matches[0];
		$tag = substr($tag, 1, strlen($tag) -2);
		return $tag;
	}	
	
	
	public function logStr($str, $print = false) {
		//return ;
		if($print) {
			print_r($str);
		} else {
			echo $str . PHP_EOL;
		}
	}
	
}