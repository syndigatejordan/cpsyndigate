<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

//$mailTo = array('khaled@syndigate.info','eyad@syndigate.info','carla@syndigate.info','safaa@syndigate.info');
$mailTo = array('hani@corp.albawaba.com','mark@syndigate.info','khaled@syndigate.info','eyad@syndigate.info');
//-------------------------------------- Main--------------------------------------------------

$date = date('Y-m-d', strtotime('-1 day'));
$countInfo = getCountInfo($date);
$countInfo = "EMBIN Articles count for date $date. , (Date is based on the date tag in the XML file, not the date when they were sent/updated on the XML file) \n\n\n" . $countInfo;
$countInfo .= $msg . "\n";
sendEmail($countInfo, $mailTo);
echo $countInfo . PHP_EOL;
deleteOldArticles();

function getCountInfo($date) {
  $info = "<table>";
  $result_1 = mysql_query("select count(*) cnt from article where title_id =538 and date='{$date}';");
  $row_1 = mysql_fetch_assoc($result_1);
  $result_2 = mysql_query("select count(*) cnt from embin_custom where date='{$date}'");
  $row_2 = mysql_fetch_assoc($result_2);
  $original_count = intval($row_1["cnt"]) - intval($row_2["cnt"]);
  if ($original_count <= 0) {
    $original_count = 0;
  }
  $info .="<tr><td>Original EMBIN (Emerging Markets Business Information News) </td><td  style='width:50px;text-align:center;'>{$original_count}</td></tr>";
  $embin_custom = mysql_query("select publisher,count(*) count from embin_custom where date='{$date}' group by publisher");
  $publishers = array();
  $i = 0;
  while ($row = mysql_fetch_row($embin_custom)) {
    $publishers[$i]['publisher'] = $row[0];
    $publishers[$i]['count'] = $row[1];
    $i++;
  }
  foreach ($publishers as $publisher) {
    $msg .= "<tr><td> {$publisher['publisher']}</td><td style='width:50px;text-align:center;'>{$publisher['count']}</td></tr>";
  }
  $info .=$msg ."<tr><td>Total Articles</td><td style='width:50px;text-align:center;'>{$row_1["cnt"]}</td></tr>";
  $info .="</table>";
  return $info;
}

function sendEmail($body, $mailTo) {
  $mail = new ezcMailComposer();
  $mail->from = new ezcMailAddress('report@syndigate.info', 'EMBIN No reply');
  foreach ($mailTo as $to) {
    $mail->addTo(new ezcMailAddress($to));
  }
  $mail->subject = "EMBIN Custom Report,";
  $mail->htmlText = $body;
  //$mail->plainText = $body;
  $mail->build();
  $transport = new ezcMailMtaTransport();
  $transport->send($mail);
}

function deleteOldArticles() {
  $date = date('Y-m-d', strtotime('-2 day'));
  mysql_query("delete from embin_custom where date <'{$date}';");
}
