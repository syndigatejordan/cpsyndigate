#!/bin/bash -x

# =================================================
# This script will install syndigate system on a 
# fresh installed debian system
# Please note that mysql, apache, php, symfony must 
# be installed with their modules on the system
# =================================================
# Created by Abd Al-fattah Abu Dhair
# Under the supervision of Zaid Amireh
# =================================================

# Syndigation System Paths definition
SYND_PATH='/usr/local/syndigate'
SYNDWEB_PATH='/var/www/syndigate_web'

# System commands definition
LN='/bin/ln'
MKDIR='/bin/mkdir'
CHOWN='/bin/chown'
CHMOD='/bin/chmod'
USERMOD='/usr/sbin/usermod'
USERDEL='/usr/sbin/userdel'
GROUPDEL='/usr/sbin/groupdel'
RM='/bin/rm'
MV='/bin/mv'
CP='/bin/cp'
GROUPMOD='/usr/sbin/groupmod'
ECHO='/bin/echo'

# Create the needed symbolic links for the system
$LN -s $SYND_PATH/syndigate_web $SYNDWEB_PATH
$LN -s $SYND_PATH/scripts/syndigate.conf /etc/apache2/sites-enabled
$LN -s $SYND_PATH/scripts/vcrond /etc/init.d
$LN -s $SYND_PATH/misc/nagios/sms_api.php /usr/local/bin
$LN -s $SYND_PATH/misc/nagios/commands.cfg /usr/local/nagios/etc
$LN -s $SYND_PATH/misc/nagios/albawabaservices.cfg /usr/local/nagios/etc


# Define paths in nagios.cfg

$ECHO "# Define albawaba services" >> /usr/local/nagios/etc/nagios.cfg
$ECHO "###########################" >> /usr/local/nagios/etc/nagios.cfg
$ECHO "cfg_file=/usr/local/nagios/etc/albawabaservices.cfg" >> /usr/local/nagios/etc/nagios.cfg
$ECHO "# Syndigate nagios path's" >> /usr/local/nagios/etc/nagios.cfg
$ECHO "#########################" >> /usr/local/nagios/etc/nagios.cfg
$ECHO "cfg_dir=/etc/nagios/albawaba" >> /usr/local/nagios/etc/nagios.cfg

# Change Owner of /var/www/syndigate_web file to www-data
$CHOWN -R www-data:www-data $SYNDWEB_PATH/*

# Change the user/group from svnserve user to become as root
$USERMOD -g 0 -u 0 -o svnserve

# Insert the needed sudo command in /etc/sudoers
$CHMOD 660 /etc/sudoers
[ -n "$(grep '#Custom' /etc/sudoers)" ] 		|| $ECHO "#Custom Syndigation system needed commands" >> /etc/sudoers
[ -n "$(grep '#======' /etc/sudoers)" ] 		|| $ECHO "#=========================================" >> /etc/sudoers
[ -n "$(grep 'syndAddSource.sh' /etc/sudoers)" ] 	|| $ECHO "www-data		ALL = NOPASSWD : $SYNDWEB_PATH/batch/syndAddSource.sh" >> /etc/sudoers
[ -n "$(grep 'syndAddClient.sh' /etc/sudoers)" ] 	|| $ECHO "www-data		ALL = NOPASSWD : $SYNDWEB_PATH/batch/syndAddClient.sh" >> /etc/sudoers
[ -n "$(grep 'syndChangePassword.sh' /etc/sudoers)" ] 	|| $ECHO "www-data		ALL = NOPASSWD : $SYNDWEB_PATH/batch/syndChangePassword.sh" >> /etc/sudoers
[ -n "$(grep 'syndDeletePublisher.sh' /etc/sudoers)" ] 	|| $ECHO "www-data         ALL = NOPASSWD : $SYNDWEB_PATH/batch/syndDeletePublisher.sh" >> /etc/sudoers
[ -n "$(grep 'syndDeleteTitle.sh' /etc/sudoers)" ] 	|| $ECHO "www-data         ALL = NOPASSWD : $SYNDWEB_PATH/batch/syndDeleteTitle.sh" >> /etc/sudoers
[ -n "$(grep 'syndDeleteClient.sh' /etc/sudoers)" ] 	|| $ECHO "www-data         ALL = NOPASSWD : $SYNDWEB_PATH/batch/syndDeleteClient.sh" >> /etc/sudoers
[ -n "$(grep $USERDEL /etc/sudoers)" ] 			|| $ECHO "www-data		ALL = NOPASSWD : $USERDEL" >> /etc/sudoers 
[ -n "$(grep $GROUPDEL /etc/sudoers)" ] 		|| $ECHO "www-data		ALL = NOPASSWD : $GROUPDEL" >> /etc/sudoers
[ -n "$(grep $RM /etc/sudoers)" ] 			|| $ECHO "www-data		ALL = NOPASSWD : $RM" >> /etc/sudoers
[ -n "$(grep $MV /etc/sudoers)" ] 			|| $ECHO "www-data		ALL = NOPASSWD : $MV" >> /etc/sudoers
[ -n "$(grep $CP /etc/sudoers)" ] 			|| $ECHO "www-data		ALL = NOPASSWD : $CP" >> /etc/sudoers
[ -n "$(grep $CHOWN /etc/sudoers)" ] 			|| $ECHO "www-data		ALL = NOPASSWD : $CHOWN" >> /etc/sudoers
[ -n "$(grep $USERMOD /etc/sudoers)" ] 			|| $ECHO "www-data		ALL = NOPASSWD : $USERMOD" >> /etc/sudoers
[ -n "$(grep $GROUPMOD /etc/sudoers)" ] 		|| $ECHO "www-data		ALL = NOPASSWD : $GROUPMOD" >> /etc/sudoers
[ -n "$(grep '/etc/init.d/nagios' /etc/sudoers)" ] 	|| $ECHO "www-data		ALL = NOPASSWD : /etc/init.d/nagios" >> /etc/sudoers
$CHMOD 440 /etc/sudoers

# Logrorate the logs for Syndigatation system
$ECHO "/var/log/syndigate/*.log {" >> /etc/logrotate.d/syndigate
$ECHO "		daily" >> /etc/logrotate.d/syndigate
$ECHO "		missingok" >> /etc/logrotate.d/syndigate
$ECHO "		rotate 7" >> /etc/logrotate.d/syndigate
$ECHO "		compress" >> /etc/logrotate.d/syndigate
$ECHO "		delaycompress" >> /etc/logrotate.d/syndigate
$ECHO "		notifempty" >> /etc/logrotate.d/syndigate
$ECHO "		create 777 svnserve svnserve" >> /etc/logrotate.d/syndigate
$ECHO "		endscript" >> /etc/logrotate.d/syndigate
$ECHO "}" >> /etc/logrotate.d/syndigate
$ECHO "---------------------" >> /etc/logrotate.d/syndigate
$ECHO "/var/www/syndigate_web/log/*.log {" >> /etc/logrotate.d/syndigate
$ECHO "daily" >> /etc/logrotate.d/syndigate
$ECHO "		missingok" >> /etc/logrotate.d/syndigate
$ECHO "		rotate 7" >> /etc/logrotate.d/syndigate
$ECHO "		compress" >> /etc/logrotate.d/syndigate
$ECHO "		delaycompress" >> /etc/logrotate.d/syndigate
$ECHO "		notifempty" >> /etc/logrotate.d/syndigate
$ECHO "		create 777 www-data www-data" >> /etc/logrotate.d/syndigate
$ECHO "		endscript" >> /etc/logrotate.d/syndigate
$ECHO "" >> /etc/logrotate.d/syndigate

# Create shepherd Supervise files
$MKDIR -p /var/shepherd
$MKDIR -p /var/log/syndigate/shepherd/stdout/

$LN -s /var/log/syndigate/shepherd/stdout/ /var/shepherd/log
$ECHO "#!/bin/sh" >> /var/shepherd/run
$ECHO "exec /usr/local/syndigate/shepherd/shepherd.php --config=/usr/local/syndigate/conf/shepherd.conf.php" >> /var/shepherd/run
$CHMOD 755 /var/shepherd
$CHMOD 755 /var/shepherd/run

$ECHO "#!/bin/sh" >> /var/log/syndigate/shepherd/stdout/run
$ECHO "exec multilog t s10000000 ./main" >> /var/log/syndigate/shepherd/stdout/run
$LN -s /var/shepherd /service/shepherd
$CHMOD 755 /var/log/syndigate/shepherd/stdout
$CHMOD 755 /var/log/syndigate/shepherd/stdout/run


# Create SVN Supervise files
$MKDIR -p /var/svnserve/repos
$MKDIR -p /var/svnserve/supervise/log/main
$CHOWN -R svnserve: /var/svnserve

$ECHO "#!/bin/sh" >> /var/svnserve/supervise/run
$ECHO "exec setuidgid svnserve /usr/bin/svnserve -d --foreground -r /var/svnserve/repos" >> /var/svnserve/supervise/run
$CHMOD 755 /var/svnserve/supervise
$CHMOD 755 /var/svnserve/supervise/run

$ECHO "#!/bin/sh" >> /var/svnserve/supervise/log/run
$ECHO "exec setuidgid svnserve multilog t s10000000 ./main" >> /var/svnserve/supervise/log/run
$CHMOD 1755 /var/svnserve/supervise/log
$CHMOD 755 /var/svnserve/supervise/log/run
$LN -s /var/svnserve/supervise/ /service/svnserve

