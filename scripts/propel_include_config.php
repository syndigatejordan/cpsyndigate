<?php 

require_once '/usr/local/syndigate/ez_autoload.php';

// -- symfony --/

if(!defined('SF_ROOT_DIR')) {
	define('SF_ROOT_DIR',    '/usr/local/syndigate/syndigate_web');
}

if(!defined('SF_APP')) {
	define('SF_APP',         'technical');
}

if(!defined('SF_ENVIRONMENT')) {
	define('SF_ENVIRONMENT', 'dev');
}

if(!defined('SF_DEBUG')) {
	define('SF_DEBUG',       true);
}

require_once(SF_ROOT_DIR.DIRECTORY_SEPARATOR.'apps'.DIRECTORY_SEPARATOR.SF_APP.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.php');

$databaseManager = new sfDatabaseManager();
$databaseManager->initialize();
$con = Propel::getConnection();

$propelConf = Propel::getConfiguration();
$dbConf = $propelConf['datasources']['propel']['connection'];
$dsn = sprintf('%s://%s:%s@%s/%s', $dbConf['phptype'], $dbConf['username'], $dbConf['password'], $dbConf['hostspec'], $dbConf['database']);
// -- symfony --/

global $conn, $db;

$conn = mysql_connect($dbConf['hostspec'], $dbConf['username'], $dbConf['password']) or die('Can not connect to mysql');
$con = mysql_connect($dbConf['hostspec'], $dbConf['username'], $dbConf['password']) or die('Can not connect to mysql');
$db   = mysql_select_db($dbConf['database']) OR die('Can not select database');

