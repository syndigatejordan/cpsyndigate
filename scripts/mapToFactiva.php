<?php

/* This script get last report articles from title ID 193 and add it to the article_jumper table. Should be run each 2 hours
 * create table article_jumper (`id` int(255) NOT NULL AUTO_INCREMENT,article_id int(255),report_id int(255),title_id int(255),is_sent tinyint DEFAULT '0', mapped_title int(255), send_date datetime DEFAULT NULL, PRIMARY KEY (`id`),KEY `article_jumper_article_id` (`article_id`),KEY `article_jumper_report_id` (`report_id`), KEY `article_jumper_is_sent` (`is_sent`), KEY `article_jumper_send_date` (`send_date`)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
 * alter table article_jumper add `mapped_date` datetime DEFAULT NULL;
 */
require_once '/usr/local/syndigate/scripts/propel_include_config.php';

//$title_id = 193;
// 'Albawaba News'=>812, 'Albawaba Projects'=>811, 'Albawaba Contract Award'=>813
// php /usr/local/syndigate/scripts/mapToFactiva.php 812
// php /usr/local/syndigate/scripts/mapToFactiva.php 811
// php /usr/local/syndigate/scripts/mapToFactiva.php 813
$title_id = trim($argv[1]);
// $title_map = array(TITLE_ID=>PATCHES,TITLE_ID=>PATCHES);
/*

  406 => array('title_id' => 406, 'share_percent' => 0.0006926054186, 'max_per_patch' => 2, 'articles_count' => 0),
  408 => array('title_id' => 408, 'share_percent' => 0.000733346914, 'max_per_patch' => 2, 'articles_count' => 0),
  717 => array('title_id' => 717, 'share_percent' => 0.0005296394381, 'max_per_patch' => 2, 'articles_count' => 0),
  729 => array('title_id' => 729, 'share_percent' => 0.003340802608, 'max_per_patch' => 2, 'articles_count' => 0),
  76 => array('title_id' => 76, 'share_percent' => 0.0001018537383, 'max_per_patch' => 2, 'articles_count' => 0),
  1086 => array('title_id' => 1086, 'share_percent' => 0.001222244857, 'max_per_patch' => 2, 'articles_count' => 0),
  407 => array('title_id' => 407, 'share_percent' => 0.00004074149542, 'max_per_patch' => 2, 'articles_count' => 0),
  851 => array('title_id' => 851, 'share_percent' => 0.0003463027096, 'max_per_patch' => 2, 'articles_count' => 0),
  419 => array('title_id' => 419, 'share_percent' => 0.0001833367285, 'max_per_patch' => 2, 'articles_count' => 0),
 *  */
$title_map = array(
    402 => array('title_id' => 402, 'share_percent' => 0.0009981666327, 'max_per_patch' => 2, 'articles_count' => 0),
    366 => array('title_id' => 366, 'share_percent' => 0.0009777958853, 'max_per_patch' => 2, 'articles_count' => 0),
    1366 => array('title_id' => 1366, 'share_percent' => 0.0007740884094, 'max_per_patch' => 2, 'articles_count' => 0),
    338 => array('title_id' => 338, 'share_percent' => 0.0006518639237, 'max_per_patch' => 2, 'articles_count' => 0),
    1054 => array('title_id' => 1054, 'share_percent' => 0.0006518639237, 'max_per_patch' => 2, 'articles_count' => 0),
    7 => array('title_id' => 7, 'share_percent' => 0.0005703809329, 'max_per_patch' => 2, 'articles_count' => 0),
    1084 => array('title_id' => 1084, 'share_percent' => 0.0005500101855, 'max_per_patch' => 2, 'articles_count' => 0),
    //432 => array('title_id' => 432, 'share_percent' => 0.0004888979427, 'max_per_patch' => 2, 'articles_count' => 0),
    //825 => array('title_id' => 825, 'share_percent' => 0.0004074149524, 'max_per_patch' => 2, 'articles_count' => 0),
    853 => array('title_id' => 853, 'share_percent' => 0.0003870442044, 'max_per_patch' => 2, 'articles_count' => 0),
    880 => array('title_id' => 880, 'share_percent' => 0.000366673457, 'max_per_patch' => 2, 'articles_count' => 0),
    1075 => array('title_id' => 1075, 'share_percent' => 0.0003259319616, 'max_per_patch' => 2, 'articles_count' => 0),
    230 => array('title_id' => 230, 'share_percent' => 0.0003055612142, 'max_per_patch' => 2, 'articles_count' => 0),
    1446 => array('title_id' => 1446, 'share_percent' => 0.0003055612142, 'max_per_patch' => 2, 'articles_count' => 0),
    //919 => array('title_id' => 919, 'share_percent' => 0.0002648197187, 'max_per_patch' => 2, 'articles_count' => 0),
    496 => array('title_id' => 496, 'share_percent' => 0.0002037074759, 'max_per_patch' => 2, 'articles_count' => 0),
    780 => array('title_id' => 780, 'share_percent' => 0.0002037074759, 'max_per_patch' => 2, 'articles_count' => 0),
    784 => array('title_id' => 784, 'share_percent' => 0.0002037074759, 'max_per_patch' => 2, 'articles_count' => 0),
    1048 => array('title_id' => 1048, 'share_percent' => 0.0001629659811, 'max_per_patch' => 2, 'articles_count' => 0),
    602 => array('title_id' => 602, 'share_percent' => 0.0001425952331, 'max_per_patch' => 2, 'articles_count' => 0),
    1152 => array('title_id' => 1152, 'share_percent' => 0.0001425952331, 'max_per_patch' => 2, 'articles_count' => 0),
    401 => array('title_id' => 401, 'share_percent' => 0.00008148299024, 'max_per_patch' => 2, 'articles_count' => 0),
    233 => array('title_id' => 233, 'share_percent' => 0.00006111224283, 'max_per_patch' => 2, 'articles_count' => 0),
    461 => array('title_id' => 461, 'share_percent' => 0.00006111224283, 'max_per_patch' => 2, 'articles_count' => 0),
    163 => array('title_id' => 163, 'share_percent' => 0.00004074149542, 'max_per_patch' => 2, 'articles_count' => 0),
    //4 => array('title_id' => 4, 'share_percent' => 0.00002037074741, 'max_per_patch' => 2, 'articles_count' => 0),
    701 => array('title_id' => 701, 'share_percent' => 0.01008352007, 'max_per_patch' => 2, 'articles_count' => 0),
    391 => array('title_id' => 391, 'share_percent' => 0.009839071096, 'max_per_patch' => 2, 'articles_count' => 0),
    386 => array('title_id' => 386, 'share_percent' => 0.009798329599, 'max_per_patch' => 2, 'articles_count' => 0),
    159 => array('title_id' => 159, 'share_percent' => 0.009350173153, 'max_per_patch' => 2, 'articles_count' => 0),
    185 => array('title_id' => 185, 'share_percent' => 0.00776125484, 'max_per_patch' => 2, 'articles_count' => 0),
    598 => array('title_id' => 598, 'share_percent' => 0.007679771851, 'max_per_patch' => 2, 'articles_count' => 0),
    1442 => array('title_id' => 1442, 'share_percent' => 0.007557547366, 'max_per_patch' => 2, 'articles_count' => 0),
    62 => array('title_id' => 62, 'share_percent' => 0.007251986151, 'max_per_patch' => 2, 'articles_count' => 0),
    200 => array('title_id' => 200, 'share_percent' => 0.006701975966, 'max_per_patch' => 2, 'articles_count' => 0),
    //1026 => array('title_id' => 1026, 'share_percent' => 0.00647789774, 'max_per_patch' => 2, 'articles_count' => 0),
    393 => array('title_id' => 393, 'share_percent' => 0.006192707272, 'max_per_patch' => 2, 'articles_count' => 0),
    398 => array('title_id' => 398, 'share_percent' => 0.006070482788, 'max_per_patch' => 2, 'articles_count' => 0),
    //10 => array('title_id' => 10, 'share_percent' => 0.00554084335, 'max_per_patch' => 2, 'articles_count' => 0),
    164 => array('title_id' => 164, 'share_percent' => 0.005520472602, 'max_per_patch' => 2, 'articles_count' => 0),
    789 => array('title_id' => 789, 'share_percent' => 0.005337135874, 'max_per_patch' => 2, 'articles_count' => 0),
    389 => array('title_id' => 389, 'share_percent' => 0.005072316155, 'max_per_patch' => 2, 'articles_count' => 0),
    //36 => array('title_id' => 36, 'share_percent' => 0.005051945408, 'max_per_patch' => 2, 'articles_count' => 0),
    //247 => array('title_id' => 247, 'share_percent' => 0.004950091669, 'max_per_patch' => 2, 'articles_count' => 0),
    //176 => array('title_id' => 176, 'share_percent' => 0.004766754941, 'max_per_patch' => 2, 'articles_count' => 0),
    713 => array('title_id' => 713, 'share_percent' => 0.003890812794, 'max_per_patch' => 2, 'articles_count' => 0),
    172 => array('title_id' => 172, 'share_percent' => 0.003768588308, 'max_per_patch' => 2, 'articles_count' => 0),
    1 => array('title_id' => 1, 'share_percent' => 0.003646363822, 'max_per_patch' => 2, 'articles_count' => 0),
    888 => array('title_id' => 888, 'share_percent' => 0.003605622327, 'max_per_patch' => 2, 'articles_count' => 0),
    788 => array('title_id' => 788, 'share_percent' => 0.003381544103, 'max_per_patch' => 2, 'articles_count' => 0),
    13 => array('title_id' => 13, 'share_percent' => 0.003300061113, 'max_per_patch' => 2, 'articles_count' => 0),
    1387 => array('title_id' => 1387, 'share_percent' => 0.003259319617, 'max_per_patch' => 2, 'articles_count' => 0),
    425 => array('title_id' => 425, 'share_percent' => 0.003116724384, 'max_per_patch' => 2, 'articles_count' => 0),
    //37 => array('title_id' => 37, 'share_percent' => 0.00272968018, 'max_per_patch' => 2, 'articles_count' => 0),
    781 => array('title_id' => 781, 'share_percent' => 0.002688938685, 'max_per_patch' => 2, 'articles_count' => 0),
    25 => array('title_id' => 25, 'share_percent' => 0.002607455694, 'max_per_patch' => 2, 'articles_count' => 0),
    600 => array('title_id' => 600, 'share_percent' => 0.001975962518, 'max_per_patch' => 2, 'articles_count' => 0),
    889 => array('title_id' => 889, 'share_percent' => 0.001690772052, 'max_per_patch' => 2, 'articles_count' => 0),
    348 => array('title_id' => 348, 'share_percent' => 0.001670401304, 'max_per_patch' => 2, 'articles_count' => 0),
    1223 => array('title_id' => 1223, 'share_percent' => 0.001609289061, 'max_per_patch' => 2, 'articles_count' => 0),
    217 => array('title_id' => 217, 'share_percent' => 0.001466693828, 'max_per_patch' => 2, 'articles_count' => 0),
    1244 => array('title_id' => 1244, 'share_percent' => 0.001466693828, 'max_per_patch' => 2, 'articles_count' => 0),
    35 => array('title_id' => 35, 'share_percent' => 0.001018537381, 'max_per_patch' => 2, 'articles_count' => 0),
    1390 => array('title_id' => 1390, 'share_percent' => 0.02878386637, 'max_per_patch' => 2, 'articles_count' => 0),
    365 => array('title_id' => 365, 'share_percent' => 0.02833570993, 'max_per_patch' => 2, 'articles_count' => 0),
    9 => array('title_id' => 9, 'share_percent' => 0.02735791404, 'max_per_patch' => 2, 'articles_count' => 0),
    21 => array('title_id' => 21, 'share_percent' => 0.02654308413, 'max_per_patch' => 2, 'articles_count' => 0),
    //70 => array('title_id' => 70, 'share_percent' => 0.02495416582, 'max_per_patch' => 2, 'articles_count' => 0),
    44 => array('title_id' => 44, 'share_percent' => 0.02479119984, 'max_per_patch' => 2, 'articles_count' => 0),
    387 => array('title_id' => 387, 'share_percent' => 0.02446526788, 'max_per_patch' => 2, 'articles_count' => 0),
    1609 => array('title_id' => 1609, 'share_percent' => 0.02110409452, 'max_per_patch' => 2, 'articles_count' => 0),
    392 => array('title_id' => 392, 'share_percent' => 0.01939295172, 'max_per_patch' => 2, 'articles_count' => 0),
    11 => array('title_id' => 11, 'share_percent' => 0.01599103688, 'max_per_patch' => 2, 'articles_count' => 0),
    23 => array('title_id' => 23, 'share_percent' => 0.01574658791, 'max_per_patch' => 2, 'articles_count' => 0),
    787 => array('title_id' => 787, 'share_percent' => 0.01377062538, 'max_per_patch' => 2, 'articles_count' => 0),
    775 => array('title_id' => 775, 'share_percent' => 0.01315950295, 'max_per_patch' => 2, 'articles_count' => 0),
    878 => array('title_id' => 878, 'share_percent' => 0.01273171725, 'max_per_patch' => 2, 'articles_count' => 0),
    183 => array('title_id' => 183, 'share_percent' => 0.0123650438, 'max_per_patch' => 2, 'articles_count' => 0),
    399 => array('title_id' => 399, 'share_percent' => 0.01189651661, 'max_per_patch' => 2, 'articles_count' => 0),
    369 => array('title_id' => 369, 'share_percent' => 0.0115502139, 'max_per_patch' => 2, 'articles_count' => 0),
    240 => array('title_id' => 240, 'share_percent' => 0.01126502343, 'max_per_patch' => 2, 'articles_count' => 0),
    1386 => array('title_id' => 1386, 'share_percent' => 0.01126502343, 'max_per_patch' => 2, 'articles_count' => 0),
    1436 => array('title_id' => 1436, 'share_percent' => 0.01106131595, 'max_per_patch' => 2, 'articles_count' => 0),
    364 => array('title_id' => 364, 'share_percent' => 0.01034833978, 'max_per_patch' => 2, 'articles_count' => 0),
    1310 => array('title_id' => 1310, 'share_percent' => 0.03984518233, 'max_per_patch' => 2, 'articles_count' => 0),
    1388 => array('title_id' => 1388, 'share_percent' => 0.03758402934, 'max_per_patch' => 2, 'articles_count' => 0),
    243 => array('title_id' => 243, 'share_percent' => 0.0332858016, 'max_per_patch' => 2, 'articles_count' => 0),
    367 => array('title_id' => 367, 'share_percent' => 0.03253208393, 'max_per_patch' => 2, 'articles_count' => 0),
    1007 => array('title_id' => 1007, 'share_percent' => 0.02992462824, 'max_per_patch' => 2, 'articles_count' => 0),
    368 => array('title_id' => 368, 'share_percent' => 0.02906905684, 'max_per_patch' => 2, 'articles_count' => 0),
    //223 => array('title_id' => 223, 'share_percent' => 0.04960277043, 'max_per_patch' => 2, 'articles_count' => 0),
    55 => array('title_id' => 55, 'share_percent' => 0.04210633531, 'max_per_patch' => 2, 'articles_count' => 0),
    //1492 => array('title_id' => 1492, 'share_percent' => 0.05764921574, 'max_per_patch' => 2, 'articles_count' => 0),
    363 => array('title_id' => 363, 'share_percent' => 0.05620289266, 'max_per_patch' => 2, 'articles_count' => 0),
);
$max_report_id = mysql_query("select max(report_id) from article_jumper where title_id=$title_id");
$max_report_id = mysql_fetch_row($max_report_id);
$max_report_id = $max_report_id[0];
if (is_null($max_report_id)) {
  $max_report_id = mysql_query("select max(id) from report_parse where title_id=$title_id and id not in (select max(id) from report_parse where title_id=$title_id)");
  $max_report_id = mysql_fetch_row($max_report_id);
  $max_report_id = $max_report_id[0];
}

$report_parse = mysql_query("select id,article_num,articles_ids,title_id from report_parse where title_id=$title_id and id>$max_report_id order by id");
$article_ids = array();
while ($row = mysql_fetch_assoc($report_parse)) {
  if ($row['article_num'] > 0) {
    $article_ids = explode(',', $row['articles_ids']);
    $title_id = $row['title_id'];
    $max_report_id = $row['id'];
    $next_mapped_title = reset($title_map);
    foreach ($article_ids as $article) {
      if ($next_mapped_title['articles_count'] >= floor(count($article_ids) * $next_mapped_title['share_percent'])) {
        $next_mapped_title = next($title_map);
      }
      insert_article($article, $title_id, $max_report_id, $next_mapped_title['title_id']);
      $next_mapped_title['articles_count'] = $next_mapped_title['articles_count'] + 1;
    }
  }
}
checkTitles();

function insert_article($article_id, $title_id, $max_report_id, $mapped_title_id) {
  global $title_map;
  $mapped_date = date('Y-m-d H:i:s');
  $q = mysql_query("insert into article_jumper (article_id,title_id,report_id,mapped_date,mapped_title) values ($article_id,$title_id,$max_report_id,'$mapped_date',$mapped_title_id)");
}

function checkTitles() {
  $mailTo = array('khaled@syndigate.info', 'eyad@syndigate.info', 'majdi@syndigate.info');

  //$mailTo = array('eyad@syndigate.info');
  $titles_id = "1,7,9,11,13,21,23,25,35,44,55,62,159,163,164,172,183,185,200,217,230,233,"
          . "240,243,338,348,363,364,365,366,367,368,369,386,387,389,391,392,393,398,399,401,402,425,461,"
          . "496,598,600,602,701,713,775,780,781,784,787,788,789,853,878,880,888,889,1007,1048,1054,"
          . "1075,1084,1152,1223,1244,1310,1366,1386,1387,1388,1390,1436,1442,1446,1609";
  $query = mysql_query(" select id, name from title where id in ($titles_id) and is_active =0 order by id desc");
  $title = array();
  $i = 0;
  while ($row = mysql_fetch_row($query)) {
    $title[$i]['id'] = $row[0];
    $title[$i]['name'] = $row[1];
    $i++;
  }
  $count = 0;
  $count = count($title);
  if ($count > 0) {
    $table = "";
    $table .= "<table border='1'><tr><th>Title Id</th>";
    $table .= "<th>Title name</th></tr>";
    foreach ($title as $sell) {
      $table .= "<tr>";
      $table .="<td align='center'>{$sell["id"]}</td>";
      $table .="<td align='center'>{$sell["name"]}</td>";
      $table .= "</tr>";
    }
    $table .="</table>";
    $body = "<p>The below titles inactive titles please check it in map Tenders info To Factiva </p>";
    $body = $body . $table;
    $mail = new ezcMailComposer();
    $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply');
    foreach ($mailTo as $to) {
      $mail->addTo(new ezcMailAddress($to));
    }
    $mail->subject = "Inactive titles in map Tenders info To Factiva,";
    $mail->htmlText = $body;
    //$mail->plainText = $body;
    $mail->build();
    $transport = new ezcMailMtaTransport();
    $transport->send($mail);
  }
}

?>
