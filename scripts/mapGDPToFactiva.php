<?php

/* This script get last report articles from title ID 193 and add it to the article_jumper table. Should be run each 2 hours
 * create table article_jumperGDP (`id` int(255) NOT NULL AUTO_INCREMENT,article_id int(255),report_id int(255),title_id int(255),is_sent tinyint DEFAULT '0', mapped_title int(255), send_date datetime DEFAULT NULL, PRIMARY KEY (`id`),KEY `article_jumper_article_id` (`article_id`),KEY `article_jumper_report_id` (`report_id`), KEY `article_jumper_is_sent` (`is_sent`), KEY `article_jumper_send_date` (`send_date`)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
 * alter table article_jumperGDP add `mapped_date` datetime DEFAULT NULL;
 */
/*
  +----------+--------------------+
  | title_id | ROUND(count(*)/30) |
  +----------+--------------------+
  |     1258 |                 95 |
  |     1340 |                 38 |
  |     1606 |                157 |
  |     1630 |                166 |
  |     1631 |                101 |
  |     1632 |                151 |
  |     1633 |                 58 |
  |     1663 |                  7 |
  |     1788 |                 34 |
  |     1789 |                 53 |
  |     1790 |                 71 |
  |     1791 |                 45 |
  |     1844 |                 18 |
  |     1845 |                 14 |
  |     1846 |                 15 |
  |     1847 |                 16 |
  |     1848 |                 28 |
  |     1849 |                 22 |
  |     1850 |                 28 |
  |     1851 |                 10 |
  |     1852 |                 24 |
  |     2053 |                 35 |
  |     2324 |                 14 |
  |     2657 |                 35 |
  +----------+--------------------+
 *  */
require_once '/usr/local/syndigate/scripts/propel_include_config.php';

// $title_map = array(TITLE_ID=>PATCHES,TITLE_ID=>PATCHES);
$percent = 1;
$title_map = array(
    1258 => array(
        //5
        array('mapped_title_id' => 713, 'share_percent' => 0.2, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 366, 'share_percent' => 0.2, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 392, 'share_percent' => 0.2, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 894, 'share_percent' => 0.2, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1048, 'share_percent' => 0.2, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1340 => array(
        //4
        array('mapped_title_id' => 386, 'share_percent' => 0.25, 'max_per_patch' => 20, 'articles_count' => 0),
        //array('mapped_title_id' => 1609, 'share_percent' => 0.2, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 598, 'share_percent' => 0.25, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 9, 'share_percent' => 0.25, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1143, 'share_percent' => 0.25, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1606 => array(
        //13
        array('mapped_title_id' => 167, 'share_percent' => 0.0769, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 896, 'share_percent' => 0.0769, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 897, 'share_percent' => 0.0769, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 425, 'share_percent' => 0.0769, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 2724, 'share_percent' => 0.0769, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 387, 'share_percent' => 0.0769, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 775, 'share_percent' => 0.0769, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1800, 'share_percent' => 0.0769, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 787, 'share_percent' => 0.0769, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1244, 'share_percent' => 0.0769, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 903, 'share_percent' => 0.0769, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 780, 'share_percent' => 0.0769, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 898, 'share_percent' => 0.0769, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1630 => array(
        //13
        array('mapped_title_id' => 1803, 'share_percent' => 0.076, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 11, 'share_percent' => 0.076, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 21, 'share_percent' => 0.076, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 902, 'share_percent' => 0.076, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 899, 'share_percent' => 0.076, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1386, 'share_percent' => 0.076, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 55, 'share_percent' => 0.076, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1387, 'share_percent' => 0.076, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1633, 'share_percent' => 0.076, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 363, 'share_percent' => 0.076, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1388, 'share_percent' => 0.076, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 496, 'share_percent' => 0.076, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 5, 'share_percent' => 0.076, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1631 => array(
        //10
        //array('mapped_title_id' => 781, 'share_percent' => 0.1, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 786, 'share_percent' => 0.11, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 183, 'share_percent' => 0.11, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 2732, 'share_percent' => 0.11, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 900, 'share_percent' => 0.11, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1007, 'share_percent' => 0.11, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1056, 'share_percent' => 0.11, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 398, 'share_percent' => 0.11, 'max_per_patch' => 20, 'articles_count' => 0),
        //array('mapped_title_id' => 402, 'share_percent' => 0.1, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1674, 'share_percent' => 0.11, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 904, 'share_percent' => 0.11, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1632 => array(
        //11
        array('mapped_title_id' => 364, 'share_percent' => 0.1, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 230, 'share_percent' => 0.1, 'max_per_patch' => 20, 'articles_count' => 0),
        //array('mapped_title_id' => 44, 'share_percent' => 0.0769, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 365, 'share_percent' => 0.1, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 185, 'share_percent' => 0.1, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 401, 'share_percent' => 0.1, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1505, 'share_percent' => 0.1, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 399, 'share_percent' => 0.1, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1005, 'share_percent' => 0.1, 'max_per_patch' => 20, 'articles_count' => 0),
        //array('mapped_title_id' => 2745, 'share_percent' => 0.09, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1058, 'share_percent' => 0.1, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1808, 'share_percent' => 0.1, 'max_per_patch' => 20, 'articles_count' => 0),
        //array('mapped_title_id' => 243, 'share_percent' => 0.0769, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1633 => array(
        //6
        array('mapped_title_id' => 367, 'share_percent' => 0.16, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1310, 'share_percent' => 0.16, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 163, 'share_percent' => 0.16, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 389, 'share_percent' => 0.16, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 200, 'share_percent' => 0.16, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 164, 'share_percent' => 0.16, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1663 => array(
        //2
        array('mapped_title_id' => 174, 'share_percent' => 0.5, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 2747, 'share_percent' => 0.5, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1788 => array(
        //2
        array('mapped_title_id' => 2746, 'share_percent' => 0.5, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 701, 'share_percent' => 0.5, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1789 => array(
        //4
        array('mapped_title_id' => 25, 'share_percent' => 0.25, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 368, 'share_percent' => 0.25, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 2749, 'share_percent' => 0.25, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1349, 'share_percent' => 0.25, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1790 => array(
        //6
        array('mapped_title_id' => 793, 'share_percent' => 0.16, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 391, 'share_percent' => 0.16, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 159, 'share_percent' => 0.16, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 369, 'share_percent' => 0.16, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 814, 'share_percent' => 0.16, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1312, 'share_percent' => 0.16, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1791 => array(
        //5
        array('mapped_title_id' => 2754, 'share_percent' => 0.2, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 23, 'share_percent' => 0.2, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 2756, 'share_percent' => 0.2, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 2757, 'share_percent' => 0.2, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1801, 'share_percent' => 0.2, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1844 => array(
        //3
        array('mapped_title_id' => 2758, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 62, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 2759, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1845 => array(
        //3
        array('mapped_title_id' => 2760, 'share_percent' => 0.5, 'max_per_patch' => 20, 'articles_count' => 0),
        // array('mapped_title_id' => 818, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1442, 'share_percent' => 0.5, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1846 => array(
        //3
        array('mapped_title_id' => 1217, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 2761, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1811, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1847 => array(
        //3
        array('mapped_title_id' => 35, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1059, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 2762, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1848 => array(
        //3
        array('mapped_title_id' => 2764, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 2765, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1526, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1849 => array(
        //3
        array('mapped_title_id' => 1228, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 2766, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1673, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1850 => array(
        //2
        //array('mapped_title_id' => 4, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1286, 'share_percent' => 0.5, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 2767, 'share_percent' => 0.5, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1851 => array(
        //1
        array('mapped_title_id' => 89, 'share_percent' => 1, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    1852 => array(
        //3
        array('mapped_title_id' => 2768, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1809, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 6, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    2053 => array(
        //3
        array('mapped_title_id' => 788, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 789, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 901, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    2324 => array(
        //3
        array('mapped_title_id' => 217, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 2769, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 1436, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
    2657 => array(
        //5 ?? 3
        array('mapped_title_id' => 1084, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 393, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
        array('mapped_title_id' => 2771, 'share_percent' => 0.33, 'max_per_patch' => 20, 'articles_count' => 0),
    ),
);
foreach ($title_map as $title_id => $maped_titles) {
  $max_report_id = mysql_query("select max(report_id) from article_jumperGDP where title_id=$title_id ");
  $max_report_id = mysql_fetch_row($max_report_id);
  $max_report_id = $max_report_id[0];
  if (is_null($max_report_id)) {
    $max_report_id = mysql_query("select max(id) from report_parse where title_id=$title_id and article_num> 0 and id not in (select max(id) from report_parse where title_id=$title_id and article_num> 0)");
    $max_report_id = mysql_fetch_row($max_report_id);
    $max_report_id = $max_report_id[0];
  }
  $report_parse = mysql_query("select id,article_num,articles_ids,title_id from report_parse where title_id=$title_id and id>$max_report_id order by id");
  $article_ids = array();
  while ($row = mysql_fetch_assoc($report_parse)) {
    if ($row['article_num'] > 0) {
      $article_ids = explode(',', $row['articles_ids']);
      $title_id = $row['title_id'];
      $max_report_id = $row['id'];
      $next_mapped_title = reset($maped_titles);

      $article_percent_count = floor($row['article_num'] * $percent) < 1 ? 1 : floor($row['article_num'] * $percent);
      $new_article_ids = array_slice($article_ids, 0, $article_percent_count);
      foreach ($new_article_ids as $article) {
        if ($next_mapped_title['articles_count'] >= floor(count($new_article_ids) * $next_mapped_title['share_percent'])) {
          $next_mapped_title = next($maped_titles);
        }
        insert_article($article, $title_id, $max_report_id, $next_mapped_title['mapped_title_id']);
        $next_mapped_title['articles_count'] = $next_mapped_title['articles_count'] + 1;
      }
    }
  }
}
checkTitles();

function insert_article($article_id, $title_id, $max_report_id, $mapped_title_id) {
  $mapped_date = date('Y-m-d H:i:s');
  $max_id = mysql_query("select * from article_jumperGDP where article_id=$article_id order by id desc limit 1");
  $max_id = mysql_fetch_assoc($max_id);
  if (false == $max_id) {
    mysql_query("insert into article_jumperGDP (article_id,title_id,report_id,mapped_date,mapped_title) values ($article_id,$title_id,$max_report_id,'$mapped_date',$mapped_title_id)");
  } else {
    echo 'Update article id ' . $article_id . chr(10);
    $mapped_title_id = $max_id['mapped_title'];
    mysql_query("insert into article_jumperGDP (article_id,title_id,report_id,mapped_date,mapped_title) values ($article_id,$title_id,$max_report_id,'$mapped_date',$mapped_title_id)");
  }
}

function checkTitles() {
  $mailTo = array('khaled@syndigate.info', 'eyad@syndigate.info', 'majdi@syndigate.info', 'rand@syndigate.info');

  //$mailTo = array('eyad@syndigate.info');
  $titles_id = "5,6,9,11,21,23,25,35,55,62,89,159,163,164,167,174,183,185,200,217,230,363,364,365,366,367,368,369,386,"
          . "387,389,391,392,393,398,399,401,425,496,598,701,713,775,780,786,787,788,789,793,814,894,896,897,898,"
          . "899,900,901,902,903,904,1005,1007,1048,1056,1058,1059,1084,1143,1217,1228,1244,1286,1310,1312,1349,"
          . "1386,1387,1388,1436,1442,1505,1526,1633,1673,1674,1800,1801,1803,1808,1809,1811,2724,2732,2746,2747,"
          . "2749,2754,2756,2757,2758,2759,2760,2761,2762,2764,2765,2766,2767,2768,2769,2771";
  $query = mysql_query(" select id, name from title where id in ($titles_id) and is_active =0 order by id desc");
  $title = array();
  $i = 0;
  while ($row = mysql_fetch_row($query)) {
    $title[$i]['id'] = $row[0];
    $title[$i]['name'] = $row[1];
    $i++;
  }
  $count = 0;
  $count = count($title);
  if ($count > 0) {
    $table = "";
    $table .= "<table border='1'><tr><th>Title Id</th>";
    $table .= "<th>Title name</th></tr>";
    foreach ($title as $sell) {
      $table .= "<tr>";
      $table .="<td align='center'>{$sell["id"]}</td>";
      $table .="<td align='center'>{$sell["name"]}</td>";
      $table .= "</tr>";
    }
    $table .="</table>";
    $body = "<p>The below titles inactive titles please check it in map GDP To Factiva </p>";
    $body = $body . $table;

    $mail = new ezcMailComposer();
    $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply');
    foreach ($mailTo as $to) {
      $mail->addTo(new ezcMailAddress($to));
    }
    $mail->subject = "Inactive titles in map GDP To Factiva,";
    $mail->htmlText = $body;
    //$mail->plainText = $body;
    $mail->build();
    $transport = new ezcMailMtaTransport();
    $transport->send($mail);
  }
}

?>
