<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

$titles	    = array ( 1345, 1346 );
$titlesFile = array(1345 => ' ;ls -laht', '1346' => ' ;ls -laht');

$log_stream   = '/var/log/LFTP' . date('/Y/m/d/');
$tmp_log_stem = '/tmp/LFTP/';

if(!is_dir($log_stream)) {
	mkdir($log_stream, 0777, true);				
}	
if(!is_dir($tmp_log_stem)) {
        mkdir($tmp_log_stem, 0777, true);                         
}

ini_set('display_errors', 1);
error_reporting(E_ALL);

foreach ( $titles as $title  ) {
	$c = new Criteria();
	$c->add(TitlePeer::IS_ACTIVE, 1);
	$c->add(TitlePeer::ID, $title);
	$sourceTitle = TitlePeer::doSelectOne($c);

	$clients     = TitlesClientsRulesPeer::getTitleClients($title);
	
	foreach ( $clients as $client ) {
		$client	     = ClientPeer::retrieveByPK( $client->getID() );
		$connections = $client->getClientConnections();

		foreach ( $connections as $connection ) {
			$remotDir = $connection->getRemoteDir();
			if ( empty($remotDir)  ) {
				$remotDir = '/';
			}
			
			$logStream 	= $log_stream 	. $title . '.log';
			$tmpLogStream 	= $tmp_log_stem . $title . '.log';
			//$command = 'lftp -e "mirror -R --only-newer '.$sourceTitle->getHomeDir().' '.$remotDir.';ls -l Aaj\ Tak\ Political\ News/ ;quit" -u '.$connection->getUsername().','.$connection->getPassword().' '.$connection->getUrl().' 1> ' . $logStream;

			//$command = 'lftp -e "mirror -R --only-newer '.$sourceTitle->getHomeDir().' '.$remotDir.';ls -l '.$titlesFile[$title].' ;quit" -u '.$connection->getUsername().','.$connection->getPassword().' '.$connection->getUrl().' 1> ' . $tmpLogStream;

			$command = 'lftp -e "mirror -R --only-newer '.$sourceTitle->getHomeDir().' '.$remotDir. $titlesFile[$title] .';quit" -u '.$connection->getUsername().','.$connection->getPassword().' '.$connection->getUrl().' 1> ' . $tmpLogStream;
		
			echo PHP_EOL . $command . PHP_EOL;
			
			shell_exec( $command );
			
			$currentLog = file_get_contents($tmpLogStream);
			$currentLog = 'This Log for Title ' . $title . ' and For client ' . $client->getID() . PHP_EOL . $currentLog . PHP_EOL . PHP_EOL;
			file_put_contents($logStream, $currentLog, FILE_APPEND );
		}

	}
	
	
	//$command = "lftp -e 'mirror -R --only-newer /syndigate/sources/home/91/1345/ /;quit' -u ftp2,JustForTest 205.234.150.179 | tee $lfOutfile ' 1>&2'";
}
