<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

$q = mysql_query("select a.title_id,t.publisher_id,i.original_name,i.id from article a inner join image i on i.article_id=a.id "
        . "inner join title t on a.title_id=t.id where t.id in (56,392,395,867,1081) and img_name=''");

while ($row = mysql_fetch_assoc($q)) {

  $filename = $row["original_name"];
  $img = basename($row["original_name"]);
  $titleId = $row["title_id"];
  $pubId = $row["publisher_id"];
  $path = dirname($row["original_name"]);

  if (file_exists($filename)) {
    echo "The file $filename exists" . PHP_EOL;

    $name = getImgReplacement($img, $path, $titleId, $pubId);

    $img_name = str_replace(IMGS_PATH, IMGS_HOST, $name);
    $qu = "update image set img_name='$img_name'  where id={$row['id']} ;";
    if (mysql_query($qu)) {
      echo 'Update image name' . PHP_EOL;
    } else {
      echo 'Something error on image id ' . $row['id'] . PHP_EOL;
    }
  } else {
    echo "The file $filename does not exist". PHP_EOL;
  }
}

function getImgReplacement($img, $path, $titleId, $pubId) {
  $storingDir = "$pubId/$titleId/" . ((abs(crc32($img)) % 100) + 1) . '/';
  if (!is_dir(IMGS_PATH . $storingDir)) {
    mkdir(IMGS_PATH . $storingDir, 0777, true);
  }
  $sourcePath = "$path/$img";
  $pathInfo = pathinfo($sourcePath);

  do {
    $destPath = IMGS_PATH . $storingDir . getUniqueName();
    if (isset($pathInfo['extension'])) {
      $destPath .= '.' . $pathInfo['extension'];
    }
  } while (file_exists($destPath));

  if (copy($sourcePath, $destPath)) {
    return IMGS_PATH . $storingDir . basename($destPath);
  }
  return '';
}

function getUniqueName() {
  return microtime(1) * 100;
}
