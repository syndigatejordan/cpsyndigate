#!/bin/bash -x

# Syndigation System Paths definition
SYND_PATH='/usr/local/syndigate'
SYNDWEB_PATH='/var/www/syndigate_web'

# System commands definition
LN='/bin/ln'
MKDIR='/bin/mkdir'
CHOWN='/bin/chown'
CHMOD='/bin/chmod'
USERMOD='/usr/sbin/usermod'
USERDEL='/usr/sbin/userdel'
GROUPDEL='/usr/sbin/groupdel'
RM='/bin/rm'
MV='/bin/mv'
CP='/bin/cp'
GROUPMOD='/usr/sbin/groupmod'
ECHO='/bin/echo'

# Create shepherd Supervise files
$MKDIR -p /var/shepherd
$MKDIR -p /var/log/syndigate/shepherd/stdout/

$LN -s /var/log/syndigate/shepherd/stdout/ /var/shepherd/log
$ECHO "#!/bin/sh" >> /var/shepherd/run
$ECHO "exec /usr/local/syndigate/shepherd/shepherd.php --config=/usr/local/syndigate/conf/shepherd.conf.php" >> /var/shepherd/run
$CHMOD 755 /var/shepherd
$CHMOD 755 /var/shepherd/run

$ECHO "#!/bin/sh" >> /var/log/syndigate/shepherd/stdout/run
$ECHO "exec multilog t s10000000 ./main" >> /var/log/syndigate/shepherd/stdout/run
$LN -s /var/shepherd /service/shepherd
$CHMOD 755 /var/log/syndigate/shepherd/stdout
$CHMOD 755 /var/log/syndigate/shepherd/stdout/run