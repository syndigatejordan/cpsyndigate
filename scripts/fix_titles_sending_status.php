<?php
require_once '/usr/local/syndigate/scripts/propel_include_config.php';


// Check if an instance exists from the same scripts 
$avilable_instances = 0;
$output = shell_exec('ps aux | grep fix_titles_sending_status.php | grep -v "/bin/sh -c" | grep -v "sh -c" | grep -v "grep" ');
$output = explode(PHP_EOL, $output);
foreach ($output as $o) {
    if($o) {
       $avilable_instances++; 
    }
}

if($avilable_instances > 1) {    
    die();
}


//Get active titles 
$c = new Criteria();
$c->add(TitlePeer::IS_ACTIVE, 1);
$c->add(TitlePeer::IS_RECEIVING,1);
$activeTitles = TitlePeer::doSelect($c);


foreach ($activeTitles as $title) {
	echo "proessing title " . $title->getId() . "\r";
	
	$spanCount = $title->getArticlesSpanCount(-1, 0);
	$comment;
	
	$msg = "[ Cronjob Comment : Not receiving ]";
	$old = str_replace($msg, '',$title->getReceivingStatusComment());
	
	if($spanCount==0) {
		
		$title->setReceivingStatus(1); // Set to Error receiving
		$title->setReceivingStatusTimestamp(time());
		
		if(!trim($old)) {
			$comment = $msg;
		} else {
			$comment = $msg . "  " . $old;
		}
		
	} else {
		if(!trim($old)) {
			//remove the error state
			$comment ='';
			$title->setReceivingStatus(0); //remove the errror
		} else {
			//restor the previos error state
			$comment = $old;
			$title->setReceivingStatus(1); //set an error
		}
		
	}
	
	$title->setReceivingStatusComment($comment);
	$title->save();
}

echo "done \n";
