<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
//require_once '/home/yousef/syndigate/scripts/propel_include_config.php';

$mailTo = array('hani@corp.albawaba.com', 'krishna@tendersinfo.com', 'chirag.t@tendersinfo.com', 'khaled@syndigate.info', 'majdi@syndigate.info', 'eyad@syndigate.info');
#$mailTo = array('eyad@syndigate.info , khaled@syndigate.info');
//-- Main --------------------------------------------------------------------------------------
$fileSources = array('Albawaba News' => 812, 'Albawaba Projects' => 811, 'Albawaba Contract Award' => 813, 'Albawaba Tenders' => 81);

$date = '';
if ($argv[1]) {
  $date = $argv[1];
} else {
  $date = date('Y-m-d', strtotime('-1 day'));
}

$sendEmail = true;
if ($argv[2] && $argv[2] == 'view') {
  $sendEmail = false;
}
$info = getCountInfo($fileSources, $date);
$countInfo = $info['info'];
$result = mysql_query("SELECT COUNT(*) AS cnt FROM article WHERE title_id = 193 AND date = '$date'");
$row = mysql_fetch_assoc($result);
$msg = "\n\nAlbawaba Mena : " . $row['cnt'];
$count = intval($row['cnt']);
$msg.= "\nTenders Total : " . $count;


$countInfo = "Tenders info Articles count for date $date , (date is the date tag in sent files, not the date they being sent in ) \n\n\n" . $countInfo;
$countInfo .= $msg . "\n";

if ($sendEmail) {
  sendEmail($countInfo, $mailTo);
  echo "An email with the report has been sent " . PHP_EOL . PHP_EOL;
} else {
  echo "No email will be sent .... Just for view infomation" . PHP_EOL . PHP_EOL;
}
echo $countInfo . PHP_EOL;

function getCountInfo($values = array(), $date) {
  $count = 0;
  $info = "";
  foreach ($values as $name => $titleId) {
    $result = mysql_query("SELECT COUNT(*) as cnt FROM article INNER JOIN article_original_data ON article.article_original_data_id = article_original_data.id WHERE article.title_id = $titleId  AND article.date = '$date'");
    $row = mysql_fetch_assoc($result);
    $msg = "$name articles : " . $row['cnt'];
    $count +=intval($row['cnt']);
    $info .= $msg . "\n";
  }
  $contant['info'] = $info;
  $contant['count'] = $count;
  return $contant;
}

function sendEmail($body, $mailTo) {

  $mail = new ezcMailComposer();
  $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply');

  foreach ($mailTo as $to) {
    $mail->addTo(new ezcMailAddress($to));
  }

  $mail->subject = "Tenders info Daily report,";
  //$mail->htmlText  = $body;
  $mail->plainText = $body;

  $mail->build();
  $transport = new ezcMailMtaTransport();
  $transport->send($mail);
}
