<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

$mailTo = array( 'majdi@syndigate.info', 'eyad@syndigate.info','PCHANG@willowbridge.com');
//-- Main --------------------------------------------------------------------------------------
$fileSources = array(
    "TopicFeed: Coffee" => 11932
);

$date = '';
if (isset($argv[1])) {
    $date = $argv[1];
} else {
    $date = date('Y-m-d', strtotime('-1 day'));
}

$sendEmail = true;
if (isset($argv[2]) && $argv[2] == 'view') {
    $sendEmail = false;
}

$countInfo = getCountInfo($fileSources, $date);

$countInfo = "Hello, \n\n\nPlease find below the total story count from your selected Topic Feeds for the date $date. \n\n\n" . $countInfo;

if ($sendEmail) {
    sendEmail($countInfo, $mailTo);
    echo "An email with the report has been sent " . PHP_EOL . PHP_EOL;
} else {
    echo "No email will be sent .... Just for view infomation" . PHP_EOL . PHP_EOL;
}
echo $countInfo . PHP_EOL;

function getCountInfo($values = array(), $date)
{

    $info = "";
    foreach ($values as $name => $titleId) {
        $result = mysql_query("SELECT COUNT(*) as cnt FROM article INNER JOIN article_original_data ON article.article_original_data_id = article_original_data.id WHERE article.title_id = $titleId  AND article.date = '$date'");
        $row = mysql_fetch_assoc($result);

        $msg = "$name articles : " . $row['cnt'];
        $info .= $msg . "\n";
    }
    return $info;
}

function sendEmail($body, $mailTo)
{

    $mail = new ezcMailComposer();
    $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply');

    foreach ($mailTo as $to) {
        $mail->addTo(new ezcMailAddress($to));
    }

    $mail->subject = "Commodity story count – Willowbridge";
    $mail->plainText = $body;

    $mail->build();
    $transport = new ezcMailMtaTransport();
    $transport->send($mail);
}
