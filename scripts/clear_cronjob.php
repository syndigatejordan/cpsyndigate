<?php 
require_once '/usr/local/syndigate/scripts/propel_include_config.php';


//Delete all records that dose not have an exsit crontab id 
$crontabs   = GatherCrontabPeer::doSelect(new Criteria());
$crontabIds = array();
foreach ($crontabs as $crontab) {
	$crontabIds[] = $crontab->getId();
}

$c = new Criteria();
$c->add(GatherCronjobPeer::CRONTAB_ID , $crontabIds, Criteria::NOT_IN);
GatherCronjobPeer::doDelete($c);




//Delete expeired cronjob records for daily titles.
$c = new Criteria();
$c->add(TitlePeer::TITLE_FREQUENCY_ID ,1);
$dailyTitles = TitlePeer::doSelect($c);

$titlesIds = array();
foreach ($dailyTitles as $dt) {
	$titlesIds[] = $dt->getId();
}

$c = new Criteria();
$c->add(GatherCronjobPeer::START_TIMESTAMP, date('Y-m-d H:i:s', time() - 86400) , Criteria::LESS_THAN);
$c->addDescendingOrderByColumn(GatherCronjobPeer::ID);
GatherCronjobPeer::doDelete($c);





// Delete all finished records 
$c = new Criteria();
$c->add(GatherCronjobPeer::START_TIMESTAMP , '0000-00-00 00:00:00', Criteria::NOT_EQUAL);
$c->add(GatherCronjobPeer::END_TIMESTAMP   , '0000-00-00 00:00:00', Criteria::NOT_EQUAL); 
GatherCronjobPeer::doDelete($c);


echo "Done .................. \n\n\n";



