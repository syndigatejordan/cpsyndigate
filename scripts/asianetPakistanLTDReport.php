<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

//$mailTo = array('eyad@syndigate.info');
$mailTo = array('eyad@syndigate.info', 'majdi@syndigate.info', 'salmanshafi@asianetpakistan.net');
//-------------------------------------- Main--------------------------------------------------
$slave = connect_slave();
$date = date('Y-m-d', strtotime('-1 day'));
//$date='2014-10-27';
$title_id = getTitleId($slave);
$table = getCountInfo($title_id, $date, $slave);

$msg = "Asianet-Pakistan (Pvt) Ltd Articles count for date $date. , (Date is based on the Date tag in the XML file, not the date when they were sent/updated on the XML file) \n\n\n</br>";
sendEmail($msg, $mailTo, $table);

function getTitleId($slave) {
  $query = mysql_query("select id,name from title where publisher_id=83 and is_active=1;", $slave);
  $title_id = array();
  $i = 0;
  while ($row = mysql_fetch_row($query)) {
    $title_id[$i]['id'] = $row[0];
    $title_id[$i]['name'] = $row[1];
    $i++;
  }
  return $title_id;
}

function getCountInfo($title_id, $date, $slave) {
  $table = "";
  $table .= "<table border='1'>";
  $table .= "<tr><th>ID</th>";
  $table .= "<th>Name</th>";
  $table .= "<th>Count</th></tr>";
  foreach ($title_id as $id) {
    $table .= "<tr><td>{$id['id']}</td>";
    $table .= "<td>{$id['name']}</td>";
    $result = mysql_query("select count(*) cnt from article where title_id={$id['id']} and date='{$date}';", $slave);
    $row = mysql_fetch_assoc($result);
    $table .= "<td>{$row["cnt"]}</td></tr>";
  }
  $table .="</table>";
  return $table;
}

function sendEmail($body, $mailTo, $table) {
  $body = $body . $table;
  $body = wordwrap($body, 75, "\r\n");
  $mail = new ezcMailComposer();
  $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply');
  foreach ($mailTo as $to) {
    $mail->addTo(new ezcMailAddress($to));
  }
  $mail->subject = "Asianet-Pakistan (Pvt) Ltd Report,";
  $mail->htmlText = $body;
  //$mail->plainText = $body;
  $mail->build();
  $transport = new ezcMailMtaTransport();
  $transport->send($mail);
}

function connect_slave() {
  $conn = @mysql_connect("10.200.200.66", "syndigate", "bQAuA4DuhtX6xyaE");
  //$conn = @mysql_connect("192.168.1.200", "root", "root");
  if (!$conn) {
    die("Error connecting to syndigate : " . mysql_error());
  }
  mysql_select_db("syndigate", $conn);
  return $conn;
}
