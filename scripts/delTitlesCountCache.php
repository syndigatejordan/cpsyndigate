<?php


// Check if an instance exists from the same scripts 
$avilable_instances = 0;
$output = shell_exec('ps aux | grep delTitlesCountCache.php | grep -v "/bin/sh -c" | grep -v "sh -c" | grep -v "grep" ');
$output = explode(PHP_EOL, $output);
foreach ($output as $o) {
    if($o) {
       $avilable_instances++; 
    }
}

if($avilable_instances > 1) {    
    die();
}



require_once '/usr/local/syndigate/scripts/propel_include_config.php';


$hour = date('H');

$c = new Criteria();
$c->add(TitlePeer::IS_ACTIVE, 1);
//$c->addDescendingOrderByColumn(TitlePeer::ID);
$activeTitles = TitlePeer::doSelect($c);


foreach ($activeTitles as $title) {
	delCache($title);
/*
	
	if(1==$title->getTitleFrequencyId()) {
		delCache($title);
	} else {
		if(0==($hour % 6)) {
			delCache($title);
		}
	}
  */  
}


function delCache($title) {
	static $i;
	$i++;
    echo "{$title->getId()} starting title " . $title->getName() . "\n";
    $title->setCachedArticlesToday((int)$title->getArticlesToday());
    $title->setCachedCurrentSpan((int)$title->getArticlesSpanCount(0));
    $title->setCachedPrevSpan((int)$title->getArticlesSpanCount(-1, -1));
    $title->save();
}
echo "done  ........\n\n\n";
