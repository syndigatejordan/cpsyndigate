<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
//require_once '/home/yousef/syndigate/scripts/propel_include_config.php';

$mailTo = array( 'shanawaz@bidsinfo.com', 'shihab@bidsinfo.com', ' javed.akhtar@bidsinfo.com', 'khaled@syndigate.info', 'majdi@syndigate.info', 'eyad@syndigate.info');
////$mailTo = array('eyad@syndigate.info');
//-- Main --------------------------------------------------------------------------------------
$fileSources = array('Legal Monitor Worldwide' => 1258, 'Global Data Point' => 1340, 'Energy Monitor Worldwide' => 1606,
    'Financial Services Monitor Worldwide' => 1630, 'Real Estate Monitor Worldwide' => 1631, 'ICT Monitor Worldwide' => 1632,
    'Executive Appointments Worldwide' => 1633, 'Islamic Finance Monitor Worldwide' => 1663, 'Education Monitor Worldwide' => 1788,
    'Transportation Monitor Worldwide' => 1789, 'Pharma & Healthcare Monitor Worldwide' => 1790, 'Defence Monitor Worldwide' => 1791,
    'Agriculture Monitor Worldwide' => 1844, 'Automotive Monitor Worldwide' => 1845, 'Basic Materials & Resources Monitor Worldwide' => 1846,
    'Advertising, Marketing & Public Relations Monitor Worldwide' => 1847, 'Consumer Goods Monitor Worldwide' => 1848,
    'Industrial Goods Monitor Worldwide' => 1849, 'Leisure & Hospitality Monitor Worldwide' => 1850, 'Media & Entertainment Monitor Worldwide' => 1851,
    'Utilities Monitor Worldwide' => 1852, 'Governance, Risk & Compliance Monitor Worldwide' => 2053, 'Chemicals Monitor Worldwide' => 2324,
    'Cyber Security Monitor Worldwide' => 2657, 'Consultancy Monitor Worldwide' => 5867, 'Engineering Monitor Worldwide' => 5868,
    'Environment Monitor Worldwide' => 5869);

$date = '';
if ($argv[1]) {
  $date = $argv[1];
} else {
  $date = date('Y-m-d', strtotime('-1 day'));
}

$sendEmail = true;
if ($argv[2] && $argv[2] == 'view') {
  $sendEmail = false;
}

$countInfo = getCountInfo($fileSources, $date);

$countInfo = "Global Data Point Articles count for date $date. , (Date is based on the date tag in the XML file, not the date when they were sent/updated on the XML file) \n\n\n" . $countInfo;
$countInfo .= $msg . "\n";

if ($sendEmail) {
  sendEmail($countInfo, $mailTo);
  echo "An email with the report has been sent " . PHP_EOL . PHP_EOL;
} else {
  echo "No email will be sent .... Just for view infomation" . PHP_EOL . PHP_EOL;
}
echo $countInfo . PHP_EOL;

function getCountInfo($values = array(), $date) {

  $info = "";
  foreach ($values as $name => $titleId) {
    $result = mysql_query("SELECT COUNT(*) as cnt FROM article INNER JOIN article_original_data ON article.article_original_data_id = article_original_data.id WHERE article.title_id = $titleId  AND article.date = '$date'");
    $row = mysql_fetch_assoc($result);

    $msg = "$name articles : " . $row['cnt'];
    $info .= $msg . "\n";
  }
  return $info;
}

function sendEmail($body, $mailTo) {

  $mail = new ezcMailComposer();
  $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply');

  foreach ($mailTo as $to) {
    $mail->addTo(new ezcMailAddress($to));
  }

  $mail->subject = "Global Data Point report,";
  //$mail->htmlText  = $body;
  $mail->plainText = $body;

  $mail->build();
  $transport = new ezcMailMtaTransport();
  $transport->send($mail);
}
