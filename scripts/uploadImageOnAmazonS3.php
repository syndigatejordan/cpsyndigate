<?php

require_once '/usr/local/syndigate/classes/aws/S3Upload.php';
require_once '/usr/local/syndigate/scripts/propel_include_config.php';

$publisher_id = $argv[2];
$title_id = $argv[1];
$awsimage = new awsSyndigate();

$q = mysql_query("select image.* from image 
  inner join article on image.article_id=article.id 
  and article.title_id=$title_id");

while ($row = mysql_fetch_assoc($q)) {
  $img_name = $row['img_name'];
  $checker = explode('?', $img_name);
  if (count($checker) == 1) {
    $img_path = explode('/', $img_name);
    if ($img_path[2] == "imgs.syndigate.info") {
      $original_image = "/mnt/storage2/syndigate/syndigate_imgs/$publisher_id/$title_id/" . $img_path[5] . '/' . $img_path[6];
      $image = sha1(basename($row['original_name']));
      $pathInfo = pathinfo($row['original_name']);
      if (isset($pathInfo['extension'])) {
        $image .= '.' . $pathInfo['extension'];
      }
      $destPath = IMGS_PATH . "$publisher_id/$title_id/" . $img_path[5] . '/' . $image;


      /* $checkArray = array($destPath);
        $checkresult = $awsimage->checkImage($checkArray);
        if (!$checkresult) { */
      $imageValue = array($destPath, $original_image, "public-read");
      $newPath = $awsimage->uploadImage($imageValue);
      echo 'Update new image' . PHP_EOL;
      echo $newPath . PHP_EOL;
      /*  } else {
        $newPath = $checkresult;
        } */
      if (!empty($newPath)) {
        $qu = "update image set img_name='{$newPath}' where id={$row['id']} and article_id={$row['article_id']};";
        if (mysql_query($qu)) {
          echo 'Update image' . PHP_EOL;
        } else {
          echo 'Something error on image id ' . $row['id'] . PHP_EOL;
        }
      }
    }
  }
}

$q = mysql_query("select image_archive.* from image_archive 
  inner join article_archive on image_archive.article_id=article_archive.id 
  and article_archive.title_id=$title_id");

while ($row = mysql_fetch_assoc($q)) {
  $img_name = $row['img_name'];
  $checker = explode('?', $img_name);
  if (count($checker) == 1) {
    $img_path = explode('/', $img_name);
    if ($img_path[2] == "imgs.syndigate.info") {
      $original_image = "/mnt/storage2/syndigate/syndigate_imgs/$publisher_id/$title_id/" . $img_path[5] . '/' . $img_path[6];
      $image = sha1(basename($row['original_name']));
      $pathInfo = pathinfo($row['original_name']);
      if (isset($pathInfo['extension'])) {
        $image .= '.' . $pathInfo['extension'];
      }
      $destPath = IMGS_PATH . "$publisher_id/$title_id/" . $img_path[5] . '/' . $image;


      /* $checkArray = array($destPath);
        $checkresult = $awsimage->checkImage($checkArray);
        if (!$checkresult) { */
      $imageValue = array($destPath, $original_image, "public-read");
      $newPath = $awsimage->uploadImage($imageValue);
      echo 'Update new image' . PHP_EOL;
      echo $newPath . PHP_EOL;
      /* } else {
        $newPath = $checkresult;
        } */
      if (!empty($newPath)) {
        $qu = "update image_archive set img_name='{$newPath}' where id={$row['id']} and article_id={$row['article_id']};";
        if (mysql_query($qu)) {
          echo 'Update image archive' . PHP_EOL;
        } else {
          echo 'Something error on image archive id ' . $row['id'] . PHP_EOL;
        }
      }
    }
  }
}