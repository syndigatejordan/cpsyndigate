<?php

//error_reporting(E_ALL);ini_set("display_errors","on");
if (php_sapi_name() != 'cli') {
    exit;
}
require_once '/usr/local/syndigate/scripts/propel_include_config.php';
require_once '/usr/local/syndigate/misc/pheanstalk_workers/worker.php';

class  ClearTubes {

    public function __construct() {
        $params = $_SERVER['argv'];
        $this->start($params[1]);
    }

    public function start($tubeName) {
        $pheanstalk = new Pheanstalk(PHEANSTALK_HOST . ':' . PHEANSTALK_PORT);
        echo "\nStart clear tube $tubeName \n";
        while (1) {
            $job = $pheanstalk->watch($tubeName)->ignore('default')->reserve(5);
            if (!$job) {
                echo "\nBeanstalk Queue is supposed to be empty now\n";
                exit;
            }
            var_dump($job);
            $pheanstalk->delete($job);
        }
    }

}

$x = new ClearTubes();
