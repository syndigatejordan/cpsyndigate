<?php

/*
  This script generate XMLs to Thomson Reuters Emerging Businesses.
  run every 1/2 hour
 */


// Check if an instance exists from the same scripts 
$avilable_instances = 0;
$output = shell_exec('ps aux | grep generateManuallySLL.php | grep -v "/bin/sh -c" | grep -v "sh -c" | grep -v "grep" ');
$output = explode(PHP_EOL, $output);
foreach ($output as $o) {
    if ($o) {
        $avilable_instances++;
    }
}

if ($avilable_instances > 1) {
    die();
}


require_once '/usr/local/syndigate/scripts/propel_include_config.php';
while (1) {
  echo "New one ".date('Y-m-d H:i:s') . chr(10);
    $titles = array();

    $dateToStart = date('Y-m-d', strtotime("-1 days"));

    $query = "select report.* from report   "
        . "inner join report_parse on report.report_parse_id = report_parse.id "
        . "inner join titles_clients_rules on report.title_id = titles_clients_rules.title_id and client_id in (139) "
        . "where report_parse.article_num > 0 "
        . "and  report.report_parse_id > 0 and report.report_generate_id  = 0 and report_parse.date >= '$dateToStart' "
        . "and titles_clients_rules.title_id in (10368,10371,10402)";
    $rez = mysql_query($query);

    while ($row = mysql_fetch_assoc($rez)) {
        $titles[] = $row['title_id'];
        $cmd = "ps aux | grep /usr/local/syndigate/classes/syndGenerateRevDriver.php | grep " . $row['title_id'] . " | grep  " . $row['revision_num'];
        $result = shell_exec($cmd);
        $lines = explode(PHP_EOL, $result);

        $results = array();
        foreach ($lines as $line) {
            if ($line) {
                $tabs = explode(" ", $line);
                $tmp = array();
                foreach ($tabs as &$tab) {
                    if ($tab) {
                        $tmp[] = $tab;
                    }
                }
                $results[] = $tmp;
            }
        } // End foreach

        if (count($results) > 1) {
            // there is a process running with same title id and version
            echo "process is running with title id " . $row['title_id'] . " and revision " . $row['revision_num'] . PHP_EOL;
        } else {
            echo "process is started with title id " . $row['title_id'] . " and revision " . $row['revision_num'] . ' report_id=' . (int)$row['id'] . PHP_EOL;
            $cmd = '/usr/bin/php /usr/local/syndigate/classes/syndGenerateRevDriver.php title_id=' . $row['title_id'] . ' revision=' . $row['revision_num'] . ' report_id=' . (int)$row['id'];
            echo "Starting: " . date("Y-m-d H:i:s") . " ";
            echo PHP_EOL;
            $result = shell_exec($cmd);
            echo $cmd . PHP_EOL;
            echo "End: " . date("Y-m-d H:i:s");
            echo PHP_EOL;
        }

    }
    foreach ($titles as $title) {
        $file = "/syndigate/clients/139/$title.xml";
        if (is_file($file)) {
            $cmd = "cp $file /syndigate/clients/139/$title.sll.xml";
            echo $cmd . PHP_EOL;
            shell_exec($cmd);
        }
    }
}
