<?php

/*
  This script generate XMLs to Thomson Reuters Emerging Businesses.
  run every 1/2 hour
 */


// Check if an instance exists from the same scripts 
$avilable_instances = 0;
$output = shell_exec('ps aux | grep generateManuallyBloomberg.php | grep -v "/bin/sh -c" | grep -v "sh -c" | grep -v "grep" ');
$output = explode(PHP_EOL, $output);
foreach ($output as $o) {
  if ($o) {
    $avilable_instances++;
  }
}

if ($avilable_instances > 1) {
  die();
}


require_once '/usr/local/syndigate/scripts/propel_include_config.php';
$titles = array();

$dateToStart = date('Y-m-d', strtotime("-1 days"));

$query = "select report.* from report   "
    . "inner join report_parse on report.report_parse_id = report_parse.id "
    . "inner join titles_clients_rules on report.title_id = titles_clients_rules.title_id and client_id in (124, 134, 138) "
    . "where report_parse.article_num > 0 "
    . "and  report.report_parse_id > 0 and report.report_generate_id  = 0 and report_parse.date >= '$dateToStart' "
    . "and titles_clients_rules.title_id in (10361,10418,10382,10372,10370,10385,10386,10817,11194,10388,10828,10371,11934,10420,12383,12420,12421,12422,12423,12424,12425,12426,12427,12428,12429,12430,12431,12432,12433,12434,12435,12436,12437,12438,12439,12440,12441,12442,12443,12444,12445,12446,12447,12448,12449,12450,12451,12452,12453,12454,12455,12456,12457,10400,10441,10442,10515,10585,10759,10788,11706,12416,12627,12628,12629, 10817, 12636, 12637, 12638, 12304, 12703)";
//echo $query;
$rez = mysql_query($query);

while ($row = mysql_fetch_assoc($rez)) {
    $titles[] = $row['title_id'];
    $cmd = "ps aux | grep /usr/local/syndigate/classes/syndGenerateRevDriver.php | grep " . $row['title_id'] . " | grep  " . $row['revision_num'];
    $result = shell_exec($cmd);
    $lines = explode(PHP_EOL, $result);

    $results = array();
    foreach ($lines as $line) {
        if ($line) {
            $tabs = explode(" ", $line);
            $tmp = array();
            foreach ($tabs as &$tab) {
                if ($tab) {
                    $tmp[] = $tab;
                }
            }
            $results[] = $tmp;
        }
    } // End foreach

    if (count($results) > 1) {
        // there is a process running with same title id and version
        echo "process is running with title id " . $row['title_id'] . " and revision " . $row['revision_num'] . PHP_EOL;
    } else {
        echo "process is started with title id " . $row['title_id'] . " and revision " . $row['revision_num'] . ' report_id=' . (int)$row['id'] . PHP_EOL;
        $cmd = '/usr/bin/php /usr/local/syndigate/classes/syndGenerateRevDriver.php title_id=' . $row['title_id'] . ' revision=' . $row['revision_num'] . ' report_id=' . (int)$row['id'];
        echo "Starting: " . date("Y-m-d H:i:s") . " ";
        echo PHP_EOL;
        $result = shell_exec($cmd);
        echo $cmd . PHP_EOL;
        echo "End: " . date("Y-m-d H:i:s");
        echo PHP_EOL;
    }

}
foreach ($titles as $title) {
    $file = "/syndigate/clients/124/$title.xml";
    if (is_file($file) && $title!=12304) {
        $cmd = "cp $file /syndigate/clients/124/$title.low-latency.xml";
        echo $cmd . PHP_EOL;
        shell_exec($cmd);
    }
}
