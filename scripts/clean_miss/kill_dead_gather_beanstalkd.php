<?php 

/**
 * This script has to be run every hour
 */


// confing ------------------
$max_execution_time = 600;


$result = shell_exec("ps aux | grep /usr/local/syndigate/classes/gather/run/syndGatherRunBeanstalkd.php");
$lines  = explode(PHP_EOL, $result);

echo "all processes containing the gather beansltakd : " . PHP_EOL;
print_r($result);
echo str_repeat(PHP_EOL, 3);


$results = array();
foreach ($lines as $line) {
	$tabs = explode(" ", $line);
	
	$tmp = array();
	foreach ($tabs as &$tab) {
		if($tab) {
			$tmp[] = $tab;
		}
	}
	$results[] = $tmp;
}



foreach ($results as $result ) {
	
	if( ( trim($result[10]) == 'php' ) && ( trim($result[11]) == '/usr/local/syndigate/classes/gather/run/syndGatherRunBeanstalkd.php' )  ) {
		$processId   = $result['1'];
		$startedTime = $result['8'];

		if(strpos($startedTime, ':')) {
			// ex. 10:38
			$timeStr = strtotime( date('Y-m-d ') . $startedTime . ":00");
			if( (time() - $timeStr) > $max_execution_time) {
				echo "process $processId has been running for more than $max_execution_time secounds ... killing..." . PHP_EOL;
				shell_exec("kill -9 $processId");
			}
		} else {
			
			// ex. Sep07
			
			if(date('H') > '04' ) {
				// the script has been running since the day before and the time now is 4 am or more 
				echo "process $processId has been running since yesterday ... killing " . PHP_EOL;
				shell_exec("kill -9 $processId");
			}
		}
	}
}

echo "finish ...." . PHP_EOL;
