<?php 
/*
This script is to run generation for processess that inneruppted after parsing finished but the generation did not start
run every one hour
*/


// Check if an instance exists from the same scripts 
$avilable_instances = 0;
$output = shell_exec('ps aux | grep generate_manually.php | grep -v "/bin/sh -c" | grep -v "sh -c" | grep -v "grep" ');
$output = explode(PHP_EOL, $output);
foreach ($output as $o) {
    if($o) {
       $avilable_instances++; 
    }
}

if($avilable_instances > 1) {    
    die();
}


require_once '/usr/local/syndigate/scripts/propel_include_config.php';

$dateToStart = date('Y-m-d', strtotime("-3 days"));

$query = "select report.* from report   inner join report_parse on report.report_parse_id = report_parse.id where report_parse.article_num > 0 and  report.report_parse_id > 0 and report.report_generate_id  = 0 and report_parse.date >= '$dateToStart' order by report.id desc";


$rez = mysql_query($query); 

while($row = mysql_fetch_assoc($rez)) {
	$cmd    = "ps aux | grep /usr/local/syndigate/classes/syndGenerateRevDriver.php | grep " .  $row['title_id']  . " | grep  " .  $row['revision_num'];
	$result = shell_exec($cmd);
	$lines  = explode(PHP_EOL, $result);

	$results = array();
	foreach ($lines as $line) {
		if($line) {
                    $tabs = explode(" ", $line);
                    $tmp  = array();
                    foreach ($tabs as &$tab) {
                        if($tab) {
                        	$tmp[] = $tab;
                        }
                    }
                    $results[] = $tmp;
		}
	} // End foreach

	if(count($results) >1) {
    	// there is a process running with same title id and version
		echo "process is running with title id " .  $row['title_id'] . " and revision " . $row['revision_num'] . PHP_EOL;
	} else {               
    echo "process is started with title id " .  $row['title_id'] . " and revision " . $row['revision_num'] . ' report_id=' .  (int)$row['id']. PHP_EOL;
    $cmd = '/usr/bin/php /usr/local/syndigate/classes/syndGenerateRevDriver.php title_id=' . $row['title_id'] . ' revision=' . $row['revision_num'] . ' report_id=' .  (int)$row['id'];
    echo "Starting: ".date("Y-m-d H:i:s")." ";            
    echo PHP_EOL;
      $result = shell_exec($cmd);
    echo $cmd . PHP_EOL;
    echo "End: ".date("Y-m-d H:i:s");
    echo PHP_EOL;
	}

}
