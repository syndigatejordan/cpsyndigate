<?php 
/**
 * This script has to be run every 10 minutes
 */

$scriptStartTime = time();

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

echo "script start time : " . date("Y-m-d H:i:s") . PHP_EOL ;

/*
 * getting all running parsing state records  
 */
$c = new Criteria();
$c->add(ParsingStatePeer::LOCKED ,1);
$c->add(ParsingStatePeer::COMPLETED,0);
$parsingStates = ParsingStatePeer::doSelect($c);

$lockedParsingStateProcessesIds = array();
foreach ($parsingStates as $record) {
	$lockedParsingStateProcessesIds[] = $record->getPid();
}
 
echo "Locked pids in parsing state records are"; print_r($lockedParsingStateProcessesIds); echo PHP_EOL . PHP_EOL ;



$result = shell_exec("ps aux | grep /usr/local/syndigate/shepherd/shepherd.php");
$lines  = explode(PHP_EOL, $result);

echo "all processes containing shepherd : " . PHP_EOL;
print_r($result);
echo str_repeat(PHP_EOL, 2);


$results = array();
foreach ($lines as $line) {
	$tabs = explode(" ", $line);
	
	$tmp = array();
	foreach ($tabs as &$tab) {
		if($tab) {
			$tmp[] = $tab;
		}
	}
	$results[] = $tmp;
}



foreach ($results as $result ) {
	if(!empty($result)) {
		
		if( ( trim($result[10]) == 'php' ) && ( trim($result[11]) == '/usr/local/syndigate/shepherd/shepherd.php' )  && ( trim($result[12]) == '--config=/usr/local/syndigate/conf/shepherd.conf.php') ) {
			$processId   = $result['1'];
			$startedTime = $result['8'];
			
			if(strpos($startedTime, ':')) {
				// ex. 10:38
				$timeStr = strtotime( date('Y-m-d ') . $startedTime . ":00");
				if($timeStr< $scriptStartTime) {
					if(!in_array($processId, $lockedParsingStateProcessesIds)) {
						if('Ss' != $result['7']) {
							echo "Killing process $processId , process exsist before script running and no records in parsing state." . PHP_EOL;
							shell_exec("kill -9 $processId");
						}
					}
				}
				
				
			}
		}
	}
}


echo str_repeat(PHP_EOL, 2);
$result = shell_exec("ps aux | grep /usr/local/syndigate/shepherd/shepherd.php");
echo "all processes containing shepherd after killing expired threads : " . PHP_EOL;
print_r($result);
echo str_repeat(PHP_EOL, 2);

echo "finish ...." . PHP_EOL;