<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

$publisher_id = $argv[2];
$title_id = $argv[1];
$pwd = "/syndigate/sources/pwc/$publisher_id/$title_id";
$wc = "/syndigate/sources/wc/$publisher_id/$title_id";
$svn_dir = "/var/svnserve/repos";
if (!$publisher_id) {
  $q = mysql_query("select publisher_id from title where id=$title_id");
  $data = mysql_fetch_row($q);
  $publisher_id = $data[0];
}
if (!$publisher_id || !$title_id) {
  die('publisher and title empty' . PHP_EOL);
}
// move the old svn to backup svn
$cmd = "mv $svn_dir/$publisher_id/$title_id $svn_dir/$publisher_id/old_$title_id";
echo $cmd . chr(10);
exec($cmd);
$cmd = "svnadmin create $svn_dir/$publisher_id/$title_id";
echo $cmd . chr(10);
exec($cmd);
$cmd = "cp -r $svn_dir/$publisher_id/old_$title_id/conf/* $svn_dir/$publisher_id/$title_id/conf/";
echo $cmd . chr(10);
exec($cmd);
$cmd = "cp -r $svn_dir/$publisher_id/old_$title_id/hooks/* $svn_dir/$publisher_id/$title_id/hooks/";
echo $cmd . chr(10);
exec($cmd);
$cmd = "rm -r $svn_dir/$publisher_id/old_$title_id";
echo $cmd . chr(10);
exec($cmd);
$cmd = "rm -r /syndigate/sources/wc/$publisher_id/$title_id";
echo $cmd . chr(10);
exec($cmd);
$cmd = "svn co svn://localhost/$publisher_id/$title_id /syndigate/sources/wc/$publisher_id/$title_id";
echo $cmd . chr(10);
exec($cmd);
$cmd = "rm -r /syndigate/sources/pwc/$publisher_id/$title_id";
echo $cmd . chr(10);
exec($cmd);
$cmd = "svn co svn://localhost/$publisher_id/$title_id /syndigate/sources/pwc/$publisher_id/$title_id";
echo $cmd . chr(10);
exec($cmd);