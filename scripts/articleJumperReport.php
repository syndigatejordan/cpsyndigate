<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

//$mailTo = array('khaled@syndigate.info','eyad@syndigate.info','carla@syndigate.info','safaa@syndigate.info');
//$mailTo = array('hani@corp.albawaba.com','mark@syndigate.info','khaled@syndigate.info','eyad@syndigate.info');
$mailTo = array('khaled@syndigate.info');
//$mailTo = array('mohshami@gmail.com');
//-------------------------------------- Main--------------------------------------------------

$date = date('Y-m-d', strtotime('-1 day'));
$title_id = array('Albawaba News' => 812);
$mapped_title = getMappedTitle();
$data = array();
$data = getCountInfo($mapped_title, $title_id, $date);
foreach ($data as $key => $row) {
  $mid[$key] = $row['total'];
}
// Sort the data with mid descending
// Add $data as the last parameter, to sort by the common key
array_multisort($mid, SORT_DESC, $data);
$msg = "TendersInfo Articles count for date $date. , (Date is based on the date tag in the XML file, not the date when they were sent/updated on the XML file) \n\n\n";
sendEmail($msg, $mailTo, $data, $title_id);
//echo $countInfo . PHP_EOL;
//deleteOldArticles();

function getMappedTitle() {
  $query = mysql_query("select mapped_title from article_jumper group by mapped_title;");
  $mapped_title = array();
  $i = 0;
  while ($row = mysql_fetch_row($query)) {
    $mapped_title[$i]['mapped_title'] = $row[0];
    $i++;
  }
  return $mapped_title;
}

function getCountInfo($mapped_title, $title_id, $date) {
  $count_article = 0;
  $data = array();
  $i = 0;
  foreach ($mapped_title as $title) {
    $result = mysql_query("select name from title where id={$title['mapped_title']};");
    $name = mysql_fetch_assoc($result);
    $data[$i]["name"] = "<th>{$name["name"]} ({$title['mapped_title']})</th>";
    foreach ($title_id as $title_name => $id) {
      $result = mysql_query("select count(*) cnt from article_jumper where title_id={$id} and is_sent=1 and mapped_title={$title['mapped_title']} and  date(send_date)='{$date}';");
      $row = mysql_fetch_assoc($result);
      $data[$i]["{$id}"] = "{$row["cnt"]}";
    }
   // $result = mysql_query("select sum(article_num) sum from report_parse where title_id ={$title['mapped_title']} and date='{$date}';");
$result  = mysql_query("SELECT COUNT(*) as sum FROM article INNER JOIN article_original_data ON article.article_original_data_id = article_original_data.id WHERE article.title_id = {$title['mapped_title']} AND article.date = '$date'");
    $row = mysql_fetch_assoc($result);
    if (is_null($row["sum"])) {
      $row["sum"] = '0';
    }
    $count_article+=intval($row["sum"]);
    $data[$i]["total"] = "{$row["sum"]}";
    $i++;
  }
  return $data;
}

function sendEmail($body, $mailTo, $data, $title_id) {
  $table = "";
  $count_811 = 0;
  $count_812 = 0;
  $count_813 = 0;
  $total = 0;
  $table .= "<table border='1'><tr><th>Mapped Title</th>";
  foreach ($title_id as $title_name => $id) {
    $table .= "<th>{$title_name} ({$id})</th>";
  }
  $table .= "<th>Count articles from publisher</th></tr>";
  foreach ($data as $sell) {
    $table .= "<tr>" . $sell["name"];
    $count_811 +=intval($sell["811"]);
    $table .="<td align='center'>{$sell["811"]}</td>";
    $count_812 +=intval($sell["812"]);
    $table .="<td align='center'>{$sell["812"]}</td>";
    $count_813 +=intval($sell["813"]);
    $table .="<td align='center'>{$sell["813"]}</td>";
    $total +=intval($sell["total"]);
    $table .="<td align='center'>{$sell["total"]}</td></tr>";
  }
  $table .= "<tr><th>Total TendersInfo Articles</th>";
  $table .= "<td align='center'>{$count_811}</td>";
  $table .= "<td align='center'>{$count_812}</td>";
  $table .= "<td align='center'>{$count_813}</td>";
  $table .= "<td align='center'>{$total}</td></tr>";
  
$table .="</table>";
  
  $body=$body.$table;
  $body=wordwrap($body, 75, "\r\n");
  
  $mail = new ezcMailComposer();
  $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply');
  foreach ($mailTo as $to) {
    $mail->addTo(new ezcMailAddress($to));
  }
  $mail->subject = "TendersInfo Custom Sent Report";
  $mail->htmlText = $body;
  //$mail->plainText = $body;
  $mail->build();
  $transport = new ezcMailMtaTransport();
  $transport->send($mail);
}
