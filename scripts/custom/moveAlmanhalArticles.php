<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

class moveAlmanhalArticles {

  public $titles = array();
  public $map = array();
  public $articles = array();
  public $newDataID = array();

  public function __construct() {
    //$this->getTitles();
    //$this->getMap();
    //$this->getArticles();
    //$this->matcheArticlesWithTitles();
    //$this->matcheEThesesWithTitles();
    //$this->matcheEReportsWithTitles();
    //$this->matcheEBooksWithTitles();
    //$this->matcheIFHWithTitles();
    $this->fixImageSend();
  }

  public function getTitles() {
    $count = 0;
    $q = mysql_query("select t.id as title_id,t.name as title_name,tt.type,l.name as language_name from title t "
            . "inner join title_type tt on t.title_type_id=tt.id inner join language l on t.language_id=l.id "
            . "where publisher_id=887;");
    while ($row = mysql_fetch_array($q)) {
      $this->titles[$count]["id"] = $row["title_id"];
      $this->titles[$count]["name"] = $row["title_name"];
      $this->titles[$count]["language"] = $row["language_name"];
      $this->titles[$count]["type"] = $row["type"];
      $count++;
    }
  }

  public function getPublisherId($id) {
    $count = 0;
    $q = mysql_query("select publisher_id from title where id=$id;");
    while ($row = mysql_fetch_array($q)) {
      return $row["publisher_id"];
    }
    return FALSE;
  }

  public function getMap() {
    $file = file_get_contents("/syndigate/sources/home/887/workDir/map.json");
    $data = json_decode($file, TRUE);
    $this->map = array();
    foreach ($data as $item) {
      $this->map[$item["Journal ID"]][$item["Language"]][] = $item["SG ID"];
    }
  }

  public function getArticles() {
    $file = file_get_contents("/syndigate/sources/home/887/workDir/Articles.xml");
    $this->getMap();
    $matches = null;
    preg_match_all("/<item>(.*?)<\/item>/is", $file, $matches);
    $matches = $matches[0];
    foreach ($matches as $key => $article) {
      $article = str_replace("<items>item</items>", "", $article);
      echo "Article id $key" . PHP_EOL . PHP_EOL;

      $ORG_NO = trim($this->getElementByName("ORG_No", $article));
      $language = trim($this->getElementByName("Language", $article));
      $coverName = trim($this->getElementByName("MLT", $article));
      $journalID = trim($this->getElementByName("Journal_ID", $article));


      switch ($language) {
        case "English":
          //English
          if (!isset($this->map[$journalID]["En"])) {
            echo "Not find" . PHP_EOL . PHP_EOL;
            $type = "Not find source name";
            $data = array(
                $ORG_NO, $journalID, "English",
            );
            $this->updateJsonFile($type, $data);
            continue;
          }
          foreach ($this->map[$journalID]["En"] as $idJ) {
            $id = $idJ;
            echo "Move article to title  ID  $id" . PHP_EOL . PHP_EOL;
            $this->moveFile($id, $coverName, $article, $ORG_NO);
          }

          break;

        case "Arabic/English":
          //English
          if (!isset($this->map[$journalID]["En"])) {
            echo "Not find" . PHP_EOL . PHP_EOL;
            $type = "Not find source name";
            $data = array(
                $ORG_NO, $journalID, "English",
            );
            $this->updateJsonFile($type, $data);
            continue;
          }
          foreach ($this->map[$journalID]["En"] as $idJ) {
            $id = $idJ;
            echo "Move article to title  ID  $id" . PHP_EOL . PHP_EOL;
            $this->moveFile($id, $coverName, $article, $ORG_NO);
          }
          //Arabic
          if (!isset($this->map[$journalID]["Ar"])) {
            echo "Not find" . PHP_EOL . PHP_EOL;
            $type = "Not find source name";
            $data = array(
                $ORG_NO, $journalID, "Arabic",
            );
            $this->updateJsonFile($type, $data);
            continue;
          }
          foreach ($this->map[$journalID]["Ar"] as $idJ) {
            echo "Move article to title  ID  $id" . PHP_EOL . PHP_EOL;
            $this->moveFile($id, $coverName, $article, $ORG_NO);
          }
          break;
        case "French/English":
          //English
          if (!isset($this->map[$journalID]["En"])) {
            echo "Not find" . PHP_EOL . PHP_EOL;
            $type = "Not find source name";
            $data = array(
                $ORG_NO, $journalID, "English",
            );
            $this->updateJsonFile($type, $data);
            continue;
          }
          foreach ($this->map[$journalID]["En"] as $idJ) {
            $id = $idJ;
            echo "Move article to title  ID  $id" . PHP_EOL . PHP_EOL;
            $this->moveFile($id, $coverName, $article, $ORG_NO);
          }
          //French
          if (!isset($this->map[$journalID]["Fr"])) {
            echo "Not find" . PHP_EOL . PHP_EOL;
            $type = "Not find source name";
            $data = array(
                $ORG_NO, $journalID, "French",
            );
            $this->updateJsonFile($type, $data);
            continue;
          }
          foreach ($this->map[$journalID]["Fr"] as $idJ) {
            $id = $idJ;
            echo "Move article to title  ID  $id" . PHP_EOL . PHP_EOL;
            $this->moveFile($id, $coverName, $article, $ORG_NO);
          }
          break;
      }
    }
  }

  public function matcheArticlesWithTitles() {
    $file = "/syndigate/sources/home/887/workDir/Articles.xml";
    foreach ($this->articles as $key => $article) {
      echo "Article id $key" . PHP_EOL . PHP_EOL;
      // sleep(1);
      $publicationTitle = $this->getElementByName("Publication_Title", $article);
      //var_dump($publicationTitle);
      $collectionOriginal = $this->getElementByName("Collection", $article);
      $collection = str_replace("All Journals", "", $collectionOriginal);
      $collection = str_replace("All Articles before 2015", "", $collection);
      $collection = explode("|", $collection);
      foreach ($collection as $key => $coll) {
        $coll = trim($coll);
        if (empty($coll)) {
          unset($collection[$key]);
        }
      }
      sort($collection);
      $collection = implode(" | ", $collection);
      $collection = trim(preg_replace("!\s+!", " ", $collection));
      $ORG_NO = $this->getElementByName("ORG_NO.", $article);
      $language = $this->getElementByName("Language", $article);
      $fullTextDocument = $this->getElementByName("Full_Text_Document", $article);
      $coverName = $this->getElementByName("Cover_Name", $article);
      //var_dump($collection);
      $data = $this->fetchMap($publicationTitle, $collection, $ORG_NO, $collectionOriginal);
      if ($data) {
        $data[0] = strtolower(trim($data[0]));
        if ($data[0] == "islamic finance hub") {
          switch ($language) {
            case "English":
              $id = 3178;
              $this->moveFile($id, $coverName, $fullTextDocument, $file, $ORG_NO);
              break;
            case "Arabic":
              $id = 3177;
              $this->moveFile($id, $coverName, $fullTextDocument, $file, $ORG_NO);
              break;
            case "French":
              $id = 3181;
              $this->moveFile($id, $coverName, $fullTextDocument, $file, $ORG_NO);
              break;
          }
        } elseif ($data[0] == "science, technical and medical (stm)") {
          $type = "Science, Technical and Medical Journal";
          $id = $this->getID($publicationTitle, $type, $language);
          if ($id) {
            $this->moveFile($id, $coverName, $fullTextDocument, $file, $ORG_NO);
          }
        } elseif ($data[0] == "humanitarian and social science") {
          $type = "Humanitarian and Social Science Journal";
          $id = $this->getID($publicationTitle, $type, $language);
          if ($id) {
            $this->moveFile($id, $coverName, $fullTextDocument, $file, $ORG_NO);
          }
        } else {
          var_dump($data);
          exit;
        }
        echo "Get Title $id" . PHP_EOL . PHP_EOL;
        $this->newDataID[$id] = $id;
        $file = "/syndigate/sources/home/887/workDir/Title.json";
        $newJsonString = json_encode($this->newDataID);
        file_put_contents($file, $newJsonString);
      } else {
        var_dump($data);
        exit;
      }
    }
  }

  public function matcheEThesesWithTitles() {
    $file = file_get_contents("/syndigate/sources/home/887/workDir/eThesesArabic.xml");
    $matches = null;
    preg_match_all("/<Title>(.*?)<\/Language>/is", $file, $matches);
    $matches = $matches[0];
    foreach ($matches as $key => $article) {

      echo "Article id $key" . PHP_EOL . PHP_EOL;

      $ORG_NO = trim($this->getElementByName("ORG_No", $article));
      $language = trim($this->getElementByName("Language", $article));
      $coverName = trim($this->getElementByName("MLT", $article));
      switch ($language) {
        case "English":
          $id = 4251;
          $this->moveFile($id, $coverName, $article, $ORG_NO);
          break;
        case "Arabic":
          $id = 4252;
          $this->moveFile($id, $coverName, $article, $ORG_NO);
          break;
        case "French":
          $id = 4253;
          $this->moveFile($id, $coverName, $article, $ORG_NO);
          break;
      }
    }
  }

  public function matcheIFHWithTitles() {
    $file = file_get_contents("/syndigate/sources/home/887/workDir/IFH_En.xml");
    $matches = null;
    preg_match_all("/<item>(.*?)<\/item>/is", $file, $matches);
    $matches = $matches[0];
    foreach ($matches as $key => $article) {
      $article = str_replace("<items>item</items>", "", $article);
      echo "Article id $key" . PHP_EOL . PHP_EOL;

      $ORG_NO = trim($this->getElementByName("ORG_No", $article));
      $language = trim($this->getElementByName("Language", $article));
      $coverName = trim($this->getElementByName("MLT", $article));

      //echo "ORG_NO $ORG_NO" . PHP_EOL . PHP_EOL;
      //echo "language $language" . PHP_EOL . PHP_EOL;
      //echo "coverName $coverName" . PHP_EOL . PHP_EOL;
      switch ($language) {
        case "English":
          $id = 5486;
          $this->moveFile($id, $coverName, $article, $ORG_NO);
          break;
        case "Arabic":
          $id = 5485;
          $this->moveFile($id, $coverName, $article, $ORG_NO);
          break;
      }
    }
  }

  public function matcheEReportsWithTitles() {
    $file = file_get_contents("/syndigate/sources/home/887/workDir/eReports.xml");
    $file = str_replace("&#160;", " ", $file);
    $file = str_replace("&amp;", "&", $file);
    $file = str_replace("<eReports></eReports>", "", $file);
    $matches = null;
    preg_match_all("/<eReports>(.*?)<\/eReports>/is", $file, $matches);
    $matches = $matches[0];
    foreach ($matches as $key => $article) {
      echo "Article id $key" . PHP_EOL . PHP_EOL;
      $ORG_NO = $this->getElementByName("ORG_NO.", $article);
      $language = $this->getElementByName("Language", $article);
      $fullTextDocument = $this->getElementByName("Full_Text_Document", $article);
      $coverName = $this->getElementByName("Cover_Name", $article);
      switch ($language) {
        case "English":
          $id = 2999;
          $this->moveFile($id, $coverName, $fullTextDocument, $file, $ORG_NO);
          break;
        case "Arabic":
          $id = 2998;
          $this->moveFile($id, $coverName, $fullTextDocument, $file, $ORG_NO);
          break;
        case "French":
          $id = 3184;
          $this->moveFile($id, $coverName, $fullTextDocument, $file, $ORG_NO);
          break;
      }
    }
  }

  public function matcheEBooksWithTitles() {
    $file = file_get_contents("/syndigate/sources/home/887/workDir/eBooks.xml");
    $file = str_replace("&#160;", " ", $file);
    $file = str_replace("&amp;", "&", $file);
    $file = str_replace("<eBooks></eBooks>", "", $file);
    $matches = null;
    preg_match_all("/<eBooks>(.*?)<\/eBooks>/is", $file, $matches);
    $matches = $matches[0];
    foreach ($matches as $key => $article) {
      echo "Article id $key" . PHP_EOL . PHP_EOL;
      $ORG_NO = $this->getElementByName("ORG_NO.", $article);
      $language = $this->getElementByName("Language", $article);
      $fullTextDocument = $this->getElementByName("Full_Text_Document", $article);
      $coverName = $this->getElementByName("Cover_Name", $article);
      switch ($language) {
        case "English":
          $id = 3003;
          $this->moveFile($id, $coverName, $fullTextDocument, $file, $ORG_NO);
          break;
        case "Arabic":
          $id = 3002;
          $this->moveFile($id, $coverName, $fullTextDocument, $file, $ORG_NO);
          break;
        case "French":
          $id = 3182;
          $this->moveFile($id, $coverName, $fullTextDocument, $file, $ORG_NO);
          break;
      }
    }
  }

  public function fetchMap($publicationTitle, $collection, $ORG_NO, $collectionOriginal) {
    echo "Fetch map for article $ORG_NO" . PHP_EOL . PHP_EOL;

    $publicationTitle = strtolower($publicationTitle);
    $collection = strtolower($collection);
    foreach ($this->map as $map) {
      $find = false;
      $sourceName = strtolower($this->getElementByName("Source_Name", $map));


      $category = strtolower($this->getElementByName("Category", $map));

      $mapCollections = $this->getElementByName("Collections", $map);
      $mapCollections = str_replace("All Journals", "", $mapCollections);
      $mapCollections = str_replace("All Articles before 2015", "", $mapCollections);
      $mapCollections = explode(" | ", $mapCollections);
      foreach ($mapCollections as $key => $coll) {
        $coll = trim($coll);
        if (empty($coll)) {
          unset($mapCollections[$key]);
        }
      }
      sort($mapCollections);
      $mapCollections = implode(" | ", $mapCollections);
      $mapCollections = strtolower(trim(preg_replace("!\s+!", " ", $mapCollections)));
      //echo "$mapCollections" . PHP_EOL . PHP_EOL;
      //echo "$collection" . PHP_EOL . PHP_EOL;
      $Language = trim(strtolower($this->getElementByName("Language", $map)));
      $Language = explode(",", $Language);
      if (strpos($collection, $mapCollections) !== false && strpos($publicationTitle, $sourceName) !== false) {
        $find = true;
        echo "Find Collections and Source name" . PHP_EOL . PHP_EOL;
        $category = trim($category);
        return array($category, $Language);
      }
    }
    if ($publicationTitle == "arab journal of forensic sciences and forensic medicine" || $publicationTitle == "jordan medical journal") {
      $find = true;
      echo "Find Collections and Source name" . PHP_EOL . PHP_EOL;
      $category = "Science, Technical and Medical (STM)";
      $category = trim($category);
      return array($category, $Language);
    }
    if ($publicationTitle == "dirasat: administrative sciences" || $publicationTitle == "dirasat: educational sciences" || $publicationTitle == "dirasat: human and social sciences" || $publicationTitle == "dirasat: shari'a and law sciences" || $publicationTitle == "islam and civilisational renewal" || $publicationTitle == "iug journal of humanities research" || $publicationTitle == "jordan journal for history and archaeology" || $publicationTitle == "journal of palestinian refugee studies" || $publicationTitle == "journal of social affairs" || $publicationTitle == "al-athar" || $publicationTitle == "al-ʻarabīyah: majallat majmaʻ al-lughah al-ʻarabīyah al-filasṭīnī" || $publicationTitle == "al-majmaʻ" || $publicationTitle == "childhood and education journal" || $publicationTitle == "education and progress journal" || $publicationTitle == "islāmīyat al ma'rifah: journal of contemporary islamic thought" || $publicationTitle == "istishrāf lil-dirāsāt al-mustaqbalīyah" || $publicationTitle == "jāmiʻah" || $publicationTitle == "majallat al-dirāsāt al-tarbawīyah wa-al-insanīyah" || $publicationTitle == "majallat al-dirāsāt al-tārīkhīyah wa al-ijtimāʻīyah" || $publicationTitle == "majallat al-Ḥikmah lil-dirāsāt al-adabīyah wa-al-lughawīyah" || $publicationTitle == "majallat al-Ḥikmah lil-dirāsāt al-iqtiṣādīyah" || $publicationTitle == "majallat al-Ḥikmah lil-dirāsāt al-tārīkhīyah" || $publicationTitle == "majallat al-turāth" || $publicationTitle == "majallathu allughah" || $publicationTitle == "the journal of the faculty of economics and political science" || $publicationTitle == "journal of education") {
      $find = true;
      echo "Find Collections and Source name" . PHP_EOL . PHP_EOL;
      $category = "Humanitarian and Social Science";
      $category = trim($category);
      return array($category, $Language);
    }
    if (!$find) {
      var_dump($publicationTitle);
      exit;
      echo "Not find" . PHP_EOL . PHP_EOL;
      $type = "Not find source name";
      $data = array(
          $ORG_NO, $publicationTitle, $collection, $collectionOriginal
      );
      $this->updateJsonFile($type, $data);
    }
    return FALSE;
  }

  public function getID($name, $type, $language) {
    $language = strtolower(trim($language));
    $name = strtolower(trim($name));
    $name = preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $name);
    if ($name == "awliyat al-ulum wa-al-tiknulujiya") {
      $find = TRUE;
      return 3225;
    }
    $type = strtolower(trim($type));
    $find = FALSE;
    foreach ($this->titles as $title) {
      $title["name"] = strtolower(trim($title["name"]));
      $title["type"] = strtolower(trim($title["type"]));
      if ($title["name"] == $name && $title["type"] == $type) {
        $find = TRUE;
        return $title["id"];
      }
    }
    if ($type == "science, technical and medical journal") {
      switch ($language) {
        case "arabic":
          $find = TRUE;
          return 2996;
          break;
        case "english":
          $find = TRUE;
          return 2997;
          break;
        case "french":
          $find = TRUE;
          return 3185;
          break;
      }
      $find = TRUE;
      return 3225;
    } elseif ($type == "humanitarian and social science journal") {
      switch ($language) {
        case "arabic":
          $find = TRUE;
          return 2647;
          break;
        case "english":
          $find = TRUE;
          return 2648;
          break;
        case "french":
          $find = TRUE;
          return 3183;
          break;
      }
      $find = TRUE;
      return 3225;
    }
    if (!$find) {
      var_dump($name);
      exit;
      echo "Not find Title" . PHP_EOL . PHP_EOL;
      $typec = "Not find Title";
      $data = array(
          $name, $type
      );
      $this->updateJsonFile($typec, $data);
    }
    return FALSE;
  }

  //   $this->moveFile($id, $coverName, $article, $ORG_NO);
  public function moveFile($id, $coverName, $article, $ORG_NO) {
    $img = "default.jpg";
    $pdf = $coverName . ".pdf";
    $file = "/syndigate/sources/home/887/workDir/$coverName.xml";
    $imgFile = "/syndigate/sources/home/887/coversDir/$img";
    $pdfFile = "/syndigate/sources/home/887/pdfDir/$pdf";


    $current = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' . $article;
    file_put_contents($file, $current);


    if (is_file($pdfFile)) {
      $this->moveFileToTitleDir($id, $file, $pdfFile);

      if (is_file($imgFile)) {
        $this->moveFileToTitleDir($id, $file, $imgFile);
      } else {
        echo "Not find Image" . PHP_EOL . PHP_EOL;
        $type = "Not find Image";
        $data = array(
            $ORG_NO, $img
        );
        $this->updateJsonFile($type, $data);
      }
    } else {
      echo "Not find PDF" . PHP_EOL . PHP_EOL;
      $type = "Not find PDF";
      $data = array(
          $ORG_NO, $pdf
      );
      $this->updateJsonFile($type, $data);
    }
  }

  public function moveFileToTitleDir($id, $file, $media) {
    $publiher_id = $this->getPublisherId($id);
    if (!$publiher_id) {
      return FALSE;
    }
    $targetDir = "/syndigate/sources/home/$publiher_id/$id/2019/08/30/";
    if (!is_dir($targetDir)) {
      mkdir($targetDir, '0775', true);
    }
    $move = "cp " . $file . ' ' . $targetDir;
    echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
    shell_exec($move);

    $media = str_replace(' ', '\ ', $media);
    $move = "cp " . $media . ' ' . $targetDir;
    echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
    shell_exec($move);

    $type = "Title";
    $data = array(
        $publiher_id, $id
    );
    $this->updateJsonFile($type, $data);
  }

  public function updateJsonFile($type, $newData) {
    if ($type == "Not find source name") {
      $file = "/syndigate/sources/home/887/workDir/notFindSourceName.json";
    } elseif ($type == "Not find Title") {
      $file = "/syndigate/sources/home/887/workDir/notFindTitle.json";
    } elseif ($type == "Not find Image") {
      $file = "/syndigate/sources/home/887/workDir/notFindImage.json";
    } elseif ($type == "Not find PDF") {
      $file = "/syndigate/sources/home/887/workDir/notFindPDF.json";
    } elseif ($type == "Title") {
      $file = "/syndigate/sources/home/887/workDir/Title.json";
    }
    $jsonString = file_get_contents($file);
    $data = json_decode($jsonString, true);
    $count = count($data);
    $data[$count] = $newData;
    //update $data
    $newJsonString = json_encode($data);
    file_put_contents($file, $newJsonString);
  }

  public function getElementByName($name, $text) {
    $element = $this->getElementsByName($name, $text);
    return (isset($element[0]) ? $element[0] : null);
  }

  public function getElementsByName($name, $text) {
    $openTagRegExp = '<' . $name . '([^\>]|[\s])*>';
    $closeTagRegExp = '<\/' . $name . '[\s]*>';
    $elements = preg_split("/$openTagRegExp/i", $text);

    $elementsContents = array();
    $elementsCount = count($elements);
    for ($i = 1; $i < $elementsCount; $i++) {
      $element = preg_split("/$closeTagRegExp/i", $elements[$i]);
      $elementsContents[] = $element[0];
    }
    return $elementsContents;
  }

  public function fixImageSend() {
    $count = 0;
    $file = "/syndigate/sources/home/887/coversDir/default.jpg";
    $q = mysql_query("select publisher_id,title_id,article_id,i.id as image_id,img_name,image_caption "
            . "from image i inner join article a on a.id=i.article_id "
            . "inner join title t on a.title_id=t.id and publisher_id=1186 where img_name=''");
    while ($row = mysql_fetch_array($q)) {
      $targetDir = "/syndigate/imgs/{$row["publisher_id"]}/{$row["title_id"]}/1/";
      if (!is_dir($targetDir)) {
        mkdir($targetDir, 0777, true);
      }
      $move = "cp " . $file . ' ' . $targetDir;
      echo $move . chr(10);
      shell_exec($move);
      $img_name = "http://imgs.syndigate.info/{$row["publisher_id"]}/{$row["title_id"]}/1/default.jpg";
      $qu = "update image set img_name='$img_name', image_type='jpg' where id={$row['image_id']} ;";
      if (mysql_query($qu)) {
        echo 'Update image name' . PHP_EOL;
      } else {
        echo 'Something error on image id ' . $row['id'] . PHP_EOL;
      }
    }
  }

}

$alManhal = new moveAlmanhalArticles();
