<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

class moveArticlesToArchive2
{

    public $checkArticle = FALSE;
    public $checkArticleOriginalData = FALSE;
    public $checkImage = FALSE;
    public $checkVideo = FALSE;
    public $articleDelete = array();
    public $articleOriginalDataDelete = array();
    public $articleId = "";
    public $articleOriginalDataid = "";
    public $uloop_id = "6766,6767,6768,6769,6770,6771,6772,6773,6774,6775,6776,6777,6778,6779,6780,6781,6782,6783,6784,6785,6786,6787,6788,6789,6790,6791,6792,6793,6794,6795,6796,6797,6798,6799,6800,6801,6802,6803,6804,6805,6806,6807,6808,6809,6810,6811,6812,6813,6814,6815,6816,6817,6818,6819,6820,6821,6822,6823,6824,6825,6826,6827,6828,6829,6830,6831,6832,6833,6834,6835,6836,6837,6838,6839,6840,6841,6842,6843,6844,6845,6846,6847,6848,6849,6850,6851,6852,6853,6854,6855,6856,6857,6858,6859,6860,6861,6862,6863,6864,6865,6866,6867,6868,6869,6870,6871,6872,6873,6874,6875,6876,6877,6878,6879,6880,6881,6882,6883,6884,6885,6886,6887,6888,6889,6890,6891,6892,6893,6894,6895,6896,6897,6898,6899,6900,6901,6902,6903,6904,6905,6906,6907,6908,6909,6910,6911,6912,6913,6914,6915,6916,6917,6918,6919,6920,6921,6922,6923,6924,6925,6926,6927,6928,6929,6930,6931,6932,6933,6934,6935,6936,6937,6938,6939,6940,6941,6942,6943,6944,6945,6946,6947,6948,6949,6950,6951,6952,6953,6954,6955,6956,6969,6970,6971,6972,6973,6974,6975,6976,6977,6978,6979,6980,6981,6982,6983,6984,6985,6986,6987,6988,6989,6990,6991,6992,6993,6994,6995,6996,6997,6998,6999,7000,7001,7002,7003,7004,7005,7006,7007,7008,7009,7010,7011,7012,7013,7014,7015,7016,7042,7043,7044,7045,7046,7047,7048,7049,7050,7051,7052,7053,7054,7055,7056,7057,7058,7059,7060,7061,7062,7063,7064,7065,7066,7067,7068,7069,7070,7071,7072,7073,7074,7075,7076,7077,7078,7079,7080,7081,7082,7083,7084,7085,7086,7087,7088,7089,7090,7091,7092,7093,7094,7095,7096,7097,7098,7099,7100,7101,7102,7103,7104,7105,7106,7107,7108,7109,7110,7111,7112,7113,7114,7115,7116,7117,7118,7119,7120,7121,7122,7123,7124,7125,7126,7127,7128,7129,7130,7131,7132,7133,7134,7135,7136,7137,7138,7139,7140,7141,7142,7143,7144,7145,7146,7147,7148,7149,7150,7151,7152,7153,7154,7155,7156,7157,7158,7159,7160,7161,7162,7163,7164,7165,7166,7167,7168,7169,7170,7171,7172,7173,7174,7175,7176,7177,7178,7179,7180,7181,7182,7183,7184,7185,7186,7187,7190,7191,7192,7193,7194,7195,7196,7197,7198,7199,7200,7201,7202,7203,7204,7205,7206,7207,7208,7209,7210,7211,7212,7213,7214,7215,7216,7217,7218,7219,7220,7221,7222,7223,7224,7225,7226,7227,7228,7229,7230,7231,7232,7233,7234,7235,7236,7237,7238,7239,7240,7241,7242,7243,7244,7245,7246,7247,7248,7249,7250,7251,7252,7253,7254,7255,7256,7257,7258,7259,7260,7261,7262,7263,7264,7265,7266,7267,7268,7269,7270,7271,7272,7273,7274,7275,7276,7277,7278,7279,7280,7281,7282,7283,7284,7285,7286,7287,7288,7289,7290,7291,7292,7293,7294,7295,7296,7297,7298,7299,7300,7301,7302,7303,7304,7305,7306,7307,7308,7309,7310,7311,7312,7313,7314,7315,7316,7317,7318,7319,7320,7321,7322,7323,7324,7325,7326,7327";

    public function __construct()
    {
        echo "Starting: " . date("Y-m-d H:i:s") . " " . PHP_EOL;
        $this->articleDelete = array();
        $this->articleOriginalDataDelete = array();
        $this->articleId = "";
        $this->articleOriginalDataid = "";
        $this->getArticlesData();
        $this->insertArticleOriginalData();
        $this->insertArticle();
        $this->insertImage();
        $this->insertVideo();
        $this->DeleteData();
        echo "End: " . date("Y-m-d H:i:s") . " " . PHP_EOL;
    }

    public function connect()
    {

    }

    public function getArticlesData()
    {
        $uloopid = $this->uloop_id;
        $year = date('Y-m-d H:i:s', strtotime('1 day ago'));
        $q = "select id,article_original_data_id from article "
            . "where title_id in ($uloopid)"
            . "AND  id not in (select article_id from ignore_article) and parsed_at < \"$year\" limit 1000;";
        echo ($q) . PHP_EOL;
        $query = mysql_query($q);
        while ($row = mysql_fetch_assoc($query)) {
            array_push($this->articleOriginalDataDelete, $row['article_original_data_id']);
            array_push($this->articleDelete, $row['id']);
        }
        if (count($this->articleOriginalDataDelete) < 1 && count($this->articleDelete) < 1) {
            echo "No Articles" . PHP_EOL;
            exit;
        }
        $this->articleOriginalDataid = implode(',', $this->articleOriginalDataDelete);
        $this->articleId = implode(',', $this->articleDelete);
    }

    public function insertArticleOriginalData()
    {
        $sql = "";
        $sql = "insert into article_original_data_tmp ("
            . "id,original_article_id,original_source,issue_number,"
            . "page_number,reference,extras,hijri_date,original_cat_id,revision_num)"
            . "  SELECT * from article_original_data where id in ($this->articleOriginalDataid )";
        $result = mysql_query($sql);
        if ($result) {
            echo "INSERT article_original_data_archive successfully" . PHP_EOL;
            $this->checkArticleOriginalData = TRUE;
        } else {
            echo "INSERT article_original_data_archive Failure" . PHP_EOL;
            $this->checkArticleOriginalData = FALSE;
        }
    }

    public function insertArticle()
    {
        $sql = "";
        $sql = "insert into article_tmp ("
            . "id,iptc_id,title_id,article_original_data_id,language_id,headline,summary,body,"
            . "author,date,parsed_at,updated_at,has_time,is_Calias_called,sub_feed)"
            . "  SELECT * from article where id in ($this->articleId)";
        $result = mysql_query($sql);
        if ($result) {
            echo "INSERT article_archive successfully" . PHP_EOL;
            $this->checkArticle = TRUE;
        } else {
            echo "INSERT article_archive  Failure" . PHP_EOL;
            $this->checkArticle = FALSE;
        }
    }

    public function insertImage()
    {
        $sql = "";
        $sql = "insert into  image_tmp ("
            . "id,article_id,img_name,image_caption,is_headline,image_type,"
            . "mime_type,original_name,image_original_key) "
            . "  SELECT * from image where article_id in ($this->articleId)";
        $result = mysql_query($sql);
        if (empty($this->articleId)) {
            $this->checkImage = TRUE;
            echo "No images" . PHP_EOL;
        } else {
            if ($result) {
                echo "INSERT image_archive successfully" . PHP_EOL;
                $this->checkImage = TRUE;
            } else {
                echo "INSERT image_archive Failure" . PHP_EOL;
                $this->checkImage = FALSE;
            }
        }
    }

    public function insertVideo()
    {
        $sql = "";
        $sql = "insert into  video_tmp "
            . "(id,article_id,video_name,video_caption,original_name,video_type,bit_rate,added_time,mime_type)"
            . "  SELECT * from video where article_id in ($this->articleId)";
        $result = mysql_query($sql);
        if (empty($this->articleId)) {
            $this->checkVideo = TRUE;
            echo "No video" . PHP_EOL;
        } else {
            if ($result) {
                echo "INSERT video_archive successfully" . PHP_EOL;
                $this->checkVideo = TRUE;
            } else {
                echo "INSERT video_archive  Failure" . PHP_EOL;
                $this->checkVideo = FALSE;
            }
        }
    }

    public function DeleteData()
    {

        if ($this->checkArticle && $this->checkArticleOriginalData) {
            echo "DELETE ...... " . PHP_EOL;
            mysql_query("DELETE FROM video WHERE article_id in ($this->articleId)");
            mysql_query("DELETE FROM image WHERE article_id in ($this->articleId)");
            mysql_query("DELETE FROM article WHERE id in ($this->articleId)");
            mysql_query("DELETE FROM article_original_data WHERE id in ($this->articleOriginalDataid)");
        } else {
            mysql_query("INSERT INTO ignore_article (article_id)  SELECT id from article where id in ($this->articleId)");
        }
    }

}

// Check if an instance exists from the same scripts
$avilable_instances = 0;
$output = shell_exec('ps aux | grep moveArticlesToArchive2.php | grep -v "/bin/sh -c" | grep -v "sh -c" | grep -v "grep" ');
$output = explode(PHP_EOL, $output);
foreach ($output as $o) {
    if ($o) {
        $avilable_instances++;
    }
}
if ($avilable_instances > 1) {
    die("Job is already running...exit!" . PHP_EOL);
}

for ($i = 0; $i < 2000; $i++) {
    $t = 2000 - $i;
    echo "There are still $t movements ............ " . PHP_EOL;
    $check_ob = new moveArticlesToArchive2();
    sleep(1);
}
