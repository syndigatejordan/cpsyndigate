<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
// Check if an instance exists from the same scripts 
$avilable_instances = 0;
$output = shell_exec('ps aux | grep moveActusnewsSAS.php | grep -v "/bin/sh -c" | grep -v "sh -c" | grep -v "grep" ');
$output = explode(PHP_EOL, $output);
foreach ($output as $o) {
  if ($o) {
    $avilable_instances++;
  }
}

if ($avilable_instances > 1) {
  die();
}

$publiserDirName = '/syndigate/sources/home/1453/';
$directories = scandir($publiserDirName);
foreach ($directories as $file) {
  $path = pathinfo($file);
  if (empty($path['extension'])) {
    continue;
  }

  $path_file = $path['extension'];
  if (($file != '.') && ($file != '..') && (is_file("{$publiserDirName}{$file}")) && strtolower($path_file) == "xml") {
    $text = file_get_contents("{$publiserDirName}{$file}");
    $lang = null;
    if (preg_match('/<communique(.*?)langue="(.*?)">/', $text, $lang)) {
      $lang = $lang[2];
      if ($lang == "fr") {
        $title_id = 7329;
      } else {
        $title_id = 7328;
      }
      $targetDir = "/syndigate/sources/home/1453/" . $title_id . date('/Y/m/d/');
      if (!is_dir($targetDir)) {
        mkdir($targetDir, '0775', true);
      }

      $move = chr(10) . "mv " . $publiserDirName . $file . ' ' . $targetDir;
      echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
      shell_exec($move);
    }
  }
}