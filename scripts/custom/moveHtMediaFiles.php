<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
// Check if an instance exists from the same scripts 
$avilable_instances = 0;
$output = shell_exec('ps aux | grep moveHtMediaFiles.php | grep -v "/bin/sh -c" | grep -v "sh -c" | grep -v "grep" ');
$output = explode(PHP_EOL, $output);
foreach ($output as $o) {
  if ($o) {
    $avilable_instances++;
  }
}

if ($avilable_instances > 1) {
  die();
}
// execluded titles may be disconnected, see title notes
$execludedIds = array(651, 5977, 5978, 6416, 6417, 6418, 6419, 6420, 6421, 6422, 6423, 6424, 6425, 6426,
    6427, 6428, 6429, 6430, 6431, 6432, 6433, 6434, 6435, 6436);

//copyright pattern in the file  ex. <Contact /><Copyright>Accord Fintech</Copyright></Document>
$htmediaTitles = array(
    'Energy Bangla' => array('username' => 'htmedial_enerbang', 'titleId' => 604),
    'Accord Fintech' => array('username' => 'htmedial_accorfin', 'titleId' => 605),
    'Living Digital' => array('username' => 'htmedial_livindig', 'titleId' => 620),
    'CIOL' => array('username' => 'htmedial_ciolhtme', 'titleId' => 625),
    'Garhwal Post' => array('username' => 'htmedial_garhpost', 'titleId' => 627),
    'Sourcing Hardware' => array('username' => 'htmedial_souhardw', 'titleId' => 639),
    'Voice of Sikkim' => array('username' => 'htmedial_voisikht', 'titleId' => 654),
    'Sikkim Express' => array('username' => 'htmedial_sikkexht', 'titleId' => 655),
    'Daily Times' => array('username' => 'htmedial_dailtimh', 'titleId' => 663),
    'Daily Pak Banker' => array('username' => 'htmedial_dailpakb', 'titleId' => 664),
    'Ibex' => array('username' => 'htmedial_ibexhtme', 'titleId' => 672),
    'Sunday Times' => array('username' => 'htmedial_suntimht', 'titleId' => 674),
    'Automotive Products Finder' => array('username' => 'ipfon28_automoti', 'titleId' => 963, 'publisherId' => 281),
    'The Bangladesh Monitor' => array('username' => 'theban28_bangmoni', 'titleId' => 964),
    'Industrial Products Finder' => array('username' => 'ipfon28_industri', 'titleId' => 973),
    'Infocera' => array('username' => 'ljenter9_inforcer', 'titleId' => 974, 'publisherId' => 290),
    'SKOAR!' => array('username' => 'htmedial_skoarhtm', 'titleId' => 644, 'publisherId' => 89),
    'Hindustan Times' => array('username' => 'htmedial_hintimed', 'titleId' => 284, 'publisherId' => 89),
    'Desimartini' => array('username' => 'htmedial_desimart', 'titleId' => 5974, 'publisherId' => 89),
    'Hindustan Delhi' => array('username' => 'htmedial_livhindu', 'titleId' => 5975, 'publisherId' => 89),
    'Behind Woods' => array('username' => 'htmedial_behiwood', 'titleId' => 5976, 'publisherId' => 89),
    'Light Castle' => array('username' => 'htmedial_lightcas', 'titleId' => 5979, 'publisherId' => 89),
    'Bdnews24' => array('username' => 'htmedial_bdnewsen', 'titleId' => 5980, 'publisherId' => 89),
    'Bdnews24 Bangla' => array('username' => 'htmedial_bdnewsbg', 'titleId' => 5981, 'publisherId' => 89),
);

//Moving all the files in the publisher directory to title directory 
$files = getDirectoryFiles('/syndigate/sources/home/89/');
foreach ($files as $file) {
  if (is_file($file)) {
    $fileString = file_get_contents($file);
    foreach ($htmediaTitles as $copyright => $settings) {
      //if( strpos($fileString, "<Contact /><Copyright>$copyright</Copyright></Document>" )) {
      if (strpos($fileString, "</Location><Attribution>$copyright</Attribution>")) {

        $pubId = 89;
        if (isset($settings['publisherId'])) {
          $pubId = $settings['publisherId'];
        }

        $targetDir = "/syndigate/sources/home/$pubId/" . $settings['titleId'] . date('/Y/m/d/');

        if (!is_dir($targetDir)) {
          mkdir($targetDir, '0775', true);
        }
        $newFileName = $targetDir . basename($file);
        rename($file, $newFileName);
        echo "move $file to $newFileName \n";

        shell_exec("chown -R  {$settings['username']}:htmedial $targetDir");
        shell_exec("chmod -R  777 $targetDir");
      }
    }
  }
}




//Moving all root files on title directory to date directory 
$c = new Criteria();
$c->add(TitlePeer::PUBLISHER_ID, 89);
$c->add(TitlePeer::ID, $execludedIds, Criteria::NOT_IN);
$titles = TitlePeer::doselect($c);

$titlesSendingOnTheRoot = array();
$inactiveTitlesNeedsParsers = array();


foreach ($titles as $title) {
  $files = array();
  $files = getDirectoryFiles('/syndigate/sources/home/89/' . $title->getId() . '/');

  if (is_dir('/syndigate/sources/home/89/' . $title->getId() . date('/Y/'))) {
    $yearsFiles = getDirectoryFiles('/syndigate/sources/home/89/' . $title->getId() . date('/Y/'));
    if (count($yearsFiles) > 0) {
      echo "years ";
      print_r($yearsFiles);
    }
  }

  if (is_dir('/syndigate/sources/home/89/' . $title->getId() . date('/Y/m/'))) {
    $monthsFiles = getDirectoryFiles('/syndigate/sources/home/89/' . $title->getId() . date('/Y/m/'));
    if (count($monthsFiles) > 0) {
      echo "years ";
      print_r($monthsFiles);
    }
  }


  if (is_array($yearsFiles) && count($yearsFiles) > 0) {
    $files = array_merge($files, $yearsFiles);
  }
  if (is_array($monthsFiles) && count($monthsFiles) > 0) {
    $files = array_merge($files, $monthsFiles);
  }


  if (($title->getIsActive() == 0) && (count(getDirectoryContent('/syndigate/sources/home/89/' . $title->getId() . '/')) > 0)) {
    $inactiveTitlesNeedsParsers[] = $title->getId();
  }

  if (count($files) > 0) {
    $targetDir = '/syndigate/sources/home/89/' . $title->getId() . date('/Y/m/d/');


    foreach ($files as $file) {
      if (is_file($file)) {

        if (!is_dir($targetDir)) {
          mkdir($targetDir, '0775', true);
        }

        $newFileName = $targetDir . basename($file);
        rename($file, $newFileName);

        $titlesSendingOnTheRoot[$title->getId()] = 'on';
      }
    }

    if (is_dir($targetDir)) {
      shell_exec("chown -R  {$title->getFtpUsername()}:htmedial $targetDir");
      shell_exec("chmod -R  775 $targetDir");
    }
  }
}

echo "titles that are sending on incorrect directory : \n";
print_r($titlesSendingOnTheRoot);
echo "inactive titles that are sending : \n";
print_r($inactiveTitlesNeedsParsers);
if (count($inactiveTitlesNeedsParsers) > 0) {
  //sendEmail(print_r($inactiveTitlesNeedsParsers, true));
}

// functions -----------
function getDirectoryFiles($dir) {
  $files = array();

  if ($handle = opendir($dir)) {
    while (false !== ($file = readdir($handle))) {
      if (($file != '.') && ($file != '..')) {
        if (is_file($dir . $file)) {
          $files[] = $dir . $file;
        }
      }
    }
    closedir($handle);
    return $files;
  } else {
    echo "invalid directory $dir \n";
    return false;
  }
}

function getDirectoryContent($dir) {
  $files = array();

  if ($handle = opendir($dir)) {
    while (false !== ($file = readdir($handle))) {
      if (($file != '.') && ($file != '..')) {
        $files[] = $dir . $file;
      }
    }
    closedir($handle);
    return $files;
  } else {
    echo "invalid directory $dir \n";
    return false;
  }
}

function sendEmail($body) {
  $mailTo = array('eyad@syndigate.info');
  //$mailTo = array('yousef@corp.albawaba.com');
  $mail = new ezcMailComposer();
  $mail->from = new ezcMailAddress('report@syndigate.info', 'syndigate ht media report');

  foreach ($mailTo as $to) {
    $mail->addTo(new ezcMailAddress($to));
  }

  $mail->subject = "Syndigate Htmedia checker ";
  $mail->htmlText = "Hi, this is automates checker for inactive htmedia titles that sends content \n" . $body . " \n\n Please do not trust this email %100 but always check the server.";


  $mail->build();
  $transport = new ezcMailMtaTransport();
  $transport->send($mail);
}
