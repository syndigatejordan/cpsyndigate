<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';


$newsMlDir = '/syndigate/clients/49/' . date('Y/m/d/');
$outputDir =  '/syndigate/rss/';

$files = scandir($newsMlDir);
foreach ($files as $file) {
	if($file !='.' && $file != '..') {
		generateRSS($newsMlDir . $file);

	}
}

function generateRSS ($newsMl) {
$fileContents = @file_get_contents($newsMl);

$xml     = simplexml_load_string($fileContents);
$rssFile = '<?xml version="1.0" encoding="UTF-8"?>
			<rss version="2.1">
				<channel>';

$titleId     = str_replace('.xml','', basename($newsMl));
$titleInfo   = TitlePeer::retrieveByPK($titleId);

$title       = $titleInfo->getName();
$link        = $titleInfo->getWebsite();
$description = $titleInfo->getSourceDescription();

$languageId    = $titleInfo->getLanguageId();
$languageInfo  = LanguagePeer::retrieveByPK($languageId);
$language      = $languageInfo->getName();

$rssFile .= "<title>$title</title>
			 <link>$link</link>
			 <description>$description</description>
			 <language>$language</language>";

foreach ($xml->NewsItem->NewsComponent as $article) {
	$title = get_object_vars($article->NewsLines);
	$title = $title['HeadLine'];
	$title = strip_tags($title);
	$title = str_replace("\n", '', $title);
	
	$description = $article->NewsComponent->ContentItem;
	$description = get_object_vars($description[1]);
	$description = strip_tags($description['DataContent'],'');
	$description = str_replace("\n", '', $description);
	
	$link = '';
	$guid = '';
	
	$pubDate = get_object_vars($article->DescriptiveMetadata);
	$pubDate = get_object_vars($pubDate['Property'][5]);
	$pubDate = $pubDate['@attributes']['Value'];
	
	$date    = explode('-', $pubDate);
	$pubDate = date('D, d M Y', mktime(0, 0, 0, $date[1], $date[2], $date[0]));

	$rssFile .= "<item>
					<title>$title</title>
					<description>$description</description>
					<link>$link</link>
					<guid>$guid</guid>
					<pubDate>$pubDate</pubDate>
				</item>";
}

$rssFile .=' </channel>
			</rss>'; 

$rssFile = str_replace('&', '&amp;', $rssFile);
file_put_contents("/syndigate/rss/" . basename($newsMl), $rssFile);

}
