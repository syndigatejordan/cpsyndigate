<?php 
require_once '/usr/local/syndigate/scripts/propel_include_config.php';
//require_once '/home/yousef/syndigate_svn/syndigate/scripts/propel_include_config.php';

//254 test title
$excludedtitlesIdsFromGather = array(254);

//Some fixation before gathering
shell_exec("php /usr/local/syndigate/scripts/custom/bbc_content.php");
shell_exec("php /usr/local/syndigate/scripts/custom/moveHtMediaFiles.php");



$c = new Criteria();
$c->add(TitlePeer::IS_ACTIVE,1);
$c->add(TitlePeer::TITLE_FREQUENCY_ID, 1);

$hour = date('H');
if(($hour % 3) != 0) {
	$c->add(TitlePeer::CACHED_ARTICLES_TODAY, 0, Criteria::NOT_EQUAL);
}
gatherTitles(TitlePeer::doSelect($c));



if(($hour % 4) == 0) {
	$c = new Criteria();
	$c->add(TitlePeer::IS_ACTIVE,1);
	gatherTitles(TitlePeer::doSelect($c));
}

echo PHP_EOL ."finishd " . PHP_EOL;


/// ---- Functions -------////

function gatherTitles($titles) {
	global $excludedtitlesIdsFromGather;
	
	foreach ($titles as $title) {
	
		if(!in_array($title->getId(), $excludedtitlesIdsFromGather)) {
			echo "gathering title " . $title->getId() . PHP_EOL ;
			$rez = shell_exec ("/usr/local/syndigate/classes/gather/syndGather.php " . $title->getId());
			$excludedtitlesIdsFromGather[] = $title->getId();
			echo $rez . PHP_EOL;  	
		}
	
	}
	echo PHP_EOL;
}

