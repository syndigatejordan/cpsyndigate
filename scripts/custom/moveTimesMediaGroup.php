<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

/*
 * '' => array('Title'=>'','Edition'=>''),
 */
$timesMediaGroupTD = array(
    '2383' => array('Title' => 'FM', 'Edition' => array('Financial Mail', 'Surveys', 'Budget', 'Investors Monthly')),
    '2384' => array('Title' => 'The Times', 'Edition' => array('The Times', 'Surveys')),
    '2385' => array('Title' => 'BD', 'Edition' => array('Business Day', 'Motor News', 'Surveys', 'Law Review', '', 'Budget')),
    '2386' => array('Title' => 'Daily Dispatch', 'Edition' => array('Daily Dispatch', 'saturday dispatch', 'Supplement')),
    '2387' => array('Title' => 'Herald', 'Edition' => array('Mainbody', 'Surveys')),
    '2388' => array('Title' => 'ST', 'Edition' => array('MainBody', 'Business Times', 'Fashion Weekly', 'LifeStyle', 'Sport', 'TV Magazine', 'Travel Weekly', 'ExpressMain', 'Review', 'Surveys', 'Motoring', 'Home Weekly', 'Lifestyle Express', 'Top Brands')),
    '2389' => array('Title' => 'Sowetan', 'Edition' => array('Sowetan', 'Surveys')),
    '2390' => array('Title' => 'Sunday World', 'Edition' => 'Sunday World'),
    '2391' => array('Title' => 'Weekend', 'Edition' => array('Mainbody', 'Property', 'Surveys')),
    '2392' => array('Title' => 'Picasso', 'Edition' => 'Go!&#x0026;Express'),
    '2393' => array('Title' => 'Picasso', 'Edition' => 'talktown'),
    '2394' => array('Title' => 'Picasso', 'Edition' => 'the representative'),
);
//Fix File name
$publiserDirName = '/syndigate/sources/home/767/';
$directories = scandir($publiserDirName);
foreach ($directories as $File) {
  $path = pathinfo($File);
  if (empty($path['extension'])) {
    continue;
  }
  $path_file = $path['extension'];
  if (($File != '.') && ($File != '..') && (is_file("{$publiserDirName}{$File}")) && strtolower($path_file) == "xml") {
    $path["basename"] = preg_replace("/[  ~&,:\-\)\(\']/", ' ', $path["basename"]);
    $path["basename"] = str_replace('..', '.', $path["basename"]);
    $path["basename"] = str_replace("'", "\'", $path["basename"]);
    $path["basename"] = str_replace("\'", "", $path["basename"]);
    $path["basename"] = str_replace("`", "\`", $path["basename"]);
    $path["basename"] = str_replace("\`", "", $path["basename"]);
    $path["basename"] = str_replace("\$", "\\$", $path["basename"]);
    $path["basename"] = str_replace("\$", "", $path["basename"]);
    $path["basename"] = str_replace('  ', ' ', $path["basename"]);
    $path["basename"] = preg_replace('!\s+!', '_', $path["basename"]);
    $path["basename"] = str_replace('._', '_', $path["basename"]);
    $path["basename"] = str_replace(' _', '_', $path["basename"]);
    $File = str_replace(' ', '\ ', $File);
    $File = str_replace('(', '\(', $File);
    $File = str_replace(')', '\)', $File);
    $File = str_replace("'", "\'", $File);
    $File = str_replace("`", "\`", $File);
    $File = str_replace("&", "\&", $File);
    $File = str_replace("\$", "\\$", $File);
    $move = "mv " . $publiserDirName . $File . " " . $publiserDirName . $path["basename"];
    if ($File != $path["basename"]) {
      echo $move . chr(10);
      shell_exec($move);
    }
  }
}
//Moving all the files in the publisher directory to title directory 
$pubId = 767;
$files = getDirectoryFiles($publiserDirName);
foreach ($files as $file) {
  if (is_file($file)) {
    $filetype = pathinfo($file);
    $fileString = file_get_contents($file);

    preg_match('/<Title>(.*?)<\/Title>/s', $fileString, $Title);
    preg_match('/<Edition>(.*?)<\/Edition>/s', $fileString, $Edition);
    if (count($Title) > 1) {
      $Title = trim(strtolower($Title[1]));
    }
    if (count($Edition) > 1) {
      $Edition = trim(strtolower($Edition[1]));
    }
    foreach ($timesMediaGroupTD as $title_id => $source) {
      if (is_array($source["Edition"])) {
        foreach ($source["Edition"] as $sEdition) {
          $sTitle = $source["Title"];
          if ($Title == strtolower($sTitle) && $Edition == strtolower($sEdition)) {

            $targetDir = "/syndigate/sources/home/$pubId/" . $title_id . date('/Y/m/d/');
            if (!is_dir($targetDir)) {
              mkdir($targetDir, '0775', true);
            }
            $move = "mv " . $file . ' ' . $targetDir;
            echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
            shell_exec($move);
          }
        }
      } else {
        $sTitle = $source["Title"];
        $sEdition = $source["Edition"];
        if ($Title == strtolower($sTitle) && $Edition == strtolower($sEdition)) {

          $targetDir = "/syndigate/sources/home/$pubId/" . $title_id . date('/Y/m/d/');
          if (!is_dir($targetDir)) {
            mkdir($targetDir, '0775', true);
          }
          $move = "mv " . $file . ' ' . $targetDir;
          echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
          shell_exec($move);
        }
      }
    }
  }
}

// functions -----------
function getDirectoryFiles($dir) {
  $files = array();
  if ($handle = opendir($dir)) {
    while (false !== ($file = readdir($handle))) {
      if (($file != '.') && ($file != '..')) {
        if (is_file($dir . $file)) {
          $files[] = $dir . $file;
        }
      }
    }
    closedir($handle);
    return $files;
  } else {
    echo "invalid directory $dir \n";
    return false;
  }
}

function getDirectoryContent($dir) {
  $files = array();
  if ($handle = opendir($dir)) {
    while (false !== ($file = readdir($handle))) {
      if (($file != '.') && ($file != '..')) {
        $files[] = $dir . $file;
      }
    }
    closedir($handle);
    return $files;
  } else {
    echo "invalid directory $dir \n";
    return false;
  }
}
