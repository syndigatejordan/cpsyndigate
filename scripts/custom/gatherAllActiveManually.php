<?php 
require_once '/usr/local/syndigate/scripts/propel_include_config.php';
//require_once '/home/yousef/syndigate_svn/syndigate/scripts/propel_include_config.php';

//254 test title
$excludedtitlesIdsFromGather = array(254);

//Some fixation before gathering
shell_exec("php /usr/local/syndigate/scripts/custom/bbc_content.php");
shell_exec("php /usr/local/syndigate/scripts/custom/moveHtMediaFiles.php");


//also execulding titles in cms 
$cmsCon = mysql_connect($dbConf['hostspec'], $dbConf['username'], $dbConf['password']) or die('Can not connect to mysql');
$db     = mysql_select_db('cms') OR die('Can not select database');
$query = "SELECT DISTINCT title_id FROM publications_titles";
$rez = mysql_query($query, $cmsCon);
while($row = mysql_fetch_assoc($rez)) {
	$excludedtitlesIdsFromGather [] = $row['title_id'] ;
}





$c = new Criteria();
$c->add(TitlePeer::IS_ACTIVE,1);
$c->add(TitlePeer::ID, $excludedtitlesIdsFromGather, Criteria::NOT_IN);

$hour = date('H');
if(($hour % 3) != 0) {
	$c->add(TitlePeer::CACHED_ARTICLES_TODAY, 0);
	//$c->add(TitlePeer::TITLE_FREQUENCY_ID, 1);
}
gatherTitles(TitlePeer::doSelect($c));



echo PHP_EOL ."finished " . PHP_EOL;


/// ---- Functions -------////

function gatherTitles($titles) {
	global $excludedtitlesIdsFromGather;
	
	global $excludedtitlesIdsFromGather;
	$children = array();
	$counter  = 0;

	for ($counter =0; $counter< count($titles); $counter++) {
		
		$pid = -1;
		$counter++;		

		if ( count($children)>= 10 ) {
			sleep(30); 
			echo "sleeping for 3 scs, count is " . count($children) . PHP_EOL;			
			$counter--;
		} else {
			$pid = pcntl_fork();
		}
		
		if($pid == -1) {
			// Do notinh 
		} elseif($pid == 0) {
			
			$title = $titles[$counter];
			if(!in_array($title->getId(), $excludedtitlesIdsFromGather)) {
		
				echo "gathering title " . $title->getId() . PHP_EOL ;
				$rez = shell_exec ("/usr/local/syndigate/classes/gather/syndGather.php " . $title->getId());
				$excludedtitlesIdsFromGather[] = $title->getId();
				echo $rez . PHP_EOL;  					
				
			}
			exit();
		} else {
			$children [] = $pid;			
		}


		// When children die, this gets rid of the zombies
	        while(pcntl_wait($status, WNOHANG OR WUNTRACED) > 0) {
	        	usleep(5000);
	        }

	        while(list($key, $val) = each($children)) {

	          if(!posix_kill($val, 0)) { // This detects if the child is still running or not
	                unset($children[$key]);
	          }
	        }

	        $children = array_values($children); // Reindex the array

	}
				
	echo PHP_EOL;
}
