<?php
require_once '/usr/local/syndigate/scripts/propel_include_config.php';
// Check if an instance exists from the same scripts
$avilable_instances = 0;
$output = shell_exec('ps aux | grep moveInfomedia.php | grep -v "/bin/sh -c" | grep -v "sh -c" | grep -v "grep" ');
$output = explode(PHP_EOL, $output);
foreach ($output as $o) {
    if ($o) {
        $avilable_instances++;
    }
}

if ($avilable_instances > 1) {
    die();
}
$titles = array();
$data = mysql_query("select t.id,t.name,l.code from title t left join language l on t.language_id=l.id where publisher_id=1534 order by t.id asc;");
while ($title = mysql_fetch_array($data)) {
    $titles[$title["code"]]["id"] = $title["id"];
    $titles[$title["code"]]["name"] = trim(strtolower($title["name"]));
    $titles[$title["code"]]["code"] = trim(strtolower($title["code"]));
}
//print_r($titles);
$publiserDirName = '/syndigate/sources/home/1534/';
$directories = scandir($publiserDirName."dump/");

foreach ($directories as $file) {
    $path = pathinfo($file);
    if (empty($path['extension'])) {
        continue;
    }
    $path_file = $path['extension'];
    if (($file != '.') && ($file != '..') && (is_file("{$publiserDirName}dump/{$file}")) && strtolower($path_file) == "json") {
        $fileContent = file_get_contents("{$publiserDirName}dump/{$file}");
        $content = json_decode($fileContent, true);
        getArrayArticles($content["searchresult"]["document"],$titles);
        $move = "rm {$publiserDirName}dump/{$file}";
        echo "Remove file " . $move . chr(10);
        shell_exec($move);
    }
}


 function getArrayArticles($elements,$titles) {
    foreach ($elements as $element) {
        $language = $element["language"]["text"];
        $id_site = $element["id_site"];
        $id_article = $element["id_article"];
        $unix_timestamp = $element["unix_timestamp"];
        if (isset($titles[$language])) {
            echo "Get article id $id_article" . PHP_EOL;
            $article["article"] = $element;
            // creating object of SimpleXMLElement
            $xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');

            // function call to convert array to xml
            array_to_xml($article, $xml_data);
            $directory = "/syndigate/sources/home/1534/{$titles[$language]["id"]}/";
            //saving generated xml file;
            $result = $xml_data->asXML("$directory/{$id_site}_{$id_article}_{$unix_timestamp}.xml");

        } else {
            echo "not found language $language".PHP_EOL;
            $directory = "/syndigate/sources/home/1534/$language/";
            if (!is_dir($directory)) {
                mkdir($directory, '0775', true);
            }
            $article["article"] = $element;
            // creating object of SimpleXMLElement
            $xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');
            // function call to convert array to xml
            array_to_xml($article, $xml_data);
            //saving generated xml file;
            $result = $xml_data->asXML("$directory/{$id_site}_{$id_article}_{$unix_timestamp}.xml");
        }
    }
}

 function array_to_xml($data, &$xml_data) {
    foreach ($data as $key => $value) {
        if (is_numeric($key)) {
            $key = 'item' . $key; //dealing with <0/>..<n/> issues
        }
        if (is_array($value)) {
            $subnode = $xml_data->addChild($key);
            array_to_xml($value, $subnode);
        } else {
            $xml_data->addChild("$key", htmlspecialchars("$value"));
        }
    }
}