<?php
require_once '/usr/local/syndigate/scripts/propel_include_config.php';
 
mysql_query("DELETE FROM content_sources WHERE is_org_from_syndigate=1", $conn);
$criteria = new Criteria();
$criteria->add(TitlePeer::PUBLISHER_ID,602,  Criteria::NOT_IN);
$titles = TitlePeer::doSelect($criteria);

foreach ($titles as $title) {
	$content 		= new ContentSources();
	
	$titleName		=	$title->getName();
	$titleId		=	$title->getId();
	
	$countryObj 	=	CountryPeer::retrieveByPK($title->getCountryId());
	$countryName 	=	is_object($countryObj) ? $countryObj->getPrintableName():'';
	
	$cityName    	= 	$title->getCity();
	
	$titleTypeObj   =	TitleTypePeer::retrieveByPK($title->getTitleTypeId());
	$titleType		=	is_object($titleTypeObj) ? $titleTypeObj->getType():'';
	
	$languageObj	=	LanguagePeer::retrieveByPK($title->getLanguageId());
	$language		=	is_object($languageObj) ? $languageObj->getName(): '';
	
	$titleFreqObj	=	TitleFrequencyPeer::retrieveByPK($title->getTitleFrequencyId());
	$titleFreq		=	is_object($titleFreqObj) ? $titleFreqObj->getFrequencytype(): '';
	
	$publisherObj	=	PublisherPeer::retrieveByPK($title->getPublisherId());
	$publisherName	=	is_object($publisherObj) ? $publisherObj->getName(): '';	
	
	
	$content->setTitle($titleName);
	$content->setTitleId($titleId);
	$content->setSourceDescription($title->getSourceDescription());
	
	$content->setCountry($countryName);
	$content->setCity($cityName);
	
	$content->setType($titleType);
	$content->setLanguage($language);
	
	$content->setFrequancy($titleFreq);
	$content->setPublisher($publisherName);
	$content->setIsOrgFromSyndigate(1);
	
	$content->save();
	
}
