<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

$files = array();
$storageTitle = '/syndigate/sources/home/62/192/';
$targetTitle = '/syndigate/sources/home/62/193/';
$directories = scandir($storageTitle);
$hour = 1;
$articleNum = 4000;
foreach ($directories as $file) {
  $path = pathinfo($file);
  if (empty($path['extension'])) {
    continue;
  }

  $path_file = $path['extension'];
  if (($file != '.') && ($file != '..') && (is_file("{$storageTitle}{$file}")) && strtolower($path_file) == "xml") {
      echo "Start wit file $file" . PHP_EOL;
      $text = file_get_contents("{$storageTitle}{$file}");
      $rows = getElementsByName("Row", $text);
      if (count($rows) < $articleNum) {
          $targetDir = $storageTitle . date("Y/m/d/");
          if (!is_dir($targetDir)) {
              mkdir($targetDir, '0775', true);
          }
          $targetFile = $targetDir . $path['filename'] . date('H', strtotime("+$hour hour")) . ".xml";
          $hour++;
          $move = "mv $storageTitle$file $targetFile";
          echo "Moving the file  to target directory " . $move . PHP_EOL;
          exec($move);
      } else {
          $batch = (int)ceil(count($rows) / $articleNum);
          for ($i = 0; $i < $batch; $i++) {
              echo "Start batch $i" . PHP_EOL;
              $newfile = '<?xml version="1.0" encoding="UTF-8"?> <Import>';
              $start = $articleNum * $i;
              for ($start; $start < ($articleNum * ($i + 1)); $start++) {
                  if (isset($rows[$start])) {
                      $newfile .= "<Row>";
                      $newfile .= $rows[$start];
                      $newfile .= "</Row>";
                  }
              }
              $newfile .= "</Import>";

              $targetDir = $storageTitle . date("Y/m/d/");
              if (!is_dir($targetDir)) {
                  mkdir($targetDir, '0775', true);
              }
              $targetFile = $targetDir . $path['filename'] . date('H', strtotime("+$hour hour")) . ".xml";
              $hour++;
              file_put_contents($targetFile, $newfile);
          }
      $move = "rm {$storageTitle}{$file}";
          echo "Remove file " . $move . PHP_EOL;
      exec($move);
      $hour++;
    }
  }
}

function getElementsByName($name, $text) {
  $openTagRegExp = '<' . $name . '([^\>]|[\s])*>';
  $closeTagRegExp = '<\/' . $name . '[\s]*>';
  $elements = preg_split("/$openTagRegExp/i", $text);

  $elementsContents = array();
  $elementsCount = count($elements);
  for ($i = 1; $i < $elementsCount; $i++) {
    $element = preg_split("/$closeTagRegExp/i", $elements[$i]);
    $elementsContents[] = $element[0];
  }
  return $elementsContents;
}

function getElementByName($name, $text) {
  $element = getElementsByName($name, $text);
  return (isset($element[0]) ? $element[0] : null);
}
