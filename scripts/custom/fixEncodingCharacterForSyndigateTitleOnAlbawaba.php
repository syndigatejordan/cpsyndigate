<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once dirname(__FILE__) . '/../../ez_autoload.php';
require_once '/usr/local/syndigate/scripts/propel_include_config.php';
require_once '/usr/local/syndigate/conf/pheanstalk.conf.php';

$title_id = $argv[1];
$article_ids = explode(',', $argv[2]);

//get Accespted titles 
$acceptedTitlesIds = array();
$acceptedTitles = DrupalIdsPeer::doselect(new Criteria());
foreach ($acceptedTitles as $title) {
  $acceptedTitlesIds[] = $title->getTitleId();
}//var_dump($acceptedTitlesIds);

if (!in_array((int) $title_id, $acceptedTitlesIds)) {
  echo 'Title id is not in the accepted ids ... exiting job' . PHP_EOL;
  return true;
} else {
  foreach ($article_ids as $id) {
    $pheanstalk = new Pheanstalk(PHEANSTALK_HOST . ':' . PHEANSTALK_PORT);
    $jobData = json_encode(array(
        'articles_ids' => $id,
        'title_id' => $title_id,
        'timestamp' => time(),
            )
    );
    $pheanstalk->useTube(POST_PROCESS_ARTICLES_TUBE)->put($jobData, 100);
    echo 'successfully add job' . PHP_EOL;
  }
}

