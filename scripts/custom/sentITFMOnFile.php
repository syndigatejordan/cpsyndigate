<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

class sentITFMOnFile {

  function get_content() {
    $date = date('Y-m-d', strtotime('0 day'));
    //$date = '2015-09-22';
    //$date2 = date('Y-m-d', strtotime('-1 day'));
    //$result = mysql_query("select * from article where title_id=2343 and date(parsed_at) ='$date' order by id asc; #BETWEEN '$date2' and '$date' order by id asc");
    $result = mysql_query("select * from article where title_id=2343 and date(parsed_at)='$date' order by id asc");
    chdir(dirname(__file__));
    if (!$result)
      mail("eyad@syndigate.info", "ITFM Error", mysql_error() . "\n$query");

    $file = "fileITFMArticles.txt";
    $englishfile = fopen($file, "w");
    $output = "";
    while ($row = mysql_fetch_assoc($result)) {

      $row['date'] = date('F d, Y', strtotime($row['date']));
      $row['body'] = preg_replace('/(?:\s\s+)/', ' ', $row['body']);

      $row['body'] = preg_replace('/<\/p>/', "\r\n", $row['body']);
      $row['body'] = preg_replace("/<(.+?)[\s]*\/?[\s]*>/si", " ", $row['body']);
      $row['body'] = preg_replace("/(?<!\\n)\\r+(?!\\n)/", "\r\n", $row['body']); //replace just CR with CRLF 
      $row['body'] = preg_replace("/(?<!\\r)\\n+(?!\\r)/", "\r\n", $row['body']); //replace just LF with CRLF 
      $row['body'] = preg_replace("/(?<!\\r)\\n\\r+(?!\\n)/", "\r\n", $row['body']); //replace misordered LFCR with CRLF

      $row['headline'] = preg_replace('/(?:\s\s+)/', ' ', $row['headline']);
      $output = "Headline: " . $row["headline"] . "\r\n" . "\r\n";
      if (!empty($row["summary"])) {
        $output .= "summary: " . $row["summary"] . "\r\n";
      }
      if (!empty($row["author"])) {
        $output .= "Author: " . $row["author"] . "\r\n";
      }
      $output .= "Date: " . $row["date"] . "\r\n" . "\r\n  Body: " . $row["body"] . "\r\n" . "____________________" . "\r\n " . "\r\n";
      fwrite($englishfile, $output);
    }

    fclose($englishfile);
    if (empty($output)) {
      return FALSE;
    } else {
      return TRUE;
    }
  }

  function send_content() {
    $mailTo = array(
        ////////////////////////////Alexandra Wijmenga-Daniel////////////////////////////////////////////////
        /*array('Email' => 'mark@syndigate.info', 'User' => 'Alexandra Wijmenga-Daniel'),
        array('Email' => 'khaled@syndigate.info', 'User' => 'Alexandra Wijmenga-Daniel'),
        array('Email' => 'eyad@syndigate.info', 'User' => 'Alexandra Wijmenga-Daniel'),
        array('Email' => 'majdi@syndigate.info', 'User' => 'Alexandra Wijmenga-Daniel'),
        array('Email' => 'safaa@syndigate.info', 'User' => 'Alexandra Wijmenga-Daniel'),
        array('Email' => 'peskin1doron@gmail.com', 'User' => 'Alexandra Wijmenga-Daniel'),
        array('Email' => 'Alexandra.WIJMENGA-DANIEL@fatf-gafi.org', 'User' => 'Alexandra Wijmenga-Daniel'),*/
        ////////////////////////// Dow Jones  ////////////////////////////////////
        /*array('Email' => 'eyad@syndigate.info', 'User' => 'Dow Jones'),
        array('Email' => 'mark@syndigate.info', 'User' => 'Dow Jones'),
        array('Email' => 'majdi@syndigate.info', 'User' => 'Dow Jones'),
        array('Email' => 'safaa@syndigate.info', 'User' => 'Dow Jones'),
        array('Email' => 'peskin1doron@gmail.com', 'User' => 'Dow Jones'),
        array('Email' => 'gem.conn@dowjones.com', 'User' => 'Dow Jones'),
        array('Email' => 'amanda.lau@dowjones.com', 'User' => 'Dow Jones'),
        array('Email' => 'Kim.Patrick@dowjones.com', 'User' => 'Dow Jones'),
        array('Email' => 'david.hodgson@dowjones.com', 'User' => 'Dow Jones'),
        array('Email' => 'anna.bhreatnach@dowjones.com', 'User' => 'Dow Jones'),*/
    );
    /* $mailTo = array(
      ////////////////////////////Alexandra Wijmenga-Daniel////////////////////////////////////////////////
      array('Email' => 'eyad@syndigate.info', 'User' => 'Alexandra Wijmenga-Daniel'),
      ////////////////////////// Dow Jones  ////////////////////////////////////
      array('Email' => 'eyad@syndigate.info', 'User' => 'Dow Jones'),
      ); */
    foreach ($mailTo as $to) {
      $f = 'fileITFMArticles.txt';
      $user = $to["User"];
      $email = $to["Email"];
      $mail = new ezcMailComposer();
      $mail->from = new ezcMailAddress('content@syndigate.info', 'Your daily update from Islamic Terrorist Financing Monitor (ITFM)');
      $mail->addTo(new ezcMailAddress($email));
      $mail->subject = " Your daily update from Islamic Terrorist Financing Monitor (ITFM)";
      $mail->htmlText = "Dear $user,<br />

<p>Please find attached a file which includes all of today’s stories from Islamic Terrorist Financing Monitor (ITFM).</p>

<p>For any questions, please contact Mark Gatty Saunt, Director of Content Sales & Licensing at SyndiGate, via mark@syndigate.info or on +44 7446 895929.</p>

<p>Thank you very much,</p>

<p>The SyndiGate team.</p>"; //$mail->subject;
      $mail->addAttachment("/usr/local/syndigate/scripts/custom/{$f}", null, "text/plain", "text/plain");

      $mail->build();

      $transport = new ezcMailMtaTransport();
      $transport->send($mail);
      echo "Sent  $email\n\n";
      unset($mail);
    }
  }

}

$file = new sentITFMOnFile;
$checkIfEmpty = $file->get_content();
if ($checkIfEmpty) {
  $file->send_content();
}