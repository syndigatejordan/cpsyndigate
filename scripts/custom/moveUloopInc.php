<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
// Check if an instance exists from the same scripts 
$avilable_instances = 0;
$output = shell_exec('ps aux | grep moveUloopInc.php | grep -v "/bin/sh -c" | grep -v "sh -c" | grep -v "grep" ');
$output = explode(PHP_EOL, $output);
foreach ($output as $o) {
  if ($o) {
    $avilable_instances++;
  }
}

if ($avilable_instances > 1) {
  die();
}
$titles = array();
$data = mysql_query(" select id,name,city from title where publisher_id=1356 order by id asc;");
while ($title = mysql_fetch_array($data)) {
  $key = trim(strtolower($title["name"])) . trim(strtolower($title["city"]));
  $titles[$key]["id"] = $title["id"];
  $titles[$key]["name"] = trim(strtolower($title["name"]));
  $titles[$key]["city"] = trim(strtolower($title["city"]));
}
//var_dump($titles);
$publiserDirName = '/syndigate/sources/home/1356/';
$directories = scandir($publiserDirName);
foreach ($directories as $file) {
  $path = pathinfo($file);
  if (empty($path['extension'])) {
    continue;
  }

  $path_file = $path['extension'];
  if (($file != '.') && ($file != '..') && (is_file("{$publiserDirName}{$file}")) && strtolower($path_file) == "xml") {
    $text = file_get_contents("{$publiserDirName}{$file}");
    $items = getElementsByName("items", $text);
    foreach ($items as $item) {
      $pub = trim(strtolower(getCData(getElementByName("PUB", $item))));
      $city = trim(strtolower(getCData(getElementByName("CITY", $item))));
      $link = trim(strtolower(getCData(getElementByName("LINK", $item))));
      if (isset($titles["$pub$city"])) {
          $id = $titles["$pub$city"]["id"];
          $filename = sha1(trim($item)) . ".xml";
          $deatdir = date('/Y/m/d/', strtotime("now"));
          $targetDir = $publiserDirName . $id . $deatdir;
          if (!is_dir($targetDir)) {
              mkdir($targetDir, '0775', true);
          }
          echo "creat new file $targetDir$filename" . PHP_EOL;
          $fp = fopen("$targetDir$filename", 'w');
          $star = '<?xml version="1.0" encoding="UTF-8"?><item>';
          $end = '</item>';
          fwrite($fp, $star . $item . $end);   // here it will print the array pretty
          fclose($fp);
      } else {
          echo "Not found title $pub, $city" . PHP_EOL;
          $filename = sha1(trim($item)) . ".xml";
          echo "creat new file {$publiserDirName}not_process/$filename" . PHP_EOL;
          /*$fp = fopen("{$publiserDirName}not_process/$filename", 'w');
          $star = '<?xml version="1.0" encoding="UTF-8"?><item>';
          $end = '</item>';
          fwrite($fp, $star . $item . $end);   // here it will print the array pretty
          fclose($fp);*/
          sendEmail($item, $pub, $city, $link, $filename);
          //exit;
      }
    }
    $move = "rm {$publiserDirName}{$file}";
    echo "Remove file " . $move . chr(10);
    shell_exec($move);
  }
}

// functions -----------
function getDirectoryFiles($dir) {
  $files = array();
  if ($handle = opendir($dir)) {
    while (false !== ($file = readdir($handle))) {
      if (($file != '.') && ($file != '..')) {
        if (is_file($dir . $file)) {
          $files[] = $dir . $file;
        }
      }
    }
    closedir($handle);
    return $files;
  } else {
    echo "invalid directory $dir \n";
    return false;
  }
}

function getElementByName($name, $text) {
  $element = getElementsByName($name, $text);
  return (isset($element[0]) ? $element[0] : null);
}

function getElementsByName($name, $text) {
  $openTagRegExp = '<' . $name . '([^\>]|[\s])*>';
  $closeTagRegExp = '<\/' . $name . '[\s]*>';
  $elements = preg_split("/$openTagRegExp/i", $text);

  $elementsContents = array();
  $elementsCount = count($elements);
  for ($i = 1; $i < $elementsCount; $i++) {
    $element = preg_split("/$closeTagRegExp/i", $elements[$i]);
    $elementsContents[] = $element[0];
  }
  return $elementsContents;
}

function getCData($text) {
  ini_set("pcre.recursion_limit", "10000");
  $regExp = '<!\[CDATA\[((.*|\s*)*)\]\]>';
  if (preg_match("/$regExp/i", $text, $matches)) {
    return $matches[1];
  } else {
    return $text;
  }
}

// ------ functions

function sendEmail($item, $pub, $city, $link, $filename)
{
    $body = "<p>Title: $pub</p>";
    $body .= "<p>Link: $link</p>";
    $body .= "<p>City: $city</p>";
    $body .= "<p>Filename: $filename</p>";
    $body .= "<p>Body:  <pre><code>$item</code></pre></p>";
    $body = wordwrap($body, 75, "\r\n");
    $mailTo = array('eyad@syndigate.info', 'majdi@syndigate.info');
    // $mailTo = array('eyad@syndigate.info');
    $mail = new ezcMailComposer();
    $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply');

    foreach ($mailTo as $to) {
        $mail->addTo(new ezcMailAddress($to));
    }

  $mail->subject = "publisher Uloop Inc send title not map";
  $mail->htmlText = $body;


  $mail->build();
  $transport = new ezcMailMtaTransport();
  $transport->send($mail);
}
