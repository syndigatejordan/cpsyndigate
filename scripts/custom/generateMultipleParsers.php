<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

$titles = getTitles();
foreach ($titles as $title) {
  echo "Set parser for title id {$title["title_id"]}" . PHP_EOL . PHP_EOL;
  setParser($title);
}

function setParser($title) {
  $DEFAULT_SYNDIGATE_DIR = '/usr/local/syndigate/classes/parse/custom/';
  $titleId = $title["title_id"];
  $title_name = $title["title_name"];
  $publisherId = $title["publisher_id"];
  $publisherName = $title["publisher_name"];
  $languageName = $title["language_name"];
  $languageCode = $title["language_code"];
  $fileName = $DEFAULT_SYNDIGATE_DIR . "syndParser_$titleId.php";
  $date = date('M d, Y, g:i:s A');
  $user = "eyad";
  $fileContent = "<?php
/////////////////////////////////////////////////////////////////////////////////////
//Publisher  : $publisherName  [# publisher id = $publisherId]
//Title      : $title_name [ $languageName ]
//Created on : $date
//Author     : $user
/////////////////////////////////////////////////////////////////////////////////////

require_once '/usr/local/syndigate/classes/parse/syndParseNewstex.php';

class syndParser_{$titleId} extends syndParseNewstex {}";


  if (file_exists($fileName)) {
    $msg = "Failure in implementation, Please Check it With Technical guy.";
  } else {

    file_put_contents($fileName, $fileContent);
    chmod($fileName, 0777);
    if (file_exists($fileName)) {
      $msg = "It was successfully implemented";
    } else {
      $msg = "Failure in implementation";
    }
  }
  echo $msg . PHP_EOL . PHP_EOL;
}

function getTitles() {
  $count = 0;
  $titles = array();
  $query="select "
    . "t.id as title_id,t.name as title_name,"
    . "p.id as publisher_id,p.name as publisher_name,"
    . "l.name as language_name, l.code as language_code "
    . "from title t "
    . "inner join publisher p on t.publisher_id=p.id "
    . "inner join language l on t.language_id=l.id "
    . "where publisher_id=1519"
    . " and t.id in (12636,12637,12638)"  ;
 $q = mysql_query($query);
  while ($row = mysql_fetch_array($q)) {
    $titles[$count]["title_id"] = $row["title_id"];
    $titles[$count]["title_name"] = $row["title_name"];
    $titles[$count]["publisher_id"] = $row["publisher_id"];
    $titles[$count]["publisher_name"] = $row["publisher_name"];
    $titles[$count]["language_name"] = $row["language_name"];
    $titles[$count]["language_code"] = $row["language_code"];
    $count++;
  }
  return $titles;
}

