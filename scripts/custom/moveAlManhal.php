<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

/*
 * '' => array('Title'=>'','Edition'=>''),
 */
$alManhalTD = array(
    //'eReports' => array('Title' => 'eReports', 'Arabic' => 2998, 'English' => 2999,'path'=>'AlManhalReports'),
    //'eTheses' => array('Title' => 'eTheses', 'Arabic' => 3000, 'English' => 3001, 'French' => 3186, 'path' => 'AlManhalTheses'),
    //'eBooks' => array('Title' => 'eBooks', 'Arabic' => 3002, 'English' => 3003, 'French' => 3182, 'path' => 'AlManhalBooks'),
    //'eArticles' => array('Title' => 'eArticles', 'Arabic' => 2647, 'English' => 2648, 'French' => 3183, 'path' => 'AlManhalArticles', 'Collection' => 'Humanitites&#160;&amp;&#160;Social&#160;Sciences'),
    'eArticles' => array('Title' => 'eArticles', 'Arabic' => 2996, 'English' => 2997, 'French' => 3185, 'path' => 'AlManhalArticles', 'Collection' => 'Science,&#160;Techonology&#160;&amp;&#160;Medicine'),
);
$IFH = array(24416, 24413, 24414, 24410, 24412, 24411, 24418, 52387, 67007, 96235, 95366, 98527, 98529, 98506, 98508, 98523, 98525, 98505, 98507, 98522, 98524, 98526, 98528, 103476, 105280, 105282, 108003, 105281, 105283, 108246, 108027, 108028, 108242, 105276, 108100, 105272, 108005, 108112);
$publiserDirName = '/syndigate/sources/home/887/';
$skip = "/syndigate/sources/home/887/skip/";
$directories = scandir($skip);
foreach ($directories as $File) {
  $path = pathinfo($File);
  if (empty($path['extension'])) {
    continue;
  }
  $path_file = $path['extension'];
  if (($File != '.') && ($File != '..') && (is_file("{$skip}{$File}")) && strtolower($path_file) == "xml") {
    $namefile = str_replace(".xml", "", $File);
    $fileString = file_get_contents($skip . $File);
    $moves = NULL;
    $days = array(
        2996 => 110,
        2997 => 110,
        3181 => 110,
        3182 => 110,
        3183 => 110,
        3185 => 110,
        3186 => 110,
    );
    $count = array(
        2996 => 0,
        2997 => 0,
        3181 => 0,
        3182 => 0,
        3183 => 0,
        3185 => 0,
        3186 => 0,
    );
    preg_match_all("/<$namefile>(.*?)<\/$namefile>/is", $fileString, $moves);
    foreach ($moves[0] as $move) {
      $details = NULL;
      preg_match('/<Language>(.*?)<\/Language>(.*?)<Full_Text_Document>(.*?)<\/Full_Text_Document><Cover_Name>(.*?)<\/Cover_Name><ORG_No>(.*?)<\/ORG_No><Collection>(.*?)<\/Collection>/is', $move, $details);
      if ($details[1] == 'French' && $details[6] == "Science,&#160;Techonology&#160;&amp;&#160;Medicine") {
        $alManhalTD[$namefile][$details[1]] = 3185;
        if (in_array($details[5], $IFH)) {
          $alManhalTD[$namefile][$details[1]] = 3181;
        }
        $path = pathinfo($details[3]);
        if ($path['extension'] == "pdf") {
          //Moving PDF file
          if ($count[$alManhalTD[$namefile][$details[1]]] >= 300) {
            $deatdir = date('/Y/m/d/', strtotime("{$days[$alManhalTD[$namefile][$details[1]]]} day ago"));
            $targetDir = $publiserDirName . $alManhalTD[$namefile][$details[1]] . $deatdir;
            $move = "cp " . $skip . $File . ' ' . $targetDir;
            echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
            shell_exec($move);

            $count[$alManhalTD[$namefile][$details[1]]] = 0;
            $days[$alManhalTD[$namefile][$details[1]]] --;
          }
          $deatdir = date('/Y/m/d/', strtotime("{$days[$alManhalTD[$namefile][$details[1]]]} day ago"));
          $targetDir = $publiserDirName . $alManhalTD[$namefile][$details[1]] . $deatdir;
          if (!is_dir($targetDir)) {
            mkdir($targetDir, '0775', true);
          }
          $move = "mv " . $skip . "/" . $details[3] . ' ' . $targetDir;
          echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
          shell_exec($move);
        }
        $path = pathinfo($details[4]);
        if (!empty($path['extension'])) {
          //Moving PDF file
          if ($count[$alManhalTD[$namefile][$details[1]]] >= 300) {
            $deatdir = date('/Y/m/d/', strtotime("{$days[$alManhalTD[$namefile][$details[1]]]} day ago"));
            $targetDir = $publiserDirName . $alManhalTD[$namefile][$details[1]] . $deatdir;
            $move = "cp " . $skip . $File . ' ' . $targetDir;
            echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
            shell_exec($move);
            $count[$alManhalTD[$namefile][$details[1]]] = 0;
            $days[$alManhalTD[$namefile][$details[1]]] --;
          }
          $deatdir = date('/Y/m/d/', strtotime("{$days[$alManhalTD[$namefile][$details[1]]]} day ago"));
          $targetDir = $publiserDirName . $alManhalTD[$namefile][$details[1]] . $deatdir;
          if (!is_dir($targetDir)) {
            mkdir($targetDir, '0775', true);
          }

          $move = "mv " . $skip . "/" . $details[4] . ' ' . $targetDir;
          echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
          shell_exec($move);
        }
        $count[$alManhalTD[$namefile][$details[1]]] ++;
        var_dump($count);
      } else {
        continue;
      }
    }
    $targetDir = $publiserDirName . $alManhalTD[$namefile]['Arabic'];
    $move = "cp " . $skip . $File . ' ' . $targetDir;
    echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
    shell_exec($move);
    $targetDir = $publiserDirName . $alManhalTD[$namefile]['English'];
    $move = "cp " . $skip . $File . ' ' . $targetDir;
    echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
    shell_exec($move);
    $targetDir = $publiserDirName . $alManhalTD[$namefile]['French'];
    $move = "cp " . $skip . $File . ' ' . $targetDir;
    echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
    shell_exec($move);

//      $delete = "rm " . $publiserDirName . $File;
//      echo "Delete the file from the publisher directory " . $delete . chr(10);
//      shell_exec($move);
  }
}
