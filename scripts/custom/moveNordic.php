<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
// Check if an instance exists from the same scripts 
$avilable_instances = 0;
$output = shell_exec('ps aux | grep moveNordic.php | grep -v "/bin/sh -c" | grep -v "sh -c" | grep -v "grep" ');
$output = explode(PHP_EOL, $output);
foreach ($output as $o) {
  if ($o) {
    $avilable_instances++;
  }
}

if ($avilable_instances > 1) {
  die();
}
$titles = array();

$titles["stt"]["en"] = array("title_id" => 7450, "publisher_id" => 1497);
$titles["stt"]["fi"] = array("title_id" => 7451, "publisher_id" => 1497);
$titles["stt"]["sv"] = array("title_id" => 7452, "publisher_id" => 1497);

$titles["viatt"]["en"] = array("title_id" => 7443, "publisher_id" => 1493);
$titles["viatt"]["sv"] = array("title_id" => 7444, "publisher_id" => 1493);

$titles["ntb"]["en"] = array("title_id" => 7453, "publisher_id" => 1498);
$titles["ntb"]["no"] = array("title_id" => 7454, "publisher_id" => 1498);

$titles["ritzau"]["en"] = array("title_id" => 7455, "publisher_id" => 1499);
$titles["ritzau"]["da"] = array("title_id" => 7456, "publisher_id" => 1499);

//var_dump($titles);
$publiserDirName = '/syndigate/sources/home/1493/';
$directories = scandir($publiserDirName);
foreach ($directories as $file) {
  $path = pathinfo($file);
  if (empty($path['extension'])) {
    continue;
  }

  $path_file = $path['extension'];
  if (($file != '.') && ($file != '..') && (is_file("{$publiserDirName}{$file}")) && strtolower($path_file) == "xml") {
    $text = file_get_contents("{$publiserDirName}{$file}");
    preg_match_all("/<NewsItem>(.*?)<\/NewsItem>/is", $text, $items);

    foreach ($items[0] as $item) {
      $match = null;
      $data = trim(strtolower(getCData(getElementByName("NewsItemId", $item))));
      if (preg_match("/(.*?):(.*?)-(.*)/is", $data, $match)) {
        if (isset($match[1]) && isset($match[3])) {
          $name = strtolower(trim($match[1]));
          $lng = strtolower(trim($match[3]));
          $publiserDir = "/syndigate/sources/home/{$titles[$name][$lng]["publisher_id"]}/";

          $id = $titles[$name][$lng]["title_id"];
          $filename = sha1(trim($item)) . ".xml";
          $deatdir = date('/Y/m/d/', strtotime("now"));
          $targetDir = $publiserDir . $id . $deatdir;
          if (!is_dir($targetDir)) {
            mkdir($targetDir, '0775', true);
          }
          echo "creat new file $targetDir$filename" . PHP_EOL;
          $fp = fopen("$targetDir$filename", 'w');
          $star = '<NewsML Version="1.2" xmlns="http://iptc.org/std/NewsML/2003-10-10/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://iptc.org/std/NewsML/2003-10-10/ https://www.iptc.org/std/NewsML/1.2/specification/NewsML_1.2-xmlschema_4.xsd">';
          $end = '</NewsML>';
          fwrite($fp, $star . $item . $end);   // here it will print the array pretty
          fclose($fp);
        } else {
          echo "Not found title $data" . PHP_EOL;
          $filename = sha1(trim($item)) . ".xml";
          echo "creat new file {$publiserDirName}not_process/$filename" . PHP_EOL;
          $fp = fopen("{$publiserDirName}not_process/$filename", 'w');
          $star = '<NewsML Version="1.2" xmlns="http://iptc.org/std/NewsML/2003-10-10/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://iptc.org/std/NewsML/2003-10-10/ https://www.iptc.org/std/NewsML/1.2/specification/NewsML_1.2-xmlschema_4.xsd">';
          $end = '</NewsML>';
          fwrite($fp, $star . $item . $end);   // here it will print the array pretty
          fclose($fp);
          sendEmail($item, $data, $filename);
          //exit;
        }
      }
    }
    $move = "rm {$publiserDirName}{$file}";
    echo "Remove file " . $move . chr(10);
    shell_exec($move);
  }
}

// functions -----------
function getDirectoryFiles($dir) {
  $files = array();
  if ($handle = opendir($dir)) {
    while (false !== ($file = readdir($handle))) {
      if (($file != '.') && ($file != '..')) {
        if (is_file($dir . $file)) {
          $files[] = $dir . $file;
        }
      }
    }
    closedir($handle);
    return $files;
  } else {
    echo "invalid directory $dir \n";
    return false;
  }
}

function getElementByName($name, $text) {
  $element = getElementsByName($name, $text);
  return (isset($element[0]) ? $element[0] : null);
}

function getElementsByName($name, $text) {
  $openTagRegExp = '<' . $name . '([^\>]|[\s])*>';
  $closeTagRegExp = '<\/' . $name . '[\s]*>';
  $elements = preg_split("/$openTagRegExp/i", $text);

  $elementsContents = array();
  $elementsCount = count($elements);
  for ($i = 1; $i < $elementsCount; $i++) {
    $element = preg_split("/$closeTagRegExp/i", $elements[$i]);
    $elementsContents[] = $element[0];
  }
  return $elementsContents;
}

function getCData($text) {
  ini_set("pcre.recursion_limit", "10000");
  $regExp = '<!\[CDATA\[((.*|\s*)*)\]\]>';
  if (preg_match("/$regExp/i", $text, $matches)) {
    return $matches[1];
  } else {
    return $text;
  }
}

// ------ functions

function sendEmail($item, $data, $filename) {
  $body = "<p>Data: $data</p>";
  $body .= "<p>Filename: $filename</p>";
  $body .= "<p>Body:  <pre><code>$item</code></pre></p>";
  $body = wordwrap($body, 75, "\r\n");
  //$mailTo = array('eyad@syndigate.info', 'majdi@syndigate.info');
  $mailTo = array('eyad@syndigate.info');
  $mail = new ezcMailComposer();
  $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply');

  foreach ($mailTo as $to) {
    $mail->addTo(new ezcMailAddress($to));
  }

  $mail->subject = "publisher Nordic send title not map";
  $mail->htmlText = $body;


  $mail->build();
  $transport = new ezcMailMtaTransport();
  $transport->send($mail);
}
