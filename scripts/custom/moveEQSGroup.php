<?php
//Fix File name
$publiserDirName = '/syndigate/sources/home/1584/';
$asianCountry = array("afg", "arm", "aze", "bhr", "bgd", "btn", "brn", "khm", "chn", "cxr", "cck", "iot", "geo", "hkg", "ind", "idn", "irn", "irq", "isr", "jpn", "jor", "kaz", "kwt", "kgz", "lao", "lbn", "mac", "mys", "mdv", "mng", "mmr", "npl", "prk", "omn", "pak", "pse", "phl", "qat", "sau", "sgp", "kor", "lka", "syr", "twn", "tjk", "tha", "tur", "tkm", "are", "uzb", "vnm", "yem");
//Moving all the files in the publisher directory to title directory
$pubId = 1584;
$files = getDirectoryFiles($publiserDirName);
foreach ($files as $file) {
  if (is_file($file)) {
    $filetype = pathinfo($file);
    $fileString = file_get_contents($file);
    $language = null;
    if (preg_match('/<Language>(.*?)<\/Language>/is', $fileString, $language)) {
      $language = trim(strtolower($language[1]));
    } else {
      $language = "";
    }
    $country = null;
    if (preg_match('/<country>(.*?)<\/country>/is', $fileString, $country)) {
      $country = trim(strtolower($country[1]));
    } else {
      $country = "";
    }
    $news_type = null;
    if (preg_match('/<news_type>(.*?)<\/news_type>/is', $fileString, $news_type)) {
      $news_type = trim(strtolower($news_type[1]));
    } else {
      $news_type = "";
    }
    if ($language == "english" || $language == "en") {
      if($country=="rus"){
        moveFile(10865, $file);
      }elseif ($country == "aut" || $country == "che" || $country == "deu") {
        if ($news_type == "adhoc") {
          moveFile(10848, $file);
        } elseif ($news_type == "cms") {
          moveFile(10859, $file);
        } elseif ($news_type == "corporate") {
          moveFile(10854, $file);
        }
      } elseif (in_array($country, $asianCountry)) {
        moveFile(10852, $file);
      } else {
        if ($news_type == "fr_reg") {
          moveFile(10863, $file);
        } elseif ($news_type == "swe_reg") {
          moveFile(10861, $file);
        } elseif ($news_type == "uk-regulatory") {
          moveFile(10866, $file);
        } elseif ($news_type == "corporate") {
          moveFile(10853, $file);
        } else {
          moveFile(10847, $file);
        }
      }

    } elseif ($language == "german" || $language == "de") {
      if ($country == "aut" || $country == "che" || $country == "deu") {
        if ($news_type == "adhoc") {
          moveFile(10849, $file);
        } elseif ($news_type == "cms") {
          moveFile(10860, $file);
        } elseif ($news_type == "corporate") {
          moveFile(10856, $file);
        } elseif ($news_type == "original_research") {
          moveFile(10858, $file);
        }
      } else {
        if ($news_type == "corporate") {
          moveFile(10855, $file);
        } elseif ($news_type == "original_research") {
          moveFile(10857, $file);
        }
      }

    } elseif ($language == "french" || $language == "fr") {
      if ($news_type == "fr_reg") {
        moveFile(10864, $file);
      }
    } elseif ($language == "swedish") {
      if ($news_type == "swe_reg") {
        moveFile(10862, $file);
      }
    } elseif ($language == "zh-hans") {
      moveFile(10850, $file);
    } elseif ($language == "zh-hant") {
      moveFile(10851, $file);
    }
    if (is_file($file)) {
      $targetDir = "/syndigate/sources/home/$pubId/unprocessed/";
      $move = chr(10) . "mv " . $file . ' ' . $targetDir;
      echo "Moving the file from the publisher directory to Unprocessed directory " . $move . chr(10);
      shell_exec($move);
    }

  }
}

deleteOldFiles(10);

// functions -----------
function moveFile($title_id, $file)
{
  $pubId = 1584;
  $targetDir = "/syndigate/sources/home/$pubId/" . $title_id . date('/Y/m/d/');
  if (!is_dir($targetDir)) {
    mkdir($targetDir, '0775', true);
  }
  $move = chr(10) . "mv " . $file . ' ' . $targetDir;
  echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
  shell_exec($move);
}

function getDirectoryFiles($dir)
{
  $files = array();

  if ($handle = opendir($dir)) {
    while (false !== ($file = readdir($handle))) {
      if (($file != '.') && ($file != '..')) {
        if (is_file($dir . $file)) {
          $files[] = $dir . $file;
        }
      }
    }
    closedir($handle);
    return $files;
  } else {
    echo "invalid directory $dir \n";
    return false;
  }
}
function deleteOldFiles($days){
    $targetDir = "/syndigate/sources/home/1584/unprocessed/";
    $cmd = "find $targetDir -type f -mtime +$days  -exec rm {} \;";
    echo $cmd.PHP_EOL;
    shell_exec($cmd);
}
