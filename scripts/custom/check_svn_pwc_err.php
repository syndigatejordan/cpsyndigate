<?php 
echo date("H:i:s") . "\n";

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
$mailTo = array('eyad@syndigate.info', 'khaled@syndigate.info');
//---------------------------------------------------------------------------

$c = new Criteria();
$c->add(TitlePeer::IS_ACTIVE, 1);

if($argv[1]) {
	$c->add(TitlePeer::ID , (int)$argv[1]);
}
$titles = TitlePeer::doSelect($c);

$titlesErrors = array();

foreach ($titles as $title) {
	sleep(1);
	$count++;
	echo "checking title {$title->getId()} ({$title->getName()}) ..... ";
	$rez = shell_exec("svn st " . $title->getParseDir());

	if($rez) {
		$titlesErrors[$title->getId()] = $rez;
		echo "..............................................Errors found\n";
	} else {
		echo "\n";
		//echo "No errors found \n";
	}
		
}

if($titlesErrors) {
	echo "\n\n\nErrors found .... sending email regards to these titles \n";
	//file_put_contents('/usr/local/syndigate/tmp/svn_pwc_err.tmp', json_encode($titlesErrors));
	sendEmail('<pre>' . print_r(array_keys($titlesErrors), true) .'</pre>');
} else {
	echo "\n\n\nNO Errors found .... \n";
	//file_put_contents('/usr/local/syndigate/tmp/svn_pwc_err.tmp', json_encode(array()));
	sendEmail('<pre> No Errors found in titles  SVN</pre>');
}


echo date('H:i:s') . "\n";

// ------ functions

function sendEmail($body)
{
	global $mailTo;

	$mail = new ezcMailComposer();	
	$mail->from = new ezcMailAddress( 'report@syndigate.info', 'No reply' );
	
	foreach($mailTo as $to) {
	 	$mail->addTo( new ezcMailAddress($to) );
	 }

	$mail->subject   = "Syndigate SVN errors checker ";
	$mail->htmlText  = $body;
	
	
	$mail->build();
	$transport = new ezcMailMtaTransport();
	$transport->send( $mail ); 
}
