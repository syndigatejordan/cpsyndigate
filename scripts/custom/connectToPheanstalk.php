<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
require_once '/usr/local/syndigate/misc/pheanstalk_workers/worker.php';

class connectToPheanstalk {

  public $pheanstalk;

  public function __construct() {
    $this->pheanstalk = new Pheanstalk("10.200.200.58:11300");
    $this->go_ahead();
  }

  public function go_ahead() {
    for ($i = 1; $i < 5; $i++) {
      $name = "test$i";
      echo "Start $name".PHP_EOL;
      $this->useTube($name);
    }
  }

  public function useTube($name) {
    $array = array();
    for ($i = 0; $i < 1000; $i++) {
      $array[$i]["id"] = rand(10, 10000000000);
      $array[$i]["title_id"] = rand(10, 10000000000);
      $array[$i]["article_id"] = $this->randomListNumber();
      $json_info = json_encode($array);

      if ($this->pheanstalk) {
        $this->pheanstalk->useTube($name)->put($json_info, 100, 1, 600);
        echo $i . " articles have been added to beanstalk $name\n\n";
        unset($array);
      } else {
        echo "Losing connection $name \n\n";
      }
    }
  }

  public function randomListNumber() {
    $array = array();
    for ($i = 0; $i < 24000; $i++) {
      $array[$i] = rand(10, 10000000000);
    }
    $array = implode(",", $array);
    return $array;
  }

}
$connectToPheanstalk =new connectToPheanstalk();
