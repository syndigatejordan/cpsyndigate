<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

$year = $argv[1];

$filesnotExist = array();
$clientsIds = array(1, 3, 4, 5, 6, 8, 9, 10, 11, 15, 22, 23, 23);
$qCmd = "SELECT * FROM client_connection_file where client_connection_id=25 and local_file like'$year/%'";
echo $qCmd . chr(10);
$q = mysql_query($qCmd);
while ($row = mysql_fetch_assoc($q)) {
  $searchFile = $row["local_file"];
  echo "Start search for file $searchFile" . PHP_EOL;
  foreach ($clientsIds as $id) {
    $file = "/syndigate/clients/$id/$searchFile";
    echo "search for file $file" . PHP_EOL;
    if (file_exists($file)) {
      copyFile($file, $searchFile);
      echo "File copied successfully.";
      break;
    } else {
      continue;
    }
  }
  if (!file_exists("/syndigate/clients/4/$searchFile")) {
    $filesnotExist[] = $searchFile;
  }
}
$fp = fopen("/tmp/filesnotExist$year.json", 'w');
fwrite($fp, json_encode($filesnotExist));   // here it will print the array pretty
fclose($fp);

function copyFile($file, $searchFile) {
  $targetDir = explode("/", $searchFile);
  array_pop($targetDir);
  $targetDir = implode("/", $targetDir);
  $targetDir = "/syndigate/clients/4/$targetDir";
  if (!is_dir($targetDir)) {
    mkdir($targetDir, '0775', true);
  }
  $copy = "cp $file  $targetDir";
  echo $copy . chr(10);
  shell_exec($copy);
}
