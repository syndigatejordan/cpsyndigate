<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
fixImagesName();
fixImagesType();

function fixImagesName() {
  $id = file_get_contents('/tmp/fiximagesNameIDs.txt');
  if (!$id) {
    $id = 0;
  }
  $IMAGE = "image";
  $query = mysql_query(
          "SELECT * FROM image WHERE  ( img_name LIKE \"%?%\" OR img_name LIKE \"%&%\") "
          . "AND NOT (img_name LIKE \"%/1189/3%\" OR img_name LIKE \"%/50/3918%\") "
          . "AND ( img_name LIKE \"%imgs.syndigate.info%\") LIMIT  5000");
  while ($row = mysql_fetch_row($query)) {
    if (preg_match("!/1189/3741/!", $row[2]) || preg_match("!/1189/3822/!", $row[2]) || preg_match("!/50/3918/!", $row[2])) {
      continue;
    }
    $file = str_replace('http://imgs.syndigate.info/', "/syndigate/imgs/", $row[2]);
    if (file_exists($file)) {
      $newFile = preg_replace("/(\?|&)(.*)/is", "", $file);

      $file = str_replace(' ', '\ ', $file);
      $file = str_replace('(', '\(', $file);
      $file = str_replace(')', '\)', $file);
      $file = str_replace("'", "\'", $file);
      $file = str_replace("`", "\`", $file);
      $file = str_replace("&", "\&", $file);
      $file = str_replace("?", "\?", $file);
      $file = str_replace("=", "\=", $file);
      $file = str_replace(";", "\;", $file);
      $file = str_replace("\$", "\\$", $file);

      $move = "mv " . $file . ' ' . $newFile;
      echo $move . PHP_EOL;
      shell_exec($move);
      if (file_exists($newFile)) {
        $newFile = str_replace("/syndigate/imgs/", 'http://imgs.syndigate.info/', $newFile);
        $qu = "UPDATE $IMAGE SET img_name='$newFile' WHERE id={$row[0]} ;";
        if (mysql_query($qu)) {
          echo 'Update image name ' . $row[0] . PHP_EOL;
        } else {
          echo 'Something error on image id ' . $row[0] . PHP_EOL;
        }
      } else {
        echo "The new image does not exist id" . $row[0] . PHP_EOL;
      }
    } else {
      $newFile = preg_replace("/(\?|&)(.*)/is", "", $file);
      if (file_exists($newFile)) {
        $newFile = str_replace("/syndigate/imgs/", 'http://imgs.syndigate.info/', $newFile);
        $qu = "UPDATE $IMAGE SET img_name='$newFile' WHERE id={$row[0]} ;";
        if (mysql_query($qu)) {
          echo 'Update image name ' . $row[0] . PHP_EOL;
        } else {
          echo 'Something error on image id ' . $row[0] . PHP_EOL;
        }
      } else {
        echo "The new image and old image  does not exist id " . $row[0] . PHP_EOL;
      }
    }
    $id = file_get_contents('/tmp/fiximagesNameIDs.txt');
    if (!$id) {
      $id = 0;
    }
    if ($id < $row[0]) {
      $id = $row[0];
    }
    file_put_contents('/tmp/fiximagesNameIDs.txt', $id);
  }
}

function fixImagesType() {
  $id = file_get_contents('/tmp/fiximagesTypeIDs.txt');
  if (!$id) {
    $id = 0;
  }
  $IMAGE = "image";
  $query = mysql_query(" SELECT * FROM $IMAGE WHERE id>$id AND (image_type LIKE \"%?%\" OR image_type LIKE \"%&%\") ORDER BY id LIMIT  1000");
  while ($row = mysql_fetch_row($query)) {

    $imageType = preg_replace("/(\?|&)(.*)/is", "", $row[5]);
    $qu = "UPDATE $IMAGE SET image_type='$imageType' WHERE id={$row[0]} ;";
    if (mysql_query($qu)) {
      echo 'Update image name ' . $row[0] . PHP_EOL;
    } else {
      echo 'Something error on image id ' . $row[0] . PHP_EOL;
    }

    $id = file_get_contents('/tmp/fiximagesTypeIDs.txt');
    if (!$id) {
      $id = 0;
    }
    if ($id < $row[0]) {
      $id = $row[0];
    }
    file_put_contents('/tmp/fiximagesTypeIDs.txt', $id);
  }
}
