<?php

$heder = '<?xml version="1.0" encoding="iso-8859-1"?><nitf xmlns:php="http://php.net/xsl"><head></head><body>';
$foot = '</body></nitf>';
require_once '/usr/local/syndigate/scripts/propel_include_config.php';
for ($d = 0; $d < 4; $d++) {
  $theSundayTimesArticle = "";
  $move = FALSE;
  $publiserDirName = '/syndigate/sources/home/538/1587/' . date('Y/m/d/', strtotime('-' . $d . ' day'));
  if (is_dir($publiserDirName)) {
    $files = getDirectoryFiles($publiserDirName);
    foreach ($files as $file) {
      if (is_file($file)) {
        $fileString = file_get_contents($file);
        preg_match_all('/<article>(.*?)<\\/article>/s', $fileString, $articles);
        foreach ($articles[0] as $article) {
          preg_match('/<pubdate>(.*?)<\\/pubdate>/s', $article, $day);
          $dayName = date('D', strtotime($day[1]));
          if ($dayName == 'Sun') {
            $theSundayTimesArticle .=$article;
            $fileDay = $day[1];
            $move = TRUE;
          }
        }
      }
    }
  }
  if ($move) {
    $theSundayTimesArticle = $heder . $theSundayTimesArticle . $foot;
    $targetDir = '/syndigate/sources/home/538/1588/' . date('Y/m/d/');
    $theSundayTimesFile = $targetDir . 'syndiGateFile_' . $fileDay.'.xml';
    if (!is_dir($targetDir)) {
      mkdir($targetDir, '0775', true);
    }
    if (!file_exists($theSundayTimesFile)) {
      file_put_contents($theSundayTimesFile, $theSundayTimesArticle);
    }
  }
}
// functions -----------
function getDirectoryFiles($dir) {
  $files = array();
  if ($handle = opendir($dir)) {
    while (false !== ($file = readdir($handle))) {
      if (($file != '.') && ($file != '..')) {
        if (is_file($dir . $file)) {
          $files[] = $dir . $file;
        }
      }
    }
    closedir($handle);
    return $files;
  } else {
    echo "invalid directory $dir \n";
    return false;
  }
}