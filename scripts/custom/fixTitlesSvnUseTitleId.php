<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
$cURLConnection = curl_init();
curl_setopt($cURLConnection, CURLOPT_URL, 'http://cms.syndigate.info/rest/cms-titles');
curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
$phoneList = curl_exec($cURLConnection);
curl_close($cURLConnection);
$ids= json_decode($phoneList,true);
$ids=$ids["ids"];

$q='select PUBLISHER_ID,title.id,title.name,title.home_dir
  ,title_frequency_id
  ,(select sum(f.frequency) from orders_frequency f where title.id=f.title_id group by title_id limit 1) frq 
  from title 
  where 
  title.is_active=1 and title.id in(' . $ids . ')
  order by title_frequency_id asc, frq asc , title.id asc;';

$result = mysql_query($q);

while ($row = mysql_fetch_assoc($result)) {
    echo "Start with Title {$row["name"]} # {$row["id"]}" . PHP_EOL;
    $cmdsvn = "php /usr/local/syndigate/scripts/fix_cms_titles_svn.php {$row["id"]} {$row["PUBLISHER_ID"]}";
    echo $cmdsvn . PHP_EOL;
    shell_exec($cmdsvn);
}


