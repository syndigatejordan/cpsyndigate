<?php 
/**
 * This scaript is used when you want to move a title from a publisher to another publisher
 * 
 * it will doing the following 
 * 1- change database settings 
 * 2- move the repo (in /var/svnserver) from old publisher to the new publisher
 * 3- move the "home director",  "working copy" directory, "parser directory" from the old publisher folder to the new one
 * 4- remove the old nagios file and create a new one 
 */
require_once '/usr/local/syndigate/scripts/propel_include_config.php';

$titleId 			= trim($argv[1]);
echo "title id from argv is " . $titleId . PHP_EOL;
$toPublisherID   	= trim($argv[2]);
echo "to publisher id from argv is " . $toPublisherID . PHP_EOL;
if(isset($argv[3])) {
	$fromPublisherId    = trim($argv[3]); // Optional in case if first run failed and the database setting changed but the script cut to determine the old publisher id
	echo "from publisher id from argv is " . $fromPublisherId . PHP_EOL;
}



if(!$titleId) {
	die("enter title id" . PHP_EOL);
}

if(!$toPublisherID) {
	die("Enter the publisher id you want to move the title to " . PHP_EOL);
}

$title 		 = TitlePeer::retrieveByPK($titleId);
$toPublisher = PublisherPeer::retrieveByPK($toPublisherID);

if(!$title) {
	die('Invalid title id' . PHP_EOL);
}

if(!$toPublisher) {
	die('Invalid publisher id' . PHP_EOL);
}

if(!$fromPublisherId) {
	$fromPublisherId = $title->getPublisherId();
} else {
	if($fromPublisherId == $toPublisherID) {
		die("from publisher id is the same as to publisher id from the argv" . PHP_EOL);
	}
}



/**
* Strting moving process
*/


/**
 *Change the database settings
 */
if($title->getPublisherId() == $toPublisher->getId() ) {
	echo "Title's publisher id is the same as publisher id you want to move .. skipping database changes" . PHP_EOL;
} else {
	
	echo "Changing database settings for the title ...";
	$title->setPublisherId($toPublisherID);
	$title->setNagiosConfigFile("/etc/nagios/sources/$toPublisherID");
	$title->setHomeDir("/syndigate/sources/home/$toPublisherID/$titleId/");
	$title->setWorkDir("/syndigate/sources/wc/$toPublisherID/$titleId/");
	$title->setParseDir("/syndigate/sources/pwc/$toPublisherID/$titleId/");
	$title->setSvnRepo("/var/svnserve/repos/$toPublisherID/$titleId/");
	$title->setUnixGid($toPublisher->getUnixUid());
	$title->save();
	echo " Done." . PHP_EOL;
}

// changin FTP account home directory to be to tho "DO NOT FORGET  ................ !!!!!!!" . PHP_EOL;
echo "changing user mod to be under the new publisher group ";
shell_exec("usermod -g " . $toPublisher->getFtpUSername() . " " . $title->getFtpUsername()) ;
echo PHP_EOL;






/**
 * Moving the repo from old publisher folder to the new one
 */
echo "Moving the title repo ..." . PHP_EOL;
$fromPublisherRepoDir = "/var/svnserve/repos/$fromPublisherId/$titleId";
$toPublisherRepoDir   = "/var/svnserve/repos/$toPublisherID";

if(!is_dir($toPublisherRepoDir)) {
	echo "new publisher dir in repo $toPublisherRepoDir  not exists (creating it) ...";
	$result = mkdir($toPublisherRepoDir);
	if(!$result) {
		echo PHP_EOL . "Error .... can not create $toPublisherRepoDir, consider make it manually and moving /var/svnserve/repos/$fromPublisherId/$titleId to it" . PHP_EOL;
	} else {
		echo "created ..." . PHP_EOL;		
	}
} else {
	echo "publisher repo dir $toPublisherRepoDir exists" . PHP_EOL;
}


if(is_dir($fromPublisherRepoDir)) {
	echo "Moving old repo location  to $toPublisherRepoDir";
	exec("mv $fromPublisherRepoDir $toPublisherRepoDir/$titleId");
} else {
	echo "$fromPublisherRepoDir dose not exist ... checking if moved before" . PHP_EOL;
	if(!is_dir("$toPublisherRepoDir/$titleId")) {
		die("$toPublisherRepoDir/$titleId dose not exist .. can not continue script since the repo dose not exist " . PHP_EOL);
	} else {
		echo "$toPublisherRepoDir/$titleId exist .. it was moved before ..." . PHP_EOL; 
	}
}


if(!is_dir("$toPublisherRepoDir/$titleId")) {
	echo PHP_EOL . "ERROR .... Could not move $fromPublisherRepoDir to  $toPublisherRepoDir/$titleId conseder moving it manually" . PHP_EOL;
} else {
	echo "...done " . PHP_EOL;
	
	echo "moving home directory ...  ";
	if(!is_dir("/syndigate/sources/home/$fromPublisherId/$titleId")) {
		echo "home dir dose not exist ... checking it if moved before ";
		
		if(!is_dir("/syndigate/sources/home/$toPublisherID/$titleId")) {
			die("Also /syndigate/sources/home/$toPublisherID/$titleId dose not exist ... can not continue script without home directory" . PHP_EOL);
		} else {
			echo "Home directory was move before " . PHP_EOL;
		}
	} else {
		exec("mv /syndigate/sources/home/$fromPublisherId/$titleId /syndigate/sources/home/$toPublisherID/$titleId ");
		if(!is_dir("/syndigate/sources/home/$toPublisherID/$titleId")) {
			echo PHP_EOL . "Error can not move home directory conseder move it manually" . PHP_EOL;
		} else {
			echo "... done " . PHP_EOL;
		}

		echo "changing home dir Chown user " . PHP_EOL;
		exec("chown -R " . $title->getFtpUsername() . ":" . $toPublisher->getFtpUsername() . " /syndigate/sources/home/$toPublisherID/$titleId ");
		
	}
	
	echo "Changing user home directory usermod ..." ;
	shell_exec("usermod -d  /syndigate/sources/home/$toPublisherID/$titleId " . $title->getFtpUsername());
	echo "Done ." . PHP_EOL; 
	
	
	
	echo "check out the new working copy" . PHP_EOL;
	exec("svn co file:///$toPublisherRepoDir/$titleId/ /syndigate/sources/wc/$toPublisherID/$titleId");
	echo "check out the new parser copy copy" . PHP_EOL;
	exec("svn co file:///$toPublisherRepoDir/$titleId/ /syndigate/sources/pwc/$toPublisherID/$titleId");
	echo "Deleting old working copy" . PHP_EOL;
	exec("rm -rf /syndigate/sources/wc/$fromPublisherId/$titleId");
	echo "Deleting old parser copy" . PHP_EOL;
	exec("rm -rf /syndigate/sources/pwc/$fromPublisherId/$titleId");
	
	
	
	
	//rebuild nagious files
	echo "rebuild nagios files to the publisher ...";
	$publisherId = $toPublisherID; // re-define variable to use an existing script that accept the same name, and used from another location
	$nagiosPath  = '/etc/nagios/sources/' . $publisherId ;
	require 'syndGenerateNagiosConf.php';
	echo "done " . PHP_EOL;
}