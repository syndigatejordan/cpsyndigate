<?php

$publiserDirName = '/syndigate/sources/home/1534/';
$titles = array(10220, 10224, 10227, 10228);
/*$titles = array( 10221, 10222, 10223, 10225, 10226, 10229, 10230, 10231, 10232, 10233,
    10234, 10235, 10236, 10237, 10238, 10239, 10240, 10241, 10242, 10243, 10244, 10245, 10246, 10247, 10248, 10249,
    10250, 10251, 10252, 10253, 10254, 10255, 10256, 10257, 10258, 10259, 10260, 10261, 10262, 10263, 10264, 10265,
    10266, 10267, 10268, 10269, 10270, 10271, 10272, 10273, 10274, 10275, 10276, 10277, 10278, 10279, 10280, 10281,
    10282, 10283, 10284, 10285, 10286, 10287, 10288, 10289, 10290, 10291, 10292, 10293, 10294, 10295, 10296, 10297,
    10298, 10299, 10300, 10301, 10302, 10303, 10304, 10305, 10306, 10307, 10308, 10309, 10310, 10311, 10312, 10313,
    10314, 10315, 10316, 10317, 10318, 10319);*/
foreach ($titles as $title) {
    echo "Start with Title $title" . PHP_EOL;
    $directories = scandir($publiserDirName . $title);
    $countFile = 0;
    $day = 793;
    foreach ($directories as $file) {
        echo "Start with file $file" . PHP_EOL;

        $path = pathinfo($file);
        if (empty($path['extension'])) {
            continue;
        }


        $path_file = $path['extension'];
        if (($file != '.') && ($file != '..') && (is_file("{$publiserDirName}{$title}/{$file}")) && strtolower($path_file) == "xml") {
            $countFile++;

            $deatdir = date('/Y/m/d/', strtotime("$day day ago"));
            $targetDir = $publiserDirName . $title . $deatdir;
            if (!is_dir($targetDir)) {
                mkdir($targetDir, '0775', true);
            }

            $move = "mv " . $publiserDirName .$title."/". $file . ' ' . $targetDir;
            echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
            shell_exec($move);
            if ($countFile == 2000) {
                $countFile = 0;
                $day--;
            }

        }
    }
}
