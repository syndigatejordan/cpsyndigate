<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
require_once '/usr/local/syndigate/ez_autoload.php';
$clientsIds = array(102); //
$titlesIds = array(2375, 2376, 2377, 2338); // 
$fDate = date('Y-m-d', strtotime('-1 day'));
$sDate = date('Y-m-d');
$fDateDir = date('Y/m/d', strtotime('-1 day'));
$sDateDir = date('Y/m/d');
foreach ($clientsIds as $clientId) {

//  $rows = mysql_fetch_assoc($result);
//  $titlesIds = array(1, 2, 3);
  foreach ($titlesIds as $titleId) {
    $articlesPdf = mysql_query("select article.id,article.title_id, headline,summary,body,author,date,parsed_at,img_name,image_caption,image_type,"
            . "original_name,extras,cat_name from article left join image on article.id=image.article_id inner join article_original_data "
            . "on article.article_original_data_id=article_original_data.id left join original_article_category "
            . "on article_original_data.original_cat_id=original_article_category.id "
            . "where article.title_id=$titleId and date(parsed_at) between '$fDate' and '$sDate'");
    while ($row = mysql_fetch_assoc($articlesPdf)) {
      if (!is_null($row["img_name"]) && !($titleId == 2338 || $titleId == 2375)) {
        $datePdf = date('Ymd', strtotime($row["parsed_at"]));
        $original_name = "";
        $row["cat_name"] = str_replace(',', '_', $row["cat_name"]);
        $row["cat_name"] = preg_replace('!\s+!', '_', $row["cat_name"]);
        $original_name = $row["cat_name"] . '_' . $datePdf . '_' . $row["id"] . '.' . $row["image_type"];
        $pdf_name = str_replace(IMGS_HOST, IMGS_PATH, $row["img_name"]);

        $fDir = "/syndigate/clients/$clientId/$fDateDir/$original_name";
        $sDir = "/syndigate/clients/$clientId/$sDateDir/$original_name";
        if (file_exists($fDir) || file_exists($sDir)) {
          echo 'File exists' . chr(10);
          continue;
        } else {
          $directory = "/syndigate/clients/$clientId/$sDateDir";
          if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
          }
          $move_command = "cp $pdf_name  $sDir";
          echo $move_command . chr(10);
          shell_exec($move_command);
        }
      } else {
        $matchesfile = null;
        preg_match('/;s:(.*?):"(.*?)";}/', $row["extras"], $matchesfile);
        if ($titleId == 2375) {
          $original_name = $matchesfile[2];
          $original_name = explode('/', $original_name);
          if (count($original_name) > 1) {
            $original_name = $original_name[1];
          } else {
            $original_name = $original_name[0];
          }
          $original_name = explode('_', $original_name);
          array_pop($original_name);
          $original_name = implode('_', $original_name);
          $datePdf = date('Ymd', strtotime($row["parsed_at"]));
          $original_name.='_' . $datePdf . '_' . $row["id"] . '.pdf';
        } elseif ($titleId == 2376 || $titleId == 2377) {
          $original_name = "";
          $row["cat_name"] = str_replace(',', '_', $row["cat_name"]);
          $row["cat_name"] = preg_replace('!\s+!', '_', $row["cat_name"]);
          $original_name = $row["cat_name"] . '_' . $datePdf . '_' . $row["id"] . '.pdf';
        } elseif ($titleId == 2338) {
          $original_name = '2338_' . $datePdf . '_' . $row["id"] . '.pdf';
        }
        $htmlPage = "";
        $htmlPage = "<html><head><style>";
        $htmlPage.= "body{margin:0;padding:0;font-family:MyriadPro-Regular,'Myriad Pro Regular',MyriadPro,'Myriad Pro',Helvetica,Arial,sans-serif}#headline{font-size:12px}#author,#body,#body table,#date{font-size:8px}#body table{page-break-inside:avoid!important}#logo{text-align: left;}#copyright{text-align: left; font-size:7px;} hr{margin-top:20px;}";
        $htmlPage.= "</style></head><body><div id='article'>";
        $htmlPage.= "<div id='logo'>";
        $htmlPage.= "<img src='/usr/local/syndigate/scripts/custom/Newsbites_logo.png'>";
        $htmlPage.= "</div>";
        ///////////////////////////HeadLine////////////////////////////////////
        $headline = preg_replace('!\s+!', ' ', $row["headline"]);
        $htmlPage.='<h1 id="headline">' . $headline . '</h1>';
        ///////////////////////////author//////////////////////////////////
        $author = preg_replace('!\s+!', ' ', $row["author"]);
        $htmlPage.='<p id="author">' . $author . '</p>';
        //////////////////////////////Date/////////////////////////////////////////
        $articleDate = date('d F Y', strtotime($row["date"]));
        $htmlPage.='<p id="date">' . $articleDate . '<p>';
        ///////////////////////////Body/////////////////////////////////////////////
        $body = preg_replace('!\s+!', ' ', $row["body"]);
        $body = str_replace('<table', '<table border="1" ', $body);
        $htmlPage.='<div id="body">' . $body . '</div>';
        $htmlPage.='<hr/><div id="copyright">&copy; Copyright 2015 News Bites Pty Ltd</div>';
        $htmlPage.="</div>";
        $htmlPage.="</body></html>";

        $fDir = "/syndigate/clients/$clientId/$fDateDir/$original_name";
        $sDir = "/syndigate/clients/$clientId/$sDateDir/$original_name";
        if (file_exists($fDir) || file_exists($sDir)) {
          echo 'File exists' . chr(10);
          continue;
        } else {
          $directory = "/syndigate/clients/$clientId/$sDateDir";
          if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
          }
          $file = file_put_contents(dirname(__file__) . "/tmp.html", $htmlPage);
          $pdfFileName = dirname(__file__) . "/" . $original_name;
          $htmlDestinationName = dirname(__file__) . "/tmp.html";

          $cmd = "wkhtmltopdf $htmlDestinationName $pdfFileName";
          exec($cmd);
          exec("rm $htmlDestinationName");

          $move_command = "mv $pdfFileName  $sDir";
          echo $move_command . chr(10);
          shell_exec($move_command);

          $image_path = "341/$titleId/" . ((abs(crc32($original_name)) % 100) + 1) . '/';
          $storingDir = IMGS_PATH . $image_path;
          if (!is_dir($storingDir)) {
            mkdir($storingDir, 0777, true);
          }
          $copy_command = "cp $sDir $storingDir";
          echo $copy_command . chr(10);
          shell_exec($copy_command);
          $article_id = $row["id"];
          $img_name = IMGS_HOST . $image_path . $original_name;
          mysql_query("insert into image (article_id,img_name,image_type,original_name) values ($article_id,'$img_name','pdf','$sDir')");
        }
      }
    }
  }
}