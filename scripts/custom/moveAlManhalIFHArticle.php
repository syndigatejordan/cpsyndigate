<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
$titlesMove = array(
   /* "Academic&#160;Journal&#160;of&#160;Research&#160;in&#160;Economics&#160;and&#160;Management" => 3191,
    "African&#160;Journal&#160;of&#160;Biological&#160;Sciences" => 3192,
    "Ain&#160;Shams&#160;Dental&#160;Journal" => 3193,
    "Ain&#160;Shams&#160;Medical&#160;Journal" => 3194,
    "Alandalus&#160;Journal&#160;for&#160;Applied&#160;Sciences" => 3196,
    "Al-Azhar&#160;Journal&#160;of&#160;Pediatrics" => 3197,
    "Al-Azhar&#160;Medical&#160;Journal" => 3198,
    "Alexandria&#160;Journal&#160;of&#160;Food&#160;Science&#160;and&#160;Technology" => 3199,
    "Algerian&#160;Journal&#160;of&#160;Arid&#160;Environment" => 3200,
    "al-Ibda&#160;al-Riyaḍi" => 3201,
    "Aljouf&#160;Science&#160;and&#160;Engineering&#160;Journal" => 3202,
    "Aljouf&#160;University&#160;Medical&#160;Journal" => 3203,
    "Al-Mithqal&#160;for&#160;Economic&#160;and&#160;Administrative&#160;Sciences" => 3204,
    "American&#160;Journal&#160;of&#160;Islamic&#160;Social&#160;Sciences" => 3205,
    "Arab&#160;International&#160;Informatics&#160;Journal" => 3206,
    "Arab&#160;International&#160;Informatics&#160;Journal" => 3405,
    "Arab&#160;Journal&#160;of&#160;Forensic&#160;Sciences&#160;and&#160;Forensic&#160;Medicine" => 3207,
    "Arab&#160;Journal&#160;of&#160;Forensic&#160;Sciences&#160;and&#160;Forensic&#160;Medicine" => 3406,
    "Bahrain&#160;Medical&#160;Bulletin" => 3208,
    "Cataract&#160;and&#160;Cornea:&#160;Journal&#160;of&#160;the&#160;Egyptian&#160;Society&#160;of&#160;Cataract&#160;and&#160;Corneal&#160;Diseases" => 3209,
    "Catrina:&#160;The&#160;International&#160;Journal&#160;of&#160;Environmental&#160;Sciences" => 3210,
    "Dirasat:&#160;Administrative&#160;Sciences" => 3211,
    "Dirasat:&#160;Educational&#160;Sciences" => 3212,
    "Dirasat:&#160;Human&#160;and&#160;Social&#160;Sciences" => 3213,
    "Dirasat:&#160;Shari'a&#160;and&#160;Law&#160;Sciences" => 3214,
    "Egyptian&#160;Dermatology&#160;Online&#160;Journal" => 3215,
    "Egyptian&#160;Journal&#160;of&#160;Aquatic&#160;Biology&#160;and&#160;Fisheries" => 3216,
    "Egyptian&#160;Journal&#160;of&#160;Geriatrics&#160;and&#160;Gerontology" => 3217,
    "Egyptian&#160;Journal&#160;of&#160;Pediatrics" => 3218,
    "Egyptian&#160;Journal&#160;of&#160;Plant&#160;Breeding" => 3219,
    "Egyptian&#160;Journal&#160;of&#160;Sheep&#160;and&#160;Goat&#160;Sciences" => 3220,
    "Egyptian&#160;Journal&#160;of&#160;Zoology" => 3221,
    "Gulf&#160;University&#160;Journal:&#160;Engineering&#160;and&#160;Computer&#160;Engineering&#160;Division" => 3222,
    "Hadhramout&#160;Journal&#160;of&#160;Medical&#160;Sciences" => 3223,
    "Hamdan&#160;Medical&#160;Journal" => 3224,
   "Ḥawliyāt&#160;al-ʻUlūm&#160;wa-al-Tiknūlūjiyā" => 3225,
  "Health,&#160;Safety&#160;and&#160;Environment" => 3226,
    "Health,&#160;Safety&#160;and&#160;Environment" => 3407,
    "International&#160;Arab&#160;Journal&#160;of&#160;Dentistry" => 3227,
    "International&#160;Interdisciplinary&#160;Journal&#160;of&#160;Education" => 3228,
    "International&#160;Journal&#160;for&#160;Sciences&#160;and&#160;Technology" => 3229,
    "International&#160;Journal&#160;of&#160;Accounting&#160;Research" => 3230,
    "International&#160;Journal&#160;of&#160;Bilingual&#160;and&#160;Multilingual&#160;Teachers&#160;of&#160;English" => 3231,
    "International&#160;Journal&#160;of&#160;Child&#160;Neuropsychiatry" => 3232,
    "International&#160;Journal&#160;of&#160;Child&#160;Neuropsychiatry" => 3408,
    "International&#160;Journal&#160;of&#160;Computing&#160;and&#160;Digital&#160;Systems" => 3233,
    "International&#160;Journal&#160;of&#160;Computing&#160;and&#160;Network&#160;Technology" => 3234,
    "International&#160;Journal&#160;of&#160;Development" => 3235,
    "International&#160;Journal&#160;of&#160;Development" => 3409,
    "International&#160;Journal&#160;of&#160;Excellence&#160;in&#160;Education" => 3236,
    "International&#160;Journal&#160;of&#160;Excellence&#160;in&#160;e-Learning" => 3237,
    "International&#160;Journal&#160;of&#160;Excellence&#160;in&#160;Healthcare&#160;Management" => 3238,
    "International&#160;Journal&#160;of&#160;Excellence&#160;in&#160;Healthcare&#160;Management" => 3410,
    "International&#160;Journal&#160;of&#160;Excellence&#160;in&#160;Islamic&#160;Banking&#160;and&#160;Finance" => 3239,
    "International&#160;Journal&#160;of&#160;Excellence&#160;in&#160;Public&#160;Sector&#160;Management" => 3240,
    "International&#160;Journal&#160;of&#160;Excellence&#160;in&#160;Tourism,&#160;Hospitality&#160;and&#160;Catering" => 3241,
    "International&#160;Journal&#160;of&#160;Health&#160;Sciences" => 3242,
    "International&#160;Journal&#160;of&#160;Islamic&#160;Economics&#160;and&#160;Finance&#160;Studies" => 3243,
    "International&#160;Journal&#160;of&#160;Library&#160;and&#160;Information&#160;Sciences" => 3244,
    "International&#160;Journal&#160;of&#160;Pedagogical&#160;Innovations" => 3245,
    "International&#160;Review&#160;of&#160;Contemporary&#160;Learning&#160;Research" => 3246,
    "Islam&#160;and&#160;Civilisational&#160;Renewal" => 3247,
    "Islamic&#160;Economic&#160;Studies" => 3248,
    "ISRA&#160;International&#160;Journal&#160;of&#160;Islamic&#160;Finance" => 3249,
    "IUG&#160;Journal&#160;of&#160;Economics&#160;and&#160;Business&#160;Studies" => 3250,
    "IUG&#160;Journal&#160;of&#160;Educational&#160;and&#160;Psychological&#160;Studies" => 3251,
    "IUG&#160;Journal&#160;of&#160;Humanities&#160;Research" => 3252,
    "IUG&#160;Journal&#160;of&#160;Islamic&#160;Studies" => 3253,
    "Jordan&#160;Journal&#160;for&#160;History&#160;and&#160;Archaeology" => 3254,
    "Jordan&#160;Journal&#160;of&#160;Agricultural&#160;Sciences" => 3255,
    "Jordan&#160;Journal&#160;of&#160;Applied&#160;Science&#160;:&#160;Humanities&#160;Sciences&#160;Series" => 3256,
    "Jordan&#160;Journal&#160;of&#160;Biological&#160;Sciences" => 3257,
    "Jordan&#160;Journal&#160;of&#160;Business&#160;Administration" => 3258,
    "Jordan&#160;Journal&#160;of&#160;Chemistry" => 3259,
    "Jordan&#160;Journal&#160;of&#160;Civil&#160;Engineering" => 3260,
    "Jordan&#160;Journal&#160;of&#160;Economic&#160;Sciences" => 3261,
    "Jordan&#160;Journal&#160;of&#160;Modern&#160;Languages&#160;and&#160;Literature" => 3262,
    "Jordan&#160;Journal&#160;of&#160;Pharmaceutical&#160;Sciences" => 3263,
    "Jordan&#160;Journal&#160;of&#160;Social&#160;Sciences" => 3264,
    "Jordan&#160;Medical&#160;Journal" => 3265,
    "Journal&#160;Alandalus&#160;for&#160;Humanities&#160;and&#160;Social&#160;Sciences" => 3266,
    "Journal&#160;of&#160;ACS:&#160;Advances&#160;in&#160;Computer&#160;Science" => 3267,
    "Journal&#160;of&#160;Agricultural&#160;and&#160;Veterinary&#160;Sciences" => 3268,
    "Journal&#160;of&#160;Arab&#160;Child" => 3269,
    "Journal&#160;of&#160;Arabic&#160;and&#160;Human&#160;Sciences" => 3270,
    "Journal&#160;of&#160;Arabic&#160;Studies" => 3271,
    "Journal&#160;of&#160;Baghdad&#160;College&#160;of&#160;Dentistry" => 3272,
    "Journal&#160;of&#160;Engineering&#160;and&#160;Applied&#160;Sciences" => 3273,
    "Journal&#160;of&#160;Engineering&#160;and&#160;Computer&#160;Sciences" => 3274,
    "Journal&#160;of&#160;Islamic&#160;Business&#160;and&#160;Management" => 3275,
    "Journal&#160;of&#160;Islamic&#160;Economics,&#160;Banking&#160;and&#160;Finance" => 3276,
    "Journal&#160;of&#160;Islamic&#160;Sciences" => 3277,
    "Journal&#160;of&#160;Islamic&#160;Studies" => 3278,
    "Journal&#160;of&#160;Islamic&#160;Finance" => 3279,
    "Journal&#160;of&#160;King&#160;Abdulaziz&#160;University:&#160;Arts&#160;and&#160;Humanities" => 3280,
    "Journal&#160;of&#160;King&#160;Abdulaziz&#160;University:&#160;Earth&#160;Sciences" => 3281,
    "Journal&#160;of&#160;King&#160;Abdulaziz&#160;University:&#160;Economics&#160;and&#160;Administration" => 3282,
    "Journal&#160;of&#160;King&#160;Abdulaziz&#160;University:&#160;Engineering&#160;Sciences" => 3283,
    "Journal&#160;of&#160;King&#160;Abdulaziz&#160;University:&#160;Environmental&#160;Design&#160;Sciences" => 3284,
    "Journal&#160;of&#160;King&#160;Abdulaziz&#160;University:&#160;Environmental&#160;Design&#160;Sciences" => 3411,
    "Journal&#160;of&#160;King&#160;Abdulaziz&#160;University:&#160;Islamic&#160;Economics" => 3285,
    "Journal&#160;of&#160;King&#160;Abdulaziz&#160;University:&#160;Marine&#160;Sciences" => 3286,
    "Journal&#160;of&#160;King&#160;Abdulaziz&#160;University:&#160;Medical&#160;Sciences" => 3287,
    "Journal&#160;of&#160;King&#160;Abdulaziz&#160;University:&#160;Metrology&#160;,&#160;Environment&#160;and&#160;Arid&#160;Land&#160;Agricultural&#160;Sciences" => 3288,
    "Journal&#160;of&#160;King&#160;Abdulaziz&#160;University:&#160;Science" => 3289,
    "Journal&#160;of&#160;Middle&#160;East&#160;Media" => 3290,
    "Journal&#160;of&#160;Natural&#160;Sciences&#160;and&#160;Mathematics" => 3291,
    "Journal&#160;of&#160;New&#160;Technology&#160;and&#160;Materials" => 3292,
    "Journal&#160;of&#160;Oral&#160;and&#160;Dental&#160;Research" => 3293,
    "Journal&#160;of&#160;Palestinian&#160;Refugee&#160;Studies" => 3294,
    "Journal&#160;of&#160;Policy&#160;and&#160;Development&#160;Studies" => 3295,
    "Journal&#160;of&#160;Research&#160;and&#160;Development" => 3296,
    "Journal&#160;of&#160;South&#160;Valley&#160;University&#160;for&#160;Environmental&#160;Researches" => 3297,
    "Journal&#160;of&#160;Taibah&#160;University&#160;Arts&#160;and&#160;Humanities" => 3298,
    "Journal&#160;of&#160;Teaching&#160;and&#160;Teacher&#160;Education" => 3299,
    "Journal&#160;of&#160;the&#160;Arab&#160;American&#160;University" => 3300,
    */"Journal&#160;of&#160;the&#160;Arabian&#160;Aquaculture&#160;Society " => 3301,
    /*"Journal&#160;of&#160;the&#160;Association&#160;of&#160;Arab&#160;Universities" => 3302,
    "Journal&#160;of&#160;the&#160;Egyptian&#160;Society&#160;of&#160;Parasitology" => 3303,
    "Journal&#160;of&#160;the&#160;Royal&#160;Medical&#160;Services" => 3304,
    "Lebanese&#160;Medical&#160;Journal" => 3305,
    "Majallat&#160;ʻUlūm&#160;wa-Taqnīyat&#160;al-Nashāṭ&#160;al-Badanī&#160;al-Riyāḍī" => 3306,
    "Majallat&#160;ʻUlūm&#160;wa-Taqnīyat&#160;al-Nashāṭ&#160;al-Badanī&#160;al-Riyāḍī" => 3412,
    "Majmaah&#160;Journal&#160;of&#160;Health&#160;Sciences" => 3307,
    "Management&#160;Studies&#160;and&#160;Economic&#160;Systems" => 3308,
    "Medical&#160;Journal&#160;of&#160;Islamic&#160;World&#160;Academy&#160;of&#160;Sciences" => 3309,
    "Middle&#160;East&#160;Journal&#160;of&#160;Age&#160;and&#160;Ageing" => 3310,
    "Middle&#160;East&#160;Journal&#160;of&#160;Age&#160;and&#160;Ageing" => 3413,
    "Middle&#160;East&#160;Journal&#160;of&#160;Business" => 3311,
    "Middle&#160;East&#160;Journal&#160;of&#160;Internal&#160;Medicine" => 3312,
    "Middle&#160;East&#160;Journal&#160;of&#160;Nursing" => 3313,
    "Middle&#160;East&#160;Journal&#160;of&#160;Psychiatry&#160;and&#160;Alzheimers" => 3314,
    "Middle&#160;East&#160;Journal&#160;of&#160;Psychiatry&#160;and&#160;Alzheimers" => 3414,
    "Middle&#160;Eastern&#160;Studies&#160;Journal" => 3315,
    "Modern&#160;Egypt" => 3316,
    "NG-Journal&#160;of&#160;Social&#160;Development" => 3317,
    "Open&#160;Veterinary&#160;Journal" => 3318,
    "Psychological&#160;and&#160;Educational&#160;Studies&#160;Review" => 3319,
    "Review&#160;of&#160;Economics&#160;and&#160;Political&#160;Science" => 3320,
    "Review&#160;of&#160;Public&#160;Administration&#160;and&#160;Management" => 3321,
    "Revue&#160;des&#160;Bioressources" => 3322,
    "Scientific&#160;Journal&#160;of&#160;Environmental&#160;Sciences" => 3323,
    "Scientific&#160;Journal&#160;of&#160;Environmental&#160;Sciences" => 3415,
    "Scientific&#160;Journal&#160;of&#160;Zoology" => 3324,
    "Singaporean&#160;Journal&#160;of&#160;Business,&#160;Economics&#160;and&#160;Management&#160;Studies" => 3325,
    "Smile&#160;Dental&#160;Journal" => 3326,
    "Studies&#160;in&#160;Orthophonia&#160;and&#160;Neuropsychology" => 3327,
    "Studies&#160;in&#160;Orthophonia&#160;and&#160;Neuropsychology" => 3416,
    "Sultan&#160;Qaboos&#160;University&#160;Medical&#160;Journal" => 3328,
    "Taibah&#160;University&#160;Journal&#160;of&#160;Educational&#160;Sciences" => 3329,
    "The&#160;Arab&#160;Journal&#160;of&#160;Psychiatry" => 3330,
    "The&#160;Arab&#160;Journal&#160;of&#160;Psychiatry" => 3417,
    "The&#160;Egyptian&#160;Journal&#160;of&#160;Community&#160;Medicine" => 3331,
    "The&#160;Egyptian&#160;Journal&#160;of&#160;Environmental&#160;Change" => 3332,
    "The&#160;Egyptian&#160;Journal&#160;of&#160;Environmental&#160;Change" => 3418,
    "The&#160;Egyptian&#160;Journal&#160;of&#160;Forensic&#160;Sciences&#160;and&#160;Applied&#160;Toxicology" => 3333,
    "The&#160;Egyptian&#160;Journal&#160;of&#160;Medical&#160;Microbiology" => 3334,
    "The&#160;Scholar:&#160;Islamic&#160;Academic&#160;Research&#160;Journal" => 3335,
    "World&#160;Family&#160;Medicine&#160;Journal:&#160;Incorporating&#160;the&#160;Middle&#160;East&#160;Journal&#160;of&#160;Family&#160;Medicine" => 3336,
    "Yemeni&#160;Journal&#160;of&#160;Agriculture&#160;and&#160;Veterinary&#160;Sciences" => 3337,
    "AlBaha&#160;University&#160;Journal&#160;for&#160;Human&#160;Sciences" => 3338,
    "Al-Jouf&#160;Social&#160;Sciences&#160;Journal" => 3339,
    "Amarabac&#160;Magazin" => 3340,
    "Arab&#160;Journal&#160;of&#160;Science&#160;and&#160;Research&#160;Publishing" => 3341,
    "Arab&#160;Journal&#160;of&#160;Science&#160;and&#160;Research&#160;Publishing" => 3419,
    "Bouhouth&#160;Magazine" => 3342,
    "Compunet&#160;(&#160;The&#160;Egyptian&#160;Information&#160;Journal&#160;)" => 3343,
    "Dental&#160;News" => 3344,
    "DRASSA:&#160;Journal&#160;of&#160;Development&#160;and&#160;Research&#160;for&#160;Sport&#160;Science&#160;Activities" => 3345,
    "Dubai&#160;Judicial&#160;Institute&#160;Journal" => 3346,
    "Economic&#160;Horizons" => 3347,
    "Geopolitics" => 3348,
    "Global&#160;Journal&#160;of&#160;Economic&#160;and&#160;Business" => 3349,
    "Gulf&#160;University&#160;Journal:&#160;Law&#160;Division" => 3350,
    "International&#160;Journal&#160;of&#160;Intellectual&#160;Property" => 3351,
    "International&#160;Journal&#160;of&#160;Open&#160;Problems&#160;in&#160;Complex&#160;Analysis" => 3352,
    "International&#160;Journal&#160;of&#160;Open&#160;Problems&#160;in&#160;Computer&#160;Science&#160;and&#160;Mathematics" => 3353,
    "Journal&#160;Egyptian&#160;Acadmic&#160;Society&#160;Environmental&#160;Development&#160;(&#160;D-Environmental&#160;Studies&#160;)" => 3354,
    "Journal&#160;of&#160;Administrative&#160;and&#160;Economic&#160;Sciences" => 3355,
    "Journal&#160;of&#160;Al-Aqsa&#160;University:&#160;Series&#160;of&#160;Human&#160;Sciences" => 3356,
    "Journal&#160;of&#160;Al-Aqsa&#160;University:&#160;Series&#160;of&#160;Natural&#160;Science" => 3357,
    "Journal&#160;of&#160;Education&#160;Studies" => 3358,
    "Journal&#160;of&#160;Educational&#160;and&#160;Psychological&#160;Sciences" => 3359,
    "Journal&#160;of&#160;Educational&#160;and&#160;Psychological&#160;Sciences" => 3360,
    "Journal&#160;of&#160;Environmental&#160;Behavior" => 3361,
    "Journal&#160;of&#160;Humanities&#160;and&#160;Social&#160;Studies" => 3362,
    "Journal&#160;of&#160;Islamic&#160;and&#160;Religious&#160;Studies" => 3363,
    "Journal&#160;of&#160;Social&#160;Affairs" => 3364,
    "Journal&#160;of&#160;Social&#160;Studies" => 3365,
    "Journal&#160;of&#160;the&#160;North&#160;for&#160;Basic&#160;and&#160;Applied&#160;Sciences" => 3366,
    "Journal&#160;of&#160;the&#160;North&#160;for&#160;Basic&#160;and&#160;Applied&#160;Sciences" => 3420,
    "Journal&#160;of&#160;the&#160;North&#160;for&#160;Humanities" => 3367,
    "Oman&#160;Medical&#160;Journal" => 3368,
    "Palestinian&#160;Journal&#160;of&#160;Open&#160;Education" => 3369,
    "Sudan&#160;Medical&#160;Journal" => 3370,
    "The&#160;Arab&#160;Journal&#160;of&#160;Quality&#160;in&#160;Education" => 3371,
    "The&#160;Egyptian&#160;Journal&#160;of&#160;Hospital&#160;Medicine" => 3372,
   "The&#160;Journal&#160;of&#160;Middle&#160;East&#160;and&#160;North&#160;Africa&#160;Sciences" => 3373,
    "The&#160;Journal&#160;of&#160;Middle&#160;East&#160;and&#160;North&#160;Africa&#160;Sciences" => 3421,
    "Umm&#160;Al-Qura&#160;University&#160;Journal&#160;of&#160;Languages&#160;and&#160;Literatures" => 3374,
    "Zagazig&#160;Nursing&#160;Journal" => 3375,
    "Zarqa&#160;Journal&#160;for&#160;Research&#160;and&#160;Studies&#160;in&#160;Humanities" => 3376,*/
);
$notFindId = array();
$titles = array();
$q = mysql_query("select id from title where publisher_id =887 and language_id=2");
while ($row = mysql_fetch_array($q)) {
  array_push($titles, $row["id"]);
}

$fileString = file_get_contents("/syndigate/sources/home/887/skip/eArticles.xml");
preg_match_all("/<eArticles>(.*?)<\/eArticles>/is", $fileString, $moves);
foreach ($moves[0] as $move) {
  $details = NULL;
  $ifh = NULL;
  $id = NULL;
  preg_match('/<Publication_Title>(.*?)<\/Publication_Title>(.*?)<Language>(.*?)<\/Language>(.*?)<ORG_No>(.*?)<\/ORG_No><Collection>(.*?)<\/Collection>/is', $move, $details);

  $id = $details[5];
  $details[1] = str_replace("&amp;", "&;", $details[1]);
  var_dump($details[1]);
  $ifh = $titlesMove[$details[1]];
  if ($ifh == NULL) {
    array_push($notFindId, $id);
    continue;
  }

  //var_dump($details);
  var_dump($id);
  var_dump($ifh);
  $find = FALSE;
  $sha1 = sha1($id);
  foreach ($titles as $title) {
    $key = $title . "_" . $sha1 . "_" . $id;
    $q = mysql_query("select * from article_original_data_tmp where original_article_id='$key'");
    $row = mysql_fetch_array($q);
    if (!empty($row["original_article_id"]) && !$find) {
      echo 'Updateing ........... ' . PHP_EOL;
      $key = str_replace($title, $ifh, $key);
      $q1 = "update article_original_data_tmp set original_article_id='$key' where id={$row["id"]}";
      echo $q1 . PHP_EOL;
      mysql_query($q1);
      $q2 = "update article_tmp set title_id=$ifh where article_original_data_id={$row["id"]}";
      echo $q2 . PHP_EOL;
      mysql_query($q2);
      $find = TRUE;
      continue;
    }
  }
  if (!$find) {
    array_push($notFindId, $id);
  }
}
$notFindId = implode(",", $notFindId);
echo " Not find ids $notFindId " . PHP_EOL;
