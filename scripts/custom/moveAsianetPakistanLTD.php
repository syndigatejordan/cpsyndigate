<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';


$asianetPakistanLTD = array('1854' => 'Accounting Today', '1855' => 'African Eye News Service', '1856' => 'Airdrie and Coatbridge Advertiser',
    '1857' => 'American Banker', '1858' => 'American Banker Magazine', '1859' => 'Annual Review Social Development in Pakistan',
    '1860' => 'Asset Securitization Report', '1861' => 'Asia Pacific', '1862' => 'Azer News', '1863' => 'Azeri-Press news agency',
    '1864' => 'Bhutan Times', '1865' => 'Birmingham Mail', '1866' => 'Botswana Daily News', '1867' => 'BruDirect', '1868' => 'Balochistan Express',
    '1869' => 'Biomedica', '1870' => 'Business Bhutan', '1871' => 'Business Mirror', '1872' => 'Business Recorder', '1873' => 'Business Review',
    '1874' => 'CANA News', '1875' => 'CZECH News Agency', '1876' => 'Credit Union Journal', '1877' => 'Coventry Telegraph',
    '1878' => 'Daily Financial Times', '1879' => 'Daily Post', '1880' => 'Daily Record', '1881' => 'Diplomat East Africa',
    '1882' => 'Dumfries and Galloway Standard', '1883' => 'Development and Cooperation', '1884' => 'Eastern Worker', '1885' => 'Energy Update',
    '1886' => 'Engineering and Industrial Review-Bangladesh', '1887' => 'Engineering and Industrial Review-Pakistan',
    '1888' => 'Employee Benefit News', '1889' => 'Employee Benefit Advisor', '1890' => 'Express Tribune', '1891' => 'Evening Gazette',
    '1892' => 'Financial Planning', '1893' => 'Flare', '1894' => 'Global Media Journal', '1895' => 'Gomal University Journal of Research', '1896' => 'Hiba',
    '1897' => 'Hilal', '1898' => 'Health Data Management', '1899' => 'International Journal of Agriculture and Biology',
    '1900' => 'International Journal of Biotechnology for Wellness Industries', '1901' => 'International Journal of Criminology and Sociology',
    '1902' => 'International Research Journal of Arts and Humanities', '1903' => 'Insurance Networking News', '1904' => 'Insurance Broadcasting',
    '1905' => 'Information Management', '1906' => 'Japan Economic Foundation', '1907' => 'Journal of Basic and Applied Sciences',
    '1908' => 'Pakistan Journal of European Studies', '1909' => 'Journal of Gender and Social Issues', '1910' => 'Journal of Himalayan Earth Sciences',
    '1911' => 'Journal of Humanities and Social Science', '1912' => 'Journal of Pakistan Vision', '1913' => 'Journal of Reviews on Global Economics',
    '1914' => 'KMUJ - Khyber Medical University Journal', '1915' => 'Liverpool Echo', '1916' => 'Loughborough Echo', '1917' => 'Manchester Evening News',
    '1918' => 'CMS Nigeria - The Nation', '1919' => 'National Yemen', '1920' => 'Nigerian Tribune', '1921' => 'Paisley Daily Express',
    '1922' => 'Pakistan and Gulf Economist', '1923' => 'Pakistan Armed Forces Medical Journal', '1924' => 'Pakistan Journal of Agricultural Research',
    '1925' => 'Pakistan Journal of Analytical and Environmental Chemistry', '1926' => 'Pakistan Journal of Clinical Psychology',
    '1927' => 'Pakistan Journal of Nematology', '1928' => 'Pakistan Journal of Psychology', '1929' => 'Pakistan Journal of Science',
    '1930' => 'Pakistan Journal of Social and Clinical Psychology', '1931' => 'Pakistan Oral and Dental Journal', '1932' => 'Pakistan Perspectives',
    '1933' => 'Pakistan Sugar Journal', '1934' => 'PM News', '1935' => 'Policy Perspectives', '1936' => 'Pressenza',
    '1937' => 'Pulse International', '1938' => 'Regional Times', '1939' => 'Science International', '1940' => 'Strategic Studies', '1941' => 'The Dialogue',
    '1942' => 'Daily Messenger', '1943' => 'Dawn', '1944' => 'Democratic Voice of Burma', '1945' => 'Engineering Review',
    '1946' => 'Gomal Journal of Medical Sciences', '1947' => 'Hungarian Official News Digest', '1948' => 'Investment Management Mandate Pipeline',
    '1949' => 'Insurance Journal', '1950' => 'International Journal of Academic Research in Accounting Finance and Management Sciences',
    '1951' => 'International Journal of Academic Research in Business and Social Sciences',
    '1952' => 'International Journal of Academic Research in Economics and management Sciences',
    '1953' => 'International Journal of Academic Research in Progressive Education and Development',
    '1954' => 'International Journal of Human Resource Studies', '1955' => 'International Journal of Learning and Development',
    '1956' => 'Journal of Agriculture and Social Sciences', '1957' => 'The Journal of Animal and Plant Sciences',
    '1958' => 'Journal of Asian Civilizations', '1959' => 'Journal of Behavioral Sciences', '1960' => 'JOURNAL OF BUSINESS STRATEGIES',
    '1961' => 'Journal of Educational Research', '1962' => 'Journal of Pakistan Association of Dermatologist',
    '1963' => 'Journal of Pioneering Medical Sciences', '1964' => 'Journal of Political Studies',
    '1965' => 'Journal of Postgraduate Medical Institute', '1966' => 'Journal of Pharmacy and Nutrition Sciences',
    '1967' => 'Journal of the Chemical Society of Pakistan', '1968' => 'Journal of Statistics', '1969' => 'KASBIT Business Journal',
    '1970' => 'Kashmir Journal of Language Research', '1971' => 'Leveraged Finance News', '1972' => 'Miadhu', '1973' => 'Mizzima News',
    '1974' => 'Mizzima Business Weekly', '1975' => 'Money Management Executive', '1976' => 'National Herald Tribune', '1977' => 'National Mortgage News',
    '1978' => 'LETA', '1979' => 'New Horizons', '1980' => 'Nukta Art', '1981' => 'Pakistan Business News', '1982' => 'Pakistan Company News',
    '1983' => 'Pakistan Economic and Social Review', '1984' => 'Pakistan Food Journal', '1985' => 'Pakistan Journal of Agricultural Sciences',
    '1986' => 'Pakistan Journal of Information Management and Libraries', '1987' => 'Pakistan journal of Life and Social Sciences',
    '1988' => 'Pakistan Journal of Medical Research', '1989' => 'Pakistan Journal of Medical Sciences',
    '1990' => 'Pakistan Journal of Psychological Research', '1991' => 'Pakistan Journal of Social Sciences',
    '1992' => 'Pakistan Journal of Weed Sciences', '1993' => 'Pakistan Journal of Zoology', '1994' => 'Pakistan News Releases',
    '1995' => 'Official News Releases', '1996' => 'PaymentsSource', '1997' => "People's Review", '1998' => 'The Philippine STAR',
    '1999' => 'Pique', '2000' => 'Philippines News Agency', '2001' => 'Qatar Today', '2002' => 'Daily Patriot', '2003' => 'Sarhad Journal of Agriculture',
    '2004' => 'Securities Technology Monitor', '2005' => 'Slogan', '2006' => 'The Slovak Spectator', '2007' => 'Soil and Environment', '2008' => 'Solihull News',
    '2009' => 'South Asia', '2010' => 'South Asian Studies', '2011' => 'SBP Research Bulletin', '2012' => 'South African Official News',
    '2013' => 'South Wales Echo', '2014' => 'South Sudan Today', '2015' => 'Stirling Observer', '2016' => 'Sunday Mail', '2017' => 'Sunday Mercury',
    '2018' => 'Sunday Mirror', '2019' => 'Sunday People', '2020' => 'Sunday Sun', '2021' => 'Sunday Times', '2022' => 'Sunday Trust', '2023' => 'Taiwan News',
    '2024' => 'Technology Times', '2025' => 'Traders Magazine', '2026' => 'Bangkok Post', '2027' => 'The Birmingham Post',
    '2028' => 'The Chester Chronicle', '2029' => 'The China Post', '2030' => 'The Bond Buyer', '2031' => 'The Chronicle', '2032' => 'The Liberian Dialogue',
    '2033' => 'Daily Mirror', '2034' => 'The Diplomatic Insight', '2035' => 'Glasgow Now', '2036' => 'The Huddersfield Daily Examiner',
    '2037' => 'The Journal', '2038' => 'The Korea Times', '2039' => 'NAMPA', '2040' => 'The News Today',
    '2041' => 'The Phnom Penh Post', '2042' => 'The President Post', '2043' => 'Journal of Pakistan Medical Association',
    '2044' => 'The Shan Herald Agency for News', '2045' => 'The UB Post', '2046' => "Today's Zaman", '2047' => "Value Chain", '2048' => 'Wales on Sunday', '2049' => 'The Weekly Mirror',
    '2050' => 'Weekly Trust', '2051' => 'Western Mail', '2052' => 'Wishaw Press', '2066' => 'The Financial Daily', '2067' => 'Pakistan Observer',
    '2068' => 'Balochistan Times', '2069' => 'Defence Journal', '2070' => 'Pakistan Investor Guide', '2071' => 'Pakistan Press International (PPI) Photo Service', '2072' => 'Pakistan Press International (PPI) Radio News',
    '2073' => 'Frontier Star', '2079' => "Mergers  and  Acquisitions, The Dealmaker's Journal", '2237' => "The Nation", '2238' => "Daily Times", '2497' => 'Africa Renewal',
    '2499' => 'Afrikipresse', '2500' => 'CAJ News Agency', '2575' => "L'Intelligent D'Abidjan", '2576' => 'Lanka puvath', '2579' => 'Premium Times',
    '2504' => 'Shanghai Daily', '2505' => 'The Advocate', '2506' => 'Tribal News Network', '2507' => 'Xinhua Finance Agency', '2509' => 'Business Day',
    '2510' => 'Daily Guide Network Task #765', '2511' => 'Daily Trust', '2512' => 'Ghanaian Chronicle', '2586' => 'Le Renouveau', '2514' => 'New Era',
    '2515' => 'The Financial Express', '2516' => 'The News International', '2517' => 'The Star Task #767', '2518' => 'Afghanistan Today', '2519' => 'Myanmar Business Today',
    '2520' => 'Sunday Standard', '2521' => 'Energy and Power', '2522' => 'Addis Standard', '2523' => 'Capital Business', '2524' => 'Herald',
    '2525' => 'Jornal Transparência', '2526' => 'Aurora', '2527' => 'Journal of Global Innovations in Agricultural and Social Sciences',
    '2528' => 'Journal of Public Administration and Governance', '2529' => 'Malaysian Institute of Management',
    '2530' => 'Bahria Journal of Professional Psychology', '2531' => 'Bulletin of Education and Research',
    '2532' => 'International Journal of Accounting and Financial Reporting', '2533' => 'Journal of the Research Society of Pakistan',
    '2534' => 'Journal of Sociological Research', '2535' => 'Al-Hikmat', '2536' => 'Berkeley Journal of Middle Eastern and Islamic Law',
    '245' => 'PPI',);
//Fix File name
$publiserDirName = '/syndigate/sources/home/83/';
$directories = scandir($publiserDirName);
foreach ($directories as $File) {
  $path = pathinfo($File);
  if (empty($path['extension'])) {
    continue;
  }
  $path_file = $path['extension'];
  if (($File != '.') && ($File != '..') && (is_file("{$publiserDirName}{$File}")) && strtolower($path_file) == "xml" || strtolower($path_file) == "rar") {
    $path["basename"] = preg_replace("/[  ~&,:\-\)\(\']/", ' ', $path["basename"]);
    $path["basename"] = str_replace('..', '.', $path["basename"]);
    $path["basename"] = str_replace("'", "\'", $path["basename"]);
    $path["basename"] = str_replace("\'", "", $path["basename"]);
    $path["basename"] = str_replace("`", "\`", $path["basename"]);
    $path["basename"] = str_replace("\`", "", $path["basename"]);
    $path["basename"] = str_replace("\$", "\\$", $path["basename"]);
    $path["basename"] = str_replace("\$", "", $path["basename"]);
    $path["basename"] = str_replace('  ', ' ', $path["basename"]);
    $path["basename"] = preg_replace('!\s+!', '_', $path["basename"]);
    $path["basename"] = str_replace('._', '_', $path["basename"]);
    $path["basename"] = str_replace(' _', '_', $path["basename"]);
    $File = str_replace(' ', '\ ', $File);
    $File = str_replace('(', '\(', $File);
    $File = str_replace(')', '\)', $File);
    $File = str_replace("'", "\'", $File);
    $File = str_replace("`", "\`", $File);
    $File = str_replace("&", "\&", $File);
    $File = str_replace("\$", "\\$", $File);
    $move = "mv " . $publiserDirName . $File . " " . $publiserDirName . $path["basename"];
    if ($File != $path["basename"]) {
      echo $move . chr(10);
      shell_exec($move);
    }
  }
}
//Moving all the files in the publisher directory to title directory 
$files = getDirectoryFiles($publiserDirName);
foreach ($files as $file) {
  if (is_file($file)) {
    $filetype = pathinfo($file);
    if ($filetype["extension"] == 'rar') {
      $pubId = 83;
      $targetDir = "/syndigate/sources/home/83/2026";
      if (!is_dir($targetDir)) {
        mkdir($targetDir, '0775', true);
      }
      $move = "mv " . $file . ' ' . $targetDir;
      echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
      shell_exec($move);
    } else {
      $fileString = file_get_contents($file);
      foreach ($asianetPakistanLTD as $title_id => $source) {
        //if( strpos($fileString, "<Contact /><Copyright>$copyright</Copyright></Document>" )) {
        preg_match('/<Source>(.*?)<\\/Source>/s', $fileString, $matches);

        if (is_array($matches)) {
          $matches = trim($matches[1]);
          $matche = str_replace(']', '', strtolower($matches));
          if ($matche == "muscat daily") {
            $targetDir = "/syndigate/sources/home/1/927" . date('/Y/m/d/');
            if (!is_dir($targetDir)) {
              mkdir($targetDir, '0775', true);
            }
            $move = "cp " . $file . ' ' . $targetDir;
            echo "Cpying the file from the publisher directory to title directory " . $move . chr(10);
            shell_exec($move);
            break;
          }
          if (empty($matche)) {
            $emptyDir = "/syndigate/sources/home/83/emptyFile";
            if (!is_dir($emptyDir)) {
              mkdir($emptyDir, '0775', true);
            }
            $moveEmpty = "mv " . $file . ' ' . $emptyDir;
            echo "Moving the empty file from the publisher directory to empty directory " . $moveEmpty . chr(10);
            shell_exec($moveEmpty);
          }

          if ($matche == strtolower($source)) {
            $pubId = 83;
            $targetDir = "/syndigate/sources/home/$pubId/" . $title_id . date('/Y/m/d/');
            if (!is_dir($targetDir)) {
              mkdir($targetDir, '0775', true);
            }
            $move = "mv " . $file . ' ' . $targetDir;
            echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
            shell_exec($move);
          }
        }
      }
      if (is_file($file)) {
        echo "File not prosse move to folder unprocessedFiles";
        $move = "mv " . $file . ' /syndigate/sources/home/83/unprocessedFiles/';
        echo "Moving the file from the publisher directory to unprocessedFiles " . $move . chr(10);
        shell_exec($move);
      }
    }
  }
}
deleteOldFiles(10);

// functions -----------
function getDirectoryFiles($dir) {
  $files = array();
  if ($handle = opendir($dir)) {
    while (false !== ($file = readdir($handle))) {
      if (($file != '.') && ($file != '..')) {
        if (is_file($dir . $file)) {
          $files[] = $dir . $file;
        }
      }
    }
    closedir($handle);
    return $files;
  } else {
    echo "invalid directory $dir \n";
    return false;
  }
}

function getDirectoryContent($dir) {
  $files = array();
  if ($handle = opendir($dir)) {
    while (false !== ($file = readdir($handle))) {
      if (($file != '.') && ($file != '..')) {
        $files[] = $dir . $file;
      }
    }
    closedir($handle);
    return $files;
  } else {
    echo "invalid directory $dir \n";
    return false;
  }
}
function deleteOldFiles($days){
    $targetDir = "/syndigate/sources/home/83/unprocessedFiles/";
    $cmd = "find $targetDir -type f -mtime +$days  -exec rm {} \;";
    echo $cmd.PHP_EOL;
    shell_exec($cmd);
}
