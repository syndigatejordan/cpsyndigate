<?php

moveWENNPhotos3742();
moveCoverImages5691();
moveCoverImagesPictureFeatures5690();
moveWENNPhotos3829();
//moveCoverImagesPictureFeatures5846();
//moveCoverImagesPictureFeatures5848();
//moveCoverImagesPictureFeatures5850();

function moveWENNPhotos3742() {
  $fileArray = array();

  $titleTargetDir = '/syndigate/sources/home/1189/WENN_Photos_3742/';
  $publiserDirName = '/syndigate/sources/home/1189/3742/';
  $not_processed = "/syndigate/sources/home/1189/not_processed_3742";
  //$move = "mv  $publiserDirName*.jpg  $titleTargetDir";
  //shell_exec($move);
  //$move = "mv  $publiserDirName*.xmp  $titleTargetDir";
  //shell_exec($move);
  $move = "";
  $directories = scandir($titleTargetDir);
  foreach ($directories as $file) {
    $path = pathinfo($file);
    if (empty($path['extension'])) {
      continue;
    }
    $path_file = $path['extension'];
    if (($file != '.') && ($file != '..') && (is_file("{$titleTargetDir}{$file}")) && strtolower($path_file) == "xmp") {
      $fileString = file_get_contents($titleTargetDir . $file);
      $headline = null;
      preg_match('/<photoshop:Headline>(.*?)<\/photoshop:Headline>/s', $fileString, $headline);
      $headline = $headline[1];

      $DateCreated = null;
      preg_match('/<photoshop:DateCreated>(.*?)<\/photoshop:DateCreated>/s', $fileString, $DateCreated);
      $DateCreated = $DateCreated[1];

      $TransmissionReference = null;
      preg_match('/<photoshop:TransmissionReference>(.*?)<\/photoshop:TransmissionReference>/s', $fileString, $TransmissionReference);
      $TransmissionReference = $TransmissionReference[1];

      $photoshopSource = null;
      preg_match('/<photoshop:Source>(.*?)<\/photoshop:Source>/s', $fileString, $photoshopSource);
      $photoshopSource = $photoshopSource[1];

      $dcsubject = null;
      preg_match('/<dc:subject>(.*?)<\/dc:subject>/s', $fileString, $dcsubject);
      $dcsubject = $dcsubject[1];

      $dcdescription = null;
      preg_match('/<dc:description>(.*?)<\/dc:description>/s', $fileString, $dcdescription);
      $dcdescription = trim(strip_tags($dcdescription[1]));


      $dateKey = date("Y-m-d", strtotime($DateCreated));
      $headlineKey = sha1($headline . $dateKey);
      if (isset($fileArray[$headlineKey])) {
        $fileArray[$headlineKey]["file"][] = array("file" => trim($TransmissionReference), "dcdescription" => $dcdescription);
      } else {
        $fileArray[$headlineKey] = array();
        $fileArray[$headlineKey]["contant"] = $fileString;
        $fileArray[$headlineKey]["headline"] = trim($headline);
        $fileArray[$headlineKey]["photoshopSource"] = trim($photoshopSource);
        $fileArray[$headlineKey]["dcsubject"] = trim($dcsubject);
        $fileArray[$headlineKey]["dcdescription"] = trim($dcdescription);
        $fileArray[$headlineKey]["DateCreated"] = trim($DateCreated);
        $fileArray[$headlineKey]["file"][] = array("file" => trim($TransmissionReference), "dcdescription" => $dcdescription);
        //var_dump($fileArray);exit;
      }
    }
  }
  $contant = array();
  foreach ($fileArray as $key => &$item) {
    $article = "";
    $keyString = "<key>$key<key>";
    $headline = "<headline><![CDATA[{$item["headline"]}]]></headline>";
    $DateCreated = "<DateCreated>{$item["DateCreated"]}</DateCreated>";
    $media = "";
    foreach ($item["file"] as $itemMedia) {
      $media.="<container_media>";
      $media.="<media>{$itemMedia["file"]}.jpg</media>";
      $media.="<media_caption><![CDATA[{$itemMedia["dcdescription"]}.jpg]]></media_caption>";
      $media.="</container_media>";
    }
    $article = '<?xml version="1.0" encoding="iso-8859-1"?><item>' . $keyString . $headline . $DateCreated . $media . "</item>";
    $targetDir = $publiserDirName;


    foreach ($item["file"] as $itemMedia) {
      $file = $titleTargetDir . "{$itemMedia["file"]}.jpg";
      $delfile = $titleTargetDir . "{$itemMedia["file"]}.xmp";
      if (file_exists($file)) {
        $move = "mv " . $file . ' ' . $targetDir;
        echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
        shell_exec($move);
        $del = "rm " . $delfile;
        shell_exec($del);
        file_put_contents($targetDir . "$key.xml", $article);
      }
    }
  }
  $move = "mv  $titleTargetDir*.jpg  $not_processed";
  shell_exec($move);
  $move = "mv  $titleTargetDir*.xmp  $not_processed";
  shell_exec($move);
}

function moveCoverImages5691() {
  $fileArray = array();
  $PublisherTargetDir = '/syndigate/sources/home/1257/COVER IMAGES (5691)/*';
  $PublisherTargetDir = str_replace(' ', "\ ", $PublisherTargetDir);
  $PublisherTargetDir = str_replace('(', "\(", $PublisherTargetDir);
  $PublisherTargetDir = str_replace(")", "\)", $PublisherTargetDir);
  $titleTargetDir = '/syndigate/sources/home/1257/cover_images_5691/';

  $move = "mv  $PublisherTargetDir  $titleTargetDir";
  echo $move . PHP_EOL;
  shell_exec($move);

  $publiserDirName = '/syndigate/sources/home/1257/5691/';
  $not_processed = "/syndigate/sources/home/1257/not_processed_5691";
  $move = "";
  $directories = scandir($titleTargetDir);
  foreach ($directories as $file) {
    $path = pathinfo($file);
    if (empty($path['extension'])) {
      continue;
    }
    $path_file = $path['extension'];
    if (($file != '.') && ($file != '..') && (is_file("{$titleTargetDir}{$file}")) && strtolower($path_file) == "xmp") {
      $fileString = file_get_contents($titleTargetDir . $file);
      $headline = null;
      preg_match('/<photoshop:Headline>(.*?)<\/photoshop:Headline>/s', $fileString, $headline);
      $headline = $headline[1];

      $DateCreated = null;
      preg_match('/<photoshop:DateCreated>(.*?)<\/photoshop:DateCreated>/s', $fileString, $DateCreated);
      $DateCreated = $DateCreated[1];

      $TransmissionReference = null;
      /* preg_match('/<photoshop:TransmissionReference>(.*?)<\/photoshop:TransmissionReference>/s', $fileString, $TransmissionReference);
        $TransmissionReference = $TransmissionReference[1];
        $TransmissionReference = str_replace("coverimg", "wenn", $TransmissionReference); */
      $TransmissionReference = $path['filename'];

      $photoshopSource = null;
      preg_match('/<photoshop:Source>(.*?)<\/photoshop:Source>/s', $fileString, $photoshopSource);
      $photoshopSource = $photoshopSource[1];

      $dcsubject = null;
      preg_match('/<dc:subject>(.*?)<\/dc:subject>/s', $fileString, $dcsubject);
      $dcsubject = $dcsubject[1];

      $dcdescription = null;
      preg_match('/<dc:description>(.*?)<\/dc:description>/s', $fileString, $dcdescription);
      $dcdescription = trim(strip_tags($dcdescription[1]));



      $dateKey = date("Y-m-d", strtotime($DateCreated));
      $headlineKey = sha1($headline . $dateKey);
      if (isset($fileArray[$headlineKey])) {
        $fileArray[$headlineKey]["file"][] = array("file" => trim($TransmissionReference), "dcdescription" => $dcdescription);
      } else {
        $fileArray[$headlineKey] = array();
        $fileArray[$headlineKey]["contant"] = $fileString;
        $fileArray[$headlineKey]["headline"] = trim($headline);
        $fileArray[$headlineKey]["photoshopSource"] = trim($photoshopSource);
        $fileArray[$headlineKey]["dcsubject"] = trim($dcsubject);
        $fileArray[$headlineKey]["dcdescription"] = trim($dcdescription);
        $fileArray[$headlineKey]["DateCreated"] = trim($DateCreated);
        $fileArray[$headlineKey]["file"][] = array("file" => trim($TransmissionReference), "dcdescription" => $dcdescription);
        //var_dump($fileArray);exit;
      }
    }
  }
  $contant = array();
  foreach ($fileArray as $key => &$item) {
    $article = "";
    $keyString = "<key>$key<key>";
    $headline = "<headline><![CDATA[{$item["headline"]}]]></headline>";
    $DateCreated = "<DateCreated>{$item["DateCreated"]}</DateCreated>";
    $media = "";
    foreach ($item["file"] as $itemMedia) {
      $media.="<container_media>";
      $media.="<media>{$itemMedia["file"]}.jpg</media>";
      $media.="<media_caption><![CDATA[{$itemMedia["dcdescription"]}.jpg]]></media_caption>";
      $media.="</container_media>";
    }
    $article = '<?xml version="1.0" encoding="iso-8859-1"?><item>' . $keyString . $headline . $DateCreated . $media . "</item>";
    $targetDir = $publiserDirName;


    foreach ($item["file"] as $itemMedia) {
      $file = $titleTargetDir . "{$itemMedia["file"]}.jpg";
      $delfile = $titleTargetDir . "{$itemMedia["file"]}.xmp";
      if (file_exists($file)) {
        $move = "mv " . $file . ' ' . $targetDir;
        echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
        shell_exec($move);
        $del = "rm " . $delfile;
        shell_exec($del);
        file_put_contents($targetDir . "$key.xml", $article);
      }
    }
  }
  $move = "mv  $titleTargetDir*.jpg  $not_processed";
  shell_exec($move);
  $move = "mv  $titleTargetDir*.xmp  $not_processed";
  shell_exec($move);
}

function moveWENNPhotos3829() {
  $fileArray = array();

  $titleTargetDir = '/syndigate/sources/home/1189/COVER_IMAGES_Film_and_TV_Stills_3829/';
  $publiserDirName = '/syndigate/sources/home/1189/3829/';
  $not_processed = "/syndigate/sources/home/1189/not_processed_3829/";
  //$move = "mv  $publiserDirName*.jpg  $titleTargetDir";
  //shell_exec($move);
  //$move = "mv  $publiserDirName*.xmp  $titleTargetDir";
  //shell_exec($move);
  $move = "";
  $directories = scandir($titleTargetDir);
  foreach ($directories as $file) {
    $path = pathinfo($file);
    if (empty($path['extension'])) {
      continue;
    }
    $path_file = $path['extension'];
    if (($file != '.') && ($file != '..') && (is_file("{$titleTargetDir}{$file}")) && strtolower($path_file) == "xmp") {
      $fileString = file_get_contents($titleTargetDir . $file);
      $headline = null;
      preg_match('/<photoshop:Headline>(.*?)<\/photoshop:Headline>/s', $fileString, $headline);
      $headline = $headline[1];

      $DateCreated = null;
      preg_match('/<photoshop:DateCreated>(.*?)<\/photoshop:DateCreated>/s', $fileString, $DateCreated);
      $DateCreated = $DateCreated[1];

      $TransmissionReference = null;
      preg_match('/<photoshop:TransmissionReference>(.*?)<\/photoshop:TransmissionReference>/s', $fileString, $TransmissionReference);
      $TransmissionReference = $TransmissionReference[1];
      $TransmissionReference = str_replace("coverimg", "wenn", $TransmissionReference);

      $photoshopSource = null;
      preg_match('/<photoshop:Source>(.*?)<\/photoshop:Source>/s', $fileString, $photoshopSource);
      $photoshopSource = $photoshopSource[1];

      $dcsubject = null;
      preg_match('/<dc:subject>(.*?)<\/dc:subject>/s', $fileString, $dcsubject);
      $dcsubject = $dcsubject[1];

      $dcdescription = null;
      preg_match('/<dc:description>(.*?)<\/dc:description>/s', $fileString, $dcdescription);
      $dcdescription = trim(strip_tags($dcdescription[1]));



      $dateKey = date("Y-m-d", strtotime($DateCreated));
      $headlineKey = sha1($headline . $dateKey);
      if (isset($fileArray[$headlineKey])) {
        $fileArray[$headlineKey]["file"][] = array("file" => trim($TransmissionReference), "dcdescription" => $dcdescription);
      } else {
        $fileArray[$headlineKey] = array();
        $fileArray[$headlineKey]["contant"] = $fileString;
        $fileArray[$headlineKey]["headline"] = trim($headline);
        $fileArray[$headlineKey]["photoshopSource"] = trim($photoshopSource);
        $fileArray[$headlineKey]["dcsubject"] = trim($dcsubject);
        $fileArray[$headlineKey]["dcdescription"] = trim($dcdescription);
        $fileArray[$headlineKey]["DateCreated"] = trim($DateCreated);
        $fileArray[$headlineKey]["file"][] = array("file" => trim($TransmissionReference), "dcdescription" => $dcdescription);
        //var_dump($fileArray);exit;
      }
    }
  }
  $contant = array();
  foreach ($fileArray as $key => &$item) {
    $article = "";
    $keyString = "<key>$key<key>";
    $headline = "<headline><![CDATA[{$item["headline"]}]]></headline>";
    $DateCreated = "<DateCreated>{$item["DateCreated"]}</DateCreated>";
    $media = "";
    foreach ($item["file"] as $itemMedia) {
      $media.="<container_media>";
      $media.="<media>{$itemMedia["file"]}.jpg</media>";
      $media.="<media_caption><![CDATA[{$itemMedia["dcdescription"]}.jpg]]></media_caption>";
      $media.="</container_media>";
    }
    $article = '<?xml version="1.0" encoding="iso-8859-1"?><item>' . $keyString . $headline . $DateCreated . $media . "</item>";
    $targetDir = $publiserDirName;


    foreach ($item["file"] as $itemMedia) {
      $file = $titleTargetDir . "{$itemMedia["file"]}.jpg";
      $delfile = $titleTargetDir . "{$itemMedia["file"]}.xmp";
      if (file_exists($file)) {
        $move = "mv " . $file . ' ' . $targetDir;
        echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
        shell_exec($move);
        $del = "rm " . $delfile;
        shell_exec($del);
        file_put_contents($targetDir . "$key.xml", $article);
      }
    }
  }
  $move = "mv  $titleTargetDir*.jpg  $not_processed";
  shell_exec($move);
  $move = "mv  $titleTargetDir*.xmp  $not_processed";
  shell_exec($move);
}

function moveCoverImagesPictureFeatures5690() {
  $fileArray = array();
  $PublisherTargetDir = '/syndigate/sources/home/1257/COVER Images - Picture Features (5690)/*';
  $PublisherTargetDir = str_replace(' ', "\ ", $PublisherTargetDir);
  $PublisherTargetDir = str_replace('(', "\(", $PublisherTargetDir);
  $PublisherTargetDir = str_replace(")", "\)", $PublisherTargetDir);
  $titleTargetDir = '/syndigate/sources/home/1257/cover_images_picture_features_5690/';

  $move = "mv  $PublisherTargetDir  $titleTargetDir";
  echo $move . PHP_EOL;
  shell_exec($move);

  $publiserDirName = '/syndigate/sources/home/1257/5690/';
  $not_processed = "/syndigate/sources/home/1257/not_processed_5690";

  $move = "";
  $directories = scandir($titleTargetDir);
  foreach ($directories as $file) {
    $path = pathinfo($file);
    if (empty($path['extension'])) {
      continue;
    }
    $path_file = $path['extension'];
    if (($file != '.') && ($file != '..') && (is_file("{$titleTargetDir}{$file}")) && strtolower($path_file) == "xmp") {
      $fileString = file_get_contents($titleTargetDir . $file);
      $headline = null;
      preg_match('/<photoshop:Headline>(.*?)<\/photoshop:Headline>/s', $fileString, $headline);
      $headline = $headline[1];

      $DateCreated = null;
      preg_match('/<photoshop:DateCreated>(.*?)<\/photoshop:DateCreated>/s', $fileString, $DateCreated);
      $DateCreated = $DateCreated[1];

      $TransmissionReference = null;
      /* preg_match('/<photoshop:TransmissionReference>(.*?)<\/photoshop:TransmissionReference>/s', $fileString, $TransmissionReference);
        $TransmissionReference = $TransmissionReference[1];
        $TransmissionReference = str_replace("coverimg", "wenn", $TransmissionReference); */
      $TransmissionReference = $path['filename'];

      $photoshopSource = null;
      preg_match('/<photoshop:Source>(.*?)<\/photoshop:Source>/s', $fileString, $photoshopSource);
      $photoshopSource = $photoshopSource[1];

      $dcsubject = null;
      preg_match('/<dc:subject>(.*?)<\/dc:subject>/s', $fileString, $dcsubject);
      $dcsubject = $dcsubject[1];

      $dcdescription = null;
      preg_match('/<dc:description>(.*?)<\/dc:description>/s', $fileString, $dcdescription);
      $dcdescription = trim(strip_tags($dcdescription[1]));



      $dateKey = date("Y-m-d", strtotime($DateCreated));
      $headlineKey = sha1($headline . $dateKey);
      if (isset($fileArray[$headlineKey])) {
        $fileArray[$headlineKey]["file"][] = array("file" => trim($TransmissionReference), "dcdescription" => $dcdescription);
      } else {
        $fileArray[$headlineKey] = array();
        $fileArray[$headlineKey]["contant"] = $fileString;
        $fileArray[$headlineKey]["headline"] = trim($headline);
        $fileArray[$headlineKey]["photoshopSource"] = trim($photoshopSource);
        $fileArray[$headlineKey]["dcsubject"] = trim($dcsubject);
        $fileArray[$headlineKey]["dcdescription"] = trim($dcdescription);
        $fileArray[$headlineKey]["DateCreated"] = trim($DateCreated);
        $fileArray[$headlineKey]["file"][] = array("file" => trim($TransmissionReference), "dcdescription" => $dcdescription);
      }
    }
  }
  $contant = array();
  foreach ($fileArray as $key => &$item) {
    $article = "";
    $keyString = "<key>$key<key>";
    $headline = "<headline><![CDATA[{$item["headline"]}]]></headline>";
    $DateCreated = "<DateCreated>{$item["DateCreated"]}</DateCreated>";
    $media = "";
    foreach ($item["file"] as $itemMedia) {
      $media.="<container_media>";
      $media.="<media>{$itemMedia["file"]}.jpg</media>";
      $media.="<media_caption><![CDATA[{$itemMedia["dcdescription"]}.jpg]]></media_caption>";
      $media.="</container_media>";
    }
    $article = '<?xml version="1.0" encoding="iso-8859-1"?><item>' . $keyString . $headline . $DateCreated . $media . "</item>";
    $targetDir = $publiserDirName;


    foreach ($item["file"] as $itemMedia) {
      $file = $titleTargetDir . "{$itemMedia["file"]}.jpg";
      $delfile = $titleTargetDir . "{$itemMedia["file"]}.xmp";
      if (file_exists($file)) {
        $move = "mv " . $file . ' ' . $targetDir;
        echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
        shell_exec($move);
        $del = "rm " . $delfile;
        shell_exec($del);
        file_put_contents($targetDir . "$key.xml", $article);
      }
    }
  }
  $move = "mv  $titleTargetDir*.jpg  $not_processed";
  shell_exec($move);
  $move = "mv  $titleTargetDir*.xmp  $not_processed";
  shell_exec($move);
}

function moveCoverImagesPictureFeatures5846() {
  $fileArray = array();
  $titleTargetDir = '/syndigate/sources/home/1257/COVER_Images_Picture_Features_Showbiz_5846/';
  $publiserDirName = '/syndigate/sources/home/1257/5846/';
  $not_processed = "/syndigate/sources/home/1257/not_processed_5846";

  $move = "";
  $directories = scandir($titleTargetDir);
  foreach ($directories as $file) {
    $path = pathinfo($file);
    if (empty($path['extension'])) {
      continue;
    }
    $path_file = $path['extension'];
    if (($file != '.') && ($file != '..') && (is_file("{$titleTargetDir}{$file}")) && strtolower($path_file) == "xmp") {
      $fileString = file_get_contents($titleTargetDir . $file);
      $headline = null;
      preg_match('/<photoshop:Headline>(.*?)<\/photoshop:Headline>/s', $fileString, $headline);
      $headline = $headline[1];

      $DateCreated = null;
      preg_match('/<photoshop:DateCreated>(.*?)<\/photoshop:DateCreated>/s', $fileString, $DateCreated);
      $DateCreated = $DateCreated[1];

      $TransmissionReference = null;
      /* preg_match('/<photoshop:TransmissionReference>(.*?)<\/photoshop:TransmissionReference>/s', $fileString, $TransmissionReference);
        $TransmissionReference = $TransmissionReference[1];
        $TransmissionReference = str_replace("coverimg", "wenn", $TransmissionReference); */
      $TransmissionReference = $path['filename'];

      $photoshopSource = null;
      preg_match('/<photoshop:Source>(.*?)<\/photoshop:Source>/s', $fileString, $photoshopSource);
      $photoshopSource = $photoshopSource[1];

      $dcsubject = null;
      preg_match('/<dc:subject>(.*?)<\/dc:subject>/s', $fileString, $dcsubject);
      $dcsubject = $dcsubject[1];

      $dcdescription = null;
      preg_match('/<dc:description>(.*?)<\/dc:description>/s', $fileString, $dcdescription);
      $dcdescription = trim(strip_tags($dcdescription[1]));



      $dateKey = date("Y-m-d", strtotime($DateCreated));
      $headlineKey = sha1($headline . $dateKey);
      if (isset($fileArray[$headlineKey])) {
        $fileArray[$headlineKey]["file"][] = array("file" => trim($TransmissionReference), "dcdescription" => $dcdescription);
      } else {
        $fileArray[$headlineKey] = array();
        $fileArray[$headlineKey]["contant"] = $fileString;
        $fileArray[$headlineKey]["headline"] = trim($headline);
        $fileArray[$headlineKey]["photoshopSource"] = trim($photoshopSource);
        $fileArray[$headlineKey]["dcsubject"] = trim($dcsubject);
        $fileArray[$headlineKey]["dcdescription"] = trim($dcdescription);
        $fileArray[$headlineKey]["DateCreated"] = trim($DateCreated);
        $fileArray[$headlineKey]["file"][] = array("file" => trim($TransmissionReference), "dcdescription" => $dcdescription);
        //var_dump($fileArray);exit;
      }
    }
  }
  $contant = array();
  foreach ($fileArray as $key => &$item) {
    $article = "";
    $keyString = "<key>$key<key>";
    $headline = "<headline><![CDATA[{$item["headline"]}]]></headline>";
    $DateCreated = "<DateCreated>{$item["DateCreated"]}</DateCreated>";
    $media = "";
    foreach ($item["file"] as $itemMedia) {
      $media.="<container_media>";
      $media.="<media>{$itemMedia["file"]}.jpg</media>";
      $media.="<media_caption><![CDATA[{$itemMedia["dcdescription"]}.jpg]]></media_caption>";
      $media.="</container_media>";
    }
    $article = '<?xml version="1.0" encoding="iso-8859-1"?><item>' . $keyString . $headline . $DateCreated . $media . "</item>";
    $targetDir = $publiserDirName;


    foreach ($item["file"] as $itemMedia) {
      $file = $titleTargetDir . "{$itemMedia["file"]}.jpg";
      $delfile = $titleTargetDir . "{$itemMedia["file"]}.xmp";
      if (file_exists($file)) {
        $move = "mv " . $file . ' ' . $targetDir;
        echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
        shell_exec($move);
        $del = "rm " . $delfile;
        shell_exec($del);
        file_put_contents($targetDir . "$key.xml", $article);
      }
    }
  }
  $move = "mv  $titleTargetDir*.jpg  $not_processed";
  shell_exec($move);
  $move = "mv  $titleTargetDir*.xmp  $not_processed";
  shell_exec($move);
}

function moveCoverImagesPictureFeatures5848() {
  $fileArray = array();
  $titleTargetDir = '/syndigate/sources/home/1257/COVER_Images_Picture_Features_Fashion_5848/';
  $publiserDirName = '/syndigate/sources/home/1257/5848/';
  $not_processed = "/syndigate/sources/home/1257/not_processed_5848";

  $move = "";
  $directories = scandir($titleTargetDir);
  foreach ($directories as $file) {
    $path = pathinfo($file);
    if (empty($path['extension'])) {
      continue;
    }
    $path_file = $path['extension'];
    if (($file != '.') && ($file != '..') && (is_file("{$titleTargetDir}{$file}")) && strtolower($path_file) == "xmp") {
      $fileString = file_get_contents($titleTargetDir . $file);
      $headline = null;
      preg_match('/<photoshop:Headline>(.*?)<\/photoshop:Headline>/s', $fileString, $headline);
      $headline = $headline[1];

      $DateCreated = null;
      preg_match('/<photoshop:DateCreated>(.*?)<\/photoshop:DateCreated>/s', $fileString, $DateCreated);
      $DateCreated = $DateCreated[1];

      $TransmissionReference = null;
      /* preg_match('/<photoshop:TransmissionReference>(.*?)<\/photoshop:TransmissionReference>/s', $fileString, $TransmissionReference);
        $TransmissionReference = $TransmissionReference[1];
        $TransmissionReference = str_replace("coverimg", "wenn", $TransmissionReference); */
      $TransmissionReference = $path['filename'];

      $photoshopSource = null;
      preg_match('/<photoshop:Source>(.*?)<\/photoshop:Source>/s', $fileString, $photoshopSource);
      $photoshopSource = $photoshopSource[1];

      $dcsubject = null;
      preg_match('/<dc:subject>(.*?)<\/dc:subject>/s', $fileString, $dcsubject);
      $dcsubject = $dcsubject[1];

      $dcdescription = null;
      preg_match('/<dc:description>(.*?)<\/dc:description>/s', $fileString, $dcdescription);
      $dcdescription = trim(strip_tags($dcdescription[1]));



      $dateKey = date("Y-m-d", strtotime($DateCreated));
      $headlineKey = sha1($headline . $dateKey);
      if (isset($fileArray[$headlineKey])) {
        $fileArray[$headlineKey]["file"][] = array("file" => trim($TransmissionReference), "dcdescription" => $dcdescription);
      } else {
        $fileArray[$headlineKey] = array();
        $fileArray[$headlineKey]["contant"] = $fileString;
        $fileArray[$headlineKey]["headline"] = trim($headline);
        $fileArray[$headlineKey]["photoshopSource"] = trim($photoshopSource);
        $fileArray[$headlineKey]["dcsubject"] = trim($dcsubject);
        $fileArray[$headlineKey]["dcdescription"] = trim($dcdescription);
        $fileArray[$headlineKey]["DateCreated"] = trim($DateCreated);
        $fileArray[$headlineKey]["file"][] = array("file" => trim($TransmissionReference), "dcdescription" => $dcdescription);
        //var_dump($fileArray);exit;
      }
    }
  }
  $contant = array();
  foreach ($fileArray as $key => &$item) {
    $article = "";
    $keyString = "<key>$key<key>";
    $headline = "<headline><![CDATA[{$item["headline"]}]]></headline>";
    $DateCreated = "<DateCreated>{$item["DateCreated"]}</DateCreated>";
    $media = "";
    foreach ($item["file"] as $itemMedia) {
      $media.="<container_media>";
      $media.="<media>{$itemMedia["file"]}.jpg</media>";
      $media.="<media_caption><![CDATA[{$itemMedia["dcdescription"]}.jpg]]></media_caption>";
      $media.="</container_media>";
    }
    $article = '<?xml version="1.0" encoding="iso-8859-1"?><item>' . $keyString . $headline . $DateCreated . $media . "</item>";
    $targetDir = $publiserDirName;


    foreach ($item["file"] as $itemMedia) {
      $file = $titleTargetDir . "{$itemMedia["file"]}.jpg";
      $delfile = $titleTargetDir . "{$itemMedia["file"]}.xmp";
      if (file_exists($file)) {
        $move = "mv " . $file . ' ' . $targetDir;
        echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
        shell_exec($move);
        $del = "rm " . $delfile;
        shell_exec($del);
        file_put_contents($targetDir . "$key.xml", $article);
      }
    }
  }
  $move = "mv  $titleTargetDir*.jpg  $not_processed";
  shell_exec($move);
  $move = "mv  $titleTargetDir*.xmp  $not_processed";
  shell_exec($move);
}

function moveCoverImagesPictureFeatures5850() {
  $fileArray = array();
  $titleTargetDir = '/syndigate/sources/home/1257/COVER_Images_Picture_Features_Fitness_5850/';
  $publiserDirName = '/syndigate/sources/home/1257/5850/';
  $not_processed = "/syndigate/sources/home/1257/not_processed_5850";

  $move = "";
  $directories = scandir($titleTargetDir);
  foreach ($directories as $file) {
    $path = pathinfo($file);
    if (empty($path['extension'])) {
      continue;
    }
    $path_file = $path['extension'];
    if (($file != '.') && ($file != '..') && (is_file("{$titleTargetDir}{$file}")) && strtolower($path_file) == "xmp") {
      $fileString = file_get_contents($titleTargetDir . $file);
      $headline = null;
      preg_match('/<photoshop:Headline>(.*?)<\/photoshop:Headline>/s', $fileString, $headline);
      $headline = $headline[1];

      $DateCreated = null;
      preg_match('/<photoshop:DateCreated>(.*?)<\/photoshop:DateCreated>/s', $fileString, $DateCreated);
      $DateCreated = $DateCreated[1];

      //$TransmissionReference = null;
      //preg_match('/<photoshop:TransmissionReference>(.*?)<\/photoshop:TransmissionReference>/s', $fileString, $TransmissionReference);
      //$TransmissionReference = $TransmissionReference[1];
      $TransmissionReference = $path['filename'];

      $photoshopSource = null;
      preg_match('/<photoshop:Source>(.*?)<\/photoshop:Source>/s', $fileString, $photoshopSource);
      $photoshopSource = $photoshopSource[1];

      $dcsubject = null;
      preg_match('/<dc:subject>(.*?)<\/dc:subject>/s', $fileString, $dcsubject);
      $dcsubject = $dcsubject[1];

      $dcdescription = null;
      preg_match('/<dc:description>(.*?)<\/dc:description>/s', $fileString, $dcdescription);
      $dcdescription = trim(strip_tags($dcdescription[1]));



      $dateKey = date("Y-m-d", strtotime($DateCreated));
      $headlineKey = sha1($headline . $dateKey);
      if (isset($fileArray[$headlineKey])) {
        $fileArray[$headlineKey]["file"][] = array("file" => trim($TransmissionReference), "dcdescription" => $dcdescription);
      } else {
        $fileArray[$headlineKey] = array();
        $fileArray[$headlineKey]["contant"] = $fileString;
        $fileArray[$headlineKey]["headline"] = trim($headline);
        $fileArray[$headlineKey]["photoshopSource"] = trim($photoshopSource);
        $fileArray[$headlineKey]["dcsubject"] = trim($dcsubject);
        $fileArray[$headlineKey]["dcdescription"] = trim($dcdescription);
        $fileArray[$headlineKey]["DateCreated"] = trim($DateCreated);
        $fileArray[$headlineKey]["file"][] = array("file" => trim($TransmissionReference), "dcdescription" => $dcdescription);
        //var_dump($fileArray);exit;
      }
    }
  }
  $contant = array();
  foreach ($fileArray as $key => &$item) {
    $article = "";
    $keyString = "<key>$key<key>";
    $headline = "<headline><![CDATA[{$item["headline"]}]]></headline>";
    $DateCreated = "<DateCreated>{$item["DateCreated"]}</DateCreated>";
    $media = "";
    foreach ($item["file"] as $itemMedia) {
      $media.="<container_media>";
      $media.="<media>{$itemMedia["file"]}.jpg</media>";
      $media.="<media_caption><![CDATA[{$itemMedia["dcdescription"]}.jpg]]></media_caption>";
      $media.="</container_media>";
    }
    $article = '<?xml version="1.0" encoding="iso-8859-1"?><item>' . $keyString . $headline . $DateCreated . $media . "</item>";
    $targetDir = $publiserDirName;


    foreach ($item["file"] as $itemMedia) {
      $file = $titleTargetDir . "{$itemMedia["file"]}.jpg";
      $delfile = $titleTargetDir . "{$itemMedia["file"]}.xmp";
      if (file_exists($file)) {
        $move = "mv " . $file . ' ' . $targetDir;
        echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
        shell_exec($move);
        $del = "rm " . $delfile;
        shell_exec($del);
        file_put_contents($targetDir . "$key.xml", $article);
      }
    }
  }
  $move = "mv  $titleTargetDir*.jpg  $not_processed";
  shell_exec($move);
  $move = "mv  $titleTargetDir*.xmp  $not_processed";
  shell_exec($move);
}
