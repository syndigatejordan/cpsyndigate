<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

/*
 * '' => array('Title'=>'','Edition'=>''),
 */
$sarkeInformationAgencyID = array(
    '1168' => array('Title' => 'Sarke Daily News'),
    '1169' => array('Title' => 'Sarke Ежедневные Новости'),
    '1171' => array('Title' => 'Sarke Economic Press Monitor'),
    '1172' => array('Title' => 'Sarke Монитор Экономических Публикаций'),
    '2796' => array('Title' => 'Sarke Special Report'),
    '2797' => array('Title' => 'Sarke Специальное Обозрение'),
);
//Fix File name
$publiserDirName = '/syndigate/sources/home/352/';
$directories = scandir($publiserDirName);
foreach ($directories as $File) {
  $path = pathinfo($File);
  if (empty($path['extension'])) {
    continue;
  }
  $path_file = $path['extension'];
  if (($File != '.') && ($File != '..') && (is_file("{$publiserDirName}{$File}")) && strtolower($path_file) == "txt") {
    $path["basename"] = preg_replace("/[  ~&,:\-\)\(\']/", ' ', $path["basename"]);
    $path["basename"] = str_replace('..', '.', $path["basename"]);
    $path["basename"] = str_replace("'", "\'", $path["basename"]);
    $path["basename"] = str_replace("\'", "", $path["basename"]);
    $path["basename"] = str_replace("`", "\`", $path["basename"]);
    $path["basename"] = str_replace("\`", "", $path["basename"]);
    $path["basename"] = str_replace("\$", "\\$", $path["basename"]);
    $path["basename"] = str_replace("\$", "", $path["basename"]);
    $path["basename"] = str_replace('  ', ' ', $path["basename"]);
    $path["basename"] = preg_replace('!\s+!', '_', $path["basename"]);
    $path["basename"] = str_replace('._', '_', $path["basename"]);
    $path["basename"] = str_replace(' _', '_', $path["basename"]);
    $File = str_replace(' ', '\ ', $File);
    $File = str_replace('(', '\(', $File);
    $File = str_replace(')', '\)', $File);
    $File = str_replace("'", "\'", $File);
    $File = str_replace("`", "\`", $File);
    $File = str_replace("&", "\&", $File);
    $File = str_replace("\$", "\\$", $File);
    $move = "mv " . $publiserDirName . $File . " " . $publiserDirName . $path["basename"];
    if ($File != $path["basename"]) {
      echo $move . chr(10);
      shell_exec($move);
    }
  }
}
//Moving all the files in the publisher directory to title directory 
$pubId = 352;
$files = getDirectoryFiles($publiserDirName);
foreach ($files as $file) {
  if (is_file($file)) {
    $filetype = pathinfo($file);
    $fileString = file_get_contents($file);
    $fileString=iconv("UTF-16","UTF-8", $fileString);
    preg_match('/<PUBLICATION>(.*?)<\/PUBLICATION>/is', $fileString, $Title);

    if (count($Title) > 1) {
      $Title = trim(strtolower($Title[1]));
    }
    foreach ($sarkeInformationAgencyID as $title_id => $source) {
      $sTitle = $source["Title"];
      if ($Title == strtolower($sTitle)) {

        $targetDir = "/syndigate/sources/home/$pubId/" . $title_id . date('/Y/m/d/');
        if (!is_dir($targetDir)) {
          mkdir($targetDir, '0775', true);
        }
        $move = chr(10) . "mv " . $file . ' ' . $targetDir;
        echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
        shell_exec($move);
      }
    }
  }
}

// functions -----------
function getDirectoryFiles($dir) {
  $files = array();
  if ($handle = opendir($dir)) {
    while (false !== ($file = readdir($handle))) {
      if (($file != '.') && ($file != '..')) {
        if (is_file($dir . $file)) {
          $files[] = $dir . $file;
        }
      }
    }
    closedir($handle);
    return $files;
  } else {
    echo "invalid directory $dir \n";
    return false;
  }
}

function getDirectoryContent($dir) {
  $files = array();
  if ($handle = opendir($dir)) {
    while (false !== ($file = readdir($handle))) {
      if (($file != '.') && ($file != '..')) {
        $files[] = $dir . $file;
      }
    }
    closedir($handle);
    return $files;
  } else {
    echo "invalid directory $dir \n";
    return false;
  }
}
function ru2lat($str)
{
    $tr = array(
    "А"=>"a", "Б"=>"b", "В"=>"v", "Г"=>"g", "Д"=>"d",
    "Е"=>"e", "Ё"=>"yo", "Ж"=>"zh", "З"=>"z", "И"=>"i", 
    "Й"=>"j", "К"=>"k", "Л"=>"l", "М"=>"m", "Н"=>"n", 
    "О"=>"o", "П"=>"p", "Р"=>"r", "С"=>"s", "Т"=>"t", 
    "У"=>"u", "Ф"=>"f", "Х"=>"kh", "Ц"=>"ts", "Ч"=>"ch", 
    "Ш"=>"sh", "Щ"=>"sch", "Ъ"=>"", "Ы"=>"y", "Ь"=>"", 
    "Э"=>"e", "Ю"=>"yu", "Я"=>"ya", "а"=>"a", "б"=>"b", 
    "в"=>"v", "г"=>"g", "д"=>"d", "е"=>"e", "ё"=>"yo", 
    "ж"=>"zh", "з"=>"z", "и"=>"i", "й"=>"j", "к"=>"k", 
    "л"=>"l", "м"=>"m", "н"=>"n", "о"=>"o", "п"=>"p", 
    "р"=>"r", "с"=>"s", "т"=>"t", "у"=>"u", "ф"=>"f", 
    "х"=>"kh", "ц"=>"ts", "ч"=>"ch", "ш"=>"sh", "щ"=>"sch", 
    "ъ"=>"", "ы"=>"y", "ь"=>"", "э"=>"e", "ю"=>"yu", 
    "я"=>"ya", " "=>"-", "."=>"", ","=>"", "/"=>"-",  
    ":"=>"", ";"=>"","—"=>"", "–"=>"-"
    );
return strtr($str,$tr);
}
function remove_utf8_bom($str)
{
   if ($bytes = substr($str, 0, 3) && $bytes === "\xEF\xBB\xBF") 
   {
       $str = substr($str, 3);
   }
   return $str;
}