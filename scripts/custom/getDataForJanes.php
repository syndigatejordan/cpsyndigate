<?php
$token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjEzODk4MTUiLCJuYmYiOjE2MTgzOTE3NDIsImV4cCI6MTY0OTkyNzc0MiwiaXNzIjoiSmFuZXMiLCJhdWQiOiIxMzg5ODE1In0.CrIL7da4DGnQy5RkV9vBonTKqgUi04Ax14Aibt1IZZo";
$cURLConnection = curl_init();
curl_setopt($cURLConnection, CURLOPT_URL, 'https://developer.janes.com/api/v1/news?num=50');
curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
curl_setopt($cURLConnection, CURLOPT_USERAGENT, 'Mozilla/5.0 (Link Checker)');
curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
    'Accept: application/json',
    "Authorization:$token",
    'https://developer.janes.com/api/v1/news'

));
$response = curl_exec($cURLConnection);
curl_close($cURLConnection);
$contents = json_decode($response, true);
foreach ($contents["results"] as $item) {
    echo "Get article for id {$item['id']}" . PHP_EOL;
    $article = fullArticle($item['id'], $token);
    $cat = $item['id'];
    $date = date('Y/m/d/');
    $directory = "/syndigate/sources/home/1518/8927/$date";
    if (!is_dir($directory)) {
        mkdir($directory, 0777, true);
    }
    $fileName = $directory . str_replace(' ', '_', $cat . '.xml');
    echo "$fileName" . PHP_EOL;
    file_put_contents($fileName, $article);
}


function fullArticle($id, $token)
{
    $cURLConnection = curl_init();
    curl_setopt($cURLConnection, CURLOPT_URL, "https://developer.janes.com/api/v1/news/$id");
    curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($cURLConnection, CURLOPT_USERAGENT, 'Mozilla/5.0 (Link Checker)');
    curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
        'Accept: application/json',
        "Authorization:$token",
        "https://developer.janes.com/api/v1/news/$id"

    ));
    $response = curl_exec($cURLConnection);
    curl_close($cURLConnection);
    $contents = json_decode($response, true);
    return $contents;
}