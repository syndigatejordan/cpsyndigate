<?php


require_once '/usr/local/syndigate/scripts/propel_include_config.php';
// Check if an instance exists from the same scripts
$avilable_instances = 0;
$output = shell_exec('ps aux | grep moveNewstexLowLatency.php | grep -v "/bin/sh -c" | grep -v "sh -c" | grep -v "grep" ');
$output = explode(PHP_EOL, $output);
foreach ($output as $o) {
    if ($o) {
        $avilable_instances++;
    }
}

if ($avilable_instances > 1) {
    die("Job is already running...exit!" . PHP_EOL);
}

$newstexID = array(
    10009720 => 10361,
    10009344 => 10418,
    10009530 => 10382,
    10009434 => 10372,
    10009472 => 10370,
    10009748 => 10385,
    10009747 => 10386,
    10009428 => 10817,
    10009529 => 11194,
    10009448 => 10388,
    10009528 => 10828,
    10009473 => 10371,
    129714 => 10420,
//    10009980 => 12304,
);
$titles=array();
//Fix File name
$publiserDirName = '/syndigate/sources/home/1519/';

//Moving all the files in the publisher directory to title directory
$pubId = 1519;
$files = getDirectoryFiles($publiserDirName);
foreach ($files as $file) {
    if (is_file($file)) {
        $filetype = pathinfo($file);
        $fileString = file_get_contents($file);
        if (preg_match('/<NewstexID>(.*?)<\/NewstexID>/is', $fileString, $Title)) {
            $Title = (int)$Title[1];
            if (isset($newstexID[$Title])) {
                $title_id = $newstexID[$Title];
                $titles[]=$title_id;
            } else {
                $targetDir = "/syndigate/sources/home/$pubId/unprocessed/";
                //$move = chr(10) . "mv " . $file . ' ' . $targetDir;
                //echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
                //shell_exec($move);
                // $delete = chr(10) . "rm " . $file;
                //echo "Delete the file from the publisher directory " . $delete . chr(10);
                ///shell_exec($delete);
                continue;
            }
            $targetDir = "/syndigate/sources/home/$pubId/" . $title_id . date('/Y/m/d/');
            if (!is_dir($targetDir)) {
                mkdir($targetDir, '0775', true);
            }
            $move = chr(10) . "mv " . $file . ' ' . $targetDir;
            echo "Moving the file from the publisher directory to title directory " . $move . chr(10);
            shell_exec($move);
        }
    }
}
foreach ($titles as $title) {
    $d = date("[Y-m-d H:i:s]");
    $cmd = "/usr/local/syndigate/classes/gather/syndGather.php $title";
    echo "$d  Title ID: $title" . chr(10);
    shell_exec($cmd);
}
//deleteOldFiles(3);
// functions -----------
function getDirectoryFiles($dir)
{
    $files = array();
    if ($handle = opendir($dir)) {
        while (false !== ($file = readdir($handle))) {
            if (($file != '.') && ($file != '..')) {
                if (is_file($dir . $file)) {
                    $files[] = $dir . $file;
                }
            }
        }
        closedir($handle);
        return $files;
    } else {
        echo "invalid directory $dir \n";
        return false;
    }
}

function getDirectoryContent($dir)
{
    $files = array();
    if ($handle = opendir($dir)) {
        while (false !== ($file = readdir($handle))) {
            if (($file != '.') && ($file != '..')) {
                $files[] = $dir . $file;
            }
        }
        closedir($handle);
        return $files;
    } else {
        echo "invalid directory $dir \n";
        return false;
    }
}

function deleteOldFiles($days)
{
    $targetDir = "/syndigate/sources/home/1519/unprocessed/";
    $cmd = "find $targetDir -type f -mtime +$days  -exec rm {} \;";
    echo $cmd . PHP_EOL;
    shell_exec($cmd);
}
