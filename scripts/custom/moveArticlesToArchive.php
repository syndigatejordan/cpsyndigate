<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

class moveArticlesToArchive {

  public $checkArticle = FALSE;
  public $checkArticleOriginalData = FALSE;
  public $checkImage = FALSE;
  public $checkVideo = FALSE;
  public $articleDelete = array();
  public $articleOriginalDataDelete = array();
  public $articleId = "";
  public $articleOriginalDataid = "";

  public function __construct() {
    echo "Starting: " . date("Y-m-d H:i:s") . " " . PHP_EOL;
    $this->articleDelete = array();
    $this->articleOriginalDataDelete = array();
    $this->articleId = "";
    $this->articleOriginalDataid = "";
    $this->getArticlesData();
    $this->insertArticleOriginalData();
    $this->insertArticle();
    $this->insertImage();
    $this->insertVideo();
    $this->DeleteData();
    echo "End: " . date("Y-m-d H:i:s") . " " . PHP_EOL;
  }

  public function connect() {
    
  }

  public function getArticlesData() {
    $year = date('Y-m-d H:i:s', strtotime('1 month ago'));
    $q = "select id,article_original_data_id from article "
            . "where  id not in (select article_id from ignore_article) and parsed_at < \"$year\" limit 1000;";
    echo ($q) . PHP_EOL;
    $query = mysql_query($q);
    while ($row = mysql_fetch_assoc($query)) {
      array_push($this->articleOriginalDataDelete, $row['article_original_data_id']);
      array_push($this->articleDelete, $row['id']);
    }
    if (count($this->articleOriginalDataDelete) < 1 && count($this->articleDelete) < 1) {
      echo "No Articles" . PHP_EOL;
      exit;
    }
    $this->articleOriginalDataid = implode(',', $this->articleOriginalDataDelete);
    $this->articleId = implode(',', $this->articleDelete);
  }

  public function insertArticleOriginalData() {
    $sql = "";
    $sql = "insert into article_original_data_tmp ("
            . "id,original_article_id,original_source,issue_number,"
            . "page_number,reference,extras,hijri_date,original_cat_id,revision_num)"
            . "  SELECT * from article_original_data where id in ($this->articleOriginalDataid )";
    $result = mysql_query($sql);
    if ($result) {
      echo "INSERT article_original_data_archive successfully" . PHP_EOL;
      $this->checkArticleOriginalData = TRUE;
    } else {
      echo "INSERT article_original_data_archive Failure" . PHP_EOL;
      $this->checkArticleOriginalData = FALSE;
    }
  }

  public function insertArticle() {
    $sql = "";
    $sql = "insert into article_tmp ("
            . "id,iptc_id,title_id,article_original_data_id,language_id,headline,summary,body,"
            . "author,date,parsed_at,updated_at,has_time,is_Calias_called,sub_feed)"
            . "  SELECT * from article where id in ($this->articleId)";
    $result = mysql_query($sql);
    if ($result) {
      echo "INSERT article_archive successfully" . PHP_EOL;
      $this->checkArticle = TRUE;
    } else {
      echo "INSERT article_archive  Failure" . PHP_EOL;
      $this->checkArticle = FALSE;
    }
  }

  public function insertImage() {
    $sql = "";
    $sql = "insert into  image_tmp ("
            . "id,article_id,img_name,image_caption,is_headline,image_type,"
            . "mime_type,original_name,image_original_key) "
            . "  SELECT * from image where article_id in ($this->articleId)";
    $result = mysql_query($sql);
    if (empty($this->articleId)) {
      $this->checkImage = TRUE;
      echo "No images" . PHP_EOL;
    } else {
      if ($result) {
        echo "INSERT image_archive successfully" . PHP_EOL;
        $this->checkImage = TRUE;
      } else {
        echo "INSERT image_archive Failure" . PHP_EOL;
        $this->checkImage = FALSE;
      }
    }
  }

  public function insertVideo() {
    $sql = "";
    $sql = "insert into  video_tmp "
            . "(id,article_id,video_name,video_caption,original_name,video_type,bit_rate,added_time,mime_type)"
            . "  SELECT * from video where article_id in ($this->articleId)";
    $result = mysql_query($sql);
    if (empty($this->articleId)) {
      $this->checkVideo = TRUE;
      echo "No video" . PHP_EOL;
    } else {
      if ($result) {
        echo "INSERT video_archive successfully" . PHP_EOL;
        $this->checkVideo = TRUE;
      } else {
        echo "INSERT video_archive  Failure" . PHP_EOL;
        $this->checkVideo = FALSE;
      }
    }
  }

  public function DeleteData() {

    if ($this->checkArticle && $this->checkArticleOriginalData) {
      echo "DELETE ...... " . PHP_EOL;
      mysql_query("DELETE FROM video WHERE article_id in ($this->articleId)");
      mysql_query("DELETE FROM image WHERE article_id in ($this->articleId)");
      mysql_query("DELETE FROM article WHERE id in ($this->articleId)");
      mysql_query("DELETE FROM article_original_data WHERE id in ($this->articleOriginalDataid)");
    } else {
      mysql_query("INSERT INTO ignore_article (article_id)  SELECT id from article where id in ($this->articleId)");
    }
  }

}

// Check if an instance exists from the same scripts 
$avilable_instances = 0;
$output = shell_exec('ps aux | grep moveArticlesToArchive.php | grep -v "/bin/sh -c" | grep -v "sh -c" | grep -v "grep" ');
$output = explode(PHP_EOL, $output);
foreach ($output as $o) {
  if ($o) {
    $avilable_instances++;
  }
}
if ($avilable_instances > 1) {
  die("Job is already running...exit!" . PHP_EOL);
}

for ($i = 0; $i < 1000; $i++) {
  $t = 1000 - $i;
  echo "There are still $t movements ............ " . PHP_EOL;
  $check_ob = new moveArticlesToArchive();
  sleep(1);
}
