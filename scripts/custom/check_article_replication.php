<?php 
require_once '/usr/local/syndigate/scripts/propel_include_config.php';
$mailTo = array('khaled@syndigate.info','eyad@syndigate.info','majdi@syndigate.info','safaa@syndigate.info');
//---------------------------------------------------------------------------

$repeatedArticles = array();

$c = new Criteria();
$c->add(TitlePeer::IS_ACTIVE, 1);
$titles = TitlePeer::doSelect($c);

foreach ($titles as $title) {
	$date = date('Y-m-d');

	$rez  = mysql_query("SELECT headline, COUNT(headline) AS cnt FROM article WHERE title_id = " . $title->getId() ." AND parsed_at > '$date' GROUP BY headline ORDER BY cnt DESC");
	
	while ($row = mysql_fetch_assoc($rez)) {
		if(1 == $row['cnt']) {
			break;
		}
		//$repeatedArticles[$title->getId()][] = array($row['headline']=>$row['cnt']); 
		$repeatedArticles[$title->getId()] [$row['headline']]= $row['cnt']; 
	}
}

print_r($repeatedArticles);
$mailbody = generateReport($repeatedArticles);
sendEmail($mailbody);


// ------ functions

function sendEmail($body)
{
	global $mailTo;

	$mail = new ezcMailComposer();	
	$mail->from = new ezcMailAddress( 'report@syndigate.info', 'No reply' );
	
	foreach($mailTo as $to) {
	 	$mail->addTo( new ezcMailAddress($to) );
	 }

	$mail->subject   = "Syndigate repeated articles ";
	$mail->htmlText  = $body;
	
	
	$mail->build();
	$transport = new ezcMailMtaTransport();
	$transport->send( $mail ); 
}


function generateReport($repeatedArticles = array()) {
	$string = '';
	
	if($repeatedArticles) {
		foreach ($repeatedArticles as $titleId =>$articles) {
			$string.= $titleId . '<br />';
			$string.= "<table border='1'>\n";
			foreach ($articles as $headline => $frequency) {
				$string.= "<tr>\n";
				$string.="<td>$headline</td>\n";
				$string.="<td>$frequency</td>\n";
				$string.= "</tr>\n";
			}
			$string.= "</table><br />\n<br />\n<br />\n";			
		}
	}
	return $string;
}
