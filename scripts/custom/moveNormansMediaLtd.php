<?php
require_once '/usr/local/syndigate/scripts/propel_include_config.php';

$publiserDirName = '/syndigate/sources/home/1605/';
$titles = array(
    "Pharma_" => 11951,
    "NBR_" => 11952,
    "AII_" => 11953,
    "TWW_" => 11954,
    "EQB_" => 11955,
    "BCN_" => 11956,
    "M2BB_" => 11957,
    "IBN_" => 11958,
    "CIU_" => 11959,
    "WCPN_" => 11960,
    "MAN_" => 11961,
);
foreach ($titles as $key => $title) {
    $targetDir = $publiserDirName . $title . "/";
    $move = chr(10) . "mv {$publiserDirName}$key*  " . $targetDir;
    echo "Moving the file from the publisher directory to title directory " .PHP_EOL;
    echo $move . chr(10);
    shell_exec($move);
}