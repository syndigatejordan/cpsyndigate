<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
require_once '/usr/local/syndigate/ez_autoload.php';

$mailTo = array(
    'mark@syndigate.info',
    'eyad@syndigate.info',
    'khaled@syndigate.info',
    'majdi@syndigate.info',
    'shah.raju@syndigate.info',
    "rhea.tibrewala@citi.com",
    'rheatibrewala@gmail.com',
);
$insert = FALSE;

$id = getMaxId();
if (is_null($id)) {
  $insert = TRUE;
  $id = 1;
}
$send = FALSE;
$output = getArticles($id);
if (!empty($output)) {
  echo "Save file \n\n";
  $file = file_put_contents("/usr/local/syndigate/scripts/reports/wealthMonitorTOCitibank.txt", $output);
  $send = TRUE;
}
if ($send) {
  echo "Sent file \n\n";
  sendEmail("/usr/local/syndigate/scripts/reports/wealthMonitorTOCitibank.txt", $mailTo);
}
$maxid = $id;
$query = mysql_query("select max(id) from article where title_id =3072");
while ($row = mysql_fetch_assoc($query)) {
  $maxid = $row["max(id)"];
}
if (!$insert) {
  mysql_query("update max set article_id=$maxid where title_id=3072 and client_id=111; ");
} else {
  mysql_query("insert into max (title_id,client_id,article_id) values(3072,111,$maxid)");
}

function getMaxId() {
  $article_id = NULL;
  $query = mysql_query("select * from max where title_id =3072 and client_id=111;");
  while ($row = mysql_fetch_assoc($query)) {
    $article_id = $row["article_id"];
  }
  return $article_id;
}

function getArticles($id) {
  $articles = array();
  $output = "";

  $query = mysql_query("select headline, summary, author, date, body, reference, cat_name "
          . "from article a inner join article_original_data o  on a.article_original_data_id=o.id "
          . "left join original_article_category c on o.original_cat_id=c.id where a.title_id=3072 and a.id > $id");
  $i = 0;
  while ($row = mysql_fetch_row($query)) {
    $articles[$i]['headline'] = $row[0];
    $articles[$i]['summary'] = $row[1];
    $articles[$i]['author'] = $row[2];
    $articles[$i]['date'] = $row[3];
    $articles[$i]['body'] = $row[4];
    $articles[$i]['reference'] = $row[5];
    $articles[$i]['cat_name'] = $row[6];
    $i++;
  }
  foreach ($articles as $article) {
    $date = date('F j, Y', strtotime($article['date']));
    $body = preg_replace('/(?:\s\s+)/', ' ', $article['body']);
    $body = preg_replace('/<\/p>/', "\r\n", $body);
    $body = preg_replace("/<(.+?)[\s]*\/?[\s]*>/si", " ", $body);
    $body = preg_replace("/(?<!\\n)\\r+(?!\\n)/", "\r\n", $body); //replace just CR with CRLF
    $body = preg_replace("/(?<!\\r)\\n+(?!\\r)/", "\r\n", $body); //replace just LF with CRLF
    $body = preg_replace("/(?<!\\r)\\n\\r+(?!\\n)/", "\r\n", $body); //replace misordered LFCR with CRLF

    $orginalCat = $article['cat_name'];
    $orginalLink = $article['reference'];
    $headline = $article['headline'];
    $summary = $article['summary'];
    $author = $article['author'];

    $output .= $headline . "\r\n" . "\r\n" .
            "Category: " . $orginalCat . "\r\n" .
            "Summary: " . $summary . "\r\n" .
            "Author: " . $author . "\r\n" .
            "Article link: " . $orginalLink . "\r\n" .
            "Date: " . $date . "\r\n" . "\r\n" .
            $body . "\r\n" .
            "____________________" . "\r\n " . "\r\n";
  }
  return $output;
}

// --- Functions -------------------------------------------------------------------------//
function sendEmail($file, $mailTo = array()) {
  if (!is_array($mailTo)) {
    $mailTo = array($mailTo);
  }
  $mail = new ezcMailComposer();
  $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply');

  foreach ($mailTo as $to) {
    $mail->addTo(new ezcMailAddress($to));
  }
  $mail->subject = "Wealth Monitor";
  $mail->htmlText = "Dear Ms. Rhea,<br />
                <p>Please find attached the daily content of Wealth Monitor.</p>
                <p>Thank you very much,</p>
                <p>The SyndiGate team.</p>";
  $mail->addAttachment($file);
  $mail->build();

  $transport = new ezcMailMtaTransport();
  $transport->send($mail);
  echo "Sent\n\n";
}
