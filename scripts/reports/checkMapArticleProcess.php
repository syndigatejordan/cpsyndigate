<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

$mailTo = array('khaled@syndigate.info', 'eyad@syndigate.info', 'majdi@syndigate.info');
//-------------------------------------- Main--------------------------------------------------
$body = "";
//////////////////////////////// Global Data Point  ////////////////////////////////////////
$countGDP = getCount('article_jumperGDP');
$tableGDP = getDialyArticles('article_jumperGDP');
if ($countGDP > 500) {
  $body.="<p>The below table for delay send map articles from Global Data Point ,</p>";
  $body.=$tableGDP;
  $body.="<p>Count articles delay $countGDP .</p>";
}
/////////////////////////////////  Tenders info  ///////////////////////////////////////
$countTenders = getCount('article_jumper');
$tableTenders = getDialyArticles('article_jumper');
if ($countTenders > 500) {
  $body.="<p>The below table for delay send map articles from Tenders info ,</p>";
  $body.=$tableTenders;
  $body.="<p>Count articles delay $countTenders .</p>";
}
if (!empty($body)) {
  sendEmail($body, $mailTo);
}

function getCount($tableName) {
  $count = 0;
  $row = array();
  $result = mysql_query("select count(*) cnt from $tableName where is_sent=0 ;");
  $row = mysql_fetch_assoc($result);
  if (is_null($row["cnt"])) {
    $count = '0';
  } else {
    $count = $row["cnt"];
  }
  return $count;
}

function getDialyArticles($tableName) {
  $table = "";
  $articles = array();
  $query = mysql_query("select date(mapped_date) mappedDate ,count(*) count  from $tableName where is_sent=0 group by date(mapped_date);");
  $i = 0;
  while ($row = mysql_fetch_row($query)) {
    $articles[$i]['mappedDate'] = $row[0];
    $articles[$i]['count'] = $row[1];
    $i++;
  }
  $table .= "<table border='1'>"
          . "<tr><th>Mapped Date</th>"
          . "<th>Count</th>"
          . "</tr>";
  foreach ($articles as $article) {
    $table .= "<tr><td>{$article['mappedDate']}</td>";
    $table .= "<td>{$article['count']}</td>";
  }
  $table .="</table>";
  return $table;
}

function sendEmail($body, $mailTo) {
  $mail = new ezcMailComposer();
  $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply');
  foreach ($mailTo as $to) {
    $mail->addTo(new ezcMailAddress($to));
  }
  $mail->subject = "Delay Send Articles from Map Report,";
  $mail->htmlText = $body;
  //$mail->plainText = $body;
  $mail->build();
  $transport = new ezcMailMtaTransport();
  $transport->send($mail);
}
