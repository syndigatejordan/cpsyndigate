<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';

$mailTo = array('eyad@syndigate.info');
//-------------------------------------- Main--------------------------------------------------

$title_id = getTitleId();
$table = getCountInfo($title_id, $date);

$msg = "";
sendEmail($msg, $mailTo, $table);

function getTitleId() {
  $query = mysql_query("select id,name from title where title_frequency_id=1 and is_active=1;");
  $title_id = array();
  $i = 0;
  while ($row = mysql_fetch_row($query)) {
    $title_id[$i]['id'] = $row[0];
    $title_id[$i]['name'] = $row[1];
    $i++;
  }
  return $title_id;
}

function getCountInfo($title_id) {
  $table = "";
  $table .= "<table border='1'>";
  $table .= "<tr><th>ID</th>";
  $table .= "<th>Name</th>";
  $table .= "<th>Delay</th>"
          . "<th>Totle</th>"
          . "</tr>";
  foreach ($title_id as $id) {
    $table .= "<tr><td>{$id['id']}</td>";
    $table .= "<td>{$id['name']}</td>";
    $result = mysql_query("select * from "
            . "(select count(*) as delay from article where title_id ={$id['id']} and date<=date(parsed_at) - INTERVAL 1 Day "
            . "and date(parsed_at)>=date(parsed_at) - INTERVAL 2 Day and date(parsed_at)>'2015-11-31') as delay, "
            . "(select count(*) as totle from article where title_id ={$id['id']} and date(parsed_at)>'2015-11-31') as totle ;");
    $row = mysql_fetch_assoc($result);
    $table .= "<td>{$row["delay"]}</td>";
    $table .= "<td>{$row["totle"]}</td></tr>";
  }
  $table .="</table>";
  return $table;
}

function sendEmail($body, $mailTo, $table) {
  $body = $body . $table;
  $mail = new ezcMailComposer();
  $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply');
  foreach ($mailTo as $to) {
    $mail->addTo(new ezcMailAddress($to));
  }
  $mail->subject = "Delay Title Report,";
  $mail->htmlText = $body;
  //$mail->plainText = $body;
  $mail->build();
  $transport = new ezcMailMtaTransport();
  $transport->send($mail);
}
