<?php 

require_once '/usr/local/syndigate/ez_autoload.php';
$model = syndModel::getModelInstance();

// -- init --//
$date = isset($argv[1]) ? trim($argv[1]) : date('Y-m-d');

$titlesArticlesCount   = array();  // count of articles for titles in the database
$clientsTitlesArticles = array();  // array of titles articles count for each client  

$clients = $model->getActiveClients(); // get just active clients 

$c = new Criteria();
$c->add(TitlePeer::IS_ACTIVE , 1);
$titles = TitlePeer::doSelect($c);


// Calculating titles articles count in the database
foreach ($titles as $title) {
	$titlesArticlesCount[$title->getId()] = getTitleArticlesCount($title->getId(), $date);
}


//parsing files in each client directory 
foreach ($clients as $client) {
	$filesDir = '/syndigate/clients/' . $client->getId() . '/' . str_replace('-', '/', $date) . '/';
	if(is_dir($filesDir)) {
		$files = getDirectoryFilesRecursive($filesDir);
		$clientsTitlesArticles[$client->getId()] = getTitlesArticlesFromFiles($filesDir, $files);;
	}
}


$compare = array();
foreach ($clients as $client) {
	$clientRules = TitlesClientsRulesPeer::getClientTitlesIds($client->getId());
	sort($clientRules);
	foreach ($clientRules as $titleId) {
		$compare[$client->getId()][$titleId]['db_count'] = (int)$titlesArticlesCount[$titleId];
		$compare[$client->getId()][$titleId]['in_files'] = (int)$clientsTitlesArticles[$client->getId()][$titleId];
	}
	
}

echo getArrayHtmlstring($compare);

// --- Functions --------------------------------------------------------------------------------------------------//

function getTitleArticlesCount($titleId, $date) {
	$toDate = date('Y-m-d H:i:s', strtotime($date) + 86399);
	$sql    = "SELECT COUNT(*) AS cnt FROM article WHERE title_id = $titleId AND (parsed_at BETWEEN '$date' AND '$toDate')";
	//echo $sql . PHP_EOL;
	$rez    = mysql_query($sql);
	$row    = mysql_fetch_assoc($rez);
	return $row['cnt'];
}


function getTitlesArticlesFromFiles($clientDir, $files) {
	$titles = array();
	$filesArticlesIds = array();
	foreach ($files as $filepath) {
		$file = basename($filepath);
		if( $file != '.' && $file != '..') {
			if(!strpos($file, '_')) {
				$titleId = explode('.', $file);
			} else {
				$titleId = explode('_', $file);
			}
			$titleId = $titleId[0];
			
			$articlesIds = getArticlesIdsFromFile($filepath);
			$titles[$titleId] = !isset($titles[$titleId]) ?  $articlesIds : array_unique(array_merge($titles[$titleId], $articlesIds));
			
		}
	}
	
	$tmp = array();
	foreach ($titles as $titleId => $articlesIds) {
		$tmp[$titleId] = count($articlesIds);
	}
	return $tmp;
}

function getArticlesIdsFromFile($file) {
	$string  = file_get_contents($file);
	$matches = array();
	$ids     = array();
	
	preg_match_all('/<Property FormalName="articleId" Value="[0-9]*" \/>/', $string, $matches);
	foreach ($matches[0] as $match) {
		$id  = explode('Value="', $match);
		$ids[] = (int)$id[1];
	}
	return $ids;
}

function getDirectoryFilesRecursive($dir) {
	$files   = array();
	$subDirs = scandir($dir);
	foreach ($subDirs as $subDir) {
		if($subDir != '.' && $subDir != '..') {
			$innerPath = str_replace('//', '/',  $dir . '/' .$subDir);
			if(is_file($innerPath)) {
				$files[] = $innerPath;
			} else {
				$files = array_merge($files, getDirectoryFilesRecursive($innerPath));
			}
		}
	}
	return $files;
}

function getArrayHtmlstring($compare) {
	
	$string = "";
	foreach ($compare as $clientId => $titles) {
		$client = ClientPeer::retrieveByPK($clientId);
		
		$string .= "<table border=1>\r\n";		
		$string .= "\t<tr>\r\n \t\t<td>Client id : $clientId ( {$client->getGenerateType()} )</td>\r\n\t</tr>\r\n";
		
		$string .= "\t<tr>\r\n";
		$string .= "\t\t<td>Title ID</td>\r\n";
		$string .= "\t\t<td>Generated In DB</td>\r\n";
		$string .= "\t\t<td>Found in FTP files</td>\r\n";
		$string .= "\t<tr>\r\n";
		
		foreach ($titles as $titleId => $report) {
			
			$color = 'Black';
			if($report['db_count'] != $report['in_files']) {
				$color = 'red';
			}
			
			$string .= "\t<tr>\r\n";
			$string .= "\t\t<td><font color=\"$color\">$titleId</font></td>\r\n";
			$string .= "\t\t<td>{$report['db_count']}</td>\r\n";
			$string .= "\t\t<td>{$report['in_files']}</td>\r\n";
			$string .= "\t<tr>\r\n";
		}
		$string .= "</table>\r\n <br />\r\n <br />\r\n <br />\r\n";
	}
	
	return $string;
}