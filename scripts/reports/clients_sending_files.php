<?php 
define('SF_ENVIRONMENT', 'dev');
define('SF_DEBUG',       true);

require_once '/usr/local/syndigate/ez_autoload.php';
$albawabaEmails = array('eyad@syndigate.info','majdi@syndigate.info');

$titles         = array(); 
$titlesObjs     = TitlePeer::doSelect(new Criteria());
foreach ($titlesObjs as $obj) {
	$titles[$obj->getId()] = $obj;
}

$date = date('Y-m-d', strtotime("-1 day"));
if(isset($argv[1]) && !empty($argv[1])) {
	$date = $argv[1];
}


$model   = syndModel::getModelInstance();
$clients = $model->getPushClients();

foreach ($clients as &$client) {
	
	$c = new Criteria();
	$c->add(ClientConnectionPeer::CLIENT_ID, $client->getId());
	$connections = ClientConnectionPeer::doselect($c);
	if($connections && !empty($connections)) {
		foreach ($connections as &$connection) {
			$client->connections[] = $connection;
			
			$c = new Criteria();
			$c->add(ClientConnectionFilePeer::CLIENT_ID, $client->getId());
			$c->add(ClientConnectionFilePeer::CLIENT_CONNECTION_ID, $connection->getId());
			$c->add(ClientConnectionFilePeer::IS_SENT, 1);
			$c->add(ClientConnectionFilePeer::SENT_TIME, strtotime($date), Criteria::GREATER_EQUAL);
			$c->addAnd(ClientConnectionFilePeer::SENT_TIME, strtotime($date) + 86400, Criteria::LESS_THAN);

			$connection->sent = ClientConnectionFilePeer::doSelect($c);
		}
	}
}


foreach ($clients as $client) {
	
	$emailsTo = $albawabaEmails;
	$contacts = $client->getContacts();
	
	foreach ($contacts as $contact) {
		if($contact->getType() == 'technical') {
			$emailsTo[] = $contact->getEmail();
		}
	}
	
	$email = $client->GetClientNAme() . PHP_EOL;
	
	foreach ($client->connections as $conn) {
		$email .= "FTP  :" . $conn->getUrl() . PHP_EOL;
		$email .= "User :" . $conn->getUsername() . PHP_EOL;
		$email .= "Files :" . PHP_EOL;
		
		foreach ($conn->sent as $sent) {
			$email .=  $sent->getTitleId() .  " ) " . $titles[$sent->getTitleId()]->getName() . ' ' . $sent->getLocalFile(). PHP_EOL;			
						
		}
		$email .= PHP_EOL . PHP_EOL . PHP_EOL;
	}

	
	sendEmail($email, $date, $emailsTo);
	
}




// --- Functions -------------------------------------------------------------------------//
function sendEmail($email, $reportDate = '', $mailTo = array())
{
	if(!is_array($mailTo)) {
		$mailTo = array($mailTo);
	}

	$mail = new ezcMailComposer();
	$mail->from = new ezcMailAddress( 'report@syndigate.info', 'No reply ' );
	
	foreach($mailTo as $to) {
	 	$mail->addTo( new ezcMailAddress($to) );
	 }

	$mail->subject   = "Syndigate report - client sent files";
	$mail->plainText = "Hello, below the files sent to your FTP account From Syndigate.info on $reportDate \n \n $email" ;
	$mail->build();
	
	$transport = new ezcMailMtaTransport();
	$transport->send( $mail );

	echo "sending mail to "; print_r($mailTo); 
}

