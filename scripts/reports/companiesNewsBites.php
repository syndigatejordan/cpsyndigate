<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
$mailTo = array('H.Karmani@oabinvest.com', 'mark@syndigate.info', 'eyad@syndigate.info', 'khaled@syndigate.info', 'majdi@syndigate.info', 'safaa@syndigate.info');
//$mailTo = array('H.Karmani@oabinvest.com');
//$mailTo = array('eyad@syndigate.info');
//-------------------------------------- Main--------------------------------------------------

$titles_ids = array(2338, 2375, 2376, 2377);
$titles = array(
    '2338' => 'NewsBites Finance - China (Daily)',
    '2375' => 'NewsBites Finance - Stock Reports (Daily)',
    '2376' => 'NewsBites Finance - Research Reports (Daily)',
    '2377' => 'NewsBites Finance - Research Reports (Quarterly)');
$companies = array(
    'SABIC', 'INDUSTRIES QATAR', 'SAFCO', 'BANK MUSCAT',
    'OREEDO Oman', 'Oman Cement', 'BANK DHOFAR', 'National Bank of Oman',
    'JAZEERA AIRWAYS', 'AIR ARABIA');
$day = 1;
$date = date('Y-m-d H:i:s', strtotime("-$day day 00:00:00"));
$msg = "Dear Mr. Hettish Karmani,<br />
<p>Please find below details of the daily stock and research reports for each of the companies of interest to you, with links to the reports in PDF.</p>
<p> Once a week you will also receive the more in-depth weekly reports for each of the companies.</p>
<p>Your free trial will run until 15th February. However, if you have any questions, please don’t hesitate to contact Mark Gatty Saunt, Director of Content Sales & Licensing at SyndiGate via mark@syndigate.info</p>";

$footerMsg = "<p>Thank you very much,</p>
<p>The SyndiGate team.</p>";

$i = 0;
$data = array();
foreach ($titles_ids as $id) {
  foreach ($companies as $companie) {
    $comp = strtolower($companie);
    $query = mysql_query("select a.id,title_id,headline,img_name,date "
            . "from article a right join image i on a.id=i.article_id "
            . "where title_id=$id and parsed_at>'$date' "
            . "and headline like '%$comp%' and img_name !=''");

    while ($row = mysql_fetch_row($query)) {
      $data[$i]['id'] = $row[0];
      $data[$i]['title'] = $titles[$row[1]];
      $data[$i]['headline'] = $row[2];
      $data[$i]['link'] = $row[3];
      $data[$i]['date'] = $row[4];
      $data[$i]['companie'] = $companie;
      $i++;
    }
  }
}
$table = getCountInfo($data);
$body = $msg . $footerMsg . $table;
sendEmail($body, $mailTo);

function getCountInfo($data) {
  $table = "";
  $table .= "<table border='1'>";
  $table .= "<tr><th>Article ID</th>";
  $table .= "<th>Publisher</th>";
  $table .= "<th>Headline</th>"
          . "<th>PDF Link</th>"
          . "<th>Date</th>"
          . "<th>Company</th>"
          . "</tr>";
  foreach ($data as $field) {
    $table .= "<tr><td>{$field['id']}</td>";
    $table .= "<td>{$field['title']}</td>";
    $table .= "<td>{$field['headline']}</td>";
    $table .= "<td><a href='{$field['link']}'>pdf</a></td>";
    $table .= "<td>{$field['date']}</td>";
    $table .= "<td>{$field['companie']}</td></tr>";
  }
  $table .="</table>";
  return $table;
}

function sendEmail($body, $mailTo) {
  foreach ($mailTo as $to) {
    $mail = new ezcMailComposer();
    $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply');
    $mail->addTo(new ezcMailAddress($to));
    $mail->subject = "NewsBites Finance Trial";
    $mail->htmlText = $body;
    $mail->build();
    $transport = new ezcMailMtaTransport();
    $transport->send($mail);
    echo "Sent  $to\n\n";
    unset($mail);
  }
}
