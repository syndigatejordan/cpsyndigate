<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once dirname(__FILE__) . '/../../ez_autoload.php';
require_once '/usr/local/syndigate/scripts/propel_include_config.php';
require_once '/usr/local/syndigate/conf/pheanstalk.conf.php';

$mailTo = array('eyad@syndigate.info , khaled@syndigate.info');
//$mailTo = array('eyad@syndigate.info');

$pheanstalk = new Pheanstalk(PHEANSTALK_HOST . ':' . PHEANSTALK_PORT );
 $job=$pheanstalk->statsTube('generate_driver');
 $stats= get_object_vars($job);
 $urgent=$stats["current-jobs-urgent"];
 $ready=$stats["current-jobs-ready"];

if ($urgent > 300 || $ready > 300) {
  sendEmail($urgent, $mailTo);
}

// --- Functions -------------------------------------------------------------------------//
function sendEmail($Count, $mailTo = array()) {
  if (!is_array($mailTo)) {
    $mailTo = array($mailTo);
  }

  $mail = new ezcMailComposer();
  $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply ');

  foreach ($mailTo as $to) {
    $mail->addTo(new ezcMailAddress($to));
  }

  $mail->subject = "Syndigate Generate Driver [URGENT]";
  $body = "Hello, \n \nThere is $Count jobs suspended now, please check it ASAP.\n \n Thank you. ";
  $mail->plainText = $body;
  $mail->build();

  $transport = new ezcMailMtaTransport();
  $transport->send($mail);

  echo "sending mail to ";
  print_r($mailTo);
}
