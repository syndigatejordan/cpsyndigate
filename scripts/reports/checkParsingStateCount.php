<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
$mailTo = array('eyad@syndigate.info , khaled@syndigate.info');
//$mailTo = array('eyad@syndigate.info');
$ParsingStatePeert = "select count(*) from parsing_state  where completed=0";

$result_done = mysql_query($ParsingStatePeert);
if (mysql_num_rows($result_done) == 0) {
  $Count = (int) 0;
} else {
  $Count = (int) mysql_result(mysql_query($ParsingStatePeert), 0);
}
if ($Count > 50) {
  sendEmail($Count, $mailTo);
}

// --- Functions -------------------------------------------------------------------------//
function sendEmail($Count, $mailTo = array()) {
  if (!is_array($mailTo)) {
    $mailTo = array($mailTo);
  }

  $mail = new ezcMailComposer();
  $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply ');

  foreach ($mailTo as $to) {
    $mail->addTo(new ezcMailAddress($to));
  }

  $mail->subject = "Syndigate Parsing State [URGENT]";
  $body = "Hello, \n \nThere is more than $Count parsing state suspended now, please check it ASAP.\n \n Thank you. ";
  $mail->plainText = $body;
  $mail->build();

  $transport = new ezcMailMtaTransport();
  $transport->send($mail);

  echo "sending mail to ";
  print_r($mailTo);
}
