<?php

define('ENTITIED_TMP_DIR', '/usr/local/syndigate/tmp/entitied_xmls/');
require_once '/usr/local/syndigate/ez_autoload.php';
$albawabaEmails = array('eyad@syndigate.info', 'majdi@syndigate.info');
//$albawabaEmails = array('eyad@syndigate.info');

$datefile = date('Y/m/d/', strtotime("-1 day"));
$allFiles = array_diff(scandir(ENTITIED_TMP_DIR . $datefile), array('..', '.'));
$files = "";
foreach ($allFiles as $file) {
  $files.=$file . ' <br> ';
}
$date = date('Y-m-d', strtotime("-1 day"));
sendEmail($files, $date, $albawabaEmails);

// --- Functions -------------------------------------------------------------------------//
function sendEmail($email, $reportDate = '', $mailTo = array()) {
  $body = "Hello, below the <strong>ENTITIES XMLS FILES</strong> sent to client FTP account From Syndigate.info on $reportDate <br><br> $email";
  $body = wordwrap($body, 75, "\r\n");
  $mail = new ezcMailComposer();
  $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply');
  foreach ($mailTo as $to) {
    $mail->addTo(new ezcMailAddress($to));
  }
  $mail->subject = "Syndigate report - ENTITIES XMLS files sent to client on $reportDate";
  $mail->htmlText = $body;
  $mail->build();
  $transport = new ezcMailMtaTransport();
  $transport->send($mail);

  echo "sending mail to ";
  print_r($mailTo);
}
