<?php

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
$mailTo = array('mark@syndigate.info', 'eyad@syndigate.info', 'khaled@syndigate.info', 'majdi@syndigate.info', 'safaa@syndigate.info', 'Bana.Hajjara@cab.jo', 'doua@syndigate.info');
//$mailTo = array('eyad@syndigate.info');
//-------------------------------------- Main--------------------------------------------------

$titles_ids = array(2375, 2376);
$titles = array(
    '2375' => 'NewsBites Finance - Stock Reports (Daily)',
    '2376' => 'NewsBites Finance - Research Reports (Daily)');
$countries = array(
    'US',
    'UK',
    'Egypt',
    'Jordan',
    'Bahrain',
    'Kuwait',
    'UAE',
    'Oman',
    'Saudi',
    'Qatar',
    'GCC',
    'total');
$day = 1;
$date = date('Y-m-d H:i:s', strtotime("-$day day 00:00:00"));
$msg = "Dear Miss. Bana Hajjara,<br />
<p>Please find below details of the daily stock and research reports for each of the companies of interest to you, with links to the reports in PDF.</p>
<p>Your free trial will run until 10th June.</p>
<p>However, if you have any questions, please don’t hesitate to contact Mark Gatty Saunt, Director of Content Sales & Licensing at Syndi Gate via mark@syndigate.info</p>";

$footerMsg = "<p>Thank you very much,</p>
<p>The Syndi Gate team.</p>";

$i = 0;
$data = array();
foreach ($titles_ids as $id) {
  foreach ($countries as $country) {
    $coun = strtolower($country);
    $query = mysql_query("select a.id,title_id,headline,img_name,date "
            . "from article a right join image i on a.id=i.article_id "
            . "where title_id=$id and parsed_at>'$date' "
            . "and img_name like '%$coun%'");

    while ($row = mysql_fetch_row($query)) {
      $data[$i]['id'] = $row[0];
      $data[$i]['title'] = $titles[$row[1]];
      $data[$i]['headline'] = $row[2];
      $data[$i]['link'] = $row[3];
      $data[$i]['date'] = $row[4];
      $data[$i]['country'] = $country;
      $i++;
    }
  }
}
$table = getCountInfo($data);
$body = $msg . $footerMsg . $table;
sendEmail($body, $mailTo);

function getCountInfo($data) {
  $table = "";
  $table .= "<table border='1'>";
  $table .= "<tr><th>Article ID</th>";
  $table .= "<th>Publisher</th>";
  $table .= "<th>Headline</th>"
          . "<th>PDF Link</th>"
          . "<th>Date</th>"
          . "<th>Country</th>"
          . "</tr>";
  foreach ($data as $field) {
    $table .= "<tr><td>{$field['id']}</td>";
    $table .= "<td>{$field['title']}</td>";
    $table .= "<td>{$field['headline']}</td>";
    $table .= "<td><a href='{$field['link']}'>pdf</a></td>";
    $table .= "<td>{$field['date']}</td>";
    $table .= "<td>{$field['country']}</td></tr>";
  }
  $table .="</table>";
  return $table;
}

function sendEmail($body, $mailTo) {
  foreach ($mailTo as $to) {
    $mail = new ezcMailComposer();
    $mail->from = new ezcMailAddress('report@syndigate.info', 'No reply');
    $mail->addTo(new ezcMailAddress($to));
    $mail->subject = "NewsBites Finance Trial";
    $mail->htmlText = $body;
    $mail->build();
    $transport = new ezcMailMtaTransport();
    $transport->send($mail);
    echo "Sent  $to\n\n";
    unset($mail);
  }
}
