<?php

/**
 * Subclass for representing a row from the 'report_parse' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ReportParse extends BaseReportParse
{
	public function getLogsLink() {
		return link_to('Logs', 'logParse?report_id=' . $this->getId() );
	}
}
