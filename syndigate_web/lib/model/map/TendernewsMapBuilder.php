<?php



class TendernewsMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.TendernewsMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('tendernews');
		$tMap->setPhpName('Tendernews');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('IPTC_ID', 'IptcId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('ARTICLE_ORIGINAL_DATA_ID', 'ArticleOriginalDataId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('LANGUAGE_ID', 'LanguageId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('HEADLINE', 'Headline', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('SUMMARY', 'Summary', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('BODY', 'Body', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('AUTHOR', 'Author', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('DATE', 'Date', 'int', CreoleTypes::DATE, false, null);

		$tMap->addColumn('PARSED_AT', 'ParsedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('HAS_TIME', 'HasTime', 'int', CreoleTypes::TINYINT, false, null);

		$tMap->addColumn('IS_CALIAS_CALLED', 'IsCaliasCalled', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 