<?php



class CountryMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.CountryMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('country');
		$tMap->setPhpName('Country');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('ISO', 'Iso', 'string', CreoleTypes::VARCHAR, true, 2);

		$tMap->addColumn('NAME', 'Name', 'string', CreoleTypes::VARCHAR, true, 80);

		$tMap->addColumn('PRINTABLE_NAME', 'PrintableName', 'string', CreoleTypes::VARCHAR, true, 80);

		$tMap->addColumn('ISO3', 'Iso3', 'string', CreoleTypes::VARCHAR, false, 3);

		$tMap->addColumn('NUMCODE', 'Numcode', 'int', CreoleTypes::SMALLINT, false, null);

	} 
} 