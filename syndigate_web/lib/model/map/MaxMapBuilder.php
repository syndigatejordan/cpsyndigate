<?php



class MaxMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.MaxMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('max');
		$tMap->setPhpName('Max');

		$tMap->setUseIdGenerator(true);

		$tMap->addColumn('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('CLIENT_ID', 'ClientId', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('ARTICLE_ID', 'ArticleId', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 