<?php



class ReportSendMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ReportSendMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('report_send');
		$tMap->setPhpName('ReportSend');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('SENDER_STATUS', 'SenderStatus', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('SENDER_MSG', 'SenderMsg', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('START_TIME', 'StartTime', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('END_TIME', 'EndTime', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('FILES_COUNT', 'FilesCount', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 