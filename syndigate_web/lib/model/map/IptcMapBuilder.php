<?php



class IptcMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.IptcMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('iptc');
		$tMap->setPhpName('Iptc');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('IPTC_ENGLISH', 'IptcEnglish', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('IPTC_ARABIC', 'IptcArabic', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('IPTC_FRENCH', 'IptcFrench', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('PARENT_ID', 'ParentId', 'int', CreoleTypes::INTEGER, false, null);

	} 
} 