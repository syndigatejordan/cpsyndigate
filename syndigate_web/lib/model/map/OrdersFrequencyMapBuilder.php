<?php



class OrdersFrequencyMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.OrdersFrequencyMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('orders_frequency');
		$tMap->setPhpName('OrdersFrequency');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('O_ID', 'OId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, 'title', 'ID', true, null);

		$tMap->addColumn('FREQUENCY', 'Frequency', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('START', 'Start', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('END', 'End', 'int', CreoleTypes::TIMESTAMP, false, null);

	} 
} 