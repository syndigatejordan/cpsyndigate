<?php



class LogParseMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.LogParseMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('log_parse');
		$tMap->setPhpName('LogParse');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'string', CreoleTypes::BIGINT, true, null);

		$tMap->addColumn('REPORT_PARSE_ID', 'ReportParseId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CATEGORY', 'Category', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('MESSAGE', 'Message', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('SEVERITY', 'Severity', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('SOURCE', 'Source', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('TIME', 'Time', 'int', CreoleTypes::TIMESTAMP, true, null);

	} 
} 