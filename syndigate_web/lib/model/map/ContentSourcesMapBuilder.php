<?php



class ContentSourcesMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ContentSourcesMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('content_sources');
		$tMap->setPhpName('ContentSources');

		$tMap->setUseIdGenerator(true);

		$tMap->addColumn('COUNTRY', 'Country', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('CITY', 'City', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('TYPE', 'Type', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('TITLE', 'Title', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('URL', 'Url', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('LANGUAGE', 'Language', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('FREQUANCY', 'Frequancy', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('TIMELINESS', 'Timeliness', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('PUBLISHER', 'Publisher', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('COPYRIGHT_NOTICE', 'CopyrightNotice', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('SOURCE_DESCRIPTION', 'SourceDescription', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('TERRITORY', 'Territory', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('MODE', 'Mode', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('PUBLISHER_TYPE', 'PublisherType', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('CHANNELS', 'Channels', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('GENRE', 'Genre', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('BIZ_CONTACT', 'BizContact', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('BIZ_EMAIL', 'BizEmail', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('BIZ_PHONE', 'BizPhone', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('TECH_CONTACT', 'TechContact', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('TECH_EMAIL', 'TechEmail', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('ECH_PHONE', 'EchPhone', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('IS_ORG_FROM_SYNDIGATE', 'IsOrgFromSyndigate', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 