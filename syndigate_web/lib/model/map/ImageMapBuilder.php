<?php



class ImageMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ImageMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('image');
		$tMap->setPhpName('Image');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('ARTICLE_ID', 'ArticleId', 'int', CreoleTypes::INTEGER, 'article', 'ID', true, null);

		$tMap->addColumn('IMG_NAME', 'ImgName', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('IMAGE_CAPTION', 'ImageCaption', 'string', CreoleTypes::LONGVARCHAR, true, null);

		$tMap->addColumn('IS_HEADLINE', 'IsHeadline', 'int', CreoleTypes::TINYINT, true, null);

		$tMap->addColumn('IMAGE_TYPE', 'ImageType', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('MIME_TYPE', 'MimeType', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('ORIGINAL_NAME', 'OriginalName', 'string', CreoleTypes::VARCHAR, true, 1000);

		$tMap->addColumn('IMAGE_ORIGINAL_KEY', 'ImageOriginalKey', 'string', CreoleTypes::VARCHAR, true, 1000);

	} 
} 