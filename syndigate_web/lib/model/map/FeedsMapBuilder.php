<?php



class FeedsMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.FeedsMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('feeds');
		$tMap->setPhpName('Feeds');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('SYNDIGATEID', 'Syndigateid', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('DESTINATION', 'Destination', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('PROCESSTIME', 'Processtime', 'int', CreoleTypes::TIMESTAMP, true, null);

		$tMap->addColumn('SENT', 'Sent', 'string', CreoleTypes::CHAR, false, null);

	} 
} 