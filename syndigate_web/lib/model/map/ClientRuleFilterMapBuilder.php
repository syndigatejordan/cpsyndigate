<?php



class ClientRuleFilterMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ClientRuleFilterMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('client_rule_filter');
		$tMap->setPhpName('ClientRuleFilter');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('FILTER_TYPE', 'FilterType', 'string', CreoleTypes::CHAR, false, null);

		$tMap->addColumn('FILTER_VALUE', 'FilterValue', 'string', CreoleTypes::LONGVARCHAR, true, null);

		$tMap->addForeignKey('CLIENT_RULE_ID', 'ClientRuleId', 'int', CreoleTypes::INTEGER, 'client_rule', 'ID', true, null);

	} 
} 