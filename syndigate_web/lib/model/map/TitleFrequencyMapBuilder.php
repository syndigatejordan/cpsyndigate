<?php



class TitleFrequencyMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.TitleFrequencyMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('title_frequency');
		$tMap->setPhpName('TitleFrequency');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('FREQUENCY_TYPE', 'FrequencyType', 'string', CreoleTypes::VARCHAR, false, 255);

	} 
} 