<?php



class ReportMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ReportMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('report');
		$tMap->setPhpName('Report');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('REVISION_NUM', 'RevisionNum', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('REPORT_GATHER_ID', 'ReportGatherId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('REPORT_PARSE_ID', 'ReportParseId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('REPORT_GENERATE_ID', 'ReportGenerateId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('REPORT_SEND_ID', 'ReportSendId', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 