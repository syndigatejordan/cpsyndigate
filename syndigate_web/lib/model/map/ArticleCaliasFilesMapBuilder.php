<?php



class ArticleCaliasFilesMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ArticleCaliasFilesMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('article_calias_files');
		$tMap->setPhpName('ArticleCaliasFiles');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('ARTICLE_ID', 'ArticleId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('FILE_PATH', 'FilePath', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('PROCESSED', 'Processed', 'int', CreoleTypes::TINYINT, true, null);

	} 
} 