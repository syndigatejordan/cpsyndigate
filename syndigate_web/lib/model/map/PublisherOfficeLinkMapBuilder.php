<?php



class PublisherOfficeLinkMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.PublisherOfficeLinkMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('publisher_office_link');
		$tMap->setPhpName('PublisherOfficeLink');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('OFFICE_ID', 'OfficeId', 'int', CreoleTypes::INTEGER, 'office', 'ID', true, null);

		$tMap->addForeignKey('PUBLISHER_ID', 'PublisherId', 'int', CreoleTypes::INTEGER, 'publisher', 'ID', true, null);

	} 
} 