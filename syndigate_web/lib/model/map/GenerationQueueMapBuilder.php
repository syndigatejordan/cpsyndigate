<?php



class GenerationQueueMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.GenerationQueueMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('generation_queue');
		$tMap->setPhpName('GenerationQueue');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('REPORT_ID', 'ReportId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TIMESTAMP', 'Timestamp', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 