<?php



class ArticleOriginalDataTmpMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ArticleOriginalDataTmpMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('article_original_data_tmp');
		$tMap->setPhpName('ArticleOriginalDataTmp');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('ORIGINAL_ARTICLE_ID', 'OriginalArticleId', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('ORIGINAL_SOURCE', 'OriginalSource', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('ISSUE_NUMBER', 'IssueNumber', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('PAGE_NUMBER', 'PageNumber', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('REFERENCE', 'Reference', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('EXTRAS', 'Extras', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('HIJRI_DATE', 'HijriDate', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('ORIGINAL_CAT_ID', 'OriginalCatId', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('REVISION_NUM', 'RevisionNum', 'int', CreoleTypes::INTEGER, false, null);

	} 
} 