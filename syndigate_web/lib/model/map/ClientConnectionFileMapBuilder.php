<?php



class ClientConnectionFileMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ClientConnectionFileMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('client_connection_file');
		$tMap->setPhpName('ClientConnectionFile');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CLIENT_ID', 'ClientId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CLIENT_CONNECTION_ID', 'ClientConnectionId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('REVISION', 'Revision', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('LOCAL_FILE', 'LocalFile', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('IS_SENT', 'IsSent', 'int', CreoleTypes::TINYINT, true, null);

		$tMap->addColumn('NUM_RETRY', 'NumRetry', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('ERR_MSG', 'ErrMsg', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('REPORT_ID', 'ReportId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CREATION_TIME', 'CreationTime', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('SENT_TIME', 'SentTime', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 