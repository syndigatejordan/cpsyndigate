<?php



class TendersInfoSimilarMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.TendersInfoSimilarMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('tenders_info_similar');
		$tMap->setPhpName('TendersInfoSimilar');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('ARTICLE_ID', 'ArticleId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('ARTICLE_ID_2', 'ArticleId2', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('SIMILAR', 'Similar', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 