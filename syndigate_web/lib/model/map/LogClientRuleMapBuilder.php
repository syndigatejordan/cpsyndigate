<?php



class LogClientRuleMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.LogClientRuleMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('log_client_rule');
		$tMap->setPhpName('LogClientRule');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TIMESTAMP', 'Timestamp', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('ADMIN_ID', 'AdminId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CLIENT_ID', 'ClientId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('PREVIOUS_STATUS', 'PreviousStatus', 'string', CreoleTypes::LONGVARCHAR, true, null);

		$tMap->addColumn('OPERATION', 'Operation', 'string', CreoleTypes::LONGVARCHAR, true, null);

		$tMap->addColumn('CURRENT_STATUS', 'CurrentStatus', 'string', CreoleTypes::LONGVARCHAR, true, null);

	} 
} 