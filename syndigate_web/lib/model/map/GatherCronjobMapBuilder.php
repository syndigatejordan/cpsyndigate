<?php



class GatherCronjobMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.GatherCronjobMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('gather_cronjob');
		$tMap->setPhpName('GatherCronjob');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CRONTAB_ID', 'CrontabId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('START_TIMESTAMP', 'StartTimestamp', 'int', CreoleTypes::TIMESTAMP, true, null);

		$tMap->addColumn('END_TIMESTAMP', 'EndTimestamp', 'int', CreoleTypes::TIMESTAMP, true, null);

		$tMap->addColumn('SCRIPT', 'Script', 'string', CreoleTypes::LONGVARCHAR, true, null);

		$tMap->addColumn('CONCURRENT', 'Concurrent', 'int', CreoleTypes::TINYINT, true, null);

		$tMap->addColumn('IMPLEMENTATION_ID', 'ImplementationId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('RESULTS', 'Results', 'string', CreoleTypes::LONGVARCHAR, true, null);

		$tMap->addColumn('PID', 'Pid', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('IS_EXITED_NORMALLY', 'IsExitedNormally', 'int', CreoleTypes::TINYINT, true, null);

	} 
} 