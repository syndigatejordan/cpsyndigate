<?php



class ClientContactLinkMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ClientContactLinkMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('client_contact_link');
		$tMap->setPhpName('ClientContactLink');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('CONTACT_ID', 'ContactId', 'int', CreoleTypes::INTEGER, 'contact', 'ID', true, null);

		$tMap->addForeignKey('CLIENT_ID', 'ClientId', 'int', CreoleTypes::INTEGER, 'client', 'ID', true, null);

	} 
} 