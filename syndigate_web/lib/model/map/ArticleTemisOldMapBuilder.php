<?php



class ArticleTemisOldMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ArticleTemisOldMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('article_temis_old');
		$tMap->setPhpName('ArticleTemisOld');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('ARTICLE_ID', 'ArticleId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TEMIS_CAT', 'TemisCat', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('TEMIS_CAT1', 'TemisCat1', 'string', CreoleTypes::VARCHAR, true, 255);

	} 
} 