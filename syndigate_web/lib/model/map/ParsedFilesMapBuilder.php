<?php



class ParsedFilesMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ParsedFilesMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('parsed_files');
		$tMap->setPhpName('ParsedFiles');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('GENERATED_DATE', 'GeneratedDate', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('FILE', 'File', 'string', CreoleTypes::VARCHAR, true, 255);

	} 
} 