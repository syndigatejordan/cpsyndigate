<?php



class TrFileMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.TrFileMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('tr_file');
		$tMap->setPhpName('TrFile');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CLIENT_ID', 'ClientId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('LOCAL_FILE', 'LocalFile', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('CREATION_TIME', 'CreationTime', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('ARTICLE_COUNT', 'ArticleCount', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 