<?php



class ClientConnectionMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ClientConnectionMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('client_connection');
		$tMap->setPhpName('ClientConnection');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CLIENT_ID', 'ClientId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CONNECTION_TYPE', 'ConnectionType', 'string', CreoleTypes::CHAR, true, null);

		$tMap->addColumn('URL', 'Url', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('USERNAME', 'Username', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('PASSWORD', 'Password', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('FTP_PORT', 'FtpPort', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('REMOTE_DIR', 'RemoteDir', 'string', CreoleTypes::VARCHAR, false, 255);

	} 
} 