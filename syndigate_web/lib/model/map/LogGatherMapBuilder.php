<?php



class LogGatherMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.LogGatherMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('log_gather');
		$tMap->setPhpName('LogGather');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'string', CreoleTypes::BIGINT, true, null);

		$tMap->addColumn('REPORT_GATHER_ID', 'ReportGatherId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CATEGORY', 'Category', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('MESSAGE', 'Message', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('SEVERITY', 'Severity', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('SOURCE', 'Source', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('TIME', 'Time', 'int', CreoleTypes::TIMESTAMP, true, null);

		$tMap->addColumn('REPORT_ID', 'ReportId', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 