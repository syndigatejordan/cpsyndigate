<?php



class ArticleViewMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ArticleViewMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('article_view');
		$tMap->setPhpName('ArticleView');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('LANGUAGE_ID', 'LanguageId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('HEADLINE', 'Headline', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('DATE', 'Date', 'int', CreoleTypes::DATE, false, null);

		$tMap->addColumn('PARSED_AT', 'ParsedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

	} 
} 