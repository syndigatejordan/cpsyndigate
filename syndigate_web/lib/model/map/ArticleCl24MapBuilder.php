<?php



class ArticleCl24MapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ArticleCl24MapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('article_cl_24');
		$tMap->setPhpName('ArticleCl24');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('HEADLINE', 'Headline', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('SUMMARY', 'Summary', 'string', CreoleTypes::LONGVARCHAR, true, null);

		$tMap->addColumn('DATE', 'Date', 'int', CreoleTypes::DATE, false, null);

		$tMap->addColumn('CATEGORY', 'Category', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('BODY', 'Body', 'string', CreoleTypes::LONGVARCHAR, true, null);

		$tMap->addColumn('REPORT_DATE', 'ReportDate', 'int', CreoleTypes::DATE, false, null);

	} 
} 