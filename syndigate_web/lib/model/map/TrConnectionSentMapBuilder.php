<?php



class TrConnectionSentMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.TrConnectionSentMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('tr_connection_sent');
		$tMap->setPhpName('TrConnectionSent');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CONNECTION_ID', 'ConnectionId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('SENT_FILES', 'SentFiles', 'string', CreoleTypes::VARCHAR, false, null);

	} 
} 