<?php



class ConnectionSentMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ConnectionSentMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('connection_sent');
		$tMap->setPhpName('ConnectionSent');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CONNECTION_ID', 'ConnectionId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('SENT_FILES', 'SentFiles', 'string', CreoleTypes::LONGVARCHAR, false, null);

	} 
} 