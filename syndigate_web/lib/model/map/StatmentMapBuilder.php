<?php



class StatmentMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.StatmentMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('statment');
		$tMap->setPhpName('Statment');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('PATH', 'Path', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('STATUS', 'Status', 'string', CreoleTypes::CHAR, true, null);

		$tMap->addColumn('NOTES', 'Notes', 'string', CreoleTypes::LONGVARCHAR, true, null);

		$tMap->addColumn('UPDATE_AT', 'UpdateAt', 'int', CreoleTypes::TIMESTAMP, true, null);

		$tMap->addColumn('FILE_NAME', 'FileName', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('PUBLISHER_ID', 'PublisherId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CREATED', 'Created', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('ACCOUNTANT_ID', 'AccountantId', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 