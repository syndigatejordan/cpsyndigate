<?php



class ArticleTemisMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ArticleTemisMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('article_temis');
		$tMap->setPhpName('ArticleTemis');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ARTICLE_ID', 'ArticleId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TEMIS_CAT', 'TemisCat', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('TEMIS_CAT1', 'TemisCat1', 'string', CreoleTypes::VARCHAR, true, 255);

	} 
} 