<?php



class TitlesClientsRulesMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.TitlesClientsRulesMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('titles_clients_rules');
		$tMap->setPhpName('TitlesClientsRules');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CLIENT_ID', 'ClientId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('LAST_UPDATED', 'LastUpdated', 'int', CreoleTypes::TIMESTAMP, false, null);

	} 
} 