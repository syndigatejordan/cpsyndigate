<?php



class LanguageMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.LanguageMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('language');
		$tMap->setPhpName('Language');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('NAME', 'Name', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('CODE', 'Code', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('DIR', 'Dir', 'string', CreoleTypes::VARCHAR, false, 3);

	} 
} 