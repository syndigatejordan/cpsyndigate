<?php



class FileResourcesMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.FileResourcesMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('file_resources');
		$tMap->setPhpName('FileResources');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('DATE', 'Date', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('CLIENT_ID', 'ClientId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('FILE_NAME', 'FileName', 'string', CreoleTypes::VARCHAR, true, 500);

		$tMap->addColumn('RESOURCE', 'Resource', 'string', CreoleTypes::VARCHAR, true, 500);

		$tMap->addColumn('IS_SENT', 'IsSent', 'boolean', CreoleTypes::BOOLEAN, true, null);

	} 
} 