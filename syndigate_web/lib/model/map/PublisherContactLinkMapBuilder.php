<?php



class PublisherContactLinkMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.PublisherContactLinkMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('publisher_contact_link');
		$tMap->setPhpName('PublisherContactLink');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('CONTACT_ID', 'ContactId', 'int', CreoleTypes::INTEGER, 'contact', 'ID', true, null);

		$tMap->addForeignKey('PUBLISHER_ID', 'PublisherId', 'int', CreoleTypes::INTEGER, 'publisher', 'ID', true, null);

	} 
} 