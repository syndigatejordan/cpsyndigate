<?php



class ReportGenerateMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ReportGenerateMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('report_generate');
		$tMap->setPhpName('ReportGenerate');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('FILE', 'File', 'string', CreoleTypes::VARCHAR, true, 100);

		$tMap->addColumn('ARTICLES_COUNT', 'ArticlesCount', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('ERROR', 'Error', 'string', CreoleTypes::LONGVARCHAR, true, null);

		$tMap->addColumn('TIME', 'Time', 'int', CreoleTypes::TIMESTAMP, true, null);

		$tMap->addColumn('STATUS', 'Status', 'int', CreoleTypes::TINYINT, true, null);

	} 
} 