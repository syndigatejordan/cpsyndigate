<?php



class ParsingStateBackupMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ParsingStateBackupMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('parsing_state_backup');
		$tMap->setPhpName('ParsingStateBackup');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('REVISION_NUM', 'RevisionNum', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('COMPLETED', 'Completed', 'int', CreoleTypes::TINYINT, true, null);

		$tMap->addColumn('LOCKED', 'Locked', 'int', CreoleTypes::TINYINT, true, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'int', CreoleTypes::TIMESTAMP, true, null);

		$tMap->addColumn('PARSER_STARTED_AT', 'ParserStartedAt', 'int', CreoleTypes::TIMESTAMP, true, null);

		$tMap->addColumn('PARSER_FINISHED_AT', 'ParserFinishedAt', 'int', CreoleTypes::TIMESTAMP, true, null);

		$tMap->addColumn('PID', 'Pid', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 