<?php



class ClientMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ClientMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('client');
		$tMap->setPhpName('Client');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CLIENT_NAME', 'ClientName', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('URL', 'Url', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('CONNECTION_TYPE', 'ConnectionType', 'string', CreoleTypes::CHAR, true, null);

		$tMap->addColumn('HOME_DIR', 'HomeDir', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('USERNAME', 'Username', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('PASSWORD', 'Password', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('FTP_USERNAME', 'FtpUsername', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('FTP_PASSWORD', 'FtpPassword', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('DIR_LAYOUT', 'DirLayout', 'string', CreoleTypes::CHAR, true, null);

		$tMap->addColumn('IS_ACTIVE', 'IsActive', 'int', CreoleTypes::TINYINT, true, null);

		$tMap->addColumn('LOGO_PATH', 'LogoPath', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('FTP_PORT', 'FtpPort', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('REMOTE_DIR', 'RemoteDir', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('CONTRACT', 'Contract', 'string', CreoleTypes::CHAR, true, null);

		$tMap->addColumn('CONTRACT_START_DATE', 'ContractStartDate', 'int', CreoleTypes::DATE, false, null);

		$tMap->addColumn('CONTRACT_END_DATE', 'ContractEndDate', 'int', CreoleTypes::DATE, false, null);

		$tMap->addColumn('RENEWAL_PERIOD', 'RenewalPeriod', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('ROYALTY_RATE', 'RoyaltyRate', 'double', CreoleTypes::FLOAT, true, null);

		$tMap->addColumn('NAGIOS_FILE_PATH', 'NagiosFilePath', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('ZIPPED', 'Zipped', 'int', CreoleTypes::TINYINT, true, null);

		$tMap->addColumn('GENERATE_TYPE', 'GenerateType', 'string', CreoleTypes::CHAR, false, null);

		$tMap->addColumn('NOTES', 'Notes', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('XML_TYPE', 'XmlType', 'string', CreoleTypes::CHAR, true, null);

	} 
} 