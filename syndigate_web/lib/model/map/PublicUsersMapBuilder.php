<?php



class PublicUsersMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.PublicUsersMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('public_users');
		$tMap->setPhpName('PublicUsers');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('USER_ID', 'UserId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('USER_NAME', 'UserName', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('PASSWORD', 'Password', 'string', CreoleTypes::VARCHAR, true, 16);

		$tMap->addColumn('CREATION_TIME_STAMP', 'CreationTimeStamp', 'int', CreoleTypes::TIMESTAMP, true, null);

		$tMap->addColumn('PERIOD', 'Period', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('COMPANY_NAME', 'CompanyName', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('EMAIL', 'Email', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('USER_STATUS', 'UserStatus', 'string', CreoleTypes::CHAR, false, null);

		$tMap->addColumn('FIRST_LOGIN', 'FirstLogin', 'int', CreoleTypes::TIMESTAMP, true, null);

		$tMap->addColumn('EXPIRY_DATE', 'ExpiryDate', 'int', CreoleTypes::TIMESTAMP, true, null);

	} 
} 