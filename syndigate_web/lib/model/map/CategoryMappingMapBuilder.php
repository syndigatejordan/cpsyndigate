<?php



class CategoryMappingMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.CategoryMappingMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('category_mapping');
		$tMap->setPhpName('CategoryMapping');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('IPTC_ID', 'IptcId', 'int', CreoleTypes::INTEGER, 'iptc', 'ID', true, null);

		$tMap->addForeignKey('ORIGINAL_ARTICLE_CATEGORY_ID', 'OriginalArticleCategoryId', 'int', CreoleTypes::INTEGER, 'original_article_category', 'ID', false, null);

	} 
} 