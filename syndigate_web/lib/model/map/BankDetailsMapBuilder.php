<?php



class BankDetailsMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.BankDetailsMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('bank_details');
		$tMap->setPhpName('BankDetails');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('PUBLISHER_ID', 'PublisherId', 'int', CreoleTypes::INTEGER, 'publisher', 'ID', true, null);

		$tMap->addColumn('BRANCH', 'Branch', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addForeignKey('COUNTRY_ID', 'CountryId', 'int', CreoleTypes::INTEGER, 'country', 'ID', true, null);

		$tMap->addColumn('NOTES', 'Notes', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('ACCOUNT_NAME', 'AccountName', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('ACCOUNT_NUMBER', 'AccountNumber', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('SWIFT', 'Swift', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('IBAN', 'Iban', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('BANK_NAME', 'BankName', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('BANK_CITY', 'BankCity', 'string', CreoleTypes::VARCHAR, false, 255);

	} 
} 