<?php



class GatherCrontabMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.GatherCrontabMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('gather_crontab');
		$tMap->setPhpName('GatherCrontab');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TITLE', 'Title', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('DESCRIPTION', 'Description', 'string', CreoleTypes::LONGVARCHAR, true, null);

		$tMap->addColumn('SCRIPT', 'Script', 'string', CreoleTypes::LONGVARCHAR, true, null);

		$tMap->addColumn('CONCURRENT', 'Concurrent', 'int', CreoleTypes::TINYINT, true, null);

		$tMap->addColumn('IMPLEMENTATION_ID', 'ImplementationId', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('CRON_DEFINITION', 'CronDefinition', 'string', CreoleTypes::LONGVARCHAR, true, null);

		$tMap->addColumn('LAST_ACTUAL_TIMESTAMP', 'LastActualTimestamp', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('RUN_ONCE', 'RunOnce', 'int', CreoleTypes::TINYINT, true, null);

	} 
} 