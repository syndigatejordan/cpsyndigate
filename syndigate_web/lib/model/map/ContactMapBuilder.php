<?php



class ContactMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ContactMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('contact');
		$tMap->setPhpName('Contact');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TYPE', 'Type', 'string', CreoleTypes::CHAR, true, null);

		$tMap->addColumn('FULL_NAME', 'FullName', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('EMAIL', 'Email', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('ADDITIONAL_EMAIL', 'AdditionalEmail', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('TITLE', 'Title', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('DEPARTMENT', 'Department', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('ZIPCODE', 'Zipcode', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('POBOX', 'Pobox', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('STREET_ADDRESS', 'StreetAddress', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('CITY_NAME', 'CityName', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addForeignKey('COUNTRY_ID', 'CountryId', 'int', CreoleTypes::INTEGER, 'country', 'ID', true, null);

		$tMap->addColumn('MOBILE_NUMBER', 'MobileNumber', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('WORK_NUMBER', 'WorkNumber', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('HOME_NUMBER', 'HomeNumber', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('NOTIFICATION_SMS', 'NotificationSms', 'int', CreoleTypes::TINYINT, false, null);

		$tMap->addColumn('NOTIFICATION_EMAIL', 'NotificationEmail', 'int', CreoleTypes::TINYINT, false, null);

	} 
} 