<?php



class TendersInfoDuplicationsMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.TendersInfoDuplicationsMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('tenders_info_duplications');
		$tMap->setPhpName('TendersInfoDuplications');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('FILE_NAME', 'FileName', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('POST_ID', 'PostId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('STATUS', 'Status', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('ORIGINAL_ID', 'OriginalId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('PARSE_DATE', 'ParseDate', 'int', CreoleTypes::DATE, true, null);

		$tMap->addColumn('ARTICLE_ID', 'ArticleId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('ARTICLE_DATE', 'ArticleDate', 'int', CreoleTypes::DATE, true, null);

	} 
} 