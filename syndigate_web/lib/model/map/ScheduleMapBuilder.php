<?php



class ScheduleMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ScheduleMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('schedule');
		$tMap->setPhpName('Schedule');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, 'title', 'ID', true, null);

		$tMap->addColumn('MINUTE', 'Minute', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('HOUR', 'Hour', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('DAY', 'Day', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('MONTH', 'Month', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('WEEKDAY', 'Weekday', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('SCRIPT_PATH', 'ScriptPath', 'string', CreoleTypes::VARCHAR, false, 255);

	} 
} 