<?php



class EntityOccurancesMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.EntityOccurancesMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('entity_occurances');
		$tMap->setPhpName('EntityOccurances');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('ARTICLE_ID', 'ArticleId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('ENTITY_ID', 'EntityId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('RELEVANCE', 'Relevance', 'double', CreoleTypes::FLOAT, true, null);

	} 
} 