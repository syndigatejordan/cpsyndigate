<?php



class TitleOfficeLinkMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.TitleOfficeLinkMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('title_office_link');
		$tMap->setPhpName('TitleOfficeLink');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('OFFICE_ID', 'OfficeId', 'int', CreoleTypes::INTEGER, 'office', 'ID', true, null);

		$tMap->addForeignKey('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, 'title', 'ID', true, null);

	} 
} 