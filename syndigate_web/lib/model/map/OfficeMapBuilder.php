<?php



class OfficeMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.OfficeMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('office');
		$tMap->setPhpName('Office');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('PHONE_1', 'Phone1', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('PHONE_2', 'Phone2', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('FAX', 'Fax', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('POBOX', 'Pobox', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('ZIPCODE', 'Zipcode', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('STREET_ADDRESS', 'StreetAddress', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('CITY_NAME', 'CityName', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addForeignKey('COUNTRY_ID', 'CountryId', 'int', CreoleTypes::INTEGER, 'country', 'ID', false, null);

	} 
} 