<?php



class ReportParseMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ReportParseMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('report_parse');
		$tMap->setPhpName('ReportParse');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('ARTICLES_IDS', 'ArticlesIds', 'string', CreoleTypes::LONGVARCHAR, true, null);

		$tMap->addForeignKey('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, 'title', 'ID', false, null);

		$tMap->addColumn('DATE', 'Date', 'int', CreoleTypes::DATE, false, null);

		$tMap->addColumn('PARSER_STATUS', 'ParserStatus', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('PARSER_ERROR', 'ParserError', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('ARTICLE_NUM', 'ArticleNum', 'int', CreoleTypes::INTEGER, false, null);

	} 
} 