<?php



class PublisherMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.PublisherMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('publisher');
		$tMap->setPhpName('Publisher');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('NAME', 'Name', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('WEBSITE', 'Website', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('NOTES', 'Notes', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('LOGO_PATH', 'LogoPath', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('FTP_USERNAME', 'FtpUsername', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('FTP_PASSWORD', 'FtpPassword', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('UNIX_GID', 'UnixGid', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('UNIX_UID', 'UnixUid', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('NAGIOS_FILE_PATH', 'NagiosFilePath', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addForeignKey('COUNTRY_ID', 'CountryId', 'int', CreoleTypes::INTEGER, 'country', 'ID', false, null);

		$tMap->addColumn('CONTRACT', 'Contract', 'string', CreoleTypes::CHAR, true, null);

		$tMap->addColumn('ROYALTY_RATE', 'RoyaltyRate', 'double', CreoleTypes::FLOAT, true, null);

		$tMap->addColumn('CONTRACT_START_DATE', 'ContractStartDate', 'int', CreoleTypes::DATE, false, null);

		$tMap->addColumn('CONTRACT_END_DATE', 'ContractEndDate', 'int', CreoleTypes::DATE, false, null);

		$tMap->addColumn('RENEWAL_PERIOD', 'RenewalPeriod', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addForeignKey('ADMINS_ID', 'AdminsId', 'int', CreoleTypes::INTEGER, 'admins', 'ID', true, null);

		$tMap->addColumn('ACCOUNTANT_NOTES', 'AccountantNotes', 'string', CreoleTypes::LONGVARCHAR, true, null);

	} 
} 