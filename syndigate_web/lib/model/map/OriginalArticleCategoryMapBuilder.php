<?php



class OriginalArticleCategoryMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.OriginalArticleCategoryMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('original_article_category');
		$tMap->setPhpName('OriginalArticleCategory');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('PARENT_ID', 'ParentId', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('CAT_NAME', 'CatName', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addForeignKey('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, 'title', 'ID', false, null);

		$tMap->addColumn('ADDED_AT', 'AddedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

	} 
} 