<?php



class ArticleCategoryMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ArticleCategoryMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('article_category');
		$tMap->setPhpName('ArticleCategory');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CATEGORY_ID', 'CategoryId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('ARTICLE_ID', 'ArticleId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('SCORE', 'Score', 'double', CreoleTypes::FLOAT, true, null);

	} 
} 