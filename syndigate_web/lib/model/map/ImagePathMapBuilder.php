<?php



class ImagePathMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ImagePathMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('image_path');
		$tMap->setPhpName('ImagePath');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('CONNNECTION_ID', 'ConnnectionId', 'int', CreoleTypes::INTEGER, 'title_connection', 'ID', true, null);

		$tMap->addColumn('IMAGE_PATH', 'ImagePath', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('IMAGE_TYPE', 'ImageType', 'string', CreoleTypes::CHAR, false, null);

		$tMap->addColumn('IMAGE_CONTAINER_TYPE', 'ImageContainerType', 'string', CreoleTypes::CHAR, false, null);

		$tMap->addColumn('IMAGE_CONTAINERS_FILE', 'ImageContainersFile', 'string', CreoleTypes::CHAR, false, null);

		$tMap->addColumn('IMAGE_HEAD', 'ImageHead', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('IMAGE_EXTRA', 'ImageExtra', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('IMAGE_FILE_PATTERNS', 'ImageFilePatterns', 'string', CreoleTypes::VARCHAR, false, 255);

	} 
} 