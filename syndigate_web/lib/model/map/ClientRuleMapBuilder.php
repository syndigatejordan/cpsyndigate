<?php



class ClientRuleMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ClientRuleMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('client_rule');
		$tMap->setPhpName('ClientRule');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('CLIENT_ID', 'ClientId', 'int', CreoleTypes::INTEGER, 'client', 'ID', true, null);

	} 
} 