<?php



class Utf8ErrorsMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.Utf8ErrorsMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('utf8_errors');
		$tMap->setPhpName('Utf8Errors');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('ARTICLE_ID', 'ArticleId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('ERR_DESC', 'ErrDesc', 'string', CreoleTypes::VARCHAR, true, 3);

	} 
} 