<?php



class VideoMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.VideoMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('video');
		$tMap->setPhpName('Video');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('ARTICLE_ID', 'ArticleId', 'int', CreoleTypes::INTEGER, 'article', 'ID', true, null);

		$tMap->addColumn('VIDEO_NAME', 'VideoName', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('VIDEO_CAPTION', 'VideoCaption', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('ORIGINAL_NAME', 'OriginalName', 'string', CreoleTypes::VARCHAR, true, 1000);

		$tMap->addColumn('VIDEO_TYPE', 'VideoType', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('BIT_RATE', 'BitRate', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('ADDED_TIME', 'AddedTime', 'int', CreoleTypes::TIMESTAMP, true, null);

		$tMap->addColumn('MIME_TYPE', 'MimeType', 'string', CreoleTypes::VARCHAR, true, 255);

	} 
} 