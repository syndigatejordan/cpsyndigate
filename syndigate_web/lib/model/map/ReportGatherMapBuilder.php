<?php



class ReportGatherMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ReportGatherMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('report_gather');
		$tMap->setPhpName('ReportGather');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, 'title', 'ID', true, null);

		$tMap->addColumn('LEVEL', 'Level', 'string', CreoleTypes::CHAR, false, null);

		$tMap->addColumn('NUMBER_FILES', 'NumberFiles', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('FILE_SIZE', 'FileSize', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('LATEST_STATUS', 'LatestStatus', 'int', CreoleTypes::TINYINT, false, null);

		$tMap->addColumn('ERROR', 'Error', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('IMAGES_STATUS', 'ImagesStatus', 'int', CreoleTypes::TINYINT, false, null);

		$tMap->addColumn('NUMBER_IMAGES', 'NumberImages', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('TIME', 'Time', 'int', CreoleTypes::TIMESTAMP, true, null);

	} 
} 