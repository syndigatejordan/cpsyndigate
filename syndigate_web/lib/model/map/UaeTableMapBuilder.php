<?php



class UaeTableMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.UaeTableMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('UAE_table');
		$tMap->setPhpName('UaeTable');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('COMPANY_NAME', 'CompanyName', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('LOCATION', 'Location', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('PO_BOX', 'PoBox', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('TELEPHONE', 'Telephone', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('FAX', 'Fax', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('EMAIL', 'Email', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('WEBSITE', 'Website', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ACTIVITIES', 'Activities', 'string', CreoleTypes::VARCHAR, false, 100);

	} 
} 