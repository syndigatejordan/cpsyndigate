<?php



class EmbinCustomMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.EmbinCustomMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('embin_custom');
		$tMap->setPhpName('EmbinCustom');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('ARTICLE_ID', 'ArticleId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('PUBLISHER', 'Publisher', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('DATE', 'Date', 'int', CreoleTypes::DATE, false, null);

	} 
} 