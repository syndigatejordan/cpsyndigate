<?php



class ClientConnectionSentMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ClientConnectionSentMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('client_connection_sent');
		$tMap->setPhpName('ClientConnectionSent');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CONNECTION_ID', 'ConnectionId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('SENT_FILES', 'SentFiles', 'string', CreoleTypes::LONGVARCHAR, false, null);

	} 
} 