<?php



class ClientArticlesMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ClientArticlesMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('client_articles');
		$tMap->setPhpName('ClientArticles');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('BODY', 'Body', 'string', CreoleTypes::LONGVARCHAR, true, null);

		$tMap->addColumn('PROVIDERNAME', 'Providername', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('CATEGORYNAME', 'Categoryname', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('ORIGINALCATEGORY', 'Originalcategory', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('HEADLINEIMAGE', 'Headlineimage', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('HEADLINEIMAGECAPTION', 'Headlineimagecaption', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('ARTICLEDATE', 'Articledate', 'string', CreoleTypes::VARCHAR, true, 10);

		$tMap->addColumn('ARTICLEID', 'Articleid', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('GENERATED_TIME', 'GeneratedTime', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('HEADLINE', 'Headline', 'string', CreoleTypes::VARCHAR, true, 255);

	} 
} 