<?php



class TitleMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.TitleMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('title');
		$tMap->setPhpName('Title');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('TITLE_FREQUENCY_ID', 'TitleFrequencyId', 'int', CreoleTypes::INTEGER, 'title_frequency', 'ID', true, null);

		$tMap->addForeignKey('TITLE_TYPE_ID', 'TitleTypeId', 'int', CreoleTypes::INTEGER, 'title_type', 'ID', true, null);

		$tMap->addForeignKey('PUBLISHER_ID', 'PublisherId', 'int', CreoleTypes::INTEGER, 'publisher', 'ID', true, null);

		$tMap->addColumn('NAME', 'Name', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('WEBSITE', 'Website', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addForeignKey('LANGUAGE_ID', 'LanguageId', 'int', CreoleTypes::INTEGER, 'language', 'ID', false, null);

		$tMap->addColumn('FTP_USERNAME', 'FtpUsername', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('FTP_PASSWORD', 'FtpPassword', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('ARTICLES_THRESHOLD', 'ArticlesThreshold', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('UNIX_UID', 'UnixUid', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('UNIX_GID', 'UnixGid', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('NAGIOS_CONFIG_FILE', 'NagiosConfigFile', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('HOME_DIR', 'HomeDir', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('WORK_DIR', 'WorkDir', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('SVN_REPO', 'SvnRepo', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('GATHER_RETRIES', 'GatherRetries', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('DEFAULT_CHARSET', 'DefaultCharset', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('PARSE_DIR', 'ParseDir', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('NEXT_TRY_TIMER', 'NextTryTimer', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('COPYRIGHT', 'Copyright', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('COPYRIGHT_ARABIC', 'CopyrightArabic', 'string', CreoleTypes::VARCHAR, true, 255);

		$tMap->addColumn('IS_ACTIVE', 'IsActive', 'int', CreoleTypes::TINYINT, true, null);

		$tMap->addColumn('ACTIVATION_DATE', 'ActivationDate', 'int', CreoleTypes::DATE, false, null);

		$tMap->addColumn('LOGO_PATH', 'LogoPath', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('DIRECTORY_LAYOUT', 'DirectoryLayout', 'string', CreoleTypes::CHAR, true, null);

		$tMap->addColumn('IS_NEWSWIRE', 'IsNewswire', 'int', CreoleTypes::TINYINT, true, null);

		$tMap->addColumn('IS_GENERATE', 'IsGenerate', 'int', CreoleTypes::TINYINT, false, null);

		$tMap->addForeignKey('COUNTRY_ID', 'CountryId', 'int', CreoleTypes::INTEGER, 'country', 'ID', false, null);

		$tMap->addColumn('PHOTO_FEED', 'PhotoFeed', 'string', CreoleTypes::CHAR, true, null);

		$tMap->addColumn('CONTRACT', 'Contract', 'string', CreoleTypes::CHAR, true, null);

		$tMap->addColumn('HAS_TECHNICAL_INTRO', 'HasTechnicalIntro', 'int', CreoleTypes::TINYINT, true, null);

		$tMap->addColumn('IS_RECEIVING', 'IsReceiving', 'int', CreoleTypes::TINYINT, true, null);

		$tMap->addColumn('IS_PARSING', 'IsParsing', 'int', CreoleTypes::TINYINT, true, null);

		$tMap->addColumn('TECHNICAL_NOTES', 'TechnicalNotes', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('TIMELINESS', 'Timeliness', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('CONTRACT_START_DATE', 'ContractStartDate', 'int', CreoleTypes::DATE, false, null);

		$tMap->addColumn('CONTRACT_END_DATE', 'ContractEndDate', 'int', CreoleTypes::DATE, false, null);

		$tMap->addColumn('ROYALTY_RATE', 'RoyaltyRate', 'double', CreoleTypes::FLOAT, true, null);

		$tMap->addColumn('SOURCE_DESCRIPTION', 'SourceDescription', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('RENEWAL_PERIOD', 'RenewalPeriod', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('CITY', 'City', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('STORY_COUNT', 'StoryCount', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('RECEIVING_STATUS', 'ReceivingStatus', 'int', CreoleTypes::TINYINT, true, null);

		$tMap->addColumn('RECEIVING_STATUS_COMMENT', 'ReceivingStatusComment', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('RECEIVING_STATUS_TIMESTAMP', 'ReceivingStatusTimestamp', 'int', CreoleTypes::TIMESTAMP, true, null);

		$tMap->addColumn('NOTES', 'Notes', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('HAS_NEW_PARSED_CONTENT', 'HasNewParsedContent', 'int', CreoleTypes::TINYINT, false, null);

		$tMap->addColumn('LAST_CONTENT_TIMESTAMP', 'LastContentTimestamp', 'int', CreoleTypes::TIMESTAMP, true, null);

		$tMap->addColumn('CACHED_ARTICLES_TODAY', 'CachedArticlesToday', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CACHED_CURRENT_SPAN', 'CachedCurrentSpan', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CACHED_PREV_SPAN', 'CachedPrevSpan', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CACHED_ARTICLES_COUNT', 'CachedArticlesCount', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('ADMINS_ID', 'AdminsId', 'int', CreoleTypes::INTEGER, 'admins', 'ID', true, null);

		$tMap->addColumn('RANK', 'Rank', 'int', CreoleTypes::TINYINT, true, null);

	} 
} 