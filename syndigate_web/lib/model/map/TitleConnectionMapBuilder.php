<?php



class TitleConnectionMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.TitleConnectionMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('title_connection');
		$tMap->setPhpName('TitleConnection');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('TITLE_ID', 'TitleId', 'int', CreoleTypes::INTEGER, 'title', 'ID', true, null);

		$tMap->addColumn('CONNECTION_TYPE', 'ConnectionType', 'string', CreoleTypes::CHAR, false, null);

		$tMap->addColumn('URL', 'Url', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('FILE_PATTERN', 'FilePattern', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('AUTHINTICATION', 'Authintication', 'int', CreoleTypes::TINYINT, false, null);

		$tMap->addColumn('USERNAME', 'Username', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('PASSWORD', 'Password', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('EXTRAS', 'Extras', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('HAS_IMAGES', 'HasImages', 'int', CreoleTypes::TINYINT, false, null);

		$tMap->addColumn('FTP_PORT', 'FtpPort', 'int', CreoleTypes::INTEGER, false, null);

	} 
} 