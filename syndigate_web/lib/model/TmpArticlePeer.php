<?php

/**
 * Subclass for performing query and update operations on the 'tmp_article' table.
 *
 * 
 *
 * @package lib.model
 */ 
class TmpArticlePeer extends BaseTmpArticlePeer
{
}
