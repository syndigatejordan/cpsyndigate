<?php

/**
 * Subclass for representing a row from the 'client_connection' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ClientConnection extends BaseClientConnection
{
	public function validateMe()
	{
		require_once LINUX_SCRIPTS_PATH . 'standard.php';
		$standard = new Standard();
		
		if(!$this->getClientId()) {
			$errMsg = "Client Id not set";
		} else {
			if(!in_array($this->getConnectionType(), array('ftp_pull','ftp_push') )) {
				$errMsg = 'connection type must be : ftp_pull or ftp_push';
			} else {

				if($this->getConnectionType()=='ftp_push') {
					
					$errMsg = $standard->getUrlErrMsg($this->getUrl());
    				if(!$errMsg) {
    					$errMsg = $standard->getFtpErrMsg(strtolower($this->getUsername()), 2, 200, 'Username', true);
    				}
    				if(!$errMsg) {
    					$errMsg = $standard->getPasswordErrorV1($this->getPassword(), 6);
    				}
    				
				} else {
    				if(!$errMsg) {
    					$errMsg = !$this->getUrl() ? '' : 'Connection type is ftp_pull, you do not need URL.';
    				}
    				if(!$errMsg) {
    					$errMsg = !$this->getUsername() ? '' : 'Connection type is ftp_pull, you do not need username or password.';
    				}
    				if(!$errMsg) {
    					$errMsg = !$this->getPassword() ? '' : 'Connection type is ftp_pull, you do not need username or password.';
    				}
				}		
			}
		}
		
		return $errMsg;
	}
	

	
	public function delClientConnectionSent() {
		
		$c = new Criteria();
		$c->add(ClientConnectionSentPeer::CONNECTION_ID, $this->getId());
		return ClientConnectionSentPeer::doDelete($c);
	}
}
	

