<?php

/**
 * Subclass for representing a row from the 'client_connection_sent' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ClientConnectionSent extends BaseClientConnectionSent
{
	private $files = array();
		
	public function __destruct(){}
	
	public function addToSent($file)
	{
		if(!is_array($this->files)) {
			$this->files = array();
		}
		if(!in_array($file, $this->files)) {
			$this->files[] = $file;
		}
		
	}
	
	public function getIsSentBefore($file)
	{
		if(!is_array($this->files)) {
			return false;
		} else {
			if(in_array($file, $this->files)) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	public function saveSettings()
	{
		if(!is_array($this->files)) {
			$this->files = array();
		}
		$this->setSentFiles(serialize($this->files));
		$this->save();
		$this->__destruct();
	}
	
	/**
	 * Remove all sent files from the array which is older than X days
	 *
	 */
	public function init()
	{	
		$maxDaysToKeep = 15; 
						
		$files  	= unserialize($this->getSentFiles());
		$this->files = array();
		$daysToKeep  = array();
			
		if(!is_array($files)) {
			$this->setSentFiles(serialize($this->files));
			$this->save();
			return;
		}
				
		for ($count=0; $count<$maxDaysToKeep; $count++) {
			$daysToKeep[] = (int)date('d', strtotime("-$count days"));
		}
				
		
		foreach ($files as $file) {
			$tmp = $file;
						
			$file  = explode("_", $file);
			$title = $file[0];
			$file  = $file[1];
			$file  = explode(".xml", $file);
			$file  = (int) substr($file[0], 0, 2); // this is the day where the file was sent date('d')
					
			if(in_array($file, $daysToKeep))  { 
				$this->files[] = $tmp;
			}
		}//End foreach
		
		
		$this->setSentFiles(serialize($this->files));
		$this->save();
		return;
	}	
}
