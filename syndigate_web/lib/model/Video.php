<?php

/**
 * Subclass for representing a row from the 'video' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Video extends BaseVideo
{
	public static function getVideoByOriginalName($articleId, $originalName)
	{
		$c = new Criteria();
		$c->add(VideoPeer::ARTICLE_ID, $articleId);
		$c->add(VideoPeer::ORIGINAL_NAME, $originalName, Criteria::EQUAL);
		return VideoPeer::doselectOne($c);
	}
	
	/**
	 * Delete the Video record and the physical File on hardDisk
	 *
	 * @param Integer $videoId
	 * @return True if Success false otherwise 
	 */
	public static function deleteCascade($videoId) 
	{
		$video 		= VideoPeer::retrieveByPK($videoId);		
				
		if(file_exists($video->getVideoPath())) {			
			unlink($video->getVideoPath());
		}
		$video->delete();
		return true;
	}
	
	
	/**
	 * Return the full path of the video [with video name]
	 *
	 * @return String
	*/	
	public function getVideoPath()
	{
		return str_replace(VIDEOS_HOST, VIDEOS_PATH, $this->getVideoName());
	}
	
	
	/*
	public function getImageDisplayName()
	{	
		$array = explode('/', $this->getImgName());
		return $array[3];
	}
	*/
	
	/*
	public function getImageInternalFolder()
	{
		$array = explode('/', $this->getImagePath());
		return $array[2];
	}
	*/
	
	
}
