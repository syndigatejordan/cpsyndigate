<?php

/**
 * Subclass for performing query and update operations on the 'client_sent' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ClientSentPeer extends BaseClientSentPeer
{
}
