<?php

/**
 * Subclass for representing a row from the 'original_article_category' table.
 *
 * 
 *
 * @package lib.model
 */ 
class OriginalArticleCategory extends BaseOriginalArticleCategory
{
	public function getMappedCategoryObj()
	{
		$c = new Criteria();
		$c->add(CategoryMappingPeer::ORIGINAL_ARTICLE_CATEGORY_ID , $this->getId());
		$result = CategoryMappingPeer::doSelectOne($c);
		
		$iptc;
		if(is_object($result)) {
			 $iptc = $result->getIptc();
		} else {
			$iptc = IptcPeer::retrieveByPK(0);
		}
		
		return $iptc;
	}
	
	
	public function getMappedCategory()
	{
		return $this->getMappedCategoryObj()->getIptcEnglish();
	}
	
}
