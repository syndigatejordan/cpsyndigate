<?php

/**
 * Subclass for representing a row from the 'contact' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Contact extends BaseContact
{
	/**
	 * Defining Contact Type
	 *
	 * @var Contact types
	 */
	static $typeBusiness  	= 1;
	static $typeTechnical 	= 2;
	static $typeAny			= 3;
	static $typeBoth		= 4;
	
	
	public function __toString()
	{
		return $this->getFullName();
	}
	
	
	public function getCountry()
	{
		$countryId = $this->getCountryId();
		if(!is_numeric($countryId)) {
			$countryId =0;
		}
		return CountryPeer::retrieveByPK($countryId);
	}
	
	
	public function setCountryId($countryId)
	{
		if(!is_numeric($countryId)){
			$countryId=0;
		}
		parent::setCountryId($countryId);
		
	}
	
	
	
	public function getRelatedPublishers()
	{
		$c = new Criteria();
		$c->add(PublisherContactLinkPeer::CONTACT_ID, $this->getId());
		$contactLinks = PublisherContactLinkPeer::doSelect($c);
		
		$publishers = array();
		foreach ($contactLinks as $cl) {
			$publishers[]= $cl->getPublisher();
		}
		
		return $publishers;
	}
	
	
	
	public function getRelatedClients()
	{
		$c = new Criteria();
		$c->add(ClientContactLinkPeer::CONTACT_ID, $this->getId());
		$contactLinks = ClientContactLinkPeer::doSelect($c);
		
		$clients = array();
		foreach ($contactLinks as $cl) {
			$clients[]= $cl->getClient();
		}
		
		return $clients;
	}
	
	
	public function saveDataAndNagios($relatedType = 'publisher')
	{
		$this->save();
				
		/*
		-- Here the nagios will be generated for each related 
		-- Titles has no contact yet, as soon they have apply here 
		*/
		 
		$relateds;
		
		if($relatedType == 'publisher') {
			$relateds = $this->getRelatedPublishers();
		} elseif ($relatedType == 'client') {
			$relateds = $this->getRelatedClients();
		}
		
		
		foreach ($relateds as $r) {
			
			$nagiosPath  = $r->getNagiosFilePath();
			
			if($relatedType == 'publisher') {
				
				$publisherId = $r->getId();
				require LINUX_SCRIPTS_PATH . 'syndGenerateNagiosConf.php';
				
			} elseif ($relatedType == 'client') {
				
				$clientId = $r->getId();
				require LINUX_SCRIPTS_PATH . 'syndGenerateNagiosConfClient.php';
			}
		} // End foreach 
	} // End function
	
	
	
	/*Note that each contact is owned by one client onlt
	*/
	public function getClientId()
	{
		$contactLink = Contact::getContactLinkObject($this->getId());
		return $contactLink->getClientId();
	}
	
	
	/*Note that each contact is owned by one client only
	*/
	public static function deleteCascadeClients($contactId)
	{
		$contact 		= ContactPeer::retrieveByPK($contactId);
		$contactLink 	= Contact::getContactLinkObject($contact->getId());
		
		$contactLink->delete();
		$contact->delete();
	}
	
	
	public static function getContactLinkObject($contactId)
	{
		$c = new Criteria();
		$c->add(ClientContactLinkPeer::CONTACT_ID , $contactId);
		return ClientContactLinkPeer::doSelectOne($c);
	}
	
	
	
	public function getErrorMsg ()
	{
		if('' == trim($this->getFullName())) {
			return 'Full name not provided';
		} else {
			
			if( (1 == (int)$this->getNotificationSms()) &&( ''==trim($this->getMobileNumber()) )  ) {
  				return 'Mobile number not enterd to send SMS';
  			} else {
  				if( (1 == (int)$this->getNotificationEmail()) &&( ''==trim($this->getEmail()) )  ) {
  					return 'Email Not Entered to send messages';
  				} else {
  					return false;
  				}
  			}
		}
	}
	
	
	
	
	
	
		
	
}
