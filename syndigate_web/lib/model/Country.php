<?php

/**
 * Subclass for representing a row from the 'country' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Country extends BaseCountry
{
	public function __toString()
	{
		return $this->getPrintableName();
	}
}
