<?php

/**
 * Subclass for performing query and update operations on the 'client_connection_sent' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ClientConnectionSentPeer extends BaseClientConnectionSentPeer
{
}
