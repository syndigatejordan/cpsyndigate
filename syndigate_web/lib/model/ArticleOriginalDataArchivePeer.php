<?php

/**
 * Subclass for performing query and update operations on the 'article_original_data_archive' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ArticleOriginalDataArchivePeer extends BaseArticleOriginalDataArchivePeer
{
}
