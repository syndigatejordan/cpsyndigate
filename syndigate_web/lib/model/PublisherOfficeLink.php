<?php

/**
 * Subclass for representing a row from the 'publisher_office_link' table.
 *
 * 
 *
 * @package lib.model
 */ 
class PublisherOfficeLink extends BasePublisherOfficeLink
{
	public static function deletePublisherOffice($publisherId, $officeId)
	{
		$c = new Criteria();
		$c->add(PublisherOfficeLinkPeer::PUBLISHER_ID , $publisherId);
		$c->add(PublisherOfficeLinkPeer::OFFICE_ID  , $officeId);
		PublisherOfficeLinkPeer::dodelete($c);

		$office = OfficePeer::retrieveByPK($officeId);
		$office->delete();
				
	}
}
