<?php

/**
 * Subclass for performing query and update operations on the 'tmp_utf8_errors' table.
 *
 * 
 *
 * @package lib.model
 */ 
class TmpUtf8ErrorsPeer extends BaseTmpUtf8ErrorsPeer
{
}
