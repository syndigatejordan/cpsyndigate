<?php

/**
 * Subclass for performing query and update operations on the 'connection_sent' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ConnectionSentPeer extends BaseConnectionSentPeer
{
}
