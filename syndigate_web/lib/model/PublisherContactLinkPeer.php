<?php

/**
 * Subclass for performing query and update operations on the 'publisher_contact_link' table.
 *
 * 
 *
 * @package lib.model
 */ 
class PublisherContactLinkPeer extends BasePublisherContactLinkPeer
{
}
