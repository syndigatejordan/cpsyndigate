<?php

/**
 * Subclass for representing a row from the 'bank_details' table.
 *
 * 
 *
 * @package lib.model
 */ 
class BankDetails extends BaseBankDetails
{
	
	public function __toString() {
		
		return $this->getName() . ' - ' . $this->getBranch();
	}
	
}
