<?php

/**
 * Subclass for representing a row from the 'title_type' table.
 *
 * 
 *
 * @package lib.model
 */ 
class TitleType extends BaseTitleType
{
	public function __toString()
	{
		return $this->getType();
	}
}
