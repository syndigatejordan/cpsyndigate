<?php

/**
 * Subclass for performing query and update operations on the 'titles_clients_rules' table.
 *
 * 
 *
 * @package lib.model
 */ 
class TitlesClientsRulesPeer extends BaseTitlesClientsRulesPeer
{
	
	public static function getTitleClients($titleId) {
		$c = new Criteria();
		$c->add(TitlesClientsRulesPeer::TITLE_ID, $titleId);
		
		$clients = array();
		$rows 	 = TitlesClientsRulesPeer::doselect($c);
		
		foreach ($rows as $row) {
			$clients[] = ClientPeer::retrieveByPK($row->getClientId());
		}		
		return $clients;
	}
	
	
	public static function getTitleClientsIds($titleId) {
		$titleClients = self::getTitleClients($titleId);
		
		$titleClientsIds = array();
		
		foreach ($titleClients as $client) {
			$titleClientsIds[] = $client->getId();
		}
		
		return $titleClientsIds;
	}
	
	
	public static function getClientTitles($clientId) {
		$c = new Criteria();
		$c->add(TitlesClientsRulesPeer::CLIENT_ID, $clientId);
				
		$titles  = array();
		$rows 	 = TitlesClientsRulesPeer::doselect($c);
		
		foreach ($rows as $row) {			
			$titles[] = TitlePeer::retrieveByPK($row->getTitleId());
		}		
		return $titles;
	}
	
	public static function getClientTitlesIds($clientId) {
		$clientTitles = self::getClientTitles($clientId);
		
		$clientTitlesIds = array();
		foreach ($clientTitles as $title) {
			$clientTitlesIds[] = $title->getId();
		}
		
		return $clientTitlesIds;
	}
	
	
}
