<?php

/**
 * Subclass for representing a row from the 'connection_sent' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ConnectionSent extends BaseConnectionSent
{
	private $files = array();
		
	public function __destruct(){}
	
	public function addToSent($file)
	{
		if(!is_array($this->files)) {
			$this->files = array();
		}
		if(!in_array($file, $this->files)) {
			$this->files[] = $file;
		}
		
	}
	
	public function getIsSentBefore($file)
	{
		if(!is_array($this->files)) {
			return false;
		} else {
			if(in_array($file, $this->files)) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	public function saveSettings()
	{
		if(!is_array($this->files)) {
			$this->files = array();
		}
		$this->setSentFiles(serialize($this->files));
		$this->save();
		$this->__destruct();
	}
	
	/**
	 * Remove all sent files from the array which is older than 2 days
	 *
	 */
	public function init()
	{		
		$this->files  = unserialize($this->getSentFiles());
		
		if(!is_array($this->files)) {
			$this->files = array();
			$this->setSentFiles(serialize($this->files));
			$this->save();
		}
		/*		
		$oldestDateToKeep = strtotime("-2 days");
		echo "oldest day to keep = " . $oldestDateToKeep . "\n\n\n";
			
		foreach ($this->files as $file) {
			//$fileDate = 
		}
		*/
		
		return;
	}	
}