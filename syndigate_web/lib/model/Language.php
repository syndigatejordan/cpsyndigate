<?php

/**
 * Subclass for representing a row from the 'language' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Language extends BaseLanguage
{
	public function __toString()
	{
		return $this->getName();
	}
	
	/*
	public static function getLanguages()
	{
		$c = new Criteria();
		$c->add();
		return LanguagePeer::doSelect($c);
	}
	*/
	
}
