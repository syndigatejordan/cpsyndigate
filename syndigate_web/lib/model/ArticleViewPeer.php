<?php

/**
 * Subclass for performing query and update operations on the 'article_view' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ArticleViewPeer extends BaseArticleViewPeer
{
}
