<?php

/**
 * Subclass for performing query and update operations on the 'publisher' table.
 *
 * 
 *
 * @package lib.model
 */ 
class PublisherPeer extends BasePublisherPeer
{
	public function getPublishersAsc()
	{
		$c = new Criteria();
		$c->addAscendingOrderByColumn(PublisherPeer::NAME );
		return PublisherPeer::doSelect($c);
	}
}
