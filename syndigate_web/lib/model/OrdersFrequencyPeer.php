<?php

/**
 * Subclass for performing query and update operations on the 'orders_frequency' table.
 *
 * 
 *
 * @package lib.model
 */ 
class OrdersFrequencyPeer extends BaseOrdersFrequencyPeer
{
}
