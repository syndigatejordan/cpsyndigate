<?php

/**
 * Subclass for performing query and update operations on the 'client_rule_filter' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ClientRuleFilterPeer extends BaseClientRuleFilterPeer
{
}
