<?php
class Title extends BaseTitle
{
	private $revisions = array();
	
	/**
	 * Static function to return active titles
	 *
	 * @return array of Title object
	 */
	public static function getActiveTitles()
	{
		$c = new Criteria();
		$c->add(TitlePeer::IS_ACTIVE, 1);
		$c->addAscendingOrderByColumn(TitlePeer::NAME);
		return TitlePeer::doSelect($c);
	}
	
	
	public function __toString() 
	{
   		return $this->getName();
	}
   
	
   /**
    * Some Titles may have the same name but diffrent is other like languages 
    * this function return the title name with other flag [such as lang]
    * @return String
    */
   public function getUniqueName()
   {
   		$name = $this->getName(); 
   		$lang = $this->getTheLanguage();
   		
   		if( $lang!= '' && 0== strpos(strtolower($name), strtolower($lang)) ) {
   			$name .= " ($lang)";
   		}
   		return $name;
   }
	
	
   /**
    * Return the articles count for this title [from first article until now]
    *
    * @return Integer
    */
	public function getArticlesCount()
	{
		$c = new Criteria();
		$c->add(ArticlePeer::TITLE_ID , $this->getId());
		return ArticlePeer::doCount($c);
	}
	

	/**
	 * Return the connection type for the title
	 *
	 * @return String
	 */
	public function getConnectionType()
	{		 
		$c = new Criteria();
		$c->add(TitleConnectionPeer::TITLE_ID, $this->getId());
		$obj = TitleConnectionPeer::doSelectOne($c);

		return is_object($obj) ? $obj->getConnectionType() : false;
	}
	
		
	public function getUrl()
	{
		$return = $this->getTitleConnection();
		return is_object($return) ? $return->getUrl() : '';
	}
	
	
	public function getFilePattern()
	{
		$return = $this->getTitleConnection();
		return is_object($return) ? $return->getFilePattern() : '';
	}
	
	
	public function getAuthintication()
	{
		$return = $this->getTitleConnection();
		return is_object($return) ? $return->getAuthintication() : '';
	}
	
	
	public function getUsername()
	{
		$return = $this->getTitleConnection();
		return is_object($return) ? $return->getUsername() : '';
	}
	
	
	public function getPassword()
	{
		$return = $this->getTitleConnection();
		return is_object($return) ? $return->getPassword() : '';
	}
	
	
	public function getExtras()
	{
		$return = $this->getTitleConnection();
		return is_object($return) ? $return->getExtras() : '';
	}
	
	
	public function getHasImages()
	{
		$return = $this->getTitleConnection();
		return is_object($return) ? $return->getHasImages() : '';
	}
	
		
	
	public function updateTitleConnection($params)
	{
		$c = new Criteria();
		$c->add(TitleConnectionPeer::TITLE_ID , $this->getID());
		$titleConnection =TitleConnectionPeer::doSelectOne($c);
		
		if (!is_object($titleConnection)) {
			$titleConnection = new TitleConnection();
		}
						
		$titleConnection->setTitleId($this->getId());
    	$titleConnection->setConnectionType($params['connection_type']);
    	$titleConnection->setUrl($params['url']);
    	$titleConnection->setFilePattern($params['file_pattern']);
    	$titleConnection->setAuthintication((int)$params['authintication']);
    	$titleConnection->setUsername($params['username']);
	    $titleConnection->setPassword($params['password']);
    	$titleConnection->setExtras($params['extras']);
    	$titleConnection->setHasImages((int)$params['has_images']);
    	$titleConnection->save();
	}	


	
	public function getCrontabId($runOnce=false)
	{
		$c = new Criteria();
		$c->add(GatherCrontabPeer::TITLE, $this->getId());
		if(!$runOnce) {
			$c->add(GatherCrontabPeer::RUN_ONCE, 0);
		}
		$result = GatherCrontabPeer::doSelectOne($c);
		return is_object($result)?$result->getId() :0;
	}
	
	/**
	 * Return the count of titles having the same ftp username so when create the new
	 *		title you can add the increment.
	 * 
	 * @param String $subFtpUsername
	 * @return integer
	 */
	public static function titlesCountHavingSameSubFtpName($subFtpUsername)
	{
		$c = new Criteria();
		$c->add(TitlePeer::FTP_USERNAME, $subFtpUsername . '%', CRITERIA::LIKE);
		return TitlePeer::doCount($c);
	}
	
	
	public function getTitleConnection()
	{
		$c = new Criteria();
		$c->add(TitleConnectionPeer::TITLE_ID , $this->getId());
		$titleConnection = TitleConnectionPeer::doSelectOne($c);
		return is_object($titleConnection) ? $titleConnection : new TitleConnection();
	}
	
	
	
	/**
	 * Delete Title articles by criteria 
	 *
	 * @param string $criteria = '' or 'revisionNum' or 'parsedAt'
	 * @param  array $param  case revisionNum : revisionFrom, optional revisionTo
	 */
	public function deleteArticles($criteria='', $params =array())
	{		
		$c 					= new Criteria();
		$articles 			= $this->getArticles();
		$articlesToDelete 	= array();
		
		switch ($criteria) {
			
			case 'all':
				$articlesToDelete = $this->getArticles();
				break;

			case 'by-ids':
				$articlesIdsInDatabase = array();

				$deletIds = $params['ids'];
				$deletIds = explode(',', $deletIds);
				array_walk( $deletIds, create_function('&$val', '$val = trim($val);') ); 

				foreach ($articles as $key => $article) {
					$articlesIdsInDatabase[$article->getId()] = $article->getId();

					foreach ($deletIds as $value) {
						if ( $value ) {
							if ( $article->getId() == $value ) {
								$articlesToDelete[] = $article;
							}
						}
					}
				}

				foreach ($deletIds as $value) {
					if ( $value ) {
						if ( ! in_array($value, $articlesIdsInDatabase ) ) {
							throw new Exception('Article id ' . $value . ' NOT exist in this title ');
						}
					}
				}
				break;
				
			case 'revision' :
				
				$revisionFrom 	= $params['revisionFrom'];
				$revisionTo		= $params['revisionTo'] ? $params['revisionTo'] : 0;
				
				if(!$revisionFrom) {
					throw new Exception('Incorrect revision From number');
				} else {
					if($revisionTo > $this->getMaxRevision() && $revisionTo!=0) {
						throw new Exception('Revision to must not be greater than max revision');
					} else {
						if($revisionFrom > $revisionTo) {
							throw new Exception('Revision From Must be less than or equal revision to');
						} else {
							
							foreach ($articles as $article) {
								
								$articleOriginal = $article->getArticleOriginalData();
								if($articleOriginal->getRevisionNum() >= $revisionFrom) {
									if($revisionTo!=0) {
										if($articleOriginal->getRevisionNum() <= $revisionTo) {
											$articlesToDelete[] = $article;
										}
									} else {
										$articlesToDelete[] = $article;
									}
								}	
							}// End foreach 	
						}
					}
				}
				break;
				
				
			case 'parese-date':
				
				if($params['parseDate'] >date('Y-m-d', time())){
					throw new Exception('Parsing date can not be in future');
				} else {
					
					foreach ($articles as $article) {
						if(substr($article->getParsedAt(),0,10) == $params['parseDate']) {
							$articlesToDelete[] = $article;
						}
					} //end for each
				}
				break;
			
			default:
					throw new Exception('Incorrect criteria in function deleteArticles()');
		} //end switch
				
		
		/**
		 * Delete the articles with original data and images
		 */
		require_once APP_PATH . 'conf/pheanstalk.conf.php';
		$memcache = new Memcache;
		$memcacheConnected = true;
		try {	
			$this->memcacheConnected = $memcache->connect(MEMCACHE_HOST, MEMCACHE_PORT);
		} catch (Exception $e) {
			$memcacheConnected = false;
		}
		
		
		foreach ($articlesToDelete as $a ) {
			try {
				if($memcacheConnected) {
					$memcacheCheckSum = $a->getTitleId() . '_' . sha1($a->getHeadline()) . '_' . $a->getBody();
					if($memcache->get($memcacheCheckSum)) {
						$memcache->delete($memcacheCheckSum);
					}
				}
				Article::deleteCascade($a->getId());
			} catch (Exception $e) {
				throw new Exception($e->getMessage());
			}
		}

		$this->setCachedArticlesToday(0);
		$this->setCachedCurrentSpan(0);
		$this->setCachedPrevSpan(0);
        $this->save();
		
	} // End function deleteArticles
	
	
	
	
	
	/**
	 * Return array of revisions numbers for this title
	 *
	 * @return array
	 */
	public function getRevisionsNums()
	{	
		if(is_array($this->revisions) && count($this->revisions)>0) {
			return $this->revisions;
		}
										
		$this->revisions = array();
															
		$sql  = "SELECT DISTINCT revision_num FROM article_original_data INNER JOIN article ON (article_original_data.id =  article.article_original_data_id)	WHERE article.title_id =" . $this->getId();
		$con  = Propel::getConnection();
		$stmt = $con->prepareStatement($sql);
		$rs   = $stmt->executeQuery();
		while ($rs->next()) {
			$this->revisions[] = $rs->getInt('revision_num');
		}
		ksort($this->revisions);
		return $this->revisions;

		/*
		$articles  = $this->getArticles();
		$revisions = array();
			
		foreach ($articles as $article) {
			$originalData = $article->getArticleOriginalData();
			
			if(!in_array($originalData->getRevisionNum(), $revisions)) {
				$revisions[$originalData->getRevisionNum()] = $originalData->getRevisionNum();
			}
		}
		ksort($revisions);
		return $revisions;
		*/
	}
	

	/**
	 * Return the last revision number for this title
	 *
	 * @return Integer
	 */
	public function getMaxRevision()
	{		
		return max($this->getRevisionsNums());
	}
	
	/**
	 * Return the first available revision number for this title
	 *
	 * @return Integer
	 */
	public function getMinRevision()
	{
		return min($this->getRevisionsNums());
	}
	
	/**
	 * Another Implementation for the function getLAnguage(), due to the restriction
	 * Implementation of functions names
	 *
	 * @return String [language name] or NA if the title has no language
	 */
	public function getTheLanguage()
	{
		/*
		$language = LanguagePeer::retrieveByPK($this->getLanguageId());
		return is_object($language) ? $language->getName() : 'NA';
		*/
		$language = LanguagePeer::retrieveByPK($this->getLanguageId());
		if(!is_object($language)) {
			$language = new Language();
			$language->setName('NA');
		}
		return $language;
	}
	
	
	/**
	 * Another Implementation for the function getCountry(), due to the restriction
	 * Implementation of functions names
	 *
	 * @return Country object
	 */
	public function getTheCountry() 
	{
		$country = CountryPeer::retrieveByPK($this->getCountryId());
		if(is_object($country)) {
			return $country;
		} else {
			$country = new Country();
			$country->setId(0);
			$country->setIso('');
			$country->setName('NA');
			$country->setPrintableName('NA');
			$country->setIso3('');
			$country->setNumcode(0);
			return $country;
		}
	}
	
	
	/**
	 * return if the title publisher has contact
	 *
	 * @param Integer $contactType Contact::$typeAll =0 or Contact::$typeBusiness=1 or Contact::$typeTechnical=2
	 * @return boolean
	 */
	public function hasContact($contactType=1) {
		
		$c = new Criteria();
		$c->add(PublisherContactLinkPeer::PUBLISHER_ID, $this->getPublisherId());
		$contacts = PublisherContactLinkPeer::doSelect($c);
		
		$businessCounter =0;
		$technicalCounter=0;
		
		foreach ($contacts as $contactLink) {
			$contact = $contactLink->getContact();
			$businessCounter  = $contact->getType() == 'business' ? $businessCounter+1  : $businessCounter;
			$technicalCounter = $contact->getType() == 'technical'? $technicalCounter+1 : $technicalCounter;
		}
		
		if($contactType == Contact::$typeBusiness) {
			return $businessCounter > 0 ? true : false;
		} else if ($contactType == Contact::$typeTechnical) {
			return $technicalCounter >0 ? true : false;	
		} else if ($contactType == Contact::$typeBoth) {
			return (($technicalCounter>0) && ($businessCounter>0)) ? true : false;	
		} else if($contactType == Contact::$typeAny) {
			return (($technicalCounter>0) || ($businessCounter>0)) ? true : false;	
		} else {
			throw new Exception('Invalid Contact Type');
		}
	}
	
	
	//-- Values from publisher --------------------------------------------------------------\\
	
	# Shared values for titles having the same publisher
	# these values are from the publisher
	
	public function getContract()
	{
		return $this->getPublisher()->getContract();
	}
	
	public function getRoyaltyRate()
	{
		return $this->getPublisher()->getRoyaltyRate();
	}
	
	public function getContractStartDate($format = 'Y-m-d')
	{
		return $this->getPublisher()->getContractStartDate();
	}
	
	public function getContractEndDate($format = 'Y-m-d')
	{
		return $this->getPublisher()->getContractEndDate();
	}
	
	public function getRenewalPeriod()
	{
		return $this->getPublisher()->getRenewalPeriod();
	}
	
	//-- End values from publisher ----------------------------------------------------------\\
	
	
		
	/**
	 * Return the articles count during a span
	 *
	 * @param Integer $span : the span period  default 0 the title current span
	 * 							ex: if title frequency is weekly
	 * 								-1 : get this week and previos week
	 * 								-2 : get this week and previos previos week 
	 * 							ex. if daily 
	 * 								-1 : get this day and previos day 
	 * 						
	 */
	public function getArticlesSpanCount($spanFrom=0, $spanto=0) {
		$frequences = GatherCrontab::getDefinitions();
		$crontab    = GatherCrontabPeer::retrieveByPK($this->getCrontabId(0));
		
		if(!is_object($crontab)) {
			return false;
		} else {
			$period   = $frequences[$crontab->getCronDefinition()];
			
			if( in_array(strtolower($period), array('daily', 'daily (newswire)') ) ) {
				return $this->getDaysSpanArticlesCount($spanFrom, $spanto);
			} elseif ('weekly' == strtolower($period)) {
				return $this->getWeeksSpanArticlesCount($spanFrom, $spanto);
			} elseif ('monthly' == strtolower($period)) {
				return $this->getMonthsSpanArticlesCount($spanFrom, $spanto);
			}
		}
	}
	
	/**
	 * Adapter because we can not use parameters from generate.yml
	 *
	 */
	public function getPreviosSpanCount()
	{
		return $this->getArticlesSpanCount(-1);
	}
	
	
	/**
	 * Return the Count of articles that has been parsed today [parsed_at field]
	 *
	 * @return Integer
	 */
	public function getArticlesToday() {
		
		$from  = strtotime(date('Y-m-d', time()));
		return $this->getArticlesCountBetween($from, time());
		return $this->getDaysSpanArticlesCount(0,0);
	}
	

	/**
	 * Return the COUNT of articles that has been parsed between two timespams
	 *
	 * @param Integer $timeStampFrom
	 * @param Integer $timeStampTo
	 * @return Integer
	 */
	public function getArticlesCountBetween($timeStampFrom, $timeStampTo)
	{
		$databaseManager = new sfDatabaseManager();
		$databaseManager->initialize();
		$conn	= Propel::getConnection(PublisherPeer::DATABASE_NAME );
		
		$dateFrom = date('Y-m-d G:i:s', $timeStampFrom);
		$dateTo	  = date('Y-m-d G:i:s', $timeStampTo);
		
		/*
		$c = new Criteria();
		$c->add(ArticlePeer::TITLE_ID, $this->getId());
		$c->add(ArticlePeer::PARSED_AT, "$dateFrom", Criteria::GREATER_EQUAL);
		$c->addAND(ArticlePeer::PARSED_AT, "$dateTo", Criteria::LESS_EQUAL);
		return ArticlePeer::doCount($c);
		*/
		
		$query 		= "SELECT COUNT(article.ID) AS cnt FROM article force INDEX (article_FKIndex41,parsed_at)  WHERE article.TITLE_ID= " . $this->getId() . " AND (article.PARSED_AT>='$dateFrom' AND article.PARSED_AT<='$dateTo') ;";
		$countArr = self::executeSlaveQuery($query);
		$countArr = $countArr['cnt'];
		return $countArr;
//var_dump($countArr);exit;
		$statement 	= $conn->prepareStatement($query);
		$resultset 	= $statement->executeQuery();
		$resultset->next();
	    //return $resultset->getInt('cnt');
		
	}
	
	
	/**
	 * Return the COUNT of articles parsed between 2 spans [span unit in week 0= current week, -1 last week]
	 *
	 * @param Integer $spanFrom
	 * @param Integer $spanTo
	 * @return Integer
	 */
	public final function getWeeksSpanArticlesCount($spanFrom, $spanTo =0)
	{
		if($spanFrom>$spanTo) {
			throw new Exception('Date From can not be greater than date to');
		} else {
			
			$spanTo  +=1;		
			$spanFromTs = strtotime("$spanFrom week", strtotime("last sunday", time()));
			$spanToTs 	= strtotime("$spanTo week", strtotime("last sunday", time()));
			
			$spanFromTs = strtotime(date('Y-m-d', $spanFromTs)); 
			$spanToTs   = strtotime(date('Y-m-d', $spanToTs)) -1; 
			
			return $this->getArticlesCountBetween($spanFromTs, $spanToTs);
		}
	}
	
	/**
	 * Return the COUNT of articles parsed between 2 spans [span unit in month 0= current month, -1 last month]
	 *
	 * @param Integer $spanFrom
	 * @param Integer $spanTo
	 * @return Integer
	 */
	public final function getMonthsSpanArticlesCount($spanFrom, $spanTo =0)
	{
		if($spanFrom>$spanTo) {
			throw new Exception('Date From can not be greater than date to');
		} else {
			
			$spanTo  +=1;		
			$spanFromTs = strtotime("$spanFrom month", strtotime(date('Y-m-01', time())) );
			$spanToTs 	= strtotime("$spanTo month", strtotime(date('Y-m-01', time())) );
			
			$spanFromTs = strtotime(date('Y-m-d', $spanFromTs)); 
			$spanToTs   = strtotime(date('Y-m-d', $spanToTs)) -1; 
			
			//echo date('Y-m-d : H:i:s', $spanFromTs) . '<br>';
			//echo date('Y-m-d : H:i:s', $spanToTs);
			
			return $this->getArticlesCountBetween($spanFromTs, $spanToTs);
		}
	}
	
	/**
	 * Return the COUNT of articles parsed between 2 spans [span unit in Day 0= today, -1 yesterday]
	 *
	 * @param Integer $spanFrom
	 * @param Integer $spanTo
	 * @return Integer
	 */
	public final function getDaysSpanArticlesCount($spanFrom, $spanTo = 0) 
	{
		if($spanFrom>$spanTo) {
			throw new Exception('Date From can not be greater than date to');
		} else {
			
			$spanTo    += 1;		
			$spanFromTs = strtotime("$spanFrom day");
			$spanToTs 	= strtotime("$spanTo day");
			
			$spanFromTs = strtotime(date('Y-m-d', $spanFromTs)); 
			$spanToTs   = strtotime(date('Y-m-d', $spanToTs)) -1; 
						
			return $this->getArticlesCountBetween($spanFromTs, $spanToTs);
		}
	}
	
	
	/**
	 * Fix the next try timer and gather retries according to a crondefinition
	 * This function must be called after, set the value of the crontab 
	 *
	 * @param String $CronDefinition Cron definition syntax
	 */
	public function setTimerAndRetriesFromCronDef($cronDefinition) {
				
		$definitions = GatherCrontab::getDefinitions(true);
		
		if(0==$definitions[$cronDefinition]['nextTryTimer'] && 'Daily (Newswire)'!=$definitions[$cronDefinition]['frequency']) {
			die('Cron Definition : ' . $definitions[$cronDefinition]['frequency'] . ', the nextTryTimer value not implemented , implement it in /lib/models/GatherCrontab.php , function name is getDefinitionsSettings');
		} else {
			if(0==$definitions[$cronDefinition]['gatherRetries'] && 'Daily (Newswire)'!=$definitions[$cronDefinition]['frequency']) {
				die('Cron Definition : ' . $definitions[$cronDefinition]['frequency'] . ', the gatherRetries value not implemented , implement it in /lib/models/GatherCrontab.php , function name is getDefinitionsSettings');
			} else {
								
				$this->setNextTryTimer($definitions[$cronDefinition]['nextTryTimer']);
				$this->setGatherRetries($definitions[$cronDefinition]['gatherRetries']);
				
				if($definitions[$cronDefinition]['frequency'] != 'Daily (Newswire)') {
					$this->setIsNewswire(false);
				} else {
					$this->setIsNewswire(true);
				}
				
				return;
			}
		}
	}
	
	
	
	
	/**
	 * here the part for deleting all related tables to the title, in order to delete the title
	 */
	
	public static function deleteTitle($titleId)
	{
		$title = TitlePeer::retrieveByPK($titleId);
		
		if(! $title instanceof Title) {
			throw new Exception('Incorrect title id');
		} else {
			
			$title->delCategoryMapping();
			$title->delOriginalArticleCategories();
			$title->deleteArticles('all'); // delete articles with all related tables records 
			$title->delTitleConnection();
			$title->delTitleFromClientsFilters();
			$title->delGatherCrontabs();
			$title->delGatherCronjobs();
			$title->delParsingStates();
			$title->delReportGather();
			$title->delReportParse();
			$title->delSchedule();
			//$title->delContacts();
			//$title->delOfficesLinks();
			
			//Delete titles clients from the titles_clients_rules table 
			$c = new Criteria();
			$c->add(TitlesClientsRulesPeer::TITLE_ID, $title->getId());		
			TitlesClientsRulesPeer::dodelete($c);
			
			$title->delete();
			return true;
		}
	}
	
	public function delTitleConnection()
	{
		/**
		 * may be exist more than one connection [misktake]
		 * so remove them all to prevent constraint faults
		 */
		$c = new Criteria();
		$c->add(TitleConnectionPeer::TITLE_ID, $this->getId());
		TitleConnectionPeer::doDelete($c);
	}
	
	public function delCategoryMapping()
	{
		//get OriginalArticleCategories 
		$c = new Criteria();
		$c->add(OriginalArticleCategoryPeer::TITLE_ID, $this->getId());
		$categories = OriginalArticleCategoryPeer::doSelect($c);
		
				
		foreach ($categories as $category) {
			
			//del the category mapping for it
			$c = new Criteria();
			$c->add(CategoryMappingPeer::ORIGINAL_ARTICLE_CATEGORY_ID , $category->getId());
			CategoryMappingPeer::doDelete($c);
		}
	}
	
	public function delOriginalArticleCategories()
	{
		$c = new Criteria();
		$c->add(OriginalArticleCategoryPeer::TITLE_ID, $this->getId());
		OriginalArticleCategoryPeer::doDelete($c);
	}
	
	
	public function delTitleFromClientsFilters()
	{
		$clients = ClientPeer::doSelect(new Criteria());
		
		foreach ($clients as $client) {
			
			$c = new Criteria();
			$c->add(ClientRuleFilterPeer::FILTER_TYPE, 'title');
			$clientRuleFilters = ClientRuleFilterPeer::doSelect($c);
				
			foreach ($clientRuleFilters as $clientRuleFilter) {
				
				$filterTitles = $clientRuleFilter->getSelectedValues();
							
				if(in_array($this->getId(), array_values($filterTitles))) {
					
					$filterTitles = array_flip($filterTitles);
					unset($filterTitles[$this->getId()]);
					$filterTitles = array_flip($filterTitles);
					$clientRuleFilter->setFilterValues($filterTitles);
					$clientRuleFilter->save();
				}
			}
		}
	}
	
		
	public function delGatherCrontabs()
	{	
		$c = new Criteria();
		$c->add(GatherCrontabPeer::TITLE, $this->getId());
		GatherCrontabPeer::doDelete($c);
	}
	
	public function delGatherCronjobs()
	{
		$c = new Criteria();
		$c->add(GatherCronjobPeer::IMPLEMENTATION_ID, $this->getId());
		GatherCronjobPeer::doDelete($c);
	}
	
	public function delParsingStates()
	{
		$c = new Criteria();
		$c->add(ParsingStatePeer::TITLE_ID, $this->getId());
		ParsingStatePeer::doDelete($c);
	}
	
	public function delReportGather()
	{
		$c = new Criteria();
		$c->add(ReportGatherPeer::TITLE_ID, $this->getId());
		ReportGatherPeer::doDelete($c);
	}
	
	public function delReportParse()
	{
		$c = new Criteria();
		$c->add(ReportParsePeer::TITLE_ID, $this->getId());
		ReportParsePeer::doDelete($c);
	}
	
	public function delSchedule()
	{
		$c = new Criteria();
		$c->add(SchedulePeer::TITLE_ID, $this->getId());
		SchedulePeer::doDelete($c);
	}
	
	public function delContacts()
	{
		// empty for future modificatins
		
	}
	
	public function delOfficesLinks()
	{
		// empty for future modificatins
	}
	
	
	
		
	/**
	 * Return the story count between two spans [the span depend on the cronjop frequency]
	 *
	 * @param Integer $numOfSpans   : number of spans to calculate, Default = 0 [all spans]
	 * @param boolan  $excludeZeros : exculde spans with zero articles count 
	 */
	public function getStoryCounting($numOfSpans=0, $excludeZeros=false) 
	{
		$firstspan = $this->getFirstSpan();
		
		if(!$firstspan) {
			return false;
		} else {
			
			$spanFrom = 0;  $spanTo = 0; $countArray = array();
			
			$numOfSpans = $numOfSpans<= 0 ? abs($firstspan) : $numOfSpans;
			$numOfSpans = $numOfSpans>abs($firstspan) ? abs($firstspan) : $numOfSpans;
			$counter    =0;
			
			while ($counter<$numOfSpans) {
				$fromDate = date('Y-m-d:H:i:s', $this->spanFromToDate($spanFrom));
				$toDate   = date('Y-m-d:H:i:s', $this->spanToToDate($spanTo));
				
				$count = $this->getArticlesSpanCount($spanFrom, $spanTo);
				if($count==0) {
					if($excludeZeros && $numOfSpans<abs($firstspan)) {
						$numOfSpans++;
					}
					if(!$excludeZeros) {
						$countArray["$fromDate - $toDate"] = $count;
					} 
				} else {
					$countArray["$fromDate - $toDate"] = $count;
				}
				
				$counter++; $spanFrom--; $spanTo--;
				
			} // End while
			
			return $countArray;
		}//end if 
		
	} // End function getStoryCount()
	
	
	public function getFirstSpan()
	{
		$c = new Criteria();
		$c->addAscendingOrderByColumn(ArticlePeer::PARSED_AT);
		$c->add(ArticlePeer::TITLE_ID, $this->getId());
	//	$c->add(ArticlePeer::PARSED_AT, 'NULL', Criteria::NOT_EQUAL);
		$result = ArticlePeer::doSelectOne($c);
		
		if(!is_object($result)) {
			return false;
		}
		
		$deff = strtotime($result->getParsedAt()) - time();
		
		$period = $this->getFrequencyString();
		if( $period =='daily' ) {
			$period = 1;
		} elseif ($period =='weekly') {
			$period = 7;
		} elseif ($period == 'monthly') {
			$period = 31;
		} else {
			$period = 10000;
		}
		
		 return floor($deff / (60*60*24*$period));
		
	}
	
	
	
	
	public function spanFromToDate($spanFrom)
	{
		$frequency = $this->getFrequencyString();
		
		if($frequency=='daily') {
			
			$spanFromTs = strtotime("$spanFrom day");
			$spanFromTs = strtotime(date('Y-m-d', $spanFromTs)); 
		} elseif ($frequency == 'weekly') {
			
			$spanFromTs = strtotime("$spanFrom week", strtotime("last sunday", time()));
			$spanFromTs = strtotime(date('Y-m-d', $spanFromTs)); 
		} elseif ($frequency == 'monthly') {
			
			$spanFromTs = strtotime("$spanFrom month", strtotime(date('Y-m-01', time())));
			$spanFromTs = strtotime(date('Y-m-d', $spanFromTs)); 
		} else {
			return false;
		}
		
		return $spanFromTs;
	}
	
	
    public function spanToToDate($spanTo)
	{
		$frequency = $this->getFrequencyString();
		
		if($frequency=='daily') {
			
			$spanTo    += 1;		
			$spanToTs 	= strtotime("$spanTo day");
			$spanToTs   = strtotime(date('Y-m-d', $spanToTs)) -1; 
		} elseif ($frequency == 'weekly') {
			
			$spanTo    +=1;		
			$spanToTs 	= strtotime("$spanTo week", strtotime("last sunday", time()));
			$spanToTs   = strtotime(date('Y-m-d', $spanToTs)) -1; 
		} elseif($frequency == 'monthly') {
			
			$spanTo    +=1;		
			$spanToTs 	= strtotime("$spanTo month", strtotime(date('Y-m-01', time())));
			$spanToTs   = strtotime(date('Y-m-d', $spanToTs)) -1; 
		} else {
			return false;
		}
		
		return $spanToTs;
	}
	
	
	/**
	 * Return the frequency string without considration other flags
	 * such as daily or daily newswire are the same which is daily 
	 * and so on for the weekly or monthly etc ....
	 * 
	 *
	 * @return String
	 */
	public function getFrequencyString()
	{
		$frequences = GatherCrontab::getDefinitions();
		$crontab    = GatherCrontabPeer::retrieveByPK($this->getCrontabId(0));
		
		if(!is_object($crontab)) {
			return false;
		} else {
			$period   = $frequences[$crontab->getCronDefinition()];
			
			if( in_array(strtolower($period), array('daily', 'daily (newswire)') ) ) {
				return 'daily';
			} elseif ('weekly' == strtolower($period)) {
				return 'weekly';
			} elseif ('monthly' == strtolower($period)) {
				return 'monthly';
			} else {
				return false;
			}
		}
	}
	
	
	/**
	 * Return true if the title sent articles during a number of span or false otherwise
	 *
	 * @param Integer $spansToCheck : the number of spans to check if the title sent articles
	 * 									during it 
	 * @return boolean
	 */
	public function isTitleSending($spansToCheck =3)
	{		
		if($this->getIsReceiving()) {
			return true;
		} else {
			return $this->isTitleParsing($spansToCheck);
		}
	}
	
	public function isTitleParsing($spansToCheck =3)
	{
		if((int)$this->getArticlesSpanCount((-1*$spansToCheck), 0) >0) {
				return true;
			} else {
				return false;
		}
	}

	public function getAdminsId() {
		return $this->getPublisher()->getAdminsId();
	}	

    public static function executeSlaveQuery($sql) {
        $servername = "10.200.200.66";
        $username = "syndigate";
        $password = "bQAuA4DuhtX6xyaE";
        $dbname = "syndigate";

// Create connection
        $slave_conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
        if ($slave_conn->connect_error) {
            die("Connection failed: " . $slave_conn->connect_error);
        }

        $result = $slave_conn->query($sql);

        if ($result->num_rows > 0) {
            // output data of each row
            $data = array();
            while ($row = $result->fetch_assoc()) {
                $data = $row;
                //  echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            }
        }
        $slave_conn->close();
        return $data;
    }	
	
}
