<?php

class LogClientRule extends BaseLogClientRule
{
	
	/**
	 * Constructor :
	 *
	 * @param Integer $clientId : client id which the rules will be changed for it 
	 * @param Integer $adminId : the admin id who is logged in
	 */
	public function __construct($clientId, $adminId) {
		
		$this->setClientId($clientId);
		$this->setAdminId($adminId);
		$this->setTimestamp(time());
	}
	
	/**
	 * Insert the log record to the database, Not that the PreviousStatus and CurrentStatus
	 * must be set via logPreviousStatus and logCurrentStatus to tracking the changes in 
	 * the client rule
	 * 
	 *
	 * @param String $operation : The operation which happened -description of the operation-
	 */
	public function Log($operation) {
		$this->setOperation($operation);
	}
	
	/**
	 * Log the status of the clientRules before it has been changed
	 * this should be called -BEFORE- the changes happned
	 */
	public function logPreviousStatus() {
		$this->setPreviousStatus($this->getStatus());
	}
	
	
	/**
	 * Log the status of the ClientRules After it has been changed
	 * This should be called -AFTER- the changes happened.
	 *
	 */
	public function logCurrentStatus() {
		$this->setCurrentStatus($this->getStatus());
	}
	
	/**
	 * Get the client rules and the client rules filters stats
	 *
	 * @return serialized array of rules
	 */
	public function getStatus() {
		
		$rules = array();
		$rule;
		
		$c = new Criteria();
		$c->add(ClientRulePeer::CLIENT_ID, $this->getClientId());
		$clientRules = ClientRulePeer::doSelect($c);
				
		foreach ($clientRules as $clientRule) {
			
			$filters     = $clientRule->getClientRuleFilters();
			$ruleFilters = array();
			
			foreach ($filters as $filter)  {
				$rule = '['. $filter->getFilterType() .']' . '[' . $filter->getFilterValue() . ']';
				$ruleFilters[] = $rule;
			}
			$rules[] = $ruleFilters;
		}
		return serialize($rules);
	}
	
	
}
