<?php

/**
 * Subclass for representing a row from the 'category_mapping' table.
 *
 * 
 *
 * @package lib.model
 */ 
class CategoryMapping extends BaseCategoryMapping
{		
	
	public static function getTitleCategories($titleId)
	{
		$c = new Criteria();
		$c->add(OriginalArticleCategoryPeer::TITLE_ID , $titleId);
		return OriginalArticleCategoryPeer::doSelect($c);
	}
	
	/*
	public static function getTitleMappedCategories($titleId)
	{
		$titleCategories = CategoryMapping::getTitleCategories($titleId);
		
		
		$mappedCategories = array();
		
		foreach ($titleCategories as $titleCategory) {
			
			$c = new Criteria();
			$c->add(CategoryMappingPeer::ORIGINAL_ARTICLE_CATEGORY_ID, $titleCategory->getId());
			
			if(is_object(CategoryMappingPeer::doSelectOne($c))) {
				$mappedCategories[] = $titleCategory;
			}
		}
		
		return $mappedCategories;
	}
	*/
	
	/*
	public static function getTitleInMappedCategories($titleId)
	{
		$categoriesIds 		= array();
		$mappedCategoiesIds	= array();
		
		foreach (CategoryMapping::getTitleCategories($titleId) as $titleCat) {
			$categoriesIds[] = $titleCat->getId();
		}
		
		foreach (CategoryMapping::getTitleMappedCategories($titleId) as $mapped) {
			$mappedCategoiesIds[] = $mapped->getId();
		}
		
		$diff =  array_diff($categoriesIds, $mappedCategoiesIds);
		
		
		$diffObjects = array();
		foreach ($diff as $d) {
			$diffObjects[] = OriginalArticleCategoryPeer::retrieveByPK($d);
		}
		return $diffObjects;
	}
	*/
	
	/*
	public static function getTitleCategoryMapping($titleId)
	{
		$originalMappedCategories = CategoryMapping::getTitleMappedCategories($titleId);
		
		$titleCategoryMapping = array();
		
		foreach ($originalMappedCategories as $original) {
			
			$c = new Criteria();
			$c->add(CategoryMappingPeer::ORIGINAL_ARTICLE_CATEGORY_ID, $original->getId());
			$titleCategoryMapping[] = CategoryMappingPeer::doSelectOne($c);
		}
		
		return $titleCategoryMapping;
	}
	*/
	
	
	public static function linkMap($originalArticleCatId, $iptcId) {
		
		if(CategoryMapping::isMappedCategory($originalArticleCatId)) {
			return false;
		} else {
			$categoryMapping = new CategoryMapping();
			$categoryMapping->setOriginalArticleCategoryId($originalArticleCatId);
			$categoryMapping->setIptcId($iptcId);
			$categoryMapping->save();
			return $categoryMapping->getId();
		}
	}
	
	
	public static function unLinkMap($originalArticleCatId) {
		$c = new Criteria();
		$c->add(CategoryMappingPeer::ORIGINAL_ARTICLE_CATEGORY_ID, $originalArticleCatId);
		CategoryMappingPeer::doDelete($c);
	}
	
	public static function deleteOriginalCategory($categoryId)
	{
		if(CategoryMapping::isMappedCategory($categoryId)) {
			CategoryMapping::unLinkMap($categoryId);
		}
		
		$obj = OriginalArticleCategoryPeer::retrieveByPK($categoryId);
		return $obj->delete();
	}
	
	
	public static function isMappedCategory($originalArticleCat)
	{
		$c = new Criteria();
		
		if(is_object($originalArticleCat)) {
			$c->add(CategoryMappingPeer::ORIGINAL_ARTICLE_CATEGORY_ID , $originalArticleCat->getId());
		} else {
			$c->add(CategoryMappingPeer::ORIGINAL_ARTICLE_CATEGORY_ID , $originalArticleCat);
		}
		
		$result = CategoryMappingPeer::doSelectOne($c);
		return is_object($result) ? true : false;
	}
	
	
	//-------------------------------------------------------------------------------\\
	                
	public function getOriginalArticleCategory($con = null)
	{
		return OriginalArticleCategoryPeer::retrieveByPK($this->getOriginalArticleCategoryId());
	}
	
	public function getIptc($con = null)
	{
		return IptcPeer::retrieveByPK($this->getIptcId());
	}
	
	public static function isMappingExist($iptcId, $originalArticleCategoryId)
	{
		$c = new Criteria();
		$c->add(CategoryMappingPeer::IPTC_ID , $iptcId);
		$c->add(CategoryMappingPeer::ORIGINAL_ARTICLE_CATEGORY_ID , $originalArticleCategoryId);
		$result = CategoryMappingPeer::doSelectOne($c);
		
		return is_object($result) ? true : false;
	}
	
}
