<?php

/**
 * Subclass for representing a row from the 'client_contact_link' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ClientContactLink extends BaseClientContactLink
{
	public static function deleteClientContact($clientId, $contactId)
	{
		$c = new Criteria();
		$c->add(ClientContactLinkPeer::CLIENT_ID, $clientId);
		$c->add(ClientContactLinkPeer::CONTACT_ID , $contactId);
		ClientContactLinkPeer::dodelete($c);
		
		$client  	= ClientPeer::retrieveByPK($clientId);
		$nagiosPath = $client->getNagiosFilePath();
							
		//delete the contact
		$contact = ContactPeer::retrieveByPK($contactId);
		$contact->delete();
		
		require LINUX_SCRIPTS_PATH . 'syndGenerateNagiosConfClient.php';
	}
}
