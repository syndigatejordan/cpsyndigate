<?php

/**
 * Subclass for representing a row from the 'gather_crontab' table.
 *
 * 
 *
 * @package lib.model
 */ 
class GatherCrontab extends BaseGatherCrontab
{
	public static function getDefinitions($full = false)
	{
		$definitions = array();
	
		$definitions['9 * * * *'] 		= array('frequency'=>'Daily (Newswire)', 	'nextTryTimer'=> 0,     'gatherRetries'=>0);
		$definitions['0 1 * * *'] 		= array('frequency'=>'Daily', 				'nextTryTimer'=> 3600,  'gatherRetries'=>22);
		$definitions['0 1 * * 0'] 		= array('frequency'=>'Weekly', 				'nextTryTimer'=> 43200, 'gatherRetries'=>12);
		$definitions['0 1 * * 0,2,4,6'] = array('frequency'=>'Four Times Per Week', 'nextTryTimer'=> 43200, 'gatherRetries'=>3 );
		$definitions['0 1 * * 0,3'] 	= array('frequency'=>'Twice-Weekly', 		'nextTryTimer'=> 43200,	'gatherRetries'=>5 );
		$definitions['0 1 1 * *'] 		= array('frequency'=>'Monthly', 			'nextTryTimer'=> 43200,	'gatherRetries'=>56);
		$definitions['0 1 1,15 * *'] 	= array('frequency'=>'Bi-monthly', 			'nextTryTimer'=> 86400,	'gatherRetries'=>56);		
		
		if (!$full) {
			foreach ($definitions as $key => $def) {
				$definitions[$key] = $def['frequency'];
			}
		}
		
		return $definitions;
	}
	
}
