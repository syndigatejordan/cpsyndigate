<?php

/**
 * Subclass for representing a row from the 'client_sent' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ClientSent extends BaseClientSent
{

	private $files = array();
	public function __destruct(){}
	
	public function addToSent($file)
	{
		if(!is_array($this->files)) {
			$this->files = array();
		}
		if(!in_array($file, $this->files)) {
			$this->files[] = $file;
		}
		
	}
	
	public function getIsSentBefore($file)
	{
		if(!is_array($this->files)) {
			echo "this is not an array \n";
			return false;
		} else {
			if(in_array($file, $this->files)) {
				echo "yes it is sent before \n";
				return true;
			} else {
				echo "No this file has not been sent before \n";
				return false;
			}
		}
	}
	
	public function saveSettings()
	{
		if(!is_array($this->files)) {
			$this->files = array();
		}
		$this->setSentFiles(serialize($this->files));
		$this->save();
		echo "printing  me before exit ------------------ \n\n";
		print_r($this);
		$this->__destruct();
	}
	
	/**
	 * Remove all sent files from the array which is older than 2 days
	 *
	 */
	public function init()
	{		
		$this->files  = unserialize($this->getSentFiles());
		
		if(!is_array($this->files)) {
			$this->files = array();
			$this->setSentFiles(serialize($this->files));
			$this->save();
		}
		/*		
		$oldestDateToKeep = strtotime("-2 days");
		echo "oldest day to keep = " . $oldestDateToKeep . "\n\n\n";
			
		foreach ($this->files as $file) {
			//$fileDate = 
		}
		*/
		
		return;
	}	
	
}
