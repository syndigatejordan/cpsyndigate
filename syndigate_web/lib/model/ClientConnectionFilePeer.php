<?php

/**
 * Subclass for performing query and update operations on the 'client_connection_file' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ClientConnectionFilePeer extends BaseClientConnectionFilePeer
{
}
