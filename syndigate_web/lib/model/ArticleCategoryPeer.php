<?php

/**
 * Subclass for performing query and update operations on the 'article_category' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ArticleCategoryPeer extends BaseArticleCategoryPeer
{
}
