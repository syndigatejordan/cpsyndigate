<?php

/**
 * Subclass for representing a row from the 'admins' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Admins extends BaseAdmins
{
	public function toString() {
		return $this->getUsername();
	}	
}
