<?php

/**
 * Subclass for representing a row from the 'iptc' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Iptc extends BaseIptc
{
	public function toString()
	{
		return $this->getIptcEnglish();
	}
	/**
	 * Return iptc instance variable
	 *
	 * @param String $lang
	 * @return iptc_lang where lang = arabic or english or frensh
	 */
	public function getIptcLang($lang)
	{
		switch (strtoupper($lang)) {
			case 'ARABIC':
				return $this->getIptcArabic();
			case 'ENGLISH':
				return $this->getIptcEnglish();
			case 'FRENCH':
				return $this->getIptcFrench();
			default:
				return 'No suppoted Language';
		}
	}
	
	public static function getCategories()
	{
		$result = IptcPeer::doSelect(new Criteria());
		
		$array = array();		
		foreach ($result as $r ) {
			$array[$r->getId()] = $r->getIptcEnglish(); 
		}
				
		return $array;
	}
	
	
	
	
}
