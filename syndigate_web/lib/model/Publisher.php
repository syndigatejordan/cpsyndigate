<?php

/**
 * Subclass for representing a row from the 'publisher' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Publisher extends BasePublisher
{
	public function __toString()
	{
		return $this->getName();
	}
			
	public function getTitlesCount()
	{
		$c = new Criteria();
		$c->add(TitlePeer::PUBLISHER_ID, $this->getId());
		return TitlePeer::doCount($c);
	}
	
	
	public function getArticlesCount()
	{
		$titles = $this->getTitles();
			
		$count = 0;	
		foreach ($titles as $t) {
			$count += $t->getArticlesCount();
		}
		
		return $count;
	}
	
	
	
	public function hasBankDetails()
	{
		$c = new Criteria();
		$c->add (BankDetailsPeer::PUBLISHER_ID, $this->getId());
		return BankDetailsPeer::doCount($c);
	}
	
	
	public function getBanksDetails()
	{
		$c = new Criteria();
		$c->add (BankDetailsPeer::PUBLISHER_ID , $this->getId());
		return BankDetailsPeer::doSelect($c);
	}
	
	
	public function contactsCount()
	{
		$c = new Criteria();
		$c->add(PublisherContactLinkPeer::PUBLISHER_ID, $this->getId());
		return PublisherContactLinkPeer::doCount($c);
	}
	
	public function officesCount()
	{
		$c = new Criteria();
		$c->add(PublisherOfficeLinkPeer::PUBLISHER_ID, $this->getId());
		return PublisherOfficeLinkPeer::doCount($c);
	}
	
	
	public function hasContacts()
	{
		$c = new criteria();
		$c->add(PublisherContactLinkPeer::PUBLISHER_ID, $this->getId());
		return PublisherContactLinkPeer::doCount($c)> 0 ? true : false;
	}
	
	
	
	static public function hasRealLinuxAccount($ftpUsername)
	{		
		$userArray = posix_getpwnam($ftpUsername);
		
		if(!is_array($userArray)) {
			return false;
		} else {
			return true;
		}
	}
	
	public static function hasFtpDbRecord($ftpUsername)
	{
		$c = new Criteria();
		$c->add(PublisherPeer::FTP_USERNAME, $ftpUsername);
		$publisher = PublisherPeer::doSelectOne($c);
		
		return is_object($publisher) ? true : false;
	}
	
	
	public function getContacts()
	{
		$c = new Criteria();
		$c->add(PublisherContactLinkPeer::PUBLISHER_ID, $this->getId());
		$contactLinks = PublisherContactLinkPeer::doSelect($c);
		
		$contacts = array(); 
		foreach ($contactLinks as $cl) {
			$contacts[] = $cl->getContact();
		}
		
		
		return $contacts;
	}
	
	
	public function getOffices()
	{
		$c = new Criteria();
		$c->add(PublisherOfficeLinkPeer::PUBLISHER_ID, $this->getId());
		$officeLinks = PublisherOfficeLinkPeer::doSelect($c);
		
		$offices = array();
		foreach ($officeLinks as $ol) {
			$offices[] = $ol->getOffice();
		}
		
		return $offices;
	}
	
	
	
}
