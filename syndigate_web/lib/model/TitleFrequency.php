<?php

/**
 * Subclass for representing a row from the 'title_frequency' table.
 *
 * 
 *
 * @package lib.model
 */ 
class TitleFrequency extends BaseTitleFrequency
{
	public function __toString()
	{
		return $this->getFrequencyType();
	}
}
