<?php

/**
 * Subclass for performing query and update operations on the 'category_mapping' table.
 *
 * 
 *
 * @package lib.model
 */ 
class CategoryMappingPeer extends BaseCategoryMappingPeer
{
}
