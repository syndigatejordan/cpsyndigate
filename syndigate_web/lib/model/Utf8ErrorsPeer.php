<?php

/**
 * Subclass for performing query and update operations on the 'utf8_errors' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Utf8ErrorsPeer extends BaseUtf8ErrorsPeer
{
}
