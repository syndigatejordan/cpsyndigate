<?php

/**
 * Subclass for representing a row from the 'report_gather' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ReportGather extends BaseReportGather
{
	public function getDateTime()
	{
		$timestamp = $this->getTime();
		return date('Y-m-d:H:m', $timestamp);
	}
	
	
	public function getPublisher()
	{
		return PublisherPeer::retrieveByPK($this->getTitle()->getPublisherId());
	}
	
	public function getLogsLink() {
		return link_to('view logs', 'logGather?report_id=' . $this->getId() );
	}
		
}
