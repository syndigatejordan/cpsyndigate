<?php

/**
 * Subclass for performing query and update operations on the 'generation_queue' table.
 *
 * 
 *
 * @package lib.model
 */ 
class GenerationQueuePeer extends BaseGenerationQueuePeer
{
}
