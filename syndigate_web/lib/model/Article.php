<?php

/**
 * Subclass for representing a row from the 'article' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Article extends BaseArticle
{
	public function __toString() 
	{
		return $this->getHeadline() . ' By ' . $this->getAuthor();
	}
	
	
	public function getIptcLangField()
	{	
		return  $this->getIptc()->getIptcLang($this->getLanguage()->getName());
	}
	

		
	
	/**
	 * Delete the article, and related records [ArticleOriginalData, Images]
	 * And Delete Images saved on the harddisk 
	 *
	 * @param Integer $articleId
	 * @return Boolean true for success , otherwise false
	 */
	public static function deleteCascade($articleId)
	{		
		$article = ArticlePeer::retrieveByPK($articleId);
		if(!$article) {
			return false;
		}
				
		$articleOriginalData = $article->getArticleOriginalData();
		
		self::deleteArticleVideos($articleId);
		self::deleteARticleImages($articleId);
				
		$article->delete();
		$articleOriginalData->delete();
		
		return true;
	}
	
	
	public static function deleteArticleVideos($articleId)
	{
		$c = new Criteria();
		$c->add(VideoPeer::ARTICLE_ID, $articleId);
		$videos = VideoPeer::doSelect($c);
				
		foreach ($videos as $video) {
			try {
				Video::deleteCascade($video->getId());
			} catch (Exception $e) {
				throw new Exception($e->getMessage());
			}
		}
	}
	
	public static function deleteARticleImages($articleId)
	{
		$c = new Criteria();
		$c->add(ImagePeer::ARTICLE_ID, $articleId);
		$images = ImagePeer::doSelect($c);
				
		foreach ($images as $image) {
			try {
				Image::deleteCascade($image->getId());
			} catch (Exception $e) {
				throw new Exception($e->getMessage());
			}
		}
	}
		
}