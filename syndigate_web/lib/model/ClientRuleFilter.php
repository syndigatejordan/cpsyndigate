<?php

/**
 * Subclass for representing a row from the 'client_rule_filter' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ClientRuleFilter extends BaseClientRuleFilter
{
	
	public function getSelectedValues()
	{
		return explode(',', $this->getFilterValue());
	}
	
	public function setFilterValues($values)
	{
		$this->setFilterValue(implode(",", $values));
	}
	
	
	public function getSelectedObjects()
	{
		$className;
		$objects = array();
		
		foreach ($this->getSelectedValues() as $id) {

			switch (strtoupper($this->getFilterType())) {
				case 'LANG':
					$objects[] = LanguagePeer::retrieveByPK($id);
					break;
				case 'TITLE':
					$objects[] = TitlePeer::retrieveByPK($id);
					break;
				case 'COUNTRY':
					$objects[] = CountryPeer::retrieveByPK($id);
					break;
				case 'CATEGORY' :
				//default:
					throw new Exception('Untill now not supported');
			} // end switch
		} // end foreach
		
		return $objects;
		
	} //end function getSelectedObjects
	
}
