<?php

/**
 * Subclass for representing a row from the 'office' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Office extends BaseOffice
{
	public function __toString()
	{
		return $this->getCountryId() . ' - ' . $this->getCityName() . ' - ' . $this->getPobox();
	}
	
}
