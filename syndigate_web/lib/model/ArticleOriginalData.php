<?php

/**
 * Subclass for representing a row from the 'article_original_data' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ArticleOriginalData extends BaseArticleOriginalData
{
	public function __toString()
	{
		return $this->getId();
	}
	
	
	
	
	
	
}
