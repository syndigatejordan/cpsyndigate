<?php

/**
 * Subclass for representing a row from the 'client_rule' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ClientRule extends BaseClientRule
{
	public static function deleteRuleCascade($clientruleId) 
	{
		$rule 		 = ClientRulePeer::retrieveByPK($clientruleId);
		$ruleFilters = $rule->getClientRuleFilters();
		
		foreach ($ruleFilters as $filter) {
			$filter->delete();
		}
		
		$rule->delete();
	}
	
	/*
	public function getFilters()
	{
		$c = new Criteria();
		$c->add(ClientRuleFilterPeer::CLIENT_RULE_ID, $this->getId());
		$filters = ClientRuleFilterPeer::doSelect($c);
		return $filters;
	}
	*/
	
}
