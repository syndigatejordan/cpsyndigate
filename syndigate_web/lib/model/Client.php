<?php

/**
 * Subclass for representing a row from the 'client' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Client extends BaseClient
{

	public function toString() {
		return $this->getClientName();
	}
	
	public static function retrieveByPK($id) 
	{
		return ClientPeer::retrieveByPK($id);
	}
	
	public function getContacts()
	{
		$c = new Criteria();
		$c->add(ClientContactLinkPeer::CLIENT_ID , $this->getId());
		$contactLinks = ClientContactLinkPeer::doSelect($c);
		
		$contacts = array();
		foreach ($contactLinks as $cl) {
			$contacts[] = $cl->getContact();
		}
		
		return $contacts;
	}
	
	
	//////////////////////////////////////////////////////////
	
	/**
	 * Delete Section
	 */
	
	public static function deleteClient($id) {		
		
		$client = ClientPeer::retrieveByPK($id);
		
		if(!$client) {
			throw new Exception('Incorrect client id given to delete a client');
		} else {
			
			$ftpUSername = $client->getFtpUsername();
			$homePath    = $client->getHomeDir();
			$nagiosPath  = $client->getNagiosFilePath();
			$exe         = LINUX_USER_MOD . " " . LINUX_SCRIPTS_PATH . "syndDeleteClient.sh $id $ftpUSername $homePath $nagiosPath";
			$result      = shell_exec($exe);
			
			$client->deleteContacts();
			$client->deleteRules();
			//$client->deleteReportSend();
			$client->DeleteClientConnections();
			
			$client->delRecordFromMax();
			$client->delClientSent();
			
			$c = new Criteria();
			$c->add(TitlesClientsRulesPeer::CLIENT_ID, $client->getId());
			TitlesClientsRulesPeer::dodelete($c);
					
			$client->delete();
			return true;
		}
	}
	
	
	public function deleteContacts()
	{
		$c = new Criteria();
		$c->add(ClientContactLinkPeer::CLIENT_ID, $this->getId());
		$contactLinks = ClientContactLinkPeer::doSelect($c);
		
		foreach ($contactLinks as $link) {
			$contactId = $link->getContactId();
			$link->delete();
			ContactPeer::retrieveByPK($contactId)->delete();
		}
	}
	
	public function deleteRules()
	{
		$c = new Criteria();
		$c->add(ClientRulePeer::CLIENT_ID, $this->getId());
		$rules = ClientRulePeer::doSelect($c);
		
		foreach ($rules as $rule) {
			ClientRule::deleteRuleCascade($rule->getId());
		}
	}
	
	public function deleteReportSend()
	{
		$c = new Criteria();
		$c->add(ReportSendPeer::CLIENT_ID, $this->getId());
		ReportSendPeer::doDelete($c);
	}
	
	
	//--------------------------------------------\\
	public function getClientConnections()
	{
		$c = new Criteria();
		$c->add(ClientConnectionPeer::CLIENT_ID, $this->id);
		return ClientConnectionPeer::doselect($c);
	}
	
	public function DeleteClientConnections()
	{
		$connections = $this->getClientConnections();
		if(is_array($connections)) {
			foreach ($connections as $conn) {
				$conn->delClientConnectionSent();
				$conn->delete();
			}
		}
		return true;
	}
	
	
	
	public function delRecordFromMax() {
		
		$c = new Criteria();
		$c->add(MaxPeer::CLIENT_ID, $this->getId());
		return MaxPeer::doDelete($c);
	}
	
	
	public function delClientSent() {
		
		$c = new Criteria();
		$c->add(ClientSentPeer::CLIENT_ID, $this->getId());
		ClientSentPeer::doDelete($c);
	}
	
}
