<?php

/**
 * Subclass for performing query and update operations on the 'article_original_data' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ArticleOriginalDataPeer extends BaseArticleOriginalDataPeer
{
}
