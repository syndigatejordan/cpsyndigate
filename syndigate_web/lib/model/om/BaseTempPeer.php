<?php


abstract class BaseTempPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'temp';

	
	const CLASS_DEFAULT = 'lib.model.Temp';

	
	const NUM_COLUMNS = 15;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'temp.ID';

	
	const IPTC_ID = 'temp.IPTC_ID';

	
	const TITLE_ID = 'temp.TITLE_ID';

	
	const ARTICLE_ORIGINAL_DATA_ID = 'temp.ARTICLE_ORIGINAL_DATA_ID';

	
	const LANGUAGE_ID = 'temp.LANGUAGE_ID';

	
	const HEADLINE = 'temp.HEADLINE';

	
	const SUMMARY = 'temp.SUMMARY';

	
	const BODY = 'temp.BODY';

	
	const AUTHOR = 'temp.AUTHOR';

	
	const DATE = 'temp.DATE';

	
	const PARSED_AT = 'temp.PARSED_AT';

	
	const UPDATED_AT = 'temp.UPDATED_AT';

	
	const HAS_TIME = 'temp.HAS_TIME';

	
	const IS_CALIAS_CALLED = 'temp.IS_CALIAS_CALLED';

	
	const SUB_FEED = 'temp.SUB_FEED';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'IptcId', 'TitleId', 'ArticleOriginalDataId', 'LanguageId', 'Headline', 'Summary', 'Body', 'Author', 'Date', 'ParsedAt', 'UpdatedAt', 'HasTime', 'IsCaliasCalled', 'SubFeed', ),
		BasePeer::TYPE_COLNAME => array (TempPeer::ID, TempPeer::IPTC_ID, TempPeer::TITLE_ID, TempPeer::ARTICLE_ORIGINAL_DATA_ID, TempPeer::LANGUAGE_ID, TempPeer::HEADLINE, TempPeer::SUMMARY, TempPeer::BODY, TempPeer::AUTHOR, TempPeer::DATE, TempPeer::PARSED_AT, TempPeer::UPDATED_AT, TempPeer::HAS_TIME, TempPeer::IS_CALIAS_CALLED, TempPeer::SUB_FEED, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'iptc_id', 'title_id', 'article_original_data_id', 'language_id', 'headline', 'summary', 'body', 'author', 'date', 'parsed_at', 'updated_at', 'has_time', 'is_Calias_called', 'sub_feed', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'IptcId' => 1, 'TitleId' => 2, 'ArticleOriginalDataId' => 3, 'LanguageId' => 4, 'Headline' => 5, 'Summary' => 6, 'Body' => 7, 'Author' => 8, 'Date' => 9, 'ParsedAt' => 10, 'UpdatedAt' => 11, 'HasTime' => 12, 'IsCaliasCalled' => 13, 'SubFeed' => 14, ),
		BasePeer::TYPE_COLNAME => array (TempPeer::ID => 0, TempPeer::IPTC_ID => 1, TempPeer::TITLE_ID => 2, TempPeer::ARTICLE_ORIGINAL_DATA_ID => 3, TempPeer::LANGUAGE_ID => 4, TempPeer::HEADLINE => 5, TempPeer::SUMMARY => 6, TempPeer::BODY => 7, TempPeer::AUTHOR => 8, TempPeer::DATE => 9, TempPeer::PARSED_AT => 10, TempPeer::UPDATED_AT => 11, TempPeer::HAS_TIME => 12, TempPeer::IS_CALIAS_CALLED => 13, TempPeer::SUB_FEED => 14, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'iptc_id' => 1, 'title_id' => 2, 'article_original_data_id' => 3, 'language_id' => 4, 'headline' => 5, 'summary' => 6, 'body' => 7, 'author' => 8, 'date' => 9, 'parsed_at' => 10, 'updated_at' => 11, 'has_time' => 12, 'is_Calias_called' => 13, 'sub_feed' => 14, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/TempMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.TempMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = TempPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(TempPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(TempPeer::ID);

		$criteria->addSelectColumn(TempPeer::IPTC_ID);

		$criteria->addSelectColumn(TempPeer::TITLE_ID);

		$criteria->addSelectColumn(TempPeer::ARTICLE_ORIGINAL_DATA_ID);

		$criteria->addSelectColumn(TempPeer::LANGUAGE_ID);

		$criteria->addSelectColumn(TempPeer::HEADLINE);

		$criteria->addSelectColumn(TempPeer::SUMMARY);

		$criteria->addSelectColumn(TempPeer::BODY);

		$criteria->addSelectColumn(TempPeer::AUTHOR);

		$criteria->addSelectColumn(TempPeer::DATE);

		$criteria->addSelectColumn(TempPeer::PARSED_AT);

		$criteria->addSelectColumn(TempPeer::UPDATED_AT);

		$criteria->addSelectColumn(TempPeer::HAS_TIME);

		$criteria->addSelectColumn(TempPeer::IS_CALIAS_CALLED);

		$criteria->addSelectColumn(TempPeer::SUB_FEED);

	}

	const COUNT = 'COUNT(*)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT *)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TempPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TempPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = TempPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = TempPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return TempPeer::populateObjects(TempPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			TempPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = TempPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return TempPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(TempPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(TempPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Temp) {

			$criteria = $values->buildCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

			}

		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Temp $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(TempPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(TempPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(TempPeer::DATABASE_NAME, TempPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = TempPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

} 
if (Propel::isInit()) {
			try {
		BaseTempPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/TempMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.TempMapBuilder');
}
