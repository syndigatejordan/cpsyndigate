<?php


abstract class BaseTendersInfoDuplications extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $file_name = '';


	
	protected $post_id = 0;


	
	protected $status = '';


	
	protected $original_id = 0;


	
	protected $parse_date = -62169984000;


	
	protected $article_id = 0;


	
	protected $article_date = -62169984000;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getFileName()
	{

		return $this->file_name;
	}

	
	public function getPostId()
	{

		return $this->post_id;
	}

	
	public function getStatus()
	{

		return $this->status;
	}

	
	public function getOriginalId()
	{

		return $this->original_id;
	}

	
	public function getParseDate($format = 'Y-m-d')
	{

		if ($this->parse_date === null || $this->parse_date === '') {
			return null;
		} elseif (!is_int($this->parse_date)) {
						$ts = strtotime($this->parse_date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [parse_date] as date/time value: " . var_export($this->parse_date, true));
			}
		} else {
			$ts = $this->parse_date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getArticleId()
	{

		return $this->article_id;
	}

	
	public function getArticleDate($format = 'Y-m-d')
	{

		if ($this->article_date === null || $this->article_date === '') {
			return null;
		} elseif (!is_int($this->article_date)) {
						$ts = strtotime($this->article_date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [article_date] as date/time value: " . var_export($this->article_date, true));
			}
		} else {
			$ts = $this->article_date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function setFileName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->file_name !== $v || $v === '') {
			$this->file_name = $v;
			$this->modifiedColumns[] = TendersInfoDuplicationsPeer::FILE_NAME;
		}

	} 
	
	public function setPostId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->post_id !== $v || $v === 0) {
			$this->post_id = $v;
			$this->modifiedColumns[] = TendersInfoDuplicationsPeer::POST_ID;
		}

	} 
	
	public function setStatus($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->status !== $v || $v === '') {
			$this->status = $v;
			$this->modifiedColumns[] = TendersInfoDuplicationsPeer::STATUS;
		}

	} 
	
	public function setOriginalId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->original_id !== $v || $v === 0) {
			$this->original_id = $v;
			$this->modifiedColumns[] = TendersInfoDuplicationsPeer::ORIGINAL_ID;
		}

	} 
	
	public function setParseDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [parse_date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->parse_date !== $ts || $ts === -62169984000) {
			$this->parse_date = $ts;
			$this->modifiedColumns[] = TendersInfoDuplicationsPeer::PARSE_DATE;
		}

	} 
	
	public function setArticleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->article_id !== $v || $v === 0) {
			$this->article_id = $v;
			$this->modifiedColumns[] = TendersInfoDuplicationsPeer::ARTICLE_ID;
		}

	} 
	
	public function setArticleDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [article_date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->article_date !== $ts || $ts === -62169984000) {
			$this->article_date = $ts;
			$this->modifiedColumns[] = TendersInfoDuplicationsPeer::ARTICLE_DATE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->file_name = $rs->getString($startcol + 0);

			$this->post_id = $rs->getInt($startcol + 1);

			$this->status = $rs->getString($startcol + 2);

			$this->original_id = $rs->getInt($startcol + 3);

			$this->parse_date = $rs->getDate($startcol + 4, null);

			$this->article_id = $rs->getInt($startcol + 5);

			$this->article_date = $rs->getDate($startcol + 6, null);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 7; 
		} catch (Exception $e) {
			throw new PropelException("Error populating TendersInfoDuplications object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TendersInfoDuplicationsPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			TendersInfoDuplicationsPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TendersInfoDuplicationsPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = TendersInfoDuplicationsPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += TendersInfoDuplicationsPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = TendersInfoDuplicationsPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TendersInfoDuplicationsPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getFileName();
				break;
			case 1:
				return $this->getPostId();
				break;
			case 2:
				return $this->getStatus();
				break;
			case 3:
				return $this->getOriginalId();
				break;
			case 4:
				return $this->getParseDate();
				break;
			case 5:
				return $this->getArticleId();
				break;
			case 6:
				return $this->getArticleDate();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TendersInfoDuplicationsPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getFileName(),
			$keys[1] => $this->getPostId(),
			$keys[2] => $this->getStatus(),
			$keys[3] => $this->getOriginalId(),
			$keys[4] => $this->getParseDate(),
			$keys[5] => $this->getArticleId(),
			$keys[6] => $this->getArticleDate(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TendersInfoDuplicationsPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setFileName($value);
				break;
			case 1:
				$this->setPostId($value);
				break;
			case 2:
				$this->setStatus($value);
				break;
			case 3:
				$this->setOriginalId($value);
				break;
			case 4:
				$this->setParseDate($value);
				break;
			case 5:
				$this->setArticleId($value);
				break;
			case 6:
				$this->setArticleDate($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TendersInfoDuplicationsPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setFileName($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setPostId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setStatus($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setOriginalId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setParseDate($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setArticleId($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setArticleDate($arr[$keys[6]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(TendersInfoDuplicationsPeer::DATABASE_NAME);

		if ($this->isColumnModified(TendersInfoDuplicationsPeer::FILE_NAME)) $criteria->add(TendersInfoDuplicationsPeer::FILE_NAME, $this->file_name);
		if ($this->isColumnModified(TendersInfoDuplicationsPeer::POST_ID)) $criteria->add(TendersInfoDuplicationsPeer::POST_ID, $this->post_id);
		if ($this->isColumnModified(TendersInfoDuplicationsPeer::STATUS)) $criteria->add(TendersInfoDuplicationsPeer::STATUS, $this->status);
		if ($this->isColumnModified(TendersInfoDuplicationsPeer::ORIGINAL_ID)) $criteria->add(TendersInfoDuplicationsPeer::ORIGINAL_ID, $this->original_id);
		if ($this->isColumnModified(TendersInfoDuplicationsPeer::PARSE_DATE)) $criteria->add(TendersInfoDuplicationsPeer::PARSE_DATE, $this->parse_date);
		if ($this->isColumnModified(TendersInfoDuplicationsPeer::ARTICLE_ID)) $criteria->add(TendersInfoDuplicationsPeer::ARTICLE_ID, $this->article_id);
		if ($this->isColumnModified(TendersInfoDuplicationsPeer::ARTICLE_DATE)) $criteria->add(TendersInfoDuplicationsPeer::ARTICLE_DATE, $this->article_date);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(TendersInfoDuplicationsPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setFileName($this->file_name);

		$copyObj->setPostId($this->post_id);

		$copyObj->setStatus($this->status);

		$copyObj->setOriginalId($this->original_id);

		$copyObj->setParseDate($this->parse_date);

		$copyObj->setArticleId($this->article_id);

		$copyObj->setArticleDate($this->article_date);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new TendersInfoDuplicationsPeer();
		}
		return self::$peer;
	}

} 