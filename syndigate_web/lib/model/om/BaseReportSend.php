<?php


abstract class BaseReportSend extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $sender_status;


	
	protected $sender_msg;


	
	protected $start_time;


	
	protected $end_time;


	
	protected $files_count;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getSenderStatus()
	{

		return $this->sender_status;
	}

	
	public function getSenderMsg()
	{

		return $this->sender_msg;
	}

	
	public function getStartTime()
	{

		return $this->start_time;
	}

	
	public function getEndTime()
	{

		return $this->end_time;
	}

	
	public function getFilesCount()
	{

		return $this->files_count;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ReportSendPeer::ID;
		}

	} 
	
	public function setSenderStatus($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->sender_status !== $v) {
			$this->sender_status = $v;
			$this->modifiedColumns[] = ReportSendPeer::SENDER_STATUS;
		}

	} 
	
	public function setSenderMsg($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->sender_msg !== $v) {
			$this->sender_msg = $v;
			$this->modifiedColumns[] = ReportSendPeer::SENDER_MSG;
		}

	} 
	
	public function setStartTime($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->start_time !== $v) {
			$this->start_time = $v;
			$this->modifiedColumns[] = ReportSendPeer::START_TIME;
		}

	} 
	
	public function setEndTime($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->end_time !== $v) {
			$this->end_time = $v;
			$this->modifiedColumns[] = ReportSendPeer::END_TIME;
		}

	} 
	
	public function setFilesCount($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->files_count !== $v) {
			$this->files_count = $v;
			$this->modifiedColumns[] = ReportSendPeer::FILES_COUNT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->sender_status = $rs->getString($startcol + 1);

			$this->sender_msg = $rs->getString($startcol + 2);

			$this->start_time = $rs->getInt($startcol + 3);

			$this->end_time = $rs->getInt($startcol + 4);

			$this->files_count = $rs->getInt($startcol + 5);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 6; 
		} catch (Exception $e) {
			throw new PropelException("Error populating ReportSend object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ReportSendPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ReportSendPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ReportSendPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ReportSendPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ReportSendPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = ReportSendPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ReportSendPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getSenderStatus();
				break;
			case 2:
				return $this->getSenderMsg();
				break;
			case 3:
				return $this->getStartTime();
				break;
			case 4:
				return $this->getEndTime();
				break;
			case 5:
				return $this->getFilesCount();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ReportSendPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getSenderStatus(),
			$keys[2] => $this->getSenderMsg(),
			$keys[3] => $this->getStartTime(),
			$keys[4] => $this->getEndTime(),
			$keys[5] => $this->getFilesCount(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ReportSendPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setSenderStatus($value);
				break;
			case 2:
				$this->setSenderMsg($value);
				break;
			case 3:
				$this->setStartTime($value);
				break;
			case 4:
				$this->setEndTime($value);
				break;
			case 5:
				$this->setFilesCount($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ReportSendPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setSenderStatus($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setSenderMsg($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setStartTime($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setEndTime($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setFilesCount($arr[$keys[5]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ReportSendPeer::DATABASE_NAME);

		if ($this->isColumnModified(ReportSendPeer::ID)) $criteria->add(ReportSendPeer::ID, $this->id);
		if ($this->isColumnModified(ReportSendPeer::SENDER_STATUS)) $criteria->add(ReportSendPeer::SENDER_STATUS, $this->sender_status);
		if ($this->isColumnModified(ReportSendPeer::SENDER_MSG)) $criteria->add(ReportSendPeer::SENDER_MSG, $this->sender_msg);
		if ($this->isColumnModified(ReportSendPeer::START_TIME)) $criteria->add(ReportSendPeer::START_TIME, $this->start_time);
		if ($this->isColumnModified(ReportSendPeer::END_TIME)) $criteria->add(ReportSendPeer::END_TIME, $this->end_time);
		if ($this->isColumnModified(ReportSendPeer::FILES_COUNT)) $criteria->add(ReportSendPeer::FILES_COUNT, $this->files_count);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ReportSendPeer::DATABASE_NAME);

		$criteria->add(ReportSendPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setSenderStatus($this->sender_status);

		$copyObj->setSenderMsg($this->sender_msg);

		$copyObj->setStartTime($this->start_time);

		$copyObj->setEndTime($this->end_time);

		$copyObj->setFilesCount($this->files_count);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ReportSendPeer();
		}
		return self::$peer;
	}

} 