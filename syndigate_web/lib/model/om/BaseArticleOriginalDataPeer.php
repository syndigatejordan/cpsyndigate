<?php


abstract class BaseArticleOriginalDataPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'article_original_data';

	
	const CLASS_DEFAULT = 'lib.model.ArticleOriginalData';

	
	const NUM_COLUMNS = 10;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'article_original_data.ID';

	
	const ORIGINAL_ARTICLE_ID = 'article_original_data.ORIGINAL_ARTICLE_ID';

	
	const ORIGINAL_SOURCE = 'article_original_data.ORIGINAL_SOURCE';

	
	const ISSUE_NUMBER = 'article_original_data.ISSUE_NUMBER';

	
	const PAGE_NUMBER = 'article_original_data.PAGE_NUMBER';

	
	const REFERENCE = 'article_original_data.REFERENCE';

	
	const EXTRAS = 'article_original_data.EXTRAS';

	
	const HIJRI_DATE = 'article_original_data.HIJRI_DATE';

	
	const ORIGINAL_CAT_ID = 'article_original_data.ORIGINAL_CAT_ID';

	
	const REVISION_NUM = 'article_original_data.REVISION_NUM';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'OriginalArticleId', 'OriginalSource', 'IssueNumber', 'PageNumber', 'Reference', 'Extras', 'HijriDate', 'OriginalCatId', 'RevisionNum', ),
		BasePeer::TYPE_COLNAME => array (ArticleOriginalDataPeer::ID, ArticleOriginalDataPeer::ORIGINAL_ARTICLE_ID, ArticleOriginalDataPeer::ORIGINAL_SOURCE, ArticleOriginalDataPeer::ISSUE_NUMBER, ArticleOriginalDataPeer::PAGE_NUMBER, ArticleOriginalDataPeer::REFERENCE, ArticleOriginalDataPeer::EXTRAS, ArticleOriginalDataPeer::HIJRI_DATE, ArticleOriginalDataPeer::ORIGINAL_CAT_ID, ArticleOriginalDataPeer::REVISION_NUM, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'original_article_id', 'original_source', 'issue_number', 'page_number', 'reference', 'extras', 'hijri_date', 'original_cat_id', 'revision_num', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'OriginalArticleId' => 1, 'OriginalSource' => 2, 'IssueNumber' => 3, 'PageNumber' => 4, 'Reference' => 5, 'Extras' => 6, 'HijriDate' => 7, 'OriginalCatId' => 8, 'RevisionNum' => 9, ),
		BasePeer::TYPE_COLNAME => array (ArticleOriginalDataPeer::ID => 0, ArticleOriginalDataPeer::ORIGINAL_ARTICLE_ID => 1, ArticleOriginalDataPeer::ORIGINAL_SOURCE => 2, ArticleOriginalDataPeer::ISSUE_NUMBER => 3, ArticleOriginalDataPeer::PAGE_NUMBER => 4, ArticleOriginalDataPeer::REFERENCE => 5, ArticleOriginalDataPeer::EXTRAS => 6, ArticleOriginalDataPeer::HIJRI_DATE => 7, ArticleOriginalDataPeer::ORIGINAL_CAT_ID => 8, ArticleOriginalDataPeer::REVISION_NUM => 9, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'original_article_id' => 1, 'original_source' => 2, 'issue_number' => 3, 'page_number' => 4, 'reference' => 5, 'extras' => 6, 'hijri_date' => 7, 'original_cat_id' => 8, 'revision_num' => 9, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/ArticleOriginalDataMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.ArticleOriginalDataMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ArticleOriginalDataPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ArticleOriginalDataPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ArticleOriginalDataPeer::ID);

		$criteria->addSelectColumn(ArticleOriginalDataPeer::ORIGINAL_ARTICLE_ID);

		$criteria->addSelectColumn(ArticleOriginalDataPeer::ORIGINAL_SOURCE);

		$criteria->addSelectColumn(ArticleOriginalDataPeer::ISSUE_NUMBER);

		$criteria->addSelectColumn(ArticleOriginalDataPeer::PAGE_NUMBER);

		$criteria->addSelectColumn(ArticleOriginalDataPeer::REFERENCE);

		$criteria->addSelectColumn(ArticleOriginalDataPeer::EXTRAS);

		$criteria->addSelectColumn(ArticleOriginalDataPeer::HIJRI_DATE);

		$criteria->addSelectColumn(ArticleOriginalDataPeer::ORIGINAL_CAT_ID);

		$criteria->addSelectColumn(ArticleOriginalDataPeer::REVISION_NUM);

	}

	const COUNT = 'COUNT(article_original_data.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT article_original_data.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleOriginalDataPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleOriginalDataPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ArticleOriginalDataPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ArticleOriginalDataPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ArticleOriginalDataPeer::populateObjects(ArticleOriginalDataPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ArticleOriginalDataPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ArticleOriginalDataPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ArticleOriginalDataPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(ArticleOriginalDataPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(ArticleOriginalDataPeer::ID);
			$selectCriteria->add(ArticleOriginalDataPeer::ID, $criteria->remove(ArticleOriginalDataPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(ArticleOriginalDataPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ArticleOriginalDataPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof ArticleOriginalData) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(ArticleOriginalDataPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(ArticleOriginalData $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ArticleOriginalDataPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ArticleOriginalDataPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ArticleOriginalDataPeer::DATABASE_NAME, ArticleOriginalDataPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ArticleOriginalDataPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(ArticleOriginalDataPeer::DATABASE_NAME);

		$criteria->add(ArticleOriginalDataPeer::ID, $pk);


		$v = ArticleOriginalDataPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(ArticleOriginalDataPeer::ID, $pks, Criteria::IN);
			$objs = ArticleOriginalDataPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseArticleOriginalDataPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/ArticleOriginalDataMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.ArticleOriginalDataMapBuilder');
}
