<?php


abstract class BaseClientConnection extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $client_id;


	
	protected $connection_type;


	
	protected $url;


	
	protected $username;


	
	protected $password;


	
	protected $ftp_port = 21;


	
	protected $remote_dir;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getClientId()
	{

		return $this->client_id;
	}

	
	public function getConnectionType()
	{

		return $this->connection_type;
	}

	
	public function getUrl()
	{

		return $this->url;
	}

	
	public function getUsername()
	{

		return $this->username;
	}

	
	public function getPassword()
	{

		return $this->password;
	}

	
	public function getFtpPort()
	{

		return $this->ftp_port;
	}

	
	public function getRemoteDir()
	{

		return $this->remote_dir;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ClientConnectionPeer::ID;
		}

	} 
	
	public function setClientId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->client_id !== $v) {
			$this->client_id = $v;
			$this->modifiedColumns[] = ClientConnectionPeer::CLIENT_ID;
		}

	} 
	
	public function setConnectionType($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->connection_type !== $v) {
			$this->connection_type = $v;
			$this->modifiedColumns[] = ClientConnectionPeer::CONNECTION_TYPE;
		}

	} 
	
	public function setUrl($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->url !== $v) {
			$this->url = $v;
			$this->modifiedColumns[] = ClientConnectionPeer::URL;
		}

	} 
	
	public function setUsername($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->username !== $v) {
			$this->username = $v;
			$this->modifiedColumns[] = ClientConnectionPeer::USERNAME;
		}

	} 
	
	public function setPassword($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->password !== $v) {
			$this->password = $v;
			$this->modifiedColumns[] = ClientConnectionPeer::PASSWORD;
		}

	} 
	
	public function setFtpPort($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->ftp_port !== $v || $v === 21) {
			$this->ftp_port = $v;
			$this->modifiedColumns[] = ClientConnectionPeer::FTP_PORT;
		}

	} 
	
	public function setRemoteDir($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->remote_dir !== $v) {
			$this->remote_dir = $v;
			$this->modifiedColumns[] = ClientConnectionPeer::REMOTE_DIR;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->client_id = $rs->getInt($startcol + 1);

			$this->connection_type = $rs->getString($startcol + 2);

			$this->url = $rs->getString($startcol + 3);

			$this->username = $rs->getString($startcol + 4);

			$this->password = $rs->getString($startcol + 5);

			$this->ftp_port = $rs->getInt($startcol + 6);

			$this->remote_dir = $rs->getString($startcol + 7);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 8; 
		} catch (Exception $e) {
			throw new PropelException("Error populating ClientConnection object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ClientConnectionPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ClientConnectionPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ClientConnectionPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ClientConnectionPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ClientConnectionPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = ClientConnectionPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ClientConnectionPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getClientId();
				break;
			case 2:
				return $this->getConnectionType();
				break;
			case 3:
				return $this->getUrl();
				break;
			case 4:
				return $this->getUsername();
				break;
			case 5:
				return $this->getPassword();
				break;
			case 6:
				return $this->getFtpPort();
				break;
			case 7:
				return $this->getRemoteDir();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ClientConnectionPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getClientId(),
			$keys[2] => $this->getConnectionType(),
			$keys[3] => $this->getUrl(),
			$keys[4] => $this->getUsername(),
			$keys[5] => $this->getPassword(),
			$keys[6] => $this->getFtpPort(),
			$keys[7] => $this->getRemoteDir(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ClientConnectionPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setClientId($value);
				break;
			case 2:
				$this->setConnectionType($value);
				break;
			case 3:
				$this->setUrl($value);
				break;
			case 4:
				$this->setUsername($value);
				break;
			case 5:
				$this->setPassword($value);
				break;
			case 6:
				$this->setFtpPort($value);
				break;
			case 7:
				$this->setRemoteDir($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ClientConnectionPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setClientId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setConnectionType($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setUrl($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setUsername($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setPassword($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setFtpPort($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setRemoteDir($arr[$keys[7]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ClientConnectionPeer::DATABASE_NAME);

		if ($this->isColumnModified(ClientConnectionPeer::ID)) $criteria->add(ClientConnectionPeer::ID, $this->id);
		if ($this->isColumnModified(ClientConnectionPeer::CLIENT_ID)) $criteria->add(ClientConnectionPeer::CLIENT_ID, $this->client_id);
		if ($this->isColumnModified(ClientConnectionPeer::CONNECTION_TYPE)) $criteria->add(ClientConnectionPeer::CONNECTION_TYPE, $this->connection_type);
		if ($this->isColumnModified(ClientConnectionPeer::URL)) $criteria->add(ClientConnectionPeer::URL, $this->url);
		if ($this->isColumnModified(ClientConnectionPeer::USERNAME)) $criteria->add(ClientConnectionPeer::USERNAME, $this->username);
		if ($this->isColumnModified(ClientConnectionPeer::PASSWORD)) $criteria->add(ClientConnectionPeer::PASSWORD, $this->password);
		if ($this->isColumnModified(ClientConnectionPeer::FTP_PORT)) $criteria->add(ClientConnectionPeer::FTP_PORT, $this->ftp_port);
		if ($this->isColumnModified(ClientConnectionPeer::REMOTE_DIR)) $criteria->add(ClientConnectionPeer::REMOTE_DIR, $this->remote_dir);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ClientConnectionPeer::DATABASE_NAME);

		$criteria->add(ClientConnectionPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setClientId($this->client_id);

		$copyObj->setConnectionType($this->connection_type);

		$copyObj->setUrl($this->url);

		$copyObj->setUsername($this->username);

		$copyObj->setPassword($this->password);

		$copyObj->setFtpPort($this->ftp_port);

		$copyObj->setRemoteDir($this->remote_dir);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ClientConnectionPeer();
		}
		return self::$peer;
	}

} 