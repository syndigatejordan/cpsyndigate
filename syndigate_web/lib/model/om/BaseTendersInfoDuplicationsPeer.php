<?php


abstract class BaseTendersInfoDuplicationsPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'tenders_info_duplications';

	
	const CLASS_DEFAULT = 'lib.model.TendersInfoDuplications';

	
	const NUM_COLUMNS = 7;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const FILE_NAME = 'tenders_info_duplications.FILE_NAME';

	
	const POST_ID = 'tenders_info_duplications.POST_ID';

	
	const STATUS = 'tenders_info_duplications.STATUS';

	
	const ORIGINAL_ID = 'tenders_info_duplications.ORIGINAL_ID';

	
	const PARSE_DATE = 'tenders_info_duplications.PARSE_DATE';

	
	const ARTICLE_ID = 'tenders_info_duplications.ARTICLE_ID';

	
	const ARTICLE_DATE = 'tenders_info_duplications.ARTICLE_DATE';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('FileName', 'PostId', 'Status', 'OriginalId', 'ParseDate', 'ArticleId', 'ArticleDate', ),
		BasePeer::TYPE_COLNAME => array (TendersInfoDuplicationsPeer::FILE_NAME, TendersInfoDuplicationsPeer::POST_ID, TendersInfoDuplicationsPeer::STATUS, TendersInfoDuplicationsPeer::ORIGINAL_ID, TendersInfoDuplicationsPeer::PARSE_DATE, TendersInfoDuplicationsPeer::ARTICLE_ID, TendersInfoDuplicationsPeer::ARTICLE_DATE, ),
		BasePeer::TYPE_FIELDNAME => array ('file_name', 'post_id', 'status', 'original_id', 'parse_date', 'article_id', 'article_date', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('FileName' => 0, 'PostId' => 1, 'Status' => 2, 'OriginalId' => 3, 'ParseDate' => 4, 'ArticleId' => 5, 'ArticleDate' => 6, ),
		BasePeer::TYPE_COLNAME => array (TendersInfoDuplicationsPeer::FILE_NAME => 0, TendersInfoDuplicationsPeer::POST_ID => 1, TendersInfoDuplicationsPeer::STATUS => 2, TendersInfoDuplicationsPeer::ORIGINAL_ID => 3, TendersInfoDuplicationsPeer::PARSE_DATE => 4, TendersInfoDuplicationsPeer::ARTICLE_ID => 5, TendersInfoDuplicationsPeer::ARTICLE_DATE => 6, ),
		BasePeer::TYPE_FIELDNAME => array ('file_name' => 0, 'post_id' => 1, 'status' => 2, 'original_id' => 3, 'parse_date' => 4, 'article_id' => 5, 'article_date' => 6, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/TendersInfoDuplicationsMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.TendersInfoDuplicationsMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = TendersInfoDuplicationsPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(TendersInfoDuplicationsPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(TendersInfoDuplicationsPeer::FILE_NAME);

		$criteria->addSelectColumn(TendersInfoDuplicationsPeer::POST_ID);

		$criteria->addSelectColumn(TendersInfoDuplicationsPeer::STATUS);

		$criteria->addSelectColumn(TendersInfoDuplicationsPeer::ORIGINAL_ID);

		$criteria->addSelectColumn(TendersInfoDuplicationsPeer::PARSE_DATE);

		$criteria->addSelectColumn(TendersInfoDuplicationsPeer::ARTICLE_ID);

		$criteria->addSelectColumn(TendersInfoDuplicationsPeer::ARTICLE_DATE);

	}

	const COUNT = 'COUNT(*)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT *)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TendersInfoDuplicationsPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TendersInfoDuplicationsPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = TendersInfoDuplicationsPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = TendersInfoDuplicationsPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return TendersInfoDuplicationsPeer::populateObjects(TendersInfoDuplicationsPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			TendersInfoDuplicationsPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = TendersInfoDuplicationsPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return TendersInfoDuplicationsPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(TendersInfoDuplicationsPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(TendersInfoDuplicationsPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof TendersInfoDuplications) {

			$criteria = $values->buildCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

			}

		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(TendersInfoDuplications $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(TendersInfoDuplicationsPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(TendersInfoDuplicationsPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(TendersInfoDuplicationsPeer::DATABASE_NAME, TendersInfoDuplicationsPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = TendersInfoDuplicationsPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

} 
if (Propel::isInit()) {
			try {
		BaseTendersInfoDuplicationsPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/TendersInfoDuplicationsMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.TendersInfoDuplicationsMapBuilder');
}
