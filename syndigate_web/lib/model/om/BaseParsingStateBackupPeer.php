<?php


abstract class BaseParsingStateBackupPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'parsing_state_backup';

	
	const CLASS_DEFAULT = 'lib.model.ParsingStateBackup';

	
	const NUM_COLUMNS = 9;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'parsing_state_backup.ID';

	
	const TITLE_ID = 'parsing_state_backup.TITLE_ID';

	
	const REVISION_NUM = 'parsing_state_backup.REVISION_NUM';

	
	const COMPLETED = 'parsing_state_backup.COMPLETED';

	
	const LOCKED = 'parsing_state_backup.LOCKED';

	
	const CREATED_AT = 'parsing_state_backup.CREATED_AT';

	
	const PARSER_STARTED_AT = 'parsing_state_backup.PARSER_STARTED_AT';

	
	const PARSER_FINISHED_AT = 'parsing_state_backup.PARSER_FINISHED_AT';

	
	const PID = 'parsing_state_backup.PID';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'TitleId', 'RevisionNum', 'Completed', 'Locked', 'CreatedAt', 'ParserStartedAt', 'ParserFinishedAt', 'Pid', ),
		BasePeer::TYPE_COLNAME => array (ParsingStateBackupPeer::ID, ParsingStateBackupPeer::TITLE_ID, ParsingStateBackupPeer::REVISION_NUM, ParsingStateBackupPeer::COMPLETED, ParsingStateBackupPeer::LOCKED, ParsingStateBackupPeer::CREATED_AT, ParsingStateBackupPeer::PARSER_STARTED_AT, ParsingStateBackupPeer::PARSER_FINISHED_AT, ParsingStateBackupPeer::PID, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'title_id', 'revision_num', 'completed', 'locked', 'created_at', 'parser_started_at', 'parser_finished_at', 'pid', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'TitleId' => 1, 'RevisionNum' => 2, 'Completed' => 3, 'Locked' => 4, 'CreatedAt' => 5, 'ParserStartedAt' => 6, 'ParserFinishedAt' => 7, 'Pid' => 8, ),
		BasePeer::TYPE_COLNAME => array (ParsingStateBackupPeer::ID => 0, ParsingStateBackupPeer::TITLE_ID => 1, ParsingStateBackupPeer::REVISION_NUM => 2, ParsingStateBackupPeer::COMPLETED => 3, ParsingStateBackupPeer::LOCKED => 4, ParsingStateBackupPeer::CREATED_AT => 5, ParsingStateBackupPeer::PARSER_STARTED_AT => 6, ParsingStateBackupPeer::PARSER_FINISHED_AT => 7, ParsingStateBackupPeer::PID => 8, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'title_id' => 1, 'revision_num' => 2, 'completed' => 3, 'locked' => 4, 'created_at' => 5, 'parser_started_at' => 6, 'parser_finished_at' => 7, 'pid' => 8, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/ParsingStateBackupMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.ParsingStateBackupMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ParsingStateBackupPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ParsingStateBackupPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ParsingStateBackupPeer::ID);

		$criteria->addSelectColumn(ParsingStateBackupPeer::TITLE_ID);

		$criteria->addSelectColumn(ParsingStateBackupPeer::REVISION_NUM);

		$criteria->addSelectColumn(ParsingStateBackupPeer::COMPLETED);

		$criteria->addSelectColumn(ParsingStateBackupPeer::LOCKED);

		$criteria->addSelectColumn(ParsingStateBackupPeer::CREATED_AT);

		$criteria->addSelectColumn(ParsingStateBackupPeer::PARSER_STARTED_AT);

		$criteria->addSelectColumn(ParsingStateBackupPeer::PARSER_FINISHED_AT);

		$criteria->addSelectColumn(ParsingStateBackupPeer::PID);

	}

	const COUNT = 'COUNT(*)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT *)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ParsingStateBackupPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ParsingStateBackupPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ParsingStateBackupPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ParsingStateBackupPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ParsingStateBackupPeer::populateObjects(ParsingStateBackupPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ParsingStateBackupPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ParsingStateBackupPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ParsingStateBackupPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(ParsingStateBackupPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ParsingStateBackupPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof ParsingStateBackup) {

			$criteria = $values->buildCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

			}

		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(ParsingStateBackup $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ParsingStateBackupPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ParsingStateBackupPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ParsingStateBackupPeer::DATABASE_NAME, ParsingStateBackupPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ParsingStateBackupPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

} 
if (Propel::isInit()) {
			try {
		BaseParsingStateBackupPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/ParsingStateBackupMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.ParsingStateBackupMapBuilder');
}
