<?php


abstract class BaseGatherCrontabPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'gather_crontab';

	
	const CLASS_DEFAULT = 'lib.model.GatherCrontab';

	
	const NUM_COLUMNS = 9;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'gather_crontab.ID';

	
	const TITLE = 'gather_crontab.TITLE';

	
	const DESCRIPTION = 'gather_crontab.DESCRIPTION';

	
	const SCRIPT = 'gather_crontab.SCRIPT';

	
	const CONCURRENT = 'gather_crontab.CONCURRENT';

	
	const IMPLEMENTATION_ID = 'gather_crontab.IMPLEMENTATION_ID';

	
	const CRON_DEFINITION = 'gather_crontab.CRON_DEFINITION';

	
	const LAST_ACTUAL_TIMESTAMP = 'gather_crontab.LAST_ACTUAL_TIMESTAMP';

	
	const RUN_ONCE = 'gather_crontab.RUN_ONCE';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'Title', 'Description', 'Script', 'Concurrent', 'ImplementationId', 'CronDefinition', 'LastActualTimestamp', 'RunOnce', ),
		BasePeer::TYPE_COLNAME => array (GatherCrontabPeer::ID, GatherCrontabPeer::TITLE, GatherCrontabPeer::DESCRIPTION, GatherCrontabPeer::SCRIPT, GatherCrontabPeer::CONCURRENT, GatherCrontabPeer::IMPLEMENTATION_ID, GatherCrontabPeer::CRON_DEFINITION, GatherCrontabPeer::LAST_ACTUAL_TIMESTAMP, GatherCrontabPeer::RUN_ONCE, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'title', 'description', 'script', 'concurrent', 'implementation_id', 'cron_definition', 'last_actual_timestamp', 'run_once', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Title' => 1, 'Description' => 2, 'Script' => 3, 'Concurrent' => 4, 'ImplementationId' => 5, 'CronDefinition' => 6, 'LastActualTimestamp' => 7, 'RunOnce' => 8, ),
		BasePeer::TYPE_COLNAME => array (GatherCrontabPeer::ID => 0, GatherCrontabPeer::TITLE => 1, GatherCrontabPeer::DESCRIPTION => 2, GatherCrontabPeer::SCRIPT => 3, GatherCrontabPeer::CONCURRENT => 4, GatherCrontabPeer::IMPLEMENTATION_ID => 5, GatherCrontabPeer::CRON_DEFINITION => 6, GatherCrontabPeer::LAST_ACTUAL_TIMESTAMP => 7, GatherCrontabPeer::RUN_ONCE => 8, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'title' => 1, 'description' => 2, 'script' => 3, 'concurrent' => 4, 'implementation_id' => 5, 'cron_definition' => 6, 'last_actual_timestamp' => 7, 'run_once' => 8, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/GatherCrontabMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.GatherCrontabMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = GatherCrontabPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(GatherCrontabPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(GatherCrontabPeer::ID);

		$criteria->addSelectColumn(GatherCrontabPeer::TITLE);

		$criteria->addSelectColumn(GatherCrontabPeer::DESCRIPTION);

		$criteria->addSelectColumn(GatherCrontabPeer::SCRIPT);

		$criteria->addSelectColumn(GatherCrontabPeer::CONCURRENT);

		$criteria->addSelectColumn(GatherCrontabPeer::IMPLEMENTATION_ID);

		$criteria->addSelectColumn(GatherCrontabPeer::CRON_DEFINITION);

		$criteria->addSelectColumn(GatherCrontabPeer::LAST_ACTUAL_TIMESTAMP);

		$criteria->addSelectColumn(GatherCrontabPeer::RUN_ONCE);

	}

	const COUNT = 'COUNT(gather_crontab.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT gather_crontab.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(GatherCrontabPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(GatherCrontabPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = GatherCrontabPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = GatherCrontabPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return GatherCrontabPeer::populateObjects(GatherCrontabPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			GatherCrontabPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = GatherCrontabPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return GatherCrontabPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(GatherCrontabPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(GatherCrontabPeer::ID);
			$selectCriteria->add(GatherCrontabPeer::ID, $criteria->remove(GatherCrontabPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(GatherCrontabPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(GatherCrontabPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof GatherCrontab) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(GatherCrontabPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(GatherCrontab $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(GatherCrontabPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(GatherCrontabPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(GatherCrontabPeer::DATABASE_NAME, GatherCrontabPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = GatherCrontabPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(GatherCrontabPeer::DATABASE_NAME);

		$criteria->add(GatherCrontabPeer::ID, $pk);


		$v = GatherCrontabPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(GatherCrontabPeer::ID, $pks, Criteria::IN);
			$objs = GatherCrontabPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseGatherCrontabPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/GatherCrontabMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.GatherCrontabMapBuilder');
}
