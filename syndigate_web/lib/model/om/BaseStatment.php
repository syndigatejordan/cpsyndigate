<?php


abstract class BaseStatment extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $path = '';


	
	protected $status = 'completed';


	
	protected $notes;


	
	protected $update_at = -62169984000;


	
	protected $file_name = '';


	
	protected $publisher_id = 0;


	
	protected $created = 0;


	
	protected $accountant_id = 0;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getPath()
	{

		return $this->path;
	}

	
	public function getStatus()
	{

		return $this->status;
	}

	
	public function getNotes()
	{

		return $this->notes;
	}

	
	public function getUpdateAt($format = 'Y-m-d H:i:s')
	{

		if ($this->update_at === null || $this->update_at === '') {
			return null;
		} elseif (!is_int($this->update_at)) {
						$ts = strtotime($this->update_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [update_at] as date/time value: " . var_export($this->update_at, true));
			}
		} else {
			$ts = $this->update_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getFileName()
	{

		return $this->file_name;
	}

	
	public function getPublisherId()
	{

		return $this->publisher_id;
	}

	
	public function getCreated()
	{

		return $this->created;
	}

	
	public function getAccountantId()
	{

		return $this->accountant_id;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = StatmentPeer::ID;
		}

	} 
	
	public function setPath($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->path !== $v || $v === '') {
			$this->path = $v;
			$this->modifiedColumns[] = StatmentPeer::PATH;
		}

	} 
	
	public function setStatus($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->status !== $v || $v === 'completed') {
			$this->status = $v;
			$this->modifiedColumns[] = StatmentPeer::STATUS;
		}

	} 
	
	public function setNotes($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->notes !== $v) {
			$this->notes = $v;
			$this->modifiedColumns[] = StatmentPeer::NOTES;
		}

	} 
	
	public function setUpdateAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [update_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->update_at !== $ts || $ts === -62169984000) {
			$this->update_at = $ts;
			$this->modifiedColumns[] = StatmentPeer::UPDATE_AT;
		}

	} 
	
	public function setFileName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->file_name !== $v || $v === '') {
			$this->file_name = $v;
			$this->modifiedColumns[] = StatmentPeer::FILE_NAME;
		}

	} 
	
	public function setPublisherId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->publisher_id !== $v || $v === 0) {
			$this->publisher_id = $v;
			$this->modifiedColumns[] = StatmentPeer::PUBLISHER_ID;
		}

	} 
	
	public function setCreated($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->created !== $v || $v === 0) {
			$this->created = $v;
			$this->modifiedColumns[] = StatmentPeer::CREATED;
		}

	} 
	
	public function setAccountantId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->accountant_id !== $v || $v === 0) {
			$this->accountant_id = $v;
			$this->modifiedColumns[] = StatmentPeer::ACCOUNTANT_ID;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->path = $rs->getString($startcol + 1);

			$this->status = $rs->getString($startcol + 2);

			$this->notes = $rs->getString($startcol + 3);

			$this->update_at = $rs->getTimestamp($startcol + 4, null);

			$this->file_name = $rs->getString($startcol + 5);

			$this->publisher_id = $rs->getInt($startcol + 6);

			$this->created = $rs->getInt($startcol + 7);

			$this->accountant_id = $rs->getInt($startcol + 8);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 9; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Statment object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(StatmentPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			StatmentPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(StatmentPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = StatmentPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += StatmentPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = StatmentPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = StatmentPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getPath();
				break;
			case 2:
				return $this->getStatus();
				break;
			case 3:
				return $this->getNotes();
				break;
			case 4:
				return $this->getUpdateAt();
				break;
			case 5:
				return $this->getFileName();
				break;
			case 6:
				return $this->getPublisherId();
				break;
			case 7:
				return $this->getCreated();
				break;
			case 8:
				return $this->getAccountantId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = StatmentPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getPath(),
			$keys[2] => $this->getStatus(),
			$keys[3] => $this->getNotes(),
			$keys[4] => $this->getUpdateAt(),
			$keys[5] => $this->getFileName(),
			$keys[6] => $this->getPublisherId(),
			$keys[7] => $this->getCreated(),
			$keys[8] => $this->getAccountantId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = StatmentPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setPath($value);
				break;
			case 2:
				$this->setStatus($value);
				break;
			case 3:
				$this->setNotes($value);
				break;
			case 4:
				$this->setUpdateAt($value);
				break;
			case 5:
				$this->setFileName($value);
				break;
			case 6:
				$this->setPublisherId($value);
				break;
			case 7:
				$this->setCreated($value);
				break;
			case 8:
				$this->setAccountantId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = StatmentPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setPath($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setStatus($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setNotes($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setUpdateAt($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setFileName($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setPublisherId($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setCreated($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setAccountantId($arr[$keys[8]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(StatmentPeer::DATABASE_NAME);

		if ($this->isColumnModified(StatmentPeer::ID)) $criteria->add(StatmentPeer::ID, $this->id);
		if ($this->isColumnModified(StatmentPeer::PATH)) $criteria->add(StatmentPeer::PATH, $this->path);
		if ($this->isColumnModified(StatmentPeer::STATUS)) $criteria->add(StatmentPeer::STATUS, $this->status);
		if ($this->isColumnModified(StatmentPeer::NOTES)) $criteria->add(StatmentPeer::NOTES, $this->notes);
		if ($this->isColumnModified(StatmentPeer::UPDATE_AT)) $criteria->add(StatmentPeer::UPDATE_AT, $this->update_at);
		if ($this->isColumnModified(StatmentPeer::FILE_NAME)) $criteria->add(StatmentPeer::FILE_NAME, $this->file_name);
		if ($this->isColumnModified(StatmentPeer::PUBLISHER_ID)) $criteria->add(StatmentPeer::PUBLISHER_ID, $this->publisher_id);
		if ($this->isColumnModified(StatmentPeer::CREATED)) $criteria->add(StatmentPeer::CREATED, $this->created);
		if ($this->isColumnModified(StatmentPeer::ACCOUNTANT_ID)) $criteria->add(StatmentPeer::ACCOUNTANT_ID, $this->accountant_id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(StatmentPeer::DATABASE_NAME);

		$criteria->add(StatmentPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setPath($this->path);

		$copyObj->setStatus($this->status);

		$copyObj->setNotes($this->notes);

		$copyObj->setUpdateAt($this->update_at);

		$copyObj->setFileName($this->file_name);

		$copyObj->setPublisherId($this->publisher_id);

		$copyObj->setCreated($this->created);

		$copyObj->setAccountantId($this->accountant_id);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new StatmentPeer();
		}
		return self::$peer;
	}

} 