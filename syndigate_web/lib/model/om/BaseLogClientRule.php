<?php


abstract class BaseLogClientRule extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $timestamp = 0;


	
	protected $admin_id = 0;


	
	protected $client_id = 0;


	
	protected $previous_status;


	
	protected $operation;


	
	protected $current_status;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getTimestamp()
	{

		return $this->timestamp;
	}

	
	public function getAdminId()
	{

		return $this->admin_id;
	}

	
	public function getClientId()
	{

		return $this->client_id;
	}

	
	public function getPreviousStatus()
	{

		return $this->previous_status;
	}

	
	public function getOperation()
	{

		return $this->operation;
	}

	
	public function getCurrentStatus()
	{

		return $this->current_status;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = LogClientRulePeer::ID;
		}

	} 
	
	public function setTimestamp($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->timestamp !== $v || $v === 0) {
			$this->timestamp = $v;
			$this->modifiedColumns[] = LogClientRulePeer::TIMESTAMP;
		}

	} 
	
	public function setAdminId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->admin_id !== $v || $v === 0) {
			$this->admin_id = $v;
			$this->modifiedColumns[] = LogClientRulePeer::ADMIN_ID;
		}

	} 
	
	public function setClientId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->client_id !== $v || $v === 0) {
			$this->client_id = $v;
			$this->modifiedColumns[] = LogClientRulePeer::CLIENT_ID;
		}

	} 
	
	public function setPreviousStatus($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->previous_status !== $v) {
			$this->previous_status = $v;
			$this->modifiedColumns[] = LogClientRulePeer::PREVIOUS_STATUS;
		}

	} 
	
	public function setOperation($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->operation !== $v) {
			$this->operation = $v;
			$this->modifiedColumns[] = LogClientRulePeer::OPERATION;
		}

	} 
	
	public function setCurrentStatus($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->current_status !== $v) {
			$this->current_status = $v;
			$this->modifiedColumns[] = LogClientRulePeer::CURRENT_STATUS;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->timestamp = $rs->getInt($startcol + 1);

			$this->admin_id = $rs->getInt($startcol + 2);

			$this->client_id = $rs->getInt($startcol + 3);

			$this->previous_status = $rs->getString($startcol + 4);

			$this->operation = $rs->getString($startcol + 5);

			$this->current_status = $rs->getString($startcol + 6);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 7; 
		} catch (Exception $e) {
			throw new PropelException("Error populating LogClientRule object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LogClientRulePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			LogClientRulePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LogClientRulePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = LogClientRulePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += LogClientRulePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = LogClientRulePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LogClientRulePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTimestamp();
				break;
			case 2:
				return $this->getAdminId();
				break;
			case 3:
				return $this->getClientId();
				break;
			case 4:
				return $this->getPreviousStatus();
				break;
			case 5:
				return $this->getOperation();
				break;
			case 6:
				return $this->getCurrentStatus();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = LogClientRulePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTimestamp(),
			$keys[2] => $this->getAdminId(),
			$keys[3] => $this->getClientId(),
			$keys[4] => $this->getPreviousStatus(),
			$keys[5] => $this->getOperation(),
			$keys[6] => $this->getCurrentStatus(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LogClientRulePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTimestamp($value);
				break;
			case 2:
				$this->setAdminId($value);
				break;
			case 3:
				$this->setClientId($value);
				break;
			case 4:
				$this->setPreviousStatus($value);
				break;
			case 5:
				$this->setOperation($value);
				break;
			case 6:
				$this->setCurrentStatus($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = LogClientRulePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTimestamp($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setAdminId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setClientId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setPreviousStatus($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setOperation($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setCurrentStatus($arr[$keys[6]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(LogClientRulePeer::DATABASE_NAME);

		if ($this->isColumnModified(LogClientRulePeer::ID)) $criteria->add(LogClientRulePeer::ID, $this->id);
		if ($this->isColumnModified(LogClientRulePeer::TIMESTAMP)) $criteria->add(LogClientRulePeer::TIMESTAMP, $this->timestamp);
		if ($this->isColumnModified(LogClientRulePeer::ADMIN_ID)) $criteria->add(LogClientRulePeer::ADMIN_ID, $this->admin_id);
		if ($this->isColumnModified(LogClientRulePeer::CLIENT_ID)) $criteria->add(LogClientRulePeer::CLIENT_ID, $this->client_id);
		if ($this->isColumnModified(LogClientRulePeer::PREVIOUS_STATUS)) $criteria->add(LogClientRulePeer::PREVIOUS_STATUS, $this->previous_status);
		if ($this->isColumnModified(LogClientRulePeer::OPERATION)) $criteria->add(LogClientRulePeer::OPERATION, $this->operation);
		if ($this->isColumnModified(LogClientRulePeer::CURRENT_STATUS)) $criteria->add(LogClientRulePeer::CURRENT_STATUS, $this->current_status);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(LogClientRulePeer::DATABASE_NAME);

		$criteria->add(LogClientRulePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTimestamp($this->timestamp);

		$copyObj->setAdminId($this->admin_id);

		$copyObj->setClientId($this->client_id);

		$copyObj->setPreviousStatus($this->previous_status);

		$copyObj->setOperation($this->operation);

		$copyObj->setCurrentStatus($this->current_status);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new LogClientRulePeer();
		}
		return self::$peer;
	}

} 