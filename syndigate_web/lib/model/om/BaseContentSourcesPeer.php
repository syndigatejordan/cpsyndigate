<?php


abstract class BaseContentSourcesPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'content_sources';

	
	const CLASS_DEFAULT = 'lib.model.ContentSources';

	
	const NUM_COLUMNS = 25;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const COUNTRY = 'content_sources.COUNTRY';

	
	const CITY = 'content_sources.CITY';

	
	const TYPE = 'content_sources.TYPE';

	
	const TITLE = 'content_sources.TITLE';

	
	const URL = 'content_sources.URL';

	
	const LANGUAGE = 'content_sources.LANGUAGE';

	
	const FREQUANCY = 'content_sources.FREQUANCY';

	
	const TIMELINESS = 'content_sources.TIMELINESS';

	
	const PUBLISHER = 'content_sources.PUBLISHER';

	
	const COPYRIGHT_NOTICE = 'content_sources.COPYRIGHT_NOTICE';

	
	const SOURCE_DESCRIPTION = 'content_sources.SOURCE_DESCRIPTION';

	
	const TERRITORY = 'content_sources.TERRITORY';

	
	const MODE = 'content_sources.MODE';

	
	const PUBLISHER_TYPE = 'content_sources.PUBLISHER_TYPE';

	
	const CHANNELS = 'content_sources.CHANNELS';

	
	const GENRE = 'content_sources.GENRE';

	
	const BIZ_CONTACT = 'content_sources.BIZ_CONTACT';

	
	const BIZ_EMAIL = 'content_sources.BIZ_EMAIL';

	
	const BIZ_PHONE = 'content_sources.BIZ_PHONE';

	
	const TECH_CONTACT = 'content_sources.TECH_CONTACT';

	
	const TECH_EMAIL = 'content_sources.TECH_EMAIL';

	
	const ECH_PHONE = 'content_sources.ECH_PHONE';

	
	const IS_ORG_FROM_SYNDIGATE = 'content_sources.IS_ORG_FROM_SYNDIGATE';

	
	const TITLE_ID = 'content_sources.TITLE_ID';

	
	const ID = 'content_sources.ID';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Country', 'City', 'Type', 'Title', 'Url', 'Language', 'Frequancy', 'Timeliness', 'Publisher', 'CopyrightNotice', 'SourceDescription', 'Territory', 'Mode', 'PublisherType', 'Channels', 'Genre', 'BizContact', 'BizEmail', 'BizPhone', 'TechContact', 'TechEmail', 'EchPhone', 'IsOrgFromSyndigate', 'TitleId', 'Id', ),
		BasePeer::TYPE_COLNAME => array (ContentSourcesPeer::COUNTRY, ContentSourcesPeer::CITY, ContentSourcesPeer::TYPE, ContentSourcesPeer::TITLE, ContentSourcesPeer::URL, ContentSourcesPeer::LANGUAGE, ContentSourcesPeer::FREQUANCY, ContentSourcesPeer::TIMELINESS, ContentSourcesPeer::PUBLISHER, ContentSourcesPeer::COPYRIGHT_NOTICE, ContentSourcesPeer::SOURCE_DESCRIPTION, ContentSourcesPeer::TERRITORY, ContentSourcesPeer::MODE, ContentSourcesPeer::PUBLISHER_TYPE, ContentSourcesPeer::CHANNELS, ContentSourcesPeer::GENRE, ContentSourcesPeer::BIZ_CONTACT, ContentSourcesPeer::BIZ_EMAIL, ContentSourcesPeer::BIZ_PHONE, ContentSourcesPeer::TECH_CONTACT, ContentSourcesPeer::TECH_EMAIL, ContentSourcesPeer::ECH_PHONE, ContentSourcesPeer::IS_ORG_FROM_SYNDIGATE, ContentSourcesPeer::TITLE_ID, ContentSourcesPeer::ID, ),
		BasePeer::TYPE_FIELDNAME => array ('country', 'city', 'type', 'title', 'url', 'language', 'frequancy', 'timeliness', 'publisher', 'copyright_notice', 'source_description', 'territory', 'mode', 'publisher_type', 'channels', 'Genre', 'biz_contact', 'biz_email', 'biz_phone', 'tech_contact', 'tech_email', 'ech_phone', 'is_org_from_syndigate', 'title_id', 'id', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Country' => 0, 'City' => 1, 'Type' => 2, 'Title' => 3, 'Url' => 4, 'Language' => 5, 'Frequancy' => 6, 'Timeliness' => 7, 'Publisher' => 8, 'CopyrightNotice' => 9, 'SourceDescription' => 10, 'Territory' => 11, 'Mode' => 12, 'PublisherType' => 13, 'Channels' => 14, 'Genre' => 15, 'BizContact' => 16, 'BizEmail' => 17, 'BizPhone' => 18, 'TechContact' => 19, 'TechEmail' => 20, 'EchPhone' => 21, 'IsOrgFromSyndigate' => 22, 'TitleId' => 23, 'Id' => 24, ),
		BasePeer::TYPE_COLNAME => array (ContentSourcesPeer::COUNTRY => 0, ContentSourcesPeer::CITY => 1, ContentSourcesPeer::TYPE => 2, ContentSourcesPeer::TITLE => 3, ContentSourcesPeer::URL => 4, ContentSourcesPeer::LANGUAGE => 5, ContentSourcesPeer::FREQUANCY => 6, ContentSourcesPeer::TIMELINESS => 7, ContentSourcesPeer::PUBLISHER => 8, ContentSourcesPeer::COPYRIGHT_NOTICE => 9, ContentSourcesPeer::SOURCE_DESCRIPTION => 10, ContentSourcesPeer::TERRITORY => 11, ContentSourcesPeer::MODE => 12, ContentSourcesPeer::PUBLISHER_TYPE => 13, ContentSourcesPeer::CHANNELS => 14, ContentSourcesPeer::GENRE => 15, ContentSourcesPeer::BIZ_CONTACT => 16, ContentSourcesPeer::BIZ_EMAIL => 17, ContentSourcesPeer::BIZ_PHONE => 18, ContentSourcesPeer::TECH_CONTACT => 19, ContentSourcesPeer::TECH_EMAIL => 20, ContentSourcesPeer::ECH_PHONE => 21, ContentSourcesPeer::IS_ORG_FROM_SYNDIGATE => 22, ContentSourcesPeer::TITLE_ID => 23, ContentSourcesPeer::ID => 24, ),
		BasePeer::TYPE_FIELDNAME => array ('country' => 0, 'city' => 1, 'type' => 2, 'title' => 3, 'url' => 4, 'language' => 5, 'frequancy' => 6, 'timeliness' => 7, 'publisher' => 8, 'copyright_notice' => 9, 'source_description' => 10, 'territory' => 11, 'mode' => 12, 'publisher_type' => 13, 'channels' => 14, 'Genre' => 15, 'biz_contact' => 16, 'biz_email' => 17, 'biz_phone' => 18, 'tech_contact' => 19, 'tech_email' => 20, 'ech_phone' => 21, 'is_org_from_syndigate' => 22, 'title_id' => 23, 'id' => 24, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/ContentSourcesMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.ContentSourcesMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ContentSourcesPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ContentSourcesPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ContentSourcesPeer::COUNTRY);

		$criteria->addSelectColumn(ContentSourcesPeer::CITY);

		$criteria->addSelectColumn(ContentSourcesPeer::TYPE);

		$criteria->addSelectColumn(ContentSourcesPeer::TITLE);

		$criteria->addSelectColumn(ContentSourcesPeer::URL);

		$criteria->addSelectColumn(ContentSourcesPeer::LANGUAGE);

		$criteria->addSelectColumn(ContentSourcesPeer::FREQUANCY);

		$criteria->addSelectColumn(ContentSourcesPeer::TIMELINESS);

		$criteria->addSelectColumn(ContentSourcesPeer::PUBLISHER);

		$criteria->addSelectColumn(ContentSourcesPeer::COPYRIGHT_NOTICE);

		$criteria->addSelectColumn(ContentSourcesPeer::SOURCE_DESCRIPTION);

		$criteria->addSelectColumn(ContentSourcesPeer::TERRITORY);

		$criteria->addSelectColumn(ContentSourcesPeer::MODE);

		$criteria->addSelectColumn(ContentSourcesPeer::PUBLISHER_TYPE);

		$criteria->addSelectColumn(ContentSourcesPeer::CHANNELS);

		$criteria->addSelectColumn(ContentSourcesPeer::GENRE);

		$criteria->addSelectColumn(ContentSourcesPeer::BIZ_CONTACT);

		$criteria->addSelectColumn(ContentSourcesPeer::BIZ_EMAIL);

		$criteria->addSelectColumn(ContentSourcesPeer::BIZ_PHONE);

		$criteria->addSelectColumn(ContentSourcesPeer::TECH_CONTACT);

		$criteria->addSelectColumn(ContentSourcesPeer::TECH_EMAIL);

		$criteria->addSelectColumn(ContentSourcesPeer::ECH_PHONE);

		$criteria->addSelectColumn(ContentSourcesPeer::IS_ORG_FROM_SYNDIGATE);

		$criteria->addSelectColumn(ContentSourcesPeer::TITLE_ID);

		$criteria->addSelectColumn(ContentSourcesPeer::ID);

	}

	const COUNT = 'COUNT(content_sources.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT content_sources.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ContentSourcesPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ContentSourcesPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ContentSourcesPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ContentSourcesPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ContentSourcesPeer::populateObjects(ContentSourcesPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ContentSourcesPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ContentSourcesPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ContentSourcesPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(ContentSourcesPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(ContentSourcesPeer::ID);
			$selectCriteria->add(ContentSourcesPeer::ID, $criteria->remove(ContentSourcesPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(ContentSourcesPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ContentSourcesPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof ContentSources) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(ContentSourcesPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(ContentSources $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ContentSourcesPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ContentSourcesPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ContentSourcesPeer::DATABASE_NAME, ContentSourcesPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ContentSourcesPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(ContentSourcesPeer::DATABASE_NAME);

		$criteria->add(ContentSourcesPeer::ID, $pk);


		$v = ContentSourcesPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(ContentSourcesPeer::ID, $pks, Criteria::IN);
			$objs = ContentSourcesPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseContentSourcesPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/ContentSourcesMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.ContentSourcesMapBuilder');
}
