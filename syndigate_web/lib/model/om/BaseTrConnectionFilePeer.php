<?php


abstract class BaseTrConnectionFilePeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'tr_connection_file';

	
	const CLASS_DEFAULT = 'lib.model.TrConnectionFile';

	
	const NUM_COLUMNS = 11;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'tr_connection_file.ID';

	
	const CLIENT_ID = 'tr_connection_file.CLIENT_ID';

	
	const TR_FILE_ID = 'tr_connection_file.TR_FILE_ID';

	
	const CLIENT_CONNECTION_ID = 'tr_connection_file.CLIENT_CONNECTION_ID';

	
	const TITLE_ID = 'tr_connection_file.TITLE_ID';

	
	const LOCAL_FILE = 'tr_connection_file.LOCAL_FILE';

	
	const IS_SENT = 'tr_connection_file.IS_SENT';

	
	const NUM_RETRY = 'tr_connection_file.NUM_RETRY';

	
	const ERR_MSG = 'tr_connection_file.ERR_MSG';

	
	const CREATION_TIME = 'tr_connection_file.CREATION_TIME';

	
	const SENT_TIME = 'tr_connection_file.SENT_TIME';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'ClientId', 'TrFileId', 'ClientConnectionId', 'TitleId', 'LocalFile', 'IsSent', 'NumRetry', 'ErrMsg', 'CreationTime', 'SentTime', ),
		BasePeer::TYPE_COLNAME => array (TrConnectionFilePeer::ID, TrConnectionFilePeer::CLIENT_ID, TrConnectionFilePeer::TR_FILE_ID, TrConnectionFilePeer::CLIENT_CONNECTION_ID, TrConnectionFilePeer::TITLE_ID, TrConnectionFilePeer::LOCAL_FILE, TrConnectionFilePeer::IS_SENT, TrConnectionFilePeer::NUM_RETRY, TrConnectionFilePeer::ERR_MSG, TrConnectionFilePeer::CREATION_TIME, TrConnectionFilePeer::SENT_TIME, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'client_id', 'tr_file_id', 'client_connection_id', 'title_id', 'local_file', 'is_sent', 'num_retry', 'err_msg', 'creation_time', 'sent_time', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'ClientId' => 1, 'TrFileId' => 2, 'ClientConnectionId' => 3, 'TitleId' => 4, 'LocalFile' => 5, 'IsSent' => 6, 'NumRetry' => 7, 'ErrMsg' => 8, 'CreationTime' => 9, 'SentTime' => 10, ),
		BasePeer::TYPE_COLNAME => array (TrConnectionFilePeer::ID => 0, TrConnectionFilePeer::CLIENT_ID => 1, TrConnectionFilePeer::TR_FILE_ID => 2, TrConnectionFilePeer::CLIENT_CONNECTION_ID => 3, TrConnectionFilePeer::TITLE_ID => 4, TrConnectionFilePeer::LOCAL_FILE => 5, TrConnectionFilePeer::IS_SENT => 6, TrConnectionFilePeer::NUM_RETRY => 7, TrConnectionFilePeer::ERR_MSG => 8, TrConnectionFilePeer::CREATION_TIME => 9, TrConnectionFilePeer::SENT_TIME => 10, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'client_id' => 1, 'tr_file_id' => 2, 'client_connection_id' => 3, 'title_id' => 4, 'local_file' => 5, 'is_sent' => 6, 'num_retry' => 7, 'err_msg' => 8, 'creation_time' => 9, 'sent_time' => 10, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/TrConnectionFileMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.TrConnectionFileMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = TrConnectionFilePeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(TrConnectionFilePeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(TrConnectionFilePeer::ID);

		$criteria->addSelectColumn(TrConnectionFilePeer::CLIENT_ID);

		$criteria->addSelectColumn(TrConnectionFilePeer::TR_FILE_ID);

		$criteria->addSelectColumn(TrConnectionFilePeer::CLIENT_CONNECTION_ID);

		$criteria->addSelectColumn(TrConnectionFilePeer::TITLE_ID);

		$criteria->addSelectColumn(TrConnectionFilePeer::LOCAL_FILE);

		$criteria->addSelectColumn(TrConnectionFilePeer::IS_SENT);

		$criteria->addSelectColumn(TrConnectionFilePeer::NUM_RETRY);

		$criteria->addSelectColumn(TrConnectionFilePeer::ERR_MSG);

		$criteria->addSelectColumn(TrConnectionFilePeer::CREATION_TIME);

		$criteria->addSelectColumn(TrConnectionFilePeer::SENT_TIME);

	}

	const COUNT = 'COUNT(tr_connection_file.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT tr_connection_file.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TrConnectionFilePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TrConnectionFilePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = TrConnectionFilePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = TrConnectionFilePeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return TrConnectionFilePeer::populateObjects(TrConnectionFilePeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			TrConnectionFilePeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = TrConnectionFilePeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return TrConnectionFilePeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(TrConnectionFilePeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(TrConnectionFilePeer::ID);
			$selectCriteria->add(TrConnectionFilePeer::ID, $criteria->remove(TrConnectionFilePeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(TrConnectionFilePeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(TrConnectionFilePeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof TrConnectionFile) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(TrConnectionFilePeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(TrConnectionFile $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(TrConnectionFilePeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(TrConnectionFilePeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(TrConnectionFilePeer::DATABASE_NAME, TrConnectionFilePeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = TrConnectionFilePeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(TrConnectionFilePeer::DATABASE_NAME);

		$criteria->add(TrConnectionFilePeer::ID, $pk);


		$v = TrConnectionFilePeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(TrConnectionFilePeer::ID, $pks, Criteria::IN);
			$objs = TrConnectionFilePeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseTrConnectionFilePeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/TrConnectionFileMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.TrConnectionFileMapBuilder');
}
