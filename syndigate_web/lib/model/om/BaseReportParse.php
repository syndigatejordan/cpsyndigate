<?php


abstract class BaseReportParse extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $articles_ids;


	
	protected $title_id;


	
	protected $date;


	
	protected $parser_status;


	
	protected $parser_error;


	
	protected $article_num;

	
	protected $aTitle;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getArticlesIds()
	{

		return $this->articles_ids;
	}

	
	public function getTitleId()
	{

		return $this->title_id;
	}

	
	public function getDate($format = 'Y-m-d')
	{

		if ($this->date === null || $this->date === '') {
			return null;
		} elseif (!is_int($this->date)) {
						$ts = strtotime($this->date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [date] as date/time value: " . var_export($this->date, true));
			}
		} else {
			$ts = $this->date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getParserStatus()
	{

		return $this->parser_status;
	}

	
	public function getParserError()
	{

		return $this->parser_error;
	}

	
	public function getArticleNum()
	{

		return $this->article_num;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ReportParsePeer::ID;
		}

	} 
	
	public function setArticlesIds($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->articles_ids !== $v) {
			$this->articles_ids = $v;
			$this->modifiedColumns[] = ReportParsePeer::ARTICLES_IDS;
		}

	} 
	
	public function setTitleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_id !== $v) {
			$this->title_id = $v;
			$this->modifiedColumns[] = ReportParsePeer::TITLE_ID;
		}

		if ($this->aTitle !== null && $this->aTitle->getId() !== $v) {
			$this->aTitle = null;
		}

	} 
	
	public function setDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->date !== $ts) {
			$this->date = $ts;
			$this->modifiedColumns[] = ReportParsePeer::DATE;
		}

	} 
	
	public function setParserStatus($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->parser_status !== $v) {
			$this->parser_status = $v;
			$this->modifiedColumns[] = ReportParsePeer::PARSER_STATUS;
		}

	} 
	
	public function setParserError($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->parser_error !== $v) {
			$this->parser_error = $v;
			$this->modifiedColumns[] = ReportParsePeer::PARSER_ERROR;
		}

	} 
	
	public function setArticleNum($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->article_num !== $v) {
			$this->article_num = $v;
			$this->modifiedColumns[] = ReportParsePeer::ARTICLE_NUM;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->articles_ids = $rs->getString($startcol + 1);

			$this->title_id = $rs->getInt($startcol + 2);

			$this->date = $rs->getDate($startcol + 3, null);

			$this->parser_status = $rs->getString($startcol + 4);

			$this->parser_error = $rs->getString($startcol + 5);

			$this->article_num = $rs->getInt($startcol + 6);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 7; 
		} catch (Exception $e) {
			throw new PropelException("Error populating ReportParse object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ReportParsePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ReportParsePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ReportParsePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aTitle !== null) {
				if ($this->aTitle->isModified()) {
					$affectedRows += $this->aTitle->save($con);
				}
				$this->setTitle($this->aTitle);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ReportParsePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ReportParsePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aTitle !== null) {
				if (!$this->aTitle->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTitle->getValidationFailures());
				}
			}


			if (($retval = ReportParsePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ReportParsePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getArticlesIds();
				break;
			case 2:
				return $this->getTitleId();
				break;
			case 3:
				return $this->getDate();
				break;
			case 4:
				return $this->getParserStatus();
				break;
			case 5:
				return $this->getParserError();
				break;
			case 6:
				return $this->getArticleNum();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ReportParsePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getArticlesIds(),
			$keys[2] => $this->getTitleId(),
			$keys[3] => $this->getDate(),
			$keys[4] => $this->getParserStatus(),
			$keys[5] => $this->getParserError(),
			$keys[6] => $this->getArticleNum(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ReportParsePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setArticlesIds($value);
				break;
			case 2:
				$this->setTitleId($value);
				break;
			case 3:
				$this->setDate($value);
				break;
			case 4:
				$this->setParserStatus($value);
				break;
			case 5:
				$this->setParserError($value);
				break;
			case 6:
				$this->setArticleNum($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ReportParsePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setArticlesIds($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setTitleId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setDate($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setParserStatus($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setParserError($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setArticleNum($arr[$keys[6]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ReportParsePeer::DATABASE_NAME);

		if ($this->isColumnModified(ReportParsePeer::ID)) $criteria->add(ReportParsePeer::ID, $this->id);
		if ($this->isColumnModified(ReportParsePeer::ARTICLES_IDS)) $criteria->add(ReportParsePeer::ARTICLES_IDS, $this->articles_ids);
		if ($this->isColumnModified(ReportParsePeer::TITLE_ID)) $criteria->add(ReportParsePeer::TITLE_ID, $this->title_id);
		if ($this->isColumnModified(ReportParsePeer::DATE)) $criteria->add(ReportParsePeer::DATE, $this->date);
		if ($this->isColumnModified(ReportParsePeer::PARSER_STATUS)) $criteria->add(ReportParsePeer::PARSER_STATUS, $this->parser_status);
		if ($this->isColumnModified(ReportParsePeer::PARSER_ERROR)) $criteria->add(ReportParsePeer::PARSER_ERROR, $this->parser_error);
		if ($this->isColumnModified(ReportParsePeer::ARTICLE_NUM)) $criteria->add(ReportParsePeer::ARTICLE_NUM, $this->article_num);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ReportParsePeer::DATABASE_NAME);

		$criteria->add(ReportParsePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setArticlesIds($this->articles_ids);

		$copyObj->setTitleId($this->title_id);

		$copyObj->setDate($this->date);

		$copyObj->setParserStatus($this->parser_status);

		$copyObj->setParserError($this->parser_error);

		$copyObj->setArticleNum($this->article_num);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ReportParsePeer();
		}
		return self::$peer;
	}

	
	public function setTitle($v)
	{


		if ($v === null) {
			$this->setTitleId(NULL);
		} else {
			$this->setTitleId($v->getId());
		}


		$this->aTitle = $v;
	}


	
	public function getTitle($con = null)
	{
		if ($this->aTitle === null && ($this->title_id !== null)) {
						include_once 'lib/model/om/BaseTitlePeer.php';

			$this->aTitle = TitlePeer::retrieveByPK($this->title_id, $con);

			
		}
		return $this->aTitle;
	}

} 