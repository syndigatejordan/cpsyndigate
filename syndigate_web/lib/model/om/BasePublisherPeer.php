<?php


abstract class BasePublisherPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'publisher';

	
	const CLASS_DEFAULT = 'lib.model.Publisher';

	
	const NUM_COLUMNS = 18;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'publisher.ID';

	
	const NAME = 'publisher.NAME';

	
	const WEBSITE = 'publisher.WEBSITE';

	
	const NOTES = 'publisher.NOTES';

	
	const LOGO_PATH = 'publisher.LOGO_PATH';

	
	const FTP_USERNAME = 'publisher.FTP_USERNAME';

	
	const FTP_PASSWORD = 'publisher.FTP_PASSWORD';

	
	const UNIX_GID = 'publisher.UNIX_GID';

	
	const UNIX_UID = 'publisher.UNIX_UID';

	
	const NAGIOS_FILE_PATH = 'publisher.NAGIOS_FILE_PATH';

	
	const COUNTRY_ID = 'publisher.COUNTRY_ID';

	
	const CONTRACT = 'publisher.CONTRACT';

	
	const ROYALTY_RATE = 'publisher.ROYALTY_RATE';

	
	const CONTRACT_START_DATE = 'publisher.CONTRACT_START_DATE';

	
	const CONTRACT_END_DATE = 'publisher.CONTRACT_END_DATE';

	
	const RENEWAL_PERIOD = 'publisher.RENEWAL_PERIOD';

	
	const ADMINS_ID = 'publisher.ADMINS_ID';

	
	const ACCOUNTANT_NOTES = 'publisher.ACCOUNTANT_NOTES';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'Name', 'Website', 'Notes', 'LogoPath', 'FtpUsername', 'FtpPassword', 'UnixGid', 'UnixUid', 'NagiosFilePath', 'CountryId', 'Contract', 'RoyaltyRate', 'ContractStartDate', 'ContractEndDate', 'RenewalPeriod', 'AdminsId', 'AccountantNotes', ),
		BasePeer::TYPE_COLNAME => array (PublisherPeer::ID, PublisherPeer::NAME, PublisherPeer::WEBSITE, PublisherPeer::NOTES, PublisherPeer::LOGO_PATH, PublisherPeer::FTP_USERNAME, PublisherPeer::FTP_PASSWORD, PublisherPeer::UNIX_GID, PublisherPeer::UNIX_UID, PublisherPeer::NAGIOS_FILE_PATH, PublisherPeer::COUNTRY_ID, PublisherPeer::CONTRACT, PublisherPeer::ROYALTY_RATE, PublisherPeer::CONTRACT_START_DATE, PublisherPeer::CONTRACT_END_DATE, PublisherPeer::RENEWAL_PERIOD, PublisherPeer::ADMINS_ID, PublisherPeer::ACCOUNTANT_NOTES, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'name', 'website', 'notes', 'logo_path', 'ftp_username', 'ftp_password', 'unix_gid', 'unix_uid', 'nagios_file_path', 'country_id', 'contract', 'royalty_rate', 'contract_start_date', 'contract_end_date', 'renewal_period', 'admins_id', 'accountant_notes', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Name' => 1, 'Website' => 2, 'Notes' => 3, 'LogoPath' => 4, 'FtpUsername' => 5, 'FtpPassword' => 6, 'UnixGid' => 7, 'UnixUid' => 8, 'NagiosFilePath' => 9, 'CountryId' => 10, 'Contract' => 11, 'RoyaltyRate' => 12, 'ContractStartDate' => 13, 'ContractEndDate' => 14, 'RenewalPeriod' => 15, 'AdminsId' => 16, 'AccountantNotes' => 17, ),
		BasePeer::TYPE_COLNAME => array (PublisherPeer::ID => 0, PublisherPeer::NAME => 1, PublisherPeer::WEBSITE => 2, PublisherPeer::NOTES => 3, PublisherPeer::LOGO_PATH => 4, PublisherPeer::FTP_USERNAME => 5, PublisherPeer::FTP_PASSWORD => 6, PublisherPeer::UNIX_GID => 7, PublisherPeer::UNIX_UID => 8, PublisherPeer::NAGIOS_FILE_PATH => 9, PublisherPeer::COUNTRY_ID => 10, PublisherPeer::CONTRACT => 11, PublisherPeer::ROYALTY_RATE => 12, PublisherPeer::CONTRACT_START_DATE => 13, PublisherPeer::CONTRACT_END_DATE => 14, PublisherPeer::RENEWAL_PERIOD => 15, PublisherPeer::ADMINS_ID => 16, PublisherPeer::ACCOUNTANT_NOTES => 17, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'name' => 1, 'website' => 2, 'notes' => 3, 'logo_path' => 4, 'ftp_username' => 5, 'ftp_password' => 6, 'unix_gid' => 7, 'unix_uid' => 8, 'nagios_file_path' => 9, 'country_id' => 10, 'contract' => 11, 'royalty_rate' => 12, 'contract_start_date' => 13, 'contract_end_date' => 14, 'renewal_period' => 15, 'admins_id' => 16, 'accountant_notes' => 17, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/PublisherMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.PublisherMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = PublisherPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(PublisherPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(PublisherPeer::ID);

		$criteria->addSelectColumn(PublisherPeer::NAME);

		$criteria->addSelectColumn(PublisherPeer::WEBSITE);

		$criteria->addSelectColumn(PublisherPeer::NOTES);

		$criteria->addSelectColumn(PublisherPeer::LOGO_PATH);

		$criteria->addSelectColumn(PublisherPeer::FTP_USERNAME);

		$criteria->addSelectColumn(PublisherPeer::FTP_PASSWORD);

		$criteria->addSelectColumn(PublisherPeer::UNIX_GID);

		$criteria->addSelectColumn(PublisherPeer::UNIX_UID);

		$criteria->addSelectColumn(PublisherPeer::NAGIOS_FILE_PATH);

		$criteria->addSelectColumn(PublisherPeer::COUNTRY_ID);

		$criteria->addSelectColumn(PublisherPeer::CONTRACT);

		$criteria->addSelectColumn(PublisherPeer::ROYALTY_RATE);

		$criteria->addSelectColumn(PublisherPeer::CONTRACT_START_DATE);

		$criteria->addSelectColumn(PublisherPeer::CONTRACT_END_DATE);

		$criteria->addSelectColumn(PublisherPeer::RENEWAL_PERIOD);

		$criteria->addSelectColumn(PublisherPeer::ADMINS_ID);

		$criteria->addSelectColumn(PublisherPeer::ACCOUNTANT_NOTES);

	}

	const COUNT = 'COUNT(publisher.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT publisher.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PublisherPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PublisherPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = PublisherPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = PublisherPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return PublisherPeer::populateObjects(PublisherPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			PublisherPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = PublisherPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinCountry(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PublisherPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PublisherPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(PublisherPeer::COUNTRY_ID, CountryPeer::ID);

		$rs = PublisherPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAdmins(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PublisherPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PublisherPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(PublisherPeer::ADMINS_ID, AdminsPeer::ID);

		$rs = PublisherPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinCountry(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		PublisherPeer::addSelectColumns($c);
		$startcol = (PublisherPeer::NUM_COLUMNS - PublisherPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		CountryPeer::addSelectColumns($c);

		$c->addJoin(PublisherPeer::COUNTRY_ID, CountryPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = PublisherPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = CountryPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getCountry(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addPublisher($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initPublishers();
				$obj2->addPublisher($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAdmins(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		PublisherPeer::addSelectColumns($c);
		$startcol = (PublisherPeer::NUM_COLUMNS - PublisherPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		AdminsPeer::addSelectColumns($c);

		$c->addJoin(PublisherPeer::ADMINS_ID, AdminsPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = PublisherPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = AdminsPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getAdmins(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addPublisher($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initPublishers();
				$obj2->addPublisher($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PublisherPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PublisherPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(PublisherPeer::COUNTRY_ID, CountryPeer::ID);

		$criteria->addJoin(PublisherPeer::ADMINS_ID, AdminsPeer::ID);

		$rs = PublisherPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		PublisherPeer::addSelectColumns($c);
		$startcol2 = (PublisherPeer::NUM_COLUMNS - PublisherPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		CountryPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + CountryPeer::NUM_COLUMNS;

		AdminsPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + AdminsPeer::NUM_COLUMNS;

		$c->addJoin(PublisherPeer::COUNTRY_ID, CountryPeer::ID);

		$c->addJoin(PublisherPeer::ADMINS_ID, AdminsPeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = PublisherPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = CountryPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getCountry(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addPublisher($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initPublishers();
				$obj2->addPublisher($obj1);
			}


					
			$omClass = AdminsPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3 = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getAdmins(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addPublisher($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj3->initPublishers();
				$obj3->addPublisher($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAllExceptCountry(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PublisherPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PublisherPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(PublisherPeer::ADMINS_ID, AdminsPeer::ID);

		$rs = PublisherPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptAdmins(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PublisherPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PublisherPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(PublisherPeer::COUNTRY_ID, CountryPeer::ID);

		$rs = PublisherPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAllExceptCountry(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		PublisherPeer::addSelectColumns($c);
		$startcol2 = (PublisherPeer::NUM_COLUMNS - PublisherPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		AdminsPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + AdminsPeer::NUM_COLUMNS;

		$c->addJoin(PublisherPeer::ADMINS_ID, AdminsPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = PublisherPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = AdminsPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getAdmins(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addPublisher($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initPublishers();
				$obj2->addPublisher($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptAdmins(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		PublisherPeer::addSelectColumns($c);
		$startcol2 = (PublisherPeer::NUM_COLUMNS - PublisherPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		CountryPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + CountryPeer::NUM_COLUMNS;

		$c->addJoin(PublisherPeer::COUNTRY_ID, CountryPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = PublisherPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = CountryPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getCountry(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addPublisher($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initPublishers();
				$obj2->addPublisher($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return PublisherPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(PublisherPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(PublisherPeer::ID);
			$selectCriteria->add(PublisherPeer::ID, $criteria->remove(PublisherPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(PublisherPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(PublisherPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Publisher) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(PublisherPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Publisher $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(PublisherPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(PublisherPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(PublisherPeer::DATABASE_NAME, PublisherPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = PublisherPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(PublisherPeer::DATABASE_NAME);

		$criteria->add(PublisherPeer::ID, $pk);


		$v = PublisherPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(PublisherPeer::ID, $pks, Criteria::IN);
			$objs = PublisherPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BasePublisherPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/PublisherMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.PublisherMapBuilder');
}
