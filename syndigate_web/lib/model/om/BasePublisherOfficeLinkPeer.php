<?php


abstract class BasePublisherOfficeLinkPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'publisher_office_link';

	
	const CLASS_DEFAULT = 'lib.model.PublisherOfficeLink';

	
	const NUM_COLUMNS = 3;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'publisher_office_link.ID';

	
	const OFFICE_ID = 'publisher_office_link.OFFICE_ID';

	
	const PUBLISHER_ID = 'publisher_office_link.PUBLISHER_ID';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'OfficeId', 'PublisherId', ),
		BasePeer::TYPE_COLNAME => array (PublisherOfficeLinkPeer::ID, PublisherOfficeLinkPeer::OFFICE_ID, PublisherOfficeLinkPeer::PUBLISHER_ID, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'office_id', 'publisher_id', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'OfficeId' => 1, 'PublisherId' => 2, ),
		BasePeer::TYPE_COLNAME => array (PublisherOfficeLinkPeer::ID => 0, PublisherOfficeLinkPeer::OFFICE_ID => 1, PublisherOfficeLinkPeer::PUBLISHER_ID => 2, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'office_id' => 1, 'publisher_id' => 2, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/PublisherOfficeLinkMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.PublisherOfficeLinkMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = PublisherOfficeLinkPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(PublisherOfficeLinkPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(PublisherOfficeLinkPeer::ID);

		$criteria->addSelectColumn(PublisherOfficeLinkPeer::OFFICE_ID);

		$criteria->addSelectColumn(PublisherOfficeLinkPeer::PUBLISHER_ID);

	}

	const COUNT = 'COUNT(publisher_office_link.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT publisher_office_link.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PublisherOfficeLinkPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PublisherOfficeLinkPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = PublisherOfficeLinkPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = PublisherOfficeLinkPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return PublisherOfficeLinkPeer::populateObjects(PublisherOfficeLinkPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			PublisherOfficeLinkPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = PublisherOfficeLinkPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinPublisher(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PublisherOfficeLinkPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PublisherOfficeLinkPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(PublisherOfficeLinkPeer::PUBLISHER_ID, PublisherPeer::ID);

		$rs = PublisherOfficeLinkPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinOffice(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PublisherOfficeLinkPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PublisherOfficeLinkPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(PublisherOfficeLinkPeer::OFFICE_ID, OfficePeer::ID);

		$rs = PublisherOfficeLinkPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinPublisher(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		PublisherOfficeLinkPeer::addSelectColumns($c);
		$startcol = (PublisherOfficeLinkPeer::NUM_COLUMNS - PublisherOfficeLinkPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		PublisherPeer::addSelectColumns($c);

		$c->addJoin(PublisherOfficeLinkPeer::PUBLISHER_ID, PublisherPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = PublisherOfficeLinkPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = PublisherPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getPublisher(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addPublisherOfficeLink($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initPublisherOfficeLinks();
				$obj2->addPublisherOfficeLink($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinOffice(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		PublisherOfficeLinkPeer::addSelectColumns($c);
		$startcol = (PublisherOfficeLinkPeer::NUM_COLUMNS - PublisherOfficeLinkPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		OfficePeer::addSelectColumns($c);

		$c->addJoin(PublisherOfficeLinkPeer::OFFICE_ID, OfficePeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = PublisherOfficeLinkPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = OfficePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getOffice(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addPublisherOfficeLink($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initPublisherOfficeLinks();
				$obj2->addPublisherOfficeLink($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PublisherOfficeLinkPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PublisherOfficeLinkPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(PublisherOfficeLinkPeer::PUBLISHER_ID, PublisherPeer::ID);

		$criteria->addJoin(PublisherOfficeLinkPeer::OFFICE_ID, OfficePeer::ID);

		$rs = PublisherOfficeLinkPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		PublisherOfficeLinkPeer::addSelectColumns($c);
		$startcol2 = (PublisherOfficeLinkPeer::NUM_COLUMNS - PublisherOfficeLinkPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		PublisherPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + PublisherPeer::NUM_COLUMNS;

		OfficePeer::addSelectColumns($c);
		$startcol4 = $startcol3 + OfficePeer::NUM_COLUMNS;

		$c->addJoin(PublisherOfficeLinkPeer::PUBLISHER_ID, PublisherPeer::ID);

		$c->addJoin(PublisherOfficeLinkPeer::OFFICE_ID, OfficePeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = PublisherOfficeLinkPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = PublisherPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getPublisher(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addPublisherOfficeLink($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initPublisherOfficeLinks();
				$obj2->addPublisherOfficeLink($obj1);
			}


					
			$omClass = OfficePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3 = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getOffice(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addPublisherOfficeLink($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj3->initPublisherOfficeLinks();
				$obj3->addPublisherOfficeLink($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAllExceptPublisher(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PublisherOfficeLinkPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PublisherOfficeLinkPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(PublisherOfficeLinkPeer::OFFICE_ID, OfficePeer::ID);

		$rs = PublisherOfficeLinkPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptOffice(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PublisherOfficeLinkPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PublisherOfficeLinkPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(PublisherOfficeLinkPeer::PUBLISHER_ID, PublisherPeer::ID);

		$rs = PublisherOfficeLinkPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAllExceptPublisher(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		PublisherOfficeLinkPeer::addSelectColumns($c);
		$startcol2 = (PublisherOfficeLinkPeer::NUM_COLUMNS - PublisherOfficeLinkPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		OfficePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + OfficePeer::NUM_COLUMNS;

		$c->addJoin(PublisherOfficeLinkPeer::OFFICE_ID, OfficePeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = PublisherOfficeLinkPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = OfficePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getOffice(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addPublisherOfficeLink($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initPublisherOfficeLinks();
				$obj2->addPublisherOfficeLink($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptOffice(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		PublisherOfficeLinkPeer::addSelectColumns($c);
		$startcol2 = (PublisherOfficeLinkPeer::NUM_COLUMNS - PublisherOfficeLinkPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		PublisherPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + PublisherPeer::NUM_COLUMNS;

		$c->addJoin(PublisherOfficeLinkPeer::PUBLISHER_ID, PublisherPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = PublisherOfficeLinkPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = PublisherPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getPublisher(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addPublisherOfficeLink($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initPublisherOfficeLinks();
				$obj2->addPublisherOfficeLink($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return PublisherOfficeLinkPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(PublisherOfficeLinkPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(PublisherOfficeLinkPeer::ID);
			$selectCriteria->add(PublisherOfficeLinkPeer::ID, $criteria->remove(PublisherOfficeLinkPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(PublisherOfficeLinkPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(PublisherOfficeLinkPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof PublisherOfficeLink) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(PublisherOfficeLinkPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(PublisherOfficeLink $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(PublisherOfficeLinkPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(PublisherOfficeLinkPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(PublisherOfficeLinkPeer::DATABASE_NAME, PublisherOfficeLinkPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = PublisherOfficeLinkPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(PublisherOfficeLinkPeer::DATABASE_NAME);

		$criteria->add(PublisherOfficeLinkPeer::ID, $pk);


		$v = PublisherOfficeLinkPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(PublisherOfficeLinkPeer::ID, $pks, Criteria::IN);
			$objs = PublisherOfficeLinkPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BasePublisherOfficeLinkPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/PublisherOfficeLinkMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.PublisherOfficeLinkMapBuilder');
}
