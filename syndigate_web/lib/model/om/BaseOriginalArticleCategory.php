<?php


abstract class BaseOriginalArticleCategory extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $parent_id;


	
	protected $cat_name;


	
	protected $title_id;


	
	protected $added_at;

	
	protected $aTitle;

	
	protected $collCategoryMappings;

	
	protected $lastCategoryMappingCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getParentId()
	{

		return $this->parent_id;
	}

	
	public function getCatName()
	{

		return $this->cat_name;
	}

	
	public function getTitleId()
	{

		return $this->title_id;
	}

	
	public function getAddedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->added_at === null || $this->added_at === '') {
			return null;
		} elseif (!is_int($this->added_at)) {
						$ts = strtotime($this->added_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [added_at] as date/time value: " . var_export($this->added_at, true));
			}
		} else {
			$ts = $this->added_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = OriginalArticleCategoryPeer::ID;
		}

	} 
	
	public function setParentId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->parent_id !== $v) {
			$this->parent_id = $v;
			$this->modifiedColumns[] = OriginalArticleCategoryPeer::PARENT_ID;
		}

	} 
	
	public function setCatName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->cat_name !== $v) {
			$this->cat_name = $v;
			$this->modifiedColumns[] = OriginalArticleCategoryPeer::CAT_NAME;
		}

	} 
	
	public function setTitleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_id !== $v) {
			$this->title_id = $v;
			$this->modifiedColumns[] = OriginalArticleCategoryPeer::TITLE_ID;
		}

		if ($this->aTitle !== null && $this->aTitle->getId() !== $v) {
			$this->aTitle = null;
		}

	} 
	
	public function setAddedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [added_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->added_at !== $ts) {
			$this->added_at = $ts;
			$this->modifiedColumns[] = OriginalArticleCategoryPeer::ADDED_AT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->parent_id = $rs->getInt($startcol + 1);

			$this->cat_name = $rs->getString($startcol + 2);

			$this->title_id = $rs->getInt($startcol + 3);

			$this->added_at = $rs->getTimestamp($startcol + 4, null);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating OriginalArticleCategory object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(OriginalArticleCategoryPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			OriginalArticleCategoryPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(OriginalArticleCategoryPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aTitle !== null) {
				if ($this->aTitle->isModified()) {
					$affectedRows += $this->aTitle->save($con);
				}
				$this->setTitle($this->aTitle);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = OriginalArticleCategoryPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += OriginalArticleCategoryPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collCategoryMappings !== null) {
				foreach($this->collCategoryMappings as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aTitle !== null) {
				if (!$this->aTitle->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTitle->getValidationFailures());
				}
			}


			if (($retval = OriginalArticleCategoryPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collCategoryMappings !== null) {
					foreach($this->collCategoryMappings as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = OriginalArticleCategoryPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getParentId();
				break;
			case 2:
				return $this->getCatName();
				break;
			case 3:
				return $this->getTitleId();
				break;
			case 4:
				return $this->getAddedAt();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = OriginalArticleCategoryPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getParentId(),
			$keys[2] => $this->getCatName(),
			$keys[3] => $this->getTitleId(),
			$keys[4] => $this->getAddedAt(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = OriginalArticleCategoryPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setParentId($value);
				break;
			case 2:
				$this->setCatName($value);
				break;
			case 3:
				$this->setTitleId($value);
				break;
			case 4:
				$this->setAddedAt($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = OriginalArticleCategoryPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setParentId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setCatName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setTitleId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setAddedAt($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(OriginalArticleCategoryPeer::DATABASE_NAME);

		if ($this->isColumnModified(OriginalArticleCategoryPeer::ID)) $criteria->add(OriginalArticleCategoryPeer::ID, $this->id);
		if ($this->isColumnModified(OriginalArticleCategoryPeer::PARENT_ID)) $criteria->add(OriginalArticleCategoryPeer::PARENT_ID, $this->parent_id);
		if ($this->isColumnModified(OriginalArticleCategoryPeer::CAT_NAME)) $criteria->add(OriginalArticleCategoryPeer::CAT_NAME, $this->cat_name);
		if ($this->isColumnModified(OriginalArticleCategoryPeer::TITLE_ID)) $criteria->add(OriginalArticleCategoryPeer::TITLE_ID, $this->title_id);
		if ($this->isColumnModified(OriginalArticleCategoryPeer::ADDED_AT)) $criteria->add(OriginalArticleCategoryPeer::ADDED_AT, $this->added_at);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(OriginalArticleCategoryPeer::DATABASE_NAME);

		$criteria->add(OriginalArticleCategoryPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setParentId($this->parent_id);

		$copyObj->setCatName($this->cat_name);

		$copyObj->setTitleId($this->title_id);

		$copyObj->setAddedAt($this->added_at);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getCategoryMappings() as $relObj) {
				$copyObj->addCategoryMapping($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new OriginalArticleCategoryPeer();
		}
		return self::$peer;
	}

	
	public function setTitle($v)
	{


		if ($v === null) {
			$this->setTitleId(NULL);
		} else {
			$this->setTitleId($v->getId());
		}


		$this->aTitle = $v;
	}


	
	public function getTitle($con = null)
	{
		if ($this->aTitle === null && ($this->title_id !== null)) {
						include_once 'lib/model/om/BaseTitlePeer.php';

			$this->aTitle = TitlePeer::retrieveByPK($this->title_id, $con);

			
		}
		return $this->aTitle;
	}

	
	public function initCategoryMappings()
	{
		if ($this->collCategoryMappings === null) {
			$this->collCategoryMappings = array();
		}
	}

	
	public function getCategoryMappings($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseCategoryMappingPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collCategoryMappings === null) {
			if ($this->isNew()) {
			   $this->collCategoryMappings = array();
			} else {

				$criteria->add(CategoryMappingPeer::ORIGINAL_ARTICLE_CATEGORY_ID, $this->getId());

				CategoryMappingPeer::addSelectColumns($criteria);
				$this->collCategoryMappings = CategoryMappingPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(CategoryMappingPeer::ORIGINAL_ARTICLE_CATEGORY_ID, $this->getId());

				CategoryMappingPeer::addSelectColumns($criteria);
				if (!isset($this->lastCategoryMappingCriteria) || !$this->lastCategoryMappingCriteria->equals($criteria)) {
					$this->collCategoryMappings = CategoryMappingPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastCategoryMappingCriteria = $criteria;
		return $this->collCategoryMappings;
	}

	
	public function countCategoryMappings($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseCategoryMappingPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(CategoryMappingPeer::ORIGINAL_ARTICLE_CATEGORY_ID, $this->getId());

		return CategoryMappingPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addCategoryMapping(CategoryMapping $l)
	{
		$this->collCategoryMappings[] = $l;
		$l->setOriginalArticleCategory($this);
	}


	
	public function getCategoryMappingsJoinIptc($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseCategoryMappingPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collCategoryMappings === null) {
			if ($this->isNew()) {
				$this->collCategoryMappings = array();
			} else {

				$criteria->add(CategoryMappingPeer::ORIGINAL_ARTICLE_CATEGORY_ID, $this->getId());

				$this->collCategoryMappings = CategoryMappingPeer::doSelectJoinIptc($criteria, $con);
			}
		} else {
									
			$criteria->add(CategoryMappingPeer::ORIGINAL_ARTICLE_CATEGORY_ID, $this->getId());

			if (!isset($this->lastCategoryMappingCriteria) || !$this->lastCategoryMappingCriteria->equals($criteria)) {
				$this->collCategoryMappings = CategoryMappingPeer::doSelectJoinIptc($criteria, $con);
			}
		}
		$this->lastCategoryMappingCriteria = $criteria;

		return $this->collCategoryMappings;
	}

} 