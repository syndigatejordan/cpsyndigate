<?php


abstract class BaseGatherCronjob extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $crontab_id = 0;


	
	protected $start_timestamp;


	
	protected $end_timestamp = -62169984000;


	
	protected $script;


	
	protected $concurrent = 0;


	
	protected $implementation_id;


	
	protected $results;


	
	protected $pid = 0;


	
	protected $is_exited_normally = 0;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getCrontabId()
	{

		return $this->crontab_id;
	}

	
	public function getStartTimestamp($format = 'Y-m-d H:i:s')
	{

		if ($this->start_timestamp === null || $this->start_timestamp === '') {
			return null;
		} elseif (!is_int($this->start_timestamp)) {
						$ts = strtotime($this->start_timestamp);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [start_timestamp] as date/time value: " . var_export($this->start_timestamp, true));
			}
		} else {
			$ts = $this->start_timestamp;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getEndTimestamp($format = 'Y-m-d H:i:s')
	{

		if ($this->end_timestamp === null || $this->end_timestamp === '') {
			return null;
		} elseif (!is_int($this->end_timestamp)) {
						$ts = strtotime($this->end_timestamp);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [end_timestamp] as date/time value: " . var_export($this->end_timestamp, true));
			}
		} else {
			$ts = $this->end_timestamp;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getScript()
	{

		return $this->script;
	}

	
	public function getConcurrent()
	{

		return $this->concurrent;
	}

	
	public function getImplementationId()
	{

		return $this->implementation_id;
	}

	
	public function getResults()
	{

		return $this->results;
	}

	
	public function getPid()
	{

		return $this->pid;
	}

	
	public function getIsExitedNormally()
	{

		return $this->is_exited_normally;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = GatherCronjobPeer::ID;
		}

	} 
	
	public function setCrontabId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->crontab_id !== $v || $v === 0) {
			$this->crontab_id = $v;
			$this->modifiedColumns[] = GatherCronjobPeer::CRONTAB_ID;
		}

	} 
	
	public function setStartTimestamp($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [start_timestamp] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->start_timestamp !== $ts) {
			$this->start_timestamp = $ts;
			$this->modifiedColumns[] = GatherCronjobPeer::START_TIMESTAMP;
		}

	} 
	
	public function setEndTimestamp($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [end_timestamp] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->end_timestamp !== $ts || $ts === -62169984000) {
			$this->end_timestamp = $ts;
			$this->modifiedColumns[] = GatherCronjobPeer::END_TIMESTAMP;
		}

	} 
	
	public function setScript($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->script !== $v) {
			$this->script = $v;
			$this->modifiedColumns[] = GatherCronjobPeer::SCRIPT;
		}

	} 
	
	public function setConcurrent($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->concurrent !== $v || $v === 0) {
			$this->concurrent = $v;
			$this->modifiedColumns[] = GatherCronjobPeer::CONCURRENT;
		}

	} 
	
	public function setImplementationId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->implementation_id !== $v) {
			$this->implementation_id = $v;
			$this->modifiedColumns[] = GatherCronjobPeer::IMPLEMENTATION_ID;
		}

	} 
	
	public function setResults($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->results !== $v) {
			$this->results = $v;
			$this->modifiedColumns[] = GatherCronjobPeer::RESULTS;
		}

	} 
	
	public function setPid($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->pid !== $v || $v === 0) {
			$this->pid = $v;
			$this->modifiedColumns[] = GatherCronjobPeer::PID;
		}

	} 
	
	public function setIsExitedNormally($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->is_exited_normally !== $v || $v === 0) {
			$this->is_exited_normally = $v;
			$this->modifiedColumns[] = GatherCronjobPeer::IS_EXITED_NORMALLY;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->crontab_id = $rs->getInt($startcol + 1);

			$this->start_timestamp = $rs->getTimestamp($startcol + 2, null);

			$this->end_timestamp = $rs->getTimestamp($startcol + 3, null);

			$this->script = $rs->getString($startcol + 4);

			$this->concurrent = $rs->getInt($startcol + 5);

			$this->implementation_id = $rs->getInt($startcol + 6);

			$this->results = $rs->getString($startcol + 7);

			$this->pid = $rs->getInt($startcol + 8);

			$this->is_exited_normally = $rs->getInt($startcol + 9);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 10; 
		} catch (Exception $e) {
			throw new PropelException("Error populating GatherCronjob object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GatherCronjobPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			GatherCronjobPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GatherCronjobPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = GatherCronjobPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += GatherCronjobPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = GatherCronjobPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = GatherCronjobPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getCrontabId();
				break;
			case 2:
				return $this->getStartTimestamp();
				break;
			case 3:
				return $this->getEndTimestamp();
				break;
			case 4:
				return $this->getScript();
				break;
			case 5:
				return $this->getConcurrent();
				break;
			case 6:
				return $this->getImplementationId();
				break;
			case 7:
				return $this->getResults();
				break;
			case 8:
				return $this->getPid();
				break;
			case 9:
				return $this->getIsExitedNormally();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = GatherCronjobPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getCrontabId(),
			$keys[2] => $this->getStartTimestamp(),
			$keys[3] => $this->getEndTimestamp(),
			$keys[4] => $this->getScript(),
			$keys[5] => $this->getConcurrent(),
			$keys[6] => $this->getImplementationId(),
			$keys[7] => $this->getResults(),
			$keys[8] => $this->getPid(),
			$keys[9] => $this->getIsExitedNormally(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = GatherCronjobPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setCrontabId($value);
				break;
			case 2:
				$this->setStartTimestamp($value);
				break;
			case 3:
				$this->setEndTimestamp($value);
				break;
			case 4:
				$this->setScript($value);
				break;
			case 5:
				$this->setConcurrent($value);
				break;
			case 6:
				$this->setImplementationId($value);
				break;
			case 7:
				$this->setResults($value);
				break;
			case 8:
				$this->setPid($value);
				break;
			case 9:
				$this->setIsExitedNormally($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = GatherCronjobPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setCrontabId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setStartTimestamp($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setEndTimestamp($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setScript($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setConcurrent($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setImplementationId($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setResults($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setPid($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setIsExitedNormally($arr[$keys[9]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(GatherCronjobPeer::DATABASE_NAME);

		if ($this->isColumnModified(GatherCronjobPeer::ID)) $criteria->add(GatherCronjobPeer::ID, $this->id);
		if ($this->isColumnModified(GatherCronjobPeer::CRONTAB_ID)) $criteria->add(GatherCronjobPeer::CRONTAB_ID, $this->crontab_id);
		if ($this->isColumnModified(GatherCronjobPeer::START_TIMESTAMP)) $criteria->add(GatherCronjobPeer::START_TIMESTAMP, $this->start_timestamp);
		if ($this->isColumnModified(GatherCronjobPeer::END_TIMESTAMP)) $criteria->add(GatherCronjobPeer::END_TIMESTAMP, $this->end_timestamp);
		if ($this->isColumnModified(GatherCronjobPeer::SCRIPT)) $criteria->add(GatherCronjobPeer::SCRIPT, $this->script);
		if ($this->isColumnModified(GatherCronjobPeer::CONCURRENT)) $criteria->add(GatherCronjobPeer::CONCURRENT, $this->concurrent);
		if ($this->isColumnModified(GatherCronjobPeer::IMPLEMENTATION_ID)) $criteria->add(GatherCronjobPeer::IMPLEMENTATION_ID, $this->implementation_id);
		if ($this->isColumnModified(GatherCronjobPeer::RESULTS)) $criteria->add(GatherCronjobPeer::RESULTS, $this->results);
		if ($this->isColumnModified(GatherCronjobPeer::PID)) $criteria->add(GatherCronjobPeer::PID, $this->pid);
		if ($this->isColumnModified(GatherCronjobPeer::IS_EXITED_NORMALLY)) $criteria->add(GatherCronjobPeer::IS_EXITED_NORMALLY, $this->is_exited_normally);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(GatherCronjobPeer::DATABASE_NAME);

		$criteria->add(GatherCronjobPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setCrontabId($this->crontab_id);

		$copyObj->setStartTimestamp($this->start_timestamp);

		$copyObj->setEndTimestamp($this->end_timestamp);

		$copyObj->setScript($this->script);

		$copyObj->setConcurrent($this->concurrent);

		$copyObj->setImplementationId($this->implementation_id);

		$copyObj->setResults($this->results);

		$copyObj->setPid($this->pid);

		$copyObj->setIsExitedNormally($this->is_exited_normally);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new GatherCronjobPeer();
		}
		return self::$peer;
	}

} 