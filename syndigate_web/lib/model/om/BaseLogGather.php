<?php


abstract class BaseLogGather extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $report_gather_id;


	
	protected $category;


	
	protected $message;


	
	protected $severity;


	
	protected $source;


	
	protected $time;


	
	protected $report_id = 0;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getReportGatherId()
	{

		return $this->report_gather_id;
	}

	
	public function getCategory()
	{

		return $this->category;
	}

	
	public function getMessage()
	{

		return $this->message;
	}

	
	public function getSeverity()
	{

		return $this->severity;
	}

	
	public function getSource()
	{

		return $this->source;
	}

	
	public function getTime($format = 'Y-m-d H:i:s')
	{

		if ($this->time === null || $this->time === '') {
			return null;
		} elseif (!is_int($this->time)) {
						$ts = strtotime($this->time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [time] as date/time value: " . var_export($this->time, true));
			}
		} else {
			$ts = $this->time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getReportId()
	{

		return $this->report_id;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = LogGatherPeer::ID;
		}

	} 
	
	public function setReportGatherId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->report_gather_id !== $v) {
			$this->report_gather_id = $v;
			$this->modifiedColumns[] = LogGatherPeer::REPORT_GATHER_ID;
		}

	} 
	
	public function setCategory($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->category !== $v) {
			$this->category = $v;
			$this->modifiedColumns[] = LogGatherPeer::CATEGORY;
		}

	} 
	
	public function setMessage($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->message !== $v) {
			$this->message = $v;
			$this->modifiedColumns[] = LogGatherPeer::MESSAGE;
		}

	} 
	
	public function setSeverity($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->severity !== $v) {
			$this->severity = $v;
			$this->modifiedColumns[] = LogGatherPeer::SEVERITY;
		}

	} 
	
	public function setSource($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->source !== $v) {
			$this->source = $v;
			$this->modifiedColumns[] = LogGatherPeer::SOURCE;
		}

	} 
	
	public function setTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->time !== $ts) {
			$this->time = $ts;
			$this->modifiedColumns[] = LogGatherPeer::TIME;
		}

	} 
	
	public function setReportId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->report_id !== $v || $v === 0) {
			$this->report_id = $v;
			$this->modifiedColumns[] = LogGatherPeer::REPORT_ID;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getString($startcol + 0);

			$this->report_gather_id = $rs->getInt($startcol + 1);

			$this->category = $rs->getString($startcol + 2);

			$this->message = $rs->getString($startcol + 3);

			$this->severity = $rs->getString($startcol + 4);

			$this->source = $rs->getString($startcol + 5);

			$this->time = $rs->getTimestamp($startcol + 6, null);

			$this->report_id = $rs->getInt($startcol + 7);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 8; 
		} catch (Exception $e) {
			throw new PropelException("Error populating LogGather object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LogGatherPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			LogGatherPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LogGatherPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = LogGatherPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += LogGatherPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = LogGatherPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LogGatherPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getReportGatherId();
				break;
			case 2:
				return $this->getCategory();
				break;
			case 3:
				return $this->getMessage();
				break;
			case 4:
				return $this->getSeverity();
				break;
			case 5:
				return $this->getSource();
				break;
			case 6:
				return $this->getTime();
				break;
			case 7:
				return $this->getReportId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = LogGatherPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getReportGatherId(),
			$keys[2] => $this->getCategory(),
			$keys[3] => $this->getMessage(),
			$keys[4] => $this->getSeverity(),
			$keys[5] => $this->getSource(),
			$keys[6] => $this->getTime(),
			$keys[7] => $this->getReportId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LogGatherPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setReportGatherId($value);
				break;
			case 2:
				$this->setCategory($value);
				break;
			case 3:
				$this->setMessage($value);
				break;
			case 4:
				$this->setSeverity($value);
				break;
			case 5:
				$this->setSource($value);
				break;
			case 6:
				$this->setTime($value);
				break;
			case 7:
				$this->setReportId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = LogGatherPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setReportGatherId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setCategory($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setMessage($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setSeverity($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setSource($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setTime($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setReportId($arr[$keys[7]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(LogGatherPeer::DATABASE_NAME);

		if ($this->isColumnModified(LogGatherPeer::ID)) $criteria->add(LogGatherPeer::ID, $this->id);
		if ($this->isColumnModified(LogGatherPeer::REPORT_GATHER_ID)) $criteria->add(LogGatherPeer::REPORT_GATHER_ID, $this->report_gather_id);
		if ($this->isColumnModified(LogGatherPeer::CATEGORY)) $criteria->add(LogGatherPeer::CATEGORY, $this->category);
		if ($this->isColumnModified(LogGatherPeer::MESSAGE)) $criteria->add(LogGatherPeer::MESSAGE, $this->message);
		if ($this->isColumnModified(LogGatherPeer::SEVERITY)) $criteria->add(LogGatherPeer::SEVERITY, $this->severity);
		if ($this->isColumnModified(LogGatherPeer::SOURCE)) $criteria->add(LogGatherPeer::SOURCE, $this->source);
		if ($this->isColumnModified(LogGatherPeer::TIME)) $criteria->add(LogGatherPeer::TIME, $this->time);
		if ($this->isColumnModified(LogGatherPeer::REPORT_ID)) $criteria->add(LogGatherPeer::REPORT_ID, $this->report_id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(LogGatherPeer::DATABASE_NAME);

		$criteria->add(LogGatherPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setReportGatherId($this->report_gather_id);

		$copyObj->setCategory($this->category);

		$copyObj->setMessage($this->message);

		$copyObj->setSeverity($this->severity);

		$copyObj->setSource($this->source);

		$copyObj->setTime($this->time);

		$copyObj->setReportId($this->report_id);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new LogGatherPeer();
		}
		return self::$peer;
	}

} 