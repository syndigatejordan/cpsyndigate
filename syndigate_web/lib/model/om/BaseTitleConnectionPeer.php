<?php


abstract class BaseTitleConnectionPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'title_connection';

	
	const CLASS_DEFAULT = 'lib.model.TitleConnection';

	
	const NUM_COLUMNS = 11;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'title_connection.ID';

	
	const TITLE_ID = 'title_connection.TITLE_ID';

	
	const CONNECTION_TYPE = 'title_connection.CONNECTION_TYPE';

	
	const URL = 'title_connection.URL';

	
	const FILE_PATTERN = 'title_connection.FILE_PATTERN';

	
	const AUTHINTICATION = 'title_connection.AUTHINTICATION';

	
	const USERNAME = 'title_connection.USERNAME';

	
	const PASSWORD = 'title_connection.PASSWORD';

	
	const EXTRAS = 'title_connection.EXTRAS';

	
	const HAS_IMAGES = 'title_connection.HAS_IMAGES';

	
	const FTP_PORT = 'title_connection.FTP_PORT';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'TitleId', 'ConnectionType', 'Url', 'FilePattern', 'Authintication', 'Username', 'Password', 'Extras', 'HasImages', 'FtpPort', ),
		BasePeer::TYPE_COLNAME => array (TitleConnectionPeer::ID, TitleConnectionPeer::TITLE_ID, TitleConnectionPeer::CONNECTION_TYPE, TitleConnectionPeer::URL, TitleConnectionPeer::FILE_PATTERN, TitleConnectionPeer::AUTHINTICATION, TitleConnectionPeer::USERNAME, TitleConnectionPeer::PASSWORD, TitleConnectionPeer::EXTRAS, TitleConnectionPeer::HAS_IMAGES, TitleConnectionPeer::FTP_PORT, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'title_id', 'connection_type', 'url', 'file_pattern', 'authintication', 'username', 'password', 'extras', 'has_images', 'ftp_port', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'TitleId' => 1, 'ConnectionType' => 2, 'Url' => 3, 'FilePattern' => 4, 'Authintication' => 5, 'Username' => 6, 'Password' => 7, 'Extras' => 8, 'HasImages' => 9, 'FtpPort' => 10, ),
		BasePeer::TYPE_COLNAME => array (TitleConnectionPeer::ID => 0, TitleConnectionPeer::TITLE_ID => 1, TitleConnectionPeer::CONNECTION_TYPE => 2, TitleConnectionPeer::URL => 3, TitleConnectionPeer::FILE_PATTERN => 4, TitleConnectionPeer::AUTHINTICATION => 5, TitleConnectionPeer::USERNAME => 6, TitleConnectionPeer::PASSWORD => 7, TitleConnectionPeer::EXTRAS => 8, TitleConnectionPeer::HAS_IMAGES => 9, TitleConnectionPeer::FTP_PORT => 10, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'title_id' => 1, 'connection_type' => 2, 'url' => 3, 'file_pattern' => 4, 'authintication' => 5, 'username' => 6, 'password' => 7, 'extras' => 8, 'has_images' => 9, 'ftp_port' => 10, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/TitleConnectionMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.TitleConnectionMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = TitleConnectionPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(TitleConnectionPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(TitleConnectionPeer::ID);

		$criteria->addSelectColumn(TitleConnectionPeer::TITLE_ID);

		$criteria->addSelectColumn(TitleConnectionPeer::CONNECTION_TYPE);

		$criteria->addSelectColumn(TitleConnectionPeer::URL);

		$criteria->addSelectColumn(TitleConnectionPeer::FILE_PATTERN);

		$criteria->addSelectColumn(TitleConnectionPeer::AUTHINTICATION);

		$criteria->addSelectColumn(TitleConnectionPeer::USERNAME);

		$criteria->addSelectColumn(TitleConnectionPeer::PASSWORD);

		$criteria->addSelectColumn(TitleConnectionPeer::EXTRAS);

		$criteria->addSelectColumn(TitleConnectionPeer::HAS_IMAGES);

		$criteria->addSelectColumn(TitleConnectionPeer::FTP_PORT);

	}

	const COUNT = 'COUNT(title_connection.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT title_connection.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TitleConnectionPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TitleConnectionPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = TitleConnectionPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = TitleConnectionPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return TitleConnectionPeer::populateObjects(TitleConnectionPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			TitleConnectionPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = TitleConnectionPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinTitle(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TitleConnectionPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TitleConnectionPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(TitleConnectionPeer::TITLE_ID, TitlePeer::ID);

		$rs = TitleConnectionPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinTitle(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		TitleConnectionPeer::addSelectColumns($c);
		$startcol = (TitleConnectionPeer::NUM_COLUMNS - TitleConnectionPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		TitlePeer::addSelectColumns($c);

		$c->addJoin(TitleConnectionPeer::TITLE_ID, TitlePeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = TitleConnectionPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = TitlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getTitle(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addTitleConnection($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initTitleConnections();
				$obj2->addTitleConnection($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TitleConnectionPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TitleConnectionPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(TitleConnectionPeer::TITLE_ID, TitlePeer::ID);

		$rs = TitleConnectionPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		TitleConnectionPeer::addSelectColumns($c);
		$startcol2 = (TitleConnectionPeer::NUM_COLUMNS - TitleConnectionPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		TitlePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + TitlePeer::NUM_COLUMNS;

		$c->addJoin(TitleConnectionPeer::TITLE_ID, TitlePeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = TitleConnectionPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = TitlePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getTitle(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addTitleConnection($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initTitleConnections();
				$obj2->addTitleConnection($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return TitleConnectionPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(TitleConnectionPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(TitleConnectionPeer::ID);
			$selectCriteria->add(TitleConnectionPeer::ID, $criteria->remove(TitleConnectionPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(TitleConnectionPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(TitleConnectionPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof TitleConnection) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(TitleConnectionPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(TitleConnection $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(TitleConnectionPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(TitleConnectionPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(TitleConnectionPeer::DATABASE_NAME, TitleConnectionPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = TitleConnectionPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(TitleConnectionPeer::DATABASE_NAME);

		$criteria->add(TitleConnectionPeer::ID, $pk);


		$v = TitleConnectionPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(TitleConnectionPeer::ID, $pks, Criteria::IN);
			$objs = TitleConnectionPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseTitleConnectionPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/TitleConnectionMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.TitleConnectionMapBuilder');
}
