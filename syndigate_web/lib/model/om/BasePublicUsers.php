<?php


abstract class BasePublicUsers extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $user_id;


	
	protected $user_name;


	
	protected $password;


	
	protected $creation_time_stamp;


	
	protected $period;


	
	protected $company_name;


	
	protected $email;


	
	protected $user_status = 'NOTACTIVE';


	
	protected $first_login = -62169984000;


	
	protected $expiry_date = -62169984000;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUserId()
	{

		return $this->user_id;
	}

	
	public function getUserName()
	{

		return $this->user_name;
	}

	
	public function getPassword()
	{

		return $this->password;
	}

	
	public function getCreationTimeStamp($format = 'Y-m-d H:i:s')
	{

		if ($this->creation_time_stamp === null || $this->creation_time_stamp === '') {
			return null;
		} elseif (!is_int($this->creation_time_stamp)) {
						$ts = strtotime($this->creation_time_stamp);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [creation_time_stamp] as date/time value: " . var_export($this->creation_time_stamp, true));
			}
		} else {
			$ts = $this->creation_time_stamp;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getPeriod()
	{

		return $this->period;
	}

	
	public function getCompanyName()
	{

		return $this->company_name;
	}

	
	public function getEmail()
	{

		return $this->email;
	}

	
	public function getUserStatus()
	{

		return $this->user_status;
	}

	
	public function getFirstLogin($format = 'Y-m-d H:i:s')
	{

		if ($this->first_login === null || $this->first_login === '') {
			return null;
		} elseif (!is_int($this->first_login)) {
						$ts = strtotime($this->first_login);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [first_login] as date/time value: " . var_export($this->first_login, true));
			}
		} else {
			$ts = $this->first_login;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getExpiryDate($format = 'Y-m-d H:i:s')
	{

		if ($this->expiry_date === null || $this->expiry_date === '') {
			return null;
		} elseif (!is_int($this->expiry_date)) {
						$ts = strtotime($this->expiry_date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [expiry_date] as date/time value: " . var_export($this->expiry_date, true));
			}
		} else {
			$ts = $this->expiry_date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function setUserId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->user_id !== $v) {
			$this->user_id = $v;
			$this->modifiedColumns[] = PublicUsersPeer::USER_ID;
		}

	} 
	
	public function setUserName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_name !== $v) {
			$this->user_name = $v;
			$this->modifiedColumns[] = PublicUsersPeer::USER_NAME;
		}

	} 
	
	public function setPassword($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->password !== $v) {
			$this->password = $v;
			$this->modifiedColumns[] = PublicUsersPeer::PASSWORD;
		}

	} 
	
	public function setCreationTimeStamp($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [creation_time_stamp] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->creation_time_stamp !== $ts) {
			$this->creation_time_stamp = $ts;
			$this->modifiedColumns[] = PublicUsersPeer::CREATION_TIME_STAMP;
		}

	} 
	
	public function setPeriod($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->period !== $v) {
			$this->period = $v;
			$this->modifiedColumns[] = PublicUsersPeer::PERIOD;
		}

	} 
	
	public function setCompanyName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->company_name !== $v) {
			$this->company_name = $v;
			$this->modifiedColumns[] = PublicUsersPeer::COMPANY_NAME;
		}

	} 
	
	public function setEmail($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->email !== $v) {
			$this->email = $v;
			$this->modifiedColumns[] = PublicUsersPeer::EMAIL;
		}

	} 
	
	public function setUserStatus($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_status !== $v || $v === 'NOTACTIVE') {
			$this->user_status = $v;
			$this->modifiedColumns[] = PublicUsersPeer::USER_STATUS;
		}

	} 
	
	public function setFirstLogin($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [first_login] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->first_login !== $ts || $ts === -62169984000) {
			$this->first_login = $ts;
			$this->modifiedColumns[] = PublicUsersPeer::FIRST_LOGIN;
		}

	} 
	
	public function setExpiryDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [expiry_date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->expiry_date !== $ts || $ts === -62169984000) {
			$this->expiry_date = $ts;
			$this->modifiedColumns[] = PublicUsersPeer::EXPIRY_DATE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->user_id = $rs->getInt($startcol + 0);

			$this->user_name = $rs->getString($startcol + 1);

			$this->password = $rs->getString($startcol + 2);

			$this->creation_time_stamp = $rs->getTimestamp($startcol + 3, null);

			$this->period = $rs->getInt($startcol + 4);

			$this->company_name = $rs->getString($startcol + 5);

			$this->email = $rs->getString($startcol + 6);

			$this->user_status = $rs->getString($startcol + 7);

			$this->first_login = $rs->getTimestamp($startcol + 8, null);

			$this->expiry_date = $rs->getTimestamp($startcol + 9, null);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 10; 
		} catch (Exception $e) {
			throw new PropelException("Error populating PublicUsers object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PublicUsersPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PublicUsersPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PublicUsersPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PublicUsersPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setUserId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += PublicUsersPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = PublicUsersPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PublicUsersPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUserId();
				break;
			case 1:
				return $this->getUserName();
				break;
			case 2:
				return $this->getPassword();
				break;
			case 3:
				return $this->getCreationTimeStamp();
				break;
			case 4:
				return $this->getPeriod();
				break;
			case 5:
				return $this->getCompanyName();
				break;
			case 6:
				return $this->getEmail();
				break;
			case 7:
				return $this->getUserStatus();
				break;
			case 8:
				return $this->getFirstLogin();
				break;
			case 9:
				return $this->getExpiryDate();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PublicUsersPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUserId(),
			$keys[1] => $this->getUserName(),
			$keys[2] => $this->getPassword(),
			$keys[3] => $this->getCreationTimeStamp(),
			$keys[4] => $this->getPeriod(),
			$keys[5] => $this->getCompanyName(),
			$keys[6] => $this->getEmail(),
			$keys[7] => $this->getUserStatus(),
			$keys[8] => $this->getFirstLogin(),
			$keys[9] => $this->getExpiryDate(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PublicUsersPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUserId($value);
				break;
			case 1:
				$this->setUserName($value);
				break;
			case 2:
				$this->setPassword($value);
				break;
			case 3:
				$this->setCreationTimeStamp($value);
				break;
			case 4:
				$this->setPeriod($value);
				break;
			case 5:
				$this->setCompanyName($value);
				break;
			case 6:
				$this->setEmail($value);
				break;
			case 7:
				$this->setUserStatus($value);
				break;
			case 8:
				$this->setFirstLogin($value);
				break;
			case 9:
				$this->setExpiryDate($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PublicUsersPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUserId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUserName($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setPassword($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setCreationTimeStamp($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setPeriod($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setCompanyName($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setEmail($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setUserStatus($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setFirstLogin($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setExpiryDate($arr[$keys[9]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PublicUsersPeer::DATABASE_NAME);

		if ($this->isColumnModified(PublicUsersPeer::USER_ID)) $criteria->add(PublicUsersPeer::USER_ID, $this->user_id);
		if ($this->isColumnModified(PublicUsersPeer::USER_NAME)) $criteria->add(PublicUsersPeer::USER_NAME, $this->user_name);
		if ($this->isColumnModified(PublicUsersPeer::PASSWORD)) $criteria->add(PublicUsersPeer::PASSWORD, $this->password);
		if ($this->isColumnModified(PublicUsersPeer::CREATION_TIME_STAMP)) $criteria->add(PublicUsersPeer::CREATION_TIME_STAMP, $this->creation_time_stamp);
		if ($this->isColumnModified(PublicUsersPeer::PERIOD)) $criteria->add(PublicUsersPeer::PERIOD, $this->period);
		if ($this->isColumnModified(PublicUsersPeer::COMPANY_NAME)) $criteria->add(PublicUsersPeer::COMPANY_NAME, $this->company_name);
		if ($this->isColumnModified(PublicUsersPeer::EMAIL)) $criteria->add(PublicUsersPeer::EMAIL, $this->email);
		if ($this->isColumnModified(PublicUsersPeer::USER_STATUS)) $criteria->add(PublicUsersPeer::USER_STATUS, $this->user_status);
		if ($this->isColumnModified(PublicUsersPeer::FIRST_LOGIN)) $criteria->add(PublicUsersPeer::FIRST_LOGIN, $this->first_login);
		if ($this->isColumnModified(PublicUsersPeer::EXPIRY_DATE)) $criteria->add(PublicUsersPeer::EXPIRY_DATE, $this->expiry_date);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PublicUsersPeer::DATABASE_NAME);

		$criteria->add(PublicUsersPeer::USER_ID, $this->user_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getUserId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setUserId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUserName($this->user_name);

		$copyObj->setPassword($this->password);

		$copyObj->setCreationTimeStamp($this->creation_time_stamp);

		$copyObj->setPeriod($this->period);

		$copyObj->setCompanyName($this->company_name);

		$copyObj->setEmail($this->email);

		$copyObj->setUserStatus($this->user_status);

		$copyObj->setFirstLogin($this->first_login);

		$copyObj->setExpiryDate($this->expiry_date);


		$copyObj->setNew(true);

		$copyObj->setUserId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PublicUsersPeer();
		}
		return self::$peer;
	}

} 