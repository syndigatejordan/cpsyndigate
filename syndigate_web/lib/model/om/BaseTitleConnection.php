<?php


abstract class BaseTitleConnection extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $title_id;


	
	protected $connection_type;


	
	protected $url;


	
	protected $file_pattern;


	
	protected $authintication;


	
	protected $username;


	
	protected $password;


	
	protected $extras;


	
	protected $has_images;


	
	protected $ftp_port = 21;

	
	protected $aTitle;

	
	protected $collImagePaths;

	
	protected $lastImagePathCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getTitleId()
	{

		return $this->title_id;
	}

	
	public function getConnectionType()
	{

		return $this->connection_type;
	}

	
	public function getUrl()
	{

		return $this->url;
	}

	
	public function getFilePattern()
	{

		return $this->file_pattern;
	}

	
	public function getAuthintication()
	{

		return $this->authintication;
	}

	
	public function getUsername()
	{

		return $this->username;
	}

	
	public function getPassword()
	{

		return $this->password;
	}

	
	public function getExtras()
	{

		return $this->extras;
	}

	
	public function getHasImages()
	{

		return $this->has_images;
	}

	
	public function getFtpPort()
	{

		return $this->ftp_port;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = TitleConnectionPeer::ID;
		}

	} 
	
	public function setTitleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_id !== $v) {
			$this->title_id = $v;
			$this->modifiedColumns[] = TitleConnectionPeer::TITLE_ID;
		}

		if ($this->aTitle !== null && $this->aTitle->getId() !== $v) {
			$this->aTitle = null;
		}

	} 
	
	public function setConnectionType($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->connection_type !== $v) {
			$this->connection_type = $v;
			$this->modifiedColumns[] = TitleConnectionPeer::CONNECTION_TYPE;
		}

	} 
	
	public function setUrl($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->url !== $v) {
			$this->url = $v;
			$this->modifiedColumns[] = TitleConnectionPeer::URL;
		}

	} 
	
	public function setFilePattern($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->file_pattern !== $v) {
			$this->file_pattern = $v;
			$this->modifiedColumns[] = TitleConnectionPeer::FILE_PATTERN;
		}

	} 
	
	public function setAuthintication($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->authintication !== $v) {
			$this->authintication = $v;
			$this->modifiedColumns[] = TitleConnectionPeer::AUTHINTICATION;
		}

	} 
	
	public function setUsername($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->username !== $v) {
			$this->username = $v;
			$this->modifiedColumns[] = TitleConnectionPeer::USERNAME;
		}

	} 
	
	public function setPassword($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->password !== $v) {
			$this->password = $v;
			$this->modifiedColumns[] = TitleConnectionPeer::PASSWORD;
		}

	} 
	
	public function setExtras($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->extras !== $v) {
			$this->extras = $v;
			$this->modifiedColumns[] = TitleConnectionPeer::EXTRAS;
		}

	} 
	
	public function setHasImages($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->has_images !== $v) {
			$this->has_images = $v;
			$this->modifiedColumns[] = TitleConnectionPeer::HAS_IMAGES;
		}

	} 
	
	public function setFtpPort($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->ftp_port !== $v || $v === 21) {
			$this->ftp_port = $v;
			$this->modifiedColumns[] = TitleConnectionPeer::FTP_PORT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->title_id = $rs->getInt($startcol + 1);

			$this->connection_type = $rs->getString($startcol + 2);

			$this->url = $rs->getString($startcol + 3);

			$this->file_pattern = $rs->getString($startcol + 4);

			$this->authintication = $rs->getInt($startcol + 5);

			$this->username = $rs->getString($startcol + 6);

			$this->password = $rs->getString($startcol + 7);

			$this->extras = $rs->getString($startcol + 8);

			$this->has_images = $rs->getInt($startcol + 9);

			$this->ftp_port = $rs->getInt($startcol + 10);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 11; 
		} catch (Exception $e) {
			throw new PropelException("Error populating TitleConnection object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TitleConnectionPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			TitleConnectionPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TitleConnectionPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aTitle !== null) {
				if ($this->aTitle->isModified()) {
					$affectedRows += $this->aTitle->save($con);
				}
				$this->setTitle($this->aTitle);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = TitleConnectionPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += TitleConnectionPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collImagePaths !== null) {
				foreach($this->collImagePaths as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aTitle !== null) {
				if (!$this->aTitle->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTitle->getValidationFailures());
				}
			}


			if (($retval = TitleConnectionPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collImagePaths !== null) {
					foreach($this->collImagePaths as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TitleConnectionPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTitleId();
				break;
			case 2:
				return $this->getConnectionType();
				break;
			case 3:
				return $this->getUrl();
				break;
			case 4:
				return $this->getFilePattern();
				break;
			case 5:
				return $this->getAuthintication();
				break;
			case 6:
				return $this->getUsername();
				break;
			case 7:
				return $this->getPassword();
				break;
			case 8:
				return $this->getExtras();
				break;
			case 9:
				return $this->getHasImages();
				break;
			case 10:
				return $this->getFtpPort();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TitleConnectionPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTitleId(),
			$keys[2] => $this->getConnectionType(),
			$keys[3] => $this->getUrl(),
			$keys[4] => $this->getFilePattern(),
			$keys[5] => $this->getAuthintication(),
			$keys[6] => $this->getUsername(),
			$keys[7] => $this->getPassword(),
			$keys[8] => $this->getExtras(),
			$keys[9] => $this->getHasImages(),
			$keys[10] => $this->getFtpPort(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TitleConnectionPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTitleId($value);
				break;
			case 2:
				$this->setConnectionType($value);
				break;
			case 3:
				$this->setUrl($value);
				break;
			case 4:
				$this->setFilePattern($value);
				break;
			case 5:
				$this->setAuthintication($value);
				break;
			case 6:
				$this->setUsername($value);
				break;
			case 7:
				$this->setPassword($value);
				break;
			case 8:
				$this->setExtras($value);
				break;
			case 9:
				$this->setHasImages($value);
				break;
			case 10:
				$this->setFtpPort($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TitleConnectionPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTitleId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setConnectionType($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setUrl($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setFilePattern($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setAuthintication($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setUsername($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setPassword($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setExtras($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setHasImages($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setFtpPort($arr[$keys[10]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(TitleConnectionPeer::DATABASE_NAME);

		if ($this->isColumnModified(TitleConnectionPeer::ID)) $criteria->add(TitleConnectionPeer::ID, $this->id);
		if ($this->isColumnModified(TitleConnectionPeer::TITLE_ID)) $criteria->add(TitleConnectionPeer::TITLE_ID, $this->title_id);
		if ($this->isColumnModified(TitleConnectionPeer::CONNECTION_TYPE)) $criteria->add(TitleConnectionPeer::CONNECTION_TYPE, $this->connection_type);
		if ($this->isColumnModified(TitleConnectionPeer::URL)) $criteria->add(TitleConnectionPeer::URL, $this->url);
		if ($this->isColumnModified(TitleConnectionPeer::FILE_PATTERN)) $criteria->add(TitleConnectionPeer::FILE_PATTERN, $this->file_pattern);
		if ($this->isColumnModified(TitleConnectionPeer::AUTHINTICATION)) $criteria->add(TitleConnectionPeer::AUTHINTICATION, $this->authintication);
		if ($this->isColumnModified(TitleConnectionPeer::USERNAME)) $criteria->add(TitleConnectionPeer::USERNAME, $this->username);
		if ($this->isColumnModified(TitleConnectionPeer::PASSWORD)) $criteria->add(TitleConnectionPeer::PASSWORD, $this->password);
		if ($this->isColumnModified(TitleConnectionPeer::EXTRAS)) $criteria->add(TitleConnectionPeer::EXTRAS, $this->extras);
		if ($this->isColumnModified(TitleConnectionPeer::HAS_IMAGES)) $criteria->add(TitleConnectionPeer::HAS_IMAGES, $this->has_images);
		if ($this->isColumnModified(TitleConnectionPeer::FTP_PORT)) $criteria->add(TitleConnectionPeer::FTP_PORT, $this->ftp_port);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(TitleConnectionPeer::DATABASE_NAME);

		$criteria->add(TitleConnectionPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTitleId($this->title_id);

		$copyObj->setConnectionType($this->connection_type);

		$copyObj->setUrl($this->url);

		$copyObj->setFilePattern($this->file_pattern);

		$copyObj->setAuthintication($this->authintication);

		$copyObj->setUsername($this->username);

		$copyObj->setPassword($this->password);

		$copyObj->setExtras($this->extras);

		$copyObj->setHasImages($this->has_images);

		$copyObj->setFtpPort($this->ftp_port);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getImagePaths() as $relObj) {
				$copyObj->addImagePath($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new TitleConnectionPeer();
		}
		return self::$peer;
	}

	
	public function setTitle($v)
	{


		if ($v === null) {
			$this->setTitleId(NULL);
		} else {
			$this->setTitleId($v->getId());
		}


		$this->aTitle = $v;
	}


	
	public function getTitle($con = null)
	{
		if ($this->aTitle === null && ($this->title_id !== null)) {
						include_once 'lib/model/om/BaseTitlePeer.php';

			$this->aTitle = TitlePeer::retrieveByPK($this->title_id, $con);

			
		}
		return $this->aTitle;
	}

	
	public function initImagePaths()
	{
		if ($this->collImagePaths === null) {
			$this->collImagePaths = array();
		}
	}

	
	public function getImagePaths($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseImagePathPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collImagePaths === null) {
			if ($this->isNew()) {
			   $this->collImagePaths = array();
			} else {

				$criteria->add(ImagePathPeer::CONNNECTION_ID, $this->getId());

				ImagePathPeer::addSelectColumns($criteria);
				$this->collImagePaths = ImagePathPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ImagePathPeer::CONNNECTION_ID, $this->getId());

				ImagePathPeer::addSelectColumns($criteria);
				if (!isset($this->lastImagePathCriteria) || !$this->lastImagePathCriteria->equals($criteria)) {
					$this->collImagePaths = ImagePathPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastImagePathCriteria = $criteria;
		return $this->collImagePaths;
	}

	
	public function countImagePaths($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseImagePathPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ImagePathPeer::CONNNECTION_ID, $this->getId());

		return ImagePathPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addImagePath(ImagePath $l)
	{
		$this->collImagePaths[] = $l;
		$l->setTitleConnection($this);
	}

} 