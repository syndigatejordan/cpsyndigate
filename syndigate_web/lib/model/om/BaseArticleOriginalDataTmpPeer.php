<?php


abstract class BaseArticleOriginalDataTmpPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'article_original_data_tmp';

	
	const CLASS_DEFAULT = 'lib.model.ArticleOriginalDataTmp';

	
	const NUM_COLUMNS = 10;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'article_original_data_tmp.ID';

	
	const ORIGINAL_ARTICLE_ID = 'article_original_data_tmp.ORIGINAL_ARTICLE_ID';

	
	const ORIGINAL_SOURCE = 'article_original_data_tmp.ORIGINAL_SOURCE';

	
	const ISSUE_NUMBER = 'article_original_data_tmp.ISSUE_NUMBER';

	
	const PAGE_NUMBER = 'article_original_data_tmp.PAGE_NUMBER';

	
	const REFERENCE = 'article_original_data_tmp.REFERENCE';

	
	const EXTRAS = 'article_original_data_tmp.EXTRAS';

	
	const HIJRI_DATE = 'article_original_data_tmp.HIJRI_DATE';

	
	const ORIGINAL_CAT_ID = 'article_original_data_tmp.ORIGINAL_CAT_ID';

	
	const REVISION_NUM = 'article_original_data_tmp.REVISION_NUM';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'OriginalArticleId', 'OriginalSource', 'IssueNumber', 'PageNumber', 'Reference', 'Extras', 'HijriDate', 'OriginalCatId', 'RevisionNum', ),
		BasePeer::TYPE_COLNAME => array (ArticleOriginalDataTmpPeer::ID, ArticleOriginalDataTmpPeer::ORIGINAL_ARTICLE_ID, ArticleOriginalDataTmpPeer::ORIGINAL_SOURCE, ArticleOriginalDataTmpPeer::ISSUE_NUMBER, ArticleOriginalDataTmpPeer::PAGE_NUMBER, ArticleOriginalDataTmpPeer::REFERENCE, ArticleOriginalDataTmpPeer::EXTRAS, ArticleOriginalDataTmpPeer::HIJRI_DATE, ArticleOriginalDataTmpPeer::ORIGINAL_CAT_ID, ArticleOriginalDataTmpPeer::REVISION_NUM, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'original_article_id', 'original_source', 'issue_number', 'page_number', 'reference', 'extras', 'hijri_date', 'original_cat_id', 'revision_num', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'OriginalArticleId' => 1, 'OriginalSource' => 2, 'IssueNumber' => 3, 'PageNumber' => 4, 'Reference' => 5, 'Extras' => 6, 'HijriDate' => 7, 'OriginalCatId' => 8, 'RevisionNum' => 9, ),
		BasePeer::TYPE_COLNAME => array (ArticleOriginalDataTmpPeer::ID => 0, ArticleOriginalDataTmpPeer::ORIGINAL_ARTICLE_ID => 1, ArticleOriginalDataTmpPeer::ORIGINAL_SOURCE => 2, ArticleOriginalDataTmpPeer::ISSUE_NUMBER => 3, ArticleOriginalDataTmpPeer::PAGE_NUMBER => 4, ArticleOriginalDataTmpPeer::REFERENCE => 5, ArticleOriginalDataTmpPeer::EXTRAS => 6, ArticleOriginalDataTmpPeer::HIJRI_DATE => 7, ArticleOriginalDataTmpPeer::ORIGINAL_CAT_ID => 8, ArticleOriginalDataTmpPeer::REVISION_NUM => 9, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'original_article_id' => 1, 'original_source' => 2, 'issue_number' => 3, 'page_number' => 4, 'reference' => 5, 'extras' => 6, 'hijri_date' => 7, 'original_cat_id' => 8, 'revision_num' => 9, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/ArticleOriginalDataTmpMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.ArticleOriginalDataTmpMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ArticleOriginalDataTmpPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ArticleOriginalDataTmpPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ArticleOriginalDataTmpPeer::ID);

		$criteria->addSelectColumn(ArticleOriginalDataTmpPeer::ORIGINAL_ARTICLE_ID);

		$criteria->addSelectColumn(ArticleOriginalDataTmpPeer::ORIGINAL_SOURCE);

		$criteria->addSelectColumn(ArticleOriginalDataTmpPeer::ISSUE_NUMBER);

		$criteria->addSelectColumn(ArticleOriginalDataTmpPeer::PAGE_NUMBER);

		$criteria->addSelectColumn(ArticleOriginalDataTmpPeer::REFERENCE);

		$criteria->addSelectColumn(ArticleOriginalDataTmpPeer::EXTRAS);

		$criteria->addSelectColumn(ArticleOriginalDataTmpPeer::HIJRI_DATE);

		$criteria->addSelectColumn(ArticleOriginalDataTmpPeer::ORIGINAL_CAT_ID);

		$criteria->addSelectColumn(ArticleOriginalDataTmpPeer::REVISION_NUM);

	}

	const COUNT = 'COUNT(article_original_data_tmp.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT article_original_data_tmp.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleOriginalDataTmpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleOriginalDataTmpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ArticleOriginalDataTmpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ArticleOriginalDataTmpPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ArticleOriginalDataTmpPeer::populateObjects(ArticleOriginalDataTmpPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ArticleOriginalDataTmpPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ArticleOriginalDataTmpPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ArticleOriginalDataTmpPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(ArticleOriginalDataTmpPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(ArticleOriginalDataTmpPeer::ID);
			$selectCriteria->add(ArticleOriginalDataTmpPeer::ID, $criteria->remove(ArticleOriginalDataTmpPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(ArticleOriginalDataTmpPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ArticleOriginalDataTmpPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof ArticleOriginalDataTmp) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(ArticleOriginalDataTmpPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(ArticleOriginalDataTmp $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ArticleOriginalDataTmpPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ArticleOriginalDataTmpPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ArticleOriginalDataTmpPeer::DATABASE_NAME, ArticleOriginalDataTmpPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ArticleOriginalDataTmpPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(ArticleOriginalDataTmpPeer::DATABASE_NAME);

		$criteria->add(ArticleOriginalDataTmpPeer::ID, $pk);


		$v = ArticleOriginalDataTmpPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(ArticleOriginalDataTmpPeer::ID, $pks, Criteria::IN);
			$objs = ArticleOriginalDataTmpPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseArticleOriginalDataTmpPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/ArticleOriginalDataTmpMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.ArticleOriginalDataTmpMapBuilder');
}
