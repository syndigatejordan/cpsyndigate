<?php


abstract class BaseTitleType extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $type;

	
	protected $collTitles;

	
	protected $lastTitleCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getType()
	{

		return $this->type;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = TitleTypePeer::ID;
		}

	} 
	
	public function setType($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->type !== $v) {
			$this->type = $v;
			$this->modifiedColumns[] = TitleTypePeer::TYPE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->type = $rs->getString($startcol + 1);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 2; 
		} catch (Exception $e) {
			throw new PropelException("Error populating TitleType object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TitleTypePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			TitleTypePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TitleTypePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = TitleTypePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += TitleTypePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collTitles !== null) {
				foreach($this->collTitles as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = TitleTypePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collTitles !== null) {
					foreach($this->collTitles as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TitleTypePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getType();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TitleTypePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getType(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TitleTypePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setType($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TitleTypePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setType($arr[$keys[1]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(TitleTypePeer::DATABASE_NAME);

		if ($this->isColumnModified(TitleTypePeer::ID)) $criteria->add(TitleTypePeer::ID, $this->id);
		if ($this->isColumnModified(TitleTypePeer::TYPE)) $criteria->add(TitleTypePeer::TYPE, $this->type);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(TitleTypePeer::DATABASE_NAME);

		$criteria->add(TitleTypePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setType($this->type);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getTitles() as $relObj) {
				$copyObj->addTitle($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new TitleTypePeer();
		}
		return self::$peer;
	}

	
	public function initTitles()
	{
		if ($this->collTitles === null) {
			$this->collTitles = array();
		}
	}

	
	public function getTitles($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
			   $this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::TITLE_TYPE_ID, $this->getId());

				TitlePeer::addSelectColumns($criteria);
				$this->collTitles = TitlePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(TitlePeer::TITLE_TYPE_ID, $this->getId());

				TitlePeer::addSelectColumns($criteria);
				if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
					$this->collTitles = TitlePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastTitleCriteria = $criteria;
		return $this->collTitles;
	}

	
	public function countTitles($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(TitlePeer::TITLE_TYPE_ID, $this->getId());

		return TitlePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addTitle(Title $l)
	{
		$this->collTitles[] = $l;
		$l->setTitleType($this);
	}


	
	public function getTitlesJoinPublisher($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::TITLE_TYPE_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinPublisher($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::TITLE_TYPE_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinPublisher($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinTitleFrequency($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::TITLE_TYPE_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinTitleFrequency($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::TITLE_TYPE_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinTitleFrequency($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinCountry($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::TITLE_TYPE_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinCountry($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::TITLE_TYPE_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinCountry($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinLanguage($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::TITLE_TYPE_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinLanguage($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::TITLE_TYPE_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinLanguage($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinAdmins($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::TITLE_TYPE_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinAdmins($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::TITLE_TYPE_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinAdmins($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}

} 