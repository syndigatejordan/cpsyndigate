<?php


abstract class BaseImageTmpPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'image_tmp';

	
	const CLASS_DEFAULT = 'lib.model.ImageTmp';

	
	const NUM_COLUMNS = 9;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'image_tmp.ID';

	
	const ARTICLE_ID = 'image_tmp.ARTICLE_ID';

	
	const IMG_NAME = 'image_tmp.IMG_NAME';

	
	const IMAGE_CAPTION = 'image_tmp.IMAGE_CAPTION';

	
	const IS_HEADLINE = 'image_tmp.IS_HEADLINE';

	
	const IMAGE_TYPE = 'image_tmp.IMAGE_TYPE';

	
	const MIME_TYPE = 'image_tmp.MIME_TYPE';

	
	const ORIGINAL_NAME = 'image_tmp.ORIGINAL_NAME';

	
	const IMAGE_ORIGINAL_KEY = 'image_tmp.IMAGE_ORIGINAL_KEY';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'ArticleId', 'ImgName', 'ImageCaption', 'IsHeadline', 'ImageType', 'MimeType', 'OriginalName', 'ImageOriginalKey', ),
		BasePeer::TYPE_COLNAME => array (ImageTmpPeer::ID, ImageTmpPeer::ARTICLE_ID, ImageTmpPeer::IMG_NAME, ImageTmpPeer::IMAGE_CAPTION, ImageTmpPeer::IS_HEADLINE, ImageTmpPeer::IMAGE_TYPE, ImageTmpPeer::MIME_TYPE, ImageTmpPeer::ORIGINAL_NAME, ImageTmpPeer::IMAGE_ORIGINAL_KEY, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'article_id', 'img_name', 'image_caption', 'is_headline', 'image_type', 'mime_type', 'original_name', 'image_original_key', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'ArticleId' => 1, 'ImgName' => 2, 'ImageCaption' => 3, 'IsHeadline' => 4, 'ImageType' => 5, 'MimeType' => 6, 'OriginalName' => 7, 'ImageOriginalKey' => 8, ),
		BasePeer::TYPE_COLNAME => array (ImageTmpPeer::ID => 0, ImageTmpPeer::ARTICLE_ID => 1, ImageTmpPeer::IMG_NAME => 2, ImageTmpPeer::IMAGE_CAPTION => 3, ImageTmpPeer::IS_HEADLINE => 4, ImageTmpPeer::IMAGE_TYPE => 5, ImageTmpPeer::MIME_TYPE => 6, ImageTmpPeer::ORIGINAL_NAME => 7, ImageTmpPeer::IMAGE_ORIGINAL_KEY => 8, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'article_id' => 1, 'img_name' => 2, 'image_caption' => 3, 'is_headline' => 4, 'image_type' => 5, 'mime_type' => 6, 'original_name' => 7, 'image_original_key' => 8, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/ImageTmpMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.ImageTmpMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ImageTmpPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ImageTmpPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ImageTmpPeer::ID);

		$criteria->addSelectColumn(ImageTmpPeer::ARTICLE_ID);

		$criteria->addSelectColumn(ImageTmpPeer::IMG_NAME);

		$criteria->addSelectColumn(ImageTmpPeer::IMAGE_CAPTION);

		$criteria->addSelectColumn(ImageTmpPeer::IS_HEADLINE);

		$criteria->addSelectColumn(ImageTmpPeer::IMAGE_TYPE);

		$criteria->addSelectColumn(ImageTmpPeer::MIME_TYPE);

		$criteria->addSelectColumn(ImageTmpPeer::ORIGINAL_NAME);

		$criteria->addSelectColumn(ImageTmpPeer::IMAGE_ORIGINAL_KEY);

	}

	const COUNT = 'COUNT(image_tmp.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT image_tmp.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ImageTmpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ImageTmpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ImageTmpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ImageTmpPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ImageTmpPeer::populateObjects(ImageTmpPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ImageTmpPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ImageTmpPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinArticleTmp(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ImageTmpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ImageTmpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ImageTmpPeer::ARTICLE_ID, ArticleTmpPeer::ID);

		$rs = ImageTmpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinArticleTmp(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ImageTmpPeer::addSelectColumns($c);
		$startcol = (ImageTmpPeer::NUM_COLUMNS - ImageTmpPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		ArticleTmpPeer::addSelectColumns($c);

		$c->addJoin(ImageTmpPeer::ARTICLE_ID, ArticleTmpPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ImageTmpPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = ArticleTmpPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getArticleTmp(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addImageTmp($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initImageTmps();
				$obj2->addImageTmp($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ImageTmpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ImageTmpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ImageTmpPeer::ARTICLE_ID, ArticleTmpPeer::ID);

		$rs = ImageTmpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ImageTmpPeer::addSelectColumns($c);
		$startcol2 = (ImageTmpPeer::NUM_COLUMNS - ImageTmpPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		ArticleTmpPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + ArticleTmpPeer::NUM_COLUMNS;

		$c->addJoin(ImageTmpPeer::ARTICLE_ID, ArticleTmpPeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ImageTmpPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = ArticleTmpPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getArticleTmp(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addImageTmp($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initImageTmps();
				$obj2->addImageTmp($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ImageTmpPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(ImageTmpPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(ImageTmpPeer::ID);
			$selectCriteria->add(ImageTmpPeer::ID, $criteria->remove(ImageTmpPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(ImageTmpPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ImageTmpPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof ImageTmp) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(ImageTmpPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(ImageTmp $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ImageTmpPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ImageTmpPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ImageTmpPeer::DATABASE_NAME, ImageTmpPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ImageTmpPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(ImageTmpPeer::DATABASE_NAME);

		$criteria->add(ImageTmpPeer::ID, $pk);


		$v = ImageTmpPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(ImageTmpPeer::ID, $pks, Criteria::IN);
			$objs = ImageTmpPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseImageTmpPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/ImageTmpMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.ImageTmpMapBuilder');
}
