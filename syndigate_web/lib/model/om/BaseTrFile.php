<?php


abstract class BaseTrFile extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $client_id = 0;


	
	protected $title_id = 0;


	
	protected $local_file = '0';


	
	protected $creation_time = 0;


	
	protected $article_count = 0;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getClientId()
	{

		return $this->client_id;
	}

	
	public function getTitleId()
	{

		return $this->title_id;
	}

	
	public function getLocalFile()
	{

		return $this->local_file;
	}

	
	public function getCreationTime()
	{

		return $this->creation_time;
	}

	
	public function getArticleCount()
	{

		return $this->article_count;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = TrFilePeer::ID;
		}

	} 
	
	public function setClientId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->client_id !== $v || $v === 0) {
			$this->client_id = $v;
			$this->modifiedColumns[] = TrFilePeer::CLIENT_ID;
		}

	} 
	
	public function setTitleId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_id !== $v || $v === 0) {
			$this->title_id = $v;
			$this->modifiedColumns[] = TrFilePeer::TITLE_ID;
		}

	} 
	
	public function setLocalFile($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->local_file !== $v || $v === '0') {
			$this->local_file = $v;
			$this->modifiedColumns[] = TrFilePeer::LOCAL_FILE;
		}

	} 
	
	public function setCreationTime($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->creation_time !== $v || $v === 0) {
			$this->creation_time = $v;
			$this->modifiedColumns[] = TrFilePeer::CREATION_TIME;
		}

	} 
	
	public function setArticleCount($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->article_count !== $v || $v === 0) {
			$this->article_count = $v;
			$this->modifiedColumns[] = TrFilePeer::ARTICLE_COUNT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->client_id = $rs->getInt($startcol + 1);

			$this->title_id = $rs->getInt($startcol + 2);

			$this->local_file = $rs->getString($startcol + 3);

			$this->creation_time = $rs->getInt($startcol + 4);

			$this->article_count = $rs->getInt($startcol + 5);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 6; 
		} catch (Exception $e) {
			throw new PropelException("Error populating TrFile object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TrFilePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			TrFilePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TrFilePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = TrFilePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += TrFilePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = TrFilePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TrFilePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getClientId();
				break;
			case 2:
				return $this->getTitleId();
				break;
			case 3:
				return $this->getLocalFile();
				break;
			case 4:
				return $this->getCreationTime();
				break;
			case 5:
				return $this->getArticleCount();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TrFilePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getClientId(),
			$keys[2] => $this->getTitleId(),
			$keys[3] => $this->getLocalFile(),
			$keys[4] => $this->getCreationTime(),
			$keys[5] => $this->getArticleCount(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TrFilePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setClientId($value);
				break;
			case 2:
				$this->setTitleId($value);
				break;
			case 3:
				$this->setLocalFile($value);
				break;
			case 4:
				$this->setCreationTime($value);
				break;
			case 5:
				$this->setArticleCount($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TrFilePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setClientId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setTitleId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setLocalFile($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setCreationTime($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setArticleCount($arr[$keys[5]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(TrFilePeer::DATABASE_NAME);

		if ($this->isColumnModified(TrFilePeer::ID)) $criteria->add(TrFilePeer::ID, $this->id);
		if ($this->isColumnModified(TrFilePeer::CLIENT_ID)) $criteria->add(TrFilePeer::CLIENT_ID, $this->client_id);
		if ($this->isColumnModified(TrFilePeer::TITLE_ID)) $criteria->add(TrFilePeer::TITLE_ID, $this->title_id);
		if ($this->isColumnModified(TrFilePeer::LOCAL_FILE)) $criteria->add(TrFilePeer::LOCAL_FILE, $this->local_file);
		if ($this->isColumnModified(TrFilePeer::CREATION_TIME)) $criteria->add(TrFilePeer::CREATION_TIME, $this->creation_time);
		if ($this->isColumnModified(TrFilePeer::ARTICLE_COUNT)) $criteria->add(TrFilePeer::ARTICLE_COUNT, $this->article_count);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(TrFilePeer::DATABASE_NAME);

		$criteria->add(TrFilePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setClientId($this->client_id);

		$copyObj->setTitleId($this->title_id);

		$copyObj->setLocalFile($this->local_file);

		$copyObj->setCreationTime($this->creation_time);

		$copyObj->setArticleCount($this->article_count);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new TrFilePeer();
		}
		return self::$peer;
	}

} 