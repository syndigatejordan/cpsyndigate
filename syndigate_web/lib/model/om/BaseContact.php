<?php


abstract class BaseContact extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $type = 'technical';


	
	protected $full_name;


	
	protected $email;


	
	protected $additional_email;


	
	protected $title;


	
	protected $department;


	
	protected $zipcode;


	
	protected $pobox;


	
	protected $street_address;


	
	protected $city_name;


	
	protected $country_id = 0;


	
	protected $mobile_number;


	
	protected $work_number;


	
	protected $home_number;


	
	protected $notification_sms;


	
	protected $notification_email;

	
	protected $aCountry;

	
	protected $collClientContactLinks;

	
	protected $lastClientContactLinkCriteria = null;

	
	protected $collPublisherContactLinks;

	
	protected $lastPublisherContactLinkCriteria = null;

	
	protected $collTitleContactLinks;

	
	protected $lastTitleContactLinkCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getType()
	{

		return $this->type;
	}

	
	public function getFullName()
	{

		return $this->full_name;
	}

	
	public function getEmail()
	{

		return $this->email;
	}

	
	public function getAdditionalEmail()
	{

		return $this->additional_email;
	}

	
	public function getTitle()
	{

		return $this->title;
	}

	
	public function getDepartment()
	{

		return $this->department;
	}

	
	public function getZipcode()
	{

		return $this->zipcode;
	}

	
	public function getPobox()
	{

		return $this->pobox;
	}

	
	public function getStreetAddress()
	{

		return $this->street_address;
	}

	
	public function getCityName()
	{

		return $this->city_name;
	}

	
	public function getCountryId()
	{

		return $this->country_id;
	}

	
	public function getMobileNumber()
	{

		return $this->mobile_number;
	}

	
	public function getWorkNumber()
	{

		return $this->work_number;
	}

	
	public function getHomeNumber()
	{

		return $this->home_number;
	}

	
	public function getNotificationSms()
	{

		return $this->notification_sms;
	}

	
	public function getNotificationEmail()
	{

		return $this->notification_email;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ContactPeer::ID;
		}

	} 
	
	public function setType($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->type !== $v || $v === 'technical') {
			$this->type = $v;
			$this->modifiedColumns[] = ContactPeer::TYPE;
		}

	} 
	
	public function setFullName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->full_name !== $v) {
			$this->full_name = $v;
			$this->modifiedColumns[] = ContactPeer::FULL_NAME;
		}

	} 
	
	public function setEmail($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->email !== $v) {
			$this->email = $v;
			$this->modifiedColumns[] = ContactPeer::EMAIL;
		}

	} 
	
	public function setAdditionalEmail($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->additional_email !== $v) {
			$this->additional_email = $v;
			$this->modifiedColumns[] = ContactPeer::ADDITIONAL_EMAIL;
		}

	} 
	
	public function setTitle($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->title !== $v) {
			$this->title = $v;
			$this->modifiedColumns[] = ContactPeer::TITLE;
		}

	} 
	
	public function setDepartment($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->department !== $v) {
			$this->department = $v;
			$this->modifiedColumns[] = ContactPeer::DEPARTMENT;
		}

	} 
	
	public function setZipcode($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->zipcode !== $v) {
			$this->zipcode = $v;
			$this->modifiedColumns[] = ContactPeer::ZIPCODE;
		}

	} 
	
	public function setPobox($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->pobox !== $v) {
			$this->pobox = $v;
			$this->modifiedColumns[] = ContactPeer::POBOX;
		}

	} 
	
	public function setStreetAddress($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->street_address !== $v) {
			$this->street_address = $v;
			$this->modifiedColumns[] = ContactPeer::STREET_ADDRESS;
		}

	} 
	
	public function setCityName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->city_name !== $v) {
			$this->city_name = $v;
			$this->modifiedColumns[] = ContactPeer::CITY_NAME;
		}

	} 
	
	public function setCountryId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->country_id !== $v || $v === 0) {
			$this->country_id = $v;
			$this->modifiedColumns[] = ContactPeer::COUNTRY_ID;
		}

		if ($this->aCountry !== null && $this->aCountry->getId() !== $v) {
			$this->aCountry = null;
		}

	} 
	
	public function setMobileNumber($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->mobile_number !== $v) {
			$this->mobile_number = $v;
			$this->modifiedColumns[] = ContactPeer::MOBILE_NUMBER;
		}

	} 
	
	public function setWorkNumber($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->work_number !== $v) {
			$this->work_number = $v;
			$this->modifiedColumns[] = ContactPeer::WORK_NUMBER;
		}

	} 
	
	public function setHomeNumber($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->home_number !== $v) {
			$this->home_number = $v;
			$this->modifiedColumns[] = ContactPeer::HOME_NUMBER;
		}

	} 
	
	public function setNotificationSms($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->notification_sms !== $v) {
			$this->notification_sms = $v;
			$this->modifiedColumns[] = ContactPeer::NOTIFICATION_SMS;
		}

	} 
	
	public function setNotificationEmail($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->notification_email !== $v) {
			$this->notification_email = $v;
			$this->modifiedColumns[] = ContactPeer::NOTIFICATION_EMAIL;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->type = $rs->getString($startcol + 1);

			$this->full_name = $rs->getString($startcol + 2);

			$this->email = $rs->getString($startcol + 3);

			$this->additional_email = $rs->getString($startcol + 4);

			$this->title = $rs->getString($startcol + 5);

			$this->department = $rs->getInt($startcol + 6);

			$this->zipcode = $rs->getString($startcol + 7);

			$this->pobox = $rs->getInt($startcol + 8);

			$this->street_address = $rs->getString($startcol + 9);

			$this->city_name = $rs->getString($startcol + 10);

			$this->country_id = $rs->getInt($startcol + 11);

			$this->mobile_number = $rs->getString($startcol + 12);

			$this->work_number = $rs->getString($startcol + 13);

			$this->home_number = $rs->getString($startcol + 14);

			$this->notification_sms = $rs->getInt($startcol + 15);

			$this->notification_email = $rs->getInt($startcol + 16);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 17; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Contact object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ContactPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ContactPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ContactPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aCountry !== null) {
				if ($this->aCountry->isModified()) {
					$affectedRows += $this->aCountry->save($con);
				}
				$this->setCountry($this->aCountry);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ContactPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ContactPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collClientContactLinks !== null) {
				foreach($this->collClientContactLinks as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collPublisherContactLinks !== null) {
				foreach($this->collPublisherContactLinks as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collTitleContactLinks !== null) {
				foreach($this->collTitleContactLinks as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aCountry !== null) {
				if (!$this->aCountry->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCountry->getValidationFailures());
				}
			}


			if (($retval = ContactPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collClientContactLinks !== null) {
					foreach($this->collClientContactLinks as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collPublisherContactLinks !== null) {
					foreach($this->collPublisherContactLinks as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collTitleContactLinks !== null) {
					foreach($this->collTitleContactLinks as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ContactPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getType();
				break;
			case 2:
				return $this->getFullName();
				break;
			case 3:
				return $this->getEmail();
				break;
			case 4:
				return $this->getAdditionalEmail();
				break;
			case 5:
				return $this->getTitle();
				break;
			case 6:
				return $this->getDepartment();
				break;
			case 7:
				return $this->getZipcode();
				break;
			case 8:
				return $this->getPobox();
				break;
			case 9:
				return $this->getStreetAddress();
				break;
			case 10:
				return $this->getCityName();
				break;
			case 11:
				return $this->getCountryId();
				break;
			case 12:
				return $this->getMobileNumber();
				break;
			case 13:
				return $this->getWorkNumber();
				break;
			case 14:
				return $this->getHomeNumber();
				break;
			case 15:
				return $this->getNotificationSms();
				break;
			case 16:
				return $this->getNotificationEmail();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ContactPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getType(),
			$keys[2] => $this->getFullName(),
			$keys[3] => $this->getEmail(),
			$keys[4] => $this->getAdditionalEmail(),
			$keys[5] => $this->getTitle(),
			$keys[6] => $this->getDepartment(),
			$keys[7] => $this->getZipcode(),
			$keys[8] => $this->getPobox(),
			$keys[9] => $this->getStreetAddress(),
			$keys[10] => $this->getCityName(),
			$keys[11] => $this->getCountryId(),
			$keys[12] => $this->getMobileNumber(),
			$keys[13] => $this->getWorkNumber(),
			$keys[14] => $this->getHomeNumber(),
			$keys[15] => $this->getNotificationSms(),
			$keys[16] => $this->getNotificationEmail(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ContactPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setType($value);
				break;
			case 2:
				$this->setFullName($value);
				break;
			case 3:
				$this->setEmail($value);
				break;
			case 4:
				$this->setAdditionalEmail($value);
				break;
			case 5:
				$this->setTitle($value);
				break;
			case 6:
				$this->setDepartment($value);
				break;
			case 7:
				$this->setZipcode($value);
				break;
			case 8:
				$this->setPobox($value);
				break;
			case 9:
				$this->setStreetAddress($value);
				break;
			case 10:
				$this->setCityName($value);
				break;
			case 11:
				$this->setCountryId($value);
				break;
			case 12:
				$this->setMobileNumber($value);
				break;
			case 13:
				$this->setWorkNumber($value);
				break;
			case 14:
				$this->setHomeNumber($value);
				break;
			case 15:
				$this->setNotificationSms($value);
				break;
			case 16:
				$this->setNotificationEmail($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ContactPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setType($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setFullName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setEmail($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setAdditionalEmail($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setTitle($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setDepartment($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setZipcode($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setPobox($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setStreetAddress($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setCityName($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setCountryId($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setMobileNumber($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setWorkNumber($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setHomeNumber($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setNotificationSms($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setNotificationEmail($arr[$keys[16]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ContactPeer::DATABASE_NAME);

		if ($this->isColumnModified(ContactPeer::ID)) $criteria->add(ContactPeer::ID, $this->id);
		if ($this->isColumnModified(ContactPeer::TYPE)) $criteria->add(ContactPeer::TYPE, $this->type);
		if ($this->isColumnModified(ContactPeer::FULL_NAME)) $criteria->add(ContactPeer::FULL_NAME, $this->full_name);
		if ($this->isColumnModified(ContactPeer::EMAIL)) $criteria->add(ContactPeer::EMAIL, $this->email);
		if ($this->isColumnModified(ContactPeer::ADDITIONAL_EMAIL)) $criteria->add(ContactPeer::ADDITIONAL_EMAIL, $this->additional_email);
		if ($this->isColumnModified(ContactPeer::TITLE)) $criteria->add(ContactPeer::TITLE, $this->title);
		if ($this->isColumnModified(ContactPeer::DEPARTMENT)) $criteria->add(ContactPeer::DEPARTMENT, $this->department);
		if ($this->isColumnModified(ContactPeer::ZIPCODE)) $criteria->add(ContactPeer::ZIPCODE, $this->zipcode);
		if ($this->isColumnModified(ContactPeer::POBOX)) $criteria->add(ContactPeer::POBOX, $this->pobox);
		if ($this->isColumnModified(ContactPeer::STREET_ADDRESS)) $criteria->add(ContactPeer::STREET_ADDRESS, $this->street_address);
		if ($this->isColumnModified(ContactPeer::CITY_NAME)) $criteria->add(ContactPeer::CITY_NAME, $this->city_name);
		if ($this->isColumnModified(ContactPeer::COUNTRY_ID)) $criteria->add(ContactPeer::COUNTRY_ID, $this->country_id);
		if ($this->isColumnModified(ContactPeer::MOBILE_NUMBER)) $criteria->add(ContactPeer::MOBILE_NUMBER, $this->mobile_number);
		if ($this->isColumnModified(ContactPeer::WORK_NUMBER)) $criteria->add(ContactPeer::WORK_NUMBER, $this->work_number);
		if ($this->isColumnModified(ContactPeer::HOME_NUMBER)) $criteria->add(ContactPeer::HOME_NUMBER, $this->home_number);
		if ($this->isColumnModified(ContactPeer::NOTIFICATION_SMS)) $criteria->add(ContactPeer::NOTIFICATION_SMS, $this->notification_sms);
		if ($this->isColumnModified(ContactPeer::NOTIFICATION_EMAIL)) $criteria->add(ContactPeer::NOTIFICATION_EMAIL, $this->notification_email);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ContactPeer::DATABASE_NAME);

		$criteria->add(ContactPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setType($this->type);

		$copyObj->setFullName($this->full_name);

		$copyObj->setEmail($this->email);

		$copyObj->setAdditionalEmail($this->additional_email);

		$copyObj->setTitle($this->title);

		$copyObj->setDepartment($this->department);

		$copyObj->setZipcode($this->zipcode);

		$copyObj->setPobox($this->pobox);

		$copyObj->setStreetAddress($this->street_address);

		$copyObj->setCityName($this->city_name);

		$copyObj->setCountryId($this->country_id);

		$copyObj->setMobileNumber($this->mobile_number);

		$copyObj->setWorkNumber($this->work_number);

		$copyObj->setHomeNumber($this->home_number);

		$copyObj->setNotificationSms($this->notification_sms);

		$copyObj->setNotificationEmail($this->notification_email);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getClientContactLinks() as $relObj) {
				$copyObj->addClientContactLink($relObj->copy($deepCopy));
			}

			foreach($this->getPublisherContactLinks() as $relObj) {
				$copyObj->addPublisherContactLink($relObj->copy($deepCopy));
			}

			foreach($this->getTitleContactLinks() as $relObj) {
				$copyObj->addTitleContactLink($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ContactPeer();
		}
		return self::$peer;
	}

	
	public function setCountry($v)
	{


		if ($v === null) {
			$this->setCountryId('0');
		} else {
			$this->setCountryId($v->getId());
		}


		$this->aCountry = $v;
	}


	
	public function getCountry($con = null)
	{
		if ($this->aCountry === null && ($this->country_id !== null)) {
						include_once 'lib/model/om/BaseCountryPeer.php';

			$this->aCountry = CountryPeer::retrieveByPK($this->country_id, $con);

			
		}
		return $this->aCountry;
	}

	
	public function initClientContactLinks()
	{
		if ($this->collClientContactLinks === null) {
			$this->collClientContactLinks = array();
		}
	}

	
	public function getClientContactLinks($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseClientContactLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collClientContactLinks === null) {
			if ($this->isNew()) {
			   $this->collClientContactLinks = array();
			} else {

				$criteria->add(ClientContactLinkPeer::CONTACT_ID, $this->getId());

				ClientContactLinkPeer::addSelectColumns($criteria);
				$this->collClientContactLinks = ClientContactLinkPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ClientContactLinkPeer::CONTACT_ID, $this->getId());

				ClientContactLinkPeer::addSelectColumns($criteria);
				if (!isset($this->lastClientContactLinkCriteria) || !$this->lastClientContactLinkCriteria->equals($criteria)) {
					$this->collClientContactLinks = ClientContactLinkPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastClientContactLinkCriteria = $criteria;
		return $this->collClientContactLinks;
	}

	
	public function countClientContactLinks($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseClientContactLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ClientContactLinkPeer::CONTACT_ID, $this->getId());

		return ClientContactLinkPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addClientContactLink(ClientContactLink $l)
	{
		$this->collClientContactLinks[] = $l;
		$l->setContact($this);
	}


	
	public function getClientContactLinksJoinClient($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseClientContactLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collClientContactLinks === null) {
			if ($this->isNew()) {
				$this->collClientContactLinks = array();
			} else {

				$criteria->add(ClientContactLinkPeer::CONTACT_ID, $this->getId());

				$this->collClientContactLinks = ClientContactLinkPeer::doSelectJoinClient($criteria, $con);
			}
		} else {
									
			$criteria->add(ClientContactLinkPeer::CONTACT_ID, $this->getId());

			if (!isset($this->lastClientContactLinkCriteria) || !$this->lastClientContactLinkCriteria->equals($criteria)) {
				$this->collClientContactLinks = ClientContactLinkPeer::doSelectJoinClient($criteria, $con);
			}
		}
		$this->lastClientContactLinkCriteria = $criteria;

		return $this->collClientContactLinks;
	}

	
	public function initPublisherContactLinks()
	{
		if ($this->collPublisherContactLinks === null) {
			$this->collPublisherContactLinks = array();
		}
	}

	
	public function getPublisherContactLinks($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BasePublisherContactLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collPublisherContactLinks === null) {
			if ($this->isNew()) {
			   $this->collPublisherContactLinks = array();
			} else {

				$criteria->add(PublisherContactLinkPeer::CONTACT_ID, $this->getId());

				PublisherContactLinkPeer::addSelectColumns($criteria);
				$this->collPublisherContactLinks = PublisherContactLinkPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(PublisherContactLinkPeer::CONTACT_ID, $this->getId());

				PublisherContactLinkPeer::addSelectColumns($criteria);
				if (!isset($this->lastPublisherContactLinkCriteria) || !$this->lastPublisherContactLinkCriteria->equals($criteria)) {
					$this->collPublisherContactLinks = PublisherContactLinkPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastPublisherContactLinkCriteria = $criteria;
		return $this->collPublisherContactLinks;
	}

	
	public function countPublisherContactLinks($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BasePublisherContactLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(PublisherContactLinkPeer::CONTACT_ID, $this->getId());

		return PublisherContactLinkPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addPublisherContactLink(PublisherContactLink $l)
	{
		$this->collPublisherContactLinks[] = $l;
		$l->setContact($this);
	}


	
	public function getPublisherContactLinksJoinPublisher($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BasePublisherContactLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collPublisherContactLinks === null) {
			if ($this->isNew()) {
				$this->collPublisherContactLinks = array();
			} else {

				$criteria->add(PublisherContactLinkPeer::CONTACT_ID, $this->getId());

				$this->collPublisherContactLinks = PublisherContactLinkPeer::doSelectJoinPublisher($criteria, $con);
			}
		} else {
									
			$criteria->add(PublisherContactLinkPeer::CONTACT_ID, $this->getId());

			if (!isset($this->lastPublisherContactLinkCriteria) || !$this->lastPublisherContactLinkCriteria->equals($criteria)) {
				$this->collPublisherContactLinks = PublisherContactLinkPeer::doSelectJoinPublisher($criteria, $con);
			}
		}
		$this->lastPublisherContactLinkCriteria = $criteria;

		return $this->collPublisherContactLinks;
	}

	
	public function initTitleContactLinks()
	{
		if ($this->collTitleContactLinks === null) {
			$this->collTitleContactLinks = array();
		}
	}

	
	public function getTitleContactLinks($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitleContactLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitleContactLinks === null) {
			if ($this->isNew()) {
			   $this->collTitleContactLinks = array();
			} else {

				$criteria->add(TitleContactLinkPeer::CONTACT_ID, $this->getId());

				TitleContactLinkPeer::addSelectColumns($criteria);
				$this->collTitleContactLinks = TitleContactLinkPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(TitleContactLinkPeer::CONTACT_ID, $this->getId());

				TitleContactLinkPeer::addSelectColumns($criteria);
				if (!isset($this->lastTitleContactLinkCriteria) || !$this->lastTitleContactLinkCriteria->equals($criteria)) {
					$this->collTitleContactLinks = TitleContactLinkPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastTitleContactLinkCriteria = $criteria;
		return $this->collTitleContactLinks;
	}

	
	public function countTitleContactLinks($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseTitleContactLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(TitleContactLinkPeer::CONTACT_ID, $this->getId());

		return TitleContactLinkPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addTitleContactLink(TitleContactLink $l)
	{
		$this->collTitleContactLinks[] = $l;
		$l->setContact($this);
	}


	
	public function getTitleContactLinksJoinTitle($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitleContactLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitleContactLinks === null) {
			if ($this->isNew()) {
				$this->collTitleContactLinks = array();
			} else {

				$criteria->add(TitleContactLinkPeer::CONTACT_ID, $this->getId());

				$this->collTitleContactLinks = TitleContactLinkPeer::doSelectJoinTitle($criteria, $con);
			}
		} else {
									
			$criteria->add(TitleContactLinkPeer::CONTACT_ID, $this->getId());

			if (!isset($this->lastTitleContactLinkCriteria) || !$this->lastTitleContactLinkCriteria->equals($criteria)) {
				$this->collTitleContactLinks = TitleContactLinkPeer::doSelectJoinTitle($criteria, $con);
			}
		}
		$this->lastTitleContactLinkCriteria = $criteria;

		return $this->collTitleContactLinks;
	}

} 