<?php


abstract class BaseArticleArchivePeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'article_archive';

	
	const CLASS_DEFAULT = 'lib.model.ArticleArchive';

	
	const NUM_COLUMNS = 15;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'article_archive.ID';

	
	const IPTC_ID = 'article_archive.IPTC_ID';

	
	const TITLE_ID = 'article_archive.TITLE_ID';

	
	const ARTICLE_ORIGINAL_DATA_ID = 'article_archive.ARTICLE_ORIGINAL_DATA_ID';

	
	const LANGUAGE_ID = 'article_archive.LANGUAGE_ID';

	
	const HEADLINE = 'article_archive.HEADLINE';

	
	const SUMMARY = 'article_archive.SUMMARY';

	
	const BODY = 'article_archive.BODY';

	
	const AUTHOR = 'article_archive.AUTHOR';

	
	const DATE = 'article_archive.DATE';

	
	const PARSED_AT = 'article_archive.PARSED_AT';

	
	const UPDATED_AT = 'article_archive.UPDATED_AT';

	
	const HAS_TIME = 'article_archive.HAS_TIME';

	
	const IS_CALIAS_CALLED = 'article_archive.IS_CALIAS_CALLED';

	
	const SUB_FEED = 'article_archive.SUB_FEED';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'IptcId', 'TitleId', 'ArticleOriginalDataId', 'LanguageId', 'Headline', 'Summary', 'Body', 'Author', 'Date', 'ParsedAt', 'UpdatedAt', 'HasTime', 'IsCaliasCalled', 'SubFeed', ),
		BasePeer::TYPE_COLNAME => array (ArticleArchivePeer::ID, ArticleArchivePeer::IPTC_ID, ArticleArchivePeer::TITLE_ID, ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleArchivePeer::LANGUAGE_ID, ArticleArchivePeer::HEADLINE, ArticleArchivePeer::SUMMARY, ArticleArchivePeer::BODY, ArticleArchivePeer::AUTHOR, ArticleArchivePeer::DATE, ArticleArchivePeer::PARSED_AT, ArticleArchivePeer::UPDATED_AT, ArticleArchivePeer::HAS_TIME, ArticleArchivePeer::IS_CALIAS_CALLED, ArticleArchivePeer::SUB_FEED, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'iptc_id', 'title_id', 'article_original_data_id', 'language_id', 'headline', 'summary', 'body', 'author', 'date', 'parsed_at', 'updated_at', 'has_time', 'is_Calias_called', 'sub_feed', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'IptcId' => 1, 'TitleId' => 2, 'ArticleOriginalDataId' => 3, 'LanguageId' => 4, 'Headline' => 5, 'Summary' => 6, 'Body' => 7, 'Author' => 8, 'Date' => 9, 'ParsedAt' => 10, 'UpdatedAt' => 11, 'HasTime' => 12, 'IsCaliasCalled' => 13, 'SubFeed' => 14, ),
		BasePeer::TYPE_COLNAME => array (ArticleArchivePeer::ID => 0, ArticleArchivePeer::IPTC_ID => 1, ArticleArchivePeer::TITLE_ID => 2, ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID => 3, ArticleArchivePeer::LANGUAGE_ID => 4, ArticleArchivePeer::HEADLINE => 5, ArticleArchivePeer::SUMMARY => 6, ArticleArchivePeer::BODY => 7, ArticleArchivePeer::AUTHOR => 8, ArticleArchivePeer::DATE => 9, ArticleArchivePeer::PARSED_AT => 10, ArticleArchivePeer::UPDATED_AT => 11, ArticleArchivePeer::HAS_TIME => 12, ArticleArchivePeer::IS_CALIAS_CALLED => 13, ArticleArchivePeer::SUB_FEED => 14, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'iptc_id' => 1, 'title_id' => 2, 'article_original_data_id' => 3, 'language_id' => 4, 'headline' => 5, 'summary' => 6, 'body' => 7, 'author' => 8, 'date' => 9, 'parsed_at' => 10, 'updated_at' => 11, 'has_time' => 12, 'is_Calias_called' => 13, 'sub_feed' => 14, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/ArticleArchiveMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.ArticleArchiveMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ArticleArchivePeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ArticleArchivePeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ArticleArchivePeer::ID);

		$criteria->addSelectColumn(ArticleArchivePeer::IPTC_ID);

		$criteria->addSelectColumn(ArticleArchivePeer::TITLE_ID);

		$criteria->addSelectColumn(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID);

		$criteria->addSelectColumn(ArticleArchivePeer::LANGUAGE_ID);

		$criteria->addSelectColumn(ArticleArchivePeer::HEADLINE);

		$criteria->addSelectColumn(ArticleArchivePeer::SUMMARY);

		$criteria->addSelectColumn(ArticleArchivePeer::BODY);

		$criteria->addSelectColumn(ArticleArchivePeer::AUTHOR);

		$criteria->addSelectColumn(ArticleArchivePeer::DATE);

		$criteria->addSelectColumn(ArticleArchivePeer::PARSED_AT);

		$criteria->addSelectColumn(ArticleArchivePeer::UPDATED_AT);

		$criteria->addSelectColumn(ArticleArchivePeer::HAS_TIME);

		$criteria->addSelectColumn(ArticleArchivePeer::IS_CALIAS_CALLED);

		$criteria->addSelectColumn(ArticleArchivePeer::SUB_FEED);

	}

	const COUNT = 'COUNT(article_archive.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT article_archive.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ArticleArchivePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ArticleArchivePeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ArticleArchivePeer::populateObjects(ArticleArchivePeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ArticleArchivePeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ArticleArchivePeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinLanguage(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticleArchivePeer::LANGUAGE_ID, LanguagePeer::ID);

		$rs = ArticleArchivePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinArticleOriginalDataArchive(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataArchivePeer::ID);

		$rs = ArticleArchivePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinIptc(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticleArchivePeer::IPTC_ID, IptcPeer::ID);

		$rs = ArticleArchivePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinTitle(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticleArchivePeer::TITLE_ID, TitlePeer::ID);

		$rs = ArticleArchivePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinLanguage(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticleArchivePeer::addSelectColumns($c);
		$startcol = (ArticleArchivePeer::NUM_COLUMNS - ArticleArchivePeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		LanguagePeer::addSelectColumns($c);

		$c->addJoin(ArticleArchivePeer::LANGUAGE_ID, LanguagePeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticleArchivePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = LanguagePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getLanguage(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addArticleArchive($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initArticleArchives();
				$obj2->addArticleArchive($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinArticleOriginalDataArchive(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticleArchivePeer::addSelectColumns($c);
		$startcol = (ArticleArchivePeer::NUM_COLUMNS - ArticleArchivePeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		ArticleOriginalDataArchivePeer::addSelectColumns($c);

		$c->addJoin(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataArchivePeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticleArchivePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = ArticleOriginalDataArchivePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getArticleOriginalDataArchive(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addArticleArchive($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initArticleArchives();
				$obj2->addArticleArchive($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinIptc(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticleArchivePeer::addSelectColumns($c);
		$startcol = (ArticleArchivePeer::NUM_COLUMNS - ArticleArchivePeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		IptcPeer::addSelectColumns($c);

		$c->addJoin(ArticleArchivePeer::IPTC_ID, IptcPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticleArchivePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = IptcPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getIptc(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addArticleArchive($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initArticleArchives();
				$obj2->addArticleArchive($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinTitle(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticleArchivePeer::addSelectColumns($c);
		$startcol = (ArticleArchivePeer::NUM_COLUMNS - ArticleArchivePeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		TitlePeer::addSelectColumns($c);

		$c->addJoin(ArticleArchivePeer::TITLE_ID, TitlePeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticleArchivePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = TitlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getTitle(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addArticleArchive($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initArticleArchives();
				$obj2->addArticleArchive($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticleArchivePeer::LANGUAGE_ID, LanguagePeer::ID);

		$criteria->addJoin(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataArchivePeer::ID);

		$criteria->addJoin(ArticleArchivePeer::IPTC_ID, IptcPeer::ID);

		$criteria->addJoin(ArticleArchivePeer::TITLE_ID, TitlePeer::ID);

		$rs = ArticleArchivePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticleArchivePeer::addSelectColumns($c);
		$startcol2 = (ArticleArchivePeer::NUM_COLUMNS - ArticleArchivePeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		LanguagePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + LanguagePeer::NUM_COLUMNS;

		ArticleOriginalDataArchivePeer::addSelectColumns($c);
		$startcol4 = $startcol3 + ArticleOriginalDataArchivePeer::NUM_COLUMNS;

		IptcPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + IptcPeer::NUM_COLUMNS;

		TitlePeer::addSelectColumns($c);
		$startcol6 = $startcol5 + TitlePeer::NUM_COLUMNS;

		$c->addJoin(ArticleArchivePeer::LANGUAGE_ID, LanguagePeer::ID);

		$c->addJoin(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataArchivePeer::ID);

		$c->addJoin(ArticleArchivePeer::IPTC_ID, IptcPeer::ID);

		$c->addJoin(ArticleArchivePeer::TITLE_ID, TitlePeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticleArchivePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = LanguagePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getLanguage(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addArticleArchive($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initArticleArchives();
				$obj2->addArticleArchive($obj1);
			}


					
			$omClass = ArticleOriginalDataArchivePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3 = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getArticleOriginalDataArchive(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addArticleArchive($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj3->initArticleArchives();
				$obj3->addArticleArchive($obj1);
			}


					
			$omClass = IptcPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4 = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getIptc(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addArticleArchive($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj4->initArticleArchives();
				$obj4->addArticleArchive($obj1);
			}


					
			$omClass = TitlePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj5 = new $cls();
			$obj5->hydrate($rs, $startcol5);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj5 = $temp_obj1->getTitle(); 				if ($temp_obj5->getPrimaryKey() === $obj5->getPrimaryKey()) {
					$newObject = false;
					$temp_obj5->addArticleArchive($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj5->initArticleArchives();
				$obj5->addArticleArchive($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAllExceptLanguage(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataArchivePeer::ID);

		$criteria->addJoin(ArticleArchivePeer::IPTC_ID, IptcPeer::ID);

		$criteria->addJoin(ArticleArchivePeer::TITLE_ID, TitlePeer::ID);

		$rs = ArticleArchivePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptArticleOriginalDataArchive(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticleArchivePeer::LANGUAGE_ID, LanguagePeer::ID);

		$criteria->addJoin(ArticleArchivePeer::IPTC_ID, IptcPeer::ID);

		$criteria->addJoin(ArticleArchivePeer::TITLE_ID, TitlePeer::ID);

		$rs = ArticleArchivePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptIptc(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticleArchivePeer::LANGUAGE_ID, LanguagePeer::ID);

		$criteria->addJoin(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataArchivePeer::ID);

		$criteria->addJoin(ArticleArchivePeer::TITLE_ID, TitlePeer::ID);

		$rs = ArticleArchivePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptTitle(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleArchivePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticleArchivePeer::LANGUAGE_ID, LanguagePeer::ID);

		$criteria->addJoin(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataArchivePeer::ID);

		$criteria->addJoin(ArticleArchivePeer::IPTC_ID, IptcPeer::ID);

		$rs = ArticleArchivePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAllExceptLanguage(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticleArchivePeer::addSelectColumns($c);
		$startcol2 = (ArticleArchivePeer::NUM_COLUMNS - ArticleArchivePeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		ArticleOriginalDataArchivePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + ArticleOriginalDataArchivePeer::NUM_COLUMNS;

		IptcPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + IptcPeer::NUM_COLUMNS;

		TitlePeer::addSelectColumns($c);
		$startcol5 = $startcol4 + TitlePeer::NUM_COLUMNS;

		$c->addJoin(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataArchivePeer::ID);

		$c->addJoin(ArticleArchivePeer::IPTC_ID, IptcPeer::ID);

		$c->addJoin(ArticleArchivePeer::TITLE_ID, TitlePeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticleArchivePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = ArticleOriginalDataArchivePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getArticleOriginalDataArchive(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addArticleArchive($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initArticleArchives();
				$obj2->addArticleArchive($obj1);
			}

			$omClass = IptcPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getIptc(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addArticleArchive($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initArticleArchives();
				$obj3->addArticleArchive($obj1);
			}

			$omClass = TitlePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getTitle(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addArticleArchive($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initArticleArchives();
				$obj4->addArticleArchive($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptArticleOriginalDataArchive(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticleArchivePeer::addSelectColumns($c);
		$startcol2 = (ArticleArchivePeer::NUM_COLUMNS - ArticleArchivePeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		LanguagePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + LanguagePeer::NUM_COLUMNS;

		IptcPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + IptcPeer::NUM_COLUMNS;

		TitlePeer::addSelectColumns($c);
		$startcol5 = $startcol4 + TitlePeer::NUM_COLUMNS;

		$c->addJoin(ArticleArchivePeer::LANGUAGE_ID, LanguagePeer::ID);

		$c->addJoin(ArticleArchivePeer::IPTC_ID, IptcPeer::ID);

		$c->addJoin(ArticleArchivePeer::TITLE_ID, TitlePeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticleArchivePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = LanguagePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getLanguage(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addArticleArchive($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initArticleArchives();
				$obj2->addArticleArchive($obj1);
			}

			$omClass = IptcPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getIptc(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addArticleArchive($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initArticleArchives();
				$obj3->addArticleArchive($obj1);
			}

			$omClass = TitlePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getTitle(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addArticleArchive($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initArticleArchives();
				$obj4->addArticleArchive($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptIptc(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticleArchivePeer::addSelectColumns($c);
		$startcol2 = (ArticleArchivePeer::NUM_COLUMNS - ArticleArchivePeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		LanguagePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + LanguagePeer::NUM_COLUMNS;

		ArticleOriginalDataArchivePeer::addSelectColumns($c);
		$startcol4 = $startcol3 + ArticleOriginalDataArchivePeer::NUM_COLUMNS;

		TitlePeer::addSelectColumns($c);
		$startcol5 = $startcol4 + TitlePeer::NUM_COLUMNS;

		$c->addJoin(ArticleArchivePeer::LANGUAGE_ID, LanguagePeer::ID);

		$c->addJoin(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataArchivePeer::ID);

		$c->addJoin(ArticleArchivePeer::TITLE_ID, TitlePeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticleArchivePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = LanguagePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getLanguage(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addArticleArchive($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initArticleArchives();
				$obj2->addArticleArchive($obj1);
			}

			$omClass = ArticleOriginalDataArchivePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getArticleOriginalDataArchive(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addArticleArchive($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initArticleArchives();
				$obj3->addArticleArchive($obj1);
			}

			$omClass = TitlePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getTitle(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addArticleArchive($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initArticleArchives();
				$obj4->addArticleArchive($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptTitle(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticleArchivePeer::addSelectColumns($c);
		$startcol2 = (ArticleArchivePeer::NUM_COLUMNS - ArticleArchivePeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		LanguagePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + LanguagePeer::NUM_COLUMNS;

		ArticleOriginalDataArchivePeer::addSelectColumns($c);
		$startcol4 = $startcol3 + ArticleOriginalDataArchivePeer::NUM_COLUMNS;

		IptcPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + IptcPeer::NUM_COLUMNS;

		$c->addJoin(ArticleArchivePeer::LANGUAGE_ID, LanguagePeer::ID);

		$c->addJoin(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataArchivePeer::ID);

		$c->addJoin(ArticleArchivePeer::IPTC_ID, IptcPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticleArchivePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = LanguagePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getLanguage(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addArticleArchive($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initArticleArchives();
				$obj2->addArticleArchive($obj1);
			}

			$omClass = ArticleOriginalDataArchivePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getArticleOriginalDataArchive(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addArticleArchive($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initArticleArchives();
				$obj3->addArticleArchive($obj1);
			}

			$omClass = IptcPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getIptc(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addArticleArchive($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initArticleArchives();
				$obj4->addArticleArchive($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ArticleArchivePeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(ArticleArchivePeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(ArticleArchivePeer::ID);
			$selectCriteria->add(ArticleArchivePeer::ID, $criteria->remove(ArticleArchivePeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(ArticleArchivePeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ArticleArchivePeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof ArticleArchive) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(ArticleArchivePeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(ArticleArchive $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ArticleArchivePeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ArticleArchivePeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ArticleArchivePeer::DATABASE_NAME, ArticleArchivePeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ArticleArchivePeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(ArticleArchivePeer::DATABASE_NAME);

		$criteria->add(ArticleArchivePeer::ID, $pk);


		$v = ArticleArchivePeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(ArticleArchivePeer::ID, $pks, Criteria::IN);
			$objs = ArticleArchivePeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseArticleArchivePeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/ArticleArchiveMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.ArticleArchiveMapBuilder');
}
