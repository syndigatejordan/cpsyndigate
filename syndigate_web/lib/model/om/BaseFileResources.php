<?php


abstract class BaseFileResources extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $date;


	
	protected $client_id;


	
	protected $file_name;


	
	protected $resource;


	
	protected $is_sent = false;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getDate($format = 'Y-m-d H:i:s')
	{

		if ($this->date === null || $this->date === '') {
			return null;
		} elseif (!is_int($this->date)) {
						$ts = strtotime($this->date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [date] as date/time value: " . var_export($this->date, true));
			}
		} else {
			$ts = $this->date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getClientId()
	{

		return $this->client_id;
	}

	
	public function getFileName()
	{

		return $this->file_name;
	}

	
	public function getResource()
	{

		return $this->resource;
	}

	
	public function getIsSent()
	{

		return $this->is_sent;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = FileResourcesPeer::ID;
		}

	} 
	
	public function setDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->date !== $ts) {
			$this->date = $ts;
			$this->modifiedColumns[] = FileResourcesPeer::DATE;
		}

	} 
	
	public function setClientId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->client_id !== $v) {
			$this->client_id = $v;
			$this->modifiedColumns[] = FileResourcesPeer::CLIENT_ID;
		}

	} 
	
	public function setFileName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->file_name !== $v) {
			$this->file_name = $v;
			$this->modifiedColumns[] = FileResourcesPeer::FILE_NAME;
		}

	} 
	
	public function setResource($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->resource !== $v) {
			$this->resource = $v;
			$this->modifiedColumns[] = FileResourcesPeer::RESOURCE;
		}

	} 
	
	public function setIsSent($v)
	{

		if ($this->is_sent !== $v || $v === false) {
			$this->is_sent = $v;
			$this->modifiedColumns[] = FileResourcesPeer::IS_SENT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->date = $rs->getTimestamp($startcol + 1, null);

			$this->client_id = $rs->getInt($startcol + 2);

			$this->file_name = $rs->getString($startcol + 3);

			$this->resource = $rs->getString($startcol + 4);

			$this->is_sent = $rs->getBoolean($startcol + 5);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 6; 
		} catch (Exception $e) {
			throw new PropelException("Error populating FileResources object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(FileResourcesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			FileResourcesPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(FileResourcesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = FileResourcesPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += FileResourcesPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = FileResourcesPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = FileResourcesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getDate();
				break;
			case 2:
				return $this->getClientId();
				break;
			case 3:
				return $this->getFileName();
				break;
			case 4:
				return $this->getResource();
				break;
			case 5:
				return $this->getIsSent();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = FileResourcesPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getDate(),
			$keys[2] => $this->getClientId(),
			$keys[3] => $this->getFileName(),
			$keys[4] => $this->getResource(),
			$keys[5] => $this->getIsSent(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = FileResourcesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setDate($value);
				break;
			case 2:
				$this->setClientId($value);
				break;
			case 3:
				$this->setFileName($value);
				break;
			case 4:
				$this->setResource($value);
				break;
			case 5:
				$this->setIsSent($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = FileResourcesPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setDate($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setClientId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setFileName($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setResource($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setIsSent($arr[$keys[5]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(FileResourcesPeer::DATABASE_NAME);

		if ($this->isColumnModified(FileResourcesPeer::ID)) $criteria->add(FileResourcesPeer::ID, $this->id);
		if ($this->isColumnModified(FileResourcesPeer::DATE)) $criteria->add(FileResourcesPeer::DATE, $this->date);
		if ($this->isColumnModified(FileResourcesPeer::CLIENT_ID)) $criteria->add(FileResourcesPeer::CLIENT_ID, $this->client_id);
		if ($this->isColumnModified(FileResourcesPeer::FILE_NAME)) $criteria->add(FileResourcesPeer::FILE_NAME, $this->file_name);
		if ($this->isColumnModified(FileResourcesPeer::RESOURCE)) $criteria->add(FileResourcesPeer::RESOURCE, $this->resource);
		if ($this->isColumnModified(FileResourcesPeer::IS_SENT)) $criteria->add(FileResourcesPeer::IS_SENT, $this->is_sent);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(FileResourcesPeer::DATABASE_NAME);

		$criteria->add(FileResourcesPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setDate($this->date);

		$copyObj->setClientId($this->client_id);

		$copyObj->setFileName($this->file_name);

		$copyObj->setResource($this->resource);

		$copyObj->setIsSent($this->is_sent);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new FileResourcesPeer();
		}
		return self::$peer;
	}

} 