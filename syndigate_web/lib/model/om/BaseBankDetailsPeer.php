<?php


abstract class BaseBankDetailsPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'bank_details';

	
	const CLASS_DEFAULT = 'lib.model.BankDetails';

	
	const NUM_COLUMNS = 11;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'bank_details.ID';

	
	const PUBLISHER_ID = 'bank_details.PUBLISHER_ID';

	
	const BRANCH = 'bank_details.BRANCH';

	
	const COUNTRY_ID = 'bank_details.COUNTRY_ID';

	
	const NOTES = 'bank_details.NOTES';

	
	const ACCOUNT_NAME = 'bank_details.ACCOUNT_NAME';

	
	const ACCOUNT_NUMBER = 'bank_details.ACCOUNT_NUMBER';

	
	const SWIFT = 'bank_details.SWIFT';

	
	const IBAN = 'bank_details.IBAN';

	
	const BANK_NAME = 'bank_details.BANK_NAME';

	
	const BANK_CITY = 'bank_details.BANK_CITY';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'PublisherId', 'Branch', 'CountryId', 'Notes', 'AccountName', 'AccountNumber', 'Swift', 'Iban', 'BankName', 'BankCity', ),
		BasePeer::TYPE_COLNAME => array (BankDetailsPeer::ID, BankDetailsPeer::PUBLISHER_ID, BankDetailsPeer::BRANCH, BankDetailsPeer::COUNTRY_ID, BankDetailsPeer::NOTES, BankDetailsPeer::ACCOUNT_NAME, BankDetailsPeer::ACCOUNT_NUMBER, BankDetailsPeer::SWIFT, BankDetailsPeer::IBAN, BankDetailsPeer::BANK_NAME, BankDetailsPeer::BANK_CITY, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'publisher_id', 'branch', 'country_id', 'notes', 'account_name', 'account_number', 'swift', 'iban', 'bank_name', 'bank_city', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'PublisherId' => 1, 'Branch' => 2, 'CountryId' => 3, 'Notes' => 4, 'AccountName' => 5, 'AccountNumber' => 6, 'Swift' => 7, 'Iban' => 8, 'BankName' => 9, 'BankCity' => 10, ),
		BasePeer::TYPE_COLNAME => array (BankDetailsPeer::ID => 0, BankDetailsPeer::PUBLISHER_ID => 1, BankDetailsPeer::BRANCH => 2, BankDetailsPeer::COUNTRY_ID => 3, BankDetailsPeer::NOTES => 4, BankDetailsPeer::ACCOUNT_NAME => 5, BankDetailsPeer::ACCOUNT_NUMBER => 6, BankDetailsPeer::SWIFT => 7, BankDetailsPeer::IBAN => 8, BankDetailsPeer::BANK_NAME => 9, BankDetailsPeer::BANK_CITY => 10, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'publisher_id' => 1, 'branch' => 2, 'country_id' => 3, 'notes' => 4, 'account_name' => 5, 'account_number' => 6, 'swift' => 7, 'iban' => 8, 'bank_name' => 9, 'bank_city' => 10, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/BankDetailsMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.BankDetailsMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = BankDetailsPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(BankDetailsPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(BankDetailsPeer::ID);

		$criteria->addSelectColumn(BankDetailsPeer::PUBLISHER_ID);

		$criteria->addSelectColumn(BankDetailsPeer::BRANCH);

		$criteria->addSelectColumn(BankDetailsPeer::COUNTRY_ID);

		$criteria->addSelectColumn(BankDetailsPeer::NOTES);

		$criteria->addSelectColumn(BankDetailsPeer::ACCOUNT_NAME);

		$criteria->addSelectColumn(BankDetailsPeer::ACCOUNT_NUMBER);

		$criteria->addSelectColumn(BankDetailsPeer::SWIFT);

		$criteria->addSelectColumn(BankDetailsPeer::IBAN);

		$criteria->addSelectColumn(BankDetailsPeer::BANK_NAME);

		$criteria->addSelectColumn(BankDetailsPeer::BANK_CITY);

	}

	const COUNT = 'COUNT(bank_details.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT bank_details.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BankDetailsPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BankDetailsPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = BankDetailsPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = BankDetailsPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return BankDetailsPeer::populateObjects(BankDetailsPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			BankDetailsPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = BankDetailsPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinPublisher(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BankDetailsPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BankDetailsPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(BankDetailsPeer::PUBLISHER_ID, PublisherPeer::ID);

		$rs = BankDetailsPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinCountry(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BankDetailsPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BankDetailsPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(BankDetailsPeer::COUNTRY_ID, CountryPeer::ID);

		$rs = BankDetailsPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinPublisher(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		BankDetailsPeer::addSelectColumns($c);
		$startcol = (BankDetailsPeer::NUM_COLUMNS - BankDetailsPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		PublisherPeer::addSelectColumns($c);

		$c->addJoin(BankDetailsPeer::PUBLISHER_ID, PublisherPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = BankDetailsPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = PublisherPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getPublisher(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addBankDetails($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initBankDetailss();
				$obj2->addBankDetails($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinCountry(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		BankDetailsPeer::addSelectColumns($c);
		$startcol = (BankDetailsPeer::NUM_COLUMNS - BankDetailsPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		CountryPeer::addSelectColumns($c);

		$c->addJoin(BankDetailsPeer::COUNTRY_ID, CountryPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = BankDetailsPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = CountryPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getCountry(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addBankDetails($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initBankDetailss();
				$obj2->addBankDetails($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BankDetailsPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BankDetailsPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(BankDetailsPeer::PUBLISHER_ID, PublisherPeer::ID);

		$criteria->addJoin(BankDetailsPeer::COUNTRY_ID, CountryPeer::ID);

		$rs = BankDetailsPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		BankDetailsPeer::addSelectColumns($c);
		$startcol2 = (BankDetailsPeer::NUM_COLUMNS - BankDetailsPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		PublisherPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + PublisherPeer::NUM_COLUMNS;

		CountryPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + CountryPeer::NUM_COLUMNS;

		$c->addJoin(BankDetailsPeer::PUBLISHER_ID, PublisherPeer::ID);

		$c->addJoin(BankDetailsPeer::COUNTRY_ID, CountryPeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = BankDetailsPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = PublisherPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getPublisher(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addBankDetails($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initBankDetailss();
				$obj2->addBankDetails($obj1);
			}


					
			$omClass = CountryPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3 = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getCountry(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addBankDetails($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj3->initBankDetailss();
				$obj3->addBankDetails($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAllExceptPublisher(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BankDetailsPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BankDetailsPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(BankDetailsPeer::COUNTRY_ID, CountryPeer::ID);

		$rs = BankDetailsPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptCountry(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BankDetailsPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BankDetailsPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(BankDetailsPeer::PUBLISHER_ID, PublisherPeer::ID);

		$rs = BankDetailsPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAllExceptPublisher(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		BankDetailsPeer::addSelectColumns($c);
		$startcol2 = (BankDetailsPeer::NUM_COLUMNS - BankDetailsPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		CountryPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + CountryPeer::NUM_COLUMNS;

		$c->addJoin(BankDetailsPeer::COUNTRY_ID, CountryPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = BankDetailsPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = CountryPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getCountry(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addBankDetails($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initBankDetailss();
				$obj2->addBankDetails($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptCountry(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		BankDetailsPeer::addSelectColumns($c);
		$startcol2 = (BankDetailsPeer::NUM_COLUMNS - BankDetailsPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		PublisherPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + PublisherPeer::NUM_COLUMNS;

		$c->addJoin(BankDetailsPeer::PUBLISHER_ID, PublisherPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = BankDetailsPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = PublisherPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getPublisher(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addBankDetails($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initBankDetailss();
				$obj2->addBankDetails($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return BankDetailsPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(BankDetailsPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(BankDetailsPeer::ID);
			$selectCriteria->add(BankDetailsPeer::ID, $criteria->remove(BankDetailsPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(BankDetailsPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(BankDetailsPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof BankDetails) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(BankDetailsPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(BankDetails $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(BankDetailsPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(BankDetailsPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(BankDetailsPeer::DATABASE_NAME, BankDetailsPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = BankDetailsPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(BankDetailsPeer::DATABASE_NAME);

		$criteria->add(BankDetailsPeer::ID, $pk);


		$v = BankDetailsPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(BankDetailsPeer::ID, $pks, Criteria::IN);
			$objs = BankDetailsPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseBankDetailsPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/BankDetailsMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.BankDetailsMapBuilder');
}
