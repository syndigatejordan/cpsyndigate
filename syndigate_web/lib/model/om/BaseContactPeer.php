<?php


abstract class BaseContactPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'contact';

	
	const CLASS_DEFAULT = 'lib.model.Contact';

	
	const NUM_COLUMNS = 17;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'contact.ID';

	
	const TYPE = 'contact.TYPE';

	
	const FULL_NAME = 'contact.FULL_NAME';

	
	const EMAIL = 'contact.EMAIL';

	
	const ADDITIONAL_EMAIL = 'contact.ADDITIONAL_EMAIL';

	
	const TITLE = 'contact.TITLE';

	
	const DEPARTMENT = 'contact.DEPARTMENT';

	
	const ZIPCODE = 'contact.ZIPCODE';

	
	const POBOX = 'contact.POBOX';

	
	const STREET_ADDRESS = 'contact.STREET_ADDRESS';

	
	const CITY_NAME = 'contact.CITY_NAME';

	
	const COUNTRY_ID = 'contact.COUNTRY_ID';

	
	const MOBILE_NUMBER = 'contact.MOBILE_NUMBER';

	
	const WORK_NUMBER = 'contact.WORK_NUMBER';

	
	const HOME_NUMBER = 'contact.HOME_NUMBER';

	
	const NOTIFICATION_SMS = 'contact.NOTIFICATION_SMS';

	
	const NOTIFICATION_EMAIL = 'contact.NOTIFICATION_EMAIL';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'Type', 'FullName', 'Email', 'AdditionalEmail', 'Title', 'Department', 'Zipcode', 'Pobox', 'StreetAddress', 'CityName', 'CountryId', 'MobileNumber', 'WorkNumber', 'HomeNumber', 'NotificationSms', 'NotificationEmail', ),
		BasePeer::TYPE_COLNAME => array (ContactPeer::ID, ContactPeer::TYPE, ContactPeer::FULL_NAME, ContactPeer::EMAIL, ContactPeer::ADDITIONAL_EMAIL, ContactPeer::TITLE, ContactPeer::DEPARTMENT, ContactPeer::ZIPCODE, ContactPeer::POBOX, ContactPeer::STREET_ADDRESS, ContactPeer::CITY_NAME, ContactPeer::COUNTRY_ID, ContactPeer::MOBILE_NUMBER, ContactPeer::WORK_NUMBER, ContactPeer::HOME_NUMBER, ContactPeer::NOTIFICATION_SMS, ContactPeer::NOTIFICATION_EMAIL, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'type', 'full_name', 'email', 'additional_email', 'title', 'department', 'zipcode', 'pobox', 'street_address', 'city_name', 'country_id', 'mobile_number', 'work_number', 'home_number', 'notification_sms', 'notification_email', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Type' => 1, 'FullName' => 2, 'Email' => 3, 'AdditionalEmail' => 4, 'Title' => 5, 'Department' => 6, 'Zipcode' => 7, 'Pobox' => 8, 'StreetAddress' => 9, 'CityName' => 10, 'CountryId' => 11, 'MobileNumber' => 12, 'WorkNumber' => 13, 'HomeNumber' => 14, 'NotificationSms' => 15, 'NotificationEmail' => 16, ),
		BasePeer::TYPE_COLNAME => array (ContactPeer::ID => 0, ContactPeer::TYPE => 1, ContactPeer::FULL_NAME => 2, ContactPeer::EMAIL => 3, ContactPeer::ADDITIONAL_EMAIL => 4, ContactPeer::TITLE => 5, ContactPeer::DEPARTMENT => 6, ContactPeer::ZIPCODE => 7, ContactPeer::POBOX => 8, ContactPeer::STREET_ADDRESS => 9, ContactPeer::CITY_NAME => 10, ContactPeer::COUNTRY_ID => 11, ContactPeer::MOBILE_NUMBER => 12, ContactPeer::WORK_NUMBER => 13, ContactPeer::HOME_NUMBER => 14, ContactPeer::NOTIFICATION_SMS => 15, ContactPeer::NOTIFICATION_EMAIL => 16, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'type' => 1, 'full_name' => 2, 'email' => 3, 'additional_email' => 4, 'title' => 5, 'department' => 6, 'zipcode' => 7, 'pobox' => 8, 'street_address' => 9, 'city_name' => 10, 'country_id' => 11, 'mobile_number' => 12, 'work_number' => 13, 'home_number' => 14, 'notification_sms' => 15, 'notification_email' => 16, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/ContactMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.ContactMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ContactPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ContactPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ContactPeer::ID);

		$criteria->addSelectColumn(ContactPeer::TYPE);

		$criteria->addSelectColumn(ContactPeer::FULL_NAME);

		$criteria->addSelectColumn(ContactPeer::EMAIL);

		$criteria->addSelectColumn(ContactPeer::ADDITIONAL_EMAIL);

		$criteria->addSelectColumn(ContactPeer::TITLE);

		$criteria->addSelectColumn(ContactPeer::DEPARTMENT);

		$criteria->addSelectColumn(ContactPeer::ZIPCODE);

		$criteria->addSelectColumn(ContactPeer::POBOX);

		$criteria->addSelectColumn(ContactPeer::STREET_ADDRESS);

		$criteria->addSelectColumn(ContactPeer::CITY_NAME);

		$criteria->addSelectColumn(ContactPeer::COUNTRY_ID);

		$criteria->addSelectColumn(ContactPeer::MOBILE_NUMBER);

		$criteria->addSelectColumn(ContactPeer::WORK_NUMBER);

		$criteria->addSelectColumn(ContactPeer::HOME_NUMBER);

		$criteria->addSelectColumn(ContactPeer::NOTIFICATION_SMS);

		$criteria->addSelectColumn(ContactPeer::NOTIFICATION_EMAIL);

	}

	const COUNT = 'COUNT(contact.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT contact.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ContactPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ContactPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ContactPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ContactPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ContactPeer::populateObjects(ContactPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ContactPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ContactPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinCountry(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ContactPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ContactPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ContactPeer::COUNTRY_ID, CountryPeer::ID);

		$rs = ContactPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinCountry(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ContactPeer::addSelectColumns($c);
		$startcol = (ContactPeer::NUM_COLUMNS - ContactPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		CountryPeer::addSelectColumns($c);

		$c->addJoin(ContactPeer::COUNTRY_ID, CountryPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ContactPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = CountryPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getCountry(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addContact($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initContacts();
				$obj2->addContact($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ContactPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ContactPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ContactPeer::COUNTRY_ID, CountryPeer::ID);

		$rs = ContactPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ContactPeer::addSelectColumns($c);
		$startcol2 = (ContactPeer::NUM_COLUMNS - ContactPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		CountryPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + CountryPeer::NUM_COLUMNS;

		$c->addJoin(ContactPeer::COUNTRY_ID, CountryPeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ContactPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = CountryPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getCountry(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addContact($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initContacts();
				$obj2->addContact($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ContactPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(ContactPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(ContactPeer::ID);
			$selectCriteria->add(ContactPeer::ID, $criteria->remove(ContactPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(ContactPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ContactPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Contact) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(ContactPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Contact $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ContactPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ContactPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ContactPeer::DATABASE_NAME, ContactPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ContactPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(ContactPeer::DATABASE_NAME);

		$criteria->add(ContactPeer::ID, $pk);


		$v = ContactPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(ContactPeer::ID, $pks, Criteria::IN);
			$objs = ContactPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseContactPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/ContactMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.ContactMapBuilder');
}
