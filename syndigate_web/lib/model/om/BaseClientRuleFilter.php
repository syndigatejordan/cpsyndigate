<?php


abstract class BaseClientRuleFilter extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $filter_type;


	
	protected $filter_value;


	
	protected $client_rule_id;

	
	protected $aClientRule;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getFilterType()
	{

		return $this->filter_type;
	}

	
	public function getFilterValue()
	{

		return $this->filter_value;
	}

	
	public function getClientRuleId()
	{

		return $this->client_rule_id;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ClientRuleFilterPeer::ID;
		}

	} 
	
	public function setFilterType($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->filter_type !== $v) {
			$this->filter_type = $v;
			$this->modifiedColumns[] = ClientRuleFilterPeer::FILTER_TYPE;
		}

	} 
	
	public function setFilterValue($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->filter_value !== $v) {
			$this->filter_value = $v;
			$this->modifiedColumns[] = ClientRuleFilterPeer::FILTER_VALUE;
		}

	} 
	
	public function setClientRuleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->client_rule_id !== $v) {
			$this->client_rule_id = $v;
			$this->modifiedColumns[] = ClientRuleFilterPeer::CLIENT_RULE_ID;
		}

		if ($this->aClientRule !== null && $this->aClientRule->getId() !== $v) {
			$this->aClientRule = null;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->filter_type = $rs->getString($startcol + 1);

			$this->filter_value = $rs->getString($startcol + 2);

			$this->client_rule_id = $rs->getInt($startcol + 3);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 4; 
		} catch (Exception $e) {
			throw new PropelException("Error populating ClientRuleFilter object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ClientRuleFilterPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ClientRuleFilterPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ClientRuleFilterPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aClientRule !== null) {
				if ($this->aClientRule->isModified()) {
					$affectedRows += $this->aClientRule->save($con);
				}
				$this->setClientRule($this->aClientRule);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ClientRuleFilterPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ClientRuleFilterPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aClientRule !== null) {
				if (!$this->aClientRule->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aClientRule->getValidationFailures());
				}
			}


			if (($retval = ClientRuleFilterPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ClientRuleFilterPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getFilterType();
				break;
			case 2:
				return $this->getFilterValue();
				break;
			case 3:
				return $this->getClientRuleId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ClientRuleFilterPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getFilterType(),
			$keys[2] => $this->getFilterValue(),
			$keys[3] => $this->getClientRuleId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ClientRuleFilterPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setFilterType($value);
				break;
			case 2:
				$this->setFilterValue($value);
				break;
			case 3:
				$this->setClientRuleId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ClientRuleFilterPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setFilterType($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setFilterValue($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setClientRuleId($arr[$keys[3]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ClientRuleFilterPeer::DATABASE_NAME);

		if ($this->isColumnModified(ClientRuleFilterPeer::ID)) $criteria->add(ClientRuleFilterPeer::ID, $this->id);
		if ($this->isColumnModified(ClientRuleFilterPeer::FILTER_TYPE)) $criteria->add(ClientRuleFilterPeer::FILTER_TYPE, $this->filter_type);
		if ($this->isColumnModified(ClientRuleFilterPeer::FILTER_VALUE)) $criteria->add(ClientRuleFilterPeer::FILTER_VALUE, $this->filter_value);
		if ($this->isColumnModified(ClientRuleFilterPeer::CLIENT_RULE_ID)) $criteria->add(ClientRuleFilterPeer::CLIENT_RULE_ID, $this->client_rule_id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ClientRuleFilterPeer::DATABASE_NAME);

		$criteria->add(ClientRuleFilterPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setFilterType($this->filter_type);

		$copyObj->setFilterValue($this->filter_value);

		$copyObj->setClientRuleId($this->client_rule_id);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ClientRuleFilterPeer();
		}
		return self::$peer;
	}

	
	public function setClientRule($v)
	{


		if ($v === null) {
			$this->setClientRuleId(NULL);
		} else {
			$this->setClientRuleId($v->getId());
		}


		$this->aClientRule = $v;
	}


	
	public function getClientRule($con = null)
	{
		if ($this->aClientRule === null && ($this->client_rule_id !== null)) {
						include_once 'lib/model/om/BaseClientRulePeer.php';

			$this->aClientRule = ClientRulePeer::retrieveByPK($this->client_rule_id, $con);

			
		}
		return $this->aClientRule;
	}

} 