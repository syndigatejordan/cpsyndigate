<?php


abstract class BaseParsingStateBackup extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id = 0;


	
	protected $title_id;


	
	protected $revision_num;


	
	protected $completed = 0;


	
	protected $locked = 0;


	
	protected $created_at = -62169984000;


	
	protected $parser_started_at = -62169984000;


	
	protected $parser_finished_at = -62169984000;


	
	protected $pid;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getTitleId()
	{

		return $this->title_id;
	}

	
	public function getRevisionNum()
	{

		return $this->revision_num;
	}

	
	public function getCompleted()
	{

		return $this->completed;
	}

	
	public function getLocked()
	{

		return $this->locked;
	}

	
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->created_at === null || $this->created_at === '') {
			return null;
		} elseif (!is_int($this->created_at)) {
						$ts = strtotime($this->created_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created_at] as date/time value: " . var_export($this->created_at, true));
			}
		} else {
			$ts = $this->created_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getParserStartedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->parser_started_at === null || $this->parser_started_at === '') {
			return null;
		} elseif (!is_int($this->parser_started_at)) {
						$ts = strtotime($this->parser_started_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [parser_started_at] as date/time value: " . var_export($this->parser_started_at, true));
			}
		} else {
			$ts = $this->parser_started_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getParserFinishedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->parser_finished_at === null || $this->parser_finished_at === '') {
			return null;
		} elseif (!is_int($this->parser_finished_at)) {
						$ts = strtotime($this->parser_finished_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [parser_finished_at] as date/time value: " . var_export($this->parser_finished_at, true));
			}
		} else {
			$ts = $this->parser_finished_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getPid()
	{

		return $this->pid;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v || $v === 0) {
			$this->id = $v;
			$this->modifiedColumns[] = ParsingStateBackupPeer::ID;
		}

	} 
	
	public function setTitleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_id !== $v) {
			$this->title_id = $v;
			$this->modifiedColumns[] = ParsingStateBackupPeer::TITLE_ID;
		}

	} 
	
	public function setRevisionNum($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->revision_num !== $v) {
			$this->revision_num = $v;
			$this->modifiedColumns[] = ParsingStateBackupPeer::REVISION_NUM;
		}

	} 
	
	public function setCompleted($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->completed !== $v || $v === 0) {
			$this->completed = $v;
			$this->modifiedColumns[] = ParsingStateBackupPeer::COMPLETED;
		}

	} 
	
	public function setLocked($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->locked !== $v || $v === 0) {
			$this->locked = $v;
			$this->modifiedColumns[] = ParsingStateBackupPeer::LOCKED;
		}

	} 
	
	public function setCreatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created_at !== $ts || $ts === -62169984000) {
			$this->created_at = $ts;
			$this->modifiedColumns[] = ParsingStateBackupPeer::CREATED_AT;
		}

	} 
	
	public function setParserStartedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [parser_started_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->parser_started_at !== $ts || $ts === -62169984000) {
			$this->parser_started_at = $ts;
			$this->modifiedColumns[] = ParsingStateBackupPeer::PARSER_STARTED_AT;
		}

	} 
	
	public function setParserFinishedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [parser_finished_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->parser_finished_at !== $ts || $ts === -62169984000) {
			$this->parser_finished_at = $ts;
			$this->modifiedColumns[] = ParsingStateBackupPeer::PARSER_FINISHED_AT;
		}

	} 
	
	public function setPid($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->pid !== $v) {
			$this->pid = $v;
			$this->modifiedColumns[] = ParsingStateBackupPeer::PID;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->title_id = $rs->getInt($startcol + 1);

			$this->revision_num = $rs->getInt($startcol + 2);

			$this->completed = $rs->getInt($startcol + 3);

			$this->locked = $rs->getInt($startcol + 4);

			$this->created_at = $rs->getTimestamp($startcol + 5, null);

			$this->parser_started_at = $rs->getTimestamp($startcol + 6, null);

			$this->parser_finished_at = $rs->getTimestamp($startcol + 7, null);

			$this->pid = $rs->getInt($startcol + 8);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 9; 
		} catch (Exception $e) {
			throw new PropelException("Error populating ParsingStateBackup object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ParsingStateBackupPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ParsingStateBackupPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isNew() && !$this->isColumnModified(ParsingStateBackupPeer::CREATED_AT))
    {
      $this->setCreatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ParsingStateBackupPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ParsingStateBackupPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += ParsingStateBackupPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = ParsingStateBackupPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ParsingStateBackupPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTitleId();
				break;
			case 2:
				return $this->getRevisionNum();
				break;
			case 3:
				return $this->getCompleted();
				break;
			case 4:
				return $this->getLocked();
				break;
			case 5:
				return $this->getCreatedAt();
				break;
			case 6:
				return $this->getParserStartedAt();
				break;
			case 7:
				return $this->getParserFinishedAt();
				break;
			case 8:
				return $this->getPid();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ParsingStateBackupPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTitleId(),
			$keys[2] => $this->getRevisionNum(),
			$keys[3] => $this->getCompleted(),
			$keys[4] => $this->getLocked(),
			$keys[5] => $this->getCreatedAt(),
			$keys[6] => $this->getParserStartedAt(),
			$keys[7] => $this->getParserFinishedAt(),
			$keys[8] => $this->getPid(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ParsingStateBackupPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTitleId($value);
				break;
			case 2:
				$this->setRevisionNum($value);
				break;
			case 3:
				$this->setCompleted($value);
				break;
			case 4:
				$this->setLocked($value);
				break;
			case 5:
				$this->setCreatedAt($value);
				break;
			case 6:
				$this->setParserStartedAt($value);
				break;
			case 7:
				$this->setParserFinishedAt($value);
				break;
			case 8:
				$this->setPid($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ParsingStateBackupPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTitleId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setRevisionNum($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setCompleted($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setLocked($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setCreatedAt($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setParserStartedAt($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setParserFinishedAt($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setPid($arr[$keys[8]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ParsingStateBackupPeer::DATABASE_NAME);

		if ($this->isColumnModified(ParsingStateBackupPeer::ID)) $criteria->add(ParsingStateBackupPeer::ID, $this->id);
		if ($this->isColumnModified(ParsingStateBackupPeer::TITLE_ID)) $criteria->add(ParsingStateBackupPeer::TITLE_ID, $this->title_id);
		if ($this->isColumnModified(ParsingStateBackupPeer::REVISION_NUM)) $criteria->add(ParsingStateBackupPeer::REVISION_NUM, $this->revision_num);
		if ($this->isColumnModified(ParsingStateBackupPeer::COMPLETED)) $criteria->add(ParsingStateBackupPeer::COMPLETED, $this->completed);
		if ($this->isColumnModified(ParsingStateBackupPeer::LOCKED)) $criteria->add(ParsingStateBackupPeer::LOCKED, $this->locked);
		if ($this->isColumnModified(ParsingStateBackupPeer::CREATED_AT)) $criteria->add(ParsingStateBackupPeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(ParsingStateBackupPeer::PARSER_STARTED_AT)) $criteria->add(ParsingStateBackupPeer::PARSER_STARTED_AT, $this->parser_started_at);
		if ($this->isColumnModified(ParsingStateBackupPeer::PARSER_FINISHED_AT)) $criteria->add(ParsingStateBackupPeer::PARSER_FINISHED_AT, $this->parser_finished_at);
		if ($this->isColumnModified(ParsingStateBackupPeer::PID)) $criteria->add(ParsingStateBackupPeer::PID, $this->pid);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ParsingStateBackupPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setId($this->id);

		$copyObj->setTitleId($this->title_id);

		$copyObj->setRevisionNum($this->revision_num);

		$copyObj->setCompleted($this->completed);

		$copyObj->setLocked($this->locked);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setParserStartedAt($this->parser_started_at);

		$copyObj->setParserFinishedAt($this->parser_finished_at);

		$copyObj->setPid($this->pid);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ParsingStateBackupPeer();
		}
		return self::$peer;
	}

} 