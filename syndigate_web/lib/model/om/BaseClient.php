<?php


abstract class BaseClient extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $client_name;


	
	protected $url;


	
	protected $connection_type;


	
	protected $home_dir;


	
	protected $username;


	
	protected $password;


	
	protected $ftp_username;


	
	protected $ftp_password;


	
	protected $dir_layout = 'dates';


	
	protected $is_active = 0;


	
	protected $logo_path;


	
	protected $ftp_port = 21;


	
	protected $remote_dir;


	
	protected $contract = 'No';


	
	protected $contract_start_date;


	
	protected $contract_end_date;


	
	protected $renewal_period;


	
	protected $royalty_rate = 0;


	
	protected $nagios_file_path;


	
	protected $zipped = 0;


	
	protected $generate_type;


	
	protected $notes;


	
	protected $xml_type = 'newsml';

	
	protected $collClientContactLinks;

	
	protected $lastClientContactLinkCriteria = null;

	
	protected $collClientRules;

	
	protected $lastClientRuleCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getClientName()
	{

		return $this->client_name;
	}

	
	public function getUrl()
	{

		return $this->url;
	}

	
	public function getConnectionType()
	{

		return $this->connection_type;
	}

	
	public function getHomeDir()
	{

		return $this->home_dir;
	}

	
	public function getUsername()
	{

		return $this->username;
	}

	
	public function getPassword()
	{

		return $this->password;
	}

	
	public function getFtpUsername()
	{

		return $this->ftp_username;
	}

	
	public function getFtpPassword()
	{

		return $this->ftp_password;
	}

	
	public function getDirLayout()
	{

		return $this->dir_layout;
	}

	
	public function getIsActive()
	{

		return $this->is_active;
	}

	
	public function getLogoPath()
	{

		return $this->logo_path;
	}

	
	public function getFtpPort()
	{

		return $this->ftp_port;
	}

	
	public function getRemoteDir()
	{

		return $this->remote_dir;
	}

	
	public function getContract()
	{

		return $this->contract;
	}

	
	public function getContractStartDate($format = 'Y-m-d')
	{

		if ($this->contract_start_date === null || $this->contract_start_date === '') {
			return null;
		} elseif (!is_int($this->contract_start_date)) {
						$ts = strtotime($this->contract_start_date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [contract_start_date] as date/time value: " . var_export($this->contract_start_date, true));
			}
		} else {
			$ts = $this->contract_start_date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getContractEndDate($format = 'Y-m-d')
	{

		if ($this->contract_end_date === null || $this->contract_end_date === '') {
			return null;
		} elseif (!is_int($this->contract_end_date)) {
						$ts = strtotime($this->contract_end_date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [contract_end_date] as date/time value: " . var_export($this->contract_end_date, true));
			}
		} else {
			$ts = $this->contract_end_date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getRenewalPeriod()
	{

		return $this->renewal_period;
	}

	
	public function getRoyaltyRate()
	{

		return $this->royalty_rate;
	}

	
	public function getNagiosFilePath()
	{

		return $this->nagios_file_path;
	}

	
	public function getZipped()
	{

		return $this->zipped;
	}

	
	public function getGenerateType()
	{

		return $this->generate_type;
	}

	
	public function getNotes()
	{

		return $this->notes;
	}

	
	public function getXmlType()
	{

		return $this->xml_type;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ClientPeer::ID;
		}

	} 
	
	public function setClientName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->client_name !== $v) {
			$this->client_name = $v;
			$this->modifiedColumns[] = ClientPeer::CLIENT_NAME;
		}

	} 
	
	public function setUrl($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->url !== $v) {
			$this->url = $v;
			$this->modifiedColumns[] = ClientPeer::URL;
		}

	} 
	
	public function setConnectionType($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->connection_type !== $v) {
			$this->connection_type = $v;
			$this->modifiedColumns[] = ClientPeer::CONNECTION_TYPE;
		}

	} 
	
	public function setHomeDir($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->home_dir !== $v) {
			$this->home_dir = $v;
			$this->modifiedColumns[] = ClientPeer::HOME_DIR;
		}

	} 
	
	public function setUsername($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->username !== $v) {
			$this->username = $v;
			$this->modifiedColumns[] = ClientPeer::USERNAME;
		}

	} 
	
	public function setPassword($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->password !== $v) {
			$this->password = $v;
			$this->modifiedColumns[] = ClientPeer::PASSWORD;
		}

	} 
	
	public function setFtpUsername($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ftp_username !== $v) {
			$this->ftp_username = $v;
			$this->modifiedColumns[] = ClientPeer::FTP_USERNAME;
		}

	} 
	
	public function setFtpPassword($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ftp_password !== $v) {
			$this->ftp_password = $v;
			$this->modifiedColumns[] = ClientPeer::FTP_PASSWORD;
		}

	} 
	
	public function setDirLayout($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->dir_layout !== $v || $v === 'dates') {
			$this->dir_layout = $v;
			$this->modifiedColumns[] = ClientPeer::DIR_LAYOUT;
		}

	} 
	
	public function setIsActive($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->is_active !== $v || $v === 0) {
			$this->is_active = $v;
			$this->modifiedColumns[] = ClientPeer::IS_ACTIVE;
		}

	} 
	
	public function setLogoPath($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->logo_path !== $v) {
			$this->logo_path = $v;
			$this->modifiedColumns[] = ClientPeer::LOGO_PATH;
		}

	} 
	
	public function setFtpPort($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->ftp_port !== $v || $v === 21) {
			$this->ftp_port = $v;
			$this->modifiedColumns[] = ClientPeer::FTP_PORT;
		}

	} 
	
	public function setRemoteDir($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->remote_dir !== $v) {
			$this->remote_dir = $v;
			$this->modifiedColumns[] = ClientPeer::REMOTE_DIR;
		}

	} 
	
	public function setContract($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->contract !== $v || $v === 'No') {
			$this->contract = $v;
			$this->modifiedColumns[] = ClientPeer::CONTRACT;
		}

	} 
	
	public function setContractStartDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [contract_start_date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->contract_start_date !== $ts) {
			$this->contract_start_date = $ts;
			$this->modifiedColumns[] = ClientPeer::CONTRACT_START_DATE;
		}

	} 
	
	public function setContractEndDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [contract_end_date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->contract_end_date !== $ts) {
			$this->contract_end_date = $ts;
			$this->modifiedColumns[] = ClientPeer::CONTRACT_END_DATE;
		}

	} 
	
	public function setRenewalPeriod($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->renewal_period !== $v) {
			$this->renewal_period = $v;
			$this->modifiedColumns[] = ClientPeer::RENEWAL_PERIOD;
		}

	} 
	
	public function setRoyaltyRate($v)
	{

		if ($this->royalty_rate !== $v || $v === 0) {
			$this->royalty_rate = $v;
			$this->modifiedColumns[] = ClientPeer::ROYALTY_RATE;
		}

	} 
	
	public function setNagiosFilePath($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nagios_file_path !== $v) {
			$this->nagios_file_path = $v;
			$this->modifiedColumns[] = ClientPeer::NAGIOS_FILE_PATH;
		}

	} 
	
	public function setZipped($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->zipped !== $v || $v === 0) {
			$this->zipped = $v;
			$this->modifiedColumns[] = ClientPeer::ZIPPED;
		}

	} 
	
	public function setGenerateType($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->generate_type !== $v) {
			$this->generate_type = $v;
			$this->modifiedColumns[] = ClientPeer::GENERATE_TYPE;
		}

	} 
	
	public function setNotes($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->notes !== $v) {
			$this->notes = $v;
			$this->modifiedColumns[] = ClientPeer::NOTES;
		}

	} 
	
	public function setXmlType($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->xml_type !== $v || $v === 'newsml') {
			$this->xml_type = $v;
			$this->modifiedColumns[] = ClientPeer::XML_TYPE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->client_name = $rs->getString($startcol + 1);

			$this->url = $rs->getString($startcol + 2);

			$this->connection_type = $rs->getString($startcol + 3);

			$this->home_dir = $rs->getString($startcol + 4);

			$this->username = $rs->getString($startcol + 5);

			$this->password = $rs->getString($startcol + 6);

			$this->ftp_username = $rs->getString($startcol + 7);

			$this->ftp_password = $rs->getString($startcol + 8);

			$this->dir_layout = $rs->getString($startcol + 9);

			$this->is_active = $rs->getInt($startcol + 10);

			$this->logo_path = $rs->getString($startcol + 11);

			$this->ftp_port = $rs->getInt($startcol + 12);

			$this->remote_dir = $rs->getString($startcol + 13);

			$this->contract = $rs->getString($startcol + 14);

			$this->contract_start_date = $rs->getDate($startcol + 15, null);

			$this->contract_end_date = $rs->getDate($startcol + 16, null);

			$this->renewal_period = $rs->getString($startcol + 17);

			$this->royalty_rate = $rs->getFloat($startcol + 18);

			$this->nagios_file_path = $rs->getString($startcol + 19);

			$this->zipped = $rs->getInt($startcol + 20);

			$this->generate_type = $rs->getString($startcol + 21);

			$this->notes = $rs->getString($startcol + 22);

			$this->xml_type = $rs->getString($startcol + 23);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 24; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Client object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ClientPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ClientPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ClientPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ClientPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ClientPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collClientContactLinks !== null) {
				foreach($this->collClientContactLinks as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collClientRules !== null) {
				foreach($this->collClientRules as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = ClientPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collClientContactLinks !== null) {
					foreach($this->collClientContactLinks as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collClientRules !== null) {
					foreach($this->collClientRules as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ClientPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getClientName();
				break;
			case 2:
				return $this->getUrl();
				break;
			case 3:
				return $this->getConnectionType();
				break;
			case 4:
				return $this->getHomeDir();
				break;
			case 5:
				return $this->getUsername();
				break;
			case 6:
				return $this->getPassword();
				break;
			case 7:
				return $this->getFtpUsername();
				break;
			case 8:
				return $this->getFtpPassword();
				break;
			case 9:
				return $this->getDirLayout();
				break;
			case 10:
				return $this->getIsActive();
				break;
			case 11:
				return $this->getLogoPath();
				break;
			case 12:
				return $this->getFtpPort();
				break;
			case 13:
				return $this->getRemoteDir();
				break;
			case 14:
				return $this->getContract();
				break;
			case 15:
				return $this->getContractStartDate();
				break;
			case 16:
				return $this->getContractEndDate();
				break;
			case 17:
				return $this->getRenewalPeriod();
				break;
			case 18:
				return $this->getRoyaltyRate();
				break;
			case 19:
				return $this->getNagiosFilePath();
				break;
			case 20:
				return $this->getZipped();
				break;
			case 21:
				return $this->getGenerateType();
				break;
			case 22:
				return $this->getNotes();
				break;
			case 23:
				return $this->getXmlType();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ClientPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getClientName(),
			$keys[2] => $this->getUrl(),
			$keys[3] => $this->getConnectionType(),
			$keys[4] => $this->getHomeDir(),
			$keys[5] => $this->getUsername(),
			$keys[6] => $this->getPassword(),
			$keys[7] => $this->getFtpUsername(),
			$keys[8] => $this->getFtpPassword(),
			$keys[9] => $this->getDirLayout(),
			$keys[10] => $this->getIsActive(),
			$keys[11] => $this->getLogoPath(),
			$keys[12] => $this->getFtpPort(),
			$keys[13] => $this->getRemoteDir(),
			$keys[14] => $this->getContract(),
			$keys[15] => $this->getContractStartDate(),
			$keys[16] => $this->getContractEndDate(),
			$keys[17] => $this->getRenewalPeriod(),
			$keys[18] => $this->getRoyaltyRate(),
			$keys[19] => $this->getNagiosFilePath(),
			$keys[20] => $this->getZipped(),
			$keys[21] => $this->getGenerateType(),
			$keys[22] => $this->getNotes(),
			$keys[23] => $this->getXmlType(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ClientPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setClientName($value);
				break;
			case 2:
				$this->setUrl($value);
				break;
			case 3:
				$this->setConnectionType($value);
				break;
			case 4:
				$this->setHomeDir($value);
				break;
			case 5:
				$this->setUsername($value);
				break;
			case 6:
				$this->setPassword($value);
				break;
			case 7:
				$this->setFtpUsername($value);
				break;
			case 8:
				$this->setFtpPassword($value);
				break;
			case 9:
				$this->setDirLayout($value);
				break;
			case 10:
				$this->setIsActive($value);
				break;
			case 11:
				$this->setLogoPath($value);
				break;
			case 12:
				$this->setFtpPort($value);
				break;
			case 13:
				$this->setRemoteDir($value);
				break;
			case 14:
				$this->setContract($value);
				break;
			case 15:
				$this->setContractStartDate($value);
				break;
			case 16:
				$this->setContractEndDate($value);
				break;
			case 17:
				$this->setRenewalPeriod($value);
				break;
			case 18:
				$this->setRoyaltyRate($value);
				break;
			case 19:
				$this->setNagiosFilePath($value);
				break;
			case 20:
				$this->setZipped($value);
				break;
			case 21:
				$this->setGenerateType($value);
				break;
			case 22:
				$this->setNotes($value);
				break;
			case 23:
				$this->setXmlType($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ClientPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setClientName($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setUrl($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setConnectionType($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setHomeDir($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setUsername($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setPassword($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setFtpUsername($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setFtpPassword($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setDirLayout($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setIsActive($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setLogoPath($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setFtpPort($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setRemoteDir($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setContract($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setContractStartDate($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setContractEndDate($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setRenewalPeriod($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setRoyaltyRate($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setNagiosFilePath($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setZipped($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setGenerateType($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setNotes($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setXmlType($arr[$keys[23]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ClientPeer::DATABASE_NAME);

		if ($this->isColumnModified(ClientPeer::ID)) $criteria->add(ClientPeer::ID, $this->id);
		if ($this->isColumnModified(ClientPeer::CLIENT_NAME)) $criteria->add(ClientPeer::CLIENT_NAME, $this->client_name);
		if ($this->isColumnModified(ClientPeer::URL)) $criteria->add(ClientPeer::URL, $this->url);
		if ($this->isColumnModified(ClientPeer::CONNECTION_TYPE)) $criteria->add(ClientPeer::CONNECTION_TYPE, $this->connection_type);
		if ($this->isColumnModified(ClientPeer::HOME_DIR)) $criteria->add(ClientPeer::HOME_DIR, $this->home_dir);
		if ($this->isColumnModified(ClientPeer::USERNAME)) $criteria->add(ClientPeer::USERNAME, $this->username);
		if ($this->isColumnModified(ClientPeer::PASSWORD)) $criteria->add(ClientPeer::PASSWORD, $this->password);
		if ($this->isColumnModified(ClientPeer::FTP_USERNAME)) $criteria->add(ClientPeer::FTP_USERNAME, $this->ftp_username);
		if ($this->isColumnModified(ClientPeer::FTP_PASSWORD)) $criteria->add(ClientPeer::FTP_PASSWORD, $this->ftp_password);
		if ($this->isColumnModified(ClientPeer::DIR_LAYOUT)) $criteria->add(ClientPeer::DIR_LAYOUT, $this->dir_layout);
		if ($this->isColumnModified(ClientPeer::IS_ACTIVE)) $criteria->add(ClientPeer::IS_ACTIVE, $this->is_active);
		if ($this->isColumnModified(ClientPeer::LOGO_PATH)) $criteria->add(ClientPeer::LOGO_PATH, $this->logo_path);
		if ($this->isColumnModified(ClientPeer::FTP_PORT)) $criteria->add(ClientPeer::FTP_PORT, $this->ftp_port);
		if ($this->isColumnModified(ClientPeer::REMOTE_DIR)) $criteria->add(ClientPeer::REMOTE_DIR, $this->remote_dir);
		if ($this->isColumnModified(ClientPeer::CONTRACT)) $criteria->add(ClientPeer::CONTRACT, $this->contract);
		if ($this->isColumnModified(ClientPeer::CONTRACT_START_DATE)) $criteria->add(ClientPeer::CONTRACT_START_DATE, $this->contract_start_date);
		if ($this->isColumnModified(ClientPeer::CONTRACT_END_DATE)) $criteria->add(ClientPeer::CONTRACT_END_DATE, $this->contract_end_date);
		if ($this->isColumnModified(ClientPeer::RENEWAL_PERIOD)) $criteria->add(ClientPeer::RENEWAL_PERIOD, $this->renewal_period);
		if ($this->isColumnModified(ClientPeer::ROYALTY_RATE)) $criteria->add(ClientPeer::ROYALTY_RATE, $this->royalty_rate);
		if ($this->isColumnModified(ClientPeer::NAGIOS_FILE_PATH)) $criteria->add(ClientPeer::NAGIOS_FILE_PATH, $this->nagios_file_path);
		if ($this->isColumnModified(ClientPeer::ZIPPED)) $criteria->add(ClientPeer::ZIPPED, $this->zipped);
		if ($this->isColumnModified(ClientPeer::GENERATE_TYPE)) $criteria->add(ClientPeer::GENERATE_TYPE, $this->generate_type);
		if ($this->isColumnModified(ClientPeer::NOTES)) $criteria->add(ClientPeer::NOTES, $this->notes);
		if ($this->isColumnModified(ClientPeer::XML_TYPE)) $criteria->add(ClientPeer::XML_TYPE, $this->xml_type);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ClientPeer::DATABASE_NAME);

		$criteria->add(ClientPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setClientName($this->client_name);

		$copyObj->setUrl($this->url);

		$copyObj->setConnectionType($this->connection_type);

		$copyObj->setHomeDir($this->home_dir);

		$copyObj->setUsername($this->username);

		$copyObj->setPassword($this->password);

		$copyObj->setFtpUsername($this->ftp_username);

		$copyObj->setFtpPassword($this->ftp_password);

		$copyObj->setDirLayout($this->dir_layout);

		$copyObj->setIsActive($this->is_active);

		$copyObj->setLogoPath($this->logo_path);

		$copyObj->setFtpPort($this->ftp_port);

		$copyObj->setRemoteDir($this->remote_dir);

		$copyObj->setContract($this->contract);

		$copyObj->setContractStartDate($this->contract_start_date);

		$copyObj->setContractEndDate($this->contract_end_date);

		$copyObj->setRenewalPeriod($this->renewal_period);

		$copyObj->setRoyaltyRate($this->royalty_rate);

		$copyObj->setNagiosFilePath($this->nagios_file_path);

		$copyObj->setZipped($this->zipped);

		$copyObj->setGenerateType($this->generate_type);

		$copyObj->setNotes($this->notes);

		$copyObj->setXmlType($this->xml_type);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getClientContactLinks() as $relObj) {
				$copyObj->addClientContactLink($relObj->copy($deepCopy));
			}

			foreach($this->getClientRules() as $relObj) {
				$copyObj->addClientRule($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ClientPeer();
		}
		return self::$peer;
	}

	
	public function initClientContactLinks()
	{
		if ($this->collClientContactLinks === null) {
			$this->collClientContactLinks = array();
		}
	}

	
	public function getClientContactLinks($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseClientContactLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collClientContactLinks === null) {
			if ($this->isNew()) {
			   $this->collClientContactLinks = array();
			} else {

				$criteria->add(ClientContactLinkPeer::CLIENT_ID, $this->getId());

				ClientContactLinkPeer::addSelectColumns($criteria);
				$this->collClientContactLinks = ClientContactLinkPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ClientContactLinkPeer::CLIENT_ID, $this->getId());

				ClientContactLinkPeer::addSelectColumns($criteria);
				if (!isset($this->lastClientContactLinkCriteria) || !$this->lastClientContactLinkCriteria->equals($criteria)) {
					$this->collClientContactLinks = ClientContactLinkPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastClientContactLinkCriteria = $criteria;
		return $this->collClientContactLinks;
	}

	
	public function countClientContactLinks($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseClientContactLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ClientContactLinkPeer::CLIENT_ID, $this->getId());

		return ClientContactLinkPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addClientContactLink(ClientContactLink $l)
	{
		$this->collClientContactLinks[] = $l;
		$l->setClient($this);
	}


	
	public function getClientContactLinksJoinContact($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseClientContactLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collClientContactLinks === null) {
			if ($this->isNew()) {
				$this->collClientContactLinks = array();
			} else {

				$criteria->add(ClientContactLinkPeer::CLIENT_ID, $this->getId());

				$this->collClientContactLinks = ClientContactLinkPeer::doSelectJoinContact($criteria, $con);
			}
		} else {
									
			$criteria->add(ClientContactLinkPeer::CLIENT_ID, $this->getId());

			if (!isset($this->lastClientContactLinkCriteria) || !$this->lastClientContactLinkCriteria->equals($criteria)) {
				$this->collClientContactLinks = ClientContactLinkPeer::doSelectJoinContact($criteria, $con);
			}
		}
		$this->lastClientContactLinkCriteria = $criteria;

		return $this->collClientContactLinks;
	}

	
	public function initClientRules()
	{
		if ($this->collClientRules === null) {
			$this->collClientRules = array();
		}
	}

	
	public function getClientRules($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseClientRulePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collClientRules === null) {
			if ($this->isNew()) {
			   $this->collClientRules = array();
			} else {

				$criteria->add(ClientRulePeer::CLIENT_ID, $this->getId());

				ClientRulePeer::addSelectColumns($criteria);
				$this->collClientRules = ClientRulePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ClientRulePeer::CLIENT_ID, $this->getId());

				ClientRulePeer::addSelectColumns($criteria);
				if (!isset($this->lastClientRuleCriteria) || !$this->lastClientRuleCriteria->equals($criteria)) {
					$this->collClientRules = ClientRulePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastClientRuleCriteria = $criteria;
		return $this->collClientRules;
	}

	
	public function countClientRules($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseClientRulePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ClientRulePeer::CLIENT_ID, $this->getId());

		return ClientRulePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addClientRule(ClientRule $l)
	{
		$this->collClientRules[] = $l;
		$l->setClient($this);
	}

} 