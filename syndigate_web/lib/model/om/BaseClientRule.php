<?php


abstract class BaseClientRule extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $client_id;

	
	protected $aClient;

	
	protected $collClientRuleFilters;

	
	protected $lastClientRuleFilterCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getClientId()
	{

		return $this->client_id;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ClientRulePeer::ID;
		}

	} 
	
	public function setClientId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->client_id !== $v) {
			$this->client_id = $v;
			$this->modifiedColumns[] = ClientRulePeer::CLIENT_ID;
		}

		if ($this->aClient !== null && $this->aClient->getId() !== $v) {
			$this->aClient = null;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->client_id = $rs->getInt($startcol + 1);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 2; 
		} catch (Exception $e) {
			throw new PropelException("Error populating ClientRule object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ClientRulePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ClientRulePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ClientRulePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aClient !== null) {
				if ($this->aClient->isModified()) {
					$affectedRows += $this->aClient->save($con);
				}
				$this->setClient($this->aClient);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ClientRulePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ClientRulePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collClientRuleFilters !== null) {
				foreach($this->collClientRuleFilters as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aClient !== null) {
				if (!$this->aClient->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aClient->getValidationFailures());
				}
			}


			if (($retval = ClientRulePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collClientRuleFilters !== null) {
					foreach($this->collClientRuleFilters as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ClientRulePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getClientId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ClientRulePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getClientId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ClientRulePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setClientId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ClientRulePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setClientId($arr[$keys[1]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ClientRulePeer::DATABASE_NAME);

		if ($this->isColumnModified(ClientRulePeer::ID)) $criteria->add(ClientRulePeer::ID, $this->id);
		if ($this->isColumnModified(ClientRulePeer::CLIENT_ID)) $criteria->add(ClientRulePeer::CLIENT_ID, $this->client_id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ClientRulePeer::DATABASE_NAME);

		$criteria->add(ClientRulePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setClientId($this->client_id);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getClientRuleFilters() as $relObj) {
				$copyObj->addClientRuleFilter($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ClientRulePeer();
		}
		return self::$peer;
	}

	
	public function setClient($v)
	{


		if ($v === null) {
			$this->setClientId(NULL);
		} else {
			$this->setClientId($v->getId());
		}


		$this->aClient = $v;
	}


	
	public function getClient($con = null)
	{
		if ($this->aClient === null && ($this->client_id !== null)) {
						include_once 'lib/model/om/BaseClientPeer.php';

			$this->aClient = ClientPeer::retrieveByPK($this->client_id, $con);

			
		}
		return $this->aClient;
	}

	
	public function initClientRuleFilters()
	{
		if ($this->collClientRuleFilters === null) {
			$this->collClientRuleFilters = array();
		}
	}

	
	public function getClientRuleFilters($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseClientRuleFilterPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collClientRuleFilters === null) {
			if ($this->isNew()) {
			   $this->collClientRuleFilters = array();
			} else {

				$criteria->add(ClientRuleFilterPeer::CLIENT_RULE_ID, $this->getId());

				ClientRuleFilterPeer::addSelectColumns($criteria);
				$this->collClientRuleFilters = ClientRuleFilterPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ClientRuleFilterPeer::CLIENT_RULE_ID, $this->getId());

				ClientRuleFilterPeer::addSelectColumns($criteria);
				if (!isset($this->lastClientRuleFilterCriteria) || !$this->lastClientRuleFilterCriteria->equals($criteria)) {
					$this->collClientRuleFilters = ClientRuleFilterPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastClientRuleFilterCriteria = $criteria;
		return $this->collClientRuleFilters;
	}

	
	public function countClientRuleFilters($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseClientRuleFilterPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ClientRuleFilterPeer::CLIENT_RULE_ID, $this->getId());

		return ClientRuleFilterPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addClientRuleFilter(ClientRuleFilter $l)
	{
		$this->collClientRuleFilters[] = $l;
		$l->setClientRule($this);
	}

} 