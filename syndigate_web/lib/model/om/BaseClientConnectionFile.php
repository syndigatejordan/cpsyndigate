<?php


abstract class BaseClientConnectionFile extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $client_id = 0;


	
	protected $client_connection_id = 0;


	
	protected $title_id = 0;


	
	protected $revision = 0;


	
	protected $local_file = '';


	
	protected $is_sent = 0;


	
	protected $num_retry = 0;


	
	protected $err_msg = '';


	
	protected $report_id = 0;


	
	protected $creation_time = 0;


	
	protected $sent_time = 0;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getClientId()
	{

		return $this->client_id;
	}

	
	public function getClientConnectionId()
	{

		return $this->client_connection_id;
	}

	
	public function getTitleId()
	{

		return $this->title_id;
	}

	
	public function getRevision()
	{

		return $this->revision;
	}

	
	public function getLocalFile()
	{

		return $this->local_file;
	}

	
	public function getIsSent()
	{

		return $this->is_sent;
	}

	
	public function getNumRetry()
	{

		return $this->num_retry;
	}

	
	public function getErrMsg()
	{

		return $this->err_msg;
	}

	
	public function getReportId()
	{

		return $this->report_id;
	}

	
	public function getCreationTime()
	{

		return $this->creation_time;
	}

	
	public function getSentTime()
	{

		return $this->sent_time;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ClientConnectionFilePeer::ID;
		}

	} 
	
	public function setClientId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->client_id !== $v || $v === 0) {
			$this->client_id = $v;
			$this->modifiedColumns[] = ClientConnectionFilePeer::CLIENT_ID;
		}

	} 
	
	public function setClientConnectionId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->client_connection_id !== $v || $v === 0) {
			$this->client_connection_id = $v;
			$this->modifiedColumns[] = ClientConnectionFilePeer::CLIENT_CONNECTION_ID;
		}

	} 
	
	public function setTitleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_id !== $v || $v === 0) {
			$this->title_id = $v;
			$this->modifiedColumns[] = ClientConnectionFilePeer::TITLE_ID;
		}

	} 
	
	public function setRevision($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->revision !== $v || $v === 0) {
			$this->revision = $v;
			$this->modifiedColumns[] = ClientConnectionFilePeer::REVISION;
		}

	} 
	
	public function setLocalFile($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->local_file !== $v || $v === '') {
			$this->local_file = $v;
			$this->modifiedColumns[] = ClientConnectionFilePeer::LOCAL_FILE;
		}

	} 
	
	public function setIsSent($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->is_sent !== $v || $v === 0) {
			$this->is_sent = $v;
			$this->modifiedColumns[] = ClientConnectionFilePeer::IS_SENT;
		}

	} 
	
	public function setNumRetry($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->num_retry !== $v || $v === 0) {
			$this->num_retry = $v;
			$this->modifiedColumns[] = ClientConnectionFilePeer::NUM_RETRY;
		}

	} 
	
	public function setErrMsg($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->err_msg !== $v || $v === '') {
			$this->err_msg = $v;
			$this->modifiedColumns[] = ClientConnectionFilePeer::ERR_MSG;
		}

	} 
	
	public function setReportId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->report_id !== $v || $v === 0) {
			$this->report_id = $v;
			$this->modifiedColumns[] = ClientConnectionFilePeer::REPORT_ID;
		}

	} 
	
	public function setCreationTime($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->creation_time !== $v || $v === 0) {
			$this->creation_time = $v;
			$this->modifiedColumns[] = ClientConnectionFilePeer::CREATION_TIME;
		}

	} 
	
	public function setSentTime($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->sent_time !== $v || $v === 0) {
			$this->sent_time = $v;
			$this->modifiedColumns[] = ClientConnectionFilePeer::SENT_TIME;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->client_id = $rs->getInt($startcol + 1);

			$this->client_connection_id = $rs->getInt($startcol + 2);

			$this->title_id = $rs->getInt($startcol + 3);

			$this->revision = $rs->getInt($startcol + 4);

			$this->local_file = $rs->getString($startcol + 5);

			$this->is_sent = $rs->getInt($startcol + 6);

			$this->num_retry = $rs->getInt($startcol + 7);

			$this->err_msg = $rs->getString($startcol + 8);

			$this->report_id = $rs->getInt($startcol + 9);

			$this->creation_time = $rs->getInt($startcol + 10);

			$this->sent_time = $rs->getInt($startcol + 11);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 12; 
		} catch (Exception $e) {
			throw new PropelException("Error populating ClientConnectionFile object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ClientConnectionFilePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ClientConnectionFilePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ClientConnectionFilePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ClientConnectionFilePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ClientConnectionFilePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = ClientConnectionFilePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ClientConnectionFilePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getClientId();
				break;
			case 2:
				return $this->getClientConnectionId();
				break;
			case 3:
				return $this->getTitleId();
				break;
			case 4:
				return $this->getRevision();
				break;
			case 5:
				return $this->getLocalFile();
				break;
			case 6:
				return $this->getIsSent();
				break;
			case 7:
				return $this->getNumRetry();
				break;
			case 8:
				return $this->getErrMsg();
				break;
			case 9:
				return $this->getReportId();
				break;
			case 10:
				return $this->getCreationTime();
				break;
			case 11:
				return $this->getSentTime();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ClientConnectionFilePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getClientId(),
			$keys[2] => $this->getClientConnectionId(),
			$keys[3] => $this->getTitleId(),
			$keys[4] => $this->getRevision(),
			$keys[5] => $this->getLocalFile(),
			$keys[6] => $this->getIsSent(),
			$keys[7] => $this->getNumRetry(),
			$keys[8] => $this->getErrMsg(),
			$keys[9] => $this->getReportId(),
			$keys[10] => $this->getCreationTime(),
			$keys[11] => $this->getSentTime(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ClientConnectionFilePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setClientId($value);
				break;
			case 2:
				$this->setClientConnectionId($value);
				break;
			case 3:
				$this->setTitleId($value);
				break;
			case 4:
				$this->setRevision($value);
				break;
			case 5:
				$this->setLocalFile($value);
				break;
			case 6:
				$this->setIsSent($value);
				break;
			case 7:
				$this->setNumRetry($value);
				break;
			case 8:
				$this->setErrMsg($value);
				break;
			case 9:
				$this->setReportId($value);
				break;
			case 10:
				$this->setCreationTime($value);
				break;
			case 11:
				$this->setSentTime($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ClientConnectionFilePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setClientId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setClientConnectionId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setTitleId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setRevision($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setLocalFile($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setIsSent($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setNumRetry($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setErrMsg($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setReportId($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setCreationTime($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setSentTime($arr[$keys[11]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ClientConnectionFilePeer::DATABASE_NAME);

		if ($this->isColumnModified(ClientConnectionFilePeer::ID)) $criteria->add(ClientConnectionFilePeer::ID, $this->id);
		if ($this->isColumnModified(ClientConnectionFilePeer::CLIENT_ID)) $criteria->add(ClientConnectionFilePeer::CLIENT_ID, $this->client_id);
		if ($this->isColumnModified(ClientConnectionFilePeer::CLIENT_CONNECTION_ID)) $criteria->add(ClientConnectionFilePeer::CLIENT_CONNECTION_ID, $this->client_connection_id);
		if ($this->isColumnModified(ClientConnectionFilePeer::TITLE_ID)) $criteria->add(ClientConnectionFilePeer::TITLE_ID, $this->title_id);
		if ($this->isColumnModified(ClientConnectionFilePeer::REVISION)) $criteria->add(ClientConnectionFilePeer::REVISION, $this->revision);
		if ($this->isColumnModified(ClientConnectionFilePeer::LOCAL_FILE)) $criteria->add(ClientConnectionFilePeer::LOCAL_FILE, $this->local_file);
		if ($this->isColumnModified(ClientConnectionFilePeer::IS_SENT)) $criteria->add(ClientConnectionFilePeer::IS_SENT, $this->is_sent);
		if ($this->isColumnModified(ClientConnectionFilePeer::NUM_RETRY)) $criteria->add(ClientConnectionFilePeer::NUM_RETRY, $this->num_retry);
		if ($this->isColumnModified(ClientConnectionFilePeer::ERR_MSG)) $criteria->add(ClientConnectionFilePeer::ERR_MSG, $this->err_msg);
		if ($this->isColumnModified(ClientConnectionFilePeer::REPORT_ID)) $criteria->add(ClientConnectionFilePeer::REPORT_ID, $this->report_id);
		if ($this->isColumnModified(ClientConnectionFilePeer::CREATION_TIME)) $criteria->add(ClientConnectionFilePeer::CREATION_TIME, $this->creation_time);
		if ($this->isColumnModified(ClientConnectionFilePeer::SENT_TIME)) $criteria->add(ClientConnectionFilePeer::SENT_TIME, $this->sent_time);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ClientConnectionFilePeer::DATABASE_NAME);

		$criteria->add(ClientConnectionFilePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setClientId($this->client_id);

		$copyObj->setClientConnectionId($this->client_connection_id);

		$copyObj->setTitleId($this->title_id);

		$copyObj->setRevision($this->revision);

		$copyObj->setLocalFile($this->local_file);

		$copyObj->setIsSent($this->is_sent);

		$copyObj->setNumRetry($this->num_retry);

		$copyObj->setErrMsg($this->err_msg);

		$copyObj->setReportId($this->report_id);

		$copyObj->setCreationTime($this->creation_time);

		$copyObj->setSentTime($this->sent_time);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ClientConnectionFilePeer();
		}
		return self::$peer;
	}

} 