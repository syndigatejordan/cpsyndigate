<?php


abstract class BaseTitle extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $title_frequency_id;


	
	protected $title_type_id;


	
	protected $publisher_id;


	
	protected $name;


	
	protected $website;


	
	protected $language_id;


	
	protected $ftp_username;


	
	protected $ftp_password;


	
	protected $articles_threshold;


	
	protected $unix_uid;


	
	protected $unix_gid;


	
	protected $nagios_config_file;


	
	protected $home_dir;


	
	protected $work_dir;


	
	protected $svn_repo;


	
	protected $gather_retries;


	
	protected $default_charset;


	
	protected $parse_dir;


	
	protected $next_try_timer;


	
	protected $copyright;


	
	protected $copyright_arabic = '';


	
	protected $is_active = 0;


	
	protected $activation_date;


	
	protected $logo_path;


	
	protected $directory_layout = 'issues';


	
	protected $is_newswire = 0;


	
	protected $is_generate;


	
	protected $country_id;


	
	protected $photo_feed = 'No';


	
	protected $contract = 'No';


	
	protected $has_technical_intro = 0;


	
	protected $is_receiving = 0;


	
	protected $is_parsing = 0;


	
	protected $technical_notes;


	
	protected $timeliness;


	
	protected $contract_start_date;


	
	protected $contract_end_date;


	
	protected $royalty_rate = 0;


	
	protected $source_description;


	
	protected $renewal_period;


	
	protected $city = '';


	
	protected $story_count = 0;


	
	protected $receiving_status = 0;


	
	protected $receiving_status_comment;


	
	protected $receiving_status_timestamp;


	
	protected $notes;


	
	protected $has_new_parsed_content = 0;


	
	protected $last_content_timestamp = -62169984000;


	
	protected $cached_articles_today = 0;


	
	protected $cached_current_span = 0;


	
	protected $cached_prev_span = 0;


	
	protected $cached_articles_count = 0;


	
	protected $admins_id = 0;


	
	protected $rank = 1;

	
	protected $aPublisher;

	
	protected $aTitleType;

	
	protected $aTitleFrequency;

	
	protected $aCountry;

	
	protected $aLanguage;

	
	protected $aAdmins;

	
	protected $collArticles;

	
	protected $lastArticleCriteria = null;

	
	protected $collOriginalArticleCategorys;

	
	protected $lastOriginalArticleCategoryCriteria = null;

	
	protected $collParsingStates;

	
	protected $lastParsingStateCriteria = null;

	
	protected $collReportGathers;

	
	protected $lastReportGatherCriteria = null;

	
	protected $collReportParses;

	
	protected $lastReportParseCriteria = null;

	
	protected $collSchedules;

	
	protected $lastScheduleCriteria = null;

	
	protected $collTitleConnections;

	
	protected $lastTitleConnectionCriteria = null;

	
	protected $collTitleContactLinks;

	
	protected $lastTitleContactLinkCriteria = null;

	
	protected $collTitleOfficeLinks;

	
	protected $lastTitleOfficeLinkCriteria = null;

	
	protected $collOrdersFrequencys;

	
	protected $lastOrdersFrequencyCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getTitleFrequencyId()
	{

		return $this->title_frequency_id;
	}

	
	public function getTitleTypeId()
	{

		return $this->title_type_id;
	}

	
	public function getPublisherId()
	{

		return $this->publisher_id;
	}

	
	public function getName()
	{

		return $this->name;
	}

	
	public function getWebsite()
	{

		return $this->website;
	}

	
	public function getLanguageId()
	{

		return $this->language_id;
	}

	
	public function getFtpUsername()
	{

		return $this->ftp_username;
	}

	
	public function getFtpPassword()
	{

		return $this->ftp_password;
	}

	
	public function getArticlesThreshold()
	{

		return $this->articles_threshold;
	}

	
	public function getUnixUid()
	{

		return $this->unix_uid;
	}

	
	public function getUnixGid()
	{

		return $this->unix_gid;
	}

	
	public function getNagiosConfigFile()
	{

		return $this->nagios_config_file;
	}

	
	public function getHomeDir()
	{

		return $this->home_dir;
	}

	
	public function getWorkDir()
	{

		return $this->work_dir;
	}

	
	public function getSvnRepo()
	{

		return $this->svn_repo;
	}

	
	public function getGatherRetries()
	{

		return $this->gather_retries;
	}

	
	public function getDefaultCharset()
	{

		return $this->default_charset;
	}

	
	public function getParseDir()
	{

		return $this->parse_dir;
	}

	
	public function getNextTryTimer()
	{

		return $this->next_try_timer;
	}

	
	public function getCopyright()
	{

		return $this->copyright;
	}

	
	public function getCopyrightArabic()
	{

		return $this->copyright_arabic;
	}

	
	public function getIsActive()
	{

		return $this->is_active;
	}

	
	public function getActivationDate($format = 'Y-m-d')
	{

		if ($this->activation_date === null || $this->activation_date === '') {
			return null;
		} elseif (!is_int($this->activation_date)) {
						$ts = strtotime($this->activation_date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [activation_date] as date/time value: " . var_export($this->activation_date, true));
			}
		} else {
			$ts = $this->activation_date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getLogoPath()
	{

		return $this->logo_path;
	}

	
	public function getDirectoryLayout()
	{

		return $this->directory_layout;
	}

	
	public function getIsNewswire()
	{

		return $this->is_newswire;
	}

	
	public function getIsGenerate()
	{

		return $this->is_generate;
	}

	
	public function getCountryId()
	{

		return $this->country_id;
	}

	
	public function getPhotoFeed()
	{

		return $this->photo_feed;
	}

	
	public function getContract()
	{

		return $this->contract;
	}

	
	public function getHasTechnicalIntro()
	{

		return $this->has_technical_intro;
	}

	
	public function getIsReceiving()
	{

		return $this->is_receiving;
	}

	
	public function getIsParsing()
	{

		return $this->is_parsing;
	}

	
	public function getTechnicalNotes()
	{

		return $this->technical_notes;
	}

	
	public function getTimeliness()
	{

		return $this->timeliness;
	}

	
	public function getContractStartDate($format = 'Y-m-d')
	{

		if ($this->contract_start_date === null || $this->contract_start_date === '') {
			return null;
		} elseif (!is_int($this->contract_start_date)) {
						$ts = strtotime($this->contract_start_date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [contract_start_date] as date/time value: " . var_export($this->contract_start_date, true));
			}
		} else {
			$ts = $this->contract_start_date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getContractEndDate($format = 'Y-m-d')
	{

		if ($this->contract_end_date === null || $this->contract_end_date === '') {
			return null;
		} elseif (!is_int($this->contract_end_date)) {
						$ts = strtotime($this->contract_end_date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [contract_end_date] as date/time value: " . var_export($this->contract_end_date, true));
			}
		} else {
			$ts = $this->contract_end_date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getRoyaltyRate()
	{

		return $this->royalty_rate;
	}

	
	public function getSourceDescription()
	{

		return $this->source_description;
	}

	
	public function getRenewalPeriod()
	{

		return $this->renewal_period;
	}

	
	public function getCity()
	{

		return $this->city;
	}

	
	public function getStoryCount()
	{

		return $this->story_count;
	}

	
	public function getReceivingStatus()
	{

		return $this->receiving_status;
	}

	
	public function getReceivingStatusComment()
	{

		return $this->receiving_status_comment;
	}

	
	public function getReceivingStatusTimestamp($format = 'Y-m-d H:i:s')
	{

		if ($this->receiving_status_timestamp === null || $this->receiving_status_timestamp === '') {
			return null;
		} elseif (!is_int($this->receiving_status_timestamp)) {
						$ts = strtotime($this->receiving_status_timestamp);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [receiving_status_timestamp] as date/time value: " . var_export($this->receiving_status_timestamp, true));
			}
		} else {
			$ts = $this->receiving_status_timestamp;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getNotes()
	{

		return $this->notes;
	}

	
	public function getHasNewParsedContent()
	{

		return $this->has_new_parsed_content;
	}

	
	public function getLastContentTimestamp($format = 'Y-m-d H:i:s')
	{

		if ($this->last_content_timestamp === null || $this->last_content_timestamp === '') {
			return null;
		} elseif (!is_int($this->last_content_timestamp)) {
						$ts = strtotime($this->last_content_timestamp);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [last_content_timestamp] as date/time value: " . var_export($this->last_content_timestamp, true));
			}
		} else {
			$ts = $this->last_content_timestamp;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getCachedArticlesToday()
	{

		return $this->cached_articles_today;
	}

	
	public function getCachedCurrentSpan()
	{

		return $this->cached_current_span;
	}

	
	public function getCachedPrevSpan()
	{

		return $this->cached_prev_span;
	}

	
	public function getCachedArticlesCount()
	{

		return $this->cached_articles_count;
	}

	
	public function getAdminsId()
	{

		return $this->admins_id;
	}

	
	public function getRank()
	{

		return $this->rank;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = TitlePeer::ID;
		}

	} 
	
	public function setTitleFrequencyId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_frequency_id !== $v) {
			$this->title_frequency_id = $v;
			$this->modifiedColumns[] = TitlePeer::TITLE_FREQUENCY_ID;
		}

		if ($this->aTitleFrequency !== null && $this->aTitleFrequency->getId() !== $v) {
			$this->aTitleFrequency = null;
		}

	} 
	
	public function setTitleTypeId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_type_id !== $v) {
			$this->title_type_id = $v;
			$this->modifiedColumns[] = TitlePeer::TITLE_TYPE_ID;
		}

		if ($this->aTitleType !== null && $this->aTitleType->getId() !== $v) {
			$this->aTitleType = null;
		}

	} 
	
	public function setPublisherId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->publisher_id !== $v) {
			$this->publisher_id = $v;
			$this->modifiedColumns[] = TitlePeer::PUBLISHER_ID;
		}

		if ($this->aPublisher !== null && $this->aPublisher->getId() !== $v) {
			$this->aPublisher = null;
		}

	} 
	
	public function setName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->name !== $v) {
			$this->name = $v;
			$this->modifiedColumns[] = TitlePeer::NAME;
		}

	} 
	
	public function setWebsite($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->website !== $v) {
			$this->website = $v;
			$this->modifiedColumns[] = TitlePeer::WEBSITE;
		}

	} 
	
	public function setLanguageId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->language_id !== $v) {
			$this->language_id = $v;
			$this->modifiedColumns[] = TitlePeer::LANGUAGE_ID;
		}

		if ($this->aLanguage !== null && $this->aLanguage->getId() !== $v) {
			$this->aLanguage = null;
		}

	} 
	
	public function setFtpUsername($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ftp_username !== $v) {
			$this->ftp_username = $v;
			$this->modifiedColumns[] = TitlePeer::FTP_USERNAME;
		}

	} 
	
	public function setFtpPassword($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ftp_password !== $v) {
			$this->ftp_password = $v;
			$this->modifiedColumns[] = TitlePeer::FTP_PASSWORD;
		}

	} 
	
	public function setArticlesThreshold($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->articles_threshold !== $v) {
			$this->articles_threshold = $v;
			$this->modifiedColumns[] = TitlePeer::ARTICLES_THRESHOLD;
		}

	} 
	
	public function setUnixUid($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->unix_uid !== $v) {
			$this->unix_uid = $v;
			$this->modifiedColumns[] = TitlePeer::UNIX_UID;
		}

	} 
	
	public function setUnixGid($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->unix_gid !== $v) {
			$this->unix_gid = $v;
			$this->modifiedColumns[] = TitlePeer::UNIX_GID;
		}

	} 
	
	public function setNagiosConfigFile($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nagios_config_file !== $v) {
			$this->nagios_config_file = $v;
			$this->modifiedColumns[] = TitlePeer::NAGIOS_CONFIG_FILE;
		}

	} 
	
	public function setHomeDir($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->home_dir !== $v) {
			$this->home_dir = $v;
			$this->modifiedColumns[] = TitlePeer::HOME_DIR;
		}

	} 
	
	public function setWorkDir($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->work_dir !== $v) {
			$this->work_dir = $v;
			$this->modifiedColumns[] = TitlePeer::WORK_DIR;
		}

	} 
	
	public function setSvnRepo($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->svn_repo !== $v) {
			$this->svn_repo = $v;
			$this->modifiedColumns[] = TitlePeer::SVN_REPO;
		}

	} 
	
	public function setGatherRetries($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->gather_retries !== $v) {
			$this->gather_retries = $v;
			$this->modifiedColumns[] = TitlePeer::GATHER_RETRIES;
		}

	} 
	
	public function setDefaultCharset($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->default_charset !== $v) {
			$this->default_charset = $v;
			$this->modifiedColumns[] = TitlePeer::DEFAULT_CHARSET;
		}

	} 
	
	public function setParseDir($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->parse_dir !== $v) {
			$this->parse_dir = $v;
			$this->modifiedColumns[] = TitlePeer::PARSE_DIR;
		}

	} 
	
	public function setNextTryTimer($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->next_try_timer !== $v) {
			$this->next_try_timer = $v;
			$this->modifiedColumns[] = TitlePeer::NEXT_TRY_TIMER;
		}

	} 
	
	public function setCopyright($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->copyright !== $v) {
			$this->copyright = $v;
			$this->modifiedColumns[] = TitlePeer::COPYRIGHT;
		}

	} 
	
	public function setCopyrightArabic($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->copyright_arabic !== $v || $v === '') {
			$this->copyright_arabic = $v;
			$this->modifiedColumns[] = TitlePeer::COPYRIGHT_ARABIC;
		}

	} 
	
	public function setIsActive($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->is_active !== $v || $v === 0) {
			$this->is_active = $v;
			$this->modifiedColumns[] = TitlePeer::IS_ACTIVE;
		}

	} 
	
	public function setActivationDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [activation_date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->activation_date !== $ts) {
			$this->activation_date = $ts;
			$this->modifiedColumns[] = TitlePeer::ACTIVATION_DATE;
		}

	} 
	
	public function setLogoPath($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->logo_path !== $v) {
			$this->logo_path = $v;
			$this->modifiedColumns[] = TitlePeer::LOGO_PATH;
		}

	} 
	
	public function setDirectoryLayout($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->directory_layout !== $v || $v === 'issues') {
			$this->directory_layout = $v;
			$this->modifiedColumns[] = TitlePeer::DIRECTORY_LAYOUT;
		}

	} 
	
	public function setIsNewswire($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->is_newswire !== $v || $v === 0) {
			$this->is_newswire = $v;
			$this->modifiedColumns[] = TitlePeer::IS_NEWSWIRE;
		}

	} 
	
	public function setIsGenerate($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->is_generate !== $v) {
			$this->is_generate = $v;
			$this->modifiedColumns[] = TitlePeer::IS_GENERATE;
		}

	} 
	
	public function setCountryId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->country_id !== $v) {
			$this->country_id = $v;
			$this->modifiedColumns[] = TitlePeer::COUNTRY_ID;
		}

		if ($this->aCountry !== null && $this->aCountry->getId() !== $v) {
			$this->aCountry = null;
		}

	} 
	
	public function setPhotoFeed($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->photo_feed !== $v || $v === 'No') {
			$this->photo_feed = $v;
			$this->modifiedColumns[] = TitlePeer::PHOTO_FEED;
		}

	} 
	
	public function setContract($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->contract !== $v || $v === 'No') {
			$this->contract = $v;
			$this->modifiedColumns[] = TitlePeer::CONTRACT;
		}

	} 
	
	public function setHasTechnicalIntro($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->has_technical_intro !== $v || $v === 0) {
			$this->has_technical_intro = $v;
			$this->modifiedColumns[] = TitlePeer::HAS_TECHNICAL_INTRO;
		}

	} 
	
	public function setIsReceiving($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->is_receiving !== $v || $v === 0) {
			$this->is_receiving = $v;
			$this->modifiedColumns[] = TitlePeer::IS_RECEIVING;
		}

	} 
	
	public function setIsParsing($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->is_parsing !== $v || $v === 0) {
			$this->is_parsing = $v;
			$this->modifiedColumns[] = TitlePeer::IS_PARSING;
		}

	} 
	
	public function setTechnicalNotes($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->technical_notes !== $v) {
			$this->technical_notes = $v;
			$this->modifiedColumns[] = TitlePeer::TECHNICAL_NOTES;
		}

	} 
	
	public function setTimeliness($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->timeliness !== $v) {
			$this->timeliness = $v;
			$this->modifiedColumns[] = TitlePeer::TIMELINESS;
		}

	} 
	
	public function setContractStartDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [contract_start_date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->contract_start_date !== $ts) {
			$this->contract_start_date = $ts;
			$this->modifiedColumns[] = TitlePeer::CONTRACT_START_DATE;
		}

	} 
	
	public function setContractEndDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [contract_end_date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->contract_end_date !== $ts) {
			$this->contract_end_date = $ts;
			$this->modifiedColumns[] = TitlePeer::CONTRACT_END_DATE;
		}

	} 
	
	public function setRoyaltyRate($v)
	{

		if ($this->royalty_rate !== $v || $v === 0) {
			$this->royalty_rate = $v;
			$this->modifiedColumns[] = TitlePeer::ROYALTY_RATE;
		}

	} 
	
	public function setSourceDescription($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->source_description !== $v) {
			$this->source_description = $v;
			$this->modifiedColumns[] = TitlePeer::SOURCE_DESCRIPTION;
		}

	} 
	
	public function setRenewalPeriod($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->renewal_period !== $v) {
			$this->renewal_period = $v;
			$this->modifiedColumns[] = TitlePeer::RENEWAL_PERIOD;
		}

	} 
	
	public function setCity($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->city !== $v || $v === '') {
			$this->city = $v;
			$this->modifiedColumns[] = TitlePeer::CITY;
		}

	} 
	
	public function setStoryCount($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->story_count !== $v || $v === 0) {
			$this->story_count = $v;
			$this->modifiedColumns[] = TitlePeer::STORY_COUNT;
		}

	} 
	
	public function setReceivingStatus($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->receiving_status !== $v || $v === 0) {
			$this->receiving_status = $v;
			$this->modifiedColumns[] = TitlePeer::RECEIVING_STATUS;
		}

	} 
	
	public function setReceivingStatusComment($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->receiving_status_comment !== $v) {
			$this->receiving_status_comment = $v;
			$this->modifiedColumns[] = TitlePeer::RECEIVING_STATUS_COMMENT;
		}

	} 
	
	public function setReceivingStatusTimestamp($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [receiving_status_timestamp] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->receiving_status_timestamp !== $ts) {
			$this->receiving_status_timestamp = $ts;
			$this->modifiedColumns[] = TitlePeer::RECEIVING_STATUS_TIMESTAMP;
		}

	} 
	
	public function setNotes($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->notes !== $v) {
			$this->notes = $v;
			$this->modifiedColumns[] = TitlePeer::NOTES;
		}

	} 
	
	public function setHasNewParsedContent($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->has_new_parsed_content !== $v || $v === 0) {
			$this->has_new_parsed_content = $v;
			$this->modifiedColumns[] = TitlePeer::HAS_NEW_PARSED_CONTENT;
		}

	} 
	
	public function setLastContentTimestamp($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [last_content_timestamp] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->last_content_timestamp !== $ts || $ts === -62169984000) {
			$this->last_content_timestamp = $ts;
			$this->modifiedColumns[] = TitlePeer::LAST_CONTENT_TIMESTAMP;
		}

	} 
	
	public function setCachedArticlesToday($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->cached_articles_today !== $v || $v === 0) {
			$this->cached_articles_today = $v;
			$this->modifiedColumns[] = TitlePeer::CACHED_ARTICLES_TODAY;
		}

	} 
	
	public function setCachedCurrentSpan($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->cached_current_span !== $v || $v === 0) {
			$this->cached_current_span = $v;
			$this->modifiedColumns[] = TitlePeer::CACHED_CURRENT_SPAN;
		}

	} 
	
	public function setCachedPrevSpan($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->cached_prev_span !== $v || $v === 0) {
			$this->cached_prev_span = $v;
			$this->modifiedColumns[] = TitlePeer::CACHED_PREV_SPAN;
		}

	} 
	
	public function setCachedArticlesCount($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->cached_articles_count !== $v || $v === 0) {
			$this->cached_articles_count = $v;
			$this->modifiedColumns[] = TitlePeer::CACHED_ARTICLES_COUNT;
		}

	} 
	
	public function setAdminsId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->admins_id !== $v || $v === 0) {
			$this->admins_id = $v;
			$this->modifiedColumns[] = TitlePeer::ADMINS_ID;
		}

		if ($this->aAdmins !== null && $this->aAdmins->getId() !== $v) {
			$this->aAdmins = null;
		}

	} 
	
	public function setRank($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->rank !== $v || $v === 1) {
			$this->rank = $v;
			$this->modifiedColumns[] = TitlePeer::RANK;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->title_frequency_id = $rs->getInt($startcol + 1);

			$this->title_type_id = $rs->getInt($startcol + 2);

			$this->publisher_id = $rs->getInt($startcol + 3);

			$this->name = $rs->getString($startcol + 4);

			$this->website = $rs->getString($startcol + 5);

			$this->language_id = $rs->getInt($startcol + 6);

			$this->ftp_username = $rs->getString($startcol + 7);

			$this->ftp_password = $rs->getString($startcol + 8);

			$this->articles_threshold = $rs->getInt($startcol + 9);

			$this->unix_uid = $rs->getInt($startcol + 10);

			$this->unix_gid = $rs->getInt($startcol + 11);

			$this->nagios_config_file = $rs->getString($startcol + 12);

			$this->home_dir = $rs->getString($startcol + 13);

			$this->work_dir = $rs->getString($startcol + 14);

			$this->svn_repo = $rs->getString($startcol + 15);

			$this->gather_retries = $rs->getInt($startcol + 16);

			$this->default_charset = $rs->getString($startcol + 17);

			$this->parse_dir = $rs->getString($startcol + 18);

			$this->next_try_timer = $rs->getInt($startcol + 19);

			$this->copyright = $rs->getString($startcol + 20);

			$this->copyright_arabic = $rs->getString($startcol + 21);

			$this->is_active = $rs->getInt($startcol + 22);

			$this->activation_date = $rs->getDate($startcol + 23, null);

			$this->logo_path = $rs->getString($startcol + 24);

			$this->directory_layout = $rs->getString($startcol + 25);

			$this->is_newswire = $rs->getInt($startcol + 26);

			$this->is_generate = $rs->getInt($startcol + 27);

			$this->country_id = $rs->getInt($startcol + 28);

			$this->photo_feed = $rs->getString($startcol + 29);

			$this->contract = $rs->getString($startcol + 30);

			$this->has_technical_intro = $rs->getInt($startcol + 31);

			$this->is_receiving = $rs->getInt($startcol + 32);

			$this->is_parsing = $rs->getInt($startcol + 33);

			$this->technical_notes = $rs->getString($startcol + 34);

			$this->timeliness = $rs->getString($startcol + 35);

			$this->contract_start_date = $rs->getDate($startcol + 36, null);

			$this->contract_end_date = $rs->getDate($startcol + 37, null);

			$this->royalty_rate = $rs->getFloat($startcol + 38);

			$this->source_description = $rs->getString($startcol + 39);

			$this->renewal_period = $rs->getString($startcol + 40);

			$this->city = $rs->getString($startcol + 41);

			$this->story_count = $rs->getInt($startcol + 42);

			$this->receiving_status = $rs->getInt($startcol + 43);

			$this->receiving_status_comment = $rs->getString($startcol + 44);

			$this->receiving_status_timestamp = $rs->getTimestamp($startcol + 45, null);

			$this->notes = $rs->getString($startcol + 46);

			$this->has_new_parsed_content = $rs->getInt($startcol + 47);

			$this->last_content_timestamp = $rs->getTimestamp($startcol + 48, null);

			$this->cached_articles_today = $rs->getInt($startcol + 49);

			$this->cached_current_span = $rs->getInt($startcol + 50);

			$this->cached_prev_span = $rs->getInt($startcol + 51);

			$this->cached_articles_count = $rs->getInt($startcol + 52);

			$this->admins_id = $rs->getInt($startcol + 53);

			$this->rank = $rs->getInt($startcol + 54);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 55; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Title object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TitlePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			TitlePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TitlePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aPublisher !== null) {
				if ($this->aPublisher->isModified()) {
					$affectedRows += $this->aPublisher->save($con);
				}
				$this->setPublisher($this->aPublisher);
			}

			if ($this->aTitleType !== null) {
				if ($this->aTitleType->isModified()) {
					$affectedRows += $this->aTitleType->save($con);
				}
				$this->setTitleType($this->aTitleType);
			}

			if ($this->aTitleFrequency !== null) {
				if ($this->aTitleFrequency->isModified()) {
					$affectedRows += $this->aTitleFrequency->save($con);
				}
				$this->setTitleFrequency($this->aTitleFrequency);
			}

			if ($this->aCountry !== null) {
				if ($this->aCountry->isModified()) {
					$affectedRows += $this->aCountry->save($con);
				}
				$this->setCountry($this->aCountry);
			}

			if ($this->aLanguage !== null) {
				if ($this->aLanguage->isModified()) {
					$affectedRows += $this->aLanguage->save($con);
				}
				$this->setLanguage($this->aLanguage);
			}

			if ($this->aAdmins !== null) {
				if ($this->aAdmins->isModified()) {
					$affectedRows += $this->aAdmins->save($con);
				}
				$this->setAdmins($this->aAdmins);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = TitlePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += TitlePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collArticles !== null) {
				foreach($this->collArticles as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collOriginalArticleCategorys !== null) {
				foreach($this->collOriginalArticleCategorys as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collParsingStates !== null) {
				foreach($this->collParsingStates as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collReportGathers !== null) {
				foreach($this->collReportGathers as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collReportParses !== null) {
				foreach($this->collReportParses as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collSchedules !== null) {
				foreach($this->collSchedules as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collTitleConnections !== null) {
				foreach($this->collTitleConnections as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collTitleContactLinks !== null) {
				foreach($this->collTitleContactLinks as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collTitleOfficeLinks !== null) {
				foreach($this->collTitleOfficeLinks as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collOrdersFrequencys !== null) {
				foreach($this->collOrdersFrequencys as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aPublisher !== null) {
				if (!$this->aPublisher->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aPublisher->getValidationFailures());
				}
			}

			if ($this->aTitleType !== null) {
				if (!$this->aTitleType->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTitleType->getValidationFailures());
				}
			}

			if ($this->aTitleFrequency !== null) {
				if (!$this->aTitleFrequency->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTitleFrequency->getValidationFailures());
				}
			}

			if ($this->aCountry !== null) {
				if (!$this->aCountry->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCountry->getValidationFailures());
				}
			}

			if ($this->aLanguage !== null) {
				if (!$this->aLanguage->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aLanguage->getValidationFailures());
				}
			}

			if ($this->aAdmins !== null) {
				if (!$this->aAdmins->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aAdmins->getValidationFailures());
				}
			}


			if (($retval = TitlePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collArticles !== null) {
					foreach($this->collArticles as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collOriginalArticleCategorys !== null) {
					foreach($this->collOriginalArticleCategorys as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collParsingStates !== null) {
					foreach($this->collParsingStates as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collReportGathers !== null) {
					foreach($this->collReportGathers as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collReportParses !== null) {
					foreach($this->collReportParses as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collSchedules !== null) {
					foreach($this->collSchedules as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collTitleConnections !== null) {
					foreach($this->collTitleConnections as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collTitleContactLinks !== null) {
					foreach($this->collTitleContactLinks as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collTitleOfficeLinks !== null) {
					foreach($this->collTitleOfficeLinks as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collOrdersFrequencys !== null) {
					foreach($this->collOrdersFrequencys as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TitlePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTitleFrequencyId();
				break;
			case 2:
				return $this->getTitleTypeId();
				break;
			case 3:
				return $this->getPublisherId();
				break;
			case 4:
				return $this->getName();
				break;
			case 5:
				return $this->getWebsite();
				break;
			case 6:
				return $this->getLanguageId();
				break;
			case 7:
				return $this->getFtpUsername();
				break;
			case 8:
				return $this->getFtpPassword();
				break;
			case 9:
				return $this->getArticlesThreshold();
				break;
			case 10:
				return $this->getUnixUid();
				break;
			case 11:
				return $this->getUnixGid();
				break;
			case 12:
				return $this->getNagiosConfigFile();
				break;
			case 13:
				return $this->getHomeDir();
				break;
			case 14:
				return $this->getWorkDir();
				break;
			case 15:
				return $this->getSvnRepo();
				break;
			case 16:
				return $this->getGatherRetries();
				break;
			case 17:
				return $this->getDefaultCharset();
				break;
			case 18:
				return $this->getParseDir();
				break;
			case 19:
				return $this->getNextTryTimer();
				break;
			case 20:
				return $this->getCopyright();
				break;
			case 21:
				return $this->getCopyrightArabic();
				break;
			case 22:
				return $this->getIsActive();
				break;
			case 23:
				return $this->getActivationDate();
				break;
			case 24:
				return $this->getLogoPath();
				break;
			case 25:
				return $this->getDirectoryLayout();
				break;
			case 26:
				return $this->getIsNewswire();
				break;
			case 27:
				return $this->getIsGenerate();
				break;
			case 28:
				return $this->getCountryId();
				break;
			case 29:
				return $this->getPhotoFeed();
				break;
			case 30:
				return $this->getContract();
				break;
			case 31:
				return $this->getHasTechnicalIntro();
				break;
			case 32:
				return $this->getIsReceiving();
				break;
			case 33:
				return $this->getIsParsing();
				break;
			case 34:
				return $this->getTechnicalNotes();
				break;
			case 35:
				return $this->getTimeliness();
				break;
			case 36:
				return $this->getContractStartDate();
				break;
			case 37:
				return $this->getContractEndDate();
				break;
			case 38:
				return $this->getRoyaltyRate();
				break;
			case 39:
				return $this->getSourceDescription();
				break;
			case 40:
				return $this->getRenewalPeriod();
				break;
			case 41:
				return $this->getCity();
				break;
			case 42:
				return $this->getStoryCount();
				break;
			case 43:
				return $this->getReceivingStatus();
				break;
			case 44:
				return $this->getReceivingStatusComment();
				break;
			case 45:
				return $this->getReceivingStatusTimestamp();
				break;
			case 46:
				return $this->getNotes();
				break;
			case 47:
				return $this->getHasNewParsedContent();
				break;
			case 48:
				return $this->getLastContentTimestamp();
				break;
			case 49:
				return $this->getCachedArticlesToday();
				break;
			case 50:
				return $this->getCachedCurrentSpan();
				break;
			case 51:
				return $this->getCachedPrevSpan();
				break;
			case 52:
				return $this->getCachedArticlesCount();
				break;
			case 53:
				return $this->getAdminsId();
				break;
			case 54:
				return $this->getRank();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TitlePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTitleFrequencyId(),
			$keys[2] => $this->getTitleTypeId(),
			$keys[3] => $this->getPublisherId(),
			$keys[4] => $this->getName(),
			$keys[5] => $this->getWebsite(),
			$keys[6] => $this->getLanguageId(),
			$keys[7] => $this->getFtpUsername(),
			$keys[8] => $this->getFtpPassword(),
			$keys[9] => $this->getArticlesThreshold(),
			$keys[10] => $this->getUnixUid(),
			$keys[11] => $this->getUnixGid(),
			$keys[12] => $this->getNagiosConfigFile(),
			$keys[13] => $this->getHomeDir(),
			$keys[14] => $this->getWorkDir(),
			$keys[15] => $this->getSvnRepo(),
			$keys[16] => $this->getGatherRetries(),
			$keys[17] => $this->getDefaultCharset(),
			$keys[18] => $this->getParseDir(),
			$keys[19] => $this->getNextTryTimer(),
			$keys[20] => $this->getCopyright(),
			$keys[21] => $this->getCopyrightArabic(),
			$keys[22] => $this->getIsActive(),
			$keys[23] => $this->getActivationDate(),
			$keys[24] => $this->getLogoPath(),
			$keys[25] => $this->getDirectoryLayout(),
			$keys[26] => $this->getIsNewswire(),
			$keys[27] => $this->getIsGenerate(),
			$keys[28] => $this->getCountryId(),
			$keys[29] => $this->getPhotoFeed(),
			$keys[30] => $this->getContract(),
			$keys[31] => $this->getHasTechnicalIntro(),
			$keys[32] => $this->getIsReceiving(),
			$keys[33] => $this->getIsParsing(),
			$keys[34] => $this->getTechnicalNotes(),
			$keys[35] => $this->getTimeliness(),
			$keys[36] => $this->getContractStartDate(),
			$keys[37] => $this->getContractEndDate(),
			$keys[38] => $this->getRoyaltyRate(),
			$keys[39] => $this->getSourceDescription(),
			$keys[40] => $this->getRenewalPeriod(),
			$keys[41] => $this->getCity(),
			$keys[42] => $this->getStoryCount(),
			$keys[43] => $this->getReceivingStatus(),
			$keys[44] => $this->getReceivingStatusComment(),
			$keys[45] => $this->getReceivingStatusTimestamp(),
			$keys[46] => $this->getNotes(),
			$keys[47] => $this->getHasNewParsedContent(),
			$keys[48] => $this->getLastContentTimestamp(),
			$keys[49] => $this->getCachedArticlesToday(),
			$keys[50] => $this->getCachedCurrentSpan(),
			$keys[51] => $this->getCachedPrevSpan(),
			$keys[52] => $this->getCachedArticlesCount(),
			$keys[53] => $this->getAdminsId(),
			$keys[54] => $this->getRank(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TitlePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTitleFrequencyId($value);
				break;
			case 2:
				$this->setTitleTypeId($value);
				break;
			case 3:
				$this->setPublisherId($value);
				break;
			case 4:
				$this->setName($value);
				break;
			case 5:
				$this->setWebsite($value);
				break;
			case 6:
				$this->setLanguageId($value);
				break;
			case 7:
				$this->setFtpUsername($value);
				break;
			case 8:
				$this->setFtpPassword($value);
				break;
			case 9:
				$this->setArticlesThreshold($value);
				break;
			case 10:
				$this->setUnixUid($value);
				break;
			case 11:
				$this->setUnixGid($value);
				break;
			case 12:
				$this->setNagiosConfigFile($value);
				break;
			case 13:
				$this->setHomeDir($value);
				break;
			case 14:
				$this->setWorkDir($value);
				break;
			case 15:
				$this->setSvnRepo($value);
				break;
			case 16:
				$this->setGatherRetries($value);
				break;
			case 17:
				$this->setDefaultCharset($value);
				break;
			case 18:
				$this->setParseDir($value);
				break;
			case 19:
				$this->setNextTryTimer($value);
				break;
			case 20:
				$this->setCopyright($value);
				break;
			case 21:
				$this->setCopyrightArabic($value);
				break;
			case 22:
				$this->setIsActive($value);
				break;
			case 23:
				$this->setActivationDate($value);
				break;
			case 24:
				$this->setLogoPath($value);
				break;
			case 25:
				$this->setDirectoryLayout($value);
				break;
			case 26:
				$this->setIsNewswire($value);
				break;
			case 27:
				$this->setIsGenerate($value);
				break;
			case 28:
				$this->setCountryId($value);
				break;
			case 29:
				$this->setPhotoFeed($value);
				break;
			case 30:
				$this->setContract($value);
				break;
			case 31:
				$this->setHasTechnicalIntro($value);
				break;
			case 32:
				$this->setIsReceiving($value);
				break;
			case 33:
				$this->setIsParsing($value);
				break;
			case 34:
				$this->setTechnicalNotes($value);
				break;
			case 35:
				$this->setTimeliness($value);
				break;
			case 36:
				$this->setContractStartDate($value);
				break;
			case 37:
				$this->setContractEndDate($value);
				break;
			case 38:
				$this->setRoyaltyRate($value);
				break;
			case 39:
				$this->setSourceDescription($value);
				break;
			case 40:
				$this->setRenewalPeriod($value);
				break;
			case 41:
				$this->setCity($value);
				break;
			case 42:
				$this->setStoryCount($value);
				break;
			case 43:
				$this->setReceivingStatus($value);
				break;
			case 44:
				$this->setReceivingStatusComment($value);
				break;
			case 45:
				$this->setReceivingStatusTimestamp($value);
				break;
			case 46:
				$this->setNotes($value);
				break;
			case 47:
				$this->setHasNewParsedContent($value);
				break;
			case 48:
				$this->setLastContentTimestamp($value);
				break;
			case 49:
				$this->setCachedArticlesToday($value);
				break;
			case 50:
				$this->setCachedCurrentSpan($value);
				break;
			case 51:
				$this->setCachedPrevSpan($value);
				break;
			case 52:
				$this->setCachedArticlesCount($value);
				break;
			case 53:
				$this->setAdminsId($value);
				break;
			case 54:
				$this->setRank($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TitlePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTitleFrequencyId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setTitleTypeId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setPublisherId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setName($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setWebsite($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setLanguageId($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setFtpUsername($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setFtpPassword($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setArticlesThreshold($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setUnixUid($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setUnixGid($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setNagiosConfigFile($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setHomeDir($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setWorkDir($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setSvnRepo($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setGatherRetries($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setDefaultCharset($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setParseDir($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setNextTryTimer($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setCopyright($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setCopyrightArabic($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setIsActive($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setActivationDate($arr[$keys[23]]);
		if (array_key_exists($keys[24], $arr)) $this->setLogoPath($arr[$keys[24]]);
		if (array_key_exists($keys[25], $arr)) $this->setDirectoryLayout($arr[$keys[25]]);
		if (array_key_exists($keys[26], $arr)) $this->setIsNewswire($arr[$keys[26]]);
		if (array_key_exists($keys[27], $arr)) $this->setIsGenerate($arr[$keys[27]]);
		if (array_key_exists($keys[28], $arr)) $this->setCountryId($arr[$keys[28]]);
		if (array_key_exists($keys[29], $arr)) $this->setPhotoFeed($arr[$keys[29]]);
		if (array_key_exists($keys[30], $arr)) $this->setContract($arr[$keys[30]]);
		if (array_key_exists($keys[31], $arr)) $this->setHasTechnicalIntro($arr[$keys[31]]);
		if (array_key_exists($keys[32], $arr)) $this->setIsReceiving($arr[$keys[32]]);
		if (array_key_exists($keys[33], $arr)) $this->setIsParsing($arr[$keys[33]]);
		if (array_key_exists($keys[34], $arr)) $this->setTechnicalNotes($arr[$keys[34]]);
		if (array_key_exists($keys[35], $arr)) $this->setTimeliness($arr[$keys[35]]);
		if (array_key_exists($keys[36], $arr)) $this->setContractStartDate($arr[$keys[36]]);
		if (array_key_exists($keys[37], $arr)) $this->setContractEndDate($arr[$keys[37]]);
		if (array_key_exists($keys[38], $arr)) $this->setRoyaltyRate($arr[$keys[38]]);
		if (array_key_exists($keys[39], $arr)) $this->setSourceDescription($arr[$keys[39]]);
		if (array_key_exists($keys[40], $arr)) $this->setRenewalPeriod($arr[$keys[40]]);
		if (array_key_exists($keys[41], $arr)) $this->setCity($arr[$keys[41]]);
		if (array_key_exists($keys[42], $arr)) $this->setStoryCount($arr[$keys[42]]);
		if (array_key_exists($keys[43], $arr)) $this->setReceivingStatus($arr[$keys[43]]);
		if (array_key_exists($keys[44], $arr)) $this->setReceivingStatusComment($arr[$keys[44]]);
		if (array_key_exists($keys[45], $arr)) $this->setReceivingStatusTimestamp($arr[$keys[45]]);
		if (array_key_exists($keys[46], $arr)) $this->setNotes($arr[$keys[46]]);
		if (array_key_exists($keys[47], $arr)) $this->setHasNewParsedContent($arr[$keys[47]]);
		if (array_key_exists($keys[48], $arr)) $this->setLastContentTimestamp($arr[$keys[48]]);
		if (array_key_exists($keys[49], $arr)) $this->setCachedArticlesToday($arr[$keys[49]]);
		if (array_key_exists($keys[50], $arr)) $this->setCachedCurrentSpan($arr[$keys[50]]);
		if (array_key_exists($keys[51], $arr)) $this->setCachedPrevSpan($arr[$keys[51]]);
		if (array_key_exists($keys[52], $arr)) $this->setCachedArticlesCount($arr[$keys[52]]);
		if (array_key_exists($keys[53], $arr)) $this->setAdminsId($arr[$keys[53]]);
		if (array_key_exists($keys[54], $arr)) $this->setRank($arr[$keys[54]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(TitlePeer::DATABASE_NAME);

		if ($this->isColumnModified(TitlePeer::ID)) $criteria->add(TitlePeer::ID, $this->id);
		if ($this->isColumnModified(TitlePeer::TITLE_FREQUENCY_ID)) $criteria->add(TitlePeer::TITLE_FREQUENCY_ID, $this->title_frequency_id);
		if ($this->isColumnModified(TitlePeer::TITLE_TYPE_ID)) $criteria->add(TitlePeer::TITLE_TYPE_ID, $this->title_type_id);
		if ($this->isColumnModified(TitlePeer::PUBLISHER_ID)) $criteria->add(TitlePeer::PUBLISHER_ID, $this->publisher_id);
		if ($this->isColumnModified(TitlePeer::NAME)) $criteria->add(TitlePeer::NAME, $this->name);
		if ($this->isColumnModified(TitlePeer::WEBSITE)) $criteria->add(TitlePeer::WEBSITE, $this->website);
		if ($this->isColumnModified(TitlePeer::LANGUAGE_ID)) $criteria->add(TitlePeer::LANGUAGE_ID, $this->language_id);
		if ($this->isColumnModified(TitlePeer::FTP_USERNAME)) $criteria->add(TitlePeer::FTP_USERNAME, $this->ftp_username);
		if ($this->isColumnModified(TitlePeer::FTP_PASSWORD)) $criteria->add(TitlePeer::FTP_PASSWORD, $this->ftp_password);
		if ($this->isColumnModified(TitlePeer::ARTICLES_THRESHOLD)) $criteria->add(TitlePeer::ARTICLES_THRESHOLD, $this->articles_threshold);
		if ($this->isColumnModified(TitlePeer::UNIX_UID)) $criteria->add(TitlePeer::UNIX_UID, $this->unix_uid);
		if ($this->isColumnModified(TitlePeer::UNIX_GID)) $criteria->add(TitlePeer::UNIX_GID, $this->unix_gid);
		if ($this->isColumnModified(TitlePeer::NAGIOS_CONFIG_FILE)) $criteria->add(TitlePeer::NAGIOS_CONFIG_FILE, $this->nagios_config_file);
		if ($this->isColumnModified(TitlePeer::HOME_DIR)) $criteria->add(TitlePeer::HOME_DIR, $this->home_dir);
		if ($this->isColumnModified(TitlePeer::WORK_DIR)) $criteria->add(TitlePeer::WORK_DIR, $this->work_dir);
		if ($this->isColumnModified(TitlePeer::SVN_REPO)) $criteria->add(TitlePeer::SVN_REPO, $this->svn_repo);
		if ($this->isColumnModified(TitlePeer::GATHER_RETRIES)) $criteria->add(TitlePeer::GATHER_RETRIES, $this->gather_retries);
		if ($this->isColumnModified(TitlePeer::DEFAULT_CHARSET)) $criteria->add(TitlePeer::DEFAULT_CHARSET, $this->default_charset);
		if ($this->isColumnModified(TitlePeer::PARSE_DIR)) $criteria->add(TitlePeer::PARSE_DIR, $this->parse_dir);
		if ($this->isColumnModified(TitlePeer::NEXT_TRY_TIMER)) $criteria->add(TitlePeer::NEXT_TRY_TIMER, $this->next_try_timer);
		if ($this->isColumnModified(TitlePeer::COPYRIGHT)) $criteria->add(TitlePeer::COPYRIGHT, $this->copyright);
		if ($this->isColumnModified(TitlePeer::COPYRIGHT_ARABIC)) $criteria->add(TitlePeer::COPYRIGHT_ARABIC, $this->copyright_arabic);
		if ($this->isColumnModified(TitlePeer::IS_ACTIVE)) $criteria->add(TitlePeer::IS_ACTIVE, $this->is_active);
		if ($this->isColumnModified(TitlePeer::ACTIVATION_DATE)) $criteria->add(TitlePeer::ACTIVATION_DATE, $this->activation_date);
		if ($this->isColumnModified(TitlePeer::LOGO_PATH)) $criteria->add(TitlePeer::LOGO_PATH, $this->logo_path);
		if ($this->isColumnModified(TitlePeer::DIRECTORY_LAYOUT)) $criteria->add(TitlePeer::DIRECTORY_LAYOUT, $this->directory_layout);
		if ($this->isColumnModified(TitlePeer::IS_NEWSWIRE)) $criteria->add(TitlePeer::IS_NEWSWIRE, $this->is_newswire);
		if ($this->isColumnModified(TitlePeer::IS_GENERATE)) $criteria->add(TitlePeer::IS_GENERATE, $this->is_generate);
		if ($this->isColumnModified(TitlePeer::COUNTRY_ID)) $criteria->add(TitlePeer::COUNTRY_ID, $this->country_id);
		if ($this->isColumnModified(TitlePeer::PHOTO_FEED)) $criteria->add(TitlePeer::PHOTO_FEED, $this->photo_feed);
		if ($this->isColumnModified(TitlePeer::CONTRACT)) $criteria->add(TitlePeer::CONTRACT, $this->contract);
		if ($this->isColumnModified(TitlePeer::HAS_TECHNICAL_INTRO)) $criteria->add(TitlePeer::HAS_TECHNICAL_INTRO, $this->has_technical_intro);
		if ($this->isColumnModified(TitlePeer::IS_RECEIVING)) $criteria->add(TitlePeer::IS_RECEIVING, $this->is_receiving);
		if ($this->isColumnModified(TitlePeer::IS_PARSING)) $criteria->add(TitlePeer::IS_PARSING, $this->is_parsing);
		if ($this->isColumnModified(TitlePeer::TECHNICAL_NOTES)) $criteria->add(TitlePeer::TECHNICAL_NOTES, $this->technical_notes);
		if ($this->isColumnModified(TitlePeer::TIMELINESS)) $criteria->add(TitlePeer::TIMELINESS, $this->timeliness);
		if ($this->isColumnModified(TitlePeer::CONTRACT_START_DATE)) $criteria->add(TitlePeer::CONTRACT_START_DATE, $this->contract_start_date);
		if ($this->isColumnModified(TitlePeer::CONTRACT_END_DATE)) $criteria->add(TitlePeer::CONTRACT_END_DATE, $this->contract_end_date);
		if ($this->isColumnModified(TitlePeer::ROYALTY_RATE)) $criteria->add(TitlePeer::ROYALTY_RATE, $this->royalty_rate);
		if ($this->isColumnModified(TitlePeer::SOURCE_DESCRIPTION)) $criteria->add(TitlePeer::SOURCE_DESCRIPTION, $this->source_description);
		if ($this->isColumnModified(TitlePeer::RENEWAL_PERIOD)) $criteria->add(TitlePeer::RENEWAL_PERIOD, $this->renewal_period);
		if ($this->isColumnModified(TitlePeer::CITY)) $criteria->add(TitlePeer::CITY, $this->city);
		if ($this->isColumnModified(TitlePeer::STORY_COUNT)) $criteria->add(TitlePeer::STORY_COUNT, $this->story_count);
		if ($this->isColumnModified(TitlePeer::RECEIVING_STATUS)) $criteria->add(TitlePeer::RECEIVING_STATUS, $this->receiving_status);
		if ($this->isColumnModified(TitlePeer::RECEIVING_STATUS_COMMENT)) $criteria->add(TitlePeer::RECEIVING_STATUS_COMMENT, $this->receiving_status_comment);
		if ($this->isColumnModified(TitlePeer::RECEIVING_STATUS_TIMESTAMP)) $criteria->add(TitlePeer::RECEIVING_STATUS_TIMESTAMP, $this->receiving_status_timestamp);
		if ($this->isColumnModified(TitlePeer::NOTES)) $criteria->add(TitlePeer::NOTES, $this->notes);
		if ($this->isColumnModified(TitlePeer::HAS_NEW_PARSED_CONTENT)) $criteria->add(TitlePeer::HAS_NEW_PARSED_CONTENT, $this->has_new_parsed_content);
		if ($this->isColumnModified(TitlePeer::LAST_CONTENT_TIMESTAMP)) $criteria->add(TitlePeer::LAST_CONTENT_TIMESTAMP, $this->last_content_timestamp);
		if ($this->isColumnModified(TitlePeer::CACHED_ARTICLES_TODAY)) $criteria->add(TitlePeer::CACHED_ARTICLES_TODAY, $this->cached_articles_today);
		if ($this->isColumnModified(TitlePeer::CACHED_CURRENT_SPAN)) $criteria->add(TitlePeer::CACHED_CURRENT_SPAN, $this->cached_current_span);
		if ($this->isColumnModified(TitlePeer::CACHED_PREV_SPAN)) $criteria->add(TitlePeer::CACHED_PREV_SPAN, $this->cached_prev_span);
		if ($this->isColumnModified(TitlePeer::CACHED_ARTICLES_COUNT)) $criteria->add(TitlePeer::CACHED_ARTICLES_COUNT, $this->cached_articles_count);
		if ($this->isColumnModified(TitlePeer::ADMINS_ID)) $criteria->add(TitlePeer::ADMINS_ID, $this->admins_id);
		if ($this->isColumnModified(TitlePeer::RANK)) $criteria->add(TitlePeer::RANK, $this->rank);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(TitlePeer::DATABASE_NAME);

		$criteria->add(TitlePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTitleFrequencyId($this->title_frequency_id);

		$copyObj->setTitleTypeId($this->title_type_id);

		$copyObj->setPublisherId($this->publisher_id);

		$copyObj->setName($this->name);

		$copyObj->setWebsite($this->website);

		$copyObj->setLanguageId($this->language_id);

		$copyObj->setFtpUsername($this->ftp_username);

		$copyObj->setFtpPassword($this->ftp_password);

		$copyObj->setArticlesThreshold($this->articles_threshold);

		$copyObj->setUnixUid($this->unix_uid);

		$copyObj->setUnixGid($this->unix_gid);

		$copyObj->setNagiosConfigFile($this->nagios_config_file);

		$copyObj->setHomeDir($this->home_dir);

		$copyObj->setWorkDir($this->work_dir);

		$copyObj->setSvnRepo($this->svn_repo);

		$copyObj->setGatherRetries($this->gather_retries);

		$copyObj->setDefaultCharset($this->default_charset);

		$copyObj->setParseDir($this->parse_dir);

		$copyObj->setNextTryTimer($this->next_try_timer);

		$copyObj->setCopyright($this->copyright);

		$copyObj->setCopyrightArabic($this->copyright_arabic);

		$copyObj->setIsActive($this->is_active);

		$copyObj->setActivationDate($this->activation_date);

		$copyObj->setLogoPath($this->logo_path);

		$copyObj->setDirectoryLayout($this->directory_layout);

		$copyObj->setIsNewswire($this->is_newswire);

		$copyObj->setIsGenerate($this->is_generate);

		$copyObj->setCountryId($this->country_id);

		$copyObj->setPhotoFeed($this->photo_feed);

		$copyObj->setContract($this->contract);

		$copyObj->setHasTechnicalIntro($this->has_technical_intro);

		$copyObj->setIsReceiving($this->is_receiving);

		$copyObj->setIsParsing($this->is_parsing);

		$copyObj->setTechnicalNotes($this->technical_notes);

		$copyObj->setTimeliness($this->timeliness);

		$copyObj->setContractStartDate($this->contract_start_date);

		$copyObj->setContractEndDate($this->contract_end_date);

		$copyObj->setRoyaltyRate($this->royalty_rate);

		$copyObj->setSourceDescription($this->source_description);

		$copyObj->setRenewalPeriod($this->renewal_period);

		$copyObj->setCity($this->city);

		$copyObj->setStoryCount($this->story_count);

		$copyObj->setReceivingStatus($this->receiving_status);

		$copyObj->setReceivingStatusComment($this->receiving_status_comment);

		$copyObj->setReceivingStatusTimestamp($this->receiving_status_timestamp);

		$copyObj->setNotes($this->notes);

		$copyObj->setHasNewParsedContent($this->has_new_parsed_content);

		$copyObj->setLastContentTimestamp($this->last_content_timestamp);

		$copyObj->setCachedArticlesToday($this->cached_articles_today);

		$copyObj->setCachedCurrentSpan($this->cached_current_span);

		$copyObj->setCachedPrevSpan($this->cached_prev_span);

		$copyObj->setCachedArticlesCount($this->cached_articles_count);

		$copyObj->setAdminsId($this->admins_id);

		$copyObj->setRank($this->rank);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getArticles() as $relObj) {
				$copyObj->addArticle($relObj->copy($deepCopy));
			}

			foreach($this->getOriginalArticleCategorys() as $relObj) {
				$copyObj->addOriginalArticleCategory($relObj->copy($deepCopy));
			}

			foreach($this->getParsingStates() as $relObj) {
				$copyObj->addParsingState($relObj->copy($deepCopy));
			}

			foreach($this->getReportGathers() as $relObj) {
				$copyObj->addReportGather($relObj->copy($deepCopy));
			}

			foreach($this->getReportParses() as $relObj) {
				$copyObj->addReportParse($relObj->copy($deepCopy));
			}

			foreach($this->getSchedules() as $relObj) {
				$copyObj->addSchedule($relObj->copy($deepCopy));
			}

			foreach($this->getTitleConnections() as $relObj) {
				$copyObj->addTitleConnection($relObj->copy($deepCopy));
			}

			foreach($this->getTitleContactLinks() as $relObj) {
				$copyObj->addTitleContactLink($relObj->copy($deepCopy));
			}

			foreach($this->getTitleOfficeLinks() as $relObj) {
				$copyObj->addTitleOfficeLink($relObj->copy($deepCopy));
			}

			foreach($this->getOrdersFrequencys() as $relObj) {
				$copyObj->addOrdersFrequency($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new TitlePeer();
		}
		return self::$peer;
	}

	
	public function setPublisher($v)
	{


		if ($v === null) {
			$this->setPublisherId(NULL);
		} else {
			$this->setPublisherId($v->getId());
		}


		$this->aPublisher = $v;
	}


	
	public function getPublisher($con = null)
	{
		if ($this->aPublisher === null && ($this->publisher_id !== null)) {
						include_once 'lib/model/om/BasePublisherPeer.php';

			$this->aPublisher = PublisherPeer::retrieveByPK($this->publisher_id, $con);

			
		}
		return $this->aPublisher;
	}

	
	public function setTitleType($v)
	{


		if ($v === null) {
			$this->setTitleTypeId(NULL);
		} else {
			$this->setTitleTypeId($v->getId());
		}


		$this->aTitleType = $v;
	}


	
	public function getTitleType($con = null)
	{
		if ($this->aTitleType === null && ($this->title_type_id !== null)) {
						include_once 'lib/model/om/BaseTitleTypePeer.php';

			$this->aTitleType = TitleTypePeer::retrieveByPK($this->title_type_id, $con);

			
		}
		return $this->aTitleType;
	}

	
	public function setTitleFrequency($v)
	{


		if ($v === null) {
			$this->setTitleFrequencyId(NULL);
		} else {
			$this->setTitleFrequencyId($v->getId());
		}


		$this->aTitleFrequency = $v;
	}


	
	public function getTitleFrequency($con = null)
	{
		if ($this->aTitleFrequency === null && ($this->title_frequency_id !== null)) {
						include_once 'lib/model/om/BaseTitleFrequencyPeer.php';

			$this->aTitleFrequency = TitleFrequencyPeer::retrieveByPK($this->title_frequency_id, $con);

			
		}
		return $this->aTitleFrequency;
	}

	
	public function setCountry($v)
	{


		if ($v === null) {
			$this->setCountryId(NULL);
		} else {
			$this->setCountryId($v->getId());
		}


		$this->aCountry = $v;
	}


	
	public function getCountry($con = null)
	{
		if ($this->aCountry === null && ($this->country_id !== null)) {
						include_once 'lib/model/om/BaseCountryPeer.php';

			$this->aCountry = CountryPeer::retrieveByPK($this->country_id, $con);

			
		}
		return $this->aCountry;
	}

	
	public function setLanguage($v)
	{


		if ($v === null) {
			$this->setLanguageId(NULL);
		} else {
			$this->setLanguageId($v->getId());
		}


		$this->aLanguage = $v;
	}


	
	public function getLanguage($con = null)
	{
		if ($this->aLanguage === null && ($this->language_id !== null)) {
						include_once 'lib/model/om/BaseLanguagePeer.php';

			$this->aLanguage = LanguagePeer::retrieveByPK($this->language_id, $con);

			
		}
		return $this->aLanguage;
	}

	
	public function setAdmins($v)
	{


		if ($v === null) {
			$this->setAdminsId('0');
		} else {
			$this->setAdminsId($v->getId());
		}


		$this->aAdmins = $v;
	}


	
	public function getAdmins($con = null)
	{
		if ($this->aAdmins === null && ($this->admins_id !== null)) {
						include_once 'lib/model/om/BaseAdminsPeer.php';

			$this->aAdmins = AdminsPeer::retrieveByPK($this->admins_id, $con);

			
		}
		return $this->aAdmins;
	}

	
	public function initArticles()
	{
		if ($this->collArticles === null) {
			$this->collArticles = array();
		}
	}

	
	public function getArticles($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticles === null) {
			if ($this->isNew()) {
			   $this->collArticles = array();
			} else {

				$criteria->add(ArticlePeer::TITLE_ID, $this->getId());

				ArticlePeer::addSelectColumns($criteria);
				$this->collArticles = ArticlePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ArticlePeer::TITLE_ID, $this->getId());

				ArticlePeer::addSelectColumns($criteria);
				if (!isset($this->lastArticleCriteria) || !$this->lastArticleCriteria->equals($criteria)) {
					$this->collArticles = ArticlePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastArticleCriteria = $criteria;
		return $this->collArticles;
	}

	
	public function countArticles($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ArticlePeer::TITLE_ID, $this->getId());

		return ArticlePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addArticle(Article $l)
	{
		$this->collArticles[] = $l;
		$l->setTitle($this);
	}


	
	public function getArticlesJoinLanguage($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticles === null) {
			if ($this->isNew()) {
				$this->collArticles = array();
			} else {

				$criteria->add(ArticlePeer::TITLE_ID, $this->getId());

				$this->collArticles = ArticlePeer::doSelectJoinLanguage($criteria, $con);
			}
		} else {
									
			$criteria->add(ArticlePeer::TITLE_ID, $this->getId());

			if (!isset($this->lastArticleCriteria) || !$this->lastArticleCriteria->equals($criteria)) {
				$this->collArticles = ArticlePeer::doSelectJoinLanguage($criteria, $con);
			}
		}
		$this->lastArticleCriteria = $criteria;

		return $this->collArticles;
	}


	
	public function getArticlesJoinArticleOriginalData($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticles === null) {
			if ($this->isNew()) {
				$this->collArticles = array();
			} else {

				$criteria->add(ArticlePeer::TITLE_ID, $this->getId());

				$this->collArticles = ArticlePeer::doSelectJoinArticleOriginalData($criteria, $con);
			}
		} else {
									
			$criteria->add(ArticlePeer::TITLE_ID, $this->getId());

			if (!isset($this->lastArticleCriteria) || !$this->lastArticleCriteria->equals($criteria)) {
				$this->collArticles = ArticlePeer::doSelectJoinArticleOriginalData($criteria, $con);
			}
		}
		$this->lastArticleCriteria = $criteria;

		return $this->collArticles;
	}


	
	public function getArticlesJoinIptc($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticles === null) {
			if ($this->isNew()) {
				$this->collArticles = array();
			} else {

				$criteria->add(ArticlePeer::TITLE_ID, $this->getId());

				$this->collArticles = ArticlePeer::doSelectJoinIptc($criteria, $con);
			}
		} else {
									
			$criteria->add(ArticlePeer::TITLE_ID, $this->getId());

			if (!isset($this->lastArticleCriteria) || !$this->lastArticleCriteria->equals($criteria)) {
				$this->collArticles = ArticlePeer::doSelectJoinIptc($criteria, $con);
			}
		}
		$this->lastArticleCriteria = $criteria;

		return $this->collArticles;
	}

	
	public function initOriginalArticleCategorys()
	{
		if ($this->collOriginalArticleCategorys === null) {
			$this->collOriginalArticleCategorys = array();
		}
	}

	
	public function getOriginalArticleCategorys($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseOriginalArticleCategoryPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collOriginalArticleCategorys === null) {
			if ($this->isNew()) {
			   $this->collOriginalArticleCategorys = array();
			} else {

				$criteria->add(OriginalArticleCategoryPeer::TITLE_ID, $this->getId());

				OriginalArticleCategoryPeer::addSelectColumns($criteria);
				$this->collOriginalArticleCategorys = OriginalArticleCategoryPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(OriginalArticleCategoryPeer::TITLE_ID, $this->getId());

				OriginalArticleCategoryPeer::addSelectColumns($criteria);
				if (!isset($this->lastOriginalArticleCategoryCriteria) || !$this->lastOriginalArticleCategoryCriteria->equals($criteria)) {
					$this->collOriginalArticleCategorys = OriginalArticleCategoryPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastOriginalArticleCategoryCriteria = $criteria;
		return $this->collOriginalArticleCategorys;
	}

	
	public function countOriginalArticleCategorys($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseOriginalArticleCategoryPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(OriginalArticleCategoryPeer::TITLE_ID, $this->getId());

		return OriginalArticleCategoryPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addOriginalArticleCategory(OriginalArticleCategory $l)
	{
		$this->collOriginalArticleCategorys[] = $l;
		$l->setTitle($this);
	}

	
	public function initParsingStates()
	{
		if ($this->collParsingStates === null) {
			$this->collParsingStates = array();
		}
	}

	
	public function getParsingStates($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseParsingStatePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collParsingStates === null) {
			if ($this->isNew()) {
			   $this->collParsingStates = array();
			} else {

				$criteria->add(ParsingStatePeer::TITLE_ID, $this->getId());

				ParsingStatePeer::addSelectColumns($criteria);
				$this->collParsingStates = ParsingStatePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ParsingStatePeer::TITLE_ID, $this->getId());

				ParsingStatePeer::addSelectColumns($criteria);
				if (!isset($this->lastParsingStateCriteria) || !$this->lastParsingStateCriteria->equals($criteria)) {
					$this->collParsingStates = ParsingStatePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastParsingStateCriteria = $criteria;
		return $this->collParsingStates;
	}

	
	public function countParsingStates($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseParsingStatePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ParsingStatePeer::TITLE_ID, $this->getId());

		return ParsingStatePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addParsingState(ParsingState $l)
	{
		$this->collParsingStates[] = $l;
		$l->setTitle($this);
	}

	
	public function initReportGathers()
	{
		if ($this->collReportGathers === null) {
			$this->collReportGathers = array();
		}
	}

	
	public function getReportGathers($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseReportGatherPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collReportGathers === null) {
			if ($this->isNew()) {
			   $this->collReportGathers = array();
			} else {

				$criteria->add(ReportGatherPeer::TITLE_ID, $this->getId());

				ReportGatherPeer::addSelectColumns($criteria);
				$this->collReportGathers = ReportGatherPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ReportGatherPeer::TITLE_ID, $this->getId());

				ReportGatherPeer::addSelectColumns($criteria);
				if (!isset($this->lastReportGatherCriteria) || !$this->lastReportGatherCriteria->equals($criteria)) {
					$this->collReportGathers = ReportGatherPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastReportGatherCriteria = $criteria;
		return $this->collReportGathers;
	}

	
	public function countReportGathers($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseReportGatherPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ReportGatherPeer::TITLE_ID, $this->getId());

		return ReportGatherPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addReportGather(ReportGather $l)
	{
		$this->collReportGathers[] = $l;
		$l->setTitle($this);
	}

	
	public function initReportParses()
	{
		if ($this->collReportParses === null) {
			$this->collReportParses = array();
		}
	}

	
	public function getReportParses($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseReportParsePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collReportParses === null) {
			if ($this->isNew()) {
			   $this->collReportParses = array();
			} else {

				$criteria->add(ReportParsePeer::TITLE_ID, $this->getId());

				ReportParsePeer::addSelectColumns($criteria);
				$this->collReportParses = ReportParsePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ReportParsePeer::TITLE_ID, $this->getId());

				ReportParsePeer::addSelectColumns($criteria);
				if (!isset($this->lastReportParseCriteria) || !$this->lastReportParseCriteria->equals($criteria)) {
					$this->collReportParses = ReportParsePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastReportParseCriteria = $criteria;
		return $this->collReportParses;
	}

	
	public function countReportParses($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseReportParsePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ReportParsePeer::TITLE_ID, $this->getId());

		return ReportParsePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addReportParse(ReportParse $l)
	{
		$this->collReportParses[] = $l;
		$l->setTitle($this);
	}

	
	public function initSchedules()
	{
		if ($this->collSchedules === null) {
			$this->collSchedules = array();
		}
	}

	
	public function getSchedules($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseSchedulePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSchedules === null) {
			if ($this->isNew()) {
			   $this->collSchedules = array();
			} else {

				$criteria->add(SchedulePeer::TITLE_ID, $this->getId());

				SchedulePeer::addSelectColumns($criteria);
				$this->collSchedules = SchedulePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(SchedulePeer::TITLE_ID, $this->getId());

				SchedulePeer::addSelectColumns($criteria);
				if (!isset($this->lastScheduleCriteria) || !$this->lastScheduleCriteria->equals($criteria)) {
					$this->collSchedules = SchedulePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastScheduleCriteria = $criteria;
		return $this->collSchedules;
	}

	
	public function countSchedules($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseSchedulePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(SchedulePeer::TITLE_ID, $this->getId());

		return SchedulePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addSchedule(Schedule $l)
	{
		$this->collSchedules[] = $l;
		$l->setTitle($this);
	}

	
	public function initTitleConnections()
	{
		if ($this->collTitleConnections === null) {
			$this->collTitleConnections = array();
		}
	}

	
	public function getTitleConnections($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitleConnectionPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitleConnections === null) {
			if ($this->isNew()) {
			   $this->collTitleConnections = array();
			} else {

				$criteria->add(TitleConnectionPeer::TITLE_ID, $this->getId());

				TitleConnectionPeer::addSelectColumns($criteria);
				$this->collTitleConnections = TitleConnectionPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(TitleConnectionPeer::TITLE_ID, $this->getId());

				TitleConnectionPeer::addSelectColumns($criteria);
				if (!isset($this->lastTitleConnectionCriteria) || !$this->lastTitleConnectionCriteria->equals($criteria)) {
					$this->collTitleConnections = TitleConnectionPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastTitleConnectionCriteria = $criteria;
		return $this->collTitleConnections;
	}

	
	public function countTitleConnections($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseTitleConnectionPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(TitleConnectionPeer::TITLE_ID, $this->getId());

		return TitleConnectionPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addTitleConnection(TitleConnection $l)
	{
		$this->collTitleConnections[] = $l;
		$l->setTitle($this);
	}

	
	public function initTitleContactLinks()
	{
		if ($this->collTitleContactLinks === null) {
			$this->collTitleContactLinks = array();
		}
	}

	
	public function getTitleContactLinks($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitleContactLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitleContactLinks === null) {
			if ($this->isNew()) {
			   $this->collTitleContactLinks = array();
			} else {

				$criteria->add(TitleContactLinkPeer::TITLE_ID, $this->getId());

				TitleContactLinkPeer::addSelectColumns($criteria);
				$this->collTitleContactLinks = TitleContactLinkPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(TitleContactLinkPeer::TITLE_ID, $this->getId());

				TitleContactLinkPeer::addSelectColumns($criteria);
				if (!isset($this->lastTitleContactLinkCriteria) || !$this->lastTitleContactLinkCriteria->equals($criteria)) {
					$this->collTitleContactLinks = TitleContactLinkPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastTitleContactLinkCriteria = $criteria;
		return $this->collTitleContactLinks;
	}

	
	public function countTitleContactLinks($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseTitleContactLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(TitleContactLinkPeer::TITLE_ID, $this->getId());

		return TitleContactLinkPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addTitleContactLink(TitleContactLink $l)
	{
		$this->collTitleContactLinks[] = $l;
		$l->setTitle($this);
	}


	
	public function getTitleContactLinksJoinContact($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitleContactLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitleContactLinks === null) {
			if ($this->isNew()) {
				$this->collTitleContactLinks = array();
			} else {

				$criteria->add(TitleContactLinkPeer::TITLE_ID, $this->getId());

				$this->collTitleContactLinks = TitleContactLinkPeer::doSelectJoinContact($criteria, $con);
			}
		} else {
									
			$criteria->add(TitleContactLinkPeer::TITLE_ID, $this->getId());

			if (!isset($this->lastTitleContactLinkCriteria) || !$this->lastTitleContactLinkCriteria->equals($criteria)) {
				$this->collTitleContactLinks = TitleContactLinkPeer::doSelectJoinContact($criteria, $con);
			}
		}
		$this->lastTitleContactLinkCriteria = $criteria;

		return $this->collTitleContactLinks;
	}

	
	public function initTitleOfficeLinks()
	{
		if ($this->collTitleOfficeLinks === null) {
			$this->collTitleOfficeLinks = array();
		}
	}

	
	public function getTitleOfficeLinks($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitleOfficeLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitleOfficeLinks === null) {
			if ($this->isNew()) {
			   $this->collTitleOfficeLinks = array();
			} else {

				$criteria->add(TitleOfficeLinkPeer::TITLE_ID, $this->getId());

				TitleOfficeLinkPeer::addSelectColumns($criteria);
				$this->collTitleOfficeLinks = TitleOfficeLinkPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(TitleOfficeLinkPeer::TITLE_ID, $this->getId());

				TitleOfficeLinkPeer::addSelectColumns($criteria);
				if (!isset($this->lastTitleOfficeLinkCriteria) || !$this->lastTitleOfficeLinkCriteria->equals($criteria)) {
					$this->collTitleOfficeLinks = TitleOfficeLinkPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastTitleOfficeLinkCriteria = $criteria;
		return $this->collTitleOfficeLinks;
	}

	
	public function countTitleOfficeLinks($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseTitleOfficeLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(TitleOfficeLinkPeer::TITLE_ID, $this->getId());

		return TitleOfficeLinkPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addTitleOfficeLink(TitleOfficeLink $l)
	{
		$this->collTitleOfficeLinks[] = $l;
		$l->setTitle($this);
	}


	
	public function getTitleOfficeLinksJoinOffice($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitleOfficeLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitleOfficeLinks === null) {
			if ($this->isNew()) {
				$this->collTitleOfficeLinks = array();
			} else {

				$criteria->add(TitleOfficeLinkPeer::TITLE_ID, $this->getId());

				$this->collTitleOfficeLinks = TitleOfficeLinkPeer::doSelectJoinOffice($criteria, $con);
			}
		} else {
									
			$criteria->add(TitleOfficeLinkPeer::TITLE_ID, $this->getId());

			if (!isset($this->lastTitleOfficeLinkCriteria) || !$this->lastTitleOfficeLinkCriteria->equals($criteria)) {
				$this->collTitleOfficeLinks = TitleOfficeLinkPeer::doSelectJoinOffice($criteria, $con);
			}
		}
		$this->lastTitleOfficeLinkCriteria = $criteria;

		return $this->collTitleOfficeLinks;
	}

	
	public function initOrdersFrequencys()
	{
		if ($this->collOrdersFrequencys === null) {
			$this->collOrdersFrequencys = array();
		}
	}

	
	public function getOrdersFrequencys($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseOrdersFrequencyPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collOrdersFrequencys === null) {
			if ($this->isNew()) {
			   $this->collOrdersFrequencys = array();
			} else {

				$criteria->add(OrdersFrequencyPeer::TITLE_ID, $this->getId());

				OrdersFrequencyPeer::addSelectColumns($criteria);
				$this->collOrdersFrequencys = OrdersFrequencyPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(OrdersFrequencyPeer::TITLE_ID, $this->getId());

				OrdersFrequencyPeer::addSelectColumns($criteria);
				if (!isset($this->lastOrdersFrequencyCriteria) || !$this->lastOrdersFrequencyCriteria->equals($criteria)) {
					$this->collOrdersFrequencys = OrdersFrequencyPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastOrdersFrequencyCriteria = $criteria;
		return $this->collOrdersFrequencys;
	}

	
	public function countOrdersFrequencys($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseOrdersFrequencyPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(OrdersFrequencyPeer::TITLE_ID, $this->getId());

		return OrdersFrequencyPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addOrdersFrequency(OrdersFrequency $l)
	{
		$this->collOrdersFrequencys[] = $l;
		$l->setTitle($this);
	}

} 