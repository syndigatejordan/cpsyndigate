<?php


abstract class BasePublisher extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $name;


	
	protected $website;


	
	protected $notes;


	
	protected $logo_path;


	
	protected $ftp_username;


	
	protected $ftp_password;


	
	protected $unix_gid;


	
	protected $unix_uid;


	
	protected $nagios_file_path;


	
	protected $country_id;


	
	protected $contract = 'No';


	
	protected $royalty_rate = 0;


	
	protected $contract_start_date;


	
	protected $contract_end_date;


	
	protected $renewal_period;


	
	protected $admins_id = 0;


	
	protected $accountant_notes;

	
	protected $aCountry;

	
	protected $aAdmins;

	
	protected $collBankDetailss;

	
	protected $lastBankDetailsCriteria = null;

	
	protected $collPublisherContactLinks;

	
	protected $lastPublisherContactLinkCriteria = null;

	
	protected $collPublisherOfficeLinks;

	
	protected $lastPublisherOfficeLinkCriteria = null;

	
	protected $collTitles;

	
	protected $lastTitleCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getName()
	{

		return $this->name;
	}

	
	public function getWebsite()
	{

		return $this->website;
	}

	
	public function getNotes()
	{

		return $this->notes;
	}

	
	public function getLogoPath()
	{

		return $this->logo_path;
	}

	
	public function getFtpUsername()
	{

		return $this->ftp_username;
	}

	
	public function getFtpPassword()
	{

		return $this->ftp_password;
	}

	
	public function getUnixGid()
	{

		return $this->unix_gid;
	}

	
	public function getUnixUid()
	{

		return $this->unix_uid;
	}

	
	public function getNagiosFilePath()
	{

		return $this->nagios_file_path;
	}

	
	public function getCountryId()
	{

		return $this->country_id;
	}

	
	public function getContract()
	{

		return $this->contract;
	}

	
	public function getRoyaltyRate()
	{

		return $this->royalty_rate;
	}

	
	public function getContractStartDate($format = 'Y-m-d')
	{

		if ($this->contract_start_date === null || $this->contract_start_date === '') {
			return null;
		} elseif (!is_int($this->contract_start_date)) {
						$ts = strtotime($this->contract_start_date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [contract_start_date] as date/time value: " . var_export($this->contract_start_date, true));
			}
		} else {
			$ts = $this->contract_start_date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getContractEndDate($format = 'Y-m-d')
	{

		if ($this->contract_end_date === null || $this->contract_end_date === '') {
			return null;
		} elseif (!is_int($this->contract_end_date)) {
						$ts = strtotime($this->contract_end_date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [contract_end_date] as date/time value: " . var_export($this->contract_end_date, true));
			}
		} else {
			$ts = $this->contract_end_date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getRenewalPeriod()
	{

		return $this->renewal_period;
	}

	
	public function getAdminsId()
	{

		return $this->admins_id;
	}

	
	public function getAccountantNotes()
	{

		return $this->accountant_notes;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = PublisherPeer::ID;
		}

	} 
	
	public function setName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->name !== $v) {
			$this->name = $v;
			$this->modifiedColumns[] = PublisherPeer::NAME;
		}

	} 
	
	public function setWebsite($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->website !== $v) {
			$this->website = $v;
			$this->modifiedColumns[] = PublisherPeer::WEBSITE;
		}

	} 
	
	public function setNotes($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->notes !== $v) {
			$this->notes = $v;
			$this->modifiedColumns[] = PublisherPeer::NOTES;
		}

	} 
	
	public function setLogoPath($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->logo_path !== $v) {
			$this->logo_path = $v;
			$this->modifiedColumns[] = PublisherPeer::LOGO_PATH;
		}

	} 
	
	public function setFtpUsername($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ftp_username !== $v) {
			$this->ftp_username = $v;
			$this->modifiedColumns[] = PublisherPeer::FTP_USERNAME;
		}

	} 
	
	public function setFtpPassword($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ftp_password !== $v) {
			$this->ftp_password = $v;
			$this->modifiedColumns[] = PublisherPeer::FTP_PASSWORD;
		}

	} 
	
	public function setUnixGid($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->unix_gid !== $v) {
			$this->unix_gid = $v;
			$this->modifiedColumns[] = PublisherPeer::UNIX_GID;
		}

	} 
	
	public function setUnixUid($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->unix_uid !== $v) {
			$this->unix_uid = $v;
			$this->modifiedColumns[] = PublisherPeer::UNIX_UID;
		}

	} 
	
	public function setNagiosFilePath($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nagios_file_path !== $v) {
			$this->nagios_file_path = $v;
			$this->modifiedColumns[] = PublisherPeer::NAGIOS_FILE_PATH;
		}

	} 
	
	public function setCountryId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->country_id !== $v) {
			$this->country_id = $v;
			$this->modifiedColumns[] = PublisherPeer::COUNTRY_ID;
		}

		if ($this->aCountry !== null && $this->aCountry->getId() !== $v) {
			$this->aCountry = null;
		}

	} 
	
	public function setContract($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->contract !== $v || $v === 'No') {
			$this->contract = $v;
			$this->modifiedColumns[] = PublisherPeer::CONTRACT;
		}

	} 
	
	public function setRoyaltyRate($v)
	{

		if ($this->royalty_rate !== $v || $v === 0) {
			$this->royalty_rate = $v;
			$this->modifiedColumns[] = PublisherPeer::ROYALTY_RATE;
		}

	} 
	
	public function setContractStartDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [contract_start_date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->contract_start_date !== $ts) {
			$this->contract_start_date = $ts;
			$this->modifiedColumns[] = PublisherPeer::CONTRACT_START_DATE;
		}

	} 
	
	public function setContractEndDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [contract_end_date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->contract_end_date !== $ts) {
			$this->contract_end_date = $ts;
			$this->modifiedColumns[] = PublisherPeer::CONTRACT_END_DATE;
		}

	} 
	
	public function setRenewalPeriod($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->renewal_period !== $v) {
			$this->renewal_period = $v;
			$this->modifiedColumns[] = PublisherPeer::RENEWAL_PERIOD;
		}

	} 
	
	public function setAdminsId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->admins_id !== $v || $v === 0) {
			$this->admins_id = $v;
			$this->modifiedColumns[] = PublisherPeer::ADMINS_ID;
		}

		if ($this->aAdmins !== null && $this->aAdmins->getId() !== $v) {
			$this->aAdmins = null;
		}

	} 
	
	public function setAccountantNotes($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->accountant_notes !== $v) {
			$this->accountant_notes = $v;
			$this->modifiedColumns[] = PublisherPeer::ACCOUNTANT_NOTES;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->name = $rs->getString($startcol + 1);

			$this->website = $rs->getString($startcol + 2);

			$this->notes = $rs->getString($startcol + 3);

			$this->logo_path = $rs->getString($startcol + 4);

			$this->ftp_username = $rs->getString($startcol + 5);

			$this->ftp_password = $rs->getString($startcol + 6);

			$this->unix_gid = $rs->getInt($startcol + 7);

			$this->unix_uid = $rs->getInt($startcol + 8);

			$this->nagios_file_path = $rs->getString($startcol + 9);

			$this->country_id = $rs->getInt($startcol + 10);

			$this->contract = $rs->getString($startcol + 11);

			$this->royalty_rate = $rs->getFloat($startcol + 12);

			$this->contract_start_date = $rs->getDate($startcol + 13, null);

			$this->contract_end_date = $rs->getDate($startcol + 14, null);

			$this->renewal_period = $rs->getString($startcol + 15);

			$this->admins_id = $rs->getInt($startcol + 16);

			$this->accountant_notes = $rs->getString($startcol + 17);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 18; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Publisher object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PublisherPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PublisherPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PublisherPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aCountry !== null) {
				if ($this->aCountry->isModified()) {
					$affectedRows += $this->aCountry->save($con);
				}
				$this->setCountry($this->aCountry);
			}

			if ($this->aAdmins !== null) {
				if ($this->aAdmins->isModified()) {
					$affectedRows += $this->aAdmins->save($con);
				}
				$this->setAdmins($this->aAdmins);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PublisherPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += PublisherPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collBankDetailss !== null) {
				foreach($this->collBankDetailss as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collPublisherContactLinks !== null) {
				foreach($this->collPublisherContactLinks as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collPublisherOfficeLinks !== null) {
				foreach($this->collPublisherOfficeLinks as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collTitles !== null) {
				foreach($this->collTitles as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aCountry !== null) {
				if (!$this->aCountry->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCountry->getValidationFailures());
				}
			}

			if ($this->aAdmins !== null) {
				if (!$this->aAdmins->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aAdmins->getValidationFailures());
				}
			}


			if (($retval = PublisherPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collBankDetailss !== null) {
					foreach($this->collBankDetailss as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collPublisherContactLinks !== null) {
					foreach($this->collPublisherContactLinks as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collPublisherOfficeLinks !== null) {
					foreach($this->collPublisherOfficeLinks as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collTitles !== null) {
					foreach($this->collTitles as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PublisherPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getName();
				break;
			case 2:
				return $this->getWebsite();
				break;
			case 3:
				return $this->getNotes();
				break;
			case 4:
				return $this->getLogoPath();
				break;
			case 5:
				return $this->getFtpUsername();
				break;
			case 6:
				return $this->getFtpPassword();
				break;
			case 7:
				return $this->getUnixGid();
				break;
			case 8:
				return $this->getUnixUid();
				break;
			case 9:
				return $this->getNagiosFilePath();
				break;
			case 10:
				return $this->getCountryId();
				break;
			case 11:
				return $this->getContract();
				break;
			case 12:
				return $this->getRoyaltyRate();
				break;
			case 13:
				return $this->getContractStartDate();
				break;
			case 14:
				return $this->getContractEndDate();
				break;
			case 15:
				return $this->getRenewalPeriod();
				break;
			case 16:
				return $this->getAdminsId();
				break;
			case 17:
				return $this->getAccountantNotes();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PublisherPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getName(),
			$keys[2] => $this->getWebsite(),
			$keys[3] => $this->getNotes(),
			$keys[4] => $this->getLogoPath(),
			$keys[5] => $this->getFtpUsername(),
			$keys[6] => $this->getFtpPassword(),
			$keys[7] => $this->getUnixGid(),
			$keys[8] => $this->getUnixUid(),
			$keys[9] => $this->getNagiosFilePath(),
			$keys[10] => $this->getCountryId(),
			$keys[11] => $this->getContract(),
			$keys[12] => $this->getRoyaltyRate(),
			$keys[13] => $this->getContractStartDate(),
			$keys[14] => $this->getContractEndDate(),
			$keys[15] => $this->getRenewalPeriod(),
			$keys[16] => $this->getAdminsId(),
			$keys[17] => $this->getAccountantNotes(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PublisherPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setName($value);
				break;
			case 2:
				$this->setWebsite($value);
				break;
			case 3:
				$this->setNotes($value);
				break;
			case 4:
				$this->setLogoPath($value);
				break;
			case 5:
				$this->setFtpUsername($value);
				break;
			case 6:
				$this->setFtpPassword($value);
				break;
			case 7:
				$this->setUnixGid($value);
				break;
			case 8:
				$this->setUnixUid($value);
				break;
			case 9:
				$this->setNagiosFilePath($value);
				break;
			case 10:
				$this->setCountryId($value);
				break;
			case 11:
				$this->setContract($value);
				break;
			case 12:
				$this->setRoyaltyRate($value);
				break;
			case 13:
				$this->setContractStartDate($value);
				break;
			case 14:
				$this->setContractEndDate($value);
				break;
			case 15:
				$this->setRenewalPeriod($value);
				break;
			case 16:
				$this->setAdminsId($value);
				break;
			case 17:
				$this->setAccountantNotes($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PublisherPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setName($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setWebsite($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setNotes($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setLogoPath($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setFtpUsername($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setFtpPassword($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setUnixGid($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setUnixUid($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setNagiosFilePath($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setCountryId($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setContract($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setRoyaltyRate($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setContractStartDate($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setContractEndDate($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setRenewalPeriod($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setAdminsId($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setAccountantNotes($arr[$keys[17]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PublisherPeer::DATABASE_NAME);

		if ($this->isColumnModified(PublisherPeer::ID)) $criteria->add(PublisherPeer::ID, $this->id);
		if ($this->isColumnModified(PublisherPeer::NAME)) $criteria->add(PublisherPeer::NAME, $this->name);
		if ($this->isColumnModified(PublisherPeer::WEBSITE)) $criteria->add(PublisherPeer::WEBSITE, $this->website);
		if ($this->isColumnModified(PublisherPeer::NOTES)) $criteria->add(PublisherPeer::NOTES, $this->notes);
		if ($this->isColumnModified(PublisherPeer::LOGO_PATH)) $criteria->add(PublisherPeer::LOGO_PATH, $this->logo_path);
		if ($this->isColumnModified(PublisherPeer::FTP_USERNAME)) $criteria->add(PublisherPeer::FTP_USERNAME, $this->ftp_username);
		if ($this->isColumnModified(PublisherPeer::FTP_PASSWORD)) $criteria->add(PublisherPeer::FTP_PASSWORD, $this->ftp_password);
		if ($this->isColumnModified(PublisherPeer::UNIX_GID)) $criteria->add(PublisherPeer::UNIX_GID, $this->unix_gid);
		if ($this->isColumnModified(PublisherPeer::UNIX_UID)) $criteria->add(PublisherPeer::UNIX_UID, $this->unix_uid);
		if ($this->isColumnModified(PublisherPeer::NAGIOS_FILE_PATH)) $criteria->add(PublisherPeer::NAGIOS_FILE_PATH, $this->nagios_file_path);
		if ($this->isColumnModified(PublisherPeer::COUNTRY_ID)) $criteria->add(PublisherPeer::COUNTRY_ID, $this->country_id);
		if ($this->isColumnModified(PublisherPeer::CONTRACT)) $criteria->add(PublisherPeer::CONTRACT, $this->contract);
		if ($this->isColumnModified(PublisherPeer::ROYALTY_RATE)) $criteria->add(PublisherPeer::ROYALTY_RATE, $this->royalty_rate);
		if ($this->isColumnModified(PublisherPeer::CONTRACT_START_DATE)) $criteria->add(PublisherPeer::CONTRACT_START_DATE, $this->contract_start_date);
		if ($this->isColumnModified(PublisherPeer::CONTRACT_END_DATE)) $criteria->add(PublisherPeer::CONTRACT_END_DATE, $this->contract_end_date);
		if ($this->isColumnModified(PublisherPeer::RENEWAL_PERIOD)) $criteria->add(PublisherPeer::RENEWAL_PERIOD, $this->renewal_period);
		if ($this->isColumnModified(PublisherPeer::ADMINS_ID)) $criteria->add(PublisherPeer::ADMINS_ID, $this->admins_id);
		if ($this->isColumnModified(PublisherPeer::ACCOUNTANT_NOTES)) $criteria->add(PublisherPeer::ACCOUNTANT_NOTES, $this->accountant_notes);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PublisherPeer::DATABASE_NAME);

		$criteria->add(PublisherPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setName($this->name);

		$copyObj->setWebsite($this->website);

		$copyObj->setNotes($this->notes);

		$copyObj->setLogoPath($this->logo_path);

		$copyObj->setFtpUsername($this->ftp_username);

		$copyObj->setFtpPassword($this->ftp_password);

		$copyObj->setUnixGid($this->unix_gid);

		$copyObj->setUnixUid($this->unix_uid);

		$copyObj->setNagiosFilePath($this->nagios_file_path);

		$copyObj->setCountryId($this->country_id);

		$copyObj->setContract($this->contract);

		$copyObj->setRoyaltyRate($this->royalty_rate);

		$copyObj->setContractStartDate($this->contract_start_date);

		$copyObj->setContractEndDate($this->contract_end_date);

		$copyObj->setRenewalPeriod($this->renewal_period);

		$copyObj->setAdminsId($this->admins_id);

		$copyObj->setAccountantNotes($this->accountant_notes);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getBankDetailss() as $relObj) {
				$copyObj->addBankDetails($relObj->copy($deepCopy));
			}

			foreach($this->getPublisherContactLinks() as $relObj) {
				$copyObj->addPublisherContactLink($relObj->copy($deepCopy));
			}

			foreach($this->getPublisherOfficeLinks() as $relObj) {
				$copyObj->addPublisherOfficeLink($relObj->copy($deepCopy));
			}

			foreach($this->getTitles() as $relObj) {
				$copyObj->addTitle($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PublisherPeer();
		}
		return self::$peer;
	}

	
	public function setCountry($v)
	{


		if ($v === null) {
			$this->setCountryId(NULL);
		} else {
			$this->setCountryId($v->getId());
		}


		$this->aCountry = $v;
	}


	
	public function getCountry($con = null)
	{
		if ($this->aCountry === null && ($this->country_id !== null)) {
						include_once 'lib/model/om/BaseCountryPeer.php';

			$this->aCountry = CountryPeer::retrieveByPK($this->country_id, $con);

			
		}
		return $this->aCountry;
	}

	
	public function setAdmins($v)
	{


		if ($v === null) {
			$this->setAdminsId('0');
		} else {
			$this->setAdminsId($v->getId());
		}


		$this->aAdmins = $v;
	}


	
	public function getAdmins($con = null)
	{
		if ($this->aAdmins === null && ($this->admins_id !== null)) {
						include_once 'lib/model/om/BaseAdminsPeer.php';

			$this->aAdmins = AdminsPeer::retrieveByPK($this->admins_id, $con);

			
		}
		return $this->aAdmins;
	}

	
	public function initBankDetailss()
	{
		if ($this->collBankDetailss === null) {
			$this->collBankDetailss = array();
		}
	}

	
	public function getBankDetailss($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseBankDetailsPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collBankDetailss === null) {
			if ($this->isNew()) {
			   $this->collBankDetailss = array();
			} else {

				$criteria->add(BankDetailsPeer::PUBLISHER_ID, $this->getId());

				BankDetailsPeer::addSelectColumns($criteria);
				$this->collBankDetailss = BankDetailsPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(BankDetailsPeer::PUBLISHER_ID, $this->getId());

				BankDetailsPeer::addSelectColumns($criteria);
				if (!isset($this->lastBankDetailsCriteria) || !$this->lastBankDetailsCriteria->equals($criteria)) {
					$this->collBankDetailss = BankDetailsPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastBankDetailsCriteria = $criteria;
		return $this->collBankDetailss;
	}

	
	public function countBankDetailss($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseBankDetailsPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(BankDetailsPeer::PUBLISHER_ID, $this->getId());

		return BankDetailsPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addBankDetails(BankDetails $l)
	{
		$this->collBankDetailss[] = $l;
		$l->setPublisher($this);
	}


	
	public function getBankDetailssJoinCountry($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseBankDetailsPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collBankDetailss === null) {
			if ($this->isNew()) {
				$this->collBankDetailss = array();
			} else {

				$criteria->add(BankDetailsPeer::PUBLISHER_ID, $this->getId());

				$this->collBankDetailss = BankDetailsPeer::doSelectJoinCountry($criteria, $con);
			}
		} else {
									
			$criteria->add(BankDetailsPeer::PUBLISHER_ID, $this->getId());

			if (!isset($this->lastBankDetailsCriteria) || !$this->lastBankDetailsCriteria->equals($criteria)) {
				$this->collBankDetailss = BankDetailsPeer::doSelectJoinCountry($criteria, $con);
			}
		}
		$this->lastBankDetailsCriteria = $criteria;

		return $this->collBankDetailss;
	}

	
	public function initPublisherContactLinks()
	{
		if ($this->collPublisherContactLinks === null) {
			$this->collPublisherContactLinks = array();
		}
	}

	
	public function getPublisherContactLinks($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BasePublisherContactLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collPublisherContactLinks === null) {
			if ($this->isNew()) {
			   $this->collPublisherContactLinks = array();
			} else {

				$criteria->add(PublisherContactLinkPeer::PUBLISHER_ID, $this->getId());

				PublisherContactLinkPeer::addSelectColumns($criteria);
				$this->collPublisherContactLinks = PublisherContactLinkPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(PublisherContactLinkPeer::PUBLISHER_ID, $this->getId());

				PublisherContactLinkPeer::addSelectColumns($criteria);
				if (!isset($this->lastPublisherContactLinkCriteria) || !$this->lastPublisherContactLinkCriteria->equals($criteria)) {
					$this->collPublisherContactLinks = PublisherContactLinkPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastPublisherContactLinkCriteria = $criteria;
		return $this->collPublisherContactLinks;
	}

	
	public function countPublisherContactLinks($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BasePublisherContactLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(PublisherContactLinkPeer::PUBLISHER_ID, $this->getId());

		return PublisherContactLinkPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addPublisherContactLink(PublisherContactLink $l)
	{
		$this->collPublisherContactLinks[] = $l;
		$l->setPublisher($this);
	}


	
	public function getPublisherContactLinksJoinContact($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BasePublisherContactLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collPublisherContactLinks === null) {
			if ($this->isNew()) {
				$this->collPublisherContactLinks = array();
			} else {

				$criteria->add(PublisherContactLinkPeer::PUBLISHER_ID, $this->getId());

				$this->collPublisherContactLinks = PublisherContactLinkPeer::doSelectJoinContact($criteria, $con);
			}
		} else {
									
			$criteria->add(PublisherContactLinkPeer::PUBLISHER_ID, $this->getId());

			if (!isset($this->lastPublisherContactLinkCriteria) || !$this->lastPublisherContactLinkCriteria->equals($criteria)) {
				$this->collPublisherContactLinks = PublisherContactLinkPeer::doSelectJoinContact($criteria, $con);
			}
		}
		$this->lastPublisherContactLinkCriteria = $criteria;

		return $this->collPublisherContactLinks;
	}

	
	public function initPublisherOfficeLinks()
	{
		if ($this->collPublisherOfficeLinks === null) {
			$this->collPublisherOfficeLinks = array();
		}
	}

	
	public function getPublisherOfficeLinks($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BasePublisherOfficeLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collPublisherOfficeLinks === null) {
			if ($this->isNew()) {
			   $this->collPublisherOfficeLinks = array();
			} else {

				$criteria->add(PublisherOfficeLinkPeer::PUBLISHER_ID, $this->getId());

				PublisherOfficeLinkPeer::addSelectColumns($criteria);
				$this->collPublisherOfficeLinks = PublisherOfficeLinkPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(PublisherOfficeLinkPeer::PUBLISHER_ID, $this->getId());

				PublisherOfficeLinkPeer::addSelectColumns($criteria);
				if (!isset($this->lastPublisherOfficeLinkCriteria) || !$this->lastPublisherOfficeLinkCriteria->equals($criteria)) {
					$this->collPublisherOfficeLinks = PublisherOfficeLinkPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastPublisherOfficeLinkCriteria = $criteria;
		return $this->collPublisherOfficeLinks;
	}

	
	public function countPublisherOfficeLinks($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BasePublisherOfficeLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(PublisherOfficeLinkPeer::PUBLISHER_ID, $this->getId());

		return PublisherOfficeLinkPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addPublisherOfficeLink(PublisherOfficeLink $l)
	{
		$this->collPublisherOfficeLinks[] = $l;
		$l->setPublisher($this);
	}


	
	public function getPublisherOfficeLinksJoinOffice($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BasePublisherOfficeLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collPublisherOfficeLinks === null) {
			if ($this->isNew()) {
				$this->collPublisherOfficeLinks = array();
			} else {

				$criteria->add(PublisherOfficeLinkPeer::PUBLISHER_ID, $this->getId());

				$this->collPublisherOfficeLinks = PublisherOfficeLinkPeer::doSelectJoinOffice($criteria, $con);
			}
		} else {
									
			$criteria->add(PublisherOfficeLinkPeer::PUBLISHER_ID, $this->getId());

			if (!isset($this->lastPublisherOfficeLinkCriteria) || !$this->lastPublisherOfficeLinkCriteria->equals($criteria)) {
				$this->collPublisherOfficeLinks = PublisherOfficeLinkPeer::doSelectJoinOffice($criteria, $con);
			}
		}
		$this->lastPublisherOfficeLinkCriteria = $criteria;

		return $this->collPublisherOfficeLinks;
	}

	
	public function initTitles()
	{
		if ($this->collTitles === null) {
			$this->collTitles = array();
		}
	}

	
	public function getTitles($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
			   $this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::PUBLISHER_ID, $this->getId());

				TitlePeer::addSelectColumns($criteria);
				$this->collTitles = TitlePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(TitlePeer::PUBLISHER_ID, $this->getId());

				TitlePeer::addSelectColumns($criteria);
				if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
					$this->collTitles = TitlePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastTitleCriteria = $criteria;
		return $this->collTitles;
	}

	
	public function countTitles($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(TitlePeer::PUBLISHER_ID, $this->getId());

		return TitlePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addTitle(Title $l)
	{
		$this->collTitles[] = $l;
		$l->setPublisher($this);
	}


	
	public function getTitlesJoinTitleType($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::PUBLISHER_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinTitleType($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::PUBLISHER_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinTitleType($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinTitleFrequency($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::PUBLISHER_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinTitleFrequency($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::PUBLISHER_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinTitleFrequency($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinCountry($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::PUBLISHER_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinCountry($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::PUBLISHER_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinCountry($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinLanguage($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::PUBLISHER_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinLanguage($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::PUBLISHER_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinLanguage($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinAdmins($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::PUBLISHER_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinAdmins($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::PUBLISHER_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinAdmins($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}

} 