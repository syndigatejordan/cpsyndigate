<?php


abstract class BaseReportGenerate extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $title_id = 0;


	
	protected $file = '';


	
	protected $articles_count = 0;


	
	protected $error;


	
	protected $time = -62169984000;


	
	protected $status = 0;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getTitleId()
	{

		return $this->title_id;
	}

	
	public function getFile()
	{

		return $this->file;
	}

	
	public function getArticlesCount()
	{

		return $this->articles_count;
	}

	
	public function getError()
	{

		return $this->error;
	}

	
	public function getTime($format = 'Y-m-d H:i:s')
	{

		if ($this->time === null || $this->time === '') {
			return null;
		} elseif (!is_int($this->time)) {
						$ts = strtotime($this->time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [time] as date/time value: " . var_export($this->time, true));
			}
		} else {
			$ts = $this->time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getStatus()
	{

		return $this->status;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ReportGeneratePeer::ID;
		}

	} 
	
	public function setTitleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_id !== $v || $v === 0) {
			$this->title_id = $v;
			$this->modifiedColumns[] = ReportGeneratePeer::TITLE_ID;
		}

	} 
	
	public function setFile($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->file !== $v || $v === '') {
			$this->file = $v;
			$this->modifiedColumns[] = ReportGeneratePeer::FILE;
		}

	} 
	
	public function setArticlesCount($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->articles_count !== $v || $v === 0) {
			$this->articles_count = $v;
			$this->modifiedColumns[] = ReportGeneratePeer::ARTICLES_COUNT;
		}

	} 
	
	public function setError($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->error !== $v) {
			$this->error = $v;
			$this->modifiedColumns[] = ReportGeneratePeer::ERROR;
		}

	} 
	
	public function setTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->time !== $ts || $ts === -62169984000) {
			$this->time = $ts;
			$this->modifiedColumns[] = ReportGeneratePeer::TIME;
		}

	} 
	
	public function setStatus($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->status !== $v || $v === 0) {
			$this->status = $v;
			$this->modifiedColumns[] = ReportGeneratePeer::STATUS;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->title_id = $rs->getInt($startcol + 1);

			$this->file = $rs->getString($startcol + 2);

			$this->articles_count = $rs->getInt($startcol + 3);

			$this->error = $rs->getString($startcol + 4);

			$this->time = $rs->getTimestamp($startcol + 5, null);

			$this->status = $rs->getInt($startcol + 6);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 7; 
		} catch (Exception $e) {
			throw new PropelException("Error populating ReportGenerate object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ReportGeneratePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ReportGeneratePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ReportGeneratePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ReportGeneratePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ReportGeneratePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = ReportGeneratePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ReportGeneratePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTitleId();
				break;
			case 2:
				return $this->getFile();
				break;
			case 3:
				return $this->getArticlesCount();
				break;
			case 4:
				return $this->getError();
				break;
			case 5:
				return $this->getTime();
				break;
			case 6:
				return $this->getStatus();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ReportGeneratePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTitleId(),
			$keys[2] => $this->getFile(),
			$keys[3] => $this->getArticlesCount(),
			$keys[4] => $this->getError(),
			$keys[5] => $this->getTime(),
			$keys[6] => $this->getStatus(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ReportGeneratePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTitleId($value);
				break;
			case 2:
				$this->setFile($value);
				break;
			case 3:
				$this->setArticlesCount($value);
				break;
			case 4:
				$this->setError($value);
				break;
			case 5:
				$this->setTime($value);
				break;
			case 6:
				$this->setStatus($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ReportGeneratePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTitleId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setFile($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setArticlesCount($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setError($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setTime($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setStatus($arr[$keys[6]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ReportGeneratePeer::DATABASE_NAME);

		if ($this->isColumnModified(ReportGeneratePeer::ID)) $criteria->add(ReportGeneratePeer::ID, $this->id);
		if ($this->isColumnModified(ReportGeneratePeer::TITLE_ID)) $criteria->add(ReportGeneratePeer::TITLE_ID, $this->title_id);
		if ($this->isColumnModified(ReportGeneratePeer::FILE)) $criteria->add(ReportGeneratePeer::FILE, $this->file);
		if ($this->isColumnModified(ReportGeneratePeer::ARTICLES_COUNT)) $criteria->add(ReportGeneratePeer::ARTICLES_COUNT, $this->articles_count);
		if ($this->isColumnModified(ReportGeneratePeer::ERROR)) $criteria->add(ReportGeneratePeer::ERROR, $this->error);
		if ($this->isColumnModified(ReportGeneratePeer::TIME)) $criteria->add(ReportGeneratePeer::TIME, $this->time);
		if ($this->isColumnModified(ReportGeneratePeer::STATUS)) $criteria->add(ReportGeneratePeer::STATUS, $this->status);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ReportGeneratePeer::DATABASE_NAME);

		$criteria->add(ReportGeneratePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTitleId($this->title_id);

		$copyObj->setFile($this->file);

		$copyObj->setArticlesCount($this->articles_count);

		$copyObj->setError($this->error);

		$copyObj->setTime($this->time);

		$copyObj->setStatus($this->status);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ReportGeneratePeer();
		}
		return self::$peer;
	}

} 