<?php


abstract class BaseArticleOriginalData extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $original_article_id;


	
	protected $original_source;


	
	protected $issue_number;


	
	protected $page_number;


	
	protected $reference;


	
	protected $extras;


	
	protected $hijri_date;


	
	protected $original_cat_id;


	
	protected $revision_num;

	
	protected $collArticles;

	
	protected $lastArticleCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getOriginalArticleId()
	{

		return $this->original_article_id;
	}

	
	public function getOriginalSource()
	{

		return $this->original_source;
	}

	
	public function getIssueNumber()
	{

		return $this->issue_number;
	}

	
	public function getPageNumber()
	{

		return $this->page_number;
	}

	
	public function getReference()
	{

		return $this->reference;
	}

	
	public function getExtras()
	{

		return $this->extras;
	}

	
	public function getHijriDate()
	{

		return $this->hijri_date;
	}

	
	public function getOriginalCatId()
	{

		return $this->original_cat_id;
	}

	
	public function getRevisionNum()
	{

		return $this->revision_num;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ArticleOriginalDataPeer::ID;
		}

	} 
	
	public function setOriginalArticleId($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->original_article_id !== $v) {
			$this->original_article_id = $v;
			$this->modifiedColumns[] = ArticleOriginalDataPeer::ORIGINAL_ARTICLE_ID;
		}

	} 
	
	public function setOriginalSource($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->original_source !== $v) {
			$this->original_source = $v;
			$this->modifiedColumns[] = ArticleOriginalDataPeer::ORIGINAL_SOURCE;
		}

	} 
	
	public function setIssueNumber($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->issue_number !== $v) {
			$this->issue_number = $v;
			$this->modifiedColumns[] = ArticleOriginalDataPeer::ISSUE_NUMBER;
		}

	} 
	
	public function setPageNumber($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->page_number !== $v) {
			$this->page_number = $v;
			$this->modifiedColumns[] = ArticleOriginalDataPeer::PAGE_NUMBER;
		}

	} 
	
	public function setReference($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->reference !== $v) {
			$this->reference = $v;
			$this->modifiedColumns[] = ArticleOriginalDataPeer::REFERENCE;
		}

	} 
	
	public function setExtras($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->extras !== $v) {
			$this->extras = $v;
			$this->modifiedColumns[] = ArticleOriginalDataPeer::EXTRAS;
		}

	} 
	
	public function setHijriDate($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->hijri_date !== $v) {
			$this->hijri_date = $v;
			$this->modifiedColumns[] = ArticleOriginalDataPeer::HIJRI_DATE;
		}

	} 
	
	public function setOriginalCatId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->original_cat_id !== $v) {
			$this->original_cat_id = $v;
			$this->modifiedColumns[] = ArticleOriginalDataPeer::ORIGINAL_CAT_ID;
		}

	} 
	
	public function setRevisionNum($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->revision_num !== $v) {
			$this->revision_num = $v;
			$this->modifiedColumns[] = ArticleOriginalDataPeer::REVISION_NUM;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->original_article_id = $rs->getString($startcol + 1);

			$this->original_source = $rs->getString($startcol + 2);

			$this->issue_number = $rs->getInt($startcol + 3);

			$this->page_number = $rs->getInt($startcol + 4);

			$this->reference = $rs->getString($startcol + 5);

			$this->extras = $rs->getString($startcol + 6);

			$this->hijri_date = $rs->getString($startcol + 7);

			$this->original_cat_id = $rs->getInt($startcol + 8);

			$this->revision_num = $rs->getInt($startcol + 9);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 10; 
		} catch (Exception $e) {
			throw new PropelException("Error populating ArticleOriginalData object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ArticleOriginalDataPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ArticleOriginalDataPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ArticleOriginalDataPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ArticleOriginalDataPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ArticleOriginalDataPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collArticles !== null) {
				foreach($this->collArticles as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = ArticleOriginalDataPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collArticles !== null) {
					foreach($this->collArticles as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ArticleOriginalDataPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getOriginalArticleId();
				break;
			case 2:
				return $this->getOriginalSource();
				break;
			case 3:
				return $this->getIssueNumber();
				break;
			case 4:
				return $this->getPageNumber();
				break;
			case 5:
				return $this->getReference();
				break;
			case 6:
				return $this->getExtras();
				break;
			case 7:
				return $this->getHijriDate();
				break;
			case 8:
				return $this->getOriginalCatId();
				break;
			case 9:
				return $this->getRevisionNum();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ArticleOriginalDataPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getOriginalArticleId(),
			$keys[2] => $this->getOriginalSource(),
			$keys[3] => $this->getIssueNumber(),
			$keys[4] => $this->getPageNumber(),
			$keys[5] => $this->getReference(),
			$keys[6] => $this->getExtras(),
			$keys[7] => $this->getHijriDate(),
			$keys[8] => $this->getOriginalCatId(),
			$keys[9] => $this->getRevisionNum(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ArticleOriginalDataPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setOriginalArticleId($value);
				break;
			case 2:
				$this->setOriginalSource($value);
				break;
			case 3:
				$this->setIssueNumber($value);
				break;
			case 4:
				$this->setPageNumber($value);
				break;
			case 5:
				$this->setReference($value);
				break;
			case 6:
				$this->setExtras($value);
				break;
			case 7:
				$this->setHijriDate($value);
				break;
			case 8:
				$this->setOriginalCatId($value);
				break;
			case 9:
				$this->setRevisionNum($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ArticleOriginalDataPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setOriginalArticleId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setOriginalSource($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setIssueNumber($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setPageNumber($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setReference($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setExtras($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setHijriDate($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setOriginalCatId($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setRevisionNum($arr[$keys[9]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ArticleOriginalDataPeer::DATABASE_NAME);

		if ($this->isColumnModified(ArticleOriginalDataPeer::ID)) $criteria->add(ArticleOriginalDataPeer::ID, $this->id);
		if ($this->isColumnModified(ArticleOriginalDataPeer::ORIGINAL_ARTICLE_ID)) $criteria->add(ArticleOriginalDataPeer::ORIGINAL_ARTICLE_ID, $this->original_article_id);
		if ($this->isColumnModified(ArticleOriginalDataPeer::ORIGINAL_SOURCE)) $criteria->add(ArticleOriginalDataPeer::ORIGINAL_SOURCE, $this->original_source);
		if ($this->isColumnModified(ArticleOriginalDataPeer::ISSUE_NUMBER)) $criteria->add(ArticleOriginalDataPeer::ISSUE_NUMBER, $this->issue_number);
		if ($this->isColumnModified(ArticleOriginalDataPeer::PAGE_NUMBER)) $criteria->add(ArticleOriginalDataPeer::PAGE_NUMBER, $this->page_number);
		if ($this->isColumnModified(ArticleOriginalDataPeer::REFERENCE)) $criteria->add(ArticleOriginalDataPeer::REFERENCE, $this->reference);
		if ($this->isColumnModified(ArticleOriginalDataPeer::EXTRAS)) $criteria->add(ArticleOriginalDataPeer::EXTRAS, $this->extras);
		if ($this->isColumnModified(ArticleOriginalDataPeer::HIJRI_DATE)) $criteria->add(ArticleOriginalDataPeer::HIJRI_DATE, $this->hijri_date);
		if ($this->isColumnModified(ArticleOriginalDataPeer::ORIGINAL_CAT_ID)) $criteria->add(ArticleOriginalDataPeer::ORIGINAL_CAT_ID, $this->original_cat_id);
		if ($this->isColumnModified(ArticleOriginalDataPeer::REVISION_NUM)) $criteria->add(ArticleOriginalDataPeer::REVISION_NUM, $this->revision_num);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ArticleOriginalDataPeer::DATABASE_NAME);

		$criteria->add(ArticleOriginalDataPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setOriginalArticleId($this->original_article_id);

		$copyObj->setOriginalSource($this->original_source);

		$copyObj->setIssueNumber($this->issue_number);

		$copyObj->setPageNumber($this->page_number);

		$copyObj->setReference($this->reference);

		$copyObj->setExtras($this->extras);

		$copyObj->setHijriDate($this->hijri_date);

		$copyObj->setOriginalCatId($this->original_cat_id);

		$copyObj->setRevisionNum($this->revision_num);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getArticles() as $relObj) {
				$copyObj->addArticle($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ArticleOriginalDataPeer();
		}
		return self::$peer;
	}

	
	public function initArticles()
	{
		if ($this->collArticles === null) {
			$this->collArticles = array();
		}
	}

	
	public function getArticles($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticles === null) {
			if ($this->isNew()) {
			   $this->collArticles = array();
			} else {

				$criteria->add(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, $this->getId());

				ArticlePeer::addSelectColumns($criteria);
				$this->collArticles = ArticlePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, $this->getId());

				ArticlePeer::addSelectColumns($criteria);
				if (!isset($this->lastArticleCriteria) || !$this->lastArticleCriteria->equals($criteria)) {
					$this->collArticles = ArticlePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastArticleCriteria = $criteria;
		return $this->collArticles;
	}

	
	public function countArticles($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, $this->getId());

		return ArticlePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addArticle(Article $l)
	{
		$this->collArticles[] = $l;
		$l->setArticleOriginalData($this);
	}


	
	public function getArticlesJoinLanguage($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticles === null) {
			if ($this->isNew()) {
				$this->collArticles = array();
			} else {

				$criteria->add(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, $this->getId());

				$this->collArticles = ArticlePeer::doSelectJoinLanguage($criteria, $con);
			}
		} else {
									
			$criteria->add(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, $this->getId());

			if (!isset($this->lastArticleCriteria) || !$this->lastArticleCriteria->equals($criteria)) {
				$this->collArticles = ArticlePeer::doSelectJoinLanguage($criteria, $con);
			}
		}
		$this->lastArticleCriteria = $criteria;

		return $this->collArticles;
	}


	
	public function getArticlesJoinIptc($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticles === null) {
			if ($this->isNew()) {
				$this->collArticles = array();
			} else {

				$criteria->add(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, $this->getId());

				$this->collArticles = ArticlePeer::doSelectJoinIptc($criteria, $con);
			}
		} else {
									
			$criteria->add(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, $this->getId());

			if (!isset($this->lastArticleCriteria) || !$this->lastArticleCriteria->equals($criteria)) {
				$this->collArticles = ArticlePeer::doSelectJoinIptc($criteria, $con);
			}
		}
		$this->lastArticleCriteria = $criteria;

		return $this->collArticles;
	}


	
	public function getArticlesJoinTitle($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticles === null) {
			if ($this->isNew()) {
				$this->collArticles = array();
			} else {

				$criteria->add(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, $this->getId());

				$this->collArticles = ArticlePeer::doSelectJoinTitle($criteria, $con);
			}
		} else {
									
			$criteria->add(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, $this->getId());

			if (!isset($this->lastArticleCriteria) || !$this->lastArticleCriteria->equals($criteria)) {
				$this->collArticles = ArticlePeer::doSelectJoinTitle($criteria, $con);
			}
		}
		$this->lastArticleCriteria = $criteria;

		return $this->collArticles;
	}

} 