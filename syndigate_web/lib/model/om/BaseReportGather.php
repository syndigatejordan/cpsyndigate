<?php


abstract class BaseReportGather extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $title_id;


	
	protected $level;


	
	protected $number_files;


	
	protected $file_size;


	
	protected $latest_status;


	
	protected $error;


	
	protected $images_status;


	
	protected $number_images;


	
	protected $time;

	
	protected $aTitle;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getTitleId()
	{

		return $this->title_id;
	}

	
	public function getLevel()
	{

		return $this->level;
	}

	
	public function getNumberFiles()
	{

		return $this->number_files;
	}

	
	public function getFileSize()
	{

		return $this->file_size;
	}

	
	public function getLatestStatus()
	{

		return $this->latest_status;
	}

	
	public function getError()
	{

		return $this->error;
	}

	
	public function getImagesStatus()
	{

		return $this->images_status;
	}

	
	public function getNumberImages()
	{

		return $this->number_images;
	}

	
	public function getTime($format = 'Y-m-d H:i:s')
	{

		if ($this->time === null || $this->time === '') {
			return null;
		} elseif (!is_int($this->time)) {
						$ts = strtotime($this->time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [time] as date/time value: " . var_export($this->time, true));
			}
		} else {
			$ts = $this->time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ReportGatherPeer::ID;
		}

	} 
	
	public function setTitleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_id !== $v) {
			$this->title_id = $v;
			$this->modifiedColumns[] = ReportGatherPeer::TITLE_ID;
		}

		if ($this->aTitle !== null && $this->aTitle->getId() !== $v) {
			$this->aTitle = null;
		}

	} 
	
	public function setLevel($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->level !== $v) {
			$this->level = $v;
			$this->modifiedColumns[] = ReportGatherPeer::LEVEL;
		}

	} 
	
	public function setNumberFiles($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->number_files !== $v) {
			$this->number_files = $v;
			$this->modifiedColumns[] = ReportGatherPeer::NUMBER_FILES;
		}

	} 
	
	public function setFileSize($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->file_size !== $v) {
			$this->file_size = $v;
			$this->modifiedColumns[] = ReportGatherPeer::FILE_SIZE;
		}

	} 
	
	public function setLatestStatus($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->latest_status !== $v) {
			$this->latest_status = $v;
			$this->modifiedColumns[] = ReportGatherPeer::LATEST_STATUS;
		}

	} 
	
	public function setError($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->error !== $v) {
			$this->error = $v;
			$this->modifiedColumns[] = ReportGatherPeer::ERROR;
		}

	} 
	
	public function setImagesStatus($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->images_status !== $v) {
			$this->images_status = $v;
			$this->modifiedColumns[] = ReportGatherPeer::IMAGES_STATUS;
		}

	} 
	
	public function setNumberImages($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->number_images !== $v) {
			$this->number_images = $v;
			$this->modifiedColumns[] = ReportGatherPeer::NUMBER_IMAGES;
		}

	} 
	
	public function setTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->time !== $ts) {
			$this->time = $ts;
			$this->modifiedColumns[] = ReportGatherPeer::TIME;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->title_id = $rs->getInt($startcol + 1);

			$this->level = $rs->getString($startcol + 2);

			$this->number_files = $rs->getInt($startcol + 3);

			$this->file_size = $rs->getInt($startcol + 4);

			$this->latest_status = $rs->getInt($startcol + 5);

			$this->error = $rs->getString($startcol + 6);

			$this->images_status = $rs->getInt($startcol + 7);

			$this->number_images = $rs->getInt($startcol + 8);

			$this->time = $rs->getTimestamp($startcol + 9, null);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 10; 
		} catch (Exception $e) {
			throw new PropelException("Error populating ReportGather object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ReportGatherPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ReportGatherPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ReportGatherPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aTitle !== null) {
				if ($this->aTitle->isModified()) {
					$affectedRows += $this->aTitle->save($con);
				}
				$this->setTitle($this->aTitle);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ReportGatherPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ReportGatherPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aTitle !== null) {
				if (!$this->aTitle->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTitle->getValidationFailures());
				}
			}


			if (($retval = ReportGatherPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ReportGatherPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTitleId();
				break;
			case 2:
				return $this->getLevel();
				break;
			case 3:
				return $this->getNumberFiles();
				break;
			case 4:
				return $this->getFileSize();
				break;
			case 5:
				return $this->getLatestStatus();
				break;
			case 6:
				return $this->getError();
				break;
			case 7:
				return $this->getImagesStatus();
				break;
			case 8:
				return $this->getNumberImages();
				break;
			case 9:
				return $this->getTime();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ReportGatherPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTitleId(),
			$keys[2] => $this->getLevel(),
			$keys[3] => $this->getNumberFiles(),
			$keys[4] => $this->getFileSize(),
			$keys[5] => $this->getLatestStatus(),
			$keys[6] => $this->getError(),
			$keys[7] => $this->getImagesStatus(),
			$keys[8] => $this->getNumberImages(),
			$keys[9] => $this->getTime(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ReportGatherPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTitleId($value);
				break;
			case 2:
				$this->setLevel($value);
				break;
			case 3:
				$this->setNumberFiles($value);
				break;
			case 4:
				$this->setFileSize($value);
				break;
			case 5:
				$this->setLatestStatus($value);
				break;
			case 6:
				$this->setError($value);
				break;
			case 7:
				$this->setImagesStatus($value);
				break;
			case 8:
				$this->setNumberImages($value);
				break;
			case 9:
				$this->setTime($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ReportGatherPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTitleId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setLevel($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setNumberFiles($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setFileSize($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setLatestStatus($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setError($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setImagesStatus($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setNumberImages($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setTime($arr[$keys[9]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ReportGatherPeer::DATABASE_NAME);

		if ($this->isColumnModified(ReportGatherPeer::ID)) $criteria->add(ReportGatherPeer::ID, $this->id);
		if ($this->isColumnModified(ReportGatherPeer::TITLE_ID)) $criteria->add(ReportGatherPeer::TITLE_ID, $this->title_id);
		if ($this->isColumnModified(ReportGatherPeer::LEVEL)) $criteria->add(ReportGatherPeer::LEVEL, $this->level);
		if ($this->isColumnModified(ReportGatherPeer::NUMBER_FILES)) $criteria->add(ReportGatherPeer::NUMBER_FILES, $this->number_files);
		if ($this->isColumnModified(ReportGatherPeer::FILE_SIZE)) $criteria->add(ReportGatherPeer::FILE_SIZE, $this->file_size);
		if ($this->isColumnModified(ReportGatherPeer::LATEST_STATUS)) $criteria->add(ReportGatherPeer::LATEST_STATUS, $this->latest_status);
		if ($this->isColumnModified(ReportGatherPeer::ERROR)) $criteria->add(ReportGatherPeer::ERROR, $this->error);
		if ($this->isColumnModified(ReportGatherPeer::IMAGES_STATUS)) $criteria->add(ReportGatherPeer::IMAGES_STATUS, $this->images_status);
		if ($this->isColumnModified(ReportGatherPeer::NUMBER_IMAGES)) $criteria->add(ReportGatherPeer::NUMBER_IMAGES, $this->number_images);
		if ($this->isColumnModified(ReportGatherPeer::TIME)) $criteria->add(ReportGatherPeer::TIME, $this->time);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ReportGatherPeer::DATABASE_NAME);

		$criteria->add(ReportGatherPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTitleId($this->title_id);

		$copyObj->setLevel($this->level);

		$copyObj->setNumberFiles($this->number_files);

		$copyObj->setFileSize($this->file_size);

		$copyObj->setLatestStatus($this->latest_status);

		$copyObj->setError($this->error);

		$copyObj->setImagesStatus($this->images_status);

		$copyObj->setNumberImages($this->number_images);

		$copyObj->setTime($this->time);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ReportGatherPeer();
		}
		return self::$peer;
	}

	
	public function setTitle($v)
	{


		if ($v === null) {
			$this->setTitleId(NULL);
		} else {
			$this->setTitleId($v->getId());
		}


		$this->aTitle = $v;
	}


	
	public function getTitle($con = null)
	{
		if ($this->aTitle === null && ($this->title_id !== null)) {
						include_once 'lib/model/om/BaseTitlePeer.php';

			$this->aTitle = TitlePeer::retrieveByPK($this->title_id, $con);

			
		}
		return $this->aTitle;
	}

} 