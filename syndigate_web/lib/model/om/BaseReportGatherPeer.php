<?php


abstract class BaseReportGatherPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'report_gather';

	
	const CLASS_DEFAULT = 'lib.model.ReportGather';

	
	const NUM_COLUMNS = 10;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'report_gather.ID';

	
	const TITLE_ID = 'report_gather.TITLE_ID';

	
	const LEVEL = 'report_gather.LEVEL';

	
	const NUMBER_FILES = 'report_gather.NUMBER_FILES';

	
	const FILE_SIZE = 'report_gather.FILE_SIZE';

	
	const LATEST_STATUS = 'report_gather.LATEST_STATUS';

	
	const ERROR = 'report_gather.ERROR';

	
	const IMAGES_STATUS = 'report_gather.IMAGES_STATUS';

	
	const NUMBER_IMAGES = 'report_gather.NUMBER_IMAGES';

	
	const TIME = 'report_gather.TIME';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'TitleId', 'Level', 'NumberFiles', 'FileSize', 'LatestStatus', 'Error', 'ImagesStatus', 'NumberImages', 'Time', ),
		BasePeer::TYPE_COLNAME => array (ReportGatherPeer::ID, ReportGatherPeer::TITLE_ID, ReportGatherPeer::LEVEL, ReportGatherPeer::NUMBER_FILES, ReportGatherPeer::FILE_SIZE, ReportGatherPeer::LATEST_STATUS, ReportGatherPeer::ERROR, ReportGatherPeer::IMAGES_STATUS, ReportGatherPeer::NUMBER_IMAGES, ReportGatherPeer::TIME, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'title_id', 'level', 'number_files', 'file_size', 'latest_status', 'error', 'images_status', 'number_images', 'time', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'TitleId' => 1, 'Level' => 2, 'NumberFiles' => 3, 'FileSize' => 4, 'LatestStatus' => 5, 'Error' => 6, 'ImagesStatus' => 7, 'NumberImages' => 8, 'Time' => 9, ),
		BasePeer::TYPE_COLNAME => array (ReportGatherPeer::ID => 0, ReportGatherPeer::TITLE_ID => 1, ReportGatherPeer::LEVEL => 2, ReportGatherPeer::NUMBER_FILES => 3, ReportGatherPeer::FILE_SIZE => 4, ReportGatherPeer::LATEST_STATUS => 5, ReportGatherPeer::ERROR => 6, ReportGatherPeer::IMAGES_STATUS => 7, ReportGatherPeer::NUMBER_IMAGES => 8, ReportGatherPeer::TIME => 9, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'title_id' => 1, 'level' => 2, 'number_files' => 3, 'file_size' => 4, 'latest_status' => 5, 'error' => 6, 'images_status' => 7, 'number_images' => 8, 'time' => 9, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/ReportGatherMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.ReportGatherMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ReportGatherPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ReportGatherPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ReportGatherPeer::ID);

		$criteria->addSelectColumn(ReportGatherPeer::TITLE_ID);

		$criteria->addSelectColumn(ReportGatherPeer::LEVEL);

		$criteria->addSelectColumn(ReportGatherPeer::NUMBER_FILES);

		$criteria->addSelectColumn(ReportGatherPeer::FILE_SIZE);

		$criteria->addSelectColumn(ReportGatherPeer::LATEST_STATUS);

		$criteria->addSelectColumn(ReportGatherPeer::ERROR);

		$criteria->addSelectColumn(ReportGatherPeer::IMAGES_STATUS);

		$criteria->addSelectColumn(ReportGatherPeer::NUMBER_IMAGES);

		$criteria->addSelectColumn(ReportGatherPeer::TIME);

	}

	const COUNT = 'COUNT(report_gather.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT report_gather.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ReportGatherPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ReportGatherPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ReportGatherPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ReportGatherPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ReportGatherPeer::populateObjects(ReportGatherPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ReportGatherPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ReportGatherPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinTitle(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ReportGatherPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ReportGatherPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ReportGatherPeer::TITLE_ID, TitlePeer::ID);

		$rs = ReportGatherPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinTitle(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ReportGatherPeer::addSelectColumns($c);
		$startcol = (ReportGatherPeer::NUM_COLUMNS - ReportGatherPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		TitlePeer::addSelectColumns($c);

		$c->addJoin(ReportGatherPeer::TITLE_ID, TitlePeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ReportGatherPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = TitlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getTitle(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addReportGather($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initReportGathers();
				$obj2->addReportGather($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ReportGatherPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ReportGatherPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ReportGatherPeer::TITLE_ID, TitlePeer::ID);

		$rs = ReportGatherPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ReportGatherPeer::addSelectColumns($c);
		$startcol2 = (ReportGatherPeer::NUM_COLUMNS - ReportGatherPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		TitlePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + TitlePeer::NUM_COLUMNS;

		$c->addJoin(ReportGatherPeer::TITLE_ID, TitlePeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ReportGatherPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = TitlePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getTitle(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addReportGather($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initReportGathers();
				$obj2->addReportGather($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ReportGatherPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(ReportGatherPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(ReportGatherPeer::ID);
			$selectCriteria->add(ReportGatherPeer::ID, $criteria->remove(ReportGatherPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(ReportGatherPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ReportGatherPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof ReportGather) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(ReportGatherPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(ReportGather $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ReportGatherPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ReportGatherPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ReportGatherPeer::DATABASE_NAME, ReportGatherPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ReportGatherPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(ReportGatherPeer::DATABASE_NAME);

		$criteria->add(ReportGatherPeer::ID, $pk);


		$v = ReportGatherPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(ReportGatherPeer::ID, $pks, Criteria::IN);
			$objs = ReportGatherPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseReportGatherPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/ReportGatherMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.ReportGatherMapBuilder');
}
