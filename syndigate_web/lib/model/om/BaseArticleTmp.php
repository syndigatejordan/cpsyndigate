<?php


abstract class BaseArticleTmp extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $iptc_id;


	
	protected $title_id;


	
	protected $article_original_data_id;


	
	protected $language_id;


	
	protected $headline;


	
	protected $summary;


	
	protected $body;


	
	protected $author;


	
	protected $date;


	
	protected $parsed_at;


	
	protected $updated_at;


	
	protected $has_time;


	
	protected $is_calias_called = 0;


	
	protected $sub_feed = '';

	
	protected $aLanguage;

	
	protected $aArticleOriginalDataTmp;

	
	protected $aIptc;

	
	protected $aTitle;

	
	protected $collImageTmps;

	
	protected $lastImageTmpCriteria = null;

	
	protected $collVideoTmps;

	
	protected $lastVideoTmpCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getIptcId()
	{

		return $this->iptc_id;
	}

	
	public function getTitleId()
	{

		return $this->title_id;
	}

	
	public function getArticleOriginalDataId()
	{

		return $this->article_original_data_id;
	}

	
	public function getLanguageId()
	{

		return $this->language_id;
	}

	
	public function getHeadline()
	{

		return $this->headline;
	}

	
	public function getSummary()
	{

		return $this->summary;
	}

	
	public function getBody()
	{

		return $this->body;
	}

	
	public function getAuthor()
	{

		return $this->author;
	}

	
	public function getDate($format = 'Y-m-d')
	{

		if ($this->date === null || $this->date === '') {
			return null;
		} elseif (!is_int($this->date)) {
						$ts = strtotime($this->date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [date] as date/time value: " . var_export($this->date, true));
			}
		} else {
			$ts = $this->date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getParsedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->parsed_at === null || $this->parsed_at === '') {
			return null;
		} elseif (!is_int($this->parsed_at)) {
						$ts = strtotime($this->parsed_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [parsed_at] as date/time value: " . var_export($this->parsed_at, true));
			}
		} else {
			$ts = $this->parsed_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getHasTime()
	{

		return $this->has_time;
	}

	
	public function getIsCaliasCalled()
	{

		return $this->is_calias_called;
	}

	
	public function getSubFeed()
	{

		return $this->sub_feed;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ArticleTmpPeer::ID;
		}

	} 
	
	public function setIptcId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->iptc_id !== $v) {
			$this->iptc_id = $v;
			$this->modifiedColumns[] = ArticleTmpPeer::IPTC_ID;
		}

		if ($this->aIptc !== null && $this->aIptc->getId() !== $v) {
			$this->aIptc = null;
		}

	} 
	
	public function setTitleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_id !== $v) {
			$this->title_id = $v;
			$this->modifiedColumns[] = ArticleTmpPeer::TITLE_ID;
		}

		if ($this->aTitle !== null && $this->aTitle->getId() !== $v) {
			$this->aTitle = null;
		}

	} 
	
	public function setArticleOriginalDataId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->article_original_data_id !== $v) {
			$this->article_original_data_id = $v;
			$this->modifiedColumns[] = ArticleTmpPeer::ARTICLE_ORIGINAL_DATA_ID;
		}

		if ($this->aArticleOriginalDataTmp !== null && $this->aArticleOriginalDataTmp->getId() !== $v) {
			$this->aArticleOriginalDataTmp = null;
		}

	} 
	
	public function setLanguageId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->language_id !== $v) {
			$this->language_id = $v;
			$this->modifiedColumns[] = ArticleTmpPeer::LANGUAGE_ID;
		}

		if ($this->aLanguage !== null && $this->aLanguage->getId() !== $v) {
			$this->aLanguage = null;
		}

	} 
	
	public function setHeadline($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->headline !== $v) {
			$this->headline = $v;
			$this->modifiedColumns[] = ArticleTmpPeer::HEADLINE;
		}

	} 
	
	public function setSummary($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->summary !== $v) {
			$this->summary = $v;
			$this->modifiedColumns[] = ArticleTmpPeer::SUMMARY;
		}

	} 
	
	public function setBody($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->body !== $v) {
			$this->body = $v;
			$this->modifiedColumns[] = ArticleTmpPeer::BODY;
		}

	} 
	
	public function setAuthor($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->author !== $v) {
			$this->author = $v;
			$this->modifiedColumns[] = ArticleTmpPeer::AUTHOR;
		}

	} 
	
	public function setDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->date !== $ts) {
			$this->date = $ts;
			$this->modifiedColumns[] = ArticleTmpPeer::DATE;
		}

	} 
	
	public function setParsedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [parsed_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->parsed_at !== $ts) {
			$this->parsed_at = $ts;
			$this->modifiedColumns[] = ArticleTmpPeer::PARSED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = ArticleTmpPeer::UPDATED_AT;
		}

	} 
	
	public function setHasTime($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->has_time !== $v) {
			$this->has_time = $v;
			$this->modifiedColumns[] = ArticleTmpPeer::HAS_TIME;
		}

	} 
	
	public function setIsCaliasCalled($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->is_calias_called !== $v || $v === 0) {
			$this->is_calias_called = $v;
			$this->modifiedColumns[] = ArticleTmpPeer::IS_CALIAS_CALLED;
		}

	} 
	
	public function setSubFeed($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->sub_feed !== $v || $v === '') {
			$this->sub_feed = $v;
			$this->modifiedColumns[] = ArticleTmpPeer::SUB_FEED;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->iptc_id = $rs->getInt($startcol + 1);

			$this->title_id = $rs->getInt($startcol + 2);

			$this->article_original_data_id = $rs->getInt($startcol + 3);

			$this->language_id = $rs->getInt($startcol + 4);

			$this->headline = $rs->getString($startcol + 5);

			$this->summary = $rs->getString($startcol + 6);

			$this->body = $rs->getString($startcol + 7);

			$this->author = $rs->getString($startcol + 8);

			$this->date = $rs->getDate($startcol + 9, null);

			$this->parsed_at = $rs->getTimestamp($startcol + 10, null);

			$this->updated_at = $rs->getTimestamp($startcol + 11, null);

			$this->has_time = $rs->getInt($startcol + 12);

			$this->is_calias_called = $rs->getInt($startcol + 13);

			$this->sub_feed = $rs->getString($startcol + 14);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 15; 
		} catch (Exception $e) {
			throw new PropelException("Error populating ArticleTmp object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ArticleTmpPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ArticleTmpPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isModified() && !$this->isColumnModified(ArticleTmpPeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ArticleTmpPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aLanguage !== null) {
				if ($this->aLanguage->isModified()) {
					$affectedRows += $this->aLanguage->save($con);
				}
				$this->setLanguage($this->aLanguage);
			}

			if ($this->aArticleOriginalDataTmp !== null) {
				if ($this->aArticleOriginalDataTmp->isModified()) {
					$affectedRows += $this->aArticleOriginalDataTmp->save($con);
				}
				$this->setArticleOriginalDataTmp($this->aArticleOriginalDataTmp);
			}

			if ($this->aIptc !== null) {
				if ($this->aIptc->isModified()) {
					$affectedRows += $this->aIptc->save($con);
				}
				$this->setIptc($this->aIptc);
			}

			if ($this->aTitle !== null) {
				if ($this->aTitle->isModified()) {
					$affectedRows += $this->aTitle->save($con);
				}
				$this->setTitle($this->aTitle);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ArticleTmpPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ArticleTmpPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collImageTmps !== null) {
				foreach($this->collImageTmps as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collVideoTmps !== null) {
				foreach($this->collVideoTmps as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aLanguage !== null) {
				if (!$this->aLanguage->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aLanguage->getValidationFailures());
				}
			}

			if ($this->aArticleOriginalDataTmp !== null) {
				if (!$this->aArticleOriginalDataTmp->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aArticleOriginalDataTmp->getValidationFailures());
				}
			}

			if ($this->aIptc !== null) {
				if (!$this->aIptc->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aIptc->getValidationFailures());
				}
			}

			if ($this->aTitle !== null) {
				if (!$this->aTitle->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTitle->getValidationFailures());
				}
			}


			if (($retval = ArticleTmpPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collImageTmps !== null) {
					foreach($this->collImageTmps as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collVideoTmps !== null) {
					foreach($this->collVideoTmps as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ArticleTmpPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getIptcId();
				break;
			case 2:
				return $this->getTitleId();
				break;
			case 3:
				return $this->getArticleOriginalDataId();
				break;
			case 4:
				return $this->getLanguageId();
				break;
			case 5:
				return $this->getHeadline();
				break;
			case 6:
				return $this->getSummary();
				break;
			case 7:
				return $this->getBody();
				break;
			case 8:
				return $this->getAuthor();
				break;
			case 9:
				return $this->getDate();
				break;
			case 10:
				return $this->getParsedAt();
				break;
			case 11:
				return $this->getUpdatedAt();
				break;
			case 12:
				return $this->getHasTime();
				break;
			case 13:
				return $this->getIsCaliasCalled();
				break;
			case 14:
				return $this->getSubFeed();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ArticleTmpPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getIptcId(),
			$keys[2] => $this->getTitleId(),
			$keys[3] => $this->getArticleOriginalDataId(),
			$keys[4] => $this->getLanguageId(),
			$keys[5] => $this->getHeadline(),
			$keys[6] => $this->getSummary(),
			$keys[7] => $this->getBody(),
			$keys[8] => $this->getAuthor(),
			$keys[9] => $this->getDate(),
			$keys[10] => $this->getParsedAt(),
			$keys[11] => $this->getUpdatedAt(),
			$keys[12] => $this->getHasTime(),
			$keys[13] => $this->getIsCaliasCalled(),
			$keys[14] => $this->getSubFeed(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ArticleTmpPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setIptcId($value);
				break;
			case 2:
				$this->setTitleId($value);
				break;
			case 3:
				$this->setArticleOriginalDataId($value);
				break;
			case 4:
				$this->setLanguageId($value);
				break;
			case 5:
				$this->setHeadline($value);
				break;
			case 6:
				$this->setSummary($value);
				break;
			case 7:
				$this->setBody($value);
				break;
			case 8:
				$this->setAuthor($value);
				break;
			case 9:
				$this->setDate($value);
				break;
			case 10:
				$this->setParsedAt($value);
				break;
			case 11:
				$this->setUpdatedAt($value);
				break;
			case 12:
				$this->setHasTime($value);
				break;
			case 13:
				$this->setIsCaliasCalled($value);
				break;
			case 14:
				$this->setSubFeed($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ArticleTmpPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setIptcId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setTitleId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setArticleOriginalDataId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setLanguageId($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setHeadline($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setSummary($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setBody($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setAuthor($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setDate($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setParsedAt($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setUpdatedAt($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setHasTime($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setIsCaliasCalled($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setSubFeed($arr[$keys[14]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ArticleTmpPeer::DATABASE_NAME);

		if ($this->isColumnModified(ArticleTmpPeer::ID)) $criteria->add(ArticleTmpPeer::ID, $this->id);
		if ($this->isColumnModified(ArticleTmpPeer::IPTC_ID)) $criteria->add(ArticleTmpPeer::IPTC_ID, $this->iptc_id);
		if ($this->isColumnModified(ArticleTmpPeer::TITLE_ID)) $criteria->add(ArticleTmpPeer::TITLE_ID, $this->title_id);
		if ($this->isColumnModified(ArticleTmpPeer::ARTICLE_ORIGINAL_DATA_ID)) $criteria->add(ArticleTmpPeer::ARTICLE_ORIGINAL_DATA_ID, $this->article_original_data_id);
		if ($this->isColumnModified(ArticleTmpPeer::LANGUAGE_ID)) $criteria->add(ArticleTmpPeer::LANGUAGE_ID, $this->language_id);
		if ($this->isColumnModified(ArticleTmpPeer::HEADLINE)) $criteria->add(ArticleTmpPeer::HEADLINE, $this->headline);
		if ($this->isColumnModified(ArticleTmpPeer::SUMMARY)) $criteria->add(ArticleTmpPeer::SUMMARY, $this->summary);
		if ($this->isColumnModified(ArticleTmpPeer::BODY)) $criteria->add(ArticleTmpPeer::BODY, $this->body);
		if ($this->isColumnModified(ArticleTmpPeer::AUTHOR)) $criteria->add(ArticleTmpPeer::AUTHOR, $this->author);
		if ($this->isColumnModified(ArticleTmpPeer::DATE)) $criteria->add(ArticleTmpPeer::DATE, $this->date);
		if ($this->isColumnModified(ArticleTmpPeer::PARSED_AT)) $criteria->add(ArticleTmpPeer::PARSED_AT, $this->parsed_at);
		if ($this->isColumnModified(ArticleTmpPeer::UPDATED_AT)) $criteria->add(ArticleTmpPeer::UPDATED_AT, $this->updated_at);
		if ($this->isColumnModified(ArticleTmpPeer::HAS_TIME)) $criteria->add(ArticleTmpPeer::HAS_TIME, $this->has_time);
		if ($this->isColumnModified(ArticleTmpPeer::IS_CALIAS_CALLED)) $criteria->add(ArticleTmpPeer::IS_CALIAS_CALLED, $this->is_calias_called);
		if ($this->isColumnModified(ArticleTmpPeer::SUB_FEED)) $criteria->add(ArticleTmpPeer::SUB_FEED, $this->sub_feed);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ArticleTmpPeer::DATABASE_NAME);

		$criteria->add(ArticleTmpPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setIptcId($this->iptc_id);

		$copyObj->setTitleId($this->title_id);

		$copyObj->setArticleOriginalDataId($this->article_original_data_id);

		$copyObj->setLanguageId($this->language_id);

		$copyObj->setHeadline($this->headline);

		$copyObj->setSummary($this->summary);

		$copyObj->setBody($this->body);

		$copyObj->setAuthor($this->author);

		$copyObj->setDate($this->date);

		$copyObj->setParsedAt($this->parsed_at);

		$copyObj->setUpdatedAt($this->updated_at);

		$copyObj->setHasTime($this->has_time);

		$copyObj->setIsCaliasCalled($this->is_calias_called);

		$copyObj->setSubFeed($this->sub_feed);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getImageTmps() as $relObj) {
				$copyObj->addImageTmp($relObj->copy($deepCopy));
			}

			foreach($this->getVideoTmps() as $relObj) {
				$copyObj->addVideoTmp($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ArticleTmpPeer();
		}
		return self::$peer;
	}

	
	public function setLanguage($v)
	{


		if ($v === null) {
			$this->setLanguageId(NULL);
		} else {
			$this->setLanguageId($v->getId());
		}


		$this->aLanguage = $v;
	}


	
	public function getLanguage($con = null)
	{
		if ($this->aLanguage === null && ($this->language_id !== null)) {
						include_once 'lib/model/om/BaseLanguagePeer.php';

			$this->aLanguage = LanguagePeer::retrieveByPK($this->language_id, $con);

			
		}
		return $this->aLanguage;
	}

	
	public function setArticleOriginalDataTmp($v)
	{


		if ($v === null) {
			$this->setArticleOriginalDataId(NULL);
		} else {
			$this->setArticleOriginalDataId($v->getId());
		}


		$this->aArticleOriginalDataTmp = $v;
	}


	
	public function getArticleOriginalDataTmp($con = null)
	{
		if ($this->aArticleOriginalDataTmp === null && ($this->article_original_data_id !== null)) {
						include_once 'lib/model/om/BaseArticleOriginalDataTmpPeer.php';

			$this->aArticleOriginalDataTmp = ArticleOriginalDataTmpPeer::retrieveByPK($this->article_original_data_id, $con);

			
		}
		return $this->aArticleOriginalDataTmp;
	}

	
	public function setIptc($v)
	{


		if ($v === null) {
			$this->setIptcId(NULL);
		} else {
			$this->setIptcId($v->getId());
		}


		$this->aIptc = $v;
	}


	
	public function getIptc($con = null)
	{
		if ($this->aIptc === null && ($this->iptc_id !== null)) {
						include_once 'lib/model/om/BaseIptcPeer.php';

			$this->aIptc = IptcPeer::retrieveByPK($this->iptc_id, $con);

			
		}
		return $this->aIptc;
	}

	
	public function setTitle($v)
	{


		if ($v === null) {
			$this->setTitleId(NULL);
		} else {
			$this->setTitleId($v->getId());
		}


		$this->aTitle = $v;
	}


	
	public function getTitle($con = null)
	{
		if ($this->aTitle === null && ($this->title_id !== null)) {
						include_once 'lib/model/om/BaseTitlePeer.php';

			$this->aTitle = TitlePeer::retrieveByPK($this->title_id, $con);

			
		}
		return $this->aTitle;
	}

	
	public function initImageTmps()
	{
		if ($this->collImageTmps === null) {
			$this->collImageTmps = array();
		}
	}

	
	public function getImageTmps($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseImageTmpPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collImageTmps === null) {
			if ($this->isNew()) {
			   $this->collImageTmps = array();
			} else {

				$criteria->add(ImageTmpPeer::ARTICLE_ID, $this->getId());

				ImageTmpPeer::addSelectColumns($criteria);
				$this->collImageTmps = ImageTmpPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ImageTmpPeer::ARTICLE_ID, $this->getId());

				ImageTmpPeer::addSelectColumns($criteria);
				if (!isset($this->lastImageTmpCriteria) || !$this->lastImageTmpCriteria->equals($criteria)) {
					$this->collImageTmps = ImageTmpPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastImageTmpCriteria = $criteria;
		return $this->collImageTmps;
	}

	
	public function countImageTmps($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseImageTmpPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ImageTmpPeer::ARTICLE_ID, $this->getId());

		return ImageTmpPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addImageTmp(ImageTmp $l)
	{
		$this->collImageTmps[] = $l;
		$l->setArticleTmp($this);
	}

	
	public function initVideoTmps()
	{
		if ($this->collVideoTmps === null) {
			$this->collVideoTmps = array();
		}
	}

	
	public function getVideoTmps($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseVideoTmpPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collVideoTmps === null) {
			if ($this->isNew()) {
			   $this->collVideoTmps = array();
			} else {

				$criteria->add(VideoTmpPeer::ARTICLE_ID, $this->getId());

				VideoTmpPeer::addSelectColumns($criteria);
				$this->collVideoTmps = VideoTmpPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(VideoTmpPeer::ARTICLE_ID, $this->getId());

				VideoTmpPeer::addSelectColumns($criteria);
				if (!isset($this->lastVideoTmpCriteria) || !$this->lastVideoTmpCriteria->equals($criteria)) {
					$this->collVideoTmps = VideoTmpPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastVideoTmpCriteria = $criteria;
		return $this->collVideoTmps;
	}

	
	public function countVideoTmps($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseVideoTmpPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(VideoTmpPeer::ARTICLE_ID, $this->getId());

		return VideoTmpPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addVideoTmp(VideoTmp $l)
	{
		$this->collVideoTmps[] = $l;
		$l->setArticleTmp($this);
	}

} 