<?php


abstract class BaseSchedule extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $title_id;


	
	protected $minute;


	
	protected $hour;


	
	protected $day;


	
	protected $month;


	
	protected $weekday;


	
	protected $script_path;

	
	protected $aTitle;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getTitleId()
	{

		return $this->title_id;
	}

	
	public function getMinute()
	{

		return $this->minute;
	}

	
	public function getHour()
	{

		return $this->hour;
	}

	
	public function getDay()
	{

		return $this->day;
	}

	
	public function getMonth()
	{

		return $this->month;
	}

	
	public function getWeekday()
	{

		return $this->weekday;
	}

	
	public function getScriptPath()
	{

		return $this->script_path;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = SchedulePeer::ID;
		}

	} 
	
	public function setTitleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_id !== $v) {
			$this->title_id = $v;
			$this->modifiedColumns[] = SchedulePeer::TITLE_ID;
		}

		if ($this->aTitle !== null && $this->aTitle->getId() !== $v) {
			$this->aTitle = null;
		}

	} 
	
	public function setMinute($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->minute !== $v) {
			$this->minute = $v;
			$this->modifiedColumns[] = SchedulePeer::MINUTE;
		}

	} 
	
	public function setHour($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->hour !== $v) {
			$this->hour = $v;
			$this->modifiedColumns[] = SchedulePeer::HOUR;
		}

	} 
	
	public function setDay($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->day !== $v) {
			$this->day = $v;
			$this->modifiedColumns[] = SchedulePeer::DAY;
		}

	} 
	
	public function setMonth($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->month !== $v) {
			$this->month = $v;
			$this->modifiedColumns[] = SchedulePeer::MONTH;
		}

	} 
	
	public function setWeekday($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->weekday !== $v) {
			$this->weekday = $v;
			$this->modifiedColumns[] = SchedulePeer::WEEKDAY;
		}

	} 
	
	public function setScriptPath($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->script_path !== $v) {
			$this->script_path = $v;
			$this->modifiedColumns[] = SchedulePeer::SCRIPT_PATH;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->title_id = $rs->getInt($startcol + 1);

			$this->minute = $rs->getString($startcol + 2);

			$this->hour = $rs->getString($startcol + 3);

			$this->day = $rs->getString($startcol + 4);

			$this->month = $rs->getString($startcol + 5);

			$this->weekday = $rs->getString($startcol + 6);

			$this->script_path = $rs->getString($startcol + 7);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 8; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Schedule object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SchedulePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			SchedulePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SchedulePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aTitle !== null) {
				if ($this->aTitle->isModified()) {
					$affectedRows += $this->aTitle->save($con);
				}
				$this->setTitle($this->aTitle);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = SchedulePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += SchedulePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aTitle !== null) {
				if (!$this->aTitle->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTitle->getValidationFailures());
				}
			}


			if (($retval = SchedulePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SchedulePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTitleId();
				break;
			case 2:
				return $this->getMinute();
				break;
			case 3:
				return $this->getHour();
				break;
			case 4:
				return $this->getDay();
				break;
			case 5:
				return $this->getMonth();
				break;
			case 6:
				return $this->getWeekday();
				break;
			case 7:
				return $this->getScriptPath();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SchedulePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTitleId(),
			$keys[2] => $this->getMinute(),
			$keys[3] => $this->getHour(),
			$keys[4] => $this->getDay(),
			$keys[5] => $this->getMonth(),
			$keys[6] => $this->getWeekday(),
			$keys[7] => $this->getScriptPath(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SchedulePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTitleId($value);
				break;
			case 2:
				$this->setMinute($value);
				break;
			case 3:
				$this->setHour($value);
				break;
			case 4:
				$this->setDay($value);
				break;
			case 5:
				$this->setMonth($value);
				break;
			case 6:
				$this->setWeekday($value);
				break;
			case 7:
				$this->setScriptPath($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SchedulePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTitleId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setMinute($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setHour($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setDay($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setMonth($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setWeekday($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setScriptPath($arr[$keys[7]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(SchedulePeer::DATABASE_NAME);

		if ($this->isColumnModified(SchedulePeer::ID)) $criteria->add(SchedulePeer::ID, $this->id);
		if ($this->isColumnModified(SchedulePeer::TITLE_ID)) $criteria->add(SchedulePeer::TITLE_ID, $this->title_id);
		if ($this->isColumnModified(SchedulePeer::MINUTE)) $criteria->add(SchedulePeer::MINUTE, $this->minute);
		if ($this->isColumnModified(SchedulePeer::HOUR)) $criteria->add(SchedulePeer::HOUR, $this->hour);
		if ($this->isColumnModified(SchedulePeer::DAY)) $criteria->add(SchedulePeer::DAY, $this->day);
		if ($this->isColumnModified(SchedulePeer::MONTH)) $criteria->add(SchedulePeer::MONTH, $this->month);
		if ($this->isColumnModified(SchedulePeer::WEEKDAY)) $criteria->add(SchedulePeer::WEEKDAY, $this->weekday);
		if ($this->isColumnModified(SchedulePeer::SCRIPT_PATH)) $criteria->add(SchedulePeer::SCRIPT_PATH, $this->script_path);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(SchedulePeer::DATABASE_NAME);

		$criteria->add(SchedulePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTitleId($this->title_id);

		$copyObj->setMinute($this->minute);

		$copyObj->setHour($this->hour);

		$copyObj->setDay($this->day);

		$copyObj->setMonth($this->month);

		$copyObj->setWeekday($this->weekday);

		$copyObj->setScriptPath($this->script_path);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new SchedulePeer();
		}
		return self::$peer;
	}

	
	public function setTitle($v)
	{


		if ($v === null) {
			$this->setTitleId(NULL);
		} else {
			$this->setTitleId($v->getId());
		}


		$this->aTitle = $v;
	}


	
	public function getTitle($con = null)
	{
		if ($this->aTitle === null && ($this->title_id !== null)) {
						include_once 'lib/model/om/BaseTitlePeer.php';

			$this->aTitle = TitlePeer::retrieveByPK($this->title_id, $con);

			
		}
		return $this->aTitle;
	}

} 