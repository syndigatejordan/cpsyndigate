<?php


abstract class BaseTendernewsPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'tendernews';

	
	const CLASS_DEFAULT = 'lib.model.Tendernews';

	
	const NUM_COLUMNS = 14;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'tendernews.ID';

	
	const IPTC_ID = 'tendernews.IPTC_ID';

	
	const TITLE_ID = 'tendernews.TITLE_ID';

	
	const ARTICLE_ORIGINAL_DATA_ID = 'tendernews.ARTICLE_ORIGINAL_DATA_ID';

	
	const LANGUAGE_ID = 'tendernews.LANGUAGE_ID';

	
	const HEADLINE = 'tendernews.HEADLINE';

	
	const SUMMARY = 'tendernews.SUMMARY';

	
	const BODY = 'tendernews.BODY';

	
	const AUTHOR = 'tendernews.AUTHOR';

	
	const DATE = 'tendernews.DATE';

	
	const PARSED_AT = 'tendernews.PARSED_AT';

	
	const UPDATED_AT = 'tendernews.UPDATED_AT';

	
	const HAS_TIME = 'tendernews.HAS_TIME';

	
	const IS_CALIAS_CALLED = 'tendernews.IS_CALIAS_CALLED';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'IptcId', 'TitleId', 'ArticleOriginalDataId', 'LanguageId', 'Headline', 'Summary', 'Body', 'Author', 'Date', 'ParsedAt', 'UpdatedAt', 'HasTime', 'IsCaliasCalled', ),
		BasePeer::TYPE_COLNAME => array (TendernewsPeer::ID, TendernewsPeer::IPTC_ID, TendernewsPeer::TITLE_ID, TendernewsPeer::ARTICLE_ORIGINAL_DATA_ID, TendernewsPeer::LANGUAGE_ID, TendernewsPeer::HEADLINE, TendernewsPeer::SUMMARY, TendernewsPeer::BODY, TendernewsPeer::AUTHOR, TendernewsPeer::DATE, TendernewsPeer::PARSED_AT, TendernewsPeer::UPDATED_AT, TendernewsPeer::HAS_TIME, TendernewsPeer::IS_CALIAS_CALLED, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'iptc_id', 'title_id', 'article_original_data_id', 'language_id', 'headline', 'summary', 'body', 'author', 'date', 'parsed_at', 'updated_at', 'has_time', 'is_Calias_called', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'IptcId' => 1, 'TitleId' => 2, 'ArticleOriginalDataId' => 3, 'LanguageId' => 4, 'Headline' => 5, 'Summary' => 6, 'Body' => 7, 'Author' => 8, 'Date' => 9, 'ParsedAt' => 10, 'UpdatedAt' => 11, 'HasTime' => 12, 'IsCaliasCalled' => 13, ),
		BasePeer::TYPE_COLNAME => array (TendernewsPeer::ID => 0, TendernewsPeer::IPTC_ID => 1, TendernewsPeer::TITLE_ID => 2, TendernewsPeer::ARTICLE_ORIGINAL_DATA_ID => 3, TendernewsPeer::LANGUAGE_ID => 4, TendernewsPeer::HEADLINE => 5, TendernewsPeer::SUMMARY => 6, TendernewsPeer::BODY => 7, TendernewsPeer::AUTHOR => 8, TendernewsPeer::DATE => 9, TendernewsPeer::PARSED_AT => 10, TendernewsPeer::UPDATED_AT => 11, TendernewsPeer::HAS_TIME => 12, TendernewsPeer::IS_CALIAS_CALLED => 13, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'iptc_id' => 1, 'title_id' => 2, 'article_original_data_id' => 3, 'language_id' => 4, 'headline' => 5, 'summary' => 6, 'body' => 7, 'author' => 8, 'date' => 9, 'parsed_at' => 10, 'updated_at' => 11, 'has_time' => 12, 'is_Calias_called' => 13, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/TendernewsMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.TendernewsMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = TendernewsPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(TendernewsPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(TendernewsPeer::ID);

		$criteria->addSelectColumn(TendernewsPeer::IPTC_ID);

		$criteria->addSelectColumn(TendernewsPeer::TITLE_ID);

		$criteria->addSelectColumn(TendernewsPeer::ARTICLE_ORIGINAL_DATA_ID);

		$criteria->addSelectColumn(TendernewsPeer::LANGUAGE_ID);

		$criteria->addSelectColumn(TendernewsPeer::HEADLINE);

		$criteria->addSelectColumn(TendernewsPeer::SUMMARY);

		$criteria->addSelectColumn(TendernewsPeer::BODY);

		$criteria->addSelectColumn(TendernewsPeer::AUTHOR);

		$criteria->addSelectColumn(TendernewsPeer::DATE);

		$criteria->addSelectColumn(TendernewsPeer::PARSED_AT);

		$criteria->addSelectColumn(TendernewsPeer::UPDATED_AT);

		$criteria->addSelectColumn(TendernewsPeer::HAS_TIME);

		$criteria->addSelectColumn(TendernewsPeer::IS_CALIAS_CALLED);

	}

	const COUNT = 'COUNT(*)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT *)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TendernewsPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TendernewsPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = TendernewsPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = TendernewsPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return TendernewsPeer::populateObjects(TendernewsPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			TendernewsPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = TendernewsPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return TendernewsPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(TendernewsPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(TendernewsPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Tendernews) {

			$criteria = $values->buildCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

			}

		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Tendernews $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(TendernewsPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(TendernewsPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(TendernewsPeer::DATABASE_NAME, TendernewsPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = TendernewsPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

} 
if (Propel::isInit()) {
			try {
		BaseTendernewsPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/TendernewsMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.TendernewsMapBuilder');
}
