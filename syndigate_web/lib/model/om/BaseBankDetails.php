<?php


abstract class BaseBankDetails extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $publisher_id;


	
	protected $branch;


	
	protected $country_id;


	
	protected $notes;


	
	protected $account_name;


	
	protected $account_number;


	
	protected $swift;


	
	protected $iban;


	
	protected $bank_name;


	
	protected $bank_city;

	
	protected $aPublisher;

	
	protected $aCountry;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getPublisherId()
	{

		return $this->publisher_id;
	}

	
	public function getBranch()
	{

		return $this->branch;
	}

	
	public function getCountryId()
	{

		return $this->country_id;
	}

	
	public function getNotes()
	{

		return $this->notes;
	}

	
	public function getAccountName()
	{

		return $this->account_name;
	}

	
	public function getAccountNumber()
	{

		return $this->account_number;
	}

	
	public function getSwift()
	{

		return $this->swift;
	}

	
	public function getIban()
	{

		return $this->iban;
	}

	
	public function getBankName()
	{

		return $this->bank_name;
	}

	
	public function getBankCity()
	{

		return $this->bank_city;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = BankDetailsPeer::ID;
		}

	} 
	
	public function setPublisherId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->publisher_id !== $v) {
			$this->publisher_id = $v;
			$this->modifiedColumns[] = BankDetailsPeer::PUBLISHER_ID;
		}

		if ($this->aPublisher !== null && $this->aPublisher->getId() !== $v) {
			$this->aPublisher = null;
		}

	} 
	
	public function setBranch($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->branch !== $v) {
			$this->branch = $v;
			$this->modifiedColumns[] = BankDetailsPeer::BRANCH;
		}

	} 
	
	public function setCountryId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->country_id !== $v) {
			$this->country_id = $v;
			$this->modifiedColumns[] = BankDetailsPeer::COUNTRY_ID;
		}

		if ($this->aCountry !== null && $this->aCountry->getId() !== $v) {
			$this->aCountry = null;
		}

	} 
	
	public function setNotes($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->notes !== $v) {
			$this->notes = $v;
			$this->modifiedColumns[] = BankDetailsPeer::NOTES;
		}

	} 
	
	public function setAccountName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->account_name !== $v) {
			$this->account_name = $v;
			$this->modifiedColumns[] = BankDetailsPeer::ACCOUNT_NAME;
		}

	} 
	
	public function setAccountNumber($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->account_number !== $v) {
			$this->account_number = $v;
			$this->modifiedColumns[] = BankDetailsPeer::ACCOUNT_NUMBER;
		}

	} 
	
	public function setSwift($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->swift !== $v) {
			$this->swift = $v;
			$this->modifiedColumns[] = BankDetailsPeer::SWIFT;
		}

	} 
	
	public function setIban($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->iban !== $v) {
			$this->iban = $v;
			$this->modifiedColumns[] = BankDetailsPeer::IBAN;
		}

	} 
	
	public function setBankName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->bank_name !== $v) {
			$this->bank_name = $v;
			$this->modifiedColumns[] = BankDetailsPeer::BANK_NAME;
		}

	} 
	
	public function setBankCity($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->bank_city !== $v) {
			$this->bank_city = $v;
			$this->modifiedColumns[] = BankDetailsPeer::BANK_CITY;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->publisher_id = $rs->getInt($startcol + 1);

			$this->branch = $rs->getString($startcol + 2);

			$this->country_id = $rs->getInt($startcol + 3);

			$this->notes = $rs->getString($startcol + 4);

			$this->account_name = $rs->getString($startcol + 5);

			$this->account_number = $rs->getString($startcol + 6);

			$this->swift = $rs->getString($startcol + 7);

			$this->iban = $rs->getString($startcol + 8);

			$this->bank_name = $rs->getString($startcol + 9);

			$this->bank_city = $rs->getString($startcol + 10);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 11; 
		} catch (Exception $e) {
			throw new PropelException("Error populating BankDetails object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(BankDetailsPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			BankDetailsPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(BankDetailsPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aPublisher !== null) {
				if ($this->aPublisher->isModified()) {
					$affectedRows += $this->aPublisher->save($con);
				}
				$this->setPublisher($this->aPublisher);
			}

			if ($this->aCountry !== null) {
				if ($this->aCountry->isModified()) {
					$affectedRows += $this->aCountry->save($con);
				}
				$this->setCountry($this->aCountry);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = BankDetailsPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += BankDetailsPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aPublisher !== null) {
				if (!$this->aPublisher->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aPublisher->getValidationFailures());
				}
			}

			if ($this->aCountry !== null) {
				if (!$this->aCountry->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCountry->getValidationFailures());
				}
			}


			if (($retval = BankDetailsPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = BankDetailsPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getPublisherId();
				break;
			case 2:
				return $this->getBranch();
				break;
			case 3:
				return $this->getCountryId();
				break;
			case 4:
				return $this->getNotes();
				break;
			case 5:
				return $this->getAccountName();
				break;
			case 6:
				return $this->getAccountNumber();
				break;
			case 7:
				return $this->getSwift();
				break;
			case 8:
				return $this->getIban();
				break;
			case 9:
				return $this->getBankName();
				break;
			case 10:
				return $this->getBankCity();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = BankDetailsPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getPublisherId(),
			$keys[2] => $this->getBranch(),
			$keys[3] => $this->getCountryId(),
			$keys[4] => $this->getNotes(),
			$keys[5] => $this->getAccountName(),
			$keys[6] => $this->getAccountNumber(),
			$keys[7] => $this->getSwift(),
			$keys[8] => $this->getIban(),
			$keys[9] => $this->getBankName(),
			$keys[10] => $this->getBankCity(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = BankDetailsPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setPublisherId($value);
				break;
			case 2:
				$this->setBranch($value);
				break;
			case 3:
				$this->setCountryId($value);
				break;
			case 4:
				$this->setNotes($value);
				break;
			case 5:
				$this->setAccountName($value);
				break;
			case 6:
				$this->setAccountNumber($value);
				break;
			case 7:
				$this->setSwift($value);
				break;
			case 8:
				$this->setIban($value);
				break;
			case 9:
				$this->setBankName($value);
				break;
			case 10:
				$this->setBankCity($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = BankDetailsPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setPublisherId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setBranch($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setCountryId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setNotes($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setAccountName($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setAccountNumber($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setSwift($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setIban($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setBankName($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setBankCity($arr[$keys[10]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(BankDetailsPeer::DATABASE_NAME);

		if ($this->isColumnModified(BankDetailsPeer::ID)) $criteria->add(BankDetailsPeer::ID, $this->id);
		if ($this->isColumnModified(BankDetailsPeer::PUBLISHER_ID)) $criteria->add(BankDetailsPeer::PUBLISHER_ID, $this->publisher_id);
		if ($this->isColumnModified(BankDetailsPeer::BRANCH)) $criteria->add(BankDetailsPeer::BRANCH, $this->branch);
		if ($this->isColumnModified(BankDetailsPeer::COUNTRY_ID)) $criteria->add(BankDetailsPeer::COUNTRY_ID, $this->country_id);
		if ($this->isColumnModified(BankDetailsPeer::NOTES)) $criteria->add(BankDetailsPeer::NOTES, $this->notes);
		if ($this->isColumnModified(BankDetailsPeer::ACCOUNT_NAME)) $criteria->add(BankDetailsPeer::ACCOUNT_NAME, $this->account_name);
		if ($this->isColumnModified(BankDetailsPeer::ACCOUNT_NUMBER)) $criteria->add(BankDetailsPeer::ACCOUNT_NUMBER, $this->account_number);
		if ($this->isColumnModified(BankDetailsPeer::SWIFT)) $criteria->add(BankDetailsPeer::SWIFT, $this->swift);
		if ($this->isColumnModified(BankDetailsPeer::IBAN)) $criteria->add(BankDetailsPeer::IBAN, $this->iban);
		if ($this->isColumnModified(BankDetailsPeer::BANK_NAME)) $criteria->add(BankDetailsPeer::BANK_NAME, $this->bank_name);
		if ($this->isColumnModified(BankDetailsPeer::BANK_CITY)) $criteria->add(BankDetailsPeer::BANK_CITY, $this->bank_city);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(BankDetailsPeer::DATABASE_NAME);

		$criteria->add(BankDetailsPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setPublisherId($this->publisher_id);

		$copyObj->setBranch($this->branch);

		$copyObj->setCountryId($this->country_id);

		$copyObj->setNotes($this->notes);

		$copyObj->setAccountName($this->account_name);

		$copyObj->setAccountNumber($this->account_number);

		$copyObj->setSwift($this->swift);

		$copyObj->setIban($this->iban);

		$copyObj->setBankName($this->bank_name);

		$copyObj->setBankCity($this->bank_city);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new BankDetailsPeer();
		}
		return self::$peer;
	}

	
	public function setPublisher($v)
	{


		if ($v === null) {
			$this->setPublisherId(NULL);
		} else {
			$this->setPublisherId($v->getId());
		}


		$this->aPublisher = $v;
	}


	
	public function getPublisher($con = null)
	{
		if ($this->aPublisher === null && ($this->publisher_id !== null)) {
						include_once 'lib/model/om/BasePublisherPeer.php';

			$this->aPublisher = PublisherPeer::retrieveByPK($this->publisher_id, $con);

			
		}
		return $this->aPublisher;
	}

	
	public function setCountry($v)
	{


		if ($v === null) {
			$this->setCountryId(NULL);
		} else {
			$this->setCountryId($v->getId());
		}


		$this->aCountry = $v;
	}


	
	public function getCountry($con = null)
	{
		if ($this->aCountry === null && ($this->country_id !== null)) {
						include_once 'lib/model/om/BaseCountryPeer.php';

			$this->aCountry = CountryPeer::retrieveByPK($this->country_id, $con);

			
		}
		return $this->aCountry;
	}

} 