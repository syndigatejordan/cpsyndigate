<?php


abstract class BaseTendersInfoArticle extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id = 0;


	
	protected $iptc_id;


	
	protected $title_id;


	
	protected $article_original_data_id;


	
	protected $language_id;


	
	protected $headline;


	
	protected $summary;


	
	protected $body;


	
	protected $author;


	
	protected $date;


	
	protected $parsed_at;


	
	protected $updated_at;


	
	protected $has_time;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getIptcId()
	{

		return $this->iptc_id;
	}

	
	public function getTitleId()
	{

		return $this->title_id;
	}

	
	public function getArticleOriginalDataId()
	{

		return $this->article_original_data_id;
	}

	
	public function getLanguageId()
	{

		return $this->language_id;
	}

	
	public function getHeadline()
	{

		return $this->headline;
	}

	
	public function getSummary()
	{

		return $this->summary;
	}

	
	public function getBody()
	{

		return $this->body;
	}

	
	public function getAuthor()
	{

		return $this->author;
	}

	
	public function getDate($format = 'Y-m-d')
	{

		if ($this->date === null || $this->date === '') {
			return null;
		} elseif (!is_int($this->date)) {
						$ts = strtotime($this->date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [date] as date/time value: " . var_export($this->date, true));
			}
		} else {
			$ts = $this->date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getParsedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->parsed_at === null || $this->parsed_at === '') {
			return null;
		} elseif (!is_int($this->parsed_at)) {
						$ts = strtotime($this->parsed_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [parsed_at] as date/time value: " . var_export($this->parsed_at, true));
			}
		} else {
			$ts = $this->parsed_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getHasTime()
	{

		return $this->has_time;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v || $v === 0) {
			$this->id = $v;
			$this->modifiedColumns[] = TendersInfoArticlePeer::ID;
		}

	} 
	
	public function setIptcId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->iptc_id !== $v) {
			$this->iptc_id = $v;
			$this->modifiedColumns[] = TendersInfoArticlePeer::IPTC_ID;
		}

	} 
	
	public function setTitleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_id !== $v) {
			$this->title_id = $v;
			$this->modifiedColumns[] = TendersInfoArticlePeer::TITLE_ID;
		}

	} 
	
	public function setArticleOriginalDataId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->article_original_data_id !== $v) {
			$this->article_original_data_id = $v;
			$this->modifiedColumns[] = TendersInfoArticlePeer::ARTICLE_ORIGINAL_DATA_ID;
		}

	} 
	
	public function setLanguageId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->language_id !== $v) {
			$this->language_id = $v;
			$this->modifiedColumns[] = TendersInfoArticlePeer::LANGUAGE_ID;
		}

	} 
	
	public function setHeadline($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->headline !== $v) {
			$this->headline = $v;
			$this->modifiedColumns[] = TendersInfoArticlePeer::HEADLINE;
		}

	} 
	
	public function setSummary($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->summary !== $v) {
			$this->summary = $v;
			$this->modifiedColumns[] = TendersInfoArticlePeer::SUMMARY;
		}

	} 
	
	public function setBody($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->body !== $v) {
			$this->body = $v;
			$this->modifiedColumns[] = TendersInfoArticlePeer::BODY;
		}

	} 
	
	public function setAuthor($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->author !== $v) {
			$this->author = $v;
			$this->modifiedColumns[] = TendersInfoArticlePeer::AUTHOR;
		}

	} 
	
	public function setDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->date !== $ts) {
			$this->date = $ts;
			$this->modifiedColumns[] = TendersInfoArticlePeer::DATE;
		}

	} 
	
	public function setParsedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [parsed_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->parsed_at !== $ts) {
			$this->parsed_at = $ts;
			$this->modifiedColumns[] = TendersInfoArticlePeer::PARSED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = TendersInfoArticlePeer::UPDATED_AT;
		}

	} 
	
	public function setHasTime($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->has_time !== $v) {
			$this->has_time = $v;
			$this->modifiedColumns[] = TendersInfoArticlePeer::HAS_TIME;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->iptc_id = $rs->getInt($startcol + 1);

			$this->title_id = $rs->getInt($startcol + 2);

			$this->article_original_data_id = $rs->getInt($startcol + 3);

			$this->language_id = $rs->getInt($startcol + 4);

			$this->headline = $rs->getString($startcol + 5);

			$this->summary = $rs->getString($startcol + 6);

			$this->body = $rs->getString($startcol + 7);

			$this->author = $rs->getString($startcol + 8);

			$this->date = $rs->getDate($startcol + 9, null);

			$this->parsed_at = $rs->getTimestamp($startcol + 10, null);

			$this->updated_at = $rs->getTimestamp($startcol + 11, null);

			$this->has_time = $rs->getInt($startcol + 12);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 13; 
		} catch (Exception $e) {
			throw new PropelException("Error populating TendersInfoArticle object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TendersInfoArticlePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			TendersInfoArticlePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isModified() && !$this->isColumnModified(TendersInfoArticlePeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TendersInfoArticlePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = TendersInfoArticlePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += TendersInfoArticlePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = TendersInfoArticlePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TendersInfoArticlePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getIptcId();
				break;
			case 2:
				return $this->getTitleId();
				break;
			case 3:
				return $this->getArticleOriginalDataId();
				break;
			case 4:
				return $this->getLanguageId();
				break;
			case 5:
				return $this->getHeadline();
				break;
			case 6:
				return $this->getSummary();
				break;
			case 7:
				return $this->getBody();
				break;
			case 8:
				return $this->getAuthor();
				break;
			case 9:
				return $this->getDate();
				break;
			case 10:
				return $this->getParsedAt();
				break;
			case 11:
				return $this->getUpdatedAt();
				break;
			case 12:
				return $this->getHasTime();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TendersInfoArticlePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getIptcId(),
			$keys[2] => $this->getTitleId(),
			$keys[3] => $this->getArticleOriginalDataId(),
			$keys[4] => $this->getLanguageId(),
			$keys[5] => $this->getHeadline(),
			$keys[6] => $this->getSummary(),
			$keys[7] => $this->getBody(),
			$keys[8] => $this->getAuthor(),
			$keys[9] => $this->getDate(),
			$keys[10] => $this->getParsedAt(),
			$keys[11] => $this->getUpdatedAt(),
			$keys[12] => $this->getHasTime(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TendersInfoArticlePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setIptcId($value);
				break;
			case 2:
				$this->setTitleId($value);
				break;
			case 3:
				$this->setArticleOriginalDataId($value);
				break;
			case 4:
				$this->setLanguageId($value);
				break;
			case 5:
				$this->setHeadline($value);
				break;
			case 6:
				$this->setSummary($value);
				break;
			case 7:
				$this->setBody($value);
				break;
			case 8:
				$this->setAuthor($value);
				break;
			case 9:
				$this->setDate($value);
				break;
			case 10:
				$this->setParsedAt($value);
				break;
			case 11:
				$this->setUpdatedAt($value);
				break;
			case 12:
				$this->setHasTime($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TendersInfoArticlePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setIptcId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setTitleId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setArticleOriginalDataId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setLanguageId($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setHeadline($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setSummary($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setBody($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setAuthor($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setDate($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setParsedAt($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setUpdatedAt($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setHasTime($arr[$keys[12]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(TendersInfoArticlePeer::DATABASE_NAME);

		if ($this->isColumnModified(TendersInfoArticlePeer::ID)) $criteria->add(TendersInfoArticlePeer::ID, $this->id);
		if ($this->isColumnModified(TendersInfoArticlePeer::IPTC_ID)) $criteria->add(TendersInfoArticlePeer::IPTC_ID, $this->iptc_id);
		if ($this->isColumnModified(TendersInfoArticlePeer::TITLE_ID)) $criteria->add(TendersInfoArticlePeer::TITLE_ID, $this->title_id);
		if ($this->isColumnModified(TendersInfoArticlePeer::ARTICLE_ORIGINAL_DATA_ID)) $criteria->add(TendersInfoArticlePeer::ARTICLE_ORIGINAL_DATA_ID, $this->article_original_data_id);
		if ($this->isColumnModified(TendersInfoArticlePeer::LANGUAGE_ID)) $criteria->add(TendersInfoArticlePeer::LANGUAGE_ID, $this->language_id);
		if ($this->isColumnModified(TendersInfoArticlePeer::HEADLINE)) $criteria->add(TendersInfoArticlePeer::HEADLINE, $this->headline);
		if ($this->isColumnModified(TendersInfoArticlePeer::SUMMARY)) $criteria->add(TendersInfoArticlePeer::SUMMARY, $this->summary);
		if ($this->isColumnModified(TendersInfoArticlePeer::BODY)) $criteria->add(TendersInfoArticlePeer::BODY, $this->body);
		if ($this->isColumnModified(TendersInfoArticlePeer::AUTHOR)) $criteria->add(TendersInfoArticlePeer::AUTHOR, $this->author);
		if ($this->isColumnModified(TendersInfoArticlePeer::DATE)) $criteria->add(TendersInfoArticlePeer::DATE, $this->date);
		if ($this->isColumnModified(TendersInfoArticlePeer::PARSED_AT)) $criteria->add(TendersInfoArticlePeer::PARSED_AT, $this->parsed_at);
		if ($this->isColumnModified(TendersInfoArticlePeer::UPDATED_AT)) $criteria->add(TendersInfoArticlePeer::UPDATED_AT, $this->updated_at);
		if ($this->isColumnModified(TendersInfoArticlePeer::HAS_TIME)) $criteria->add(TendersInfoArticlePeer::HAS_TIME, $this->has_time);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(TendersInfoArticlePeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setId($this->id);

		$copyObj->setIptcId($this->iptc_id);

		$copyObj->setTitleId($this->title_id);

		$copyObj->setArticleOriginalDataId($this->article_original_data_id);

		$copyObj->setLanguageId($this->language_id);

		$copyObj->setHeadline($this->headline);

		$copyObj->setSummary($this->summary);

		$copyObj->setBody($this->body);

		$copyObj->setAuthor($this->author);

		$copyObj->setDate($this->date);

		$copyObj->setParsedAt($this->parsed_at);

		$copyObj->setUpdatedAt($this->updated_at);

		$copyObj->setHasTime($this->has_time);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new TendersInfoArticlePeer();
		}
		return self::$peer;
	}

} 