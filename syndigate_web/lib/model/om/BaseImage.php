<?php


abstract class BaseImage extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $article_id;


	
	protected $img_name;


	
	protected $image_caption;


	
	protected $is_headline = 0;


	
	protected $image_type;


	
	protected $mime_type;


	
	protected $original_name;


	
	protected $image_original_key;

	
	protected $aArticle;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getArticleId()
	{

		return $this->article_id;
	}

	
	public function getImgName()
	{

		return $this->img_name;
	}

	
	public function getImageCaption()
	{

		return $this->image_caption;
	}

	
	public function getIsHeadline()
	{

		return $this->is_headline;
	}

	
	public function getImageType()
	{

		return $this->image_type;
	}

	
	public function getMimeType()
	{

		return $this->mime_type;
	}

	
	public function getOriginalName()
	{

		return $this->original_name;
	}

	
	public function getImageOriginalKey()
	{

		return $this->image_original_key;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ImagePeer::ID;
		}

	} 
	
	public function setArticleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->article_id !== $v) {
			$this->article_id = $v;
			$this->modifiedColumns[] = ImagePeer::ARTICLE_ID;
		}

		if ($this->aArticle !== null && $this->aArticle->getId() !== $v) {
			$this->aArticle = null;
		}

	} 
	
	public function setImgName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->img_name !== $v) {
			$this->img_name = $v;
			$this->modifiedColumns[] = ImagePeer::IMG_NAME;
		}

	} 
	
	public function setImageCaption($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->image_caption !== $v) {
			$this->image_caption = $v;
			$this->modifiedColumns[] = ImagePeer::IMAGE_CAPTION;
		}

	} 
	
	public function setIsHeadline($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->is_headline !== $v || $v === 0) {
			$this->is_headline = $v;
			$this->modifiedColumns[] = ImagePeer::IS_HEADLINE;
		}

	} 
	
	public function setImageType($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->image_type !== $v) {
			$this->image_type = $v;
			$this->modifiedColumns[] = ImagePeer::IMAGE_TYPE;
		}

	} 
	
	public function setMimeType($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->mime_type !== $v) {
			$this->mime_type = $v;
			$this->modifiedColumns[] = ImagePeer::MIME_TYPE;
		}

	} 
	
	public function setOriginalName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->original_name !== $v) {
			$this->original_name = $v;
			$this->modifiedColumns[] = ImagePeer::ORIGINAL_NAME;
		}

	} 
	
	public function setImageOriginalKey($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->image_original_key !== $v) {
			$this->image_original_key = $v;
			$this->modifiedColumns[] = ImagePeer::IMAGE_ORIGINAL_KEY;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->article_id = $rs->getInt($startcol + 1);

			$this->img_name = $rs->getString($startcol + 2);

			$this->image_caption = $rs->getString($startcol + 3);

			$this->is_headline = $rs->getInt($startcol + 4);

			$this->image_type = $rs->getString($startcol + 5);

			$this->mime_type = $rs->getString($startcol + 6);

			$this->original_name = $rs->getString($startcol + 7);

			$this->image_original_key = $rs->getString($startcol + 8);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 9; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Image object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ImagePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ImagePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ImagePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aArticle !== null) {
				if ($this->aArticle->isModified()) {
					$affectedRows += $this->aArticle->save($con);
				}
				$this->setArticle($this->aArticle);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ImagePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ImagePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aArticle !== null) {
				if (!$this->aArticle->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aArticle->getValidationFailures());
				}
			}


			if (($retval = ImagePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ImagePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getArticleId();
				break;
			case 2:
				return $this->getImgName();
				break;
			case 3:
				return $this->getImageCaption();
				break;
			case 4:
				return $this->getIsHeadline();
				break;
			case 5:
				return $this->getImageType();
				break;
			case 6:
				return $this->getMimeType();
				break;
			case 7:
				return $this->getOriginalName();
				break;
			case 8:
				return $this->getImageOriginalKey();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ImagePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getArticleId(),
			$keys[2] => $this->getImgName(),
			$keys[3] => $this->getImageCaption(),
			$keys[4] => $this->getIsHeadline(),
			$keys[5] => $this->getImageType(),
			$keys[6] => $this->getMimeType(),
			$keys[7] => $this->getOriginalName(),
			$keys[8] => $this->getImageOriginalKey(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ImagePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setArticleId($value);
				break;
			case 2:
				$this->setImgName($value);
				break;
			case 3:
				$this->setImageCaption($value);
				break;
			case 4:
				$this->setIsHeadline($value);
				break;
			case 5:
				$this->setImageType($value);
				break;
			case 6:
				$this->setMimeType($value);
				break;
			case 7:
				$this->setOriginalName($value);
				break;
			case 8:
				$this->setImageOriginalKey($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ImagePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setArticleId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setImgName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setImageCaption($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setIsHeadline($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setImageType($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setMimeType($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setOriginalName($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setImageOriginalKey($arr[$keys[8]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ImagePeer::DATABASE_NAME);

		if ($this->isColumnModified(ImagePeer::ID)) $criteria->add(ImagePeer::ID, $this->id);
		if ($this->isColumnModified(ImagePeer::ARTICLE_ID)) $criteria->add(ImagePeer::ARTICLE_ID, $this->article_id);
		if ($this->isColumnModified(ImagePeer::IMG_NAME)) $criteria->add(ImagePeer::IMG_NAME, $this->img_name);
		if ($this->isColumnModified(ImagePeer::IMAGE_CAPTION)) $criteria->add(ImagePeer::IMAGE_CAPTION, $this->image_caption);
		if ($this->isColumnModified(ImagePeer::IS_HEADLINE)) $criteria->add(ImagePeer::IS_HEADLINE, $this->is_headline);
		if ($this->isColumnModified(ImagePeer::IMAGE_TYPE)) $criteria->add(ImagePeer::IMAGE_TYPE, $this->image_type);
		if ($this->isColumnModified(ImagePeer::MIME_TYPE)) $criteria->add(ImagePeer::MIME_TYPE, $this->mime_type);
		if ($this->isColumnModified(ImagePeer::ORIGINAL_NAME)) $criteria->add(ImagePeer::ORIGINAL_NAME, $this->original_name);
		if ($this->isColumnModified(ImagePeer::IMAGE_ORIGINAL_KEY)) $criteria->add(ImagePeer::IMAGE_ORIGINAL_KEY, $this->image_original_key);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ImagePeer::DATABASE_NAME);

		$criteria->add(ImagePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setArticleId($this->article_id);

		$copyObj->setImgName($this->img_name);

		$copyObj->setImageCaption($this->image_caption);

		$copyObj->setIsHeadline($this->is_headline);

		$copyObj->setImageType($this->image_type);

		$copyObj->setMimeType($this->mime_type);

		$copyObj->setOriginalName($this->original_name);

		$copyObj->setImageOriginalKey($this->image_original_key);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ImagePeer();
		}
		return self::$peer;
	}

	
	public function setArticle($v)
	{


		if ($v === null) {
			$this->setArticleId(NULL);
		} else {
			$this->setArticleId($v->getId());
		}


		$this->aArticle = $v;
	}


	
	public function getArticle($con = null)
	{
		if ($this->aArticle === null && ($this->article_id !== null)) {
						include_once 'lib/model/om/BaseArticlePeer.php';

			$this->aArticle = ArticlePeer::retrieveByPK($this->article_id, $con);

			
		}
		return $this->aArticle;
	}

} 