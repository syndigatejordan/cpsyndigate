<?php


abstract class BaseTitlePeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'title';

	
	const CLASS_DEFAULT = 'lib.model.Title';

	
	const NUM_COLUMNS = 55;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'title.ID';

	
	const TITLE_FREQUENCY_ID = 'title.TITLE_FREQUENCY_ID';

	
	const TITLE_TYPE_ID = 'title.TITLE_TYPE_ID';

	
	const PUBLISHER_ID = 'title.PUBLISHER_ID';

	
	const NAME = 'title.NAME';

	
	const WEBSITE = 'title.WEBSITE';

	
	const LANGUAGE_ID = 'title.LANGUAGE_ID';

	
	const FTP_USERNAME = 'title.FTP_USERNAME';

	
	const FTP_PASSWORD = 'title.FTP_PASSWORD';

	
	const ARTICLES_THRESHOLD = 'title.ARTICLES_THRESHOLD';

	
	const UNIX_UID = 'title.UNIX_UID';

	
	const UNIX_GID = 'title.UNIX_GID';

	
	const NAGIOS_CONFIG_FILE = 'title.NAGIOS_CONFIG_FILE';

	
	const HOME_DIR = 'title.HOME_DIR';

	
	const WORK_DIR = 'title.WORK_DIR';

	
	const SVN_REPO = 'title.SVN_REPO';

	
	const GATHER_RETRIES = 'title.GATHER_RETRIES';

	
	const DEFAULT_CHARSET = 'title.DEFAULT_CHARSET';

	
	const PARSE_DIR = 'title.PARSE_DIR';

	
	const NEXT_TRY_TIMER = 'title.NEXT_TRY_TIMER';

	
	const COPYRIGHT = 'title.COPYRIGHT';

	
	const COPYRIGHT_ARABIC = 'title.COPYRIGHT_ARABIC';

	
	const IS_ACTIVE = 'title.IS_ACTIVE';

	
	const ACTIVATION_DATE = 'title.ACTIVATION_DATE';

	
	const LOGO_PATH = 'title.LOGO_PATH';

	
	const DIRECTORY_LAYOUT = 'title.DIRECTORY_LAYOUT';

	
	const IS_NEWSWIRE = 'title.IS_NEWSWIRE';

	
	const IS_GENERATE = 'title.IS_GENERATE';

	
	const COUNTRY_ID = 'title.COUNTRY_ID';

	
	const PHOTO_FEED = 'title.PHOTO_FEED';

	
	const CONTRACT = 'title.CONTRACT';

	
	const HAS_TECHNICAL_INTRO = 'title.HAS_TECHNICAL_INTRO';

	
	const IS_RECEIVING = 'title.IS_RECEIVING';

	
	const IS_PARSING = 'title.IS_PARSING';

	
	const TECHNICAL_NOTES = 'title.TECHNICAL_NOTES';

	
	const TIMELINESS = 'title.TIMELINESS';

	
	const CONTRACT_START_DATE = 'title.CONTRACT_START_DATE';

	
	const CONTRACT_END_DATE = 'title.CONTRACT_END_DATE';

	
	const ROYALTY_RATE = 'title.ROYALTY_RATE';

	
	const SOURCE_DESCRIPTION = 'title.SOURCE_DESCRIPTION';

	
	const RENEWAL_PERIOD = 'title.RENEWAL_PERIOD';

	
	const CITY = 'title.CITY';

	
	const STORY_COUNT = 'title.STORY_COUNT';

	
	const RECEIVING_STATUS = 'title.RECEIVING_STATUS';

	
	const RECEIVING_STATUS_COMMENT = 'title.RECEIVING_STATUS_COMMENT';

	
	const RECEIVING_STATUS_TIMESTAMP = 'title.RECEIVING_STATUS_TIMESTAMP';

	
	const NOTES = 'title.NOTES';

	
	const HAS_NEW_PARSED_CONTENT = 'title.HAS_NEW_PARSED_CONTENT';

	
	const LAST_CONTENT_TIMESTAMP = 'title.LAST_CONTENT_TIMESTAMP';

	
	const CACHED_ARTICLES_TODAY = 'title.CACHED_ARTICLES_TODAY';

	
	const CACHED_CURRENT_SPAN = 'title.CACHED_CURRENT_SPAN';

	
	const CACHED_PREV_SPAN = 'title.CACHED_PREV_SPAN';

	
	const CACHED_ARTICLES_COUNT = 'title.CACHED_ARTICLES_COUNT';

	
	const ADMINS_ID = 'title.ADMINS_ID';

	
	const RANK = 'title.RANK';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'TitleFrequencyId', 'TitleTypeId', 'PublisherId', 'Name', 'Website', 'LanguageId', 'FtpUsername', 'FtpPassword', 'ArticlesThreshold', 'UnixUid', 'UnixGid', 'NagiosConfigFile', 'HomeDir', 'WorkDir', 'SvnRepo', 'GatherRetries', 'DefaultCharset', 'ParseDir', 'NextTryTimer', 'Copyright', 'CopyrightArabic', 'IsActive', 'ActivationDate', 'LogoPath', 'DirectoryLayout', 'IsNewswire', 'IsGenerate', 'CountryId', 'PhotoFeed', 'Contract', 'HasTechnicalIntro', 'IsReceiving', 'IsParsing', 'TechnicalNotes', 'Timeliness', 'ContractStartDate', 'ContractEndDate', 'RoyaltyRate', 'SourceDescription', 'RenewalPeriod', 'City', 'StoryCount', 'ReceivingStatus', 'ReceivingStatusComment', 'ReceivingStatusTimestamp', 'Notes', 'HasNewParsedContent', 'LastContentTimestamp', 'CachedArticlesToday', 'CachedCurrentSpan', 'CachedPrevSpan', 'CachedArticlesCount', 'AdminsId', 'Rank', ),
		BasePeer::TYPE_COLNAME => array (TitlePeer::ID, TitlePeer::TITLE_FREQUENCY_ID, TitlePeer::TITLE_TYPE_ID, TitlePeer::PUBLISHER_ID, TitlePeer::NAME, TitlePeer::WEBSITE, TitlePeer::LANGUAGE_ID, TitlePeer::FTP_USERNAME, TitlePeer::FTP_PASSWORD, TitlePeer::ARTICLES_THRESHOLD, TitlePeer::UNIX_UID, TitlePeer::UNIX_GID, TitlePeer::NAGIOS_CONFIG_FILE, TitlePeer::HOME_DIR, TitlePeer::WORK_DIR, TitlePeer::SVN_REPO, TitlePeer::GATHER_RETRIES, TitlePeer::DEFAULT_CHARSET, TitlePeer::PARSE_DIR, TitlePeer::NEXT_TRY_TIMER, TitlePeer::COPYRIGHT, TitlePeer::COPYRIGHT_ARABIC, TitlePeer::IS_ACTIVE, TitlePeer::ACTIVATION_DATE, TitlePeer::LOGO_PATH, TitlePeer::DIRECTORY_LAYOUT, TitlePeer::IS_NEWSWIRE, TitlePeer::IS_GENERATE, TitlePeer::COUNTRY_ID, TitlePeer::PHOTO_FEED, TitlePeer::CONTRACT, TitlePeer::HAS_TECHNICAL_INTRO, TitlePeer::IS_RECEIVING, TitlePeer::IS_PARSING, TitlePeer::TECHNICAL_NOTES, TitlePeer::TIMELINESS, TitlePeer::CONTRACT_START_DATE, TitlePeer::CONTRACT_END_DATE, TitlePeer::ROYALTY_RATE, TitlePeer::SOURCE_DESCRIPTION, TitlePeer::RENEWAL_PERIOD, TitlePeer::CITY, TitlePeer::STORY_COUNT, TitlePeer::RECEIVING_STATUS, TitlePeer::RECEIVING_STATUS_COMMENT, TitlePeer::RECEIVING_STATUS_TIMESTAMP, TitlePeer::NOTES, TitlePeer::HAS_NEW_PARSED_CONTENT, TitlePeer::LAST_CONTENT_TIMESTAMP, TitlePeer::CACHED_ARTICLES_TODAY, TitlePeer::CACHED_CURRENT_SPAN, TitlePeer::CACHED_PREV_SPAN, TitlePeer::CACHED_ARTICLES_COUNT, TitlePeer::ADMINS_ID, TitlePeer::RANK, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'title_frequency_id', 'title_type_id', 'publisher_id', 'name', 'website', 'language_id', 'ftp_username', 'ftp_password', 'articles_threshold', 'unix_uid', 'unix_gid', 'nagios_config_file', 'home_dir', 'work_dir', 'svn_repo', 'gather_retries', 'default_charset', 'parse_dir', 'next_try_timer', 'copyright', 'copyright_arabic', 'is_active', 'activation_date', 'logo_path', 'directory_layout', 'is_newswire', 'is_generate', 'country_id', 'photo_feed', 'contract', 'has_technical_intro', 'is_receiving', 'is_parsing', 'technical_notes', 'timeliness', 'contract_start_date', 'contract_end_date', 'royalty_rate', 'source_description', 'renewal_period', 'city', 'story_count', 'receiving_status', 'receiving_status_comment', 'receiving_status_timestamp', 'notes', 'has_new_parsed_content', 'last_content_timestamp', 'cached_articles_today', 'cached_current_span', 'cached_prev_span', 'cached_articles_count', 'admins_id', 'rank', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'TitleFrequencyId' => 1, 'TitleTypeId' => 2, 'PublisherId' => 3, 'Name' => 4, 'Website' => 5, 'LanguageId' => 6, 'FtpUsername' => 7, 'FtpPassword' => 8, 'ArticlesThreshold' => 9, 'UnixUid' => 10, 'UnixGid' => 11, 'NagiosConfigFile' => 12, 'HomeDir' => 13, 'WorkDir' => 14, 'SvnRepo' => 15, 'GatherRetries' => 16, 'DefaultCharset' => 17, 'ParseDir' => 18, 'NextTryTimer' => 19, 'Copyright' => 20, 'CopyrightArabic' => 21, 'IsActive' => 22, 'ActivationDate' => 23, 'LogoPath' => 24, 'DirectoryLayout' => 25, 'IsNewswire' => 26, 'IsGenerate' => 27, 'CountryId' => 28, 'PhotoFeed' => 29, 'Contract' => 30, 'HasTechnicalIntro' => 31, 'IsReceiving' => 32, 'IsParsing' => 33, 'TechnicalNotes' => 34, 'Timeliness' => 35, 'ContractStartDate' => 36, 'ContractEndDate' => 37, 'RoyaltyRate' => 38, 'SourceDescription' => 39, 'RenewalPeriod' => 40, 'City' => 41, 'StoryCount' => 42, 'ReceivingStatus' => 43, 'ReceivingStatusComment' => 44, 'ReceivingStatusTimestamp' => 45, 'Notes' => 46, 'HasNewParsedContent' => 47, 'LastContentTimestamp' => 48, 'CachedArticlesToday' => 49, 'CachedCurrentSpan' => 50, 'CachedPrevSpan' => 51, 'CachedArticlesCount' => 52, 'AdminsId' => 53, 'Rank' => 54, ),
		BasePeer::TYPE_COLNAME => array (TitlePeer::ID => 0, TitlePeer::TITLE_FREQUENCY_ID => 1, TitlePeer::TITLE_TYPE_ID => 2, TitlePeer::PUBLISHER_ID => 3, TitlePeer::NAME => 4, TitlePeer::WEBSITE => 5, TitlePeer::LANGUAGE_ID => 6, TitlePeer::FTP_USERNAME => 7, TitlePeer::FTP_PASSWORD => 8, TitlePeer::ARTICLES_THRESHOLD => 9, TitlePeer::UNIX_UID => 10, TitlePeer::UNIX_GID => 11, TitlePeer::NAGIOS_CONFIG_FILE => 12, TitlePeer::HOME_DIR => 13, TitlePeer::WORK_DIR => 14, TitlePeer::SVN_REPO => 15, TitlePeer::GATHER_RETRIES => 16, TitlePeer::DEFAULT_CHARSET => 17, TitlePeer::PARSE_DIR => 18, TitlePeer::NEXT_TRY_TIMER => 19, TitlePeer::COPYRIGHT => 20, TitlePeer::COPYRIGHT_ARABIC => 21, TitlePeer::IS_ACTIVE => 22, TitlePeer::ACTIVATION_DATE => 23, TitlePeer::LOGO_PATH => 24, TitlePeer::DIRECTORY_LAYOUT => 25, TitlePeer::IS_NEWSWIRE => 26, TitlePeer::IS_GENERATE => 27, TitlePeer::COUNTRY_ID => 28, TitlePeer::PHOTO_FEED => 29, TitlePeer::CONTRACT => 30, TitlePeer::HAS_TECHNICAL_INTRO => 31, TitlePeer::IS_RECEIVING => 32, TitlePeer::IS_PARSING => 33, TitlePeer::TECHNICAL_NOTES => 34, TitlePeer::TIMELINESS => 35, TitlePeer::CONTRACT_START_DATE => 36, TitlePeer::CONTRACT_END_DATE => 37, TitlePeer::ROYALTY_RATE => 38, TitlePeer::SOURCE_DESCRIPTION => 39, TitlePeer::RENEWAL_PERIOD => 40, TitlePeer::CITY => 41, TitlePeer::STORY_COUNT => 42, TitlePeer::RECEIVING_STATUS => 43, TitlePeer::RECEIVING_STATUS_COMMENT => 44, TitlePeer::RECEIVING_STATUS_TIMESTAMP => 45, TitlePeer::NOTES => 46, TitlePeer::HAS_NEW_PARSED_CONTENT => 47, TitlePeer::LAST_CONTENT_TIMESTAMP => 48, TitlePeer::CACHED_ARTICLES_TODAY => 49, TitlePeer::CACHED_CURRENT_SPAN => 50, TitlePeer::CACHED_PREV_SPAN => 51, TitlePeer::CACHED_ARTICLES_COUNT => 52, TitlePeer::ADMINS_ID => 53, TitlePeer::RANK => 54, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'title_frequency_id' => 1, 'title_type_id' => 2, 'publisher_id' => 3, 'name' => 4, 'website' => 5, 'language_id' => 6, 'ftp_username' => 7, 'ftp_password' => 8, 'articles_threshold' => 9, 'unix_uid' => 10, 'unix_gid' => 11, 'nagios_config_file' => 12, 'home_dir' => 13, 'work_dir' => 14, 'svn_repo' => 15, 'gather_retries' => 16, 'default_charset' => 17, 'parse_dir' => 18, 'next_try_timer' => 19, 'copyright' => 20, 'copyright_arabic' => 21, 'is_active' => 22, 'activation_date' => 23, 'logo_path' => 24, 'directory_layout' => 25, 'is_newswire' => 26, 'is_generate' => 27, 'country_id' => 28, 'photo_feed' => 29, 'contract' => 30, 'has_technical_intro' => 31, 'is_receiving' => 32, 'is_parsing' => 33, 'technical_notes' => 34, 'timeliness' => 35, 'contract_start_date' => 36, 'contract_end_date' => 37, 'royalty_rate' => 38, 'source_description' => 39, 'renewal_period' => 40, 'city' => 41, 'story_count' => 42, 'receiving_status' => 43, 'receiving_status_comment' => 44, 'receiving_status_timestamp' => 45, 'notes' => 46, 'has_new_parsed_content' => 47, 'last_content_timestamp' => 48, 'cached_articles_today' => 49, 'cached_current_span' => 50, 'cached_prev_span' => 51, 'cached_articles_count' => 52, 'admins_id' => 53, 'rank' => 54, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/TitleMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.TitleMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = TitlePeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(TitlePeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(TitlePeer::ID);

		$criteria->addSelectColumn(TitlePeer::TITLE_FREQUENCY_ID);

		$criteria->addSelectColumn(TitlePeer::TITLE_TYPE_ID);

		$criteria->addSelectColumn(TitlePeer::PUBLISHER_ID);

		$criteria->addSelectColumn(TitlePeer::NAME);

		$criteria->addSelectColumn(TitlePeer::WEBSITE);

		$criteria->addSelectColumn(TitlePeer::LANGUAGE_ID);

		$criteria->addSelectColumn(TitlePeer::FTP_USERNAME);

		$criteria->addSelectColumn(TitlePeer::FTP_PASSWORD);

		$criteria->addSelectColumn(TitlePeer::ARTICLES_THRESHOLD);

		$criteria->addSelectColumn(TitlePeer::UNIX_UID);

		$criteria->addSelectColumn(TitlePeer::UNIX_GID);

		$criteria->addSelectColumn(TitlePeer::NAGIOS_CONFIG_FILE);

		$criteria->addSelectColumn(TitlePeer::HOME_DIR);

		$criteria->addSelectColumn(TitlePeer::WORK_DIR);

		$criteria->addSelectColumn(TitlePeer::SVN_REPO);

		$criteria->addSelectColumn(TitlePeer::GATHER_RETRIES);

		$criteria->addSelectColumn(TitlePeer::DEFAULT_CHARSET);

		$criteria->addSelectColumn(TitlePeer::PARSE_DIR);

		$criteria->addSelectColumn(TitlePeer::NEXT_TRY_TIMER);

		$criteria->addSelectColumn(TitlePeer::COPYRIGHT);

		$criteria->addSelectColumn(TitlePeer::COPYRIGHT_ARABIC);

		$criteria->addSelectColumn(TitlePeer::IS_ACTIVE);

		$criteria->addSelectColumn(TitlePeer::ACTIVATION_DATE);

		$criteria->addSelectColumn(TitlePeer::LOGO_PATH);

		$criteria->addSelectColumn(TitlePeer::DIRECTORY_LAYOUT);

		$criteria->addSelectColumn(TitlePeer::IS_NEWSWIRE);

		$criteria->addSelectColumn(TitlePeer::IS_GENERATE);

		$criteria->addSelectColumn(TitlePeer::COUNTRY_ID);

		$criteria->addSelectColumn(TitlePeer::PHOTO_FEED);

		$criteria->addSelectColumn(TitlePeer::CONTRACT);

		$criteria->addSelectColumn(TitlePeer::HAS_TECHNICAL_INTRO);

		$criteria->addSelectColumn(TitlePeer::IS_RECEIVING);

		$criteria->addSelectColumn(TitlePeer::IS_PARSING);

		$criteria->addSelectColumn(TitlePeer::TECHNICAL_NOTES);

		$criteria->addSelectColumn(TitlePeer::TIMELINESS);

		$criteria->addSelectColumn(TitlePeer::CONTRACT_START_DATE);

		$criteria->addSelectColumn(TitlePeer::CONTRACT_END_DATE);

		$criteria->addSelectColumn(TitlePeer::ROYALTY_RATE);

		$criteria->addSelectColumn(TitlePeer::SOURCE_DESCRIPTION);

		$criteria->addSelectColumn(TitlePeer::RENEWAL_PERIOD);

		$criteria->addSelectColumn(TitlePeer::CITY);

		$criteria->addSelectColumn(TitlePeer::STORY_COUNT);

		$criteria->addSelectColumn(TitlePeer::RECEIVING_STATUS);

		$criteria->addSelectColumn(TitlePeer::RECEIVING_STATUS_COMMENT);

		$criteria->addSelectColumn(TitlePeer::RECEIVING_STATUS_TIMESTAMP);

		$criteria->addSelectColumn(TitlePeer::NOTES);

		$criteria->addSelectColumn(TitlePeer::HAS_NEW_PARSED_CONTENT);

		$criteria->addSelectColumn(TitlePeer::LAST_CONTENT_TIMESTAMP);

		$criteria->addSelectColumn(TitlePeer::CACHED_ARTICLES_TODAY);

		$criteria->addSelectColumn(TitlePeer::CACHED_CURRENT_SPAN);

		$criteria->addSelectColumn(TitlePeer::CACHED_PREV_SPAN);

		$criteria->addSelectColumn(TitlePeer::CACHED_ARTICLES_COUNT);

		$criteria->addSelectColumn(TitlePeer::ADMINS_ID);

		$criteria->addSelectColumn(TitlePeer::RANK);

	}

	const COUNT = 'COUNT(title.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT title.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TitlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TitlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = TitlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = TitlePeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return TitlePeer::populateObjects(TitlePeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			TitlePeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = TitlePeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinPublisher(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TitlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TitlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(TitlePeer::PUBLISHER_ID, PublisherPeer::ID);

		$rs = TitlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinTitleType(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TitlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TitlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(TitlePeer::TITLE_TYPE_ID, TitleTypePeer::ID);

		$rs = TitlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinTitleFrequency(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TitlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TitlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(TitlePeer::TITLE_FREQUENCY_ID, TitleFrequencyPeer::ID);

		$rs = TitlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinCountry(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TitlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TitlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(TitlePeer::COUNTRY_ID, CountryPeer::ID);

		$rs = TitlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinLanguage(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TitlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TitlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(TitlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$rs = TitlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAdmins(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TitlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TitlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(TitlePeer::ADMINS_ID, AdminsPeer::ID);

		$rs = TitlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinPublisher(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		TitlePeer::addSelectColumns($c);
		$startcol = (TitlePeer::NUM_COLUMNS - TitlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		PublisherPeer::addSelectColumns($c);

		$c->addJoin(TitlePeer::PUBLISHER_ID, PublisherPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = TitlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = PublisherPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getPublisher(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addTitle($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initTitles();
				$obj2->addTitle($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinTitleType(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		TitlePeer::addSelectColumns($c);
		$startcol = (TitlePeer::NUM_COLUMNS - TitlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		TitleTypePeer::addSelectColumns($c);

		$c->addJoin(TitlePeer::TITLE_TYPE_ID, TitleTypePeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = TitlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = TitleTypePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getTitleType(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addTitle($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initTitles();
				$obj2->addTitle($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinTitleFrequency(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		TitlePeer::addSelectColumns($c);
		$startcol = (TitlePeer::NUM_COLUMNS - TitlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		TitleFrequencyPeer::addSelectColumns($c);

		$c->addJoin(TitlePeer::TITLE_FREQUENCY_ID, TitleFrequencyPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = TitlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = TitleFrequencyPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getTitleFrequency(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addTitle($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initTitles();
				$obj2->addTitle($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinCountry(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		TitlePeer::addSelectColumns($c);
		$startcol = (TitlePeer::NUM_COLUMNS - TitlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		CountryPeer::addSelectColumns($c);

		$c->addJoin(TitlePeer::COUNTRY_ID, CountryPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = TitlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = CountryPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getCountry(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addTitle($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initTitles();
				$obj2->addTitle($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinLanguage(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		TitlePeer::addSelectColumns($c);
		$startcol = (TitlePeer::NUM_COLUMNS - TitlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		LanguagePeer::addSelectColumns($c);

		$c->addJoin(TitlePeer::LANGUAGE_ID, LanguagePeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = TitlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = LanguagePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getLanguage(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addTitle($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initTitles();
				$obj2->addTitle($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAdmins(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		TitlePeer::addSelectColumns($c);
		$startcol = (TitlePeer::NUM_COLUMNS - TitlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		AdminsPeer::addSelectColumns($c);

		$c->addJoin(TitlePeer::ADMINS_ID, AdminsPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = TitlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = AdminsPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getAdmins(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addTitle($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initTitles();
				$obj2->addTitle($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TitlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TitlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(TitlePeer::PUBLISHER_ID, PublisherPeer::ID);

		$criteria->addJoin(TitlePeer::TITLE_TYPE_ID, TitleTypePeer::ID);

		$criteria->addJoin(TitlePeer::TITLE_FREQUENCY_ID, TitleFrequencyPeer::ID);

		$criteria->addJoin(TitlePeer::COUNTRY_ID, CountryPeer::ID);

		$criteria->addJoin(TitlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$criteria->addJoin(TitlePeer::ADMINS_ID, AdminsPeer::ID);

		$rs = TitlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		TitlePeer::addSelectColumns($c);
		$startcol2 = (TitlePeer::NUM_COLUMNS - TitlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		PublisherPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + PublisherPeer::NUM_COLUMNS;

		TitleTypePeer::addSelectColumns($c);
		$startcol4 = $startcol3 + TitleTypePeer::NUM_COLUMNS;

		TitleFrequencyPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + TitleFrequencyPeer::NUM_COLUMNS;

		CountryPeer::addSelectColumns($c);
		$startcol6 = $startcol5 + CountryPeer::NUM_COLUMNS;

		LanguagePeer::addSelectColumns($c);
		$startcol7 = $startcol6 + LanguagePeer::NUM_COLUMNS;

		AdminsPeer::addSelectColumns($c);
		$startcol8 = $startcol7 + AdminsPeer::NUM_COLUMNS;

		$c->addJoin(TitlePeer::PUBLISHER_ID, PublisherPeer::ID);

		$c->addJoin(TitlePeer::TITLE_TYPE_ID, TitleTypePeer::ID);

		$c->addJoin(TitlePeer::TITLE_FREQUENCY_ID, TitleFrequencyPeer::ID);

		$c->addJoin(TitlePeer::COUNTRY_ID, CountryPeer::ID);

		$c->addJoin(TitlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$c->addJoin(TitlePeer::ADMINS_ID, AdminsPeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = TitlePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = PublisherPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getPublisher(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addTitle($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initTitles();
				$obj2->addTitle($obj1);
			}


					
			$omClass = TitleTypePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3 = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getTitleType(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addTitle($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj3->initTitles();
				$obj3->addTitle($obj1);
			}


					
			$omClass = TitleFrequencyPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4 = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getTitleFrequency(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addTitle($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj4->initTitles();
				$obj4->addTitle($obj1);
			}


					
			$omClass = CountryPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj5 = new $cls();
			$obj5->hydrate($rs, $startcol5);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj5 = $temp_obj1->getCountry(); 				if ($temp_obj5->getPrimaryKey() === $obj5->getPrimaryKey()) {
					$newObject = false;
					$temp_obj5->addTitle($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj5->initTitles();
				$obj5->addTitle($obj1);
			}


					
			$omClass = LanguagePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj6 = new $cls();
			$obj6->hydrate($rs, $startcol6);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj6 = $temp_obj1->getLanguage(); 				if ($temp_obj6->getPrimaryKey() === $obj6->getPrimaryKey()) {
					$newObject = false;
					$temp_obj6->addTitle($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj6->initTitles();
				$obj6->addTitle($obj1);
			}


					
			$omClass = AdminsPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj7 = new $cls();
			$obj7->hydrate($rs, $startcol7);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj7 = $temp_obj1->getAdmins(); 				if ($temp_obj7->getPrimaryKey() === $obj7->getPrimaryKey()) {
					$newObject = false;
					$temp_obj7->addTitle($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj7->initTitles();
				$obj7->addTitle($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAllExceptPublisher(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TitlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TitlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(TitlePeer::TITLE_TYPE_ID, TitleTypePeer::ID);

		$criteria->addJoin(TitlePeer::TITLE_FREQUENCY_ID, TitleFrequencyPeer::ID);

		$criteria->addJoin(TitlePeer::COUNTRY_ID, CountryPeer::ID);

		$criteria->addJoin(TitlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$criteria->addJoin(TitlePeer::ADMINS_ID, AdminsPeer::ID);

		$rs = TitlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptTitleType(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TitlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TitlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(TitlePeer::PUBLISHER_ID, PublisherPeer::ID);

		$criteria->addJoin(TitlePeer::TITLE_FREQUENCY_ID, TitleFrequencyPeer::ID);

		$criteria->addJoin(TitlePeer::COUNTRY_ID, CountryPeer::ID);

		$criteria->addJoin(TitlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$criteria->addJoin(TitlePeer::ADMINS_ID, AdminsPeer::ID);

		$rs = TitlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptTitleFrequency(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TitlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TitlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(TitlePeer::PUBLISHER_ID, PublisherPeer::ID);

		$criteria->addJoin(TitlePeer::TITLE_TYPE_ID, TitleTypePeer::ID);

		$criteria->addJoin(TitlePeer::COUNTRY_ID, CountryPeer::ID);

		$criteria->addJoin(TitlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$criteria->addJoin(TitlePeer::ADMINS_ID, AdminsPeer::ID);

		$rs = TitlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptCountry(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TitlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TitlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(TitlePeer::PUBLISHER_ID, PublisherPeer::ID);

		$criteria->addJoin(TitlePeer::TITLE_TYPE_ID, TitleTypePeer::ID);

		$criteria->addJoin(TitlePeer::TITLE_FREQUENCY_ID, TitleFrequencyPeer::ID);

		$criteria->addJoin(TitlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$criteria->addJoin(TitlePeer::ADMINS_ID, AdminsPeer::ID);

		$rs = TitlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptLanguage(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TitlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TitlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(TitlePeer::PUBLISHER_ID, PublisherPeer::ID);

		$criteria->addJoin(TitlePeer::TITLE_TYPE_ID, TitleTypePeer::ID);

		$criteria->addJoin(TitlePeer::TITLE_FREQUENCY_ID, TitleFrequencyPeer::ID);

		$criteria->addJoin(TitlePeer::COUNTRY_ID, CountryPeer::ID);

		$criteria->addJoin(TitlePeer::ADMINS_ID, AdminsPeer::ID);

		$rs = TitlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptAdmins(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TitlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TitlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(TitlePeer::PUBLISHER_ID, PublisherPeer::ID);

		$criteria->addJoin(TitlePeer::TITLE_TYPE_ID, TitleTypePeer::ID);

		$criteria->addJoin(TitlePeer::TITLE_FREQUENCY_ID, TitleFrequencyPeer::ID);

		$criteria->addJoin(TitlePeer::COUNTRY_ID, CountryPeer::ID);

		$criteria->addJoin(TitlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$rs = TitlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAllExceptPublisher(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		TitlePeer::addSelectColumns($c);
		$startcol2 = (TitlePeer::NUM_COLUMNS - TitlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		TitleTypePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + TitleTypePeer::NUM_COLUMNS;

		TitleFrequencyPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + TitleFrequencyPeer::NUM_COLUMNS;

		CountryPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + CountryPeer::NUM_COLUMNS;

		LanguagePeer::addSelectColumns($c);
		$startcol6 = $startcol5 + LanguagePeer::NUM_COLUMNS;

		AdminsPeer::addSelectColumns($c);
		$startcol7 = $startcol6 + AdminsPeer::NUM_COLUMNS;

		$c->addJoin(TitlePeer::TITLE_TYPE_ID, TitleTypePeer::ID);

		$c->addJoin(TitlePeer::TITLE_FREQUENCY_ID, TitleFrequencyPeer::ID);

		$c->addJoin(TitlePeer::COUNTRY_ID, CountryPeer::ID);

		$c->addJoin(TitlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$c->addJoin(TitlePeer::ADMINS_ID, AdminsPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = TitlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = TitleTypePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getTitleType(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initTitles();
				$obj2->addTitle($obj1);
			}

			$omClass = TitleFrequencyPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getTitleFrequency(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initTitles();
				$obj3->addTitle($obj1);
			}

			$omClass = CountryPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getCountry(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initTitles();
				$obj4->addTitle($obj1);
			}

			$omClass = LanguagePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj5  = new $cls();
			$obj5->hydrate($rs, $startcol5);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj5 = $temp_obj1->getLanguage(); 				if ($temp_obj5->getPrimaryKey() === $obj5->getPrimaryKey()) {
					$newObject = false;
					$temp_obj5->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj5->initTitles();
				$obj5->addTitle($obj1);
			}

			$omClass = AdminsPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj6  = new $cls();
			$obj6->hydrate($rs, $startcol6);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj6 = $temp_obj1->getAdmins(); 				if ($temp_obj6->getPrimaryKey() === $obj6->getPrimaryKey()) {
					$newObject = false;
					$temp_obj6->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj6->initTitles();
				$obj6->addTitle($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptTitleType(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		TitlePeer::addSelectColumns($c);
		$startcol2 = (TitlePeer::NUM_COLUMNS - TitlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		PublisherPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + PublisherPeer::NUM_COLUMNS;

		TitleFrequencyPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + TitleFrequencyPeer::NUM_COLUMNS;

		CountryPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + CountryPeer::NUM_COLUMNS;

		LanguagePeer::addSelectColumns($c);
		$startcol6 = $startcol5 + LanguagePeer::NUM_COLUMNS;

		AdminsPeer::addSelectColumns($c);
		$startcol7 = $startcol6 + AdminsPeer::NUM_COLUMNS;

		$c->addJoin(TitlePeer::PUBLISHER_ID, PublisherPeer::ID);

		$c->addJoin(TitlePeer::TITLE_FREQUENCY_ID, TitleFrequencyPeer::ID);

		$c->addJoin(TitlePeer::COUNTRY_ID, CountryPeer::ID);

		$c->addJoin(TitlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$c->addJoin(TitlePeer::ADMINS_ID, AdminsPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = TitlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = PublisherPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getPublisher(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initTitles();
				$obj2->addTitle($obj1);
			}

			$omClass = TitleFrequencyPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getTitleFrequency(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initTitles();
				$obj3->addTitle($obj1);
			}

			$omClass = CountryPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getCountry(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initTitles();
				$obj4->addTitle($obj1);
			}

			$omClass = LanguagePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj5  = new $cls();
			$obj5->hydrate($rs, $startcol5);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj5 = $temp_obj1->getLanguage(); 				if ($temp_obj5->getPrimaryKey() === $obj5->getPrimaryKey()) {
					$newObject = false;
					$temp_obj5->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj5->initTitles();
				$obj5->addTitle($obj1);
			}

			$omClass = AdminsPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj6  = new $cls();
			$obj6->hydrate($rs, $startcol6);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj6 = $temp_obj1->getAdmins(); 				if ($temp_obj6->getPrimaryKey() === $obj6->getPrimaryKey()) {
					$newObject = false;
					$temp_obj6->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj6->initTitles();
				$obj6->addTitle($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptTitleFrequency(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		TitlePeer::addSelectColumns($c);
		$startcol2 = (TitlePeer::NUM_COLUMNS - TitlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		PublisherPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + PublisherPeer::NUM_COLUMNS;

		TitleTypePeer::addSelectColumns($c);
		$startcol4 = $startcol3 + TitleTypePeer::NUM_COLUMNS;

		CountryPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + CountryPeer::NUM_COLUMNS;

		LanguagePeer::addSelectColumns($c);
		$startcol6 = $startcol5 + LanguagePeer::NUM_COLUMNS;

		AdminsPeer::addSelectColumns($c);
		$startcol7 = $startcol6 + AdminsPeer::NUM_COLUMNS;

		$c->addJoin(TitlePeer::PUBLISHER_ID, PublisherPeer::ID);

		$c->addJoin(TitlePeer::TITLE_TYPE_ID, TitleTypePeer::ID);

		$c->addJoin(TitlePeer::COUNTRY_ID, CountryPeer::ID);

		$c->addJoin(TitlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$c->addJoin(TitlePeer::ADMINS_ID, AdminsPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = TitlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = PublisherPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getPublisher(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initTitles();
				$obj2->addTitle($obj1);
			}

			$omClass = TitleTypePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getTitleType(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initTitles();
				$obj3->addTitle($obj1);
			}

			$omClass = CountryPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getCountry(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initTitles();
				$obj4->addTitle($obj1);
			}

			$omClass = LanguagePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj5  = new $cls();
			$obj5->hydrate($rs, $startcol5);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj5 = $temp_obj1->getLanguage(); 				if ($temp_obj5->getPrimaryKey() === $obj5->getPrimaryKey()) {
					$newObject = false;
					$temp_obj5->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj5->initTitles();
				$obj5->addTitle($obj1);
			}

			$omClass = AdminsPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj6  = new $cls();
			$obj6->hydrate($rs, $startcol6);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj6 = $temp_obj1->getAdmins(); 				if ($temp_obj6->getPrimaryKey() === $obj6->getPrimaryKey()) {
					$newObject = false;
					$temp_obj6->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj6->initTitles();
				$obj6->addTitle($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptCountry(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		TitlePeer::addSelectColumns($c);
		$startcol2 = (TitlePeer::NUM_COLUMNS - TitlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		PublisherPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + PublisherPeer::NUM_COLUMNS;

		TitleTypePeer::addSelectColumns($c);
		$startcol4 = $startcol3 + TitleTypePeer::NUM_COLUMNS;

		TitleFrequencyPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + TitleFrequencyPeer::NUM_COLUMNS;

		LanguagePeer::addSelectColumns($c);
		$startcol6 = $startcol5 + LanguagePeer::NUM_COLUMNS;

		AdminsPeer::addSelectColumns($c);
		$startcol7 = $startcol6 + AdminsPeer::NUM_COLUMNS;

		$c->addJoin(TitlePeer::PUBLISHER_ID, PublisherPeer::ID);

		$c->addJoin(TitlePeer::TITLE_TYPE_ID, TitleTypePeer::ID);

		$c->addJoin(TitlePeer::TITLE_FREQUENCY_ID, TitleFrequencyPeer::ID);

		$c->addJoin(TitlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$c->addJoin(TitlePeer::ADMINS_ID, AdminsPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = TitlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = PublisherPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getPublisher(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initTitles();
				$obj2->addTitle($obj1);
			}

			$omClass = TitleTypePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getTitleType(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initTitles();
				$obj3->addTitle($obj1);
			}

			$omClass = TitleFrequencyPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getTitleFrequency(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initTitles();
				$obj4->addTitle($obj1);
			}

			$omClass = LanguagePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj5  = new $cls();
			$obj5->hydrate($rs, $startcol5);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj5 = $temp_obj1->getLanguage(); 				if ($temp_obj5->getPrimaryKey() === $obj5->getPrimaryKey()) {
					$newObject = false;
					$temp_obj5->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj5->initTitles();
				$obj5->addTitle($obj1);
			}

			$omClass = AdminsPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj6  = new $cls();
			$obj6->hydrate($rs, $startcol6);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj6 = $temp_obj1->getAdmins(); 				if ($temp_obj6->getPrimaryKey() === $obj6->getPrimaryKey()) {
					$newObject = false;
					$temp_obj6->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj6->initTitles();
				$obj6->addTitle($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptLanguage(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		TitlePeer::addSelectColumns($c);
		$startcol2 = (TitlePeer::NUM_COLUMNS - TitlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		PublisherPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + PublisherPeer::NUM_COLUMNS;

		TitleTypePeer::addSelectColumns($c);
		$startcol4 = $startcol3 + TitleTypePeer::NUM_COLUMNS;

		TitleFrequencyPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + TitleFrequencyPeer::NUM_COLUMNS;

		CountryPeer::addSelectColumns($c);
		$startcol6 = $startcol5 + CountryPeer::NUM_COLUMNS;

		AdminsPeer::addSelectColumns($c);
		$startcol7 = $startcol6 + AdminsPeer::NUM_COLUMNS;

		$c->addJoin(TitlePeer::PUBLISHER_ID, PublisherPeer::ID);

		$c->addJoin(TitlePeer::TITLE_TYPE_ID, TitleTypePeer::ID);

		$c->addJoin(TitlePeer::TITLE_FREQUENCY_ID, TitleFrequencyPeer::ID);

		$c->addJoin(TitlePeer::COUNTRY_ID, CountryPeer::ID);

		$c->addJoin(TitlePeer::ADMINS_ID, AdminsPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = TitlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = PublisherPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getPublisher(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initTitles();
				$obj2->addTitle($obj1);
			}

			$omClass = TitleTypePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getTitleType(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initTitles();
				$obj3->addTitle($obj1);
			}

			$omClass = TitleFrequencyPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getTitleFrequency(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initTitles();
				$obj4->addTitle($obj1);
			}

			$omClass = CountryPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj5  = new $cls();
			$obj5->hydrate($rs, $startcol5);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj5 = $temp_obj1->getCountry(); 				if ($temp_obj5->getPrimaryKey() === $obj5->getPrimaryKey()) {
					$newObject = false;
					$temp_obj5->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj5->initTitles();
				$obj5->addTitle($obj1);
			}

			$omClass = AdminsPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj6  = new $cls();
			$obj6->hydrate($rs, $startcol6);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj6 = $temp_obj1->getAdmins(); 				if ($temp_obj6->getPrimaryKey() === $obj6->getPrimaryKey()) {
					$newObject = false;
					$temp_obj6->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj6->initTitles();
				$obj6->addTitle($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptAdmins(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		TitlePeer::addSelectColumns($c);
		$startcol2 = (TitlePeer::NUM_COLUMNS - TitlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		PublisherPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + PublisherPeer::NUM_COLUMNS;

		TitleTypePeer::addSelectColumns($c);
		$startcol4 = $startcol3 + TitleTypePeer::NUM_COLUMNS;

		TitleFrequencyPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + TitleFrequencyPeer::NUM_COLUMNS;

		CountryPeer::addSelectColumns($c);
		$startcol6 = $startcol5 + CountryPeer::NUM_COLUMNS;

		LanguagePeer::addSelectColumns($c);
		$startcol7 = $startcol6 + LanguagePeer::NUM_COLUMNS;

		$c->addJoin(TitlePeer::PUBLISHER_ID, PublisherPeer::ID);

		$c->addJoin(TitlePeer::TITLE_TYPE_ID, TitleTypePeer::ID);

		$c->addJoin(TitlePeer::TITLE_FREQUENCY_ID, TitleFrequencyPeer::ID);

		$c->addJoin(TitlePeer::COUNTRY_ID, CountryPeer::ID);

		$c->addJoin(TitlePeer::LANGUAGE_ID, LanguagePeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = TitlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = PublisherPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getPublisher(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initTitles();
				$obj2->addTitle($obj1);
			}

			$omClass = TitleTypePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getTitleType(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initTitles();
				$obj3->addTitle($obj1);
			}

			$omClass = TitleFrequencyPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getTitleFrequency(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initTitles();
				$obj4->addTitle($obj1);
			}

			$omClass = CountryPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj5  = new $cls();
			$obj5->hydrate($rs, $startcol5);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj5 = $temp_obj1->getCountry(); 				if ($temp_obj5->getPrimaryKey() === $obj5->getPrimaryKey()) {
					$newObject = false;
					$temp_obj5->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj5->initTitles();
				$obj5->addTitle($obj1);
			}

			$omClass = LanguagePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj6  = new $cls();
			$obj6->hydrate($rs, $startcol6);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj6 = $temp_obj1->getLanguage(); 				if ($temp_obj6->getPrimaryKey() === $obj6->getPrimaryKey()) {
					$newObject = false;
					$temp_obj6->addTitle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj6->initTitles();
				$obj6->addTitle($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return TitlePeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(TitlePeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(TitlePeer::ID);
			$selectCriteria->add(TitlePeer::ID, $criteria->remove(TitlePeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(TitlePeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(TitlePeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Title) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(TitlePeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Title $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(TitlePeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(TitlePeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(TitlePeer::DATABASE_NAME, TitlePeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = TitlePeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(TitlePeer::DATABASE_NAME);

		$criteria->add(TitlePeer::ID, $pk);


		$v = TitlePeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(TitlePeer::ID, $pks, Criteria::IN);
			$objs = TitlePeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseTitlePeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/TitleMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.TitleMapBuilder');
}
