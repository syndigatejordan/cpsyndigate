<?php


abstract class BaseLogGatherPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'log_gather';

	
	const CLASS_DEFAULT = 'lib.model.LogGather';

	
	const NUM_COLUMNS = 8;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'log_gather.ID';

	
	const REPORT_GATHER_ID = 'log_gather.REPORT_GATHER_ID';

	
	const CATEGORY = 'log_gather.CATEGORY';

	
	const MESSAGE = 'log_gather.MESSAGE';

	
	const SEVERITY = 'log_gather.SEVERITY';

	
	const SOURCE = 'log_gather.SOURCE';

	
	const TIME = 'log_gather.TIME';

	
	const REPORT_ID = 'log_gather.REPORT_ID';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'ReportGatherId', 'Category', 'Message', 'Severity', 'Source', 'Time', 'ReportId', ),
		BasePeer::TYPE_COLNAME => array (LogGatherPeer::ID, LogGatherPeer::REPORT_GATHER_ID, LogGatherPeer::CATEGORY, LogGatherPeer::MESSAGE, LogGatherPeer::SEVERITY, LogGatherPeer::SOURCE, LogGatherPeer::TIME, LogGatherPeer::REPORT_ID, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'report_gather_id', 'category', 'message', 'severity', 'source', 'time', 'report_id', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'ReportGatherId' => 1, 'Category' => 2, 'Message' => 3, 'Severity' => 4, 'Source' => 5, 'Time' => 6, 'ReportId' => 7, ),
		BasePeer::TYPE_COLNAME => array (LogGatherPeer::ID => 0, LogGatherPeer::REPORT_GATHER_ID => 1, LogGatherPeer::CATEGORY => 2, LogGatherPeer::MESSAGE => 3, LogGatherPeer::SEVERITY => 4, LogGatherPeer::SOURCE => 5, LogGatherPeer::TIME => 6, LogGatherPeer::REPORT_ID => 7, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'report_gather_id' => 1, 'category' => 2, 'message' => 3, 'severity' => 4, 'source' => 5, 'time' => 6, 'report_id' => 7, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/LogGatherMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.LogGatherMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = LogGatherPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(LogGatherPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(LogGatherPeer::ID);

		$criteria->addSelectColumn(LogGatherPeer::REPORT_GATHER_ID);

		$criteria->addSelectColumn(LogGatherPeer::CATEGORY);

		$criteria->addSelectColumn(LogGatherPeer::MESSAGE);

		$criteria->addSelectColumn(LogGatherPeer::SEVERITY);

		$criteria->addSelectColumn(LogGatherPeer::SOURCE);

		$criteria->addSelectColumn(LogGatherPeer::TIME);

		$criteria->addSelectColumn(LogGatherPeer::REPORT_ID);

	}

	const COUNT = 'COUNT(log_gather.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT log_gather.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(LogGatherPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(LogGatherPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = LogGatherPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = LogGatherPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return LogGatherPeer::populateObjects(LogGatherPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			LogGatherPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = LogGatherPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return LogGatherPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(LogGatherPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(LogGatherPeer::ID);
			$selectCriteria->add(LogGatherPeer::ID, $criteria->remove(LogGatherPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(LogGatherPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(LogGatherPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof LogGather) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(LogGatherPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(LogGather $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(LogGatherPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(LogGatherPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(LogGatherPeer::DATABASE_NAME, LogGatherPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = LogGatherPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(LogGatherPeer::DATABASE_NAME);

		$criteria->add(LogGatherPeer::ID, $pk);


		$v = LogGatherPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(LogGatherPeer::ID, $pks, Criteria::IN);
			$objs = LogGatherPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseLogGatherPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/LogGatherMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.LogGatherMapBuilder');
}
