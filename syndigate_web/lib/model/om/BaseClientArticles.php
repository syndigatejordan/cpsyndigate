<?php


abstract class BaseClientArticles extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $title_id = 0;


	
	protected $body;


	
	protected $providername = '';


	
	protected $categoryname = '';


	
	protected $originalcategory = '';


	
	protected $headlineimage = '';


	
	protected $headlineimagecaption = '';


	
	protected $articledate = '';


	
	protected $articleid = 0;


	
	protected $generated_time = 0;


	
	protected $headline = '';

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getTitleId()
	{

		return $this->title_id;
	}

	
	public function getBody()
	{

		return $this->body;
	}

	
	public function getProvidername()
	{

		return $this->providername;
	}

	
	public function getCategoryname()
	{

		return $this->categoryname;
	}

	
	public function getOriginalcategory()
	{

		return $this->originalcategory;
	}

	
	public function getHeadlineimage()
	{

		return $this->headlineimage;
	}

	
	public function getHeadlineimagecaption()
	{

		return $this->headlineimagecaption;
	}

	
	public function getArticledate()
	{

		return $this->articledate;
	}

	
	public function getArticleid()
	{

		return $this->articleid;
	}

	
	public function getGeneratedTime()
	{

		return $this->generated_time;
	}

	
	public function getHeadline()
	{

		return $this->headline;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ClientArticlesPeer::ID;
		}

	} 
	
	public function setTitleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_id !== $v || $v === 0) {
			$this->title_id = $v;
			$this->modifiedColumns[] = ClientArticlesPeer::TITLE_ID;
		}

	} 
	
	public function setBody($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->body !== $v) {
			$this->body = $v;
			$this->modifiedColumns[] = ClientArticlesPeer::BODY;
		}

	} 
	
	public function setProvidername($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->providername !== $v || $v === '') {
			$this->providername = $v;
			$this->modifiedColumns[] = ClientArticlesPeer::PROVIDERNAME;
		}

	} 
	
	public function setCategoryname($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->categoryname !== $v || $v === '') {
			$this->categoryname = $v;
			$this->modifiedColumns[] = ClientArticlesPeer::CATEGORYNAME;
		}

	} 
	
	public function setOriginalcategory($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->originalcategory !== $v || $v === '') {
			$this->originalcategory = $v;
			$this->modifiedColumns[] = ClientArticlesPeer::ORIGINALCATEGORY;
		}

	} 
	
	public function setHeadlineimage($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->headlineimage !== $v || $v === '') {
			$this->headlineimage = $v;
			$this->modifiedColumns[] = ClientArticlesPeer::HEADLINEIMAGE;
		}

	} 
	
	public function setHeadlineimagecaption($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->headlineimagecaption !== $v || $v === '') {
			$this->headlineimagecaption = $v;
			$this->modifiedColumns[] = ClientArticlesPeer::HEADLINEIMAGECAPTION;
		}

	} 
	
	public function setArticledate($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->articledate !== $v || $v === '') {
			$this->articledate = $v;
			$this->modifiedColumns[] = ClientArticlesPeer::ARTICLEDATE;
		}

	} 
	
	public function setArticleid($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->articleid !== $v || $v === 0) {
			$this->articleid = $v;
			$this->modifiedColumns[] = ClientArticlesPeer::ARTICLEID;
		}

	} 
	
	public function setGeneratedTime($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->generated_time !== $v || $v === 0) {
			$this->generated_time = $v;
			$this->modifiedColumns[] = ClientArticlesPeer::GENERATED_TIME;
		}

	} 
	
	public function setHeadline($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->headline !== $v || $v === '') {
			$this->headline = $v;
			$this->modifiedColumns[] = ClientArticlesPeer::HEADLINE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->title_id = $rs->getInt($startcol + 1);

			$this->body = $rs->getString($startcol + 2);

			$this->providername = $rs->getString($startcol + 3);

			$this->categoryname = $rs->getString($startcol + 4);

			$this->originalcategory = $rs->getString($startcol + 5);

			$this->headlineimage = $rs->getString($startcol + 6);

			$this->headlineimagecaption = $rs->getString($startcol + 7);

			$this->articledate = $rs->getString($startcol + 8);

			$this->articleid = $rs->getInt($startcol + 9);

			$this->generated_time = $rs->getInt($startcol + 10);

			$this->headline = $rs->getString($startcol + 11);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 12; 
		} catch (Exception $e) {
			throw new PropelException("Error populating ClientArticles object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ClientArticlesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ClientArticlesPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ClientArticlesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ClientArticlesPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ClientArticlesPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = ClientArticlesPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ClientArticlesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTitleId();
				break;
			case 2:
				return $this->getBody();
				break;
			case 3:
				return $this->getProvidername();
				break;
			case 4:
				return $this->getCategoryname();
				break;
			case 5:
				return $this->getOriginalcategory();
				break;
			case 6:
				return $this->getHeadlineimage();
				break;
			case 7:
				return $this->getHeadlineimagecaption();
				break;
			case 8:
				return $this->getArticledate();
				break;
			case 9:
				return $this->getArticleid();
				break;
			case 10:
				return $this->getGeneratedTime();
				break;
			case 11:
				return $this->getHeadline();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ClientArticlesPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTitleId(),
			$keys[2] => $this->getBody(),
			$keys[3] => $this->getProvidername(),
			$keys[4] => $this->getCategoryname(),
			$keys[5] => $this->getOriginalcategory(),
			$keys[6] => $this->getHeadlineimage(),
			$keys[7] => $this->getHeadlineimagecaption(),
			$keys[8] => $this->getArticledate(),
			$keys[9] => $this->getArticleid(),
			$keys[10] => $this->getGeneratedTime(),
			$keys[11] => $this->getHeadline(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ClientArticlesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTitleId($value);
				break;
			case 2:
				$this->setBody($value);
				break;
			case 3:
				$this->setProvidername($value);
				break;
			case 4:
				$this->setCategoryname($value);
				break;
			case 5:
				$this->setOriginalcategory($value);
				break;
			case 6:
				$this->setHeadlineimage($value);
				break;
			case 7:
				$this->setHeadlineimagecaption($value);
				break;
			case 8:
				$this->setArticledate($value);
				break;
			case 9:
				$this->setArticleid($value);
				break;
			case 10:
				$this->setGeneratedTime($value);
				break;
			case 11:
				$this->setHeadline($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ClientArticlesPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTitleId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setBody($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setProvidername($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setCategoryname($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setOriginalcategory($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setHeadlineimage($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setHeadlineimagecaption($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setArticledate($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setArticleid($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setGeneratedTime($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setHeadline($arr[$keys[11]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ClientArticlesPeer::DATABASE_NAME);

		if ($this->isColumnModified(ClientArticlesPeer::ID)) $criteria->add(ClientArticlesPeer::ID, $this->id);
		if ($this->isColumnModified(ClientArticlesPeer::TITLE_ID)) $criteria->add(ClientArticlesPeer::TITLE_ID, $this->title_id);
		if ($this->isColumnModified(ClientArticlesPeer::BODY)) $criteria->add(ClientArticlesPeer::BODY, $this->body);
		if ($this->isColumnModified(ClientArticlesPeer::PROVIDERNAME)) $criteria->add(ClientArticlesPeer::PROVIDERNAME, $this->providername);
		if ($this->isColumnModified(ClientArticlesPeer::CATEGORYNAME)) $criteria->add(ClientArticlesPeer::CATEGORYNAME, $this->categoryname);
		if ($this->isColumnModified(ClientArticlesPeer::ORIGINALCATEGORY)) $criteria->add(ClientArticlesPeer::ORIGINALCATEGORY, $this->originalcategory);
		if ($this->isColumnModified(ClientArticlesPeer::HEADLINEIMAGE)) $criteria->add(ClientArticlesPeer::HEADLINEIMAGE, $this->headlineimage);
		if ($this->isColumnModified(ClientArticlesPeer::HEADLINEIMAGECAPTION)) $criteria->add(ClientArticlesPeer::HEADLINEIMAGECAPTION, $this->headlineimagecaption);
		if ($this->isColumnModified(ClientArticlesPeer::ARTICLEDATE)) $criteria->add(ClientArticlesPeer::ARTICLEDATE, $this->articledate);
		if ($this->isColumnModified(ClientArticlesPeer::ARTICLEID)) $criteria->add(ClientArticlesPeer::ARTICLEID, $this->articleid);
		if ($this->isColumnModified(ClientArticlesPeer::GENERATED_TIME)) $criteria->add(ClientArticlesPeer::GENERATED_TIME, $this->generated_time);
		if ($this->isColumnModified(ClientArticlesPeer::HEADLINE)) $criteria->add(ClientArticlesPeer::HEADLINE, $this->headline);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ClientArticlesPeer::DATABASE_NAME);

		$criteria->add(ClientArticlesPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTitleId($this->title_id);

		$copyObj->setBody($this->body);

		$copyObj->setProvidername($this->providername);

		$copyObj->setCategoryname($this->categoryname);

		$copyObj->setOriginalcategory($this->originalcategory);

		$copyObj->setHeadlineimage($this->headlineimage);

		$copyObj->setHeadlineimagecaption($this->headlineimagecaption);

		$copyObj->setArticledate($this->articledate);

		$copyObj->setArticleid($this->articleid);

		$copyObj->setGeneratedTime($this->generated_time);

		$copyObj->setHeadline($this->headline);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ClientArticlesPeer();
		}
		return self::$peer;
	}

} 