<?php


abstract class BaseLogClientRulePeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'log_client_rule';

	
	const CLASS_DEFAULT = 'lib.model.LogClientRule';

	
	const NUM_COLUMNS = 7;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'log_client_rule.ID';

	
	const TIMESTAMP = 'log_client_rule.TIMESTAMP';

	
	const ADMIN_ID = 'log_client_rule.ADMIN_ID';

	
	const CLIENT_ID = 'log_client_rule.CLIENT_ID';

	
	const PREVIOUS_STATUS = 'log_client_rule.PREVIOUS_STATUS';

	
	const OPERATION = 'log_client_rule.OPERATION';

	
	const CURRENT_STATUS = 'log_client_rule.CURRENT_STATUS';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'Timestamp', 'AdminId', 'ClientId', 'PreviousStatus', 'Operation', 'CurrentStatus', ),
		BasePeer::TYPE_COLNAME => array (LogClientRulePeer::ID, LogClientRulePeer::TIMESTAMP, LogClientRulePeer::ADMIN_ID, LogClientRulePeer::CLIENT_ID, LogClientRulePeer::PREVIOUS_STATUS, LogClientRulePeer::OPERATION, LogClientRulePeer::CURRENT_STATUS, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'timestamp', 'admin_id', 'client_id', 'previous_status', 'operation', 'current_status', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Timestamp' => 1, 'AdminId' => 2, 'ClientId' => 3, 'PreviousStatus' => 4, 'Operation' => 5, 'CurrentStatus' => 6, ),
		BasePeer::TYPE_COLNAME => array (LogClientRulePeer::ID => 0, LogClientRulePeer::TIMESTAMP => 1, LogClientRulePeer::ADMIN_ID => 2, LogClientRulePeer::CLIENT_ID => 3, LogClientRulePeer::PREVIOUS_STATUS => 4, LogClientRulePeer::OPERATION => 5, LogClientRulePeer::CURRENT_STATUS => 6, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'timestamp' => 1, 'admin_id' => 2, 'client_id' => 3, 'previous_status' => 4, 'operation' => 5, 'current_status' => 6, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/LogClientRuleMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.LogClientRuleMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = LogClientRulePeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(LogClientRulePeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(LogClientRulePeer::ID);

		$criteria->addSelectColumn(LogClientRulePeer::TIMESTAMP);

		$criteria->addSelectColumn(LogClientRulePeer::ADMIN_ID);

		$criteria->addSelectColumn(LogClientRulePeer::CLIENT_ID);

		$criteria->addSelectColumn(LogClientRulePeer::PREVIOUS_STATUS);

		$criteria->addSelectColumn(LogClientRulePeer::OPERATION);

		$criteria->addSelectColumn(LogClientRulePeer::CURRENT_STATUS);

	}

	const COUNT = 'COUNT(log_client_rule.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT log_client_rule.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(LogClientRulePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(LogClientRulePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = LogClientRulePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = LogClientRulePeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return LogClientRulePeer::populateObjects(LogClientRulePeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			LogClientRulePeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = LogClientRulePeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return LogClientRulePeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(LogClientRulePeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(LogClientRulePeer::ID);
			$selectCriteria->add(LogClientRulePeer::ID, $criteria->remove(LogClientRulePeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(LogClientRulePeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(LogClientRulePeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof LogClientRule) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(LogClientRulePeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(LogClientRule $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(LogClientRulePeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(LogClientRulePeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(LogClientRulePeer::DATABASE_NAME, LogClientRulePeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = LogClientRulePeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(LogClientRulePeer::DATABASE_NAME);

		$criteria->add(LogClientRulePeer::ID, $pk);


		$v = LogClientRulePeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(LogClientRulePeer::ID, $pks, Criteria::IN);
			$objs = LogClientRulePeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseLogClientRulePeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/LogClientRuleMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.LogClientRuleMapBuilder');
}
