<?php


abstract class BaseArticleOriginalDataArchive extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $original_article_id;


	
	protected $original_source;


	
	protected $issue_number;


	
	protected $page_number;


	
	protected $reference;


	
	protected $extras;


	
	protected $hijri_date;


	
	protected $original_cat_id;


	
	protected $revision_num;

	
	protected $collArticleArchives;

	
	protected $lastArticleArchiveCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getOriginalArticleId()
	{

		return $this->original_article_id;
	}

	
	public function getOriginalSource()
	{

		return $this->original_source;
	}

	
	public function getIssueNumber()
	{

		return $this->issue_number;
	}

	
	public function getPageNumber()
	{

		return $this->page_number;
	}

	
	public function getReference()
	{

		return $this->reference;
	}

	
	public function getExtras()
	{

		return $this->extras;
	}

	
	public function getHijriDate()
	{

		return $this->hijri_date;
	}

	
	public function getOriginalCatId()
	{

		return $this->original_cat_id;
	}

	
	public function getRevisionNum()
	{

		return $this->revision_num;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ArticleOriginalDataArchivePeer::ID;
		}

	} 
	
	public function setOriginalArticleId($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->original_article_id !== $v) {
			$this->original_article_id = $v;
			$this->modifiedColumns[] = ArticleOriginalDataArchivePeer::ORIGINAL_ARTICLE_ID;
		}

	} 
	
	public function setOriginalSource($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->original_source !== $v) {
			$this->original_source = $v;
			$this->modifiedColumns[] = ArticleOriginalDataArchivePeer::ORIGINAL_SOURCE;
		}

	} 
	
	public function setIssueNumber($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->issue_number !== $v) {
			$this->issue_number = $v;
			$this->modifiedColumns[] = ArticleOriginalDataArchivePeer::ISSUE_NUMBER;
		}

	} 
	
	public function setPageNumber($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->page_number !== $v) {
			$this->page_number = $v;
			$this->modifiedColumns[] = ArticleOriginalDataArchivePeer::PAGE_NUMBER;
		}

	} 
	
	public function setReference($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->reference !== $v) {
			$this->reference = $v;
			$this->modifiedColumns[] = ArticleOriginalDataArchivePeer::REFERENCE;
		}

	} 
	
	public function setExtras($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->extras !== $v) {
			$this->extras = $v;
			$this->modifiedColumns[] = ArticleOriginalDataArchivePeer::EXTRAS;
		}

	} 
	
	public function setHijriDate($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->hijri_date !== $v) {
			$this->hijri_date = $v;
			$this->modifiedColumns[] = ArticleOriginalDataArchivePeer::HIJRI_DATE;
		}

	} 
	
	public function setOriginalCatId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->original_cat_id !== $v) {
			$this->original_cat_id = $v;
			$this->modifiedColumns[] = ArticleOriginalDataArchivePeer::ORIGINAL_CAT_ID;
		}

	} 
	
	public function setRevisionNum($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->revision_num !== $v) {
			$this->revision_num = $v;
			$this->modifiedColumns[] = ArticleOriginalDataArchivePeer::REVISION_NUM;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->original_article_id = $rs->getString($startcol + 1);

			$this->original_source = $rs->getString($startcol + 2);

			$this->issue_number = $rs->getInt($startcol + 3);

			$this->page_number = $rs->getInt($startcol + 4);

			$this->reference = $rs->getString($startcol + 5);

			$this->extras = $rs->getString($startcol + 6);

			$this->hijri_date = $rs->getString($startcol + 7);

			$this->original_cat_id = $rs->getInt($startcol + 8);

			$this->revision_num = $rs->getInt($startcol + 9);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 10; 
		} catch (Exception $e) {
			throw new PropelException("Error populating ArticleOriginalDataArchive object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ArticleOriginalDataArchivePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ArticleOriginalDataArchivePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ArticleOriginalDataArchivePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ArticleOriginalDataArchivePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ArticleOriginalDataArchivePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collArticleArchives !== null) {
				foreach($this->collArticleArchives as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = ArticleOriginalDataArchivePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collArticleArchives !== null) {
					foreach($this->collArticleArchives as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ArticleOriginalDataArchivePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getOriginalArticleId();
				break;
			case 2:
				return $this->getOriginalSource();
				break;
			case 3:
				return $this->getIssueNumber();
				break;
			case 4:
				return $this->getPageNumber();
				break;
			case 5:
				return $this->getReference();
				break;
			case 6:
				return $this->getExtras();
				break;
			case 7:
				return $this->getHijriDate();
				break;
			case 8:
				return $this->getOriginalCatId();
				break;
			case 9:
				return $this->getRevisionNum();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ArticleOriginalDataArchivePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getOriginalArticleId(),
			$keys[2] => $this->getOriginalSource(),
			$keys[3] => $this->getIssueNumber(),
			$keys[4] => $this->getPageNumber(),
			$keys[5] => $this->getReference(),
			$keys[6] => $this->getExtras(),
			$keys[7] => $this->getHijriDate(),
			$keys[8] => $this->getOriginalCatId(),
			$keys[9] => $this->getRevisionNum(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ArticleOriginalDataArchivePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setOriginalArticleId($value);
				break;
			case 2:
				$this->setOriginalSource($value);
				break;
			case 3:
				$this->setIssueNumber($value);
				break;
			case 4:
				$this->setPageNumber($value);
				break;
			case 5:
				$this->setReference($value);
				break;
			case 6:
				$this->setExtras($value);
				break;
			case 7:
				$this->setHijriDate($value);
				break;
			case 8:
				$this->setOriginalCatId($value);
				break;
			case 9:
				$this->setRevisionNum($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ArticleOriginalDataArchivePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setOriginalArticleId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setOriginalSource($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setIssueNumber($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setPageNumber($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setReference($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setExtras($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setHijriDate($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setOriginalCatId($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setRevisionNum($arr[$keys[9]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ArticleOriginalDataArchivePeer::DATABASE_NAME);

		if ($this->isColumnModified(ArticleOriginalDataArchivePeer::ID)) $criteria->add(ArticleOriginalDataArchivePeer::ID, $this->id);
		if ($this->isColumnModified(ArticleOriginalDataArchivePeer::ORIGINAL_ARTICLE_ID)) $criteria->add(ArticleOriginalDataArchivePeer::ORIGINAL_ARTICLE_ID, $this->original_article_id);
		if ($this->isColumnModified(ArticleOriginalDataArchivePeer::ORIGINAL_SOURCE)) $criteria->add(ArticleOriginalDataArchivePeer::ORIGINAL_SOURCE, $this->original_source);
		if ($this->isColumnModified(ArticleOriginalDataArchivePeer::ISSUE_NUMBER)) $criteria->add(ArticleOriginalDataArchivePeer::ISSUE_NUMBER, $this->issue_number);
		if ($this->isColumnModified(ArticleOriginalDataArchivePeer::PAGE_NUMBER)) $criteria->add(ArticleOriginalDataArchivePeer::PAGE_NUMBER, $this->page_number);
		if ($this->isColumnModified(ArticleOriginalDataArchivePeer::REFERENCE)) $criteria->add(ArticleOriginalDataArchivePeer::REFERENCE, $this->reference);
		if ($this->isColumnModified(ArticleOriginalDataArchivePeer::EXTRAS)) $criteria->add(ArticleOriginalDataArchivePeer::EXTRAS, $this->extras);
		if ($this->isColumnModified(ArticleOriginalDataArchivePeer::HIJRI_DATE)) $criteria->add(ArticleOriginalDataArchivePeer::HIJRI_DATE, $this->hijri_date);
		if ($this->isColumnModified(ArticleOriginalDataArchivePeer::ORIGINAL_CAT_ID)) $criteria->add(ArticleOriginalDataArchivePeer::ORIGINAL_CAT_ID, $this->original_cat_id);
		if ($this->isColumnModified(ArticleOriginalDataArchivePeer::REVISION_NUM)) $criteria->add(ArticleOriginalDataArchivePeer::REVISION_NUM, $this->revision_num);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ArticleOriginalDataArchivePeer::DATABASE_NAME);

		$criteria->add(ArticleOriginalDataArchivePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setOriginalArticleId($this->original_article_id);

		$copyObj->setOriginalSource($this->original_source);

		$copyObj->setIssueNumber($this->issue_number);

		$copyObj->setPageNumber($this->page_number);

		$copyObj->setReference($this->reference);

		$copyObj->setExtras($this->extras);

		$copyObj->setHijriDate($this->hijri_date);

		$copyObj->setOriginalCatId($this->original_cat_id);

		$copyObj->setRevisionNum($this->revision_num);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getArticleArchives() as $relObj) {
				$copyObj->addArticleArchive($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ArticleOriginalDataArchivePeer();
		}
		return self::$peer;
	}

	
	public function initArticleArchives()
	{
		if ($this->collArticleArchives === null) {
			$this->collArticleArchives = array();
		}
	}

	
	public function getArticleArchives($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticleArchivePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticleArchives === null) {
			if ($this->isNew()) {
			   $this->collArticleArchives = array();
			} else {

				$criteria->add(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, $this->getId());

				ArticleArchivePeer::addSelectColumns($criteria);
				$this->collArticleArchives = ArticleArchivePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, $this->getId());

				ArticleArchivePeer::addSelectColumns($criteria);
				if (!isset($this->lastArticleArchiveCriteria) || !$this->lastArticleArchiveCriteria->equals($criteria)) {
					$this->collArticleArchives = ArticleArchivePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastArticleArchiveCriteria = $criteria;
		return $this->collArticleArchives;
	}

	
	public function countArticleArchives($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseArticleArchivePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, $this->getId());

		return ArticleArchivePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addArticleArchive(ArticleArchive $l)
	{
		$this->collArticleArchives[] = $l;
		$l->setArticleOriginalDataArchive($this);
	}


	
	public function getArticleArchivesJoinLanguage($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticleArchivePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticleArchives === null) {
			if ($this->isNew()) {
				$this->collArticleArchives = array();
			} else {

				$criteria->add(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, $this->getId());

				$this->collArticleArchives = ArticleArchivePeer::doSelectJoinLanguage($criteria, $con);
			}
		} else {
									
			$criteria->add(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, $this->getId());

			if (!isset($this->lastArticleArchiveCriteria) || !$this->lastArticleArchiveCriteria->equals($criteria)) {
				$this->collArticleArchives = ArticleArchivePeer::doSelectJoinLanguage($criteria, $con);
			}
		}
		$this->lastArticleArchiveCriteria = $criteria;

		return $this->collArticleArchives;
	}


	
	public function getArticleArchivesJoinIptc($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticleArchivePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticleArchives === null) {
			if ($this->isNew()) {
				$this->collArticleArchives = array();
			} else {

				$criteria->add(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, $this->getId());

				$this->collArticleArchives = ArticleArchivePeer::doSelectJoinIptc($criteria, $con);
			}
		} else {
									
			$criteria->add(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, $this->getId());

			if (!isset($this->lastArticleArchiveCriteria) || !$this->lastArticleArchiveCriteria->equals($criteria)) {
				$this->collArticleArchives = ArticleArchivePeer::doSelectJoinIptc($criteria, $con);
			}
		}
		$this->lastArticleArchiveCriteria = $criteria;

		return $this->collArticleArchives;
	}


	
	public function getArticleArchivesJoinTitle($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticleArchivePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticleArchives === null) {
			if ($this->isNew()) {
				$this->collArticleArchives = array();
			} else {

				$criteria->add(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, $this->getId());

				$this->collArticleArchives = ArticleArchivePeer::doSelectJoinTitle($criteria, $con);
			}
		} else {
									
			$criteria->add(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, $this->getId());

			if (!isset($this->lastArticleArchiveCriteria) || !$this->lastArticleArchiveCriteria->equals($criteria)) {
				$this->collArticleArchives = ArticleArchivePeer::doSelectJoinTitle($criteria, $con);
			}
		}
		$this->lastArticleArchiveCriteria = $criteria;

		return $this->collArticleArchives;
	}

} 