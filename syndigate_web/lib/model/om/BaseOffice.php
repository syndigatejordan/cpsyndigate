<?php


abstract class BaseOffice extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $phone_1;


	
	protected $phone_2;


	
	protected $fax;


	
	protected $pobox;


	
	protected $zipcode;


	
	protected $street_address;


	
	protected $city_name;


	
	protected $country_id;

	
	protected $aCountry;

	
	protected $collPublisherOfficeLinks;

	
	protected $lastPublisherOfficeLinkCriteria = null;

	
	protected $collTitleOfficeLinks;

	
	protected $lastTitleOfficeLinkCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getPhone1()
	{

		return $this->phone_1;
	}

	
	public function getPhone2()
	{

		return $this->phone_2;
	}

	
	public function getFax()
	{

		return $this->fax;
	}

	
	public function getPobox()
	{

		return $this->pobox;
	}

	
	public function getZipcode()
	{

		return $this->zipcode;
	}

	
	public function getStreetAddress()
	{

		return $this->street_address;
	}

	
	public function getCityName()
	{

		return $this->city_name;
	}

	
	public function getCountryId()
	{

		return $this->country_id;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = OfficePeer::ID;
		}

	} 
	
	public function setPhone1($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->phone_1 !== $v) {
			$this->phone_1 = $v;
			$this->modifiedColumns[] = OfficePeer::PHONE_1;
		}

	} 
	
	public function setPhone2($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->phone_2 !== $v) {
			$this->phone_2 = $v;
			$this->modifiedColumns[] = OfficePeer::PHONE_2;
		}

	} 
	
	public function setFax($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->fax !== $v) {
			$this->fax = $v;
			$this->modifiedColumns[] = OfficePeer::FAX;
		}

	} 
	
	public function setPobox($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->pobox !== $v) {
			$this->pobox = $v;
			$this->modifiedColumns[] = OfficePeer::POBOX;
		}

	} 
	
	public function setZipcode($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->zipcode !== $v) {
			$this->zipcode = $v;
			$this->modifiedColumns[] = OfficePeer::ZIPCODE;
		}

	} 
	
	public function setStreetAddress($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->street_address !== $v) {
			$this->street_address = $v;
			$this->modifiedColumns[] = OfficePeer::STREET_ADDRESS;
		}

	} 
	
	public function setCityName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->city_name !== $v) {
			$this->city_name = $v;
			$this->modifiedColumns[] = OfficePeer::CITY_NAME;
		}

	} 
	
	public function setCountryId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->country_id !== $v) {
			$this->country_id = $v;
			$this->modifiedColumns[] = OfficePeer::COUNTRY_ID;
		}

		if ($this->aCountry !== null && $this->aCountry->getId() !== $v) {
			$this->aCountry = null;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->phone_1 = $rs->getString($startcol + 1);

			$this->phone_2 = $rs->getString($startcol + 2);

			$this->fax = $rs->getString($startcol + 3);

			$this->pobox = $rs->getString($startcol + 4);

			$this->zipcode = $rs->getInt($startcol + 5);

			$this->street_address = $rs->getString($startcol + 6);

			$this->city_name = $rs->getString($startcol + 7);

			$this->country_id = $rs->getInt($startcol + 8);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 9; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Office object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(OfficePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			OfficePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(OfficePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aCountry !== null) {
				if ($this->aCountry->isModified()) {
					$affectedRows += $this->aCountry->save($con);
				}
				$this->setCountry($this->aCountry);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = OfficePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += OfficePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collPublisherOfficeLinks !== null) {
				foreach($this->collPublisherOfficeLinks as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collTitleOfficeLinks !== null) {
				foreach($this->collTitleOfficeLinks as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aCountry !== null) {
				if (!$this->aCountry->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCountry->getValidationFailures());
				}
			}


			if (($retval = OfficePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collPublisherOfficeLinks !== null) {
					foreach($this->collPublisherOfficeLinks as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collTitleOfficeLinks !== null) {
					foreach($this->collTitleOfficeLinks as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = OfficePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getPhone1();
				break;
			case 2:
				return $this->getPhone2();
				break;
			case 3:
				return $this->getFax();
				break;
			case 4:
				return $this->getPobox();
				break;
			case 5:
				return $this->getZipcode();
				break;
			case 6:
				return $this->getStreetAddress();
				break;
			case 7:
				return $this->getCityName();
				break;
			case 8:
				return $this->getCountryId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = OfficePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getPhone1(),
			$keys[2] => $this->getPhone2(),
			$keys[3] => $this->getFax(),
			$keys[4] => $this->getPobox(),
			$keys[5] => $this->getZipcode(),
			$keys[6] => $this->getStreetAddress(),
			$keys[7] => $this->getCityName(),
			$keys[8] => $this->getCountryId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = OfficePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setPhone1($value);
				break;
			case 2:
				$this->setPhone2($value);
				break;
			case 3:
				$this->setFax($value);
				break;
			case 4:
				$this->setPobox($value);
				break;
			case 5:
				$this->setZipcode($value);
				break;
			case 6:
				$this->setStreetAddress($value);
				break;
			case 7:
				$this->setCityName($value);
				break;
			case 8:
				$this->setCountryId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = OfficePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setPhone1($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setPhone2($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setFax($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setPobox($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setZipcode($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setStreetAddress($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setCityName($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setCountryId($arr[$keys[8]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(OfficePeer::DATABASE_NAME);

		if ($this->isColumnModified(OfficePeer::ID)) $criteria->add(OfficePeer::ID, $this->id);
		if ($this->isColumnModified(OfficePeer::PHONE_1)) $criteria->add(OfficePeer::PHONE_1, $this->phone_1);
		if ($this->isColumnModified(OfficePeer::PHONE_2)) $criteria->add(OfficePeer::PHONE_2, $this->phone_2);
		if ($this->isColumnModified(OfficePeer::FAX)) $criteria->add(OfficePeer::FAX, $this->fax);
		if ($this->isColumnModified(OfficePeer::POBOX)) $criteria->add(OfficePeer::POBOX, $this->pobox);
		if ($this->isColumnModified(OfficePeer::ZIPCODE)) $criteria->add(OfficePeer::ZIPCODE, $this->zipcode);
		if ($this->isColumnModified(OfficePeer::STREET_ADDRESS)) $criteria->add(OfficePeer::STREET_ADDRESS, $this->street_address);
		if ($this->isColumnModified(OfficePeer::CITY_NAME)) $criteria->add(OfficePeer::CITY_NAME, $this->city_name);
		if ($this->isColumnModified(OfficePeer::COUNTRY_ID)) $criteria->add(OfficePeer::COUNTRY_ID, $this->country_id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(OfficePeer::DATABASE_NAME);

		$criteria->add(OfficePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setPhone1($this->phone_1);

		$copyObj->setPhone2($this->phone_2);

		$copyObj->setFax($this->fax);

		$copyObj->setPobox($this->pobox);

		$copyObj->setZipcode($this->zipcode);

		$copyObj->setStreetAddress($this->street_address);

		$copyObj->setCityName($this->city_name);

		$copyObj->setCountryId($this->country_id);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getPublisherOfficeLinks() as $relObj) {
				$copyObj->addPublisherOfficeLink($relObj->copy($deepCopy));
			}

			foreach($this->getTitleOfficeLinks() as $relObj) {
				$copyObj->addTitleOfficeLink($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new OfficePeer();
		}
		return self::$peer;
	}

	
	public function setCountry($v)
	{


		if ($v === null) {
			$this->setCountryId(NULL);
		} else {
			$this->setCountryId($v->getId());
		}


		$this->aCountry = $v;
	}


	
	public function getCountry($con = null)
	{
		if ($this->aCountry === null && ($this->country_id !== null)) {
						include_once 'lib/model/om/BaseCountryPeer.php';

			$this->aCountry = CountryPeer::retrieveByPK($this->country_id, $con);

			
		}
		return $this->aCountry;
	}

	
	public function initPublisherOfficeLinks()
	{
		if ($this->collPublisherOfficeLinks === null) {
			$this->collPublisherOfficeLinks = array();
		}
	}

	
	public function getPublisherOfficeLinks($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BasePublisherOfficeLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collPublisherOfficeLinks === null) {
			if ($this->isNew()) {
			   $this->collPublisherOfficeLinks = array();
			} else {

				$criteria->add(PublisherOfficeLinkPeer::OFFICE_ID, $this->getId());

				PublisherOfficeLinkPeer::addSelectColumns($criteria);
				$this->collPublisherOfficeLinks = PublisherOfficeLinkPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(PublisherOfficeLinkPeer::OFFICE_ID, $this->getId());

				PublisherOfficeLinkPeer::addSelectColumns($criteria);
				if (!isset($this->lastPublisherOfficeLinkCriteria) || !$this->lastPublisherOfficeLinkCriteria->equals($criteria)) {
					$this->collPublisherOfficeLinks = PublisherOfficeLinkPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastPublisherOfficeLinkCriteria = $criteria;
		return $this->collPublisherOfficeLinks;
	}

	
	public function countPublisherOfficeLinks($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BasePublisherOfficeLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(PublisherOfficeLinkPeer::OFFICE_ID, $this->getId());

		return PublisherOfficeLinkPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addPublisherOfficeLink(PublisherOfficeLink $l)
	{
		$this->collPublisherOfficeLinks[] = $l;
		$l->setOffice($this);
	}


	
	public function getPublisherOfficeLinksJoinPublisher($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BasePublisherOfficeLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collPublisherOfficeLinks === null) {
			if ($this->isNew()) {
				$this->collPublisherOfficeLinks = array();
			} else {

				$criteria->add(PublisherOfficeLinkPeer::OFFICE_ID, $this->getId());

				$this->collPublisherOfficeLinks = PublisherOfficeLinkPeer::doSelectJoinPublisher($criteria, $con);
			}
		} else {
									
			$criteria->add(PublisherOfficeLinkPeer::OFFICE_ID, $this->getId());

			if (!isset($this->lastPublisherOfficeLinkCriteria) || !$this->lastPublisherOfficeLinkCriteria->equals($criteria)) {
				$this->collPublisherOfficeLinks = PublisherOfficeLinkPeer::doSelectJoinPublisher($criteria, $con);
			}
		}
		$this->lastPublisherOfficeLinkCriteria = $criteria;

		return $this->collPublisherOfficeLinks;
	}

	
	public function initTitleOfficeLinks()
	{
		if ($this->collTitleOfficeLinks === null) {
			$this->collTitleOfficeLinks = array();
		}
	}

	
	public function getTitleOfficeLinks($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitleOfficeLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitleOfficeLinks === null) {
			if ($this->isNew()) {
			   $this->collTitleOfficeLinks = array();
			} else {

				$criteria->add(TitleOfficeLinkPeer::OFFICE_ID, $this->getId());

				TitleOfficeLinkPeer::addSelectColumns($criteria);
				$this->collTitleOfficeLinks = TitleOfficeLinkPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(TitleOfficeLinkPeer::OFFICE_ID, $this->getId());

				TitleOfficeLinkPeer::addSelectColumns($criteria);
				if (!isset($this->lastTitleOfficeLinkCriteria) || !$this->lastTitleOfficeLinkCriteria->equals($criteria)) {
					$this->collTitleOfficeLinks = TitleOfficeLinkPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastTitleOfficeLinkCriteria = $criteria;
		return $this->collTitleOfficeLinks;
	}

	
	public function countTitleOfficeLinks($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseTitleOfficeLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(TitleOfficeLinkPeer::OFFICE_ID, $this->getId());

		return TitleOfficeLinkPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addTitleOfficeLink(TitleOfficeLink $l)
	{
		$this->collTitleOfficeLinks[] = $l;
		$l->setOffice($this);
	}


	
	public function getTitleOfficeLinksJoinTitle($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitleOfficeLinkPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitleOfficeLinks === null) {
			if ($this->isNew()) {
				$this->collTitleOfficeLinks = array();
			} else {

				$criteria->add(TitleOfficeLinkPeer::OFFICE_ID, $this->getId());

				$this->collTitleOfficeLinks = TitleOfficeLinkPeer::doSelectJoinTitle($criteria, $con);
			}
		} else {
									
			$criteria->add(TitleOfficeLinkPeer::OFFICE_ID, $this->getId());

			if (!isset($this->lastTitleOfficeLinkCriteria) || !$this->lastTitleOfficeLinkCriteria->equals($criteria)) {
				$this->collTitleOfficeLinks = TitleOfficeLinkPeer::doSelectJoinTitle($criteria, $con);
			}
		}
		$this->lastTitleOfficeLinkCriteria = $criteria;

		return $this->collTitleOfficeLinks;
	}

} 