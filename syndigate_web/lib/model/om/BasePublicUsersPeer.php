<?php


abstract class BasePublicUsersPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'public_users';

	
	const CLASS_DEFAULT = 'lib.model.PublicUsers';

	
	const NUM_COLUMNS = 10;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const USER_ID = 'public_users.USER_ID';

	
	const USER_NAME = 'public_users.USER_NAME';

	
	const PASSWORD = 'public_users.PASSWORD';

	
	const CREATION_TIME_STAMP = 'public_users.CREATION_TIME_STAMP';

	
	const PERIOD = 'public_users.PERIOD';

	
	const COMPANY_NAME = 'public_users.COMPANY_NAME';

	
	const EMAIL = 'public_users.EMAIL';

	
	const USER_STATUS = 'public_users.USER_STATUS';

	
	const FIRST_LOGIN = 'public_users.FIRST_LOGIN';

	
	const EXPIRY_DATE = 'public_users.EXPIRY_DATE';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UserId', 'UserName', 'Password', 'CreationTimeStamp', 'Period', 'CompanyName', 'Email', 'UserStatus', 'FirstLogin', 'ExpiryDate', ),
		BasePeer::TYPE_COLNAME => array (PublicUsersPeer::USER_ID, PublicUsersPeer::USER_NAME, PublicUsersPeer::PASSWORD, PublicUsersPeer::CREATION_TIME_STAMP, PublicUsersPeer::PERIOD, PublicUsersPeer::COMPANY_NAME, PublicUsersPeer::EMAIL, PublicUsersPeer::USER_STATUS, PublicUsersPeer::FIRST_LOGIN, PublicUsersPeer::EXPIRY_DATE, ),
		BasePeer::TYPE_FIELDNAME => array ('user_id', 'user_name', 'password', 'creation_time_stamp', 'period', 'company_name', 'email', 'user_status', 'first_login', 'expiry_date', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UserId' => 0, 'UserName' => 1, 'Password' => 2, 'CreationTimeStamp' => 3, 'Period' => 4, 'CompanyName' => 5, 'Email' => 6, 'UserStatus' => 7, 'FirstLogin' => 8, 'ExpiryDate' => 9, ),
		BasePeer::TYPE_COLNAME => array (PublicUsersPeer::USER_ID => 0, PublicUsersPeer::USER_NAME => 1, PublicUsersPeer::PASSWORD => 2, PublicUsersPeer::CREATION_TIME_STAMP => 3, PublicUsersPeer::PERIOD => 4, PublicUsersPeer::COMPANY_NAME => 5, PublicUsersPeer::EMAIL => 6, PublicUsersPeer::USER_STATUS => 7, PublicUsersPeer::FIRST_LOGIN => 8, PublicUsersPeer::EXPIRY_DATE => 9, ),
		BasePeer::TYPE_FIELDNAME => array ('user_id' => 0, 'user_name' => 1, 'password' => 2, 'creation_time_stamp' => 3, 'period' => 4, 'company_name' => 5, 'email' => 6, 'user_status' => 7, 'first_login' => 8, 'expiry_date' => 9, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/PublicUsersMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.PublicUsersMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = PublicUsersPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(PublicUsersPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(PublicUsersPeer::USER_ID);

		$criteria->addSelectColumn(PublicUsersPeer::USER_NAME);

		$criteria->addSelectColumn(PublicUsersPeer::PASSWORD);

		$criteria->addSelectColumn(PublicUsersPeer::CREATION_TIME_STAMP);

		$criteria->addSelectColumn(PublicUsersPeer::PERIOD);

		$criteria->addSelectColumn(PublicUsersPeer::COMPANY_NAME);

		$criteria->addSelectColumn(PublicUsersPeer::EMAIL);

		$criteria->addSelectColumn(PublicUsersPeer::USER_STATUS);

		$criteria->addSelectColumn(PublicUsersPeer::FIRST_LOGIN);

		$criteria->addSelectColumn(PublicUsersPeer::EXPIRY_DATE);

	}

	const COUNT = 'COUNT(public_users.USER_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT public_users.USER_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PublicUsersPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PublicUsersPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = PublicUsersPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = PublicUsersPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return PublicUsersPeer::populateObjects(PublicUsersPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			PublicUsersPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = PublicUsersPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return PublicUsersPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(PublicUsersPeer::USER_ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(PublicUsersPeer::USER_ID);
			$selectCriteria->add(PublicUsersPeer::USER_ID, $criteria->remove(PublicUsersPeer::USER_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(PublicUsersPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(PublicUsersPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof PublicUsers) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(PublicUsersPeer::USER_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(PublicUsers $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(PublicUsersPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(PublicUsersPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(PublicUsersPeer::DATABASE_NAME, PublicUsersPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = PublicUsersPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(PublicUsersPeer::DATABASE_NAME);

		$criteria->add(PublicUsersPeer::USER_ID, $pk);


		$v = PublicUsersPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(PublicUsersPeer::USER_ID, $pks, Criteria::IN);
			$objs = PublicUsersPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BasePublicUsersPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/PublicUsersMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.PublicUsersMapBuilder');
}
