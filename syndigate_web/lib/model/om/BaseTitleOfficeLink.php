<?php


abstract class BaseTitleOfficeLink extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $office_id;


	
	protected $title_id;

	
	protected $aTitle;

	
	protected $aOffice;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getOfficeId()
	{

		return $this->office_id;
	}

	
	public function getTitleId()
	{

		return $this->title_id;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = TitleOfficeLinkPeer::ID;
		}

	} 
	
	public function setOfficeId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->office_id !== $v) {
			$this->office_id = $v;
			$this->modifiedColumns[] = TitleOfficeLinkPeer::OFFICE_ID;
		}

		if ($this->aOffice !== null && $this->aOffice->getId() !== $v) {
			$this->aOffice = null;
		}

	} 
	
	public function setTitleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_id !== $v) {
			$this->title_id = $v;
			$this->modifiedColumns[] = TitleOfficeLinkPeer::TITLE_ID;
		}

		if ($this->aTitle !== null && $this->aTitle->getId() !== $v) {
			$this->aTitle = null;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->office_id = $rs->getInt($startcol + 1);

			$this->title_id = $rs->getInt($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 3; 
		} catch (Exception $e) {
			throw new PropelException("Error populating TitleOfficeLink object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TitleOfficeLinkPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			TitleOfficeLinkPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TitleOfficeLinkPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aTitle !== null) {
				if ($this->aTitle->isModified()) {
					$affectedRows += $this->aTitle->save($con);
				}
				$this->setTitle($this->aTitle);
			}

			if ($this->aOffice !== null) {
				if ($this->aOffice->isModified()) {
					$affectedRows += $this->aOffice->save($con);
				}
				$this->setOffice($this->aOffice);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = TitleOfficeLinkPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += TitleOfficeLinkPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aTitle !== null) {
				if (!$this->aTitle->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTitle->getValidationFailures());
				}
			}

			if ($this->aOffice !== null) {
				if (!$this->aOffice->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aOffice->getValidationFailures());
				}
			}


			if (($retval = TitleOfficeLinkPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TitleOfficeLinkPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getOfficeId();
				break;
			case 2:
				return $this->getTitleId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TitleOfficeLinkPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getOfficeId(),
			$keys[2] => $this->getTitleId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TitleOfficeLinkPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setOfficeId($value);
				break;
			case 2:
				$this->setTitleId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TitleOfficeLinkPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setOfficeId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setTitleId($arr[$keys[2]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(TitleOfficeLinkPeer::DATABASE_NAME);

		if ($this->isColumnModified(TitleOfficeLinkPeer::ID)) $criteria->add(TitleOfficeLinkPeer::ID, $this->id);
		if ($this->isColumnModified(TitleOfficeLinkPeer::OFFICE_ID)) $criteria->add(TitleOfficeLinkPeer::OFFICE_ID, $this->office_id);
		if ($this->isColumnModified(TitleOfficeLinkPeer::TITLE_ID)) $criteria->add(TitleOfficeLinkPeer::TITLE_ID, $this->title_id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(TitleOfficeLinkPeer::DATABASE_NAME);

		$criteria->add(TitleOfficeLinkPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setOfficeId($this->office_id);

		$copyObj->setTitleId($this->title_id);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new TitleOfficeLinkPeer();
		}
		return self::$peer;
	}

	
	public function setTitle($v)
	{


		if ($v === null) {
			$this->setTitleId(NULL);
		} else {
			$this->setTitleId($v->getId());
		}


		$this->aTitle = $v;
	}


	
	public function getTitle($con = null)
	{
		if ($this->aTitle === null && ($this->title_id !== null)) {
						include_once 'lib/model/om/BaseTitlePeer.php';

			$this->aTitle = TitlePeer::retrieveByPK($this->title_id, $con);

			
		}
		return $this->aTitle;
	}

	
	public function setOffice($v)
	{


		if ($v === null) {
			$this->setOfficeId(NULL);
		} else {
			$this->setOfficeId($v->getId());
		}


		$this->aOffice = $v;
	}


	
	public function getOffice($con = null)
	{
		if ($this->aOffice === null && ($this->office_id !== null)) {
						include_once 'lib/model/om/BaseOfficePeer.php';

			$this->aOffice = OfficePeer::retrieveByPK($this->office_id, $con);

			
		}
		return $this->aOffice;
	}

} 