<?php


abstract class BaseArticleTmpPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'article_tmp';

	
	const CLASS_DEFAULT = 'lib.model.ArticleTmp';

	
	const NUM_COLUMNS = 15;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'article_tmp.ID';

	
	const IPTC_ID = 'article_tmp.IPTC_ID';

	
	const TITLE_ID = 'article_tmp.TITLE_ID';

	
	const ARTICLE_ORIGINAL_DATA_ID = 'article_tmp.ARTICLE_ORIGINAL_DATA_ID';

	
	const LANGUAGE_ID = 'article_tmp.LANGUAGE_ID';

	
	const HEADLINE = 'article_tmp.HEADLINE';

	
	const SUMMARY = 'article_tmp.SUMMARY';

	
	const BODY = 'article_tmp.BODY';

	
	const AUTHOR = 'article_tmp.AUTHOR';

	
	const DATE = 'article_tmp.DATE';

	
	const PARSED_AT = 'article_tmp.PARSED_AT';

	
	const UPDATED_AT = 'article_tmp.UPDATED_AT';

	
	const HAS_TIME = 'article_tmp.HAS_TIME';

	
	const IS_CALIAS_CALLED = 'article_tmp.IS_CALIAS_CALLED';

	
	const SUB_FEED = 'article_tmp.SUB_FEED';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'IptcId', 'TitleId', 'ArticleOriginalDataId', 'LanguageId', 'Headline', 'Summary', 'Body', 'Author', 'Date', 'ParsedAt', 'UpdatedAt', 'HasTime', 'IsCaliasCalled', 'SubFeed', ),
		BasePeer::TYPE_COLNAME => array (ArticleTmpPeer::ID, ArticleTmpPeer::IPTC_ID, ArticleTmpPeer::TITLE_ID, ArticleTmpPeer::ARTICLE_ORIGINAL_DATA_ID, ArticleTmpPeer::LANGUAGE_ID, ArticleTmpPeer::HEADLINE, ArticleTmpPeer::SUMMARY, ArticleTmpPeer::BODY, ArticleTmpPeer::AUTHOR, ArticleTmpPeer::DATE, ArticleTmpPeer::PARSED_AT, ArticleTmpPeer::UPDATED_AT, ArticleTmpPeer::HAS_TIME, ArticleTmpPeer::IS_CALIAS_CALLED, ArticleTmpPeer::SUB_FEED, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'iptc_id', 'title_id', 'article_original_data_id', 'language_id', 'headline', 'summary', 'body', 'author', 'date', 'parsed_at', 'updated_at', 'has_time', 'is_Calias_called', 'sub_feed', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'IptcId' => 1, 'TitleId' => 2, 'ArticleOriginalDataId' => 3, 'LanguageId' => 4, 'Headline' => 5, 'Summary' => 6, 'Body' => 7, 'Author' => 8, 'Date' => 9, 'ParsedAt' => 10, 'UpdatedAt' => 11, 'HasTime' => 12, 'IsCaliasCalled' => 13, 'SubFeed' => 14, ),
		BasePeer::TYPE_COLNAME => array (ArticleTmpPeer::ID => 0, ArticleTmpPeer::IPTC_ID => 1, ArticleTmpPeer::TITLE_ID => 2, ArticleTmpPeer::ARTICLE_ORIGINAL_DATA_ID => 3, ArticleTmpPeer::LANGUAGE_ID => 4, ArticleTmpPeer::HEADLINE => 5, ArticleTmpPeer::SUMMARY => 6, ArticleTmpPeer::BODY => 7, ArticleTmpPeer::AUTHOR => 8, ArticleTmpPeer::DATE => 9, ArticleTmpPeer::PARSED_AT => 10, ArticleTmpPeer::UPDATED_AT => 11, ArticleTmpPeer::HAS_TIME => 12, ArticleTmpPeer::IS_CALIAS_CALLED => 13, ArticleTmpPeer::SUB_FEED => 14, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'iptc_id' => 1, 'title_id' => 2, 'article_original_data_id' => 3, 'language_id' => 4, 'headline' => 5, 'summary' => 6, 'body' => 7, 'author' => 8, 'date' => 9, 'parsed_at' => 10, 'updated_at' => 11, 'has_time' => 12, 'is_Calias_called' => 13, 'sub_feed' => 14, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/ArticleTmpMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.ArticleTmpMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ArticleTmpPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ArticleTmpPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ArticleTmpPeer::ID);

		$criteria->addSelectColumn(ArticleTmpPeer::IPTC_ID);

		$criteria->addSelectColumn(ArticleTmpPeer::TITLE_ID);

		$criteria->addSelectColumn(ArticleTmpPeer::ARTICLE_ORIGINAL_DATA_ID);

		$criteria->addSelectColumn(ArticleTmpPeer::LANGUAGE_ID);

		$criteria->addSelectColumn(ArticleTmpPeer::HEADLINE);

		$criteria->addSelectColumn(ArticleTmpPeer::SUMMARY);

		$criteria->addSelectColumn(ArticleTmpPeer::BODY);

		$criteria->addSelectColumn(ArticleTmpPeer::AUTHOR);

		$criteria->addSelectColumn(ArticleTmpPeer::DATE);

		$criteria->addSelectColumn(ArticleTmpPeer::PARSED_AT);

		$criteria->addSelectColumn(ArticleTmpPeer::UPDATED_AT);

		$criteria->addSelectColumn(ArticleTmpPeer::HAS_TIME);

		$criteria->addSelectColumn(ArticleTmpPeer::IS_CALIAS_CALLED);

		$criteria->addSelectColumn(ArticleTmpPeer::SUB_FEED);

	}

	const COUNT = 'COUNT(article_tmp.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT article_tmp.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ArticleTmpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ArticleTmpPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ArticleTmpPeer::populateObjects(ArticleTmpPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ArticleTmpPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ArticleTmpPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinLanguage(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticleTmpPeer::LANGUAGE_ID, LanguagePeer::ID);

		$rs = ArticleTmpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinArticleOriginalDataTmp(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticleTmpPeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataTmpPeer::ID);

		$rs = ArticleTmpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinIptc(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticleTmpPeer::IPTC_ID, IptcPeer::ID);

		$rs = ArticleTmpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinTitle(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticleTmpPeer::TITLE_ID, TitlePeer::ID);

		$rs = ArticleTmpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinLanguage(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticleTmpPeer::addSelectColumns($c);
		$startcol = (ArticleTmpPeer::NUM_COLUMNS - ArticleTmpPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		LanguagePeer::addSelectColumns($c);

		$c->addJoin(ArticleTmpPeer::LANGUAGE_ID, LanguagePeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticleTmpPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = LanguagePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getLanguage(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addArticleTmp($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initArticleTmps();
				$obj2->addArticleTmp($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinArticleOriginalDataTmp(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticleTmpPeer::addSelectColumns($c);
		$startcol = (ArticleTmpPeer::NUM_COLUMNS - ArticleTmpPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		ArticleOriginalDataTmpPeer::addSelectColumns($c);

		$c->addJoin(ArticleTmpPeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataTmpPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticleTmpPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = ArticleOriginalDataTmpPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getArticleOriginalDataTmp(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addArticleTmp($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initArticleTmps();
				$obj2->addArticleTmp($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinIptc(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticleTmpPeer::addSelectColumns($c);
		$startcol = (ArticleTmpPeer::NUM_COLUMNS - ArticleTmpPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		IptcPeer::addSelectColumns($c);

		$c->addJoin(ArticleTmpPeer::IPTC_ID, IptcPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticleTmpPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = IptcPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getIptc(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addArticleTmp($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initArticleTmps();
				$obj2->addArticleTmp($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinTitle(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticleTmpPeer::addSelectColumns($c);
		$startcol = (ArticleTmpPeer::NUM_COLUMNS - ArticleTmpPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		TitlePeer::addSelectColumns($c);

		$c->addJoin(ArticleTmpPeer::TITLE_ID, TitlePeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticleTmpPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = TitlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getTitle(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addArticleTmp($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initArticleTmps();
				$obj2->addArticleTmp($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticleTmpPeer::LANGUAGE_ID, LanguagePeer::ID);

		$criteria->addJoin(ArticleTmpPeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataTmpPeer::ID);

		$criteria->addJoin(ArticleTmpPeer::IPTC_ID, IptcPeer::ID);

		$criteria->addJoin(ArticleTmpPeer::TITLE_ID, TitlePeer::ID);

		$rs = ArticleTmpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticleTmpPeer::addSelectColumns($c);
		$startcol2 = (ArticleTmpPeer::NUM_COLUMNS - ArticleTmpPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		LanguagePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + LanguagePeer::NUM_COLUMNS;

		ArticleOriginalDataTmpPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + ArticleOriginalDataTmpPeer::NUM_COLUMNS;

		IptcPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + IptcPeer::NUM_COLUMNS;

		TitlePeer::addSelectColumns($c);
		$startcol6 = $startcol5 + TitlePeer::NUM_COLUMNS;

		$c->addJoin(ArticleTmpPeer::LANGUAGE_ID, LanguagePeer::ID);

		$c->addJoin(ArticleTmpPeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataTmpPeer::ID);

		$c->addJoin(ArticleTmpPeer::IPTC_ID, IptcPeer::ID);

		$c->addJoin(ArticleTmpPeer::TITLE_ID, TitlePeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticleTmpPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = LanguagePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getLanguage(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addArticleTmp($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initArticleTmps();
				$obj2->addArticleTmp($obj1);
			}


					
			$omClass = ArticleOriginalDataTmpPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3 = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getArticleOriginalDataTmp(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addArticleTmp($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj3->initArticleTmps();
				$obj3->addArticleTmp($obj1);
			}


					
			$omClass = IptcPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4 = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getIptc(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addArticleTmp($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj4->initArticleTmps();
				$obj4->addArticleTmp($obj1);
			}


					
			$omClass = TitlePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj5 = new $cls();
			$obj5->hydrate($rs, $startcol5);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj5 = $temp_obj1->getTitle(); 				if ($temp_obj5->getPrimaryKey() === $obj5->getPrimaryKey()) {
					$newObject = false;
					$temp_obj5->addArticleTmp($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj5->initArticleTmps();
				$obj5->addArticleTmp($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAllExceptLanguage(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticleTmpPeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataTmpPeer::ID);

		$criteria->addJoin(ArticleTmpPeer::IPTC_ID, IptcPeer::ID);

		$criteria->addJoin(ArticleTmpPeer::TITLE_ID, TitlePeer::ID);

		$rs = ArticleTmpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptArticleOriginalDataTmp(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticleTmpPeer::LANGUAGE_ID, LanguagePeer::ID);

		$criteria->addJoin(ArticleTmpPeer::IPTC_ID, IptcPeer::ID);

		$criteria->addJoin(ArticleTmpPeer::TITLE_ID, TitlePeer::ID);

		$rs = ArticleTmpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptIptc(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticleTmpPeer::LANGUAGE_ID, LanguagePeer::ID);

		$criteria->addJoin(ArticleTmpPeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataTmpPeer::ID);

		$criteria->addJoin(ArticleTmpPeer::TITLE_ID, TitlePeer::ID);

		$rs = ArticleTmpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptTitle(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticleTmpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticleTmpPeer::LANGUAGE_ID, LanguagePeer::ID);

		$criteria->addJoin(ArticleTmpPeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataTmpPeer::ID);

		$criteria->addJoin(ArticleTmpPeer::IPTC_ID, IptcPeer::ID);

		$rs = ArticleTmpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAllExceptLanguage(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticleTmpPeer::addSelectColumns($c);
		$startcol2 = (ArticleTmpPeer::NUM_COLUMNS - ArticleTmpPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		ArticleOriginalDataTmpPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + ArticleOriginalDataTmpPeer::NUM_COLUMNS;

		IptcPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + IptcPeer::NUM_COLUMNS;

		TitlePeer::addSelectColumns($c);
		$startcol5 = $startcol4 + TitlePeer::NUM_COLUMNS;

		$c->addJoin(ArticleTmpPeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataTmpPeer::ID);

		$c->addJoin(ArticleTmpPeer::IPTC_ID, IptcPeer::ID);

		$c->addJoin(ArticleTmpPeer::TITLE_ID, TitlePeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticleTmpPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = ArticleOriginalDataTmpPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getArticleOriginalDataTmp(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addArticleTmp($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initArticleTmps();
				$obj2->addArticleTmp($obj1);
			}

			$omClass = IptcPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getIptc(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addArticleTmp($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initArticleTmps();
				$obj3->addArticleTmp($obj1);
			}

			$omClass = TitlePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getTitle(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addArticleTmp($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initArticleTmps();
				$obj4->addArticleTmp($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptArticleOriginalDataTmp(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticleTmpPeer::addSelectColumns($c);
		$startcol2 = (ArticleTmpPeer::NUM_COLUMNS - ArticleTmpPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		LanguagePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + LanguagePeer::NUM_COLUMNS;

		IptcPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + IptcPeer::NUM_COLUMNS;

		TitlePeer::addSelectColumns($c);
		$startcol5 = $startcol4 + TitlePeer::NUM_COLUMNS;

		$c->addJoin(ArticleTmpPeer::LANGUAGE_ID, LanguagePeer::ID);

		$c->addJoin(ArticleTmpPeer::IPTC_ID, IptcPeer::ID);

		$c->addJoin(ArticleTmpPeer::TITLE_ID, TitlePeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticleTmpPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = LanguagePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getLanguage(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addArticleTmp($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initArticleTmps();
				$obj2->addArticleTmp($obj1);
			}

			$omClass = IptcPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getIptc(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addArticleTmp($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initArticleTmps();
				$obj3->addArticleTmp($obj1);
			}

			$omClass = TitlePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getTitle(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addArticleTmp($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initArticleTmps();
				$obj4->addArticleTmp($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptIptc(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticleTmpPeer::addSelectColumns($c);
		$startcol2 = (ArticleTmpPeer::NUM_COLUMNS - ArticleTmpPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		LanguagePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + LanguagePeer::NUM_COLUMNS;

		ArticleOriginalDataTmpPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + ArticleOriginalDataTmpPeer::NUM_COLUMNS;

		TitlePeer::addSelectColumns($c);
		$startcol5 = $startcol4 + TitlePeer::NUM_COLUMNS;

		$c->addJoin(ArticleTmpPeer::LANGUAGE_ID, LanguagePeer::ID);

		$c->addJoin(ArticleTmpPeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataTmpPeer::ID);

		$c->addJoin(ArticleTmpPeer::TITLE_ID, TitlePeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticleTmpPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = LanguagePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getLanguage(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addArticleTmp($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initArticleTmps();
				$obj2->addArticleTmp($obj1);
			}

			$omClass = ArticleOriginalDataTmpPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getArticleOriginalDataTmp(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addArticleTmp($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initArticleTmps();
				$obj3->addArticleTmp($obj1);
			}

			$omClass = TitlePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getTitle(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addArticleTmp($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initArticleTmps();
				$obj4->addArticleTmp($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptTitle(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticleTmpPeer::addSelectColumns($c);
		$startcol2 = (ArticleTmpPeer::NUM_COLUMNS - ArticleTmpPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		LanguagePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + LanguagePeer::NUM_COLUMNS;

		ArticleOriginalDataTmpPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + ArticleOriginalDataTmpPeer::NUM_COLUMNS;

		IptcPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + IptcPeer::NUM_COLUMNS;

		$c->addJoin(ArticleTmpPeer::LANGUAGE_ID, LanguagePeer::ID);

		$c->addJoin(ArticleTmpPeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataTmpPeer::ID);

		$c->addJoin(ArticleTmpPeer::IPTC_ID, IptcPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticleTmpPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = LanguagePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getLanguage(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addArticleTmp($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initArticleTmps();
				$obj2->addArticleTmp($obj1);
			}

			$omClass = ArticleOriginalDataTmpPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getArticleOriginalDataTmp(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addArticleTmp($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initArticleTmps();
				$obj3->addArticleTmp($obj1);
			}

			$omClass = IptcPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getIptc(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addArticleTmp($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initArticleTmps();
				$obj4->addArticleTmp($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ArticleTmpPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(ArticleTmpPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(ArticleTmpPeer::ID);
			$selectCriteria->add(ArticleTmpPeer::ID, $criteria->remove(ArticleTmpPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(ArticleTmpPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ArticleTmpPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof ArticleTmp) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(ArticleTmpPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(ArticleTmp $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ArticleTmpPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ArticleTmpPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ArticleTmpPeer::DATABASE_NAME, ArticleTmpPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ArticleTmpPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(ArticleTmpPeer::DATABASE_NAME);

		$criteria->add(ArticleTmpPeer::ID, $pk);


		$v = ArticleTmpPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(ArticleTmpPeer::ID, $pks, Criteria::IN);
			$objs = ArticleTmpPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseArticleTmpPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/ArticleTmpMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.ArticleTmpMapBuilder');
}
