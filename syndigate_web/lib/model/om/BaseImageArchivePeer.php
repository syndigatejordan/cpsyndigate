<?php


abstract class BaseImageArchivePeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'image_archive';

	
	const CLASS_DEFAULT = 'lib.model.ImageArchive';

	
	const NUM_COLUMNS = 9;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'image_archive.ID';

	
	const ARTICLE_ID = 'image_archive.ARTICLE_ID';

	
	const IMG_NAME = 'image_archive.IMG_NAME';

	
	const IMAGE_CAPTION = 'image_archive.IMAGE_CAPTION';

	
	const IS_HEADLINE = 'image_archive.IS_HEADLINE';

	
	const IMAGE_TYPE = 'image_archive.IMAGE_TYPE';

	
	const MIME_TYPE = 'image_archive.MIME_TYPE';

	
	const ORIGINAL_NAME = 'image_archive.ORIGINAL_NAME';

	
	const IMAGE_ORIGINAL_KEY = 'image_archive.IMAGE_ORIGINAL_KEY';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'ArticleId', 'ImgName', 'ImageCaption', 'IsHeadline', 'ImageType', 'MimeType', 'OriginalName', 'ImageOriginalKey', ),
		BasePeer::TYPE_COLNAME => array (ImageArchivePeer::ID, ImageArchivePeer::ARTICLE_ID, ImageArchivePeer::IMG_NAME, ImageArchivePeer::IMAGE_CAPTION, ImageArchivePeer::IS_HEADLINE, ImageArchivePeer::IMAGE_TYPE, ImageArchivePeer::MIME_TYPE, ImageArchivePeer::ORIGINAL_NAME, ImageArchivePeer::IMAGE_ORIGINAL_KEY, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'article_id', 'img_name', 'image_caption', 'is_headline', 'image_type', 'mime_type', 'original_name', 'image_original_key', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'ArticleId' => 1, 'ImgName' => 2, 'ImageCaption' => 3, 'IsHeadline' => 4, 'ImageType' => 5, 'MimeType' => 6, 'OriginalName' => 7, 'ImageOriginalKey' => 8, ),
		BasePeer::TYPE_COLNAME => array (ImageArchivePeer::ID => 0, ImageArchivePeer::ARTICLE_ID => 1, ImageArchivePeer::IMG_NAME => 2, ImageArchivePeer::IMAGE_CAPTION => 3, ImageArchivePeer::IS_HEADLINE => 4, ImageArchivePeer::IMAGE_TYPE => 5, ImageArchivePeer::MIME_TYPE => 6, ImageArchivePeer::ORIGINAL_NAME => 7, ImageArchivePeer::IMAGE_ORIGINAL_KEY => 8, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'article_id' => 1, 'img_name' => 2, 'image_caption' => 3, 'is_headline' => 4, 'image_type' => 5, 'mime_type' => 6, 'original_name' => 7, 'image_original_key' => 8, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/ImageArchiveMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.ImageArchiveMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ImageArchivePeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ImageArchivePeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ImageArchivePeer::ID);

		$criteria->addSelectColumn(ImageArchivePeer::ARTICLE_ID);

		$criteria->addSelectColumn(ImageArchivePeer::IMG_NAME);

		$criteria->addSelectColumn(ImageArchivePeer::IMAGE_CAPTION);

		$criteria->addSelectColumn(ImageArchivePeer::IS_HEADLINE);

		$criteria->addSelectColumn(ImageArchivePeer::IMAGE_TYPE);

		$criteria->addSelectColumn(ImageArchivePeer::MIME_TYPE);

		$criteria->addSelectColumn(ImageArchivePeer::ORIGINAL_NAME);

		$criteria->addSelectColumn(ImageArchivePeer::IMAGE_ORIGINAL_KEY);

	}

	const COUNT = 'COUNT(image_archive.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT image_archive.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ImageArchivePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ImageArchivePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ImageArchivePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ImageArchivePeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ImageArchivePeer::populateObjects(ImageArchivePeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ImageArchivePeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ImageArchivePeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinArticleArchive(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ImageArchivePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ImageArchivePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ImageArchivePeer::ARTICLE_ID, ArticleArchivePeer::ID);

		$rs = ImageArchivePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinArticleArchive(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ImageArchivePeer::addSelectColumns($c);
		$startcol = (ImageArchivePeer::NUM_COLUMNS - ImageArchivePeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		ArticleArchivePeer::addSelectColumns($c);

		$c->addJoin(ImageArchivePeer::ARTICLE_ID, ArticleArchivePeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ImageArchivePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = ArticleArchivePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getArticleArchive(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addImageArchive($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initImageArchives();
				$obj2->addImageArchive($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ImageArchivePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ImageArchivePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ImageArchivePeer::ARTICLE_ID, ArticleArchivePeer::ID);

		$rs = ImageArchivePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ImageArchivePeer::addSelectColumns($c);
		$startcol2 = (ImageArchivePeer::NUM_COLUMNS - ImageArchivePeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		ArticleArchivePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + ArticleArchivePeer::NUM_COLUMNS;

		$c->addJoin(ImageArchivePeer::ARTICLE_ID, ArticleArchivePeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ImageArchivePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = ArticleArchivePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getArticleArchive(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addImageArchive($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initImageArchives();
				$obj2->addImageArchive($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ImageArchivePeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(ImageArchivePeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(ImageArchivePeer::ID);
			$selectCriteria->add(ImageArchivePeer::ID, $criteria->remove(ImageArchivePeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(ImageArchivePeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ImageArchivePeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof ImageArchive) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(ImageArchivePeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(ImageArchive $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ImageArchivePeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ImageArchivePeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ImageArchivePeer::DATABASE_NAME, ImageArchivePeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ImageArchivePeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(ImageArchivePeer::DATABASE_NAME);

		$criteria->add(ImageArchivePeer::ID, $pk);


		$v = ImageArchivePeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(ImageArchivePeer::ID, $pks, Criteria::IN);
			$objs = ImageArchivePeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseImageArchivePeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/ImageArchiveMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.ImageArchiveMapBuilder');
}
