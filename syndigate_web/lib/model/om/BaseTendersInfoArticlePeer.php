<?php


abstract class BaseTendersInfoArticlePeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'tenders_info_article';

	
	const CLASS_DEFAULT = 'lib.model.TendersInfoArticle';

	
	const NUM_COLUMNS = 13;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'tenders_info_article.ID';

	
	const IPTC_ID = 'tenders_info_article.IPTC_ID';

	
	const TITLE_ID = 'tenders_info_article.TITLE_ID';

	
	const ARTICLE_ORIGINAL_DATA_ID = 'tenders_info_article.ARTICLE_ORIGINAL_DATA_ID';

	
	const LANGUAGE_ID = 'tenders_info_article.LANGUAGE_ID';

	
	const HEADLINE = 'tenders_info_article.HEADLINE';

	
	const SUMMARY = 'tenders_info_article.SUMMARY';

	
	const BODY = 'tenders_info_article.BODY';

	
	const AUTHOR = 'tenders_info_article.AUTHOR';

	
	const DATE = 'tenders_info_article.DATE';

	
	const PARSED_AT = 'tenders_info_article.PARSED_AT';

	
	const UPDATED_AT = 'tenders_info_article.UPDATED_AT';

	
	const HAS_TIME = 'tenders_info_article.HAS_TIME';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'IptcId', 'TitleId', 'ArticleOriginalDataId', 'LanguageId', 'Headline', 'Summary', 'Body', 'Author', 'Date', 'ParsedAt', 'UpdatedAt', 'HasTime', ),
		BasePeer::TYPE_COLNAME => array (TendersInfoArticlePeer::ID, TendersInfoArticlePeer::IPTC_ID, TendersInfoArticlePeer::TITLE_ID, TendersInfoArticlePeer::ARTICLE_ORIGINAL_DATA_ID, TendersInfoArticlePeer::LANGUAGE_ID, TendersInfoArticlePeer::HEADLINE, TendersInfoArticlePeer::SUMMARY, TendersInfoArticlePeer::BODY, TendersInfoArticlePeer::AUTHOR, TendersInfoArticlePeer::DATE, TendersInfoArticlePeer::PARSED_AT, TendersInfoArticlePeer::UPDATED_AT, TendersInfoArticlePeer::HAS_TIME, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'iptc_id', 'title_id', 'article_original_data_id', 'language_id', 'headline', 'summary', 'body', 'author', 'date', 'parsed_at', 'updated_at', 'has_time', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'IptcId' => 1, 'TitleId' => 2, 'ArticleOriginalDataId' => 3, 'LanguageId' => 4, 'Headline' => 5, 'Summary' => 6, 'Body' => 7, 'Author' => 8, 'Date' => 9, 'ParsedAt' => 10, 'UpdatedAt' => 11, 'HasTime' => 12, ),
		BasePeer::TYPE_COLNAME => array (TendersInfoArticlePeer::ID => 0, TendersInfoArticlePeer::IPTC_ID => 1, TendersInfoArticlePeer::TITLE_ID => 2, TendersInfoArticlePeer::ARTICLE_ORIGINAL_DATA_ID => 3, TendersInfoArticlePeer::LANGUAGE_ID => 4, TendersInfoArticlePeer::HEADLINE => 5, TendersInfoArticlePeer::SUMMARY => 6, TendersInfoArticlePeer::BODY => 7, TendersInfoArticlePeer::AUTHOR => 8, TendersInfoArticlePeer::DATE => 9, TendersInfoArticlePeer::PARSED_AT => 10, TendersInfoArticlePeer::UPDATED_AT => 11, TendersInfoArticlePeer::HAS_TIME => 12, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'iptc_id' => 1, 'title_id' => 2, 'article_original_data_id' => 3, 'language_id' => 4, 'headline' => 5, 'summary' => 6, 'body' => 7, 'author' => 8, 'date' => 9, 'parsed_at' => 10, 'updated_at' => 11, 'has_time' => 12, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/TendersInfoArticleMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.TendersInfoArticleMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = TendersInfoArticlePeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(TendersInfoArticlePeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(TendersInfoArticlePeer::ID);

		$criteria->addSelectColumn(TendersInfoArticlePeer::IPTC_ID);

		$criteria->addSelectColumn(TendersInfoArticlePeer::TITLE_ID);

		$criteria->addSelectColumn(TendersInfoArticlePeer::ARTICLE_ORIGINAL_DATA_ID);

		$criteria->addSelectColumn(TendersInfoArticlePeer::LANGUAGE_ID);

		$criteria->addSelectColumn(TendersInfoArticlePeer::HEADLINE);

		$criteria->addSelectColumn(TendersInfoArticlePeer::SUMMARY);

		$criteria->addSelectColumn(TendersInfoArticlePeer::BODY);

		$criteria->addSelectColumn(TendersInfoArticlePeer::AUTHOR);

		$criteria->addSelectColumn(TendersInfoArticlePeer::DATE);

		$criteria->addSelectColumn(TendersInfoArticlePeer::PARSED_AT);

		$criteria->addSelectColumn(TendersInfoArticlePeer::UPDATED_AT);

		$criteria->addSelectColumn(TendersInfoArticlePeer::HAS_TIME);

	}

	const COUNT = 'COUNT(*)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT *)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TendersInfoArticlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TendersInfoArticlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = TendersInfoArticlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = TendersInfoArticlePeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return TendersInfoArticlePeer::populateObjects(TendersInfoArticlePeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			TendersInfoArticlePeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = TendersInfoArticlePeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return TendersInfoArticlePeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(TendersInfoArticlePeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(TendersInfoArticlePeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof TendersInfoArticle) {

			$criteria = $values->buildCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

			}

		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(TendersInfoArticle $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(TendersInfoArticlePeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(TendersInfoArticlePeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(TendersInfoArticlePeer::DATABASE_NAME, TendersInfoArticlePeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = TendersInfoArticlePeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

} 
if (Propel::isInit()) {
			try {
		BaseTendersInfoArticlePeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/TendersInfoArticleMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.TendersInfoArticleMapBuilder');
}
