<?php


abstract class BaseVideo extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $article_id;


	
	protected $video_name;


	
	protected $video_caption;


	
	protected $original_name;


	
	protected $video_type;


	
	protected $bit_rate;


	
	protected $added_time;


	
	protected $mime_type;

	
	protected $aArticle;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getArticleId()
	{

		return $this->article_id;
	}

	
	public function getVideoName()
	{

		return $this->video_name;
	}

	
	public function getVideoCaption()
	{

		return $this->video_caption;
	}

	
	public function getOriginalName()
	{

		return $this->original_name;
	}

	
	public function getVideoType()
	{

		return $this->video_type;
	}

	
	public function getBitRate()
	{

		return $this->bit_rate;
	}

	
	public function getAddedTime($format = 'Y-m-d H:i:s')
	{

		if ($this->added_time === null || $this->added_time === '') {
			return null;
		} elseif (!is_int($this->added_time)) {
						$ts = strtotime($this->added_time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [added_time] as date/time value: " . var_export($this->added_time, true));
			}
		} else {
			$ts = $this->added_time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getMimeType()
	{

		return $this->mime_type;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = VideoPeer::ID;
		}

	} 
	
	public function setArticleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->article_id !== $v) {
			$this->article_id = $v;
			$this->modifiedColumns[] = VideoPeer::ARTICLE_ID;
		}

		if ($this->aArticle !== null && $this->aArticle->getId() !== $v) {
			$this->aArticle = null;
		}

	} 
	
	public function setVideoName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->video_name !== $v) {
			$this->video_name = $v;
			$this->modifiedColumns[] = VideoPeer::VIDEO_NAME;
		}

	} 
	
	public function setVideoCaption($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->video_caption !== $v) {
			$this->video_caption = $v;
			$this->modifiedColumns[] = VideoPeer::VIDEO_CAPTION;
		}

	} 
	
	public function setOriginalName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->original_name !== $v) {
			$this->original_name = $v;
			$this->modifiedColumns[] = VideoPeer::ORIGINAL_NAME;
		}

	} 
	
	public function setVideoType($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->video_type !== $v) {
			$this->video_type = $v;
			$this->modifiedColumns[] = VideoPeer::VIDEO_TYPE;
		}

	} 
	
	public function setBitRate($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->bit_rate !== $v) {
			$this->bit_rate = $v;
			$this->modifiedColumns[] = VideoPeer::BIT_RATE;
		}

	} 
	
	public function setAddedTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [added_time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->added_time !== $ts) {
			$this->added_time = $ts;
			$this->modifiedColumns[] = VideoPeer::ADDED_TIME;
		}

	} 
	
	public function setMimeType($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->mime_type !== $v) {
			$this->mime_type = $v;
			$this->modifiedColumns[] = VideoPeer::MIME_TYPE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->article_id = $rs->getInt($startcol + 1);

			$this->video_name = $rs->getString($startcol + 2);

			$this->video_caption = $rs->getString($startcol + 3);

			$this->original_name = $rs->getString($startcol + 4);

			$this->video_type = $rs->getString($startcol + 5);

			$this->bit_rate = $rs->getString($startcol + 6);

			$this->added_time = $rs->getTimestamp($startcol + 7, null);

			$this->mime_type = $rs->getString($startcol + 8);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 9; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Video object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(VideoPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			VideoPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(VideoPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aArticle !== null) {
				if ($this->aArticle->isModified()) {
					$affectedRows += $this->aArticle->save($con);
				}
				$this->setArticle($this->aArticle);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = VideoPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += VideoPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aArticle !== null) {
				if (!$this->aArticle->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aArticle->getValidationFailures());
				}
			}


			if (($retval = VideoPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = VideoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getArticleId();
				break;
			case 2:
				return $this->getVideoName();
				break;
			case 3:
				return $this->getVideoCaption();
				break;
			case 4:
				return $this->getOriginalName();
				break;
			case 5:
				return $this->getVideoType();
				break;
			case 6:
				return $this->getBitRate();
				break;
			case 7:
				return $this->getAddedTime();
				break;
			case 8:
				return $this->getMimeType();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = VideoPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getArticleId(),
			$keys[2] => $this->getVideoName(),
			$keys[3] => $this->getVideoCaption(),
			$keys[4] => $this->getOriginalName(),
			$keys[5] => $this->getVideoType(),
			$keys[6] => $this->getBitRate(),
			$keys[7] => $this->getAddedTime(),
			$keys[8] => $this->getMimeType(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = VideoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setArticleId($value);
				break;
			case 2:
				$this->setVideoName($value);
				break;
			case 3:
				$this->setVideoCaption($value);
				break;
			case 4:
				$this->setOriginalName($value);
				break;
			case 5:
				$this->setVideoType($value);
				break;
			case 6:
				$this->setBitRate($value);
				break;
			case 7:
				$this->setAddedTime($value);
				break;
			case 8:
				$this->setMimeType($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = VideoPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setArticleId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setVideoName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setVideoCaption($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setOriginalName($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setVideoType($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setBitRate($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setAddedTime($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setMimeType($arr[$keys[8]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(VideoPeer::DATABASE_NAME);

		if ($this->isColumnModified(VideoPeer::ID)) $criteria->add(VideoPeer::ID, $this->id);
		if ($this->isColumnModified(VideoPeer::ARTICLE_ID)) $criteria->add(VideoPeer::ARTICLE_ID, $this->article_id);
		if ($this->isColumnModified(VideoPeer::VIDEO_NAME)) $criteria->add(VideoPeer::VIDEO_NAME, $this->video_name);
		if ($this->isColumnModified(VideoPeer::VIDEO_CAPTION)) $criteria->add(VideoPeer::VIDEO_CAPTION, $this->video_caption);
		if ($this->isColumnModified(VideoPeer::ORIGINAL_NAME)) $criteria->add(VideoPeer::ORIGINAL_NAME, $this->original_name);
		if ($this->isColumnModified(VideoPeer::VIDEO_TYPE)) $criteria->add(VideoPeer::VIDEO_TYPE, $this->video_type);
		if ($this->isColumnModified(VideoPeer::BIT_RATE)) $criteria->add(VideoPeer::BIT_RATE, $this->bit_rate);
		if ($this->isColumnModified(VideoPeer::ADDED_TIME)) $criteria->add(VideoPeer::ADDED_TIME, $this->added_time);
		if ($this->isColumnModified(VideoPeer::MIME_TYPE)) $criteria->add(VideoPeer::MIME_TYPE, $this->mime_type);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(VideoPeer::DATABASE_NAME);

		$criteria->add(VideoPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setArticleId($this->article_id);

		$copyObj->setVideoName($this->video_name);

		$copyObj->setVideoCaption($this->video_caption);

		$copyObj->setOriginalName($this->original_name);

		$copyObj->setVideoType($this->video_type);

		$copyObj->setBitRate($this->bit_rate);

		$copyObj->setAddedTime($this->added_time);

		$copyObj->setMimeType($this->mime_type);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new VideoPeer();
		}
		return self::$peer;
	}

	
	public function setArticle($v)
	{


		if ($v === null) {
			$this->setArticleId(NULL);
		} else {
			$this->setArticleId($v->getId());
		}


		$this->aArticle = $v;
	}


	
	public function getArticle($con = null)
	{
		if ($this->aArticle === null && ($this->article_id !== null)) {
						include_once 'lib/model/om/BaseArticlePeer.php';

			$this->aArticle = ArticlePeer::retrieveByPK($this->article_id, $con);

			
		}
		return $this->aArticle;
	}

} 