<?php


abstract class BaseOrdersFrequency extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $o_id;


	
	protected $title_id;


	
	protected $frequency;


	
	protected $start;


	
	protected $end;

	
	protected $aTitle;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getOId()
	{

		return $this->o_id;
	}

	
	public function getTitleId()
	{

		return $this->title_id;
	}

	
	public function getFrequency()
	{

		return $this->frequency;
	}

	
	public function getStart($format = 'Y-m-d H:i:s')
	{

		if ($this->start === null || $this->start === '') {
			return null;
		} elseif (!is_int($this->start)) {
						$ts = strtotime($this->start);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [start] as date/time value: " . var_export($this->start, true));
			}
		} else {
			$ts = $this->start;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getEnd($format = 'Y-m-d H:i:s')
	{

		if ($this->end === null || $this->end === '') {
			return null;
		} elseif (!is_int($this->end)) {
						$ts = strtotime($this->end);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [end] as date/time value: " . var_export($this->end, true));
			}
		} else {
			$ts = $this->end;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function setOId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->o_id !== $v) {
			$this->o_id = $v;
			$this->modifiedColumns[] = OrdersFrequencyPeer::O_ID;
		}

	} 
	
	public function setTitleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_id !== $v) {
			$this->title_id = $v;
			$this->modifiedColumns[] = OrdersFrequencyPeer::TITLE_ID;
		}

		if ($this->aTitle !== null && $this->aTitle->getId() !== $v) {
			$this->aTitle = null;
		}

	} 
	
	public function setFrequency($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->frequency !== $v) {
			$this->frequency = $v;
			$this->modifiedColumns[] = OrdersFrequencyPeer::FREQUENCY;
		}

	} 
	
	public function setStart($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [start] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->start !== $ts) {
			$this->start = $ts;
			$this->modifiedColumns[] = OrdersFrequencyPeer::START;
		}

	} 
	
	public function setEnd($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [end] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->end !== $ts) {
			$this->end = $ts;
			$this->modifiedColumns[] = OrdersFrequencyPeer::END;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->o_id = $rs->getInt($startcol + 0);

			$this->title_id = $rs->getInt($startcol + 1);

			$this->frequency = $rs->getInt($startcol + 2);

			$this->start = $rs->getTimestamp($startcol + 3, null);

			$this->end = $rs->getTimestamp($startcol + 4, null);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating OrdersFrequency object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(OrdersFrequencyPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			OrdersFrequencyPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(OrdersFrequencyPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aTitle !== null) {
				if ($this->aTitle->isModified()) {
					$affectedRows += $this->aTitle->save($con);
				}
				$this->setTitle($this->aTitle);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = OrdersFrequencyPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setOId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += OrdersFrequencyPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aTitle !== null) {
				if (!$this->aTitle->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTitle->getValidationFailures());
				}
			}


			if (($retval = OrdersFrequencyPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = OrdersFrequencyPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getOId();
				break;
			case 1:
				return $this->getTitleId();
				break;
			case 2:
				return $this->getFrequency();
				break;
			case 3:
				return $this->getStart();
				break;
			case 4:
				return $this->getEnd();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = OrdersFrequencyPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getOId(),
			$keys[1] => $this->getTitleId(),
			$keys[2] => $this->getFrequency(),
			$keys[3] => $this->getStart(),
			$keys[4] => $this->getEnd(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = OrdersFrequencyPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setOId($value);
				break;
			case 1:
				$this->setTitleId($value);
				break;
			case 2:
				$this->setFrequency($value);
				break;
			case 3:
				$this->setStart($value);
				break;
			case 4:
				$this->setEnd($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = OrdersFrequencyPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setOId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTitleId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setFrequency($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setStart($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setEnd($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(OrdersFrequencyPeer::DATABASE_NAME);

		if ($this->isColumnModified(OrdersFrequencyPeer::O_ID)) $criteria->add(OrdersFrequencyPeer::O_ID, $this->o_id);
		if ($this->isColumnModified(OrdersFrequencyPeer::TITLE_ID)) $criteria->add(OrdersFrequencyPeer::TITLE_ID, $this->title_id);
		if ($this->isColumnModified(OrdersFrequencyPeer::FREQUENCY)) $criteria->add(OrdersFrequencyPeer::FREQUENCY, $this->frequency);
		if ($this->isColumnModified(OrdersFrequencyPeer::START)) $criteria->add(OrdersFrequencyPeer::START, $this->start);
		if ($this->isColumnModified(OrdersFrequencyPeer::END)) $criteria->add(OrdersFrequencyPeer::END, $this->end);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(OrdersFrequencyPeer::DATABASE_NAME);

		$criteria->add(OrdersFrequencyPeer::O_ID, $this->o_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getOId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setOId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTitleId($this->title_id);

		$copyObj->setFrequency($this->frequency);

		$copyObj->setStart($this->start);

		$copyObj->setEnd($this->end);


		$copyObj->setNew(true);

		$copyObj->setOId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new OrdersFrequencyPeer();
		}
		return self::$peer;
	}

	
	public function setTitle($v)
	{


		if ($v === null) {
			$this->setTitleId(NULL);
		} else {
			$this->setTitleId($v->getId());
		}


		$this->aTitle = $v;
	}


	
	public function getTitle($con = null)
	{
		if ($this->aTitle === null && ($this->title_id !== null)) {
						include_once 'lib/model/om/BaseTitlePeer.php';

			$this->aTitle = TitlePeer::retrieveByPK($this->title_id, $con);

			
		}
		return $this->aTitle;
	}

} 