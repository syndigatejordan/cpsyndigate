<?php


abstract class BaseAdmins extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $username = '';


	
	protected $password = '';


	
	protected $last_login = 0;


	
	protected $email = '';

	
	protected $collPublishers;

	
	protected $lastPublisherCriteria = null;

	
	protected $collTitles;

	
	protected $lastTitleCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getUsername()
	{

		return $this->username;
	}

	
	public function getPassword()
	{

		return $this->password;
	}

	
	public function getLastLogin()
	{

		return $this->last_login;
	}

	
	public function getEmail()
	{

		return $this->email;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = AdminsPeer::ID;
		}

	} 
	
	public function setUsername($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->username !== $v || $v === '') {
			$this->username = $v;
			$this->modifiedColumns[] = AdminsPeer::USERNAME;
		}

	} 
	
	public function setPassword($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->password !== $v || $v === '') {
			$this->password = $v;
			$this->modifiedColumns[] = AdminsPeer::PASSWORD;
		}

	} 
	
	public function setLastLogin($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->last_login !== $v || $v === 0) {
			$this->last_login = $v;
			$this->modifiedColumns[] = AdminsPeer::LAST_LOGIN;
		}

	} 
	
	public function setEmail($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->email !== $v || $v === '') {
			$this->email = $v;
			$this->modifiedColumns[] = AdminsPeer::EMAIL;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->username = $rs->getString($startcol + 1);

			$this->password = $rs->getString($startcol + 2);

			$this->last_login = $rs->getInt($startcol + 3);

			$this->email = $rs->getString($startcol + 4);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Admins object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(AdminsPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			AdminsPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(AdminsPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = AdminsPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += AdminsPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collPublishers !== null) {
				foreach($this->collPublishers as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collTitles !== null) {
				foreach($this->collTitles as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = AdminsPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collPublishers !== null) {
					foreach($this->collPublishers as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collTitles !== null) {
					foreach($this->collTitles as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = AdminsPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getUsername();
				break;
			case 2:
				return $this->getPassword();
				break;
			case 3:
				return $this->getLastLogin();
				break;
			case 4:
				return $this->getEmail();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = AdminsPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getUsername(),
			$keys[2] => $this->getPassword(),
			$keys[3] => $this->getLastLogin(),
			$keys[4] => $this->getEmail(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = AdminsPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setUsername($value);
				break;
			case 2:
				$this->setPassword($value);
				break;
			case 3:
				$this->setLastLogin($value);
				break;
			case 4:
				$this->setEmail($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = AdminsPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUsername($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setPassword($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setLastLogin($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setEmail($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(AdminsPeer::DATABASE_NAME);

		if ($this->isColumnModified(AdminsPeer::ID)) $criteria->add(AdminsPeer::ID, $this->id);
		if ($this->isColumnModified(AdminsPeer::USERNAME)) $criteria->add(AdminsPeer::USERNAME, $this->username);
		if ($this->isColumnModified(AdminsPeer::PASSWORD)) $criteria->add(AdminsPeer::PASSWORD, $this->password);
		if ($this->isColumnModified(AdminsPeer::LAST_LOGIN)) $criteria->add(AdminsPeer::LAST_LOGIN, $this->last_login);
		if ($this->isColumnModified(AdminsPeer::EMAIL)) $criteria->add(AdminsPeer::EMAIL, $this->email);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(AdminsPeer::DATABASE_NAME);

		$criteria->add(AdminsPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUsername($this->username);

		$copyObj->setPassword($this->password);

		$copyObj->setLastLogin($this->last_login);

		$copyObj->setEmail($this->email);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getPublishers() as $relObj) {
				$copyObj->addPublisher($relObj->copy($deepCopy));
			}

			foreach($this->getTitles() as $relObj) {
				$copyObj->addTitle($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new AdminsPeer();
		}
		return self::$peer;
	}

	
	public function initPublishers()
	{
		if ($this->collPublishers === null) {
			$this->collPublishers = array();
		}
	}

	
	public function getPublishers($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BasePublisherPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collPublishers === null) {
			if ($this->isNew()) {
			   $this->collPublishers = array();
			} else {

				$criteria->add(PublisherPeer::ADMINS_ID, $this->getId());

				PublisherPeer::addSelectColumns($criteria);
				$this->collPublishers = PublisherPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(PublisherPeer::ADMINS_ID, $this->getId());

				PublisherPeer::addSelectColumns($criteria);
				if (!isset($this->lastPublisherCriteria) || !$this->lastPublisherCriteria->equals($criteria)) {
					$this->collPublishers = PublisherPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastPublisherCriteria = $criteria;
		return $this->collPublishers;
	}

	
	public function countPublishers($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BasePublisherPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(PublisherPeer::ADMINS_ID, $this->getId());

		return PublisherPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addPublisher(Publisher $l)
	{
		$this->collPublishers[] = $l;
		$l->setAdmins($this);
	}


	
	public function getPublishersJoinCountry($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BasePublisherPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collPublishers === null) {
			if ($this->isNew()) {
				$this->collPublishers = array();
			} else {

				$criteria->add(PublisherPeer::ADMINS_ID, $this->getId());

				$this->collPublishers = PublisherPeer::doSelectJoinCountry($criteria, $con);
			}
		} else {
									
			$criteria->add(PublisherPeer::ADMINS_ID, $this->getId());

			if (!isset($this->lastPublisherCriteria) || !$this->lastPublisherCriteria->equals($criteria)) {
				$this->collPublishers = PublisherPeer::doSelectJoinCountry($criteria, $con);
			}
		}
		$this->lastPublisherCriteria = $criteria;

		return $this->collPublishers;
	}

	
	public function initTitles()
	{
		if ($this->collTitles === null) {
			$this->collTitles = array();
		}
	}

	
	public function getTitles($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
			   $this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::ADMINS_ID, $this->getId());

				TitlePeer::addSelectColumns($criteria);
				$this->collTitles = TitlePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(TitlePeer::ADMINS_ID, $this->getId());

				TitlePeer::addSelectColumns($criteria);
				if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
					$this->collTitles = TitlePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastTitleCriteria = $criteria;
		return $this->collTitles;
	}

	
	public function countTitles($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(TitlePeer::ADMINS_ID, $this->getId());

		return TitlePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addTitle(Title $l)
	{
		$this->collTitles[] = $l;
		$l->setAdmins($this);
	}


	
	public function getTitlesJoinPublisher($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::ADMINS_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinPublisher($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::ADMINS_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinPublisher($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinTitleType($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::ADMINS_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinTitleType($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::ADMINS_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinTitleType($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinTitleFrequency($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::ADMINS_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinTitleFrequency($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::ADMINS_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinTitleFrequency($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinCountry($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::ADMINS_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinCountry($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::ADMINS_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinCountry($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinLanguage($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::ADMINS_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinLanguage($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::ADMINS_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinLanguage($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}

} 