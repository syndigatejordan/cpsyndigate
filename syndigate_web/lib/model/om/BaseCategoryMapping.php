<?php


abstract class BaseCategoryMapping extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $iptc_id;


	
	protected $original_article_category_id;

	
	protected $aIptc;

	
	protected $aOriginalArticleCategory;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getIptcId()
	{

		return $this->iptc_id;
	}

	
	public function getOriginalArticleCategoryId()
	{

		return $this->original_article_category_id;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = CategoryMappingPeer::ID;
		}

	} 
	
	public function setIptcId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->iptc_id !== $v) {
			$this->iptc_id = $v;
			$this->modifiedColumns[] = CategoryMappingPeer::IPTC_ID;
		}

		if ($this->aIptc !== null && $this->aIptc->getId() !== $v) {
			$this->aIptc = null;
		}

	} 
	
	public function setOriginalArticleCategoryId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->original_article_category_id !== $v) {
			$this->original_article_category_id = $v;
			$this->modifiedColumns[] = CategoryMappingPeer::ORIGINAL_ARTICLE_CATEGORY_ID;
		}

		if ($this->aOriginalArticleCategory !== null && $this->aOriginalArticleCategory->getId() !== $v) {
			$this->aOriginalArticleCategory = null;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->iptc_id = $rs->getInt($startcol + 1);

			$this->original_article_category_id = $rs->getInt($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 3; 
		} catch (Exception $e) {
			throw new PropelException("Error populating CategoryMapping object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(CategoryMappingPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			CategoryMappingPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(CategoryMappingPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aIptc !== null) {
				if ($this->aIptc->isModified()) {
					$affectedRows += $this->aIptc->save($con);
				}
				$this->setIptc($this->aIptc);
			}

			if ($this->aOriginalArticleCategory !== null) {
				if ($this->aOriginalArticleCategory->isModified()) {
					$affectedRows += $this->aOriginalArticleCategory->save($con);
				}
				$this->setOriginalArticleCategory($this->aOriginalArticleCategory);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = CategoryMappingPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += CategoryMappingPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aIptc !== null) {
				if (!$this->aIptc->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aIptc->getValidationFailures());
				}
			}

			if ($this->aOriginalArticleCategory !== null) {
				if (!$this->aOriginalArticleCategory->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aOriginalArticleCategory->getValidationFailures());
				}
			}


			if (($retval = CategoryMappingPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = CategoryMappingPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getIptcId();
				break;
			case 2:
				return $this->getOriginalArticleCategoryId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = CategoryMappingPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getIptcId(),
			$keys[2] => $this->getOriginalArticleCategoryId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = CategoryMappingPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setIptcId($value);
				break;
			case 2:
				$this->setOriginalArticleCategoryId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = CategoryMappingPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setIptcId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setOriginalArticleCategoryId($arr[$keys[2]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(CategoryMappingPeer::DATABASE_NAME);

		if ($this->isColumnModified(CategoryMappingPeer::ID)) $criteria->add(CategoryMappingPeer::ID, $this->id);
		if ($this->isColumnModified(CategoryMappingPeer::IPTC_ID)) $criteria->add(CategoryMappingPeer::IPTC_ID, $this->iptc_id);
		if ($this->isColumnModified(CategoryMappingPeer::ORIGINAL_ARTICLE_CATEGORY_ID)) $criteria->add(CategoryMappingPeer::ORIGINAL_ARTICLE_CATEGORY_ID, $this->original_article_category_id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(CategoryMappingPeer::DATABASE_NAME);

		$criteria->add(CategoryMappingPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setIptcId($this->iptc_id);

		$copyObj->setOriginalArticleCategoryId($this->original_article_category_id);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new CategoryMappingPeer();
		}
		return self::$peer;
	}

	
	public function setIptc($v)
	{


		if ($v === null) {
			$this->setIptcId(NULL);
		} else {
			$this->setIptcId($v->getId());
		}


		$this->aIptc = $v;
	}


	
	public function getIptc($con = null)
	{
		if ($this->aIptc === null && ($this->iptc_id !== null)) {
						include_once 'lib/model/om/BaseIptcPeer.php';

			$this->aIptc = IptcPeer::retrieveByPK($this->iptc_id, $con);

			
		}
		return $this->aIptc;
	}

	
	public function setOriginalArticleCategory($v)
	{


		if ($v === null) {
			$this->setOriginalArticleCategoryId(NULL);
		} else {
			$this->setOriginalArticleCategoryId($v->getId());
		}


		$this->aOriginalArticleCategory = $v;
	}


	
	public function getOriginalArticleCategory($con = null)
	{
		if ($this->aOriginalArticleCategory === null && ($this->original_article_category_id !== null)) {
						include_once 'lib/model/om/BaseOriginalArticleCategoryPeer.php';

			$this->aOriginalArticleCategory = OriginalArticleCategoryPeer::retrieveByPK($this->original_article_category_id, $con);

			
		}
		return $this->aOriginalArticleCategory;
	}

} 