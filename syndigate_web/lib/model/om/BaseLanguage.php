<?php


abstract class BaseLanguage extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $name;


	
	protected $code;


	
	protected $dir;

	
	protected $collArticles;

	
	protected $lastArticleCriteria = null;

	
	protected $collTitles;

	
	protected $lastTitleCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getName()
	{

		return $this->name;
	}

	
	public function getCode()
	{

		return $this->code;
	}

	
	public function getDir()
	{

		return $this->dir;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = LanguagePeer::ID;
		}

	} 
	
	public function setName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->name !== $v) {
			$this->name = $v;
			$this->modifiedColumns[] = LanguagePeer::NAME;
		}

	} 
	
	public function setCode($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->code !== $v) {
			$this->code = $v;
			$this->modifiedColumns[] = LanguagePeer::CODE;
		}

	} 
	
	public function setDir($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->dir !== $v) {
			$this->dir = $v;
			$this->modifiedColumns[] = LanguagePeer::DIR;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->name = $rs->getString($startcol + 1);

			$this->code = $rs->getString($startcol + 2);

			$this->dir = $rs->getString($startcol + 3);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 4; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Language object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LanguagePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			LanguagePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LanguagePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = LanguagePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += LanguagePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collArticles !== null) {
				foreach($this->collArticles as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collTitles !== null) {
				foreach($this->collTitles as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = LanguagePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collArticles !== null) {
					foreach($this->collArticles as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collTitles !== null) {
					foreach($this->collTitles as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LanguagePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getName();
				break;
			case 2:
				return $this->getCode();
				break;
			case 3:
				return $this->getDir();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = LanguagePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getName(),
			$keys[2] => $this->getCode(),
			$keys[3] => $this->getDir(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LanguagePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setName($value);
				break;
			case 2:
				$this->setCode($value);
				break;
			case 3:
				$this->setDir($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = LanguagePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setName($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setCode($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setDir($arr[$keys[3]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(LanguagePeer::DATABASE_NAME);

		if ($this->isColumnModified(LanguagePeer::ID)) $criteria->add(LanguagePeer::ID, $this->id);
		if ($this->isColumnModified(LanguagePeer::NAME)) $criteria->add(LanguagePeer::NAME, $this->name);
		if ($this->isColumnModified(LanguagePeer::CODE)) $criteria->add(LanguagePeer::CODE, $this->code);
		if ($this->isColumnModified(LanguagePeer::DIR)) $criteria->add(LanguagePeer::DIR, $this->dir);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(LanguagePeer::DATABASE_NAME);

		$criteria->add(LanguagePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setName($this->name);

		$copyObj->setCode($this->code);

		$copyObj->setDir($this->dir);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getArticles() as $relObj) {
				$copyObj->addArticle($relObj->copy($deepCopy));
			}

			foreach($this->getTitles() as $relObj) {
				$copyObj->addTitle($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new LanguagePeer();
		}
		return self::$peer;
	}

	
	public function initArticles()
	{
		if ($this->collArticles === null) {
			$this->collArticles = array();
		}
	}

	
	public function getArticles($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticles === null) {
			if ($this->isNew()) {
			   $this->collArticles = array();
			} else {

				$criteria->add(ArticlePeer::LANGUAGE_ID, $this->getId());

				ArticlePeer::addSelectColumns($criteria);
				$this->collArticles = ArticlePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ArticlePeer::LANGUAGE_ID, $this->getId());

				ArticlePeer::addSelectColumns($criteria);
				if (!isset($this->lastArticleCriteria) || !$this->lastArticleCriteria->equals($criteria)) {
					$this->collArticles = ArticlePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastArticleCriteria = $criteria;
		return $this->collArticles;
	}

	
	public function countArticles($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ArticlePeer::LANGUAGE_ID, $this->getId());

		return ArticlePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addArticle(Article $l)
	{
		$this->collArticles[] = $l;
		$l->setLanguage($this);
	}


	
	public function getArticlesJoinArticleOriginalData($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticles === null) {
			if ($this->isNew()) {
				$this->collArticles = array();
			} else {

				$criteria->add(ArticlePeer::LANGUAGE_ID, $this->getId());

				$this->collArticles = ArticlePeer::doSelectJoinArticleOriginalData($criteria, $con);
			}
		} else {
									
			$criteria->add(ArticlePeer::LANGUAGE_ID, $this->getId());

			if (!isset($this->lastArticleCriteria) || !$this->lastArticleCriteria->equals($criteria)) {
				$this->collArticles = ArticlePeer::doSelectJoinArticleOriginalData($criteria, $con);
			}
		}
		$this->lastArticleCriteria = $criteria;

		return $this->collArticles;
	}


	
	public function getArticlesJoinIptc($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticles === null) {
			if ($this->isNew()) {
				$this->collArticles = array();
			} else {

				$criteria->add(ArticlePeer::LANGUAGE_ID, $this->getId());

				$this->collArticles = ArticlePeer::doSelectJoinIptc($criteria, $con);
			}
		} else {
									
			$criteria->add(ArticlePeer::LANGUAGE_ID, $this->getId());

			if (!isset($this->lastArticleCriteria) || !$this->lastArticleCriteria->equals($criteria)) {
				$this->collArticles = ArticlePeer::doSelectJoinIptc($criteria, $con);
			}
		}
		$this->lastArticleCriteria = $criteria;

		return $this->collArticles;
	}


	
	public function getArticlesJoinTitle($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticles === null) {
			if ($this->isNew()) {
				$this->collArticles = array();
			} else {

				$criteria->add(ArticlePeer::LANGUAGE_ID, $this->getId());

				$this->collArticles = ArticlePeer::doSelectJoinTitle($criteria, $con);
			}
		} else {
									
			$criteria->add(ArticlePeer::LANGUAGE_ID, $this->getId());

			if (!isset($this->lastArticleCriteria) || !$this->lastArticleCriteria->equals($criteria)) {
				$this->collArticles = ArticlePeer::doSelectJoinTitle($criteria, $con);
			}
		}
		$this->lastArticleCriteria = $criteria;

		return $this->collArticles;
	}

	
	public function initTitles()
	{
		if ($this->collTitles === null) {
			$this->collTitles = array();
		}
	}

	
	public function getTitles($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
			   $this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::LANGUAGE_ID, $this->getId());

				TitlePeer::addSelectColumns($criteria);
				$this->collTitles = TitlePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(TitlePeer::LANGUAGE_ID, $this->getId());

				TitlePeer::addSelectColumns($criteria);
				if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
					$this->collTitles = TitlePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastTitleCriteria = $criteria;
		return $this->collTitles;
	}

	
	public function countTitles($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(TitlePeer::LANGUAGE_ID, $this->getId());

		return TitlePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addTitle(Title $l)
	{
		$this->collTitles[] = $l;
		$l->setLanguage($this);
	}


	
	public function getTitlesJoinPublisher($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::LANGUAGE_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinPublisher($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::LANGUAGE_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinPublisher($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinTitleType($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::LANGUAGE_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinTitleType($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::LANGUAGE_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinTitleType($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinTitleFrequency($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::LANGUAGE_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinTitleFrequency($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::LANGUAGE_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinTitleFrequency($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinCountry($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::LANGUAGE_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinCountry($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::LANGUAGE_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinCountry($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinAdmins($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::LANGUAGE_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinAdmins($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::LANGUAGE_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinAdmins($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}

} 