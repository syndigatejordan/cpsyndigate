<?php


abstract class BasePublisherContactLink extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $contact_id;


	
	protected $publisher_id;

	
	protected $aPublisher;

	
	protected $aContact;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getContactId()
	{

		return $this->contact_id;
	}

	
	public function getPublisherId()
	{

		return $this->publisher_id;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = PublisherContactLinkPeer::ID;
		}

	} 
	
	public function setContactId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->contact_id !== $v) {
			$this->contact_id = $v;
			$this->modifiedColumns[] = PublisherContactLinkPeer::CONTACT_ID;
		}

		if ($this->aContact !== null && $this->aContact->getId() !== $v) {
			$this->aContact = null;
		}

	} 
	
	public function setPublisherId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->publisher_id !== $v) {
			$this->publisher_id = $v;
			$this->modifiedColumns[] = PublisherContactLinkPeer::PUBLISHER_ID;
		}

		if ($this->aPublisher !== null && $this->aPublisher->getId() !== $v) {
			$this->aPublisher = null;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->contact_id = $rs->getInt($startcol + 1);

			$this->publisher_id = $rs->getInt($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 3; 
		} catch (Exception $e) {
			throw new PropelException("Error populating PublisherContactLink object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PublisherContactLinkPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PublisherContactLinkPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PublisherContactLinkPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aPublisher !== null) {
				if ($this->aPublisher->isModified()) {
					$affectedRows += $this->aPublisher->save($con);
				}
				$this->setPublisher($this->aPublisher);
			}

			if ($this->aContact !== null) {
				if ($this->aContact->isModified()) {
					$affectedRows += $this->aContact->save($con);
				}
				$this->setContact($this->aContact);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PublisherContactLinkPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += PublisherContactLinkPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aPublisher !== null) {
				if (!$this->aPublisher->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aPublisher->getValidationFailures());
				}
			}

			if ($this->aContact !== null) {
				if (!$this->aContact->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aContact->getValidationFailures());
				}
			}


			if (($retval = PublisherContactLinkPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PublisherContactLinkPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getContactId();
				break;
			case 2:
				return $this->getPublisherId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PublisherContactLinkPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getContactId(),
			$keys[2] => $this->getPublisherId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PublisherContactLinkPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setContactId($value);
				break;
			case 2:
				$this->setPublisherId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PublisherContactLinkPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setContactId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setPublisherId($arr[$keys[2]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PublisherContactLinkPeer::DATABASE_NAME);

		if ($this->isColumnModified(PublisherContactLinkPeer::ID)) $criteria->add(PublisherContactLinkPeer::ID, $this->id);
		if ($this->isColumnModified(PublisherContactLinkPeer::CONTACT_ID)) $criteria->add(PublisherContactLinkPeer::CONTACT_ID, $this->contact_id);
		if ($this->isColumnModified(PublisherContactLinkPeer::PUBLISHER_ID)) $criteria->add(PublisherContactLinkPeer::PUBLISHER_ID, $this->publisher_id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PublisherContactLinkPeer::DATABASE_NAME);

		$criteria->add(PublisherContactLinkPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setContactId($this->contact_id);

		$copyObj->setPublisherId($this->publisher_id);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PublisherContactLinkPeer();
		}
		return self::$peer;
	}

	
	public function setPublisher($v)
	{


		if ($v === null) {
			$this->setPublisherId(NULL);
		} else {
			$this->setPublisherId($v->getId());
		}


		$this->aPublisher = $v;
	}


	
	public function getPublisher($con = null)
	{
		if ($this->aPublisher === null && ($this->publisher_id !== null)) {
						include_once 'lib/model/om/BasePublisherPeer.php';

			$this->aPublisher = PublisherPeer::retrieveByPK($this->publisher_id, $con);

			
		}
		return $this->aPublisher;
	}

	
	public function setContact($v)
	{


		if ($v === null) {
			$this->setContactId(NULL);
		} else {
			$this->setContactId($v->getId());
		}


		$this->aContact = $v;
	}


	
	public function getContact($con = null)
	{
		if ($this->aContact === null && ($this->contact_id !== null)) {
						include_once 'lib/model/om/BaseContactPeer.php';

			$this->aContact = ContactPeer::retrieveByPK($this->contact_id, $con);

			
		}
		return $this->aContact;
	}

} 