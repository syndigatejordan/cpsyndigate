<?php


abstract class BaseImagePathPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'image_path';

	
	const CLASS_DEFAULT = 'lib.model.ImagePath';

	
	const NUM_COLUMNS = 9;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'image_path.ID';

	
	const CONNNECTION_ID = 'image_path.CONNNECTION_ID';

	
	const IMAGE_PATH = 'image_path.IMAGE_PATH';

	
	const IMAGE_TYPE = 'image_path.IMAGE_TYPE';

	
	const IMAGE_CONTAINER_TYPE = 'image_path.IMAGE_CONTAINER_TYPE';

	
	const IMAGE_CONTAINERS_FILE = 'image_path.IMAGE_CONTAINERS_FILE';

	
	const IMAGE_HEAD = 'image_path.IMAGE_HEAD';

	
	const IMAGE_EXTRA = 'image_path.IMAGE_EXTRA';

	
	const IMAGE_FILE_PATTERNS = 'image_path.IMAGE_FILE_PATTERNS';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'ConnnectionId', 'ImagePath', 'ImageType', 'ImageContainerType', 'ImageContainersFile', 'ImageHead', 'ImageExtra', 'ImageFilePatterns', ),
		BasePeer::TYPE_COLNAME => array (ImagePathPeer::ID, ImagePathPeer::CONNNECTION_ID, ImagePathPeer::IMAGE_PATH, ImagePathPeer::IMAGE_TYPE, ImagePathPeer::IMAGE_CONTAINER_TYPE, ImagePathPeer::IMAGE_CONTAINERS_FILE, ImagePathPeer::IMAGE_HEAD, ImagePathPeer::IMAGE_EXTRA, ImagePathPeer::IMAGE_FILE_PATTERNS, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'connnection_id', 'image_path', 'image_type', 'image_container_type', 'image_containers_file', 'image_head', 'image_extra', 'image_file_patterns', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'ConnnectionId' => 1, 'ImagePath' => 2, 'ImageType' => 3, 'ImageContainerType' => 4, 'ImageContainersFile' => 5, 'ImageHead' => 6, 'ImageExtra' => 7, 'ImageFilePatterns' => 8, ),
		BasePeer::TYPE_COLNAME => array (ImagePathPeer::ID => 0, ImagePathPeer::CONNNECTION_ID => 1, ImagePathPeer::IMAGE_PATH => 2, ImagePathPeer::IMAGE_TYPE => 3, ImagePathPeer::IMAGE_CONTAINER_TYPE => 4, ImagePathPeer::IMAGE_CONTAINERS_FILE => 5, ImagePathPeer::IMAGE_HEAD => 6, ImagePathPeer::IMAGE_EXTRA => 7, ImagePathPeer::IMAGE_FILE_PATTERNS => 8, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'connnection_id' => 1, 'image_path' => 2, 'image_type' => 3, 'image_container_type' => 4, 'image_containers_file' => 5, 'image_head' => 6, 'image_extra' => 7, 'image_file_patterns' => 8, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/ImagePathMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.ImagePathMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ImagePathPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ImagePathPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ImagePathPeer::ID);

		$criteria->addSelectColumn(ImagePathPeer::CONNNECTION_ID);

		$criteria->addSelectColumn(ImagePathPeer::IMAGE_PATH);

		$criteria->addSelectColumn(ImagePathPeer::IMAGE_TYPE);

		$criteria->addSelectColumn(ImagePathPeer::IMAGE_CONTAINER_TYPE);

		$criteria->addSelectColumn(ImagePathPeer::IMAGE_CONTAINERS_FILE);

		$criteria->addSelectColumn(ImagePathPeer::IMAGE_HEAD);

		$criteria->addSelectColumn(ImagePathPeer::IMAGE_EXTRA);

		$criteria->addSelectColumn(ImagePathPeer::IMAGE_FILE_PATTERNS);

	}

	const COUNT = 'COUNT(image_path.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT image_path.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ImagePathPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ImagePathPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ImagePathPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ImagePathPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ImagePathPeer::populateObjects(ImagePathPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ImagePathPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ImagePathPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinTitleConnection(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ImagePathPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ImagePathPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ImagePathPeer::CONNNECTION_ID, TitleConnectionPeer::ID);

		$rs = ImagePathPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinTitleConnection(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ImagePathPeer::addSelectColumns($c);
		$startcol = (ImagePathPeer::NUM_COLUMNS - ImagePathPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		TitleConnectionPeer::addSelectColumns($c);

		$c->addJoin(ImagePathPeer::CONNNECTION_ID, TitleConnectionPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ImagePathPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = TitleConnectionPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getTitleConnection(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addImagePath($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initImagePaths();
				$obj2->addImagePath($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ImagePathPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ImagePathPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ImagePathPeer::CONNNECTION_ID, TitleConnectionPeer::ID);

		$rs = ImagePathPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ImagePathPeer::addSelectColumns($c);
		$startcol2 = (ImagePathPeer::NUM_COLUMNS - ImagePathPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		TitleConnectionPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + TitleConnectionPeer::NUM_COLUMNS;

		$c->addJoin(ImagePathPeer::CONNNECTION_ID, TitleConnectionPeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ImagePathPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = TitleConnectionPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getTitleConnection(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addImagePath($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initImagePaths();
				$obj2->addImagePath($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ImagePathPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(ImagePathPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(ImagePathPeer::ID);
			$selectCriteria->add(ImagePathPeer::ID, $criteria->remove(ImagePathPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(ImagePathPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ImagePathPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof ImagePath) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(ImagePathPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(ImagePath $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ImagePathPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ImagePathPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ImagePathPeer::DATABASE_NAME, ImagePathPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ImagePathPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(ImagePathPeer::DATABASE_NAME);

		$criteria->add(ImagePathPeer::ID, $pk);


		$v = ImagePathPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(ImagePathPeer::ID, $pks, Criteria::IN);
			$objs = ImagePathPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseImagePathPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/ImagePathMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.ImagePathMapBuilder');
}
