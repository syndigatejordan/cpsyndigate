<?php


abstract class BaseReport extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $title_id = 0;


	
	protected $revision_num = 0;


	
	protected $report_gather_id = 0;


	
	protected $report_parse_id = 0;


	
	protected $report_generate_id = 0;


	
	protected $report_send_id = 0;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getTitleId()
	{

		return $this->title_id;
	}

	
	public function getRevisionNum()
	{

		return $this->revision_num;
	}

	
	public function getReportGatherId()
	{

		return $this->report_gather_id;
	}

	
	public function getReportParseId()
	{

		return $this->report_parse_id;
	}

	
	public function getReportGenerateId()
	{

		return $this->report_generate_id;
	}

	
	public function getReportSendId()
	{

		return $this->report_send_id;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ReportPeer::ID;
		}

	} 
	
	public function setTitleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_id !== $v || $v === 0) {
			$this->title_id = $v;
			$this->modifiedColumns[] = ReportPeer::TITLE_ID;
		}

	} 
	
	public function setRevisionNum($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->revision_num !== $v || $v === 0) {
			$this->revision_num = $v;
			$this->modifiedColumns[] = ReportPeer::REVISION_NUM;
		}

	} 
	
	public function setReportGatherId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->report_gather_id !== $v || $v === 0) {
			$this->report_gather_id = $v;
			$this->modifiedColumns[] = ReportPeer::REPORT_GATHER_ID;
		}

	} 
	
	public function setReportParseId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->report_parse_id !== $v || $v === 0) {
			$this->report_parse_id = $v;
			$this->modifiedColumns[] = ReportPeer::REPORT_PARSE_ID;
		}

	} 
	
	public function setReportGenerateId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->report_generate_id !== $v || $v === 0) {
			$this->report_generate_id = $v;
			$this->modifiedColumns[] = ReportPeer::REPORT_GENERATE_ID;
		}

	} 
	
	public function setReportSendId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->report_send_id !== $v || $v === 0) {
			$this->report_send_id = $v;
			$this->modifiedColumns[] = ReportPeer::REPORT_SEND_ID;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->title_id = $rs->getInt($startcol + 1);

			$this->revision_num = $rs->getInt($startcol + 2);

			$this->report_gather_id = $rs->getInt($startcol + 3);

			$this->report_parse_id = $rs->getInt($startcol + 4);

			$this->report_generate_id = $rs->getInt($startcol + 5);

			$this->report_send_id = $rs->getInt($startcol + 6);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 7; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Report object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ReportPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ReportPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ReportPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ReportPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ReportPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = ReportPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ReportPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTitleId();
				break;
			case 2:
				return $this->getRevisionNum();
				break;
			case 3:
				return $this->getReportGatherId();
				break;
			case 4:
				return $this->getReportParseId();
				break;
			case 5:
				return $this->getReportGenerateId();
				break;
			case 6:
				return $this->getReportSendId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ReportPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTitleId(),
			$keys[2] => $this->getRevisionNum(),
			$keys[3] => $this->getReportGatherId(),
			$keys[4] => $this->getReportParseId(),
			$keys[5] => $this->getReportGenerateId(),
			$keys[6] => $this->getReportSendId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ReportPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTitleId($value);
				break;
			case 2:
				$this->setRevisionNum($value);
				break;
			case 3:
				$this->setReportGatherId($value);
				break;
			case 4:
				$this->setReportParseId($value);
				break;
			case 5:
				$this->setReportGenerateId($value);
				break;
			case 6:
				$this->setReportSendId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ReportPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTitleId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setRevisionNum($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setReportGatherId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setReportParseId($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setReportGenerateId($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setReportSendId($arr[$keys[6]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ReportPeer::DATABASE_NAME);

		if ($this->isColumnModified(ReportPeer::ID)) $criteria->add(ReportPeer::ID, $this->id);
		if ($this->isColumnModified(ReportPeer::TITLE_ID)) $criteria->add(ReportPeer::TITLE_ID, $this->title_id);
		if ($this->isColumnModified(ReportPeer::REVISION_NUM)) $criteria->add(ReportPeer::REVISION_NUM, $this->revision_num);
		if ($this->isColumnModified(ReportPeer::REPORT_GATHER_ID)) $criteria->add(ReportPeer::REPORT_GATHER_ID, $this->report_gather_id);
		if ($this->isColumnModified(ReportPeer::REPORT_PARSE_ID)) $criteria->add(ReportPeer::REPORT_PARSE_ID, $this->report_parse_id);
		if ($this->isColumnModified(ReportPeer::REPORT_GENERATE_ID)) $criteria->add(ReportPeer::REPORT_GENERATE_ID, $this->report_generate_id);
		if ($this->isColumnModified(ReportPeer::REPORT_SEND_ID)) $criteria->add(ReportPeer::REPORT_SEND_ID, $this->report_send_id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ReportPeer::DATABASE_NAME);

		$criteria->add(ReportPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTitleId($this->title_id);

		$copyObj->setRevisionNum($this->revision_num);

		$copyObj->setReportGatherId($this->report_gather_id);

		$copyObj->setReportParseId($this->report_parse_id);

		$copyObj->setReportGenerateId($this->report_generate_id);

		$copyObj->setReportSendId($this->report_send_id);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ReportPeer();
		}
		return self::$peer;
	}

} 