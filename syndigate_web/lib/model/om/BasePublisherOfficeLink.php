<?php


abstract class BasePublisherOfficeLink extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $office_id;


	
	protected $publisher_id;

	
	protected $aPublisher;

	
	protected $aOffice;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getOfficeId()
	{

		return $this->office_id;
	}

	
	public function getPublisherId()
	{

		return $this->publisher_id;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = PublisherOfficeLinkPeer::ID;
		}

	} 
	
	public function setOfficeId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->office_id !== $v) {
			$this->office_id = $v;
			$this->modifiedColumns[] = PublisherOfficeLinkPeer::OFFICE_ID;
		}

		if ($this->aOffice !== null && $this->aOffice->getId() !== $v) {
			$this->aOffice = null;
		}

	} 
	
	public function setPublisherId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->publisher_id !== $v) {
			$this->publisher_id = $v;
			$this->modifiedColumns[] = PublisherOfficeLinkPeer::PUBLISHER_ID;
		}

		if ($this->aPublisher !== null && $this->aPublisher->getId() !== $v) {
			$this->aPublisher = null;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->office_id = $rs->getInt($startcol + 1);

			$this->publisher_id = $rs->getInt($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 3; 
		} catch (Exception $e) {
			throw new PropelException("Error populating PublisherOfficeLink object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PublisherOfficeLinkPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PublisherOfficeLinkPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PublisherOfficeLinkPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aPublisher !== null) {
				if ($this->aPublisher->isModified()) {
					$affectedRows += $this->aPublisher->save($con);
				}
				$this->setPublisher($this->aPublisher);
			}

			if ($this->aOffice !== null) {
				if ($this->aOffice->isModified()) {
					$affectedRows += $this->aOffice->save($con);
				}
				$this->setOffice($this->aOffice);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PublisherOfficeLinkPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += PublisherOfficeLinkPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aPublisher !== null) {
				if (!$this->aPublisher->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aPublisher->getValidationFailures());
				}
			}

			if ($this->aOffice !== null) {
				if (!$this->aOffice->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aOffice->getValidationFailures());
				}
			}


			if (($retval = PublisherOfficeLinkPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PublisherOfficeLinkPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getOfficeId();
				break;
			case 2:
				return $this->getPublisherId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PublisherOfficeLinkPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getOfficeId(),
			$keys[2] => $this->getPublisherId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PublisherOfficeLinkPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setOfficeId($value);
				break;
			case 2:
				$this->setPublisherId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PublisherOfficeLinkPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setOfficeId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setPublisherId($arr[$keys[2]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PublisherOfficeLinkPeer::DATABASE_NAME);

		if ($this->isColumnModified(PublisherOfficeLinkPeer::ID)) $criteria->add(PublisherOfficeLinkPeer::ID, $this->id);
		if ($this->isColumnModified(PublisherOfficeLinkPeer::OFFICE_ID)) $criteria->add(PublisherOfficeLinkPeer::OFFICE_ID, $this->office_id);
		if ($this->isColumnModified(PublisherOfficeLinkPeer::PUBLISHER_ID)) $criteria->add(PublisherOfficeLinkPeer::PUBLISHER_ID, $this->publisher_id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PublisherOfficeLinkPeer::DATABASE_NAME);

		$criteria->add(PublisherOfficeLinkPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setOfficeId($this->office_id);

		$copyObj->setPublisherId($this->publisher_id);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PublisherOfficeLinkPeer();
		}
		return self::$peer;
	}

	
	public function setPublisher($v)
	{


		if ($v === null) {
			$this->setPublisherId(NULL);
		} else {
			$this->setPublisherId($v->getId());
		}


		$this->aPublisher = $v;
	}


	
	public function getPublisher($con = null)
	{
		if ($this->aPublisher === null && ($this->publisher_id !== null)) {
						include_once 'lib/model/om/BasePublisherPeer.php';

			$this->aPublisher = PublisherPeer::retrieveByPK($this->publisher_id, $con);

			
		}
		return $this->aPublisher;
	}

	
	public function setOffice($v)
	{


		if ($v === null) {
			$this->setOfficeId(NULL);
		} else {
			$this->setOfficeId($v->getId());
		}


		$this->aOffice = $v;
	}


	
	public function getOffice($con = null)
	{
		if ($this->aOffice === null && ($this->office_id !== null)) {
						include_once 'lib/model/om/BaseOfficePeer.php';

			$this->aOffice = OfficePeer::retrieveByPK($this->office_id, $con);

			
		}
		return $this->aOffice;
	}

} 