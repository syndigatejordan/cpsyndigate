<?php


abstract class BaseArticlePeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'article';

	
	const CLASS_DEFAULT = 'lib.model.Article';

	
	const NUM_COLUMNS = 15;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'article.ID';

	
	const IPTC_ID = 'article.IPTC_ID';

	
	const TITLE_ID = 'article.TITLE_ID';

	
	const ARTICLE_ORIGINAL_DATA_ID = 'article.ARTICLE_ORIGINAL_DATA_ID';

	
	const LANGUAGE_ID = 'article.LANGUAGE_ID';

	
	const HEADLINE = 'article.HEADLINE';

	
	const SUMMARY = 'article.SUMMARY';

	
	const BODY = 'article.BODY';

	
	const AUTHOR = 'article.AUTHOR';

	
	const DATE = 'article.DATE';

	
	const PARSED_AT = 'article.PARSED_AT';

	
	const UPDATED_AT = 'article.UPDATED_AT';

	
	const HAS_TIME = 'article.HAS_TIME';

	
	const IS_CALIAS_CALLED = 'article.IS_CALIAS_CALLED';

	
	const SUB_FEED = 'article.SUB_FEED';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'IptcId', 'TitleId', 'ArticleOriginalDataId', 'LanguageId', 'Headline', 'Summary', 'Body', 'Author', 'Date', 'ParsedAt', 'UpdatedAt', 'HasTime', 'IsCaliasCalled', 'SubFeed', ),
		BasePeer::TYPE_COLNAME => array (ArticlePeer::ID, ArticlePeer::IPTC_ID, ArticlePeer::TITLE_ID, ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, ArticlePeer::LANGUAGE_ID, ArticlePeer::HEADLINE, ArticlePeer::SUMMARY, ArticlePeer::BODY, ArticlePeer::AUTHOR, ArticlePeer::DATE, ArticlePeer::PARSED_AT, ArticlePeer::UPDATED_AT, ArticlePeer::HAS_TIME, ArticlePeer::IS_CALIAS_CALLED, ArticlePeer::SUB_FEED, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'iptc_id', 'title_id', 'article_original_data_id', 'language_id', 'headline', 'summary', 'body', 'author', 'date', 'parsed_at', 'updated_at', 'has_time', 'is_Calias_called', 'sub_feed', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'IptcId' => 1, 'TitleId' => 2, 'ArticleOriginalDataId' => 3, 'LanguageId' => 4, 'Headline' => 5, 'Summary' => 6, 'Body' => 7, 'Author' => 8, 'Date' => 9, 'ParsedAt' => 10, 'UpdatedAt' => 11, 'HasTime' => 12, 'IsCaliasCalled' => 13, 'SubFeed' => 14, ),
		BasePeer::TYPE_COLNAME => array (ArticlePeer::ID => 0, ArticlePeer::IPTC_ID => 1, ArticlePeer::TITLE_ID => 2, ArticlePeer::ARTICLE_ORIGINAL_DATA_ID => 3, ArticlePeer::LANGUAGE_ID => 4, ArticlePeer::HEADLINE => 5, ArticlePeer::SUMMARY => 6, ArticlePeer::BODY => 7, ArticlePeer::AUTHOR => 8, ArticlePeer::DATE => 9, ArticlePeer::PARSED_AT => 10, ArticlePeer::UPDATED_AT => 11, ArticlePeer::HAS_TIME => 12, ArticlePeer::IS_CALIAS_CALLED => 13, ArticlePeer::SUB_FEED => 14, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'iptc_id' => 1, 'title_id' => 2, 'article_original_data_id' => 3, 'language_id' => 4, 'headline' => 5, 'summary' => 6, 'body' => 7, 'author' => 8, 'date' => 9, 'parsed_at' => 10, 'updated_at' => 11, 'has_time' => 12, 'is_Calias_called' => 13, 'sub_feed' => 14, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/ArticleMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.ArticleMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ArticlePeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ArticlePeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ArticlePeer::ID);

		$criteria->addSelectColumn(ArticlePeer::IPTC_ID);

		$criteria->addSelectColumn(ArticlePeer::TITLE_ID);

		$criteria->addSelectColumn(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID);

		$criteria->addSelectColumn(ArticlePeer::LANGUAGE_ID);

		$criteria->addSelectColumn(ArticlePeer::HEADLINE);

		$criteria->addSelectColumn(ArticlePeer::SUMMARY);

		$criteria->addSelectColumn(ArticlePeer::BODY);

		$criteria->addSelectColumn(ArticlePeer::AUTHOR);

		$criteria->addSelectColumn(ArticlePeer::DATE);

		$criteria->addSelectColumn(ArticlePeer::PARSED_AT);

		$criteria->addSelectColumn(ArticlePeer::UPDATED_AT);

		$criteria->addSelectColumn(ArticlePeer::HAS_TIME);

		$criteria->addSelectColumn(ArticlePeer::IS_CALIAS_CALLED);

		$criteria->addSelectColumn(ArticlePeer::SUB_FEED);

	}

	const COUNT = 'COUNT(article.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT article.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ArticlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ArticlePeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ArticlePeer::populateObjects(ArticlePeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ArticlePeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ArticlePeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinLanguage(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$rs = ArticlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinArticleOriginalData(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataPeer::ID);

		$rs = ArticlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinIptc(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticlePeer::IPTC_ID, IptcPeer::ID);

		$rs = ArticlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinTitle(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticlePeer::TITLE_ID, TitlePeer::ID);

		$rs = ArticlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinLanguage(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticlePeer::addSelectColumns($c);
		$startcol = (ArticlePeer::NUM_COLUMNS - ArticlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		LanguagePeer::addSelectColumns($c);

		$c->addJoin(ArticlePeer::LANGUAGE_ID, LanguagePeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = LanguagePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getLanguage(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addArticle($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initArticles();
				$obj2->addArticle($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinArticleOriginalData(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticlePeer::addSelectColumns($c);
		$startcol = (ArticlePeer::NUM_COLUMNS - ArticlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		ArticleOriginalDataPeer::addSelectColumns($c);

		$c->addJoin(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = ArticleOriginalDataPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getArticleOriginalData(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addArticle($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initArticles();
				$obj2->addArticle($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinIptc(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticlePeer::addSelectColumns($c);
		$startcol = (ArticlePeer::NUM_COLUMNS - ArticlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		IptcPeer::addSelectColumns($c);

		$c->addJoin(ArticlePeer::IPTC_ID, IptcPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = IptcPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getIptc(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addArticle($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initArticles();
				$obj2->addArticle($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinTitle(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticlePeer::addSelectColumns($c);
		$startcol = (ArticlePeer::NUM_COLUMNS - ArticlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		TitlePeer::addSelectColumns($c);

		$c->addJoin(ArticlePeer::TITLE_ID, TitlePeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = TitlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getTitle(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addArticle($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initArticles();
				$obj2->addArticle($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$criteria->addJoin(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataPeer::ID);

		$criteria->addJoin(ArticlePeer::IPTC_ID, IptcPeer::ID);

		$criteria->addJoin(ArticlePeer::TITLE_ID, TitlePeer::ID);

		$rs = ArticlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticlePeer::addSelectColumns($c);
		$startcol2 = (ArticlePeer::NUM_COLUMNS - ArticlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		LanguagePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + LanguagePeer::NUM_COLUMNS;

		ArticleOriginalDataPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + ArticleOriginalDataPeer::NUM_COLUMNS;

		IptcPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + IptcPeer::NUM_COLUMNS;

		TitlePeer::addSelectColumns($c);
		$startcol6 = $startcol5 + TitlePeer::NUM_COLUMNS;

		$c->addJoin(ArticlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$c->addJoin(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataPeer::ID);

		$c->addJoin(ArticlePeer::IPTC_ID, IptcPeer::ID);

		$c->addJoin(ArticlePeer::TITLE_ID, TitlePeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticlePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = LanguagePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getLanguage(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addArticle($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initArticles();
				$obj2->addArticle($obj1);
			}


					
			$omClass = ArticleOriginalDataPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3 = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getArticleOriginalData(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addArticle($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj3->initArticles();
				$obj3->addArticle($obj1);
			}


					
			$omClass = IptcPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4 = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getIptc(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addArticle($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj4->initArticles();
				$obj4->addArticle($obj1);
			}


					
			$omClass = TitlePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj5 = new $cls();
			$obj5->hydrate($rs, $startcol5);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj5 = $temp_obj1->getTitle(); 				if ($temp_obj5->getPrimaryKey() === $obj5->getPrimaryKey()) {
					$newObject = false;
					$temp_obj5->addArticle($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj5->initArticles();
				$obj5->addArticle($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAllExceptLanguage(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataPeer::ID);

		$criteria->addJoin(ArticlePeer::IPTC_ID, IptcPeer::ID);

		$criteria->addJoin(ArticlePeer::TITLE_ID, TitlePeer::ID);

		$rs = ArticlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptArticleOriginalData(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$criteria->addJoin(ArticlePeer::IPTC_ID, IptcPeer::ID);

		$criteria->addJoin(ArticlePeer::TITLE_ID, TitlePeer::ID);

		$rs = ArticlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptIptc(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$criteria->addJoin(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataPeer::ID);

		$criteria->addJoin(ArticlePeer::TITLE_ID, TitlePeer::ID);

		$rs = ArticlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptTitle(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArticlePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArticlePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ArticlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$criteria->addJoin(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataPeer::ID);

		$criteria->addJoin(ArticlePeer::IPTC_ID, IptcPeer::ID);

		$rs = ArticlePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAllExceptLanguage(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticlePeer::addSelectColumns($c);
		$startcol2 = (ArticlePeer::NUM_COLUMNS - ArticlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		ArticleOriginalDataPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + ArticleOriginalDataPeer::NUM_COLUMNS;

		IptcPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + IptcPeer::NUM_COLUMNS;

		TitlePeer::addSelectColumns($c);
		$startcol5 = $startcol4 + TitlePeer::NUM_COLUMNS;

		$c->addJoin(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataPeer::ID);

		$c->addJoin(ArticlePeer::IPTC_ID, IptcPeer::ID);

		$c->addJoin(ArticlePeer::TITLE_ID, TitlePeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = ArticleOriginalDataPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getArticleOriginalData(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addArticle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initArticles();
				$obj2->addArticle($obj1);
			}

			$omClass = IptcPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getIptc(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addArticle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initArticles();
				$obj3->addArticle($obj1);
			}

			$omClass = TitlePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getTitle(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addArticle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initArticles();
				$obj4->addArticle($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptArticleOriginalData(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticlePeer::addSelectColumns($c);
		$startcol2 = (ArticlePeer::NUM_COLUMNS - ArticlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		LanguagePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + LanguagePeer::NUM_COLUMNS;

		IptcPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + IptcPeer::NUM_COLUMNS;

		TitlePeer::addSelectColumns($c);
		$startcol5 = $startcol4 + TitlePeer::NUM_COLUMNS;

		$c->addJoin(ArticlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$c->addJoin(ArticlePeer::IPTC_ID, IptcPeer::ID);

		$c->addJoin(ArticlePeer::TITLE_ID, TitlePeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = LanguagePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getLanguage(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addArticle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initArticles();
				$obj2->addArticle($obj1);
			}

			$omClass = IptcPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getIptc(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addArticle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initArticles();
				$obj3->addArticle($obj1);
			}

			$omClass = TitlePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getTitle(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addArticle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initArticles();
				$obj4->addArticle($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptIptc(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticlePeer::addSelectColumns($c);
		$startcol2 = (ArticlePeer::NUM_COLUMNS - ArticlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		LanguagePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + LanguagePeer::NUM_COLUMNS;

		ArticleOriginalDataPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + ArticleOriginalDataPeer::NUM_COLUMNS;

		TitlePeer::addSelectColumns($c);
		$startcol5 = $startcol4 + TitlePeer::NUM_COLUMNS;

		$c->addJoin(ArticlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$c->addJoin(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataPeer::ID);

		$c->addJoin(ArticlePeer::TITLE_ID, TitlePeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = LanguagePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getLanguage(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addArticle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initArticles();
				$obj2->addArticle($obj1);
			}

			$omClass = ArticleOriginalDataPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getArticleOriginalData(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addArticle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initArticles();
				$obj3->addArticle($obj1);
			}

			$omClass = TitlePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getTitle(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addArticle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initArticles();
				$obj4->addArticle($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptTitle(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ArticlePeer::addSelectColumns($c);
		$startcol2 = (ArticlePeer::NUM_COLUMNS - ArticlePeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		LanguagePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + LanguagePeer::NUM_COLUMNS;

		ArticleOriginalDataPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + ArticleOriginalDataPeer::NUM_COLUMNS;

		IptcPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + IptcPeer::NUM_COLUMNS;

		$c->addJoin(ArticlePeer::LANGUAGE_ID, LanguagePeer::ID);

		$c->addJoin(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, ArticleOriginalDataPeer::ID);

		$c->addJoin(ArticlePeer::IPTC_ID, IptcPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ArticlePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = LanguagePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getLanguage(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addArticle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initArticles();
				$obj2->addArticle($obj1);
			}

			$omClass = ArticleOriginalDataPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getArticleOriginalData(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addArticle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initArticles();
				$obj3->addArticle($obj1);
			}

			$omClass = IptcPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4  = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getIptc(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addArticle($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj4->initArticles();
				$obj4->addArticle($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ArticlePeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(ArticlePeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(ArticlePeer::ID);
			$selectCriteria->add(ArticlePeer::ID, $criteria->remove(ArticlePeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(ArticlePeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ArticlePeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Article) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(ArticlePeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Article $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ArticlePeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ArticlePeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ArticlePeer::DATABASE_NAME, ArticlePeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ArticlePeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(ArticlePeer::DATABASE_NAME);

		$criteria->add(ArticlePeer::ID, $pk);


		$v = ArticlePeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(ArticlePeer::ID, $pks, Criteria::IN);
			$objs = ArticlePeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseArticlePeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/ArticleMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.ArticleMapBuilder');
}
