<?php


abstract class BaseLogParse extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $report_parse_id;


	
	protected $category;


	
	protected $message;


	
	protected $severity;


	
	protected $source;


	
	protected $time;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getReportParseId()
	{

		return $this->report_parse_id;
	}

	
	public function getCategory()
	{

		return $this->category;
	}

	
	public function getMessage()
	{

		return $this->message;
	}

	
	public function getSeverity()
	{

		return $this->severity;
	}

	
	public function getSource()
	{

		return $this->source;
	}

	
	public function getTime($format = 'Y-m-d H:i:s')
	{

		if ($this->time === null || $this->time === '') {
			return null;
		} elseif (!is_int($this->time)) {
						$ts = strtotime($this->time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [time] as date/time value: " . var_export($this->time, true));
			}
		} else {
			$ts = $this->time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = LogParsePeer::ID;
		}

	} 
	
	public function setReportParseId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->report_parse_id !== $v) {
			$this->report_parse_id = $v;
			$this->modifiedColumns[] = LogParsePeer::REPORT_PARSE_ID;
		}

	} 
	
	public function setCategory($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->category !== $v) {
			$this->category = $v;
			$this->modifiedColumns[] = LogParsePeer::CATEGORY;
		}

	} 
	
	public function setMessage($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->message !== $v) {
			$this->message = $v;
			$this->modifiedColumns[] = LogParsePeer::MESSAGE;
		}

	} 
	
	public function setSeverity($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->severity !== $v) {
			$this->severity = $v;
			$this->modifiedColumns[] = LogParsePeer::SEVERITY;
		}

	} 
	
	public function setSource($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->source !== $v) {
			$this->source = $v;
			$this->modifiedColumns[] = LogParsePeer::SOURCE;
		}

	} 
	
	public function setTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->time !== $ts) {
			$this->time = $ts;
			$this->modifiedColumns[] = LogParsePeer::TIME;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getString($startcol + 0);

			$this->report_parse_id = $rs->getInt($startcol + 1);

			$this->category = $rs->getString($startcol + 2);

			$this->message = $rs->getString($startcol + 3);

			$this->severity = $rs->getString($startcol + 4);

			$this->source = $rs->getString($startcol + 5);

			$this->time = $rs->getTimestamp($startcol + 6, null);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 7; 
		} catch (Exception $e) {
			throw new PropelException("Error populating LogParse object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LogParsePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			LogParsePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LogParsePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = LogParsePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += LogParsePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = LogParsePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LogParsePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getReportParseId();
				break;
			case 2:
				return $this->getCategory();
				break;
			case 3:
				return $this->getMessage();
				break;
			case 4:
				return $this->getSeverity();
				break;
			case 5:
				return $this->getSource();
				break;
			case 6:
				return $this->getTime();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = LogParsePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getReportParseId(),
			$keys[2] => $this->getCategory(),
			$keys[3] => $this->getMessage(),
			$keys[4] => $this->getSeverity(),
			$keys[5] => $this->getSource(),
			$keys[6] => $this->getTime(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LogParsePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setReportParseId($value);
				break;
			case 2:
				$this->setCategory($value);
				break;
			case 3:
				$this->setMessage($value);
				break;
			case 4:
				$this->setSeverity($value);
				break;
			case 5:
				$this->setSource($value);
				break;
			case 6:
				$this->setTime($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = LogParsePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setReportParseId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setCategory($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setMessage($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setSeverity($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setSource($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setTime($arr[$keys[6]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(LogParsePeer::DATABASE_NAME);

		if ($this->isColumnModified(LogParsePeer::ID)) $criteria->add(LogParsePeer::ID, $this->id);
		if ($this->isColumnModified(LogParsePeer::REPORT_PARSE_ID)) $criteria->add(LogParsePeer::REPORT_PARSE_ID, $this->report_parse_id);
		if ($this->isColumnModified(LogParsePeer::CATEGORY)) $criteria->add(LogParsePeer::CATEGORY, $this->category);
		if ($this->isColumnModified(LogParsePeer::MESSAGE)) $criteria->add(LogParsePeer::MESSAGE, $this->message);
		if ($this->isColumnModified(LogParsePeer::SEVERITY)) $criteria->add(LogParsePeer::SEVERITY, $this->severity);
		if ($this->isColumnModified(LogParsePeer::SOURCE)) $criteria->add(LogParsePeer::SOURCE, $this->source);
		if ($this->isColumnModified(LogParsePeer::TIME)) $criteria->add(LogParsePeer::TIME, $this->time);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(LogParsePeer::DATABASE_NAME);

		$criteria->add(LogParsePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setReportParseId($this->report_parse_id);

		$copyObj->setCategory($this->category);

		$copyObj->setMessage($this->message);

		$copyObj->setSeverity($this->severity);

		$copyObj->setSource($this->source);

		$copyObj->setTime($this->time);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new LogParsePeer();
		}
		return self::$peer;
	}

} 