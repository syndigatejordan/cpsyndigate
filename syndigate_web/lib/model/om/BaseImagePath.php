<?php


abstract class BaseImagePath extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $connnection_id;


	
	protected $image_path;


	
	protected $image_type;


	
	protected $image_container_type;


	
	protected $image_containers_file;


	
	protected $image_head;


	
	protected $image_extra;


	
	protected $image_file_patterns;

	
	protected $aTitleConnection;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getConnnectionId()
	{

		return $this->connnection_id;
	}

	
	public function getImagePath()
	{

		return $this->image_path;
	}

	
	public function getImageType()
	{

		return $this->image_type;
	}

	
	public function getImageContainerType()
	{

		return $this->image_container_type;
	}

	
	public function getImageContainersFile()
	{

		return $this->image_containers_file;
	}

	
	public function getImageHead()
	{

		return $this->image_head;
	}

	
	public function getImageExtra()
	{

		return $this->image_extra;
	}

	
	public function getImageFilePatterns()
	{

		return $this->image_file_patterns;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ImagePathPeer::ID;
		}

	} 
	
	public function setConnnectionId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->connnection_id !== $v) {
			$this->connnection_id = $v;
			$this->modifiedColumns[] = ImagePathPeer::CONNNECTION_ID;
		}

		if ($this->aTitleConnection !== null && $this->aTitleConnection->getId() !== $v) {
			$this->aTitleConnection = null;
		}

	} 
	
	public function setImagePath($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->image_path !== $v) {
			$this->image_path = $v;
			$this->modifiedColumns[] = ImagePathPeer::IMAGE_PATH;
		}

	} 
	
	public function setImageType($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->image_type !== $v) {
			$this->image_type = $v;
			$this->modifiedColumns[] = ImagePathPeer::IMAGE_TYPE;
		}

	} 
	
	public function setImageContainerType($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->image_container_type !== $v) {
			$this->image_container_type = $v;
			$this->modifiedColumns[] = ImagePathPeer::IMAGE_CONTAINER_TYPE;
		}

	} 
	
	public function setImageContainersFile($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->image_containers_file !== $v) {
			$this->image_containers_file = $v;
			$this->modifiedColumns[] = ImagePathPeer::IMAGE_CONTAINERS_FILE;
		}

	} 
	
	public function setImageHead($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->image_head !== $v) {
			$this->image_head = $v;
			$this->modifiedColumns[] = ImagePathPeer::IMAGE_HEAD;
		}

	} 
	
	public function setImageExtra($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->image_extra !== $v) {
			$this->image_extra = $v;
			$this->modifiedColumns[] = ImagePathPeer::IMAGE_EXTRA;
		}

	} 
	
	public function setImageFilePatterns($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->image_file_patterns !== $v) {
			$this->image_file_patterns = $v;
			$this->modifiedColumns[] = ImagePathPeer::IMAGE_FILE_PATTERNS;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->connnection_id = $rs->getInt($startcol + 1);

			$this->image_path = $rs->getString($startcol + 2);

			$this->image_type = $rs->getString($startcol + 3);

			$this->image_container_type = $rs->getString($startcol + 4);

			$this->image_containers_file = $rs->getString($startcol + 5);

			$this->image_head = $rs->getString($startcol + 6);

			$this->image_extra = $rs->getString($startcol + 7);

			$this->image_file_patterns = $rs->getString($startcol + 8);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 9; 
		} catch (Exception $e) {
			throw new PropelException("Error populating ImagePath object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ImagePathPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ImagePathPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ImagePathPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aTitleConnection !== null) {
				if ($this->aTitleConnection->isModified()) {
					$affectedRows += $this->aTitleConnection->save($con);
				}
				$this->setTitleConnection($this->aTitleConnection);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ImagePathPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ImagePathPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aTitleConnection !== null) {
				if (!$this->aTitleConnection->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTitleConnection->getValidationFailures());
				}
			}


			if (($retval = ImagePathPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ImagePathPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getConnnectionId();
				break;
			case 2:
				return $this->getImagePath();
				break;
			case 3:
				return $this->getImageType();
				break;
			case 4:
				return $this->getImageContainerType();
				break;
			case 5:
				return $this->getImageContainersFile();
				break;
			case 6:
				return $this->getImageHead();
				break;
			case 7:
				return $this->getImageExtra();
				break;
			case 8:
				return $this->getImageFilePatterns();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ImagePathPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getConnnectionId(),
			$keys[2] => $this->getImagePath(),
			$keys[3] => $this->getImageType(),
			$keys[4] => $this->getImageContainerType(),
			$keys[5] => $this->getImageContainersFile(),
			$keys[6] => $this->getImageHead(),
			$keys[7] => $this->getImageExtra(),
			$keys[8] => $this->getImageFilePatterns(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ImagePathPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setConnnectionId($value);
				break;
			case 2:
				$this->setImagePath($value);
				break;
			case 3:
				$this->setImageType($value);
				break;
			case 4:
				$this->setImageContainerType($value);
				break;
			case 5:
				$this->setImageContainersFile($value);
				break;
			case 6:
				$this->setImageHead($value);
				break;
			case 7:
				$this->setImageExtra($value);
				break;
			case 8:
				$this->setImageFilePatterns($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ImagePathPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setConnnectionId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setImagePath($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setImageType($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setImageContainerType($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setImageContainersFile($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setImageHead($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setImageExtra($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setImageFilePatterns($arr[$keys[8]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ImagePathPeer::DATABASE_NAME);

		if ($this->isColumnModified(ImagePathPeer::ID)) $criteria->add(ImagePathPeer::ID, $this->id);
		if ($this->isColumnModified(ImagePathPeer::CONNNECTION_ID)) $criteria->add(ImagePathPeer::CONNNECTION_ID, $this->connnection_id);
		if ($this->isColumnModified(ImagePathPeer::IMAGE_PATH)) $criteria->add(ImagePathPeer::IMAGE_PATH, $this->image_path);
		if ($this->isColumnModified(ImagePathPeer::IMAGE_TYPE)) $criteria->add(ImagePathPeer::IMAGE_TYPE, $this->image_type);
		if ($this->isColumnModified(ImagePathPeer::IMAGE_CONTAINER_TYPE)) $criteria->add(ImagePathPeer::IMAGE_CONTAINER_TYPE, $this->image_container_type);
		if ($this->isColumnModified(ImagePathPeer::IMAGE_CONTAINERS_FILE)) $criteria->add(ImagePathPeer::IMAGE_CONTAINERS_FILE, $this->image_containers_file);
		if ($this->isColumnModified(ImagePathPeer::IMAGE_HEAD)) $criteria->add(ImagePathPeer::IMAGE_HEAD, $this->image_head);
		if ($this->isColumnModified(ImagePathPeer::IMAGE_EXTRA)) $criteria->add(ImagePathPeer::IMAGE_EXTRA, $this->image_extra);
		if ($this->isColumnModified(ImagePathPeer::IMAGE_FILE_PATTERNS)) $criteria->add(ImagePathPeer::IMAGE_FILE_PATTERNS, $this->image_file_patterns);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ImagePathPeer::DATABASE_NAME);

		$criteria->add(ImagePathPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setConnnectionId($this->connnection_id);

		$copyObj->setImagePath($this->image_path);

		$copyObj->setImageType($this->image_type);

		$copyObj->setImageContainerType($this->image_container_type);

		$copyObj->setImageContainersFile($this->image_containers_file);

		$copyObj->setImageHead($this->image_head);

		$copyObj->setImageExtra($this->image_extra);

		$copyObj->setImageFilePatterns($this->image_file_patterns);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ImagePathPeer();
		}
		return self::$peer;
	}

	
	public function setTitleConnection($v)
	{


		if ($v === null) {
			$this->setConnnectionId(NULL);
		} else {
			$this->setConnnectionId($v->getId());
		}


		$this->aTitleConnection = $v;
	}


	
	public function getTitleConnection($con = null)
	{
		if ($this->aTitleConnection === null && ($this->connnection_id !== null)) {
						include_once 'lib/model/om/BaseTitleConnectionPeer.php';

			$this->aTitleConnection = TitleConnectionPeer::retrieveByPK($this->connnection_id, $con);

			
		}
		return $this->aTitleConnection;
	}

} 