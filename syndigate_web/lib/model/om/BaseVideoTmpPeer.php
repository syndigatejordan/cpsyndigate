<?php


abstract class BaseVideoTmpPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'video_tmp';

	
	const CLASS_DEFAULT = 'lib.model.VideoTmp';

	
	const NUM_COLUMNS = 9;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'video_tmp.ID';

	
	const ARTICLE_ID = 'video_tmp.ARTICLE_ID';

	
	const VIDEO_NAME = 'video_tmp.VIDEO_NAME';

	
	const VIDEO_CAPTION = 'video_tmp.VIDEO_CAPTION';

	
	const ORIGINAL_NAME = 'video_tmp.ORIGINAL_NAME';

	
	const VIDEO_TYPE = 'video_tmp.VIDEO_TYPE';

	
	const BIT_RATE = 'video_tmp.BIT_RATE';

	
	const ADDED_TIME = 'video_tmp.ADDED_TIME';

	
	const MIME_TYPE = 'video_tmp.MIME_TYPE';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'ArticleId', 'VideoName', 'VideoCaption', 'OriginalName', 'VideoType', 'BitRate', 'AddedTime', 'MimeType', ),
		BasePeer::TYPE_COLNAME => array (VideoTmpPeer::ID, VideoTmpPeer::ARTICLE_ID, VideoTmpPeer::VIDEO_NAME, VideoTmpPeer::VIDEO_CAPTION, VideoTmpPeer::ORIGINAL_NAME, VideoTmpPeer::VIDEO_TYPE, VideoTmpPeer::BIT_RATE, VideoTmpPeer::ADDED_TIME, VideoTmpPeer::MIME_TYPE, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'article_id', 'video_name', 'video_caption', 'original_name', 'video_type', 'bit_rate', 'added_time', 'mime_type', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'ArticleId' => 1, 'VideoName' => 2, 'VideoCaption' => 3, 'OriginalName' => 4, 'VideoType' => 5, 'BitRate' => 6, 'AddedTime' => 7, 'MimeType' => 8, ),
		BasePeer::TYPE_COLNAME => array (VideoTmpPeer::ID => 0, VideoTmpPeer::ARTICLE_ID => 1, VideoTmpPeer::VIDEO_NAME => 2, VideoTmpPeer::VIDEO_CAPTION => 3, VideoTmpPeer::ORIGINAL_NAME => 4, VideoTmpPeer::VIDEO_TYPE => 5, VideoTmpPeer::BIT_RATE => 6, VideoTmpPeer::ADDED_TIME => 7, VideoTmpPeer::MIME_TYPE => 8, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'article_id' => 1, 'video_name' => 2, 'video_caption' => 3, 'original_name' => 4, 'video_type' => 5, 'bit_rate' => 6, 'added_time' => 7, 'mime_type' => 8, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/VideoTmpMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.VideoTmpMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = VideoTmpPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(VideoTmpPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(VideoTmpPeer::ID);

		$criteria->addSelectColumn(VideoTmpPeer::ARTICLE_ID);

		$criteria->addSelectColumn(VideoTmpPeer::VIDEO_NAME);

		$criteria->addSelectColumn(VideoTmpPeer::VIDEO_CAPTION);

		$criteria->addSelectColumn(VideoTmpPeer::ORIGINAL_NAME);

		$criteria->addSelectColumn(VideoTmpPeer::VIDEO_TYPE);

		$criteria->addSelectColumn(VideoTmpPeer::BIT_RATE);

		$criteria->addSelectColumn(VideoTmpPeer::ADDED_TIME);

		$criteria->addSelectColumn(VideoTmpPeer::MIME_TYPE);

	}

	const COUNT = 'COUNT(video_tmp.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT video_tmp.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(VideoTmpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(VideoTmpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = VideoTmpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = VideoTmpPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return VideoTmpPeer::populateObjects(VideoTmpPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			VideoTmpPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = VideoTmpPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinArticleTmp(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(VideoTmpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(VideoTmpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(VideoTmpPeer::ARTICLE_ID, ArticleTmpPeer::ID);

		$rs = VideoTmpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinArticleTmp(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		VideoTmpPeer::addSelectColumns($c);
		$startcol = (VideoTmpPeer::NUM_COLUMNS - VideoTmpPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		ArticleTmpPeer::addSelectColumns($c);

		$c->addJoin(VideoTmpPeer::ARTICLE_ID, ArticleTmpPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = VideoTmpPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = ArticleTmpPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getArticleTmp(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addVideoTmp($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initVideoTmps();
				$obj2->addVideoTmp($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(VideoTmpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(VideoTmpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(VideoTmpPeer::ARTICLE_ID, ArticleTmpPeer::ID);

		$rs = VideoTmpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		VideoTmpPeer::addSelectColumns($c);
		$startcol2 = (VideoTmpPeer::NUM_COLUMNS - VideoTmpPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		ArticleTmpPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + ArticleTmpPeer::NUM_COLUMNS;

		$c->addJoin(VideoTmpPeer::ARTICLE_ID, ArticleTmpPeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = VideoTmpPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = ArticleTmpPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getArticleTmp(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addVideoTmp($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initVideoTmps();
				$obj2->addVideoTmp($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return VideoTmpPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(VideoTmpPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(VideoTmpPeer::ID);
			$selectCriteria->add(VideoTmpPeer::ID, $criteria->remove(VideoTmpPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(VideoTmpPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(VideoTmpPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof VideoTmp) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(VideoTmpPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(VideoTmp $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(VideoTmpPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(VideoTmpPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(VideoTmpPeer::DATABASE_NAME, VideoTmpPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = VideoTmpPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(VideoTmpPeer::DATABASE_NAME);

		$criteria->add(VideoTmpPeer::ID, $pk);


		$v = VideoTmpPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(VideoTmpPeer::ID, $pks, Criteria::IN);
			$objs = VideoTmpPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseVideoTmpPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/VideoTmpMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.VideoTmpMapBuilder');
}
