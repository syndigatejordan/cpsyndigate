<?php


abstract class BaseGatherCrontab extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $title;


	
	protected $description;


	
	protected $script;


	
	protected $concurrent;


	
	protected $implementation_id;


	
	protected $cron_definition;


	
	protected $last_actual_timestamp = 0;


	
	protected $run_once = 0;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getTitle()
	{

		return $this->title;
	}

	
	public function getDescription()
	{

		return $this->description;
	}

	
	public function getScript()
	{

		return $this->script;
	}

	
	public function getConcurrent()
	{

		return $this->concurrent;
	}

	
	public function getImplementationId()
	{

		return $this->implementation_id;
	}

	
	public function getCronDefinition()
	{

		return $this->cron_definition;
	}

	
	public function getLastActualTimestamp()
	{

		return $this->last_actual_timestamp;
	}

	
	public function getRunOnce()
	{

		return $this->run_once;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = GatherCrontabPeer::ID;
		}

	} 
	
	public function setTitle($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title !== $v) {
			$this->title = $v;
			$this->modifiedColumns[] = GatherCrontabPeer::TITLE;
		}

	} 
	
	public function setDescription($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->description !== $v) {
			$this->description = $v;
			$this->modifiedColumns[] = GatherCrontabPeer::DESCRIPTION;
		}

	} 
	
	public function setScript($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->script !== $v) {
			$this->script = $v;
			$this->modifiedColumns[] = GatherCrontabPeer::SCRIPT;
		}

	} 
	
	public function setConcurrent($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->concurrent !== $v) {
			$this->concurrent = $v;
			$this->modifiedColumns[] = GatherCrontabPeer::CONCURRENT;
		}

	} 
	
	public function setImplementationId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->implementation_id !== $v) {
			$this->implementation_id = $v;
			$this->modifiedColumns[] = GatherCrontabPeer::IMPLEMENTATION_ID;
		}

	} 
	
	public function setCronDefinition($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->cron_definition !== $v) {
			$this->cron_definition = $v;
			$this->modifiedColumns[] = GatherCrontabPeer::CRON_DEFINITION;
		}

	} 
	
	public function setLastActualTimestamp($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->last_actual_timestamp !== $v || $v === 0) {
			$this->last_actual_timestamp = $v;
			$this->modifiedColumns[] = GatherCrontabPeer::LAST_ACTUAL_TIMESTAMP;
		}

	} 
	
	public function setRunOnce($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->run_once !== $v || $v === 0) {
			$this->run_once = $v;
			$this->modifiedColumns[] = GatherCrontabPeer::RUN_ONCE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->title = $rs->getInt($startcol + 1);

			$this->description = $rs->getString($startcol + 2);

			$this->script = $rs->getString($startcol + 3);

			$this->concurrent = $rs->getInt($startcol + 4);

			$this->implementation_id = $rs->getInt($startcol + 5);

			$this->cron_definition = $rs->getString($startcol + 6);

			$this->last_actual_timestamp = $rs->getInt($startcol + 7);

			$this->run_once = $rs->getInt($startcol + 8);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 9; 
		} catch (Exception $e) {
			throw new PropelException("Error populating GatherCrontab object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GatherCrontabPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			GatherCrontabPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GatherCrontabPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = GatherCrontabPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += GatherCrontabPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = GatherCrontabPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = GatherCrontabPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTitle();
				break;
			case 2:
				return $this->getDescription();
				break;
			case 3:
				return $this->getScript();
				break;
			case 4:
				return $this->getConcurrent();
				break;
			case 5:
				return $this->getImplementationId();
				break;
			case 6:
				return $this->getCronDefinition();
				break;
			case 7:
				return $this->getLastActualTimestamp();
				break;
			case 8:
				return $this->getRunOnce();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = GatherCrontabPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTitle(),
			$keys[2] => $this->getDescription(),
			$keys[3] => $this->getScript(),
			$keys[4] => $this->getConcurrent(),
			$keys[5] => $this->getImplementationId(),
			$keys[6] => $this->getCronDefinition(),
			$keys[7] => $this->getLastActualTimestamp(),
			$keys[8] => $this->getRunOnce(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = GatherCrontabPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTitle($value);
				break;
			case 2:
				$this->setDescription($value);
				break;
			case 3:
				$this->setScript($value);
				break;
			case 4:
				$this->setConcurrent($value);
				break;
			case 5:
				$this->setImplementationId($value);
				break;
			case 6:
				$this->setCronDefinition($value);
				break;
			case 7:
				$this->setLastActualTimestamp($value);
				break;
			case 8:
				$this->setRunOnce($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = GatherCrontabPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTitle($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setDescription($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setScript($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setConcurrent($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setImplementationId($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setCronDefinition($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setLastActualTimestamp($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setRunOnce($arr[$keys[8]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(GatherCrontabPeer::DATABASE_NAME);

		if ($this->isColumnModified(GatherCrontabPeer::ID)) $criteria->add(GatherCrontabPeer::ID, $this->id);
		if ($this->isColumnModified(GatherCrontabPeer::TITLE)) $criteria->add(GatherCrontabPeer::TITLE, $this->title);
		if ($this->isColumnModified(GatherCrontabPeer::DESCRIPTION)) $criteria->add(GatherCrontabPeer::DESCRIPTION, $this->description);
		if ($this->isColumnModified(GatherCrontabPeer::SCRIPT)) $criteria->add(GatherCrontabPeer::SCRIPT, $this->script);
		if ($this->isColumnModified(GatherCrontabPeer::CONCURRENT)) $criteria->add(GatherCrontabPeer::CONCURRENT, $this->concurrent);
		if ($this->isColumnModified(GatherCrontabPeer::IMPLEMENTATION_ID)) $criteria->add(GatherCrontabPeer::IMPLEMENTATION_ID, $this->implementation_id);
		if ($this->isColumnModified(GatherCrontabPeer::CRON_DEFINITION)) $criteria->add(GatherCrontabPeer::CRON_DEFINITION, $this->cron_definition);
		if ($this->isColumnModified(GatherCrontabPeer::LAST_ACTUAL_TIMESTAMP)) $criteria->add(GatherCrontabPeer::LAST_ACTUAL_TIMESTAMP, $this->last_actual_timestamp);
		if ($this->isColumnModified(GatherCrontabPeer::RUN_ONCE)) $criteria->add(GatherCrontabPeer::RUN_ONCE, $this->run_once);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(GatherCrontabPeer::DATABASE_NAME);

		$criteria->add(GatherCrontabPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTitle($this->title);

		$copyObj->setDescription($this->description);

		$copyObj->setScript($this->script);

		$copyObj->setConcurrent($this->concurrent);

		$copyObj->setImplementationId($this->implementation_id);

		$copyObj->setCronDefinition($this->cron_definition);

		$copyObj->setLastActualTimestamp($this->last_actual_timestamp);

		$copyObj->setRunOnce($this->run_once);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new GatherCrontabPeer();
		}
		return self::$peer;
	}

} 