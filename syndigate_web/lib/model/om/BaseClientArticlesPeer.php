<?php


abstract class BaseClientArticlesPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'client_articles';

	
	const CLASS_DEFAULT = 'lib.model.ClientArticles';

	
	const NUM_COLUMNS = 12;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'client_articles.ID';

	
	const TITLE_ID = 'client_articles.TITLE_ID';

	
	const BODY = 'client_articles.BODY';

	
	const PROVIDERNAME = 'client_articles.PROVIDERNAME';

	
	const CATEGORYNAME = 'client_articles.CATEGORYNAME';

	
	const ORIGINALCATEGORY = 'client_articles.ORIGINALCATEGORY';

	
	const HEADLINEIMAGE = 'client_articles.HEADLINEIMAGE';

	
	const HEADLINEIMAGECAPTION = 'client_articles.HEADLINEIMAGECAPTION';

	
	const ARTICLEDATE = 'client_articles.ARTICLEDATE';

	
	const ARTICLEID = 'client_articles.ARTICLEID';

	
	const GENERATED_TIME = 'client_articles.GENERATED_TIME';

	
	const HEADLINE = 'client_articles.HEADLINE';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'TitleId', 'Body', 'Providername', 'Categoryname', 'Originalcategory', 'Headlineimage', 'Headlineimagecaption', 'Articledate', 'Articleid', 'GeneratedTime', 'Headline', ),
		BasePeer::TYPE_COLNAME => array (ClientArticlesPeer::ID, ClientArticlesPeer::TITLE_ID, ClientArticlesPeer::BODY, ClientArticlesPeer::PROVIDERNAME, ClientArticlesPeer::CATEGORYNAME, ClientArticlesPeer::ORIGINALCATEGORY, ClientArticlesPeer::HEADLINEIMAGE, ClientArticlesPeer::HEADLINEIMAGECAPTION, ClientArticlesPeer::ARTICLEDATE, ClientArticlesPeer::ARTICLEID, ClientArticlesPeer::GENERATED_TIME, ClientArticlesPeer::HEADLINE, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'title_id', 'body', 'providerName', 'categoryName', 'originalCategory', 'headlineImage', 'headlineImageCaption', 'articleDate', 'articleId', 'generated_time', 'headline', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'TitleId' => 1, 'Body' => 2, 'Providername' => 3, 'Categoryname' => 4, 'Originalcategory' => 5, 'Headlineimage' => 6, 'Headlineimagecaption' => 7, 'Articledate' => 8, 'Articleid' => 9, 'GeneratedTime' => 10, 'Headline' => 11, ),
		BasePeer::TYPE_COLNAME => array (ClientArticlesPeer::ID => 0, ClientArticlesPeer::TITLE_ID => 1, ClientArticlesPeer::BODY => 2, ClientArticlesPeer::PROVIDERNAME => 3, ClientArticlesPeer::CATEGORYNAME => 4, ClientArticlesPeer::ORIGINALCATEGORY => 5, ClientArticlesPeer::HEADLINEIMAGE => 6, ClientArticlesPeer::HEADLINEIMAGECAPTION => 7, ClientArticlesPeer::ARTICLEDATE => 8, ClientArticlesPeer::ARTICLEID => 9, ClientArticlesPeer::GENERATED_TIME => 10, ClientArticlesPeer::HEADLINE => 11, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'title_id' => 1, 'body' => 2, 'providerName' => 3, 'categoryName' => 4, 'originalCategory' => 5, 'headlineImage' => 6, 'headlineImageCaption' => 7, 'articleDate' => 8, 'articleId' => 9, 'generated_time' => 10, 'headline' => 11, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/ClientArticlesMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.ClientArticlesMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ClientArticlesPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ClientArticlesPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ClientArticlesPeer::ID);

		$criteria->addSelectColumn(ClientArticlesPeer::TITLE_ID);

		$criteria->addSelectColumn(ClientArticlesPeer::BODY);

		$criteria->addSelectColumn(ClientArticlesPeer::PROVIDERNAME);

		$criteria->addSelectColumn(ClientArticlesPeer::CATEGORYNAME);

		$criteria->addSelectColumn(ClientArticlesPeer::ORIGINALCATEGORY);

		$criteria->addSelectColumn(ClientArticlesPeer::HEADLINEIMAGE);

		$criteria->addSelectColumn(ClientArticlesPeer::HEADLINEIMAGECAPTION);

		$criteria->addSelectColumn(ClientArticlesPeer::ARTICLEDATE);

		$criteria->addSelectColumn(ClientArticlesPeer::ARTICLEID);

		$criteria->addSelectColumn(ClientArticlesPeer::GENERATED_TIME);

		$criteria->addSelectColumn(ClientArticlesPeer::HEADLINE);

	}

	const COUNT = 'COUNT(client_articles.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT client_articles.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ClientArticlesPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ClientArticlesPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ClientArticlesPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ClientArticlesPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ClientArticlesPeer::populateObjects(ClientArticlesPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ClientArticlesPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ClientArticlesPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ClientArticlesPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(ClientArticlesPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(ClientArticlesPeer::ID);
			$selectCriteria->add(ClientArticlesPeer::ID, $criteria->remove(ClientArticlesPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(ClientArticlesPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ClientArticlesPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof ClientArticles) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(ClientArticlesPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(ClientArticles $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ClientArticlesPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ClientArticlesPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ClientArticlesPeer::DATABASE_NAME, ClientArticlesPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ClientArticlesPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(ClientArticlesPeer::DATABASE_NAME);

		$criteria->add(ClientArticlesPeer::ID, $pk);


		$v = ClientArticlesPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(ClientArticlesPeer::ID, $pks, Criteria::IN);
			$objs = ClientArticlesPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseClientArticlesPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/ClientArticlesMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.ClientArticlesMapBuilder');
}
