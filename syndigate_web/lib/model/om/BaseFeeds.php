<?php


abstract class BaseFeeds extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $syndigateid;


	
	protected $destination;


	
	protected $processtime;


	
	protected $sent = 'no';

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getSyndigateid()
	{

		return $this->syndigateid;
	}

	
	public function getDestination()
	{

		return $this->destination;
	}

	
	public function getProcesstime($format = 'Y-m-d H:i:s')
	{

		if ($this->processtime === null || $this->processtime === '') {
			return null;
		} elseif (!is_int($this->processtime)) {
						$ts = strtotime($this->processtime);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [processtime] as date/time value: " . var_export($this->processtime, true));
			}
		} else {
			$ts = $this->processtime;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getSent()
	{

		return $this->sent;
	}

	
	public function setSyndigateid($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->syndigateid !== $v) {
			$this->syndigateid = $v;
			$this->modifiedColumns[] = FeedsPeer::SYNDIGATEID;
		}

	} 
	
	public function setDestination($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->destination !== $v) {
			$this->destination = $v;
			$this->modifiedColumns[] = FeedsPeer::DESTINATION;
		}

	} 
	
	public function setProcesstime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [processtime] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->processtime !== $ts) {
			$this->processtime = $ts;
			$this->modifiedColumns[] = FeedsPeer::PROCESSTIME;
		}

	} 
	
	public function setSent($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->sent !== $v || $v === 'no') {
			$this->sent = $v;
			$this->modifiedColumns[] = FeedsPeer::SENT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->syndigateid = $rs->getInt($startcol + 0);

			$this->destination = $rs->getInt($startcol + 1);

			$this->processtime = $rs->getTimestamp($startcol + 2, null);

			$this->sent = $rs->getString($startcol + 3);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 4; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Feeds object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(FeedsPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			FeedsPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(FeedsPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = FeedsPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += FeedsPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = FeedsPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = FeedsPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getSyndigateid();
				break;
			case 1:
				return $this->getDestination();
				break;
			case 2:
				return $this->getProcesstime();
				break;
			case 3:
				return $this->getSent();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = FeedsPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getSyndigateid(),
			$keys[1] => $this->getDestination(),
			$keys[2] => $this->getProcesstime(),
			$keys[3] => $this->getSent(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = FeedsPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setSyndigateid($value);
				break;
			case 1:
				$this->setDestination($value);
				break;
			case 2:
				$this->setProcesstime($value);
				break;
			case 3:
				$this->setSent($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = FeedsPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setSyndigateid($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setDestination($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setProcesstime($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setSent($arr[$keys[3]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(FeedsPeer::DATABASE_NAME);

		if ($this->isColumnModified(FeedsPeer::SYNDIGATEID)) $criteria->add(FeedsPeer::SYNDIGATEID, $this->syndigateid);
		if ($this->isColumnModified(FeedsPeer::DESTINATION)) $criteria->add(FeedsPeer::DESTINATION, $this->destination);
		if ($this->isColumnModified(FeedsPeer::PROCESSTIME)) $criteria->add(FeedsPeer::PROCESSTIME, $this->processtime);
		if ($this->isColumnModified(FeedsPeer::SENT)) $criteria->add(FeedsPeer::SENT, $this->sent);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(FeedsPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setSyndigateid($this->syndigateid);

		$copyObj->setDestination($this->destination);

		$copyObj->setProcesstime($this->processtime);

		$copyObj->setSent($this->sent);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new FeedsPeer();
		}
		return self::$peer;
	}

} 