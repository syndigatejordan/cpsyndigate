<?php


abstract class BaseArticleArchive extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $iptc_id;


	
	protected $title_id;


	
	protected $article_original_data_id;


	
	protected $language_id;


	
	protected $headline;


	
	protected $summary;


	
	protected $body;


	
	protected $author;


	
	protected $date;


	
	protected $parsed_at;


	
	protected $updated_at;


	
	protected $has_time;


	
	protected $is_calias_called = 0;


	
	protected $sub_feed = '';

	
	protected $aLanguage;

	
	protected $aArticleOriginalDataArchive;

	
	protected $aIptc;

	
	protected $aTitle;

	
	protected $collImageArchives;

	
	protected $lastImageArchiveCriteria = null;

	
	protected $collVideoArchives;

	
	protected $lastVideoArchiveCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getIptcId()
	{

		return $this->iptc_id;
	}

	
	public function getTitleId()
	{

		return $this->title_id;
	}

	
	public function getArticleOriginalDataId()
	{

		return $this->article_original_data_id;
	}

	
	public function getLanguageId()
	{

		return $this->language_id;
	}

	
	public function getHeadline()
	{

		return $this->headline;
	}

	
	public function getSummary()
	{

		return $this->summary;
	}

	
	public function getBody()
	{

		return $this->body;
	}

	
	public function getAuthor()
	{

		return $this->author;
	}

	
	public function getDate($format = 'Y-m-d')
	{

		if ($this->date === null || $this->date === '') {
			return null;
		} elseif (!is_int($this->date)) {
						$ts = strtotime($this->date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [date] as date/time value: " . var_export($this->date, true));
			}
		} else {
			$ts = $this->date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getParsedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->parsed_at === null || $this->parsed_at === '') {
			return null;
		} elseif (!is_int($this->parsed_at)) {
						$ts = strtotime($this->parsed_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [parsed_at] as date/time value: " . var_export($this->parsed_at, true));
			}
		} else {
			$ts = $this->parsed_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getHasTime()
	{

		return $this->has_time;
	}

	
	public function getIsCaliasCalled()
	{

		return $this->is_calias_called;
	}

	
	public function getSubFeed()
	{

		return $this->sub_feed;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ArticleArchivePeer::ID;
		}

	} 
	
	public function setIptcId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->iptc_id !== $v) {
			$this->iptc_id = $v;
			$this->modifiedColumns[] = ArticleArchivePeer::IPTC_ID;
		}

		if ($this->aIptc !== null && $this->aIptc->getId() !== $v) {
			$this->aIptc = null;
		}

	} 
	
	public function setTitleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_id !== $v) {
			$this->title_id = $v;
			$this->modifiedColumns[] = ArticleArchivePeer::TITLE_ID;
		}

		if ($this->aTitle !== null && $this->aTitle->getId() !== $v) {
			$this->aTitle = null;
		}

	} 
	
	public function setArticleOriginalDataId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->article_original_data_id !== $v) {
			$this->article_original_data_id = $v;
			$this->modifiedColumns[] = ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID;
		}

		if ($this->aArticleOriginalDataArchive !== null && $this->aArticleOriginalDataArchive->getId() !== $v) {
			$this->aArticleOriginalDataArchive = null;
		}

	} 
	
	public function setLanguageId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->language_id !== $v) {
			$this->language_id = $v;
			$this->modifiedColumns[] = ArticleArchivePeer::LANGUAGE_ID;
		}

		if ($this->aLanguage !== null && $this->aLanguage->getId() !== $v) {
			$this->aLanguage = null;
		}

	} 
	
	public function setHeadline($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->headline !== $v) {
			$this->headline = $v;
			$this->modifiedColumns[] = ArticleArchivePeer::HEADLINE;
		}

	} 
	
	public function setSummary($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->summary !== $v) {
			$this->summary = $v;
			$this->modifiedColumns[] = ArticleArchivePeer::SUMMARY;
		}

	} 
	
	public function setBody($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->body !== $v) {
			$this->body = $v;
			$this->modifiedColumns[] = ArticleArchivePeer::BODY;
		}

	} 
	
	public function setAuthor($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->author !== $v) {
			$this->author = $v;
			$this->modifiedColumns[] = ArticleArchivePeer::AUTHOR;
		}

	} 
	
	public function setDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->date !== $ts) {
			$this->date = $ts;
			$this->modifiedColumns[] = ArticleArchivePeer::DATE;
		}

	} 
	
	public function setParsedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [parsed_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->parsed_at !== $ts) {
			$this->parsed_at = $ts;
			$this->modifiedColumns[] = ArticleArchivePeer::PARSED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = ArticleArchivePeer::UPDATED_AT;
		}

	} 
	
	public function setHasTime($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->has_time !== $v) {
			$this->has_time = $v;
			$this->modifiedColumns[] = ArticleArchivePeer::HAS_TIME;
		}

	} 
	
	public function setIsCaliasCalled($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->is_calias_called !== $v || $v === 0) {
			$this->is_calias_called = $v;
			$this->modifiedColumns[] = ArticleArchivePeer::IS_CALIAS_CALLED;
		}

	} 
	
	public function setSubFeed($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->sub_feed !== $v || $v === '') {
			$this->sub_feed = $v;
			$this->modifiedColumns[] = ArticleArchivePeer::SUB_FEED;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->iptc_id = $rs->getInt($startcol + 1);

			$this->title_id = $rs->getInt($startcol + 2);

			$this->article_original_data_id = $rs->getInt($startcol + 3);

			$this->language_id = $rs->getInt($startcol + 4);

			$this->headline = $rs->getString($startcol + 5);

			$this->summary = $rs->getString($startcol + 6);

			$this->body = $rs->getString($startcol + 7);

			$this->author = $rs->getString($startcol + 8);

			$this->date = $rs->getDate($startcol + 9, null);

			$this->parsed_at = $rs->getTimestamp($startcol + 10, null);

			$this->updated_at = $rs->getTimestamp($startcol + 11, null);

			$this->has_time = $rs->getInt($startcol + 12);

			$this->is_calias_called = $rs->getInt($startcol + 13);

			$this->sub_feed = $rs->getString($startcol + 14);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 15; 
		} catch (Exception $e) {
			throw new PropelException("Error populating ArticleArchive object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ArticleArchivePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ArticleArchivePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isModified() && !$this->isColumnModified(ArticleArchivePeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ArticleArchivePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aLanguage !== null) {
				if ($this->aLanguage->isModified()) {
					$affectedRows += $this->aLanguage->save($con);
				}
				$this->setLanguage($this->aLanguage);
			}

			if ($this->aArticleOriginalDataArchive !== null) {
				if ($this->aArticleOriginalDataArchive->isModified()) {
					$affectedRows += $this->aArticleOriginalDataArchive->save($con);
				}
				$this->setArticleOriginalDataArchive($this->aArticleOriginalDataArchive);
			}

			if ($this->aIptc !== null) {
				if ($this->aIptc->isModified()) {
					$affectedRows += $this->aIptc->save($con);
				}
				$this->setIptc($this->aIptc);
			}

			if ($this->aTitle !== null) {
				if ($this->aTitle->isModified()) {
					$affectedRows += $this->aTitle->save($con);
				}
				$this->setTitle($this->aTitle);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ArticleArchivePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ArticleArchivePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collImageArchives !== null) {
				foreach($this->collImageArchives as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collVideoArchives !== null) {
				foreach($this->collVideoArchives as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aLanguage !== null) {
				if (!$this->aLanguage->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aLanguage->getValidationFailures());
				}
			}

			if ($this->aArticleOriginalDataArchive !== null) {
				if (!$this->aArticleOriginalDataArchive->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aArticleOriginalDataArchive->getValidationFailures());
				}
			}

			if ($this->aIptc !== null) {
				if (!$this->aIptc->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aIptc->getValidationFailures());
				}
			}

			if ($this->aTitle !== null) {
				if (!$this->aTitle->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTitle->getValidationFailures());
				}
			}


			if (($retval = ArticleArchivePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collImageArchives !== null) {
					foreach($this->collImageArchives as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collVideoArchives !== null) {
					foreach($this->collVideoArchives as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ArticleArchivePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getIptcId();
				break;
			case 2:
				return $this->getTitleId();
				break;
			case 3:
				return $this->getArticleOriginalDataId();
				break;
			case 4:
				return $this->getLanguageId();
				break;
			case 5:
				return $this->getHeadline();
				break;
			case 6:
				return $this->getSummary();
				break;
			case 7:
				return $this->getBody();
				break;
			case 8:
				return $this->getAuthor();
				break;
			case 9:
				return $this->getDate();
				break;
			case 10:
				return $this->getParsedAt();
				break;
			case 11:
				return $this->getUpdatedAt();
				break;
			case 12:
				return $this->getHasTime();
				break;
			case 13:
				return $this->getIsCaliasCalled();
				break;
			case 14:
				return $this->getSubFeed();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ArticleArchivePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getIptcId(),
			$keys[2] => $this->getTitleId(),
			$keys[3] => $this->getArticleOriginalDataId(),
			$keys[4] => $this->getLanguageId(),
			$keys[5] => $this->getHeadline(),
			$keys[6] => $this->getSummary(),
			$keys[7] => $this->getBody(),
			$keys[8] => $this->getAuthor(),
			$keys[9] => $this->getDate(),
			$keys[10] => $this->getParsedAt(),
			$keys[11] => $this->getUpdatedAt(),
			$keys[12] => $this->getHasTime(),
			$keys[13] => $this->getIsCaliasCalled(),
			$keys[14] => $this->getSubFeed(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ArticleArchivePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setIptcId($value);
				break;
			case 2:
				$this->setTitleId($value);
				break;
			case 3:
				$this->setArticleOriginalDataId($value);
				break;
			case 4:
				$this->setLanguageId($value);
				break;
			case 5:
				$this->setHeadline($value);
				break;
			case 6:
				$this->setSummary($value);
				break;
			case 7:
				$this->setBody($value);
				break;
			case 8:
				$this->setAuthor($value);
				break;
			case 9:
				$this->setDate($value);
				break;
			case 10:
				$this->setParsedAt($value);
				break;
			case 11:
				$this->setUpdatedAt($value);
				break;
			case 12:
				$this->setHasTime($value);
				break;
			case 13:
				$this->setIsCaliasCalled($value);
				break;
			case 14:
				$this->setSubFeed($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ArticleArchivePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setIptcId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setTitleId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setArticleOriginalDataId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setLanguageId($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setHeadline($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setSummary($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setBody($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setAuthor($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setDate($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setParsedAt($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setUpdatedAt($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setHasTime($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setIsCaliasCalled($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setSubFeed($arr[$keys[14]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ArticleArchivePeer::DATABASE_NAME);

		if ($this->isColumnModified(ArticleArchivePeer::ID)) $criteria->add(ArticleArchivePeer::ID, $this->id);
		if ($this->isColumnModified(ArticleArchivePeer::IPTC_ID)) $criteria->add(ArticleArchivePeer::IPTC_ID, $this->iptc_id);
		if ($this->isColumnModified(ArticleArchivePeer::TITLE_ID)) $criteria->add(ArticleArchivePeer::TITLE_ID, $this->title_id);
		if ($this->isColumnModified(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID)) $criteria->add(ArticleArchivePeer::ARTICLE_ORIGINAL_DATA_ID, $this->article_original_data_id);
		if ($this->isColumnModified(ArticleArchivePeer::LANGUAGE_ID)) $criteria->add(ArticleArchivePeer::LANGUAGE_ID, $this->language_id);
		if ($this->isColumnModified(ArticleArchivePeer::HEADLINE)) $criteria->add(ArticleArchivePeer::HEADLINE, $this->headline);
		if ($this->isColumnModified(ArticleArchivePeer::SUMMARY)) $criteria->add(ArticleArchivePeer::SUMMARY, $this->summary);
		if ($this->isColumnModified(ArticleArchivePeer::BODY)) $criteria->add(ArticleArchivePeer::BODY, $this->body);
		if ($this->isColumnModified(ArticleArchivePeer::AUTHOR)) $criteria->add(ArticleArchivePeer::AUTHOR, $this->author);
		if ($this->isColumnModified(ArticleArchivePeer::DATE)) $criteria->add(ArticleArchivePeer::DATE, $this->date);
		if ($this->isColumnModified(ArticleArchivePeer::PARSED_AT)) $criteria->add(ArticleArchivePeer::PARSED_AT, $this->parsed_at);
		if ($this->isColumnModified(ArticleArchivePeer::UPDATED_AT)) $criteria->add(ArticleArchivePeer::UPDATED_AT, $this->updated_at);
		if ($this->isColumnModified(ArticleArchivePeer::HAS_TIME)) $criteria->add(ArticleArchivePeer::HAS_TIME, $this->has_time);
		if ($this->isColumnModified(ArticleArchivePeer::IS_CALIAS_CALLED)) $criteria->add(ArticleArchivePeer::IS_CALIAS_CALLED, $this->is_calias_called);
		if ($this->isColumnModified(ArticleArchivePeer::SUB_FEED)) $criteria->add(ArticleArchivePeer::SUB_FEED, $this->sub_feed);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ArticleArchivePeer::DATABASE_NAME);

		$criteria->add(ArticleArchivePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setIptcId($this->iptc_id);

		$copyObj->setTitleId($this->title_id);

		$copyObj->setArticleOriginalDataId($this->article_original_data_id);

		$copyObj->setLanguageId($this->language_id);

		$copyObj->setHeadline($this->headline);

		$copyObj->setSummary($this->summary);

		$copyObj->setBody($this->body);

		$copyObj->setAuthor($this->author);

		$copyObj->setDate($this->date);

		$copyObj->setParsedAt($this->parsed_at);

		$copyObj->setUpdatedAt($this->updated_at);

		$copyObj->setHasTime($this->has_time);

		$copyObj->setIsCaliasCalled($this->is_calias_called);

		$copyObj->setSubFeed($this->sub_feed);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getImageArchives() as $relObj) {
				$copyObj->addImageArchive($relObj->copy($deepCopy));
			}

			foreach($this->getVideoArchives() as $relObj) {
				$copyObj->addVideoArchive($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ArticleArchivePeer();
		}
		return self::$peer;
	}

	
	public function setLanguage($v)
	{


		if ($v === null) {
			$this->setLanguageId(NULL);
		} else {
			$this->setLanguageId($v->getId());
		}


		$this->aLanguage = $v;
	}


	
	public function getLanguage($con = null)
	{
		if ($this->aLanguage === null && ($this->language_id !== null)) {
						include_once 'lib/model/om/BaseLanguagePeer.php';

			$this->aLanguage = LanguagePeer::retrieveByPK($this->language_id, $con);

			
		}
		return $this->aLanguage;
	}

	
	public function setArticleOriginalDataArchive($v)
	{


		if ($v === null) {
			$this->setArticleOriginalDataId(NULL);
		} else {
			$this->setArticleOriginalDataId($v->getId());
		}


		$this->aArticleOriginalDataArchive = $v;
	}


	
	public function getArticleOriginalDataArchive($con = null)
	{
		if ($this->aArticleOriginalDataArchive === null && ($this->article_original_data_id !== null)) {
						include_once 'lib/model/om/BaseArticleOriginalDataArchivePeer.php';

			$this->aArticleOriginalDataArchive = ArticleOriginalDataArchivePeer::retrieveByPK($this->article_original_data_id, $con);

			
		}
		return $this->aArticleOriginalDataArchive;
	}

	
	public function setIptc($v)
	{


		if ($v === null) {
			$this->setIptcId(NULL);
		} else {
			$this->setIptcId($v->getId());
		}


		$this->aIptc = $v;
	}


	
	public function getIptc($con = null)
	{
		if ($this->aIptc === null && ($this->iptc_id !== null)) {
						include_once 'lib/model/om/BaseIptcPeer.php';

			$this->aIptc = IptcPeer::retrieveByPK($this->iptc_id, $con);

			
		}
		return $this->aIptc;
	}

	
	public function setTitle($v)
	{


		if ($v === null) {
			$this->setTitleId(NULL);
		} else {
			$this->setTitleId($v->getId());
		}


		$this->aTitle = $v;
	}


	
	public function getTitle($con = null)
	{
		if ($this->aTitle === null && ($this->title_id !== null)) {
						include_once 'lib/model/om/BaseTitlePeer.php';

			$this->aTitle = TitlePeer::retrieveByPK($this->title_id, $con);

			
		}
		return $this->aTitle;
	}

	
	public function initImageArchives()
	{
		if ($this->collImageArchives === null) {
			$this->collImageArchives = array();
		}
	}

	
	public function getImageArchives($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseImageArchivePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collImageArchives === null) {
			if ($this->isNew()) {
			   $this->collImageArchives = array();
			} else {

				$criteria->add(ImageArchivePeer::ARTICLE_ID, $this->getId());

				ImageArchivePeer::addSelectColumns($criteria);
				$this->collImageArchives = ImageArchivePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ImageArchivePeer::ARTICLE_ID, $this->getId());

				ImageArchivePeer::addSelectColumns($criteria);
				if (!isset($this->lastImageArchiveCriteria) || !$this->lastImageArchiveCriteria->equals($criteria)) {
					$this->collImageArchives = ImageArchivePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastImageArchiveCriteria = $criteria;
		return $this->collImageArchives;
	}

	
	public function countImageArchives($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseImageArchivePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ImageArchivePeer::ARTICLE_ID, $this->getId());

		return ImageArchivePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addImageArchive(ImageArchive $l)
	{
		$this->collImageArchives[] = $l;
		$l->setArticleArchive($this);
	}

	
	public function initVideoArchives()
	{
		if ($this->collVideoArchives === null) {
			$this->collVideoArchives = array();
		}
	}

	
	public function getVideoArchives($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseVideoArchivePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collVideoArchives === null) {
			if ($this->isNew()) {
			   $this->collVideoArchives = array();
			} else {

				$criteria->add(VideoArchivePeer::ARTICLE_ID, $this->getId());

				VideoArchivePeer::addSelectColumns($criteria);
				$this->collVideoArchives = VideoArchivePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(VideoArchivePeer::ARTICLE_ID, $this->getId());

				VideoArchivePeer::addSelectColumns($criteria);
				if (!isset($this->lastVideoArchiveCriteria) || !$this->lastVideoArchiveCriteria->equals($criteria)) {
					$this->collVideoArchives = VideoArchivePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastVideoArchiveCriteria = $criteria;
		return $this->collVideoArchives;
	}

	
	public function countVideoArchives($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseVideoArchivePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(VideoArchivePeer::ARTICLE_ID, $this->getId());

		return VideoArchivePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addVideoArchive(VideoArchive $l)
	{
		$this->collVideoArchives[] = $l;
		$l->setArticleArchive($this);
	}

} 