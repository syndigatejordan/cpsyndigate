<?php


abstract class BaseIptc extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $iptc_english;


	
	protected $iptc_arabic;


	
	protected $iptc_french;


	
	protected $parent_id;

	
	protected $collArticles;

	
	protected $lastArticleCriteria = null;

	
	protected $collCategoryMappings;

	
	protected $lastCategoryMappingCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getIptcEnglish()
	{

		return $this->iptc_english;
	}

	
	public function getIptcArabic()
	{

		return $this->iptc_arabic;
	}

	
	public function getIptcFrench()
	{

		return $this->iptc_french;
	}

	
	public function getParentId()
	{

		return $this->parent_id;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = IptcPeer::ID;
		}

	} 
	
	public function setIptcEnglish($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->iptc_english !== $v) {
			$this->iptc_english = $v;
			$this->modifiedColumns[] = IptcPeer::IPTC_ENGLISH;
		}

	} 
	
	public function setIptcArabic($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->iptc_arabic !== $v) {
			$this->iptc_arabic = $v;
			$this->modifiedColumns[] = IptcPeer::IPTC_ARABIC;
		}

	} 
	
	public function setIptcFrench($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->iptc_french !== $v) {
			$this->iptc_french = $v;
			$this->modifiedColumns[] = IptcPeer::IPTC_FRENCH;
		}

	} 
	
	public function setParentId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->parent_id !== $v) {
			$this->parent_id = $v;
			$this->modifiedColumns[] = IptcPeer::PARENT_ID;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->iptc_english = $rs->getString($startcol + 1);

			$this->iptc_arabic = $rs->getString($startcol + 2);

			$this->iptc_french = $rs->getString($startcol + 3);

			$this->parent_id = $rs->getInt($startcol + 4);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Iptc object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(IptcPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			IptcPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(IptcPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = IptcPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += IptcPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collArticles !== null) {
				foreach($this->collArticles as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collCategoryMappings !== null) {
				foreach($this->collCategoryMappings as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = IptcPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collArticles !== null) {
					foreach($this->collArticles as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collCategoryMappings !== null) {
					foreach($this->collCategoryMappings as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = IptcPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getIptcEnglish();
				break;
			case 2:
				return $this->getIptcArabic();
				break;
			case 3:
				return $this->getIptcFrench();
				break;
			case 4:
				return $this->getParentId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = IptcPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getIptcEnglish(),
			$keys[2] => $this->getIptcArabic(),
			$keys[3] => $this->getIptcFrench(),
			$keys[4] => $this->getParentId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = IptcPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setIptcEnglish($value);
				break;
			case 2:
				$this->setIptcArabic($value);
				break;
			case 3:
				$this->setIptcFrench($value);
				break;
			case 4:
				$this->setParentId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = IptcPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setIptcEnglish($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setIptcArabic($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setIptcFrench($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setParentId($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(IptcPeer::DATABASE_NAME);

		if ($this->isColumnModified(IptcPeer::ID)) $criteria->add(IptcPeer::ID, $this->id);
		if ($this->isColumnModified(IptcPeer::IPTC_ENGLISH)) $criteria->add(IptcPeer::IPTC_ENGLISH, $this->iptc_english);
		if ($this->isColumnModified(IptcPeer::IPTC_ARABIC)) $criteria->add(IptcPeer::IPTC_ARABIC, $this->iptc_arabic);
		if ($this->isColumnModified(IptcPeer::IPTC_FRENCH)) $criteria->add(IptcPeer::IPTC_FRENCH, $this->iptc_french);
		if ($this->isColumnModified(IptcPeer::PARENT_ID)) $criteria->add(IptcPeer::PARENT_ID, $this->parent_id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(IptcPeer::DATABASE_NAME);

		$criteria->add(IptcPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setIptcEnglish($this->iptc_english);

		$copyObj->setIptcArabic($this->iptc_arabic);

		$copyObj->setIptcFrench($this->iptc_french);

		$copyObj->setParentId($this->parent_id);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getArticles() as $relObj) {
				$copyObj->addArticle($relObj->copy($deepCopy));
			}

			foreach($this->getCategoryMappings() as $relObj) {
				$copyObj->addCategoryMapping($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new IptcPeer();
		}
		return self::$peer;
	}

	
	public function initArticles()
	{
		if ($this->collArticles === null) {
			$this->collArticles = array();
		}
	}

	
	public function getArticles($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticles === null) {
			if ($this->isNew()) {
			   $this->collArticles = array();
			} else {

				$criteria->add(ArticlePeer::IPTC_ID, $this->getId());

				ArticlePeer::addSelectColumns($criteria);
				$this->collArticles = ArticlePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ArticlePeer::IPTC_ID, $this->getId());

				ArticlePeer::addSelectColumns($criteria);
				if (!isset($this->lastArticleCriteria) || !$this->lastArticleCriteria->equals($criteria)) {
					$this->collArticles = ArticlePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastArticleCriteria = $criteria;
		return $this->collArticles;
	}

	
	public function countArticles($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ArticlePeer::IPTC_ID, $this->getId());

		return ArticlePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addArticle(Article $l)
	{
		$this->collArticles[] = $l;
		$l->setIptc($this);
	}


	
	public function getArticlesJoinLanguage($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticles === null) {
			if ($this->isNew()) {
				$this->collArticles = array();
			} else {

				$criteria->add(ArticlePeer::IPTC_ID, $this->getId());

				$this->collArticles = ArticlePeer::doSelectJoinLanguage($criteria, $con);
			}
		} else {
									
			$criteria->add(ArticlePeer::IPTC_ID, $this->getId());

			if (!isset($this->lastArticleCriteria) || !$this->lastArticleCriteria->equals($criteria)) {
				$this->collArticles = ArticlePeer::doSelectJoinLanguage($criteria, $con);
			}
		}
		$this->lastArticleCriteria = $criteria;

		return $this->collArticles;
	}


	
	public function getArticlesJoinArticleOriginalData($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticles === null) {
			if ($this->isNew()) {
				$this->collArticles = array();
			} else {

				$criteria->add(ArticlePeer::IPTC_ID, $this->getId());

				$this->collArticles = ArticlePeer::doSelectJoinArticleOriginalData($criteria, $con);
			}
		} else {
									
			$criteria->add(ArticlePeer::IPTC_ID, $this->getId());

			if (!isset($this->lastArticleCriteria) || !$this->lastArticleCriteria->equals($criteria)) {
				$this->collArticles = ArticlePeer::doSelectJoinArticleOriginalData($criteria, $con);
			}
		}
		$this->lastArticleCriteria = $criteria;

		return $this->collArticles;
	}


	
	public function getArticlesJoinTitle($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseArticlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collArticles === null) {
			if ($this->isNew()) {
				$this->collArticles = array();
			} else {

				$criteria->add(ArticlePeer::IPTC_ID, $this->getId());

				$this->collArticles = ArticlePeer::doSelectJoinTitle($criteria, $con);
			}
		} else {
									
			$criteria->add(ArticlePeer::IPTC_ID, $this->getId());

			if (!isset($this->lastArticleCriteria) || !$this->lastArticleCriteria->equals($criteria)) {
				$this->collArticles = ArticlePeer::doSelectJoinTitle($criteria, $con);
			}
		}
		$this->lastArticleCriteria = $criteria;

		return $this->collArticles;
	}

	
	public function initCategoryMappings()
	{
		if ($this->collCategoryMappings === null) {
			$this->collCategoryMappings = array();
		}
	}

	
	public function getCategoryMappings($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseCategoryMappingPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collCategoryMappings === null) {
			if ($this->isNew()) {
			   $this->collCategoryMappings = array();
			} else {

				$criteria->add(CategoryMappingPeer::IPTC_ID, $this->getId());

				CategoryMappingPeer::addSelectColumns($criteria);
				$this->collCategoryMappings = CategoryMappingPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(CategoryMappingPeer::IPTC_ID, $this->getId());

				CategoryMappingPeer::addSelectColumns($criteria);
				if (!isset($this->lastCategoryMappingCriteria) || !$this->lastCategoryMappingCriteria->equals($criteria)) {
					$this->collCategoryMappings = CategoryMappingPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastCategoryMappingCriteria = $criteria;
		return $this->collCategoryMappings;
	}

	
	public function countCategoryMappings($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseCategoryMappingPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(CategoryMappingPeer::IPTC_ID, $this->getId());

		return CategoryMappingPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addCategoryMapping(CategoryMapping $l)
	{
		$this->collCategoryMappings[] = $l;
		$l->setIptc($this);
	}


	
	public function getCategoryMappingsJoinOriginalArticleCategory($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseCategoryMappingPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collCategoryMappings === null) {
			if ($this->isNew()) {
				$this->collCategoryMappings = array();
			} else {

				$criteria->add(CategoryMappingPeer::IPTC_ID, $this->getId());

				$this->collCategoryMappings = CategoryMappingPeer::doSelectJoinOriginalArticleCategory($criteria, $con);
			}
		} else {
									
			$criteria->add(CategoryMappingPeer::IPTC_ID, $this->getId());

			if (!isset($this->lastCategoryMappingCriteria) || !$this->lastCategoryMappingCriteria->equals($criteria)) {
				$this->collCategoryMappings = CategoryMappingPeer::doSelectJoinOriginalArticleCategory($criteria, $con);
			}
		}
		$this->lastCategoryMappingCriteria = $criteria;

		return $this->collCategoryMappings;
	}

} 