<?php


abstract class BaseClientConnectionFilePeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'client_connection_file';

	
	const CLASS_DEFAULT = 'lib.model.ClientConnectionFile';

	
	const NUM_COLUMNS = 12;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'client_connection_file.ID';

	
	const CLIENT_ID = 'client_connection_file.CLIENT_ID';

	
	const CLIENT_CONNECTION_ID = 'client_connection_file.CLIENT_CONNECTION_ID';

	
	const TITLE_ID = 'client_connection_file.TITLE_ID';

	
	const REVISION = 'client_connection_file.REVISION';

	
	const LOCAL_FILE = 'client_connection_file.LOCAL_FILE';

	
	const IS_SENT = 'client_connection_file.IS_SENT';

	
	const NUM_RETRY = 'client_connection_file.NUM_RETRY';

	
	const ERR_MSG = 'client_connection_file.ERR_MSG';

	
	const REPORT_ID = 'client_connection_file.REPORT_ID';

	
	const CREATION_TIME = 'client_connection_file.CREATION_TIME';

	
	const SENT_TIME = 'client_connection_file.SENT_TIME';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'ClientId', 'ClientConnectionId', 'TitleId', 'Revision', 'LocalFile', 'IsSent', 'NumRetry', 'ErrMsg', 'ReportId', 'CreationTime', 'SentTime', ),
		BasePeer::TYPE_COLNAME => array (ClientConnectionFilePeer::ID, ClientConnectionFilePeer::CLIENT_ID, ClientConnectionFilePeer::CLIENT_CONNECTION_ID, ClientConnectionFilePeer::TITLE_ID, ClientConnectionFilePeer::REVISION, ClientConnectionFilePeer::LOCAL_FILE, ClientConnectionFilePeer::IS_SENT, ClientConnectionFilePeer::NUM_RETRY, ClientConnectionFilePeer::ERR_MSG, ClientConnectionFilePeer::REPORT_ID, ClientConnectionFilePeer::CREATION_TIME, ClientConnectionFilePeer::SENT_TIME, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'client_id', 'client_connection_id', 'title_id', 'revision', 'local_file', 'is_sent', 'num_retry', 'err_msg', 'report_id', 'creation_time', 'sent_time', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'ClientId' => 1, 'ClientConnectionId' => 2, 'TitleId' => 3, 'Revision' => 4, 'LocalFile' => 5, 'IsSent' => 6, 'NumRetry' => 7, 'ErrMsg' => 8, 'ReportId' => 9, 'CreationTime' => 10, 'SentTime' => 11, ),
		BasePeer::TYPE_COLNAME => array (ClientConnectionFilePeer::ID => 0, ClientConnectionFilePeer::CLIENT_ID => 1, ClientConnectionFilePeer::CLIENT_CONNECTION_ID => 2, ClientConnectionFilePeer::TITLE_ID => 3, ClientConnectionFilePeer::REVISION => 4, ClientConnectionFilePeer::LOCAL_FILE => 5, ClientConnectionFilePeer::IS_SENT => 6, ClientConnectionFilePeer::NUM_RETRY => 7, ClientConnectionFilePeer::ERR_MSG => 8, ClientConnectionFilePeer::REPORT_ID => 9, ClientConnectionFilePeer::CREATION_TIME => 10, ClientConnectionFilePeer::SENT_TIME => 11, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'client_id' => 1, 'client_connection_id' => 2, 'title_id' => 3, 'revision' => 4, 'local_file' => 5, 'is_sent' => 6, 'num_retry' => 7, 'err_msg' => 8, 'report_id' => 9, 'creation_time' => 10, 'sent_time' => 11, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/ClientConnectionFileMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.ClientConnectionFileMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ClientConnectionFilePeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ClientConnectionFilePeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ClientConnectionFilePeer::ID);

		$criteria->addSelectColumn(ClientConnectionFilePeer::CLIENT_ID);

		$criteria->addSelectColumn(ClientConnectionFilePeer::CLIENT_CONNECTION_ID);

		$criteria->addSelectColumn(ClientConnectionFilePeer::TITLE_ID);

		$criteria->addSelectColumn(ClientConnectionFilePeer::REVISION);

		$criteria->addSelectColumn(ClientConnectionFilePeer::LOCAL_FILE);

		$criteria->addSelectColumn(ClientConnectionFilePeer::IS_SENT);

		$criteria->addSelectColumn(ClientConnectionFilePeer::NUM_RETRY);

		$criteria->addSelectColumn(ClientConnectionFilePeer::ERR_MSG);

		$criteria->addSelectColumn(ClientConnectionFilePeer::REPORT_ID);

		$criteria->addSelectColumn(ClientConnectionFilePeer::CREATION_TIME);

		$criteria->addSelectColumn(ClientConnectionFilePeer::SENT_TIME);

	}

	const COUNT = 'COUNT(client_connection_file.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT client_connection_file.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ClientConnectionFilePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ClientConnectionFilePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ClientConnectionFilePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ClientConnectionFilePeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ClientConnectionFilePeer::populateObjects(ClientConnectionFilePeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ClientConnectionFilePeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ClientConnectionFilePeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ClientConnectionFilePeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(ClientConnectionFilePeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(ClientConnectionFilePeer::ID);
			$selectCriteria->add(ClientConnectionFilePeer::ID, $criteria->remove(ClientConnectionFilePeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(ClientConnectionFilePeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ClientConnectionFilePeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof ClientConnectionFile) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(ClientConnectionFilePeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(ClientConnectionFile $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ClientConnectionFilePeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ClientConnectionFilePeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ClientConnectionFilePeer::DATABASE_NAME, ClientConnectionFilePeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ClientConnectionFilePeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(ClientConnectionFilePeer::DATABASE_NAME);

		$criteria->add(ClientConnectionFilePeer::ID, $pk);


		$v = ClientConnectionFilePeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(ClientConnectionFilePeer::ID, $pks, Criteria::IN);
			$objs = ClientConnectionFilePeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseClientConnectionFilePeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/ClientConnectionFileMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.ClientConnectionFileMapBuilder');
}
