<?php


abstract class BaseClientPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'client';

	
	const CLASS_DEFAULT = 'lib.model.Client';

	
	const NUM_COLUMNS = 24;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'client.ID';

	
	const CLIENT_NAME = 'client.CLIENT_NAME';

	
	const URL = 'client.URL';

	
	const CONNECTION_TYPE = 'client.CONNECTION_TYPE';

	
	const HOME_DIR = 'client.HOME_DIR';

	
	const USERNAME = 'client.USERNAME';

	
	const PASSWORD = 'client.PASSWORD';

	
	const FTP_USERNAME = 'client.FTP_USERNAME';

	
	const FTP_PASSWORD = 'client.FTP_PASSWORD';

	
	const DIR_LAYOUT = 'client.DIR_LAYOUT';

	
	const IS_ACTIVE = 'client.IS_ACTIVE';

	
	const LOGO_PATH = 'client.LOGO_PATH';

	
	const FTP_PORT = 'client.FTP_PORT';

	
	const REMOTE_DIR = 'client.REMOTE_DIR';

	
	const CONTRACT = 'client.CONTRACT';

	
	const CONTRACT_START_DATE = 'client.CONTRACT_START_DATE';

	
	const CONTRACT_END_DATE = 'client.CONTRACT_END_DATE';

	
	const RENEWAL_PERIOD = 'client.RENEWAL_PERIOD';

	
	const ROYALTY_RATE = 'client.ROYALTY_RATE';

	
	const NAGIOS_FILE_PATH = 'client.NAGIOS_FILE_PATH';

	
	const ZIPPED = 'client.ZIPPED';

	
	const GENERATE_TYPE = 'client.GENERATE_TYPE';

	
	const NOTES = 'client.NOTES';

	
	const XML_TYPE = 'client.XML_TYPE';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'ClientName', 'Url', 'ConnectionType', 'HomeDir', 'Username', 'Password', 'FtpUsername', 'FtpPassword', 'DirLayout', 'IsActive', 'LogoPath', 'FtpPort', 'RemoteDir', 'Contract', 'ContractStartDate', 'ContractEndDate', 'RenewalPeriod', 'RoyaltyRate', 'NagiosFilePath', 'Zipped', 'GenerateType', 'Notes', 'XmlType', ),
		BasePeer::TYPE_COLNAME => array (ClientPeer::ID, ClientPeer::CLIENT_NAME, ClientPeer::URL, ClientPeer::CONNECTION_TYPE, ClientPeer::HOME_DIR, ClientPeer::USERNAME, ClientPeer::PASSWORD, ClientPeer::FTP_USERNAME, ClientPeer::FTP_PASSWORD, ClientPeer::DIR_LAYOUT, ClientPeer::IS_ACTIVE, ClientPeer::LOGO_PATH, ClientPeer::FTP_PORT, ClientPeer::REMOTE_DIR, ClientPeer::CONTRACT, ClientPeer::CONTRACT_START_DATE, ClientPeer::CONTRACT_END_DATE, ClientPeer::RENEWAL_PERIOD, ClientPeer::ROYALTY_RATE, ClientPeer::NAGIOS_FILE_PATH, ClientPeer::ZIPPED, ClientPeer::GENERATE_TYPE, ClientPeer::NOTES, ClientPeer::XML_TYPE, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'client_name', 'url', 'connection_type', 'home_dir', 'username', 'password', 'ftp_username', 'ftp_password', 'dir_layout', 'is_active', 'logo_path', 'ftp_port', 'remote_dir', 'contract', 'contract_start_date', 'contract_end_date', 'renewal_period', 'royalty_rate', 'nagios_file_path', 'zipped', 'generate_type', 'notes', 'xml_type', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'ClientName' => 1, 'Url' => 2, 'ConnectionType' => 3, 'HomeDir' => 4, 'Username' => 5, 'Password' => 6, 'FtpUsername' => 7, 'FtpPassword' => 8, 'DirLayout' => 9, 'IsActive' => 10, 'LogoPath' => 11, 'FtpPort' => 12, 'RemoteDir' => 13, 'Contract' => 14, 'ContractStartDate' => 15, 'ContractEndDate' => 16, 'RenewalPeriod' => 17, 'RoyaltyRate' => 18, 'NagiosFilePath' => 19, 'Zipped' => 20, 'GenerateType' => 21, 'Notes' => 22, 'XmlType' => 23, ),
		BasePeer::TYPE_COLNAME => array (ClientPeer::ID => 0, ClientPeer::CLIENT_NAME => 1, ClientPeer::URL => 2, ClientPeer::CONNECTION_TYPE => 3, ClientPeer::HOME_DIR => 4, ClientPeer::USERNAME => 5, ClientPeer::PASSWORD => 6, ClientPeer::FTP_USERNAME => 7, ClientPeer::FTP_PASSWORD => 8, ClientPeer::DIR_LAYOUT => 9, ClientPeer::IS_ACTIVE => 10, ClientPeer::LOGO_PATH => 11, ClientPeer::FTP_PORT => 12, ClientPeer::REMOTE_DIR => 13, ClientPeer::CONTRACT => 14, ClientPeer::CONTRACT_START_DATE => 15, ClientPeer::CONTRACT_END_DATE => 16, ClientPeer::RENEWAL_PERIOD => 17, ClientPeer::ROYALTY_RATE => 18, ClientPeer::NAGIOS_FILE_PATH => 19, ClientPeer::ZIPPED => 20, ClientPeer::GENERATE_TYPE => 21, ClientPeer::NOTES => 22, ClientPeer::XML_TYPE => 23, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'client_name' => 1, 'url' => 2, 'connection_type' => 3, 'home_dir' => 4, 'username' => 5, 'password' => 6, 'ftp_username' => 7, 'ftp_password' => 8, 'dir_layout' => 9, 'is_active' => 10, 'logo_path' => 11, 'ftp_port' => 12, 'remote_dir' => 13, 'contract' => 14, 'contract_start_date' => 15, 'contract_end_date' => 16, 'renewal_period' => 17, 'royalty_rate' => 18, 'nagios_file_path' => 19, 'zipped' => 20, 'generate_type' => 21, 'notes' => 22, 'xml_type' => 23, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/ClientMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.ClientMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ClientPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ClientPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ClientPeer::ID);

		$criteria->addSelectColumn(ClientPeer::CLIENT_NAME);

		$criteria->addSelectColumn(ClientPeer::URL);

		$criteria->addSelectColumn(ClientPeer::CONNECTION_TYPE);

		$criteria->addSelectColumn(ClientPeer::HOME_DIR);

		$criteria->addSelectColumn(ClientPeer::USERNAME);

		$criteria->addSelectColumn(ClientPeer::PASSWORD);

		$criteria->addSelectColumn(ClientPeer::FTP_USERNAME);

		$criteria->addSelectColumn(ClientPeer::FTP_PASSWORD);

		$criteria->addSelectColumn(ClientPeer::DIR_LAYOUT);

		$criteria->addSelectColumn(ClientPeer::IS_ACTIVE);

		$criteria->addSelectColumn(ClientPeer::LOGO_PATH);

		$criteria->addSelectColumn(ClientPeer::FTP_PORT);

		$criteria->addSelectColumn(ClientPeer::REMOTE_DIR);

		$criteria->addSelectColumn(ClientPeer::CONTRACT);

		$criteria->addSelectColumn(ClientPeer::CONTRACT_START_DATE);

		$criteria->addSelectColumn(ClientPeer::CONTRACT_END_DATE);

		$criteria->addSelectColumn(ClientPeer::RENEWAL_PERIOD);

		$criteria->addSelectColumn(ClientPeer::ROYALTY_RATE);

		$criteria->addSelectColumn(ClientPeer::NAGIOS_FILE_PATH);

		$criteria->addSelectColumn(ClientPeer::ZIPPED);

		$criteria->addSelectColumn(ClientPeer::GENERATE_TYPE);

		$criteria->addSelectColumn(ClientPeer::NOTES);

		$criteria->addSelectColumn(ClientPeer::XML_TYPE);

	}

	const COUNT = 'COUNT(client.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT client.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ClientPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ClientPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ClientPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ClientPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ClientPeer::populateObjects(ClientPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ClientPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ClientPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ClientPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(ClientPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(ClientPeer::ID);
			$selectCriteria->add(ClientPeer::ID, $criteria->remove(ClientPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(ClientPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ClientPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Client) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(ClientPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Client $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ClientPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ClientPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ClientPeer::DATABASE_NAME, ClientPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ClientPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(ClientPeer::DATABASE_NAME);

		$criteria->add(ClientPeer::ID, $pk);


		$v = ClientPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(ClientPeer::ID, $pks, Criteria::IN);
			$objs = ClientPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseClientPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/ClientMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.ClientMapBuilder');
}
