<?php


abstract class BaseVideoArchivePeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'video_archive';

	
	const CLASS_DEFAULT = 'lib.model.VideoArchive';

	
	const NUM_COLUMNS = 9;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'video_archive.ID';

	
	const ARTICLE_ID = 'video_archive.ARTICLE_ID';

	
	const VIDEO_NAME = 'video_archive.VIDEO_NAME';

	
	const VIDEO_CAPTION = 'video_archive.VIDEO_CAPTION';

	
	const ORIGINAL_NAME = 'video_archive.ORIGINAL_NAME';

	
	const VIDEO_TYPE = 'video_archive.VIDEO_TYPE';

	
	const BIT_RATE = 'video_archive.BIT_RATE';

	
	const ADDED_TIME = 'video_archive.ADDED_TIME';

	
	const MIME_TYPE = 'video_archive.MIME_TYPE';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'ArticleId', 'VideoName', 'VideoCaption', 'OriginalName', 'VideoType', 'BitRate', 'AddedTime', 'MimeType', ),
		BasePeer::TYPE_COLNAME => array (VideoArchivePeer::ID, VideoArchivePeer::ARTICLE_ID, VideoArchivePeer::VIDEO_NAME, VideoArchivePeer::VIDEO_CAPTION, VideoArchivePeer::ORIGINAL_NAME, VideoArchivePeer::VIDEO_TYPE, VideoArchivePeer::BIT_RATE, VideoArchivePeer::ADDED_TIME, VideoArchivePeer::MIME_TYPE, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'article_id', 'video_name', 'video_caption', 'original_name', 'video_type', 'bit_rate', 'added_time', 'mime_type', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'ArticleId' => 1, 'VideoName' => 2, 'VideoCaption' => 3, 'OriginalName' => 4, 'VideoType' => 5, 'BitRate' => 6, 'AddedTime' => 7, 'MimeType' => 8, ),
		BasePeer::TYPE_COLNAME => array (VideoArchivePeer::ID => 0, VideoArchivePeer::ARTICLE_ID => 1, VideoArchivePeer::VIDEO_NAME => 2, VideoArchivePeer::VIDEO_CAPTION => 3, VideoArchivePeer::ORIGINAL_NAME => 4, VideoArchivePeer::VIDEO_TYPE => 5, VideoArchivePeer::BIT_RATE => 6, VideoArchivePeer::ADDED_TIME => 7, VideoArchivePeer::MIME_TYPE => 8, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'article_id' => 1, 'video_name' => 2, 'video_caption' => 3, 'original_name' => 4, 'video_type' => 5, 'bit_rate' => 6, 'added_time' => 7, 'mime_type' => 8, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/VideoArchiveMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.VideoArchiveMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = VideoArchivePeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(VideoArchivePeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(VideoArchivePeer::ID);

		$criteria->addSelectColumn(VideoArchivePeer::ARTICLE_ID);

		$criteria->addSelectColumn(VideoArchivePeer::VIDEO_NAME);

		$criteria->addSelectColumn(VideoArchivePeer::VIDEO_CAPTION);

		$criteria->addSelectColumn(VideoArchivePeer::ORIGINAL_NAME);

		$criteria->addSelectColumn(VideoArchivePeer::VIDEO_TYPE);

		$criteria->addSelectColumn(VideoArchivePeer::BIT_RATE);

		$criteria->addSelectColumn(VideoArchivePeer::ADDED_TIME);

		$criteria->addSelectColumn(VideoArchivePeer::MIME_TYPE);

	}

	const COUNT = 'COUNT(video_archive.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT video_archive.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(VideoArchivePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(VideoArchivePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = VideoArchivePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = VideoArchivePeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return VideoArchivePeer::populateObjects(VideoArchivePeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			VideoArchivePeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = VideoArchivePeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinArticleArchive(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(VideoArchivePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(VideoArchivePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(VideoArchivePeer::ARTICLE_ID, ArticleArchivePeer::ID);

		$rs = VideoArchivePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinArticleArchive(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		VideoArchivePeer::addSelectColumns($c);
		$startcol = (VideoArchivePeer::NUM_COLUMNS - VideoArchivePeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		ArticleArchivePeer::addSelectColumns($c);

		$c->addJoin(VideoArchivePeer::ARTICLE_ID, ArticleArchivePeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = VideoArchivePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = ArticleArchivePeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getArticleArchive(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addVideoArchive($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initVideoArchives();
				$obj2->addVideoArchive($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(VideoArchivePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(VideoArchivePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(VideoArchivePeer::ARTICLE_ID, ArticleArchivePeer::ID);

		$rs = VideoArchivePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		VideoArchivePeer::addSelectColumns($c);
		$startcol2 = (VideoArchivePeer::NUM_COLUMNS - VideoArchivePeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		ArticleArchivePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + ArticleArchivePeer::NUM_COLUMNS;

		$c->addJoin(VideoArchivePeer::ARTICLE_ID, ArticleArchivePeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = VideoArchivePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = ArticleArchivePeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getArticleArchive(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addVideoArchive($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initVideoArchives();
				$obj2->addVideoArchive($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return VideoArchivePeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(VideoArchivePeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(VideoArchivePeer::ID);
			$selectCriteria->add(VideoArchivePeer::ID, $criteria->remove(VideoArchivePeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(VideoArchivePeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(VideoArchivePeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof VideoArchive) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(VideoArchivePeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(VideoArchive $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(VideoArchivePeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(VideoArchivePeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(VideoArchivePeer::DATABASE_NAME, VideoArchivePeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = VideoArchivePeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(VideoArchivePeer::DATABASE_NAME);

		$criteria->add(VideoArchivePeer::ID, $pk);


		$v = VideoArchivePeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(VideoArchivePeer::ID, $pks, Criteria::IN);
			$objs = VideoArchivePeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseVideoArchivePeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/VideoArchiveMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.VideoArchiveMapBuilder');
}
