<?php


abstract class BaseGatherCronjobPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'gather_cronjob';

	
	const CLASS_DEFAULT = 'lib.model.GatherCronjob';

	
	const NUM_COLUMNS = 10;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'gather_cronjob.ID';

	
	const CRONTAB_ID = 'gather_cronjob.CRONTAB_ID';

	
	const START_TIMESTAMP = 'gather_cronjob.START_TIMESTAMP';

	
	const END_TIMESTAMP = 'gather_cronjob.END_TIMESTAMP';

	
	const SCRIPT = 'gather_cronjob.SCRIPT';

	
	const CONCURRENT = 'gather_cronjob.CONCURRENT';

	
	const IMPLEMENTATION_ID = 'gather_cronjob.IMPLEMENTATION_ID';

	
	const RESULTS = 'gather_cronjob.RESULTS';

	
	const PID = 'gather_cronjob.PID';

	
	const IS_EXITED_NORMALLY = 'gather_cronjob.IS_EXITED_NORMALLY';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'CrontabId', 'StartTimestamp', 'EndTimestamp', 'Script', 'Concurrent', 'ImplementationId', 'Results', 'Pid', 'IsExitedNormally', ),
		BasePeer::TYPE_COLNAME => array (GatherCronjobPeer::ID, GatherCronjobPeer::CRONTAB_ID, GatherCronjobPeer::START_TIMESTAMP, GatherCronjobPeer::END_TIMESTAMP, GatherCronjobPeer::SCRIPT, GatherCronjobPeer::CONCURRENT, GatherCronjobPeer::IMPLEMENTATION_ID, GatherCronjobPeer::RESULTS, GatherCronjobPeer::PID, GatherCronjobPeer::IS_EXITED_NORMALLY, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'crontab_id', 'start_timestamp', 'end_timestamp', 'script', 'concurrent', 'implementation_id', 'results', 'pid', 'is_exited_normally', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'CrontabId' => 1, 'StartTimestamp' => 2, 'EndTimestamp' => 3, 'Script' => 4, 'Concurrent' => 5, 'ImplementationId' => 6, 'Results' => 7, 'Pid' => 8, 'IsExitedNormally' => 9, ),
		BasePeer::TYPE_COLNAME => array (GatherCronjobPeer::ID => 0, GatherCronjobPeer::CRONTAB_ID => 1, GatherCronjobPeer::START_TIMESTAMP => 2, GatherCronjobPeer::END_TIMESTAMP => 3, GatherCronjobPeer::SCRIPT => 4, GatherCronjobPeer::CONCURRENT => 5, GatherCronjobPeer::IMPLEMENTATION_ID => 6, GatherCronjobPeer::RESULTS => 7, GatherCronjobPeer::PID => 8, GatherCronjobPeer::IS_EXITED_NORMALLY => 9, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'crontab_id' => 1, 'start_timestamp' => 2, 'end_timestamp' => 3, 'script' => 4, 'concurrent' => 5, 'implementation_id' => 6, 'results' => 7, 'pid' => 8, 'is_exited_normally' => 9, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/GatherCronjobMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.GatherCronjobMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = GatherCronjobPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(GatherCronjobPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(GatherCronjobPeer::ID);

		$criteria->addSelectColumn(GatherCronjobPeer::CRONTAB_ID);

		$criteria->addSelectColumn(GatherCronjobPeer::START_TIMESTAMP);

		$criteria->addSelectColumn(GatherCronjobPeer::END_TIMESTAMP);

		$criteria->addSelectColumn(GatherCronjobPeer::SCRIPT);

		$criteria->addSelectColumn(GatherCronjobPeer::CONCURRENT);

		$criteria->addSelectColumn(GatherCronjobPeer::IMPLEMENTATION_ID);

		$criteria->addSelectColumn(GatherCronjobPeer::RESULTS);

		$criteria->addSelectColumn(GatherCronjobPeer::PID);

		$criteria->addSelectColumn(GatherCronjobPeer::IS_EXITED_NORMALLY);

	}

	const COUNT = 'COUNT(gather_cronjob.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT gather_cronjob.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(GatherCronjobPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(GatherCronjobPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = GatherCronjobPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = GatherCronjobPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return GatherCronjobPeer::populateObjects(GatherCronjobPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			GatherCronjobPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = GatherCronjobPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return GatherCronjobPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(GatherCronjobPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(GatherCronjobPeer::ID);
			$selectCriteria->add(GatherCronjobPeer::ID, $criteria->remove(GatherCronjobPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(GatherCronjobPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(GatherCronjobPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof GatherCronjob) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(GatherCronjobPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(GatherCronjob $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(GatherCronjobPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(GatherCronjobPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(GatherCronjobPeer::DATABASE_NAME, GatherCronjobPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = GatherCronjobPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(GatherCronjobPeer::DATABASE_NAME);

		$criteria->add(GatherCronjobPeer::ID, $pk);


		$v = GatherCronjobPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(GatherCronjobPeer::ID, $pks, Criteria::IN);
			$objs = GatherCronjobPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseGatherCronjobPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/GatherCronjobMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.GatherCronjobMapBuilder');
}
