<?php


abstract class BaseCountry extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $iso;


	
	protected $name;


	
	protected $printable_name;


	
	protected $iso3;


	
	protected $numcode;

	
	protected $collBankDetailss;

	
	protected $lastBankDetailsCriteria = null;

	
	protected $collContacts;

	
	protected $lastContactCriteria = null;

	
	protected $collOffices;

	
	protected $lastOfficeCriteria = null;

	
	protected $collPublishers;

	
	protected $lastPublisherCriteria = null;

	
	protected $collTitles;

	
	protected $lastTitleCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getIso()
	{

		return $this->iso;
	}

	
	public function getName()
	{

		return $this->name;
	}

	
	public function getPrintableName()
	{

		return $this->printable_name;
	}

	
	public function getIso3()
	{

		return $this->iso3;
	}

	
	public function getNumcode()
	{

		return $this->numcode;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = CountryPeer::ID;
		}

	} 
	
	public function setIso($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->iso !== $v) {
			$this->iso = $v;
			$this->modifiedColumns[] = CountryPeer::ISO;
		}

	} 
	
	public function setName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->name !== $v) {
			$this->name = $v;
			$this->modifiedColumns[] = CountryPeer::NAME;
		}

	} 
	
	public function setPrintableName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->printable_name !== $v) {
			$this->printable_name = $v;
			$this->modifiedColumns[] = CountryPeer::PRINTABLE_NAME;
		}

	} 
	
	public function setIso3($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->iso3 !== $v) {
			$this->iso3 = $v;
			$this->modifiedColumns[] = CountryPeer::ISO3;
		}

	} 
	
	public function setNumcode($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->numcode !== $v) {
			$this->numcode = $v;
			$this->modifiedColumns[] = CountryPeer::NUMCODE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->iso = $rs->getString($startcol + 1);

			$this->name = $rs->getString($startcol + 2);

			$this->printable_name = $rs->getString($startcol + 3);

			$this->iso3 = $rs->getString($startcol + 4);

			$this->numcode = $rs->getInt($startcol + 5);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 6; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Country object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(CountryPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			CountryPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(CountryPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = CountryPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += CountryPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collBankDetailss !== null) {
				foreach($this->collBankDetailss as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collContacts !== null) {
				foreach($this->collContacts as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collOffices !== null) {
				foreach($this->collOffices as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collPublishers !== null) {
				foreach($this->collPublishers as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collTitles !== null) {
				foreach($this->collTitles as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = CountryPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collBankDetailss !== null) {
					foreach($this->collBankDetailss as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collContacts !== null) {
					foreach($this->collContacts as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collOffices !== null) {
					foreach($this->collOffices as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collPublishers !== null) {
					foreach($this->collPublishers as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collTitles !== null) {
					foreach($this->collTitles as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = CountryPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getIso();
				break;
			case 2:
				return $this->getName();
				break;
			case 3:
				return $this->getPrintableName();
				break;
			case 4:
				return $this->getIso3();
				break;
			case 5:
				return $this->getNumcode();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = CountryPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getIso(),
			$keys[2] => $this->getName(),
			$keys[3] => $this->getPrintableName(),
			$keys[4] => $this->getIso3(),
			$keys[5] => $this->getNumcode(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = CountryPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setIso($value);
				break;
			case 2:
				$this->setName($value);
				break;
			case 3:
				$this->setPrintableName($value);
				break;
			case 4:
				$this->setIso3($value);
				break;
			case 5:
				$this->setNumcode($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = CountryPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setIso($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setPrintableName($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setIso3($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setNumcode($arr[$keys[5]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(CountryPeer::DATABASE_NAME);

		if ($this->isColumnModified(CountryPeer::ID)) $criteria->add(CountryPeer::ID, $this->id);
		if ($this->isColumnModified(CountryPeer::ISO)) $criteria->add(CountryPeer::ISO, $this->iso);
		if ($this->isColumnModified(CountryPeer::NAME)) $criteria->add(CountryPeer::NAME, $this->name);
		if ($this->isColumnModified(CountryPeer::PRINTABLE_NAME)) $criteria->add(CountryPeer::PRINTABLE_NAME, $this->printable_name);
		if ($this->isColumnModified(CountryPeer::ISO3)) $criteria->add(CountryPeer::ISO3, $this->iso3);
		if ($this->isColumnModified(CountryPeer::NUMCODE)) $criteria->add(CountryPeer::NUMCODE, $this->numcode);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(CountryPeer::DATABASE_NAME);

		$criteria->add(CountryPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setIso($this->iso);

		$copyObj->setName($this->name);

		$copyObj->setPrintableName($this->printable_name);

		$copyObj->setIso3($this->iso3);

		$copyObj->setNumcode($this->numcode);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getBankDetailss() as $relObj) {
				$copyObj->addBankDetails($relObj->copy($deepCopy));
			}

			foreach($this->getContacts() as $relObj) {
				$copyObj->addContact($relObj->copy($deepCopy));
			}

			foreach($this->getOffices() as $relObj) {
				$copyObj->addOffice($relObj->copy($deepCopy));
			}

			foreach($this->getPublishers() as $relObj) {
				$copyObj->addPublisher($relObj->copy($deepCopy));
			}

			foreach($this->getTitles() as $relObj) {
				$copyObj->addTitle($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new CountryPeer();
		}
		return self::$peer;
	}

	
	public function initBankDetailss()
	{
		if ($this->collBankDetailss === null) {
			$this->collBankDetailss = array();
		}
	}

	
	public function getBankDetailss($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseBankDetailsPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collBankDetailss === null) {
			if ($this->isNew()) {
			   $this->collBankDetailss = array();
			} else {

				$criteria->add(BankDetailsPeer::COUNTRY_ID, $this->getId());

				BankDetailsPeer::addSelectColumns($criteria);
				$this->collBankDetailss = BankDetailsPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(BankDetailsPeer::COUNTRY_ID, $this->getId());

				BankDetailsPeer::addSelectColumns($criteria);
				if (!isset($this->lastBankDetailsCriteria) || !$this->lastBankDetailsCriteria->equals($criteria)) {
					$this->collBankDetailss = BankDetailsPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastBankDetailsCriteria = $criteria;
		return $this->collBankDetailss;
	}

	
	public function countBankDetailss($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseBankDetailsPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(BankDetailsPeer::COUNTRY_ID, $this->getId());

		return BankDetailsPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addBankDetails(BankDetails $l)
	{
		$this->collBankDetailss[] = $l;
		$l->setCountry($this);
	}


	
	public function getBankDetailssJoinPublisher($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseBankDetailsPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collBankDetailss === null) {
			if ($this->isNew()) {
				$this->collBankDetailss = array();
			} else {

				$criteria->add(BankDetailsPeer::COUNTRY_ID, $this->getId());

				$this->collBankDetailss = BankDetailsPeer::doSelectJoinPublisher($criteria, $con);
			}
		} else {
									
			$criteria->add(BankDetailsPeer::COUNTRY_ID, $this->getId());

			if (!isset($this->lastBankDetailsCriteria) || !$this->lastBankDetailsCriteria->equals($criteria)) {
				$this->collBankDetailss = BankDetailsPeer::doSelectJoinPublisher($criteria, $con);
			}
		}
		$this->lastBankDetailsCriteria = $criteria;

		return $this->collBankDetailss;
	}

	
	public function initContacts()
	{
		if ($this->collContacts === null) {
			$this->collContacts = array();
		}
	}

	
	public function getContacts($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseContactPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collContacts === null) {
			if ($this->isNew()) {
			   $this->collContacts = array();
			} else {

				$criteria->add(ContactPeer::COUNTRY_ID, $this->getId());

				ContactPeer::addSelectColumns($criteria);
				$this->collContacts = ContactPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ContactPeer::COUNTRY_ID, $this->getId());

				ContactPeer::addSelectColumns($criteria);
				if (!isset($this->lastContactCriteria) || !$this->lastContactCriteria->equals($criteria)) {
					$this->collContacts = ContactPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastContactCriteria = $criteria;
		return $this->collContacts;
	}

	
	public function countContacts($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseContactPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ContactPeer::COUNTRY_ID, $this->getId());

		return ContactPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addContact(Contact $l)
	{
		$this->collContacts[] = $l;
		$l->setCountry($this);
	}

	
	public function initOffices()
	{
		if ($this->collOffices === null) {
			$this->collOffices = array();
		}
	}

	
	public function getOffices($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseOfficePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collOffices === null) {
			if ($this->isNew()) {
			   $this->collOffices = array();
			} else {

				$criteria->add(OfficePeer::COUNTRY_ID, $this->getId());

				OfficePeer::addSelectColumns($criteria);
				$this->collOffices = OfficePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(OfficePeer::COUNTRY_ID, $this->getId());

				OfficePeer::addSelectColumns($criteria);
				if (!isset($this->lastOfficeCriteria) || !$this->lastOfficeCriteria->equals($criteria)) {
					$this->collOffices = OfficePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastOfficeCriteria = $criteria;
		return $this->collOffices;
	}

	
	public function countOffices($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseOfficePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(OfficePeer::COUNTRY_ID, $this->getId());

		return OfficePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addOffice(Office $l)
	{
		$this->collOffices[] = $l;
		$l->setCountry($this);
	}

	
	public function initPublishers()
	{
		if ($this->collPublishers === null) {
			$this->collPublishers = array();
		}
	}

	
	public function getPublishers($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BasePublisherPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collPublishers === null) {
			if ($this->isNew()) {
			   $this->collPublishers = array();
			} else {

				$criteria->add(PublisherPeer::COUNTRY_ID, $this->getId());

				PublisherPeer::addSelectColumns($criteria);
				$this->collPublishers = PublisherPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(PublisherPeer::COUNTRY_ID, $this->getId());

				PublisherPeer::addSelectColumns($criteria);
				if (!isset($this->lastPublisherCriteria) || !$this->lastPublisherCriteria->equals($criteria)) {
					$this->collPublishers = PublisherPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastPublisherCriteria = $criteria;
		return $this->collPublishers;
	}

	
	public function countPublishers($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BasePublisherPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(PublisherPeer::COUNTRY_ID, $this->getId());

		return PublisherPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addPublisher(Publisher $l)
	{
		$this->collPublishers[] = $l;
		$l->setCountry($this);
	}


	
	public function getPublishersJoinAdmins($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BasePublisherPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collPublishers === null) {
			if ($this->isNew()) {
				$this->collPublishers = array();
			} else {

				$criteria->add(PublisherPeer::COUNTRY_ID, $this->getId());

				$this->collPublishers = PublisherPeer::doSelectJoinAdmins($criteria, $con);
			}
		} else {
									
			$criteria->add(PublisherPeer::COUNTRY_ID, $this->getId());

			if (!isset($this->lastPublisherCriteria) || !$this->lastPublisherCriteria->equals($criteria)) {
				$this->collPublishers = PublisherPeer::doSelectJoinAdmins($criteria, $con);
			}
		}
		$this->lastPublisherCriteria = $criteria;

		return $this->collPublishers;
	}

	
	public function initTitles()
	{
		if ($this->collTitles === null) {
			$this->collTitles = array();
		}
	}

	
	public function getTitles($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
			   $this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::COUNTRY_ID, $this->getId());

				TitlePeer::addSelectColumns($criteria);
				$this->collTitles = TitlePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(TitlePeer::COUNTRY_ID, $this->getId());

				TitlePeer::addSelectColumns($criteria);
				if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
					$this->collTitles = TitlePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastTitleCriteria = $criteria;
		return $this->collTitles;
	}

	
	public function countTitles($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(TitlePeer::COUNTRY_ID, $this->getId());

		return TitlePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addTitle(Title $l)
	{
		$this->collTitles[] = $l;
		$l->setCountry($this);
	}


	
	public function getTitlesJoinPublisher($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::COUNTRY_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinPublisher($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::COUNTRY_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinPublisher($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinTitleType($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::COUNTRY_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinTitleType($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::COUNTRY_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinTitleType($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinTitleFrequency($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::COUNTRY_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinTitleFrequency($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::COUNTRY_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinTitleFrequency($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinLanguage($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::COUNTRY_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinLanguage($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::COUNTRY_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinLanguage($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}


	
	public function getTitlesJoinAdmins($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseTitlePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collTitles === null) {
			if ($this->isNew()) {
				$this->collTitles = array();
			} else {

				$criteria->add(TitlePeer::COUNTRY_ID, $this->getId());

				$this->collTitles = TitlePeer::doSelectJoinAdmins($criteria, $con);
			}
		} else {
									
			$criteria->add(TitlePeer::COUNTRY_ID, $this->getId());

			if (!isset($this->lastTitleCriteria) || !$this->lastTitleCriteria->equals($criteria)) {
				$this->collTitles = TitlePeer::doSelectJoinAdmins($criteria, $con);
			}
		}
		$this->lastTitleCriteria = $criteria;

		return $this->collTitles;
	}

} 