<?php


abstract class BaseArticleView extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id = 0;


	
	protected $title_id;


	
	protected $language_id;


	
	protected $headline;


	
	protected $date;


	
	protected $parsed_at;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getTitleId()
	{

		return $this->title_id;
	}

	
	public function getLanguageId()
	{

		return $this->language_id;
	}

	
	public function getHeadline()
	{

		return $this->headline;
	}

	
	public function getDate($format = 'Y-m-d')
	{

		if ($this->date === null || $this->date === '') {
			return null;
		} elseif (!is_int($this->date)) {
						$ts = strtotime($this->date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [date] as date/time value: " . var_export($this->date, true));
			}
		} else {
			$ts = $this->date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getParsedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->parsed_at === null || $this->parsed_at === '') {
			return null;
		} elseif (!is_int($this->parsed_at)) {
						$ts = strtotime($this->parsed_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [parsed_at] as date/time value: " . var_export($this->parsed_at, true));
			}
		} else {
			$ts = $this->parsed_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v || $v === 0) {
			$this->id = $v;
			$this->modifiedColumns[] = ArticleViewPeer::ID;
		}

	} 
	
	public function setTitleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_id !== $v) {
			$this->title_id = $v;
			$this->modifiedColumns[] = ArticleViewPeer::TITLE_ID;
		}

	} 
	
	public function setLanguageId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->language_id !== $v) {
			$this->language_id = $v;
			$this->modifiedColumns[] = ArticleViewPeer::LANGUAGE_ID;
		}

	} 
	
	public function setHeadline($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->headline !== $v) {
			$this->headline = $v;
			$this->modifiedColumns[] = ArticleViewPeer::HEADLINE;
		}

	} 
	
	public function setDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->date !== $ts) {
			$this->date = $ts;
			$this->modifiedColumns[] = ArticleViewPeer::DATE;
		}

	} 
	
	public function setParsedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [parsed_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->parsed_at !== $ts) {
			$this->parsed_at = $ts;
			$this->modifiedColumns[] = ArticleViewPeer::PARSED_AT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->title_id = $rs->getInt($startcol + 1);

			$this->language_id = $rs->getInt($startcol + 2);

			$this->headline = $rs->getString($startcol + 3);

			$this->date = $rs->getDate($startcol + 4, null);

			$this->parsed_at = $rs->getTimestamp($startcol + 5, null);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 6; 
		} catch (Exception $e) {
			throw new PropelException("Error populating ArticleView object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ArticleViewPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ArticleViewPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ArticleViewPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ArticleViewPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += ArticleViewPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = ArticleViewPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ArticleViewPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTitleId();
				break;
			case 2:
				return $this->getLanguageId();
				break;
			case 3:
				return $this->getHeadline();
				break;
			case 4:
				return $this->getDate();
				break;
			case 5:
				return $this->getParsedAt();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ArticleViewPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTitleId(),
			$keys[2] => $this->getLanguageId(),
			$keys[3] => $this->getHeadline(),
			$keys[4] => $this->getDate(),
			$keys[5] => $this->getParsedAt(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ArticleViewPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTitleId($value);
				break;
			case 2:
				$this->setLanguageId($value);
				break;
			case 3:
				$this->setHeadline($value);
				break;
			case 4:
				$this->setDate($value);
				break;
			case 5:
				$this->setParsedAt($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ArticleViewPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTitleId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setLanguageId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setHeadline($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setDate($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setParsedAt($arr[$keys[5]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ArticleViewPeer::DATABASE_NAME);

		if ($this->isColumnModified(ArticleViewPeer::ID)) $criteria->add(ArticleViewPeer::ID, $this->id);
		if ($this->isColumnModified(ArticleViewPeer::TITLE_ID)) $criteria->add(ArticleViewPeer::TITLE_ID, $this->title_id);
		if ($this->isColumnModified(ArticleViewPeer::LANGUAGE_ID)) $criteria->add(ArticleViewPeer::LANGUAGE_ID, $this->language_id);
		if ($this->isColumnModified(ArticleViewPeer::HEADLINE)) $criteria->add(ArticleViewPeer::HEADLINE, $this->headline);
		if ($this->isColumnModified(ArticleViewPeer::DATE)) $criteria->add(ArticleViewPeer::DATE, $this->date);
		if ($this->isColumnModified(ArticleViewPeer::PARSED_AT)) $criteria->add(ArticleViewPeer::PARSED_AT, $this->parsed_at);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ArticleViewPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setId($this->id);

		$copyObj->setTitleId($this->title_id);

		$copyObj->setLanguageId($this->language_id);

		$copyObj->setHeadline($this->headline);

		$copyObj->setDate($this->date);

		$copyObj->setParsedAt($this->parsed_at);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ArticleViewPeer();
		}
		return self::$peer;
	}

} 