<?php


abstract class BaseArticleCl24 extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id = 0;


	
	protected $title_id = 0;


	
	protected $headline = '';


	
	protected $summary;


	
	protected $date;


	
	protected $category = '';


	
	protected $body;


	
	protected $report_date;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getTitleId()
	{

		return $this->title_id;
	}

	
	public function getHeadline()
	{

		return $this->headline;
	}

	
	public function getSummary()
	{

		return $this->summary;
	}

	
	public function getDate($format = 'Y-m-d')
	{

		if ($this->date === null || $this->date === '') {
			return null;
		} elseif (!is_int($this->date)) {
						$ts = strtotime($this->date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [date] as date/time value: " . var_export($this->date, true));
			}
		} else {
			$ts = $this->date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getCategory()
	{

		return $this->category;
	}

	
	public function getBody()
	{

		return $this->body;
	}

	
	public function getReportDate($format = 'Y-m-d')
	{

		if ($this->report_date === null || $this->report_date === '') {
			return null;
		} elseif (!is_int($this->report_date)) {
						$ts = strtotime($this->report_date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [report_date] as date/time value: " . var_export($this->report_date, true));
			}
		} else {
			$ts = $this->report_date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v || $v === 0) {
			$this->id = $v;
			$this->modifiedColumns[] = ArticleCl24Peer::ID;
		}

	} 
	
	public function setTitleId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->title_id !== $v || $v === 0) {
			$this->title_id = $v;
			$this->modifiedColumns[] = ArticleCl24Peer::TITLE_ID;
		}

	} 
	
	public function setHeadline($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->headline !== $v || $v === '') {
			$this->headline = $v;
			$this->modifiedColumns[] = ArticleCl24Peer::HEADLINE;
		}

	} 
	
	public function setSummary($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->summary !== $v) {
			$this->summary = $v;
			$this->modifiedColumns[] = ArticleCl24Peer::SUMMARY;
		}

	} 
	
	public function setDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->date !== $ts) {
			$this->date = $ts;
			$this->modifiedColumns[] = ArticleCl24Peer::DATE;
		}

	} 
	
	public function setCategory($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->category !== $v || $v === '') {
			$this->category = $v;
			$this->modifiedColumns[] = ArticleCl24Peer::CATEGORY;
		}

	} 
	
	public function setBody($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->body !== $v) {
			$this->body = $v;
			$this->modifiedColumns[] = ArticleCl24Peer::BODY;
		}

	} 
	
	public function setReportDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [report_date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->report_date !== $ts) {
			$this->report_date = $ts;
			$this->modifiedColumns[] = ArticleCl24Peer::REPORT_DATE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->title_id = $rs->getInt($startcol + 1);

			$this->headline = $rs->getString($startcol + 2);

			$this->summary = $rs->getString($startcol + 3);

			$this->date = $rs->getDate($startcol + 4, null);

			$this->category = $rs->getString($startcol + 5);

			$this->body = $rs->getString($startcol + 6);

			$this->report_date = $rs->getDate($startcol + 7, null);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 8; 
		} catch (Exception $e) {
			throw new PropelException("Error populating ArticleCl24 object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ArticleCl24Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ArticleCl24Peer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ArticleCl24Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ArticleCl24Peer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += ArticleCl24Peer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = ArticleCl24Peer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ArticleCl24Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTitleId();
				break;
			case 2:
				return $this->getHeadline();
				break;
			case 3:
				return $this->getSummary();
				break;
			case 4:
				return $this->getDate();
				break;
			case 5:
				return $this->getCategory();
				break;
			case 6:
				return $this->getBody();
				break;
			case 7:
				return $this->getReportDate();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ArticleCl24Peer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTitleId(),
			$keys[2] => $this->getHeadline(),
			$keys[3] => $this->getSummary(),
			$keys[4] => $this->getDate(),
			$keys[5] => $this->getCategory(),
			$keys[6] => $this->getBody(),
			$keys[7] => $this->getReportDate(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ArticleCl24Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTitleId($value);
				break;
			case 2:
				$this->setHeadline($value);
				break;
			case 3:
				$this->setSummary($value);
				break;
			case 4:
				$this->setDate($value);
				break;
			case 5:
				$this->setCategory($value);
				break;
			case 6:
				$this->setBody($value);
				break;
			case 7:
				$this->setReportDate($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ArticleCl24Peer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTitleId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setHeadline($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setSummary($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setDate($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setCategory($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setBody($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setReportDate($arr[$keys[7]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ArticleCl24Peer::DATABASE_NAME);

		if ($this->isColumnModified(ArticleCl24Peer::ID)) $criteria->add(ArticleCl24Peer::ID, $this->id);
		if ($this->isColumnModified(ArticleCl24Peer::TITLE_ID)) $criteria->add(ArticleCl24Peer::TITLE_ID, $this->title_id);
		if ($this->isColumnModified(ArticleCl24Peer::HEADLINE)) $criteria->add(ArticleCl24Peer::HEADLINE, $this->headline);
		if ($this->isColumnModified(ArticleCl24Peer::SUMMARY)) $criteria->add(ArticleCl24Peer::SUMMARY, $this->summary);
		if ($this->isColumnModified(ArticleCl24Peer::DATE)) $criteria->add(ArticleCl24Peer::DATE, $this->date);
		if ($this->isColumnModified(ArticleCl24Peer::CATEGORY)) $criteria->add(ArticleCl24Peer::CATEGORY, $this->category);
		if ($this->isColumnModified(ArticleCl24Peer::BODY)) $criteria->add(ArticleCl24Peer::BODY, $this->body);
		if ($this->isColumnModified(ArticleCl24Peer::REPORT_DATE)) $criteria->add(ArticleCl24Peer::REPORT_DATE, $this->report_date);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ArticleCl24Peer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setId($this->id);

		$copyObj->setTitleId($this->title_id);

		$copyObj->setHeadline($this->headline);

		$copyObj->setSummary($this->summary);

		$copyObj->setDate($this->date);

		$copyObj->setCategory($this->category);

		$copyObj->setBody($this->body);

		$copyObj->setReportDate($this->report_date);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ArticleCl24Peer();
		}
		return self::$peer;
	}

} 