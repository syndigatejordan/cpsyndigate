<?php

/**
 * Subclass for performing query and update operations on the 'title' table.
 *
 * 
 *
 * @package lib.model
 */ 
class TitlePeer extends BaseTitlePeer
{
	public static function getActiveTitles()
	{
		$c = new Criteria();
		$c->add(TitlePeer::IS_ACTIVE, 1);
		$c->addAscendingOrderByColumn(TitlePeer::NAME);
		return TitlePeer::doSelect($c);
	}
		
}
