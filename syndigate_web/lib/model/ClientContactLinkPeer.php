<?php

/**
 * Subclass for performing query and update operations on the 'client_contact_link' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ClientContactLinkPeer extends BaseClientContactLinkPeer
{
}
