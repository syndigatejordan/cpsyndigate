<?php

/**
 * Subclass for representing a row from the 'publisher_contact_link' table.
 *
 * 
 *
 * @package lib.model
 */ 
class PublisherContactLink extends BasePublisherContactLink
{
	public static function deletePublisherContact($publisherId, $contactId)
	{
		$c = new Criteria();
		$c->add(PublisherContactLinkPeer::PUBLISHER_ID , $publisherId);
		$c->add(PublisherContactLinkPeer::CONTACT_ID , $contactId);
		PublisherContactLinkPeer::dodelete($c);
		
		$publisher  = PublisherPeer::retrieveByPK($publisherId);
		$nagiosPath = $publisher->getNagiosFilePath();
							
		//delete the contact
		$contact = ContactPeer::retrieveByPK($contactId);
		$contact->delete();
		
		require LINUX_SCRIPTS_PATH . 'syndGenerateNagiosConf.php';
	}
	
}
