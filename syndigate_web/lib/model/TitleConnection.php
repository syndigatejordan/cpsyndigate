<?php

/**
 * Subclass for representing a row from the 'title_connection' table.
 *
 * 
 *
 * @package lib.model
 */ 
class TitleConnection extends BaseTitleConnection
{
	public function __toString()
	{
		return $this->getId();
	}
	
	
	public function doValidation()
	{
		$this->setUrl(str_ireplace('FTP://', '', $this->getUrl()));
		
		
			if(!in_array($this->getConnectionType(), array('http','ftp','local'))) {
				return "Connection type must be in [http,'ftp,local]";
			} else {
				
				if(in_array($this->getConnectionType(), array('http','ftp'))) {
					
					if(!$this->getUrl()) {
						return 'URL Not provided';
					} else {
								
						if ($this->getAuthintication()) {
							if(!$this->getUsername() || !$this->getPassword()) {
								return 'Username or password not set';
							}
						} else {
							$this->setUsername('');
							$this->setPassword('');
							/*
							if($this->getUsername() || $this->getPassword()) {
								return 'Set the authentication to true [username or password provided]';
							}
							*/
						}
						return false;
					}
					
				} else {
					//local connection here
					
					//correcting values [ some values can come incorrect by javascript hidden fileds]
					//Now this will escape the errors for the next validation				
					$this->setUrl('');
					$this->setAuthintication(false);
					$this->setUsername('');
					$this->setPassword('');
					/*
					if(trim($this->getUrl()) != '') {
						return 'URL must not be set for local connection';
					} else { 
						if($this->getAuthintication()) {
							return 'You can not set authentication for local connection';
						} else {
							if($this->getUsername() || $this->getPassword()) {
								return 'You can not set Username or password for local connection.';
							} else {
								return false;
							}
						}
					}
					*/
				}
			}
	}
	
	
	
	public function Update($titleId)
	{
		$this->setTitleId($titleId);
		$errMsg = $this->doValidation();
		
		if(!$errMsg) {
			$this->save();
			return false;
		} else {
			return $errMsg;
		}
	}

}
