<?php

/**
 * Subclass for performing query and update operations on the 'tenders_info_duplications' table.
 *
 * 
 *
 * @package lib.model
 */ 
class TendersInfoDuplicationsPeer extends BaseTendersInfoDuplicationsPeer
{
}
