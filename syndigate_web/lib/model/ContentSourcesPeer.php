<?php

/**
 * Subclass for performing query and update operations on the 'content_sources' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ContentSourcesPeer extends BaseContentSourcesPeer
{
}
