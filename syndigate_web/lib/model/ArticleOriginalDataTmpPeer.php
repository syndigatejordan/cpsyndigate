<?php

/**
 * Subclass for performing query and update operations on the 'article_original_data_tmp' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ArticleOriginalDataTmpPeer extends BaseArticleOriginalDataTmpPeer
{
}
