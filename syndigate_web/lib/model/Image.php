<?php

/**
 * Subclass for representing a row from the 'image' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Image extends BaseImage
{
	
	/**
	 * Return image object by it's name 
	 *
	 * @param Integer $articleId
	 * @param String $originalName : The full name [with title home dir path] path
	 * @return Image Object
	 */
	public static function getImageByOriginalName($articleId, $originalName)
	{
		$c = new Criteria();
		$c->add(ImagePeer::ARTICLE_ID, $articleId);
		$c->add(ImagePeer::ORIGINAL_NAME, $originalName, Criteria::EQUAL);
		return ImagePeer::doselectOne($c);
	}
	
	
	/**
	 * Delete the Image record and the physical File on hardDisk
	 *
	 * @param Integer $imageId
	 * @return True if Success false otherwise 
	 */
	public static function deleteCascade($imageId) 
	{
		$image 		= ImagePeer::retrieveByPK($imageId);
				
		if(file_exists($image->getImagePath())) {
			unlink($image->getImagePath());
		}
		$image->delete();
		return true;
	}
	
	
	/**
	 * Return the full path of the image [with image name]
	 *
	 * @return String
	*/	
	public function getImagePath()
	{
		return str_replace(IMGS_HOST, IMGS_PATH, $this->getImgName());
	}
	
	
	/*
	public function getImageDisplayName()
	{	
		$array = explode('/', $this->getImgName());
		return $array[3];
	}
	*/
	
	/*
	public function getImageInternalFolder()
	{
		$array = explode('/', $this->getImagePath());
		return $array[2];
	}
	*/
	
	
	
	
	
}
