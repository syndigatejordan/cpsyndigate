<?php

/**
 * Subclass for performing query and update operations on the 'client_connection' table.
 *
 * 
 *
 * @package lib.model
 */ 
class ClientConnectionPeer extends BaseClientConnectionPeer
{
}
