<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<?php include_http_metas() ?>
<?php include_metas() ?>

<?php include_title() ?>

<link rel="shortcut icon" href="/favicon.ico" />

<script type="text/javascript">

var server_timestamp = <?php echo time()?>;

function init ( )
{
  timeDisplay = document.createTextNode ( "" );
  document.getElementById("clock").appendChild ( timeDisplay );
}


function updateClock ( )
{
  server_timestamp = server_timestamp + 1;
  
  var currentTime = new Date ();
  currentTime.setTime(server_timestamp * 1000 );

  var currentHours   = currentTime.getUTCHours ( );
  var currentMinutes = currentTime.getMinutes ( );
  var currentSeconds = currentTime.getSeconds ( );

  // Pad the minutes and seconds with leading zeros, if required
  currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
  currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

  // Choose either "AM" or "PM" as appropriate
  var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";

  // Convert the hours component to 12-hour format if needed
  currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

  // Convert an hours component of "0" to "12"
  currentHours = ( currentHours == 0 ) ? 12 : currentHours;

  // Compose the string for display
  var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;

  // Update the time display
  document.getElementById("clock").firstChild.nodeValue = currentTimeString;
}

</script>


</head>

<body  onload="updateClock(); setInterval('updateClock()', 1000 )">

<div id="tabs">
	<?php 
	$tabDashboard   = 'dashboard';
	$tabPublisherId = 'publisher';
	$tabTitleId     = 'title';
	$tabClientId	= 'client';
	
	if(($sf_context->getModuleName() == 'dashboard')||($sf_context->getModuleName() == 'dashboard')) {
		$tabDashboard   = 'current';
	} elseif(($sf_context->getModuleName() == 'publisher')||($sf_context->getModuleName() == 'adminPublisher')) {
		$tabPublisherId = 'current';
	} elseif (($sf_context->getModuleName() == 'title')||($sf_context->getModuleName() == 'adminTitle')) {
		$tabTitleId  	= 'current';
	} elseif (($sf_context->getModuleName() == 'client')||($sf_context->getModuleName() == 'adminClient')) {
		$tabClientId 	= 'current';
	}
	?>
	<ul>
		<li id="<?php echo $tabDashboard ?>"><?php echo link_to('Dashboard', 'dashboard'); ?></li>
        <li id="<?php echo $tabPublisherId ?>"><?php echo link_to('Publishers', 'adminPublisher/list?filter=filter'); ?></li>
        <li id="<?php echo $tabTitleId?>"><?php echo link_to('Titles', 'adminTitle/list?filter=filter');?></li>
        <li id="<?php echo $tabClientId?>"><?php echo link_to('Clients', 'client/list');?></li>
        <li id="li-admin"  ><a id="lnk-admin"  onclick="javaScript: displayDivMnu('div-admin',  'lnk-admin') ;  setMeToSelected(this.parentNode.id);  "  style="cursor: pointer;">Admin</a></li>
        <li id="li-export" ><a id="lnk-export" onclick="javaScript: displayDivMnu('div-export', 'lnk-export');  setMeToSelected(this.parentNode.id);  " style="cursor: pointer;">Reports</a></li>
        
    </ul>
</div>



<div id="submnu" style="clear: both;">
    <table width="100%">
    	<tr> 
    		<td width="100" nowrap="nowrap"> 
    			<div id="div-admin" style="display:none; ">
    				<?php echo link_to('Language', 'language/list?filter=filter');?> &nbsp; | &nbsp;
    				<?php echo link_to('Country', 'country/list?filter=filter');?> &nbsp; | &nbsp;
    				<?php echo link_to('Articles', 'article/list?filter=filter');?> &nbsp; | &nbsp;
    				<?php echo link_to('IPTC', 'iptc/list?filter=filter');?> &nbsp; | &nbsp;
    				<?php echo link_to('Crontab', 'gatherCrontab/list?filter=filter');?> &nbsp; | &nbsp;
    				<?php echo link_to('Cronjob', 'gatherCronjob/list?filter=filter');?> &nbsp; | &nbsp;
    				<?php echo link_to('Report', 'report/list?filter=filter');?> &nbsp; | &nbsp;
    				<?php echo link_to('Gather', 'reportGather/list?filter=filter');?> &nbsp; | &nbsp;
					<?php echo link_to('Parse', 'reportParse/list?filter=filter');?> &nbsp; | &nbsp;					
					<?php echo link_to('Parsing State', 'parsingState/list?filter=filter');?> &nbsp;
					<?php echo link_to('Client Titles', 'client_connection_file/firstDate');?> &nbsp;
				</div>

				<div id="div-export" style="display:none;">
    				<?php echo link_to('Titles', 'sheet/title');?> &nbsp; |
    				<?php echo link_to('Clients', 'sheet/client');?> &nbsp; |
    				<?php echo link_to('Titles & Clients', 'sheet/titlesClients');?> &nbsp; | 
					<?php echo link_to('Story count', 'sheet/storyCount');?> &nbsp; | 
    				<?php echo link_to('Emails', 'sheet/emails');?> &nbsp; 
				</div>
				
    		</td>
    	</tr>
    	<tr>
    		<td align="right">
    		<?php 
    		echo "<span id=\"clock\">&nbsp;</span>" . ' | ';
    		echo link_to('change password', 'login/password') . ' | ';
    		
    		if ($sf_user->getAttribute('logged')) {
    			echo link_to('Logout', 'login/logout');
    		} else {
    			echo link_to('Login', 'login/index');
    		}
    		?>
    		</td>
    	</tr>
    </table>
	

</div>



<hr>

<?php echo $sf_data->getRaw('sf_content') ?>

</body>
</html>
<script>
	function displayDivMnu(divId, callerLinkId) {
		hideAllDivsMnu();
		document.getElementById(divId).style.display='block';
	}
	
	function hideAllDivsMnu()
	{
		document.getElementById('div-admin').style.display='none';
		document.getElementById('div-export').style.display='none';
	}
	
	
	function setMeToSelected(liId) {
		rnd = 'tmp' + (Math.random() * 1000)
		try{
			document.getElementById('current').id = rnd;
		} catch(err) {
			
		}
		
		try {
			document.getElementById(liId).id = 'current';
		} catch(err) {
			document.getElementById(rnd).id = 'current';
		}
		
	}	
	

</script>


