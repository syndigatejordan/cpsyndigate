<div id="office_<?php echo $office->getId(); ?>">

<?php 
use_helper('Object');
use_helper("Javascript"); 

//echo form_remote_tag(array('update' => "office_" . $office->getId(), 'url' => 'publisherOffices/update',));
echo form_tag('publisherOffices/update');
echo object_input_hidden_tag($office, 'getId');

if(isset($publisherId)) {
	echo input_hidden_tag('publisher_id', $publisherId);
}
?>

<table>
<tbody>
<tr>
  <th>Phone1:</th>
  <td><?php echo object_input_tag($office, 'getPhone1', array ('size' => 80,)) ?></td>
</tr>
<tr>
  <th>Phone2:</th>
  <td><?php echo object_input_tag($office, 'getPhone2', array ('size' => 80,)) ?></td>
</tr>
<tr>
  <th>Fax:</th>
  <td><?php echo object_input_tag($office, 'getFax', array ('size' => 80,)) ?></td>
</tr>
<tr>
  <th>Country:</th>
  <td><?php echo object_select_tag($office, 'getCountryId', array ('related_class' => 'Country',  'include_blank' => true,)) ?></td>
</tr>
<tr>
  <th>City name:</th>
  <td><?php echo object_input_tag($office, 'getCityName', array ('size' => 80,)) ?></td>
</tr>
<tr>
  <th>Pobox:</th>
  <td><?php echo object_input_tag($office, 'getPobox', array ('size' => 80,)) ?></td>
</tr>
<tr>
  <th>Zipcode:</th>
  <td><?php echo object_input_tag($office, 'getZipcode', array ('size' => 7,)) ?></td>
</tr>
<tr>
  <th>Street address:</th>
  <td><?php echo object_textarea_tag($office, 'getStreetAddress', array ('size' => '30x3',)) ?></td>
</tr>


</tbody>
</table>
<hr />
<?php echo submit_tag('save') ?>
&nbsp; <?php echo button_to('Delete', 'publisherOffices/delete?id=' . $office->getId()) ?>
&nbsp; <?php echo button_to('Cancel', 'publisher/edit?id=' . $publisherId . '#offices') ?>
</form>
</div>

<?php if(isset($msg) &&(trim($msg)!= '')) {
	?>
	<script>alert('<?php echo $msg ?>');</script>
	<?php
}
?>