<div id="new_office">
<h2>New Office</h2>
<b><font color="Red"><?php echo isset($msg)? $msg : '';?></font></b><br />

<?php 
use_helper('Object');
use_helper("Javascript"); 


echo form_remote_tag(array('update' => "new_office", 'url' => 'publisherOffices/update',));
echo object_input_hidden_tag($office, 'getId');
echo input_hidden_tag('publisher_id', $publisherId);
?>

<table>
<tbody>
<tr>
  <th>Phone1:</th>
  <td><?php echo object_input_tag($office, 'getPhone1', array ('size' => 80,)) ?></td>
</tr>
<tr>
  <th>Phone2:</th>
  <td><?php echo object_input_tag($office, 'getPhone2', array ('size' => 80,)) ?></td>
</tr>
<tr>
  <th>Fax:</th>
  <td><?php echo object_input_tag($office, 'getFax', array ('size' => 80,)) ?></td>
</tr>
<tr>
  <th>Pobox:</th>
  <td><?php echo object_input_tag($office, 'getPobox', array ('size' => 80,)) ?></td>
</tr>
<tr>
  <th>Zipcode:</th>
  <td><?php echo object_input_tag($office, 'getZipcode', array ('size' => 7,)) ?></td>
</tr>
<tr>
  <th>Street address:</th>
  <td><?php echo object_textarea_tag($office, 'getStreetAddress', array ('size' => '30x3',)) ?></td>
</tr>
<tr>
  <th>City name:</th>
  <td><?php echo object_input_tag($office, 'getCityName', array ('size' => 80,)) ?></td>
</tr>
<tr>
  <th>Country:</th>
  <td><?php echo object_select_tag($office, 'getCountryId', array ('related_class' => 'Country','include_blank' => true,
)) ?></td>
</tr>
</tbody>
</table>
<hr />
<?php 
echo submit_tag('Add Office');

/*
echo button_to_remote('cancel', array(  
			'url'	   => '',
			'loading'  => "Element.show('show_add_office')", 
			'complete' => "Element.hide('new_office')",)); 
			*/
echo button_to_remote('Finish', array(  'url'	   => 'publisherOffices/list?publisher_id=' . $publisherId,
										'update'   => 'new_office', 'loading'  => "", 'complete' => "",));  
?>
</form>
</div>