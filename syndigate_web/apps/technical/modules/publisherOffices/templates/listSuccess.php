<b><font color="Red"><?php echo isset($msg)? $msg : '';?></font></b><br />
<?php 
use_helper('Object'); 
use_helper("Javascript");  
?>

<table border="1" width="100%">
<thead>
<tr>
  <th>&nbsp;</th>
  <th>Phone1</th>
  <th>Phone2</th>
  <th>Fax</th>
  <th>Pobox</th>
  <th>Zipcode</th>
  <th>City name</th>
  <th>Country</th>
</tr>
</thead>
<tbody>
<?php foreach ($offices as $office): ?>
<tr>
      <td><a href="/technical.php/publisherOffices/edit?id=<?php echo $office->getId();?>&publisher_id=<?php echo $publisherId; ?>"><?php echo '#' 							?></a></td>
      <td><a href="/technical.php/publisherOffices/edit?id=<?php echo $office->getId();?>&publisher_id=<?php echo $publisherId; ?>"><?php echo $office->getPhone1(); 		?></a></td>
      <td><a href="/technical.php/publisherOffices/edit?id=<?php echo $office->getId();?>&publisher_id=<?php echo $publisherId; ?>"><?php echo $office->getPhone2(); 		?></a></td>
      <td><a href="/technical.php/publisherOffices/edit?id=<?php echo $office->getId();?>&publisher_id=<?php echo $publisherId; ?>"><?php echo $office->getFax();    		?></a></td>
      <td><a href="/technical.php/publisherOffices/edit?id=<?php echo $office->getId();?>&publisher_id=<?php echo $publisherId; ?>"><?php echo $office->getPobox();  		?></a></td>
      <td><a href="/technical.php/publisherOffices/edit?id=<?php echo $office->getId();?>&publisher_id=<?php echo $publisherId; ?>"><?php echo $office->getZipcode();		?></a></td>
      <td><a href="/technical.php/publisherOffices/edit?id=<?php echo $office->getId();?>&publisher_id=<?php echo $publisherId; ?>"><?php echo $office->getCityName();		?></a></td>
      <td><a href="/technical.php/publisherOffices/edit?id=<?php echo $office->getId();?>&publisher_id=<?php echo $publisherId; ?>"><?php echo $office->getCountry();		?></a></td>
</tr>
<?php endforeach; ?>
</tbody>
</table>

<?php 
echo form_remote_tag(array('update' => "offices_table", 'url' => 'publisherOffices/create',));
echo input_hidden_tag("publisher_id", $publisherId);
echo submit_tag('Add Office', 'technical/publisherOffices/create?publisher_id=' . $publisherId);
?>
</form>