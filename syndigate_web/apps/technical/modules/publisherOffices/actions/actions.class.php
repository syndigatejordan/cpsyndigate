<?php
class publisherOfficesActions extends sfActions
{
	/**
	 * Forword to list publisher offices
	 * Needs publisher_id
	 * 
	 */
	public function executeIndex()
	{
		return $this->forward('publisherOffices', 'list');
	}
	
	/**
	 * List publisher offices 
	 * Needs publisher_id
	 */
	public function executeList()
	{
		$this->forward404Unless($this->getRequestParameter('publisher_id'));
		
		$c = new Criteria();
		$c->add(PublisherOfficeLinkPeer::PUBLISHER_ID , $this->getRequestParameter('publisher_id'));
		$publisherOfficeLinks = PublisherOfficeLinkPeer::doSelect($c);
		
		$offices = array();
		foreach ($publisherOfficeLinks as $publisherOfficeLink) {
			$offices[] = $publisherOfficeLink->getOffice();
		}
		
		$this->setLayout('emptyLayout');
		$this->offices 		= $offices;
		$this->publisherId 	= $this->getRequestParameter('publisher_id');
		$this->msg         = $this->getRequestParameter('msg');
	}
	
	
	/**
	 * display create publisher office form [ajax view]
	 * Needs publisher_id
	 */
	public function executeCreate()
	{
		$this->setLayout(false);
		$publisherId = $this->getRequestParameter('publisher_id');
		$this->forward404Unless($publisherId);
		$this->publisherId = $publisherId;
		
		$this->office = new Office();
	}
	
	
	/**
	 * Display edit publisher office form
	 * Needs id [the office id] and publisher_id
	 */
	public function executeEdit()
	{
		$this->office = OfficePeer::retrieveByPk($this->getRequestParameter('id'));
		$this->forward404Unless($this->office);
		$this->publisherId  = $this->getRequestParameter('publisher_id');
		$this->msg			= $this->getRequestParameter('msg');
	}
	
	
	/**
	 * Update create office form or edit office form
	 */
	public function executeUpdate()
	{
		if (!$this->getRequestParameter('id')) {
			
			$office = new Office();
			$this->forward404Unless($this->getRequestParameter('publisher_id'));
			$isNew = true;
		} else {
			$office = OfficePeer::retrieveByPk($this->getRequestParameter('id'));
			$this->forward404Unless($office);
		}
        
    	$office->setId($this->getRequestParameter('id'));
    	$office->setPhone1($this->getRequestParameter('phone1'));
    	$office->setPhone2($this->getRequestParameter('phone2'));
    	$office->setFax($this->getRequestParameter('fax'));
    	$office->setPobox($this->getRequestParameter('pobox'));
    	$office->setZipcode($this->getRequestParameter('zipcode'));
    	$office->setStreetAddress($this->getRequestParameter('street_address'));
    	$office->setCityName($this->getRequestParameter('city_name'));
    	$office->setCountryId($this->getRequestParameter('country_id') ? $this->getRequestParameter('country_id') : null);
    
    	$errMsg = $this->getOfficeErrMsg($office);
   
    	if($errMsg) {
	    	$this->msg 			= $errMsg;
    		$this->publisherId 	= $this->getRequestParameter('publisher_id');
    		$this->setTemplate($isNew ? 'create' : 'edit');
    	} else {
    	
    		$office->save();
    	
    		if($isNew) {
    			$publisherOfficeLink = new PublisherOfficeLink();
   				$publisherOfficeLink->setOfficeId($office->getId());
   				$publisherOfficeLink->setPublisherId($this->getRequestParameter('publisher_id'));
   				$publisherOfficeLink->save();
   			
   				$this->getRequest()->setParameter('msg', 'msg');
   				return $this->redirect('publisherOffices/list?publisher_id='. $this->getRequestParameter('publisher_id') . "&msg=Office added");
   			} else {
    			return $this->redirect('publisherOffices/edit?id='.$office->getId() . "&publisher_id=" . $this->getRequestParameter('publisher_id') . '&msg=Updated');
    		}
    	}
    
	}
	
	
	/**
	 * Delete publisher office [office and publishr office link]
	 * Needs id parameter [the office id]
	 */
	public function executeDelete()
	{
		$office = OfficePeer::retrieveByPk($this->getRequestParameter('id'));
		$this->forward404Unless($office);
		
		$c = new Criteria();
		$c->add(PublisherOfficeLinkPeer::OFFICE_ID, $office->getId());
		$publisherLink = PublisherOfficeLinkPeer::doSelectOne($c);
		
		PublisherOfficeLink::deletePublisherOffice($publisherLink->getPublisherId(), $office->getId());
		
		return $this->redirect('publisher/edit?id=' . $publisherLink->getPublisherId() . '#offices');   
	}
  
  
	/**
	 * return office parameters error or false if no error found 
	 *
	 * @param Office $officeObj
	 * @return String error or false
	 */
	private function getOfficeErrMsg($officeObj)
	{
		return false;
	}
	
}
