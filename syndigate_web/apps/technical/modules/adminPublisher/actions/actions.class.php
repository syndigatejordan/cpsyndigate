<?php

/**
 * adminPublisher actions.
 *
 * @package    syndigate
 * @subpackage adminPublisher
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class adminPublisherActions extends autoadminPublisherActions
{
	
	public function executeCreate()
	{
		$this->redirect('publisher/create');
	}
	
	public function executeEdit()
	{
		$this->redirect('publisher/edit?id=' . $this->getRequestParameter('id'));
	}
	
	public function executeShow()
	{
		$this->redirect('publisher/edit?id=' . $this->getRequestParameter('id'));
	}
	
	/**
	 * Delete not allowed
	 *
	 */
	public function executeDelete()
	{
		$this->redirect('publisher/edit?id=' . $this->getRequestParameter('id'));
	}
}
