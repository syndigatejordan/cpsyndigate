<?php use_helper('I18N', 'Date') ?>
<?php use_stylesheet('modyfiedMain') ?>

<div id="sf_admin_container">

<h1>Publishers</h1>

<div id="sf_admin_header">
	<?php include_partial('adminPublisher/list_header', array('pager' => $pager)) ?>
	<?php include_partial('adminPublisher/list_messages', array('pager' => $pager)) ?>
</div>
<div id="sf_admin_bar" >
	<?php include_partial('filters', array('filters' => $filters)) ?>
</div>

<div id="sf_admin_content">

	<div align="right"> <ul class="sf_admin_actions"> <li><?php echo button_to('New Publisher', 'publisher/create', array ('class' => 'sf_admin_action_create',)); ?></li> </ul> 
	</div>
	
	<?php 
	if (!$pager->getNbResults()) {
		echo __('no result');
	} else {
		include_partial('adminPublisher/list', array('pager' => $pager));
	}
	?>
	
	<div align="right"> <ul class="sf_admin_actions"> <li><?php echo button_to('New Publisher', 'publisher/create', array ('class' => 'sf_admin_action_create',)); ?></li> </ul> 
	</div>
	
	<?php include_partial('list_actions') ?>
</div>

<div id="sf_admin_footer">
	<?php include_partial('adminPublisher/list_footer', array('pager' => $pager)) ?>
</div>


</div>
