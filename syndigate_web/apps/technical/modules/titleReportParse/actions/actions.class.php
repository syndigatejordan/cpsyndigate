<?php

/**
 * titleReportrParse actions.
 *
 * @package    syndigate
 * @subpackage titleReportParse
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class titleReportParseActions extends autotitleReportParseActions
{
	protected function addFiltersCriteria($c)
	{
		$c->add(ReportParsePeer::TITLE_ID, $this->getRequestParameter('title_id'));
		parent::addFiltersCriteria(&$c);
	}
}
