<?php

class loginActions extends sfActions
{
    
    public function executeIndex() {
    	$this->logout();
    	
    	if ($this->getRequest()->getMethod() != sfRequest::POST) {
    		return sfView::SUCCESS;
    	} else {
    		
    		$usr = $this->getRequestParameter('username');
			$pwd = $this->getRequestParameter('password');
			$pwd = sha1($pwd);
			
			$c = new Criteria();
			$c->add(AdminsPeer::USERNAME, $usr);
			$c->add(AdminsPeer::PASSWORD, $pwd);
			$admin = AdminsPeer::doSelectOne($c);
			
			if ($admin) {
				
				$admin->setLastLogin(time());
			    $admin->save();

				$this->getUser()->setAuthenticated(true);
				$this->getUser()->setAttribute("logged", true);
				$this->getUser()->setAttribute("userId", $admin->getId());
				$this->getUser()->setAttribute("username", $admin->getUsername());
				//$this->redirect ('adminPublisher/list');
				$this->redirect ('dashboard/index');

			} else {
				$this->msg = "Invalid username or password.";
				return sfView::SUCCESS;
			}
    	}
    	
    	return sfView::NONE;
    }
    
    
    
    
    
    public function executePassword() {
    	
    	if ($this->getRequest()->getMethod() != sfRequest::POST) {
    		return sfView::SUCCESS;
    	} else {
    		
    		require_once LINUX_SCRIPTS_PATH . 'standard.php'; 
    		$standard = new Standard();
    		
    		$usr     = $this->getRequestParameter('username');
			$pwd     = $this->getRequestParameter('password');
			$newPass = $this->getRequestParameter('new_pass');
			$confirm = $this->getRequestParameter('confirm');
			$pwd 	 = sha1($pwd);
			
			if ($err = $standard->getPasswordErrorV2($newPass, $confirm, 4)) {
				$this->msg = $err;
			} else {
				
				$c = new Criteria();
				$c->add(AdminsPeer::USERNAME, $usr);
				$c->add(AdminsPeer::PASSWORD, $pwd);
				$admin = AdminsPeer::doSelectOne($c);
				
				if ($admin) {
					$admin->setPassword(sha1($newPass));
					$admin->save();
					$this->msg = "Password has been changed.";
				} else {
					$this->msg = "Invalid username or password.";
				}
						
			} // end if passwords matches
			return sfView::SUCCESS;
		} // end if post
    	
    	return sfView::NONE;
    }
    
    
    
    public function executeLogout() {
    	
    	$this->logout();
    	$this->msg = "You have been logged out.";
    	$this->setTemplate('index');
    	return sfView::SUCCESS;
    }
    
    
    private function logout() {
    	
    	$this->getUser()->setAuthenticated(false);
		$this->getUser()->setAttribute("logged", false);
		$this->getUser()->setAttribute("userId", null);
		$this->getUser()->setAttribute("username", null);
    }
    
} // End class
