<?php

/**
 * publisher actions.
 *
 * @package    syndigate
 * @subpackage publisher
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 3335 2007-01-23 16:19:56Z fabien $
 */
class publisherActions extends sfActions
{		
	public function executeIndex() {
		return $this->forward('publisher', 'list');
	}
	
	public function executeList() {
		$this->publishers = PublisherPeer::doSelect(new Criteria());
	}
	

	public function executeShow() {
		$this->publisher = PublisherPeer::retrieveByPk($this->getRequestParameter('id'));
		$this->forward404Unless($this->publisher);
	}
	
	
	public function executeCreate() {
		$this->publisher = new Publisher();
	}
	

	public function executeEdit() {
		
		$this->publisher = PublisherPeer::retrieveByPk($this->getRequestParameter('id'));
		$this->forward404Unless($this->publisher);
		
		$this->publisherContacts = $this->publisher->getContacts();
		$this->publisherOffices  = $this->publisher->getOffices();
		
	}


	
	public function executeUpdate() {
		
		if (!$this->getRequestParameter('id')) {
			$publisher = new Publisher();
			$isNew     = true;
		} else {
			$publisher 		= PublisherPeer::retrieveByPk($this->getRequestParameter('id'));
			$oldPublisher	= PublisherPeer::retrieveByPk($this->getRequestParameter('id'));
			$isNew	   		= false;
			$this->forward404Unless($publisher);
		}
		

		$publisher->setId($this->getRequestParameter('id'));
		$publisher->setName($this->getRequestParameter('name'));
		$publisher->setWebsite($this->getRequestParameter('website'));
		$publisher->setNotes($this->getRequestParameter('notes'));
		$publisher->setLogoPath($this->getRequestParameter('logo_path'));
		$publisher->setFtpUsername($isNew ? $this->getRequestParameter('ftp_username') : $oldPublisher->getFtpUsername());
		$publisher->setFtpPassword($isNew ? $this->getRequestParameter('ftp_password') : $oldPublisher->getFtpPassword());
		$publisher->setCountryId($this->getRequestParameter('country_id'));
		
		$publisher->setContract($this->getRequestParameter('contract'));
		$publisher->setContractStartDate($this->getRequestParameter('contract_start_date') ? $this->getRequestParameter('contract_start_date') : null);
		$publisher->setContractEndDate($this->getRequestParameter('contract_end_date') ? $this->getRequestParameter('contract_end_date') : null);
		$publisher->setRenewalPeriod($this->getRequestParameter('renewal_period'));
		$publisher->setRoyaltyRate($this->getRequestParameter('royalty_rate'));
		$publisher->setAdminsId($this->getRequestParameter('admins_id'));	

		require_once LINUX_SCRIPTS_PATH . 'standard.php';
		$standard 	= new Standard(); 
		
				
		$errMsg = '';
		$errMsg = $standard->getNameErrMsg($publisher->getName());
		
		if(!$errMsg && $isNew) {
			$errMsg = $standard->getPublisherFtpErrMsg($publisher->getFtpUsername());
			
			if(!$errMsg) {
				$errMsg = $standard->getPasswordErrorV1($publisher->getFtpPassword());
			}
			
			if(!$errMsg && Publisher::hasRealLinuxAccount($publisher->getFtpUsername())) {
				$errMsg = 'Username exist, select another name';
			}
		}
		
				
		if(!$errMsg && !$isNew) {
			
			if (!Publisher::hasRealLinuxAccount($publisher->getFtpUsername())) {
				$errMsg = 'Error, publisher has no linux account [ just available in DB ]';
			} 
		}
		
		
		if(!$errMsg) {
			if(('' != $publisher->getLogoPath())&& (!file_exists($standard->getAbsolutePathFromSystem($publisher->getLogoPath())))) {
				$errMsg = 'Logo path Image can not be found, reupload it';
			}
		}
		
		if(!$errMsg) {
			if(!$publisher->getCountryId()) {
				$errMsg = 'Country Not set';
			}
		}
		
		if(!$errMsg) {
			if($publisher->getContractStartDate() > $publisher->getContractEndDate()) {
				$errMsg = 'Contract start date must not be after contract end date.';
			}
		}
										
		
		$errTemplate = !$isNew ? 'edit' : 'create';

		if(isset($errMsg) && $errMsg!='') {
			$this->raisPhisycalError($publisher, $errMsg, $errTemplate);
			
		} else {
			$publisher->save();
			$titles = $publisher->getTitles();
			foreach ($titles as $title) {
				$title->setAdminsId($publisher->getAdminsId());
				$title->save();
			}	
			
			if($isNew) {
				
				//if this is a new publisher then run the script
				$username 	= $this->getRequestParameter('ftp_username');
				$pass		= $this->getRequestParameter('ftp_password');
				$synId		= $publisher->getId();
				$homePath	= HOMEPATH; 
				$wcPath		= WCPATH;
			
				$exe    = LINUX_USER_MOD . " " . LINUX_SCRIPTS_PATH . "syndAddSource.sh PUBLISHER $username $pass $synId $homePath $wcPath";
			        exec($exe);
				
				$userArray = posix_getpwnam($username); //getting the real unix_gid , unix_uid and update the database
								
				if(!is_array($userArray)) {
					$this->raisPhisycalError($publisher, 'Could Not add the account , please try again', 'create');
					
				} else {
					$publisher->setFtpUsername($username);
					$publisher->setFtpPassword($pass);
					$publisher->setUnixGid($userArray['uid']);
					$publisher->setUnixUid($userArray['gid']);
					$publisher->setNagiosFilePath(NAGIOSPATH . $publisher->getId() . '/');
					$publisher->save();
					
					//optional parameters
					return $this->redirect('publisher/edit?id='.$publisher->getId());
				}
				
			} else {
				return $this->redirect('publisher/edit?id='.$publisher->getId());
			}
			
		}
	}// End function execute update

	
	
	public function executeDelete()
	{
		$publisher = PublisherPeer::retrieveByPk($this->getRequestParameter('id'));
		$this->publisher = $publisher;
		$this->forward404Unless($publisher);
		$this->msg = 'Deleting a publisher is not allowed.';
		$this->setTemplate('edit');
	}
	
	
	
	public function executeCreateTitle() 
	{
		$this->redirect("title/create?publisher_id=" . $this->getRequestParameter('id'));
	}
	
	
	
	/**
	 * Show change publisher password form
	 *
	 */
	public function executePassword()
	{
		$publisher = PublisherPeer::retrieveByPk($this->getRequestParameter('id'));
		$this->forward404Unless($publisher);
		
		$this->publisher = $publisher;
		$this->msg   	 = $this->getRequestParameter('msg');
	}
	
	
	
	public function executeChangePassword()
	{
		$publisher 	= PublisherPeer::retrieveByPk($this->getRequestParameter('id'));
		$this->forward404Unless($publisher);

		
		$newPassword = $this->getRequestParameter('new_password');
		$confirm	 = $this->getRequestParameter('confirm');
		
		require_once LINUX_SCRIPTS_PATH . 'standerd.php';
		$standers = new Standerd();
		$msg 	  = $standers->getPasswordErrorV2($newPassword, $confirm);
		
		if(!$msg) {
			
			$username 	= $publisher->getFtpUsername();
			$exe 		=  LINUX_USER_MOD .' ' . LINUX_SCRIPTS_PATH . "chng_synd_pswd.sh $username $newPassword";
						
			exec($exe, $return);
									
			if(!$exe) {
				$msg = 'Could not change password';
			}else {
				$msg = 'Password has been changed';
				$publisher->setFtpPassword($newPassword);
				$publisher->save();
			}
		}
						
		$this->publisher = $publisher;
		$this->msg 	 	 = $msg;
		$this->setTemplate('password');
	}
	
	
	//////////////////////////
	//functions 
		
	public function raisPhisycalError(&$publisherObject, $errMsg, $templateSet) 
	{
		$this->publisher = clone $publisherObject;
				
		if('create' ==$templateSet) {
			$publisherObject->delete();
			$this->publisher->setId('');
		}
  				
		$this->msg = $errMsg;
		$this->setTemplate($templateSet);
  	}
  	
}
