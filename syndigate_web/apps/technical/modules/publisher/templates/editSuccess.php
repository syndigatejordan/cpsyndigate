<?php 
use_helper('Object');  
use_helper("Javascript"); 

include_once LINUX_SCRIPTS_PATH . 'database_helper.php';
require_once LINUX_SCRIPTS_PATH . 'standard.php';

$standard = new Standard();
$dbHelper = new dataBaseHelper();
?>

<div>
	<b><font color="Red"><?php echo isset($msg)? $msg : '';?></font></b>
	<br>
	<br>
</div>
<h1>Publisher <?php echo $publisher ?></h1> <br>

<?php 
echo form_tag('publisher/update');
echo object_input_hidden_tag($publisher, 'getId');
?>

<table>
<tbody>
	<tr>
		<th>Name:</th>
		<td><?php echo object_input_tag($publisher, 'getName', array (  'size' => 80,)) ?></td>
	</tr>
	<tr>
		<th>Syndigate username:</th>
		<td><?php echo $publisher->getFtpUsername(); ?></td>
	</tr>
	
	<tr>
		<th>Syndigate password:</th>
		<td><?php echo $publisher->getFtpPassword(); ?></td>
	</tr>
		
	<tr>
		<th>Website:</th>
		<td><?php echo object_input_tag($publisher, 'getWebsite', array (  'size' => 80,)) ?></td>
	</tr>
	
	<tr>
  		<th>Country:</th>
  		<td><?php echo object_select_tag($publisher, 'getCountryId', array ('related_class' => 'Country',)) ?></td>
  	</tr>

  	
	<tr>
		<th>Logo path</th>
		<td><?php echo object_input_tag($publisher, 'getLogoPath', array('readonly'=>'true')); ?><input type="button" value="Reset" onclick="javaScript: document.getElementById('logo_path').value='<?php echo $publisher->getLogoPath(); ?>'"> <input type="button" value="Clear" onclick="javaScript: document.getElementById('logo_path').value=''">  <input type="button" value="change" onClick="window.open('<?php echo DOMAIN_NAME; ?>/<?php echo  SF_APP . '.php/upload/index' ?>','mywindow','width=400,height=200, left=400, top=300')">	</td>
	</tr>

		
	<tr>
		<th>Notes:</th>
		<td><?php echo object_textarea_tag($publisher, 'getNotes', array (  'size' => '92x3',)) ?></td>
	</tr>
	
	<tr>
		<th>Contract</th>
  		<td><?php echo select_tag('contract', options_for_select( $dbHelper->getEnum('publisher', 'contract') , $publisher->getContract() ));?></td>
	</tr>

	<tr>
  		<th>Contract Start Date:</th>
  		<td><?php echo input_date_tag('contract_start_date', $publisher->getContractStartDate(), 'rich=true readonly=readonly ')." " ?><a  onclick="javascript: document.getElementById('contract_start_date').value=''">clear</a></td>
	</tr>
  	
	<tr>
		<th>Contract End Date:</th>
		<td><?php echo input_date_tag('contract_end_date', $publisher->getContractEndDate(), 'rich=true readonly=readonly ')." " ?><a  onclick="javascript: document.getElementById('contract_end_date').value=''">clear</a></td>
	</tr>

	<tr>
    	<th>Renewal Period</th>
  		<td><?php echo object_input_tag($publisher, 'getRenewalPeriod', array('size'=>67,)) ?></td>
	</tr>

	<tr>
		<th>Royalty Rate</th>
		<td><?php echo object_input_tag($publisher, 'getRoyaltyRate', array (  'size' => 7,)) ?> &nbsp; <b>%</b></td>
	</tr>
	
	<tr>
		<th>Account manager:</th>
		<td><?php echo object_select_tag($publisher, 'getAdminsId', array ('related_class' => 'Admins',)) ?></td>
	</tr>	
</tbody>
</table>
<hr />

<div style="width:50%;">
	<?php echo submit_tag('save') ?> &nbsp; <?php echo button_to('cancel', 'publisher/list') ?>
</div>
<div style="width:50%; float:right;" align="right">
	<?php echo button_to('create Title', 'title/create?publisher_id=' . $publisher->getId()) ?>
&nbsp; <?php echo button_to('Publisher Titles', 'title/listAdmin?publisher_id=' . $publisher->getId()) ?>
</div>


</form>



<br />
<br />
<br />
<br />
<!--bank details -->
<div id="bank_details"></div>
<?php
	echo javascript_tag(remote_function(array(
								'update'  => "bank_details",
				 				'url'    => "publisherBankDetails/edit?publisher_id=" . $publisher->getId() ,   )) );
	?>
</div>



<br />
<br />
<br />
<br />
<br />
<br />

<!--publisher titles -->
<div id="publisher_titles"></div>
<?php
	echo javascript_tag(remote_function(array(
								'update'  => "publisher_titles",
				 				'url'    =>  "title/list?publisher_id=" . $publisher->getId(),   )) );
	?>
</div>



<br />
<br />
<br />
<br />
<br />
<br />



<div id="contacts">

<h1><a name= 'contacts'>Contacts</a></h1>
<hr>
<div id="add_contact"></div>
<div id="show_add_contact" on>
	<?php /* echo button_to_remote('add Contact', array(
    	'update'   => 'add_contact',
    	'url'      => 'publisherContacts/create?publisher_id=' . $publisher->getId(),
    	'loading'  => "Element.show('new_contact')",
    	'complete' => "Element.hide('show_add_contact')", ))
    	*/
	?>
</div>	



	<?php
		echo "<div id=\"contacts_table\"></div>";		
		echo javascript_tag(remote_function(array( 'update'  => "contacts_table", 'url'    => "publisherContacts/list?publisher_id={$publisher->getId()}"   ,   )) );
		echo '<br /><br />';
	?>
</div>




<br />
<br />
<br />
<br />


<div id="offices">

<h1><a name="offices">Offices</a></h1>
<hr>
<div id="add_office"></div>
<div id="show_add_office">
	<?php /*
	 echo button_to_remote('add office', array(
    	'update'   => 'add_office',
    	'url'      => 'publisherOffices/create?publisher_id=' . $publisher->getId(),
    	'loading'  => "Element.show('new_office')",
    	'complete' => "Element.hide('show_add_office')",
	))
	*/
	?>
</div>	


	<?php
		echo "<div id=\"offices_table\"></div>";		
		echo javascript_tag(remote_function(array( 'update'  => "offices_table", 'url'    => "publisherOffices/list?publisher_id={$publisher->getId()}"   ,   )) );
		echo '<br /><br />';
 	?>
</div>

<script type='text/javascript'>
	function refreshUploader(){
		u = document.getElementById('uploader');
		u.innerHTML="<input type='file' />";
	}
	
	
	function checkContact()
	{
		name  		= document.getElementById('full_name').value;
		mobile 		= document.getElementById('mobile_number').value;
		email 		= document.getElementById('email').value;
		notifySms	= document.getElementById('notification_sms').checked;
		notifyEmail = document.getElementById('notification_email').checked;
		
		if(name == '') {
			return 'Full name not provided';
		} else {
			
			if( (notifySms) && ( mobile == '' ) ) {
				return 'Mobile number not enterd to send SMS';
			} else {
				if( (notifyEmail) &&( email== '') ) {
					return 'Email Not Entered to send messages';
				} else {
					return null;
				}
			}
		}
		
	}
	
	
	
	
	function refreshAjax(divId, url) {
		
		var xmlHttp;
		
		try {
			xmlHttp=new XMLHttpRequest();
		} catch (e) {
			try {
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
					xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {
					alert("Your browser does not support AJAX!");
					return false;
				}
			}
		}
		
		xmlHttp.onreadystatechange = function() {
			document.getElementById(divId).innerHTML = xmlHttp.responseText;
		}
		
		xmlHttp.open("GET", "<?php echo DOMAIN_NAME ?>/technical.php/" + url, true);
		xmlHttp.send(null);
	}
</script>
