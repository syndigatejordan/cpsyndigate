<h1>Publishers List</h1>
<br>
<div align="right"><?php echo button_to ('New Publisher', 'publisher/create') ?></div>
<table border="1" width="100%">
<thead>
<tr>
  <th>Id</th>
  <th>Name</th>
  <th>Titles</th>
  <th>Notes</th>
</tr>
</thead>
<tbody>
<?php foreach ($publishers as $publisher): ?>
<tr>
      <td align="center"><?php echo link_to($publisher->getId(),   'publisher/edit?id='.$publisher->getId()) ?></td>
      <td><?php echo link_to($publisher->getName(), 'publisher/edit?id='.$publisher->getId()) ?></td>
      <td align="center"><?php echo link_to($publisher->getTitlesCount(),'title/list?publisher_id='.$publisher->getId()) ?></td>
      <td><?php echo $publisher->getNotes() ?></td>
</tr>
<?php endforeach; ?>

</tbody>
</table>

<div align="right"><?php echo button_to ('New Publisher', 'publisher/create') ?></div>