<div>
	<b><font color="Red"><?php echo isset($msg)? $msg : '';?></font></b> 
	<br>
	<br>
</div>

<?php
use_helper('Object');

include_once LINUX_SCRIPTS_PATH . 'database_helper.php';
require_once LINUX_SCRIPTS_PATH . 'standard.php';

$standard = new Standard();
$dbHelper = new dataBaseHelper();

echo form_tag('publisher/update');
echo object_input_hidden_tag($publisher, 'getId');

?>

<h1>Create Publisher</h1> 
<br>

<table>
<tbody>

<tr>
  <th>Name:</th>
  <td><?php echo object_input_tag($publisher, 'getName', array (  'size' => 80, 'onkeyup'=>"javascript: theName = document.getElementById('name').value;  ajaxSuggestUsername(theName, 'suggestedPubName')")) ?></td>
</tr>
<tr>
  <th>Syndigate username:</th>
  <td><?php echo object_input_tag($publisher, 'getFtpUsername', array (  'size' => 30, 'maxlength' =>8)) ?></td>
</tr>
<tr>
  <th>Syndigate password:</th>
  <td><?php echo '' !=  trim($publisher->getFtpPassword()) ? object_input_tag($publisher, 'getFtpPassword', array (  'size' => 30, 'maxlength'=> 8, )) : input_tag('ftp_password', $standard->getRandomPassword(6), 'maxlength=8') ?></td>
</tr>

<tr>
  <th>Website:</th>
  <td><?php echo object_input_tag($publisher, 'getWebsite', array (  'size' => 80,)) ?></td>
</tr>

<tr>
  	<th>Country:</th>
  	<td><?php echo object_select_tag($publisher, 'getCountryId', array ('related_class' => 'Country',)) ?></td>
</tr>

<tr>
	<th>Logo path</th>
	<td><?php echo object_input_tag($publisher, 'getLogoPath', array('readonly'=>'true')); ?> <input type="button" value="Clear" onclick="javaScript: document.getElementById('logo_path').value=''">  <input type="button" value="change" onClick="window.open('<?php echo DOMAIN_NAME; ?>/<?php echo  SF_APP . '.php/upload/index' ?>','mywindow','width=400,height=200, left=400, top=300')">	</td>
</tr>
	
<tr>
  <th>Notes:</th>
  <td><?php echo object_textarea_tag($publisher, 'getNotes', array (  'size' => '92x3',)) ?></td>
</tr>

<tr>
  <th>Contract</th>
  <td><?php echo select_tag('contract', options_for_select( $dbHelper->getEnum('publisher', 'contract') , $publisher->getContract() ));?></td>
</tr>

<tr>
  <th>Contract Start Date:</th>
  <td><?php echo input_date_tag('contract_start_date', $publisher->getContractStartDate(), 'rich=true readonly=readonly ')." " ?><a  onclick="javascript: document.getElementById('contract_start_date').value=''">clear</a></td>
</tr>
  	
<tr>
	<th>Contract End Date:</th>
	<td><?php echo input_date_tag('contract_end_date', $publisher->getContractEndDate(), 'rich=true readonly=readonly ')." " ?><a  onclick="javascript: document.getElementById('contract_end_date').value=''">clear</a></td>
</tr>

<tr>
    <th>Renewal Period</th>
  	<td><?php echo object_input_tag($publisher, 'getRenewalPeriod', array('size'=>67,)) ?></td>
</tr>

<tr>
	<th>Royalty Rate</th>
	<td><?php echo object_input_tag($publisher, 'getRoyaltyRate', array (  'size' => 7,)) ?> &nbsp; <b>%</b></td>
</tr>

<tr>
	<th>Account manager:</th>
	<td><?php echo object_select_tag($publisher, 'getAdminsId', array ('related_class' => 'Admins',)) ?></td>
</tr>	
  	
</tbody>
</table>
<hr />
<?php 
echo submit_tag('save');
if ($publisher->getId()): ?>
  &nbsp;<?php echo button_to('Cancel', 'publisher/show?id='.$publisher->getId()) ?>
<?php else: ?>
  &nbsp;<?php echo button_to('Cancel', 'publisher/list') ?>
<?php endif; ?>
</form>


<script>
	function ajaxSuggestUsername(name, method) {
		
		var xmlHttp;

		try {
			xmlHttp=new XMLHttpRequest();
		} catch (e) {
			try {
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
					xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {
					alert("Your browser does not support AJAX!");
					return false;
				}
			}
		}
		
		xmlHttp.onreadystatechange = function() {
			document.getElementById('ftp_username').value = xmlHttp.responseText;
		}
		
		xmlHttp.open("GET", "<?php echo DOMAIN_NAME ; ?>/ajax.php?method=" + method + "&text=" + name, true);
		xmlHttp.send(null);
	}
</script>
