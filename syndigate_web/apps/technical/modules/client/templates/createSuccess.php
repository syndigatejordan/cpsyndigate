<h1>Create Client</h1>
<font color="Red"><?php echo isset($msg)? $msg : ''; echo '<br>' ?></font>


<?php
use_helper('Object');
use_helper('Javascript');  
include_once LINUX_SCRIPTS_PATH . 'database_helper.php';
include_once LINUX_SCRIPTS_PATH . 'standard.php'; 

$standard = new Standard();
$dbHelper = new dataBaseHelper();

echo form_tag('client/update');
echo object_input_hidden_tag($client, 'getId') ?>

<table>
<tbody>
	<tr>
  		<th>Client name*:</th>
  		<td><?php echo object_input_tag($client, 'getClientName', array ('size' => 80, )) ?></td>
	</tr>
	
	<tr>
  		<th>Syndigate username*:</th>
  		<td><font color="Red"><b>cl_</b></font><?php echo object_input_tag($client, 'getFtpUsername', array (  'size' => 30, 'maxlength'=>8)) ?></td>
	</tr>
	
	<tr>
  		<th>Syndigate password*:</th>
  		<td><?php echo '' !=  trim($client->getFtpPassword()) ? object_input_tag($client, 'getFtpPassword', array (  'size' => 30, 'maxlength'=>8)) : input_tag('ftp_password', $standard->getRandomPassword(6), 'maxlength=8') ?></td>
	</tr>
	
	<tr>
		<th>Is active:</th>
  		<td><?php echo object_checkbox_tag($client, 'getIsActive', array (  'size' => 7,)) ?></td>
	</tr>
	
	<tr>
		<th>Logo path</th>
		<td><?php echo object_input_tag($client, 'getLogoPath', array('readonly'=>'true')); ?> <input type="button" value="Clear" onclick="javaScript: document.getElementById('logo_path').value=''">  <input type="button" value="change" onClick="window.open('<?php echo DOMAIN_NAME; ?>/<?php echo  SF_APP . '.php/upload/index' ?>','mywindow','width=400,height=200, left=400, top=300')">	</td>
	</tr>
	
	<tr>
  		<th>Contract</th>
  		<td><?php echo select_tag('contract', options_for_select( $dbHelper->getEnum('client', 'contract') , $client->getContract() ));?></td>
  	</tr>

	<tr>
  		<th>Contract Start Date:</th>
  		<td><?php echo input_date_tag('contract_start_date', $client->getContractStartDate(), 'rich=true readonly=readonly ')." " ?><a  onclick="javascript: document.getElementById('contract_start_date').value=''">clear</a></td>
	</tr>
  	
	<tr>
		<th>Contract End Date:</th>
  		<td><?php echo input_date_tag('contract_end_date', $client->getContractEndDate(), 'rich=true readonly=readonly ')." " ?><a  onclick="javascript: document.getElementById('contract_end_date').value=''">clear</a></td>
  	</tr>
  	
  	<tr>
  		<th>Renewal Period</th>
  		<td><?php echo object_input_tag($client, 'getRenewalPeriod', array('size'=>67,)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Royalty Rate</th>
  		<td><?php echo object_input_tag($client, 'getRoyaltyRate', array (  'size' => 7,)) ?> <b>%</b></td>
  	</tr>
	
	<tr>
  		<th>Directory layout*:</th>
  		<td><?php echo select_tag('dir_layout', options_for_select($dbHelper->getEnum('client', 'dir_layout' ), $client->getDirLayout() ));?></td>
	</tr>
		
	
	<tr>
  		<th>Connection type*:</th>
  		<td><?php echo select_tag('connection_type', options_for_select($dbHelper->getEnum('client', 'connection_type' ), $client->getConnectionType() ));?></td>
	</tr>
		
	
	<tr>
  		<th>Url*:</th>
  		<td><?php echo object_input_tag($client, 'getUrl', array ('size' => 80,)) ?></td>
	</tr>

	<tr>
  		<th>Username*:</th>
  		<td><?php echo object_input_tag($client, 'getUsername', array ( 'size' => 30, 'maxlength' => 12,)) ?></td>
	</tr>
	
	<tr>
  		<th>Password*:</th>
  		<td><?php echo object_input_tag($client, 'getPassword', array (  'size' => 30, 'maxlength' => 12,)) ?></td>
	</tr>
			
	<tr>
  		<th>Generate</th>
  		<td><?php echo select_tag('generate_type', options_for_select($dbHelper->getEnum('client', 'generate_type' ), $client->getGenerateType() ));?></td>
  	</tr>
		
</tbody>
</table>
<hr />
<?php echo submit_tag('save') ?> &nbsp;<?php echo button_to('cancel', 'client/list') ?>
</form>
