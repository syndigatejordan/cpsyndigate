<div id="ajax_div_<?php echo $counter ?>">
	<div align="right"><img src="<?php echo sfConfig::get('sf_admin_web_dir').'/images/cancel.png'?>" onclick="javascript: delBackupConnection('div_<?php echo $counter ?>')"></div>
	<input type="hidden" name="connection_id[<?php echo $counter ?>]" value="<?php echo $connection->getId() ?>">
	<table>
		<tr>
			<td colspan="2"><font color="Red"><?php echo $err[$counter] ?></font></td>
		</tr>
		
		<tr>
			<th>Type :</th>
  			<td>ftp_push</td>
		</tr>
				
		<tr>
			<th>Connection ID:</th>
  			<td><?php echo $connection->getId();?></td>
		</tr>
		
		<tr>
			<th>Url*:</th>
			<td><?php echo input_tag("conn_url[$counter]", $connection->getUrl(), array (  'size' => 80,)) ?></td>	
		</tr>
		
		<tr>
			<th>Username*:</th>
			<td><?php echo input_tag("conn_username[$counter]", $connection->getUsername(), array (  'size' => 80,)) ?></td>
		</tr>
		
		<tr>
			<th>Password*:</th>
			<td><?php echo input_tag("conn_password[$counter]", $connection->getPassword(), array (  'size' => 80,)) ?></td>
		</tr>
		
		<tr>
			<th>Remote dir:</th>
			<td><?php echo input_tag("conn_remote_dir[$counter]", $connection->getRemoteDir(), array (  'size' => 80,)) ?></td>
		</tr>
		
		<tr>
			<th>Ftp Port:</th>
			<td><?php echo input_tag("conn_ftp_port[$counter]", $connection->getFtpPort(), array (  'size' => 10,)) ?></td>
		</tr>
		
	</table>

</div>