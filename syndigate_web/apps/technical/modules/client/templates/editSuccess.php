<h1>Edit Client</h1>
<font color="Red"><?php echo isset($msg)? $msg : ''; echo '<br>' ?></font>

<div align="right">	
	<?php echo link_to('Delete', 'client/delete?id=' . $client->getId(), array('id' => 'del_client_link', 'confirm' => 'Delete this Client ?')); ?>
  <span style="margin-left: 10px;"><?php echo button_to ('Logos Client', 'client/LogoTiles?id=' . $client->getId()) ?></span>
</div>

<?php
use_helper('Object');
use_helper('Javascript');
include_once LINUX_SCRIPTS_PATH . 'database_helper.php';
$dbHelper = new dataBaseHelper();



echo form_tag('client/update'); 
echo object_input_hidden_tag($client, 'getId') ?>

<table>
<tbody>
	
	<tr>
  		<th>Client name*:</th>
  		<td><?php echo object_input_tag($client, 'getClientName', array (  'size' => 80,)) ?></td>
	</tr>
	
	<tr>
		<th>Syndigate username*:</th>
  		<td><?php echo $client->getFtpUsername();?></td>
	</tr>

	<tr>
  		<th>Syndigate password*:</th>
  		<td><?php echo $client->getFtpPassword(); ?></td>
	</tr>
	
	<tr>
		<th>Is active:</th>
  		<td><?php echo object_checkbox_tag($client, 'getIsActive', array (  'size' => 7,)) ?></td>
	</tr>
	
	<tr>
		<th>Logo path</th>
		<td><?php echo object_input_tag($client, 'getLogoPath', array('readonly'=>'true')); ?><input type="button" value="Reset" onclick="javaScript: document.getElementById('logo_path').value='<?php echo $client->getLogoPath(); ?>'"> <input type="button" value="Clear" onclick="javaScript: document.getElementById('logo_path').value=''">  <input type="button" value="change" onClick="window.open('<?php echo DOMAIN_NAME; ?>/<?php echo  SF_APP . '.php/upload/index' ?>','mywindow','width=400,height=200, left=400, top=300')">	</td>
	</tr>
	
	<tr>
  		<th>Contract</th>
  		<td><?php echo select_tag('contract', options_for_select( $dbHelper->getEnum('client', 'contract') , $client->getContract() ));?></td>
  	</tr>
	
	<tr>
  		<th>Contract Start Date:</th>
  		<td><?php echo input_date_tag('contract_start_date', $client->getContractStartDate(), 'rich=true readonly=readonly ')." " ?><a  onclick="javascript: document.getElementById('contract_start_date').value=''">clear</a></td>
	</tr>
  	
	<tr>
		<th>Contract End Date:</th>
  		<td><?php echo input_date_tag('contract_end_date', $client->getContractEndDate(), 'rich=true readonly=readonly ')." " ?><a  onclick="javascript: document.getElementById('contract_end_date').value=''">clear</a></td>
  	</tr>
  	
  	<tr>
  		<th>Renewal Period</th>
  		<td><?php echo object_input_tag($client, 'getRenewalPeriod', array('size'=>67,)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Royalty Rate</th>
  		<td><?php echo object_input_tag($client, 'getRoyaltyRate', array (  'size' => 7,)) ?> <b>%</b></td>
  	</tr>

	<tr>
  		<th>Directory layout*:</th>
  		<td><?php echo select_tag('dir_layout', options_for_select($dbHelper->getEnum('client', 'dir_layout' ), $client->getDirLayout() ));?></td>
	</tr>
	
	<tr>
  		<th>Generate :</th>
  		<td><?php echo select_tag('generate_type', options_for_select($dbHelper->getEnum('client', 'generate_type' ), $client->getGenerateType() ));?></td>
  	</tr>
	
	<tr>
  		<th>Connection type*:</th>
  		<td><?php echo select_tag('connection_type', options_for_select($dbHelper->getEnum('client', 'connection_type' ), $client->getConnectionType() ));?></td>
	</tr>
	
	
	<tr>
  		<th>Url*:</th>
  		<td><?php echo object_input_tag($client, 'getUrl', array (  'size' => 80,)) ?></td>	
	</tr>

	<tr>
  		<th>Username*:</th>
  		<td><?php echo object_input_tag($client, 'getUsername', array (  'size' => 80,)) ?></td>
	</tr>
	
	<tr>
  		<th>Password*:</th>
  		<td><?php echo object_input_tag($client, 'getPassword', array (  'size' => 80,)) ?></td>
	</tr>
	 	
  	<tr>
  		<th valign="top">Connections</th>
  		<td>
  			<?php
				echo "<div id=\"div-connections\"></div>";
				echo javascript_tag(remote_function(array( 'update'  => "div-connections", 'url'  => "/client/clientConnections?id={$client->getId()}&connections=" . urlencode(serialize($connections)) . "&err=" . urlencode(serialize($err)),   )) );
	?>
  		</td>
  	</tr>
  	
  		
	<tr>
  		<th>Notes</th>
  		<td><?php echo object_textarea_tag($client, 'getNotes', array('size'=>80,)) ?></td>
  	</tr>
  	
</tbody>
</table>
<hr />
<?php echo submit_tag('save') ?> &nbsp; <?php echo button_to('cancel', 'client/list') ?>

</form>


<br />
<br />
<br />
<br />
<div id="contacts">

<h1>Contacts</h1>
<hr>
<div id="add_contact"></div>
<div id="show_add_contact">
	<?php 
	/*
	echo button_to_remote('add Contact', array(
	 						'update'   => 'add_contact',
    						'url'      => 'clientContacts/create?client_id=' . $client->getId(),
    						'loading'  => "Element.show('new_contact')",
    						'complete' => "Element.hide('show_add_contact')",));
    						*/
    ?>
</div>



	<?php
		echo "<div id=\"contacts_table\"></div>";
		echo javascript_tag(remote_function(array( 'update'  => "contacts_table", 'url'    => "clientContacts/list?client_id={$client->getId()}"   ,   )) );
	?>
</div>




<br />
<br />
<br />
<br />




<!--
<h1 id="rules">Filters and Rules</h1>
<hr>

<div id="show_add_rule">
	<?php
	/**
	echo button_to_remote('add Rule', array(
	 						'update'   => 'add_rule',
    						'url'      => 'clientRule/create?client_id=' . $client->getId(),
    						'loading'  => "Element.show('new_rule')",
    						'complete' => "Element.hide('show_add_rule')",));
	**/
    ?>
</div>
<div id="add_rule"></div>

<br>
<br>
<br>

	<?php
		/**
		$clientRules = array_reverse($client->getClientRules());
				
		if(count($clientRules)==0) {
			echo 'No Filter Rules for this client.';
		} else {
			
			foreach ($clientRules as $clientRule) {
				
				$ruleId = $clientRule->getId();
				
				echo "<fieldset> <legend><b>Rule # {$clientRule->getId()}</b></legend>";
				echo "<div id=\"rule_$ruleId\" style=\" padding:20px; background-color: 	#FFFFFF \" ></div>";
				echo javascript_tag(remote_function(array( 'update'  => "rule_$ruleId", 'url'    => "clientRule/edit?id=$ruleId&client_id=" . $client->getId() ,   )) );
				echo '</fieldset><br /><br /><br />';
			}
 		}
 		**/
	?>
</div>
-->


<h1 id="ruless">Titles receiving</h1>
<hr>

<div id="titles_channels" style=" padding:20px; background-color: 	#FFFFFF " >
	<?php 
	echo javascript_tag(remote_function(array( 'update'  => "titles_channels", 'url'    => "titlesClientsRules/showclienttitles?client_id=" . $client->getId() ,   )) );
    ?>
</div>



<br />
<br />
<br />
<br />
<script>
function checkContact()
	{
		name  		= document.getElementById('full_name').value;
		mobile 		= document.getElementById('mobile_number').value;
		email 		= document.getElementById('email').value;
		notifySms	= document.getElementById('notification_sms').checked;
		notifyEmail = document.getElementById('notification_email').checked;
		
		if(name == '') {
			return 'Full name not provided';
		} else {
			
			if( (notifySms) && ( mobile == '' ) ) {
				return 'Mobile number not enterd to send SMS';
			} else {
				if( (notifyEmail) &&( email== '') ) {
					return 'Email Not Entered to send messages';
				} else {
					return null;
				}
			}
		}
		
	}
	
	
function clearAllDivsBut(filterType)
{
	alert(filterType);
	ruleDivId = document.getElementById(filterType).parentNode.id;
	alert(ruleDivId);
	
}

function delBackupConnection(divToDelId)
{
	var container = document.getElementById('div-connections');  
	container.removeChild(document.getElementById(divToDelId));
}
</script>
