<?php 
if($client) {
	?><h1>Check sent content For client <?php echo $client->getClientName() ?></h1><?php
} else {
	?>
	<h1>Check sent content For client </h1>
	<?php
}



use_helper('Object');  
use_helper("Javascript"); 

echo form_tag('client/checkSentContent');
?>
<select name="client_id">
	<option value="" selected>Please select a client</option>
	<?php
	foreach ($clients as $c) {
		$selectd = $c->getId() == $clientId ? 'selected' : '';
		?><option value="<?php echo $c->getId()?>" <?php echo $selectd ?> ><?php echo $c->getClientName()?></option>
		<?php
	}
	?>
</select>
<?php 
echo input_date_tag('date', $date, 'rich=true readonly=readonly ')." " ?><a onclick="javascript: document.getElementById('date').value=''">clear</a>
<?php
 
if(!$date) {
	?><b> <font color="Red">Please select a Date :</font></b><?php 
}


?>

<input type="submit" value="Update">
<br />
<br />

<?php if($articlesCount) {?>
<table>
	<tr>
		<th> File </th>
		<th>Articles count</th>
	</tr>
	
	<?php
	foreach ($articlesCount as $file => $count) {
		?>
		<tr>
			<td><?php echo $file?></td>
			<td align="right"><?php echo $count?></td>
		</tr>
		<?php
	}
	?>
</table>
<?php } else {
	echo "No Files Found.";
}
?>
