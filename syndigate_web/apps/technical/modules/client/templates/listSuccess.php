<h1>Clients</h1>
<div align="right"><?php echo button_to ('New Client', 'client/create') ?></div>
<table border="1" width="100%">
<thead>
<tr>
  <th>Id</th>
  <th>Client name</th>
  <th>Connection type</th>
</tr>
</thead>
<tbody>
<?php foreach ($clients as $client):
	$class = $client->getIsActive()? '' : 'class=inactive_client';
?>
<tr>
      <td align="center"><?php echo link_to($client->getId(), 'client/edit?id='.$client->getId()) ?></td>
      <td               ><?php echo link_to($client->getClientName(), 'client/edit?id='.$client->getId(), $class) ?></td>
      <td align="center"><?php echo $client->getConnectionType() ?></td>
</tr>
<?php endforeach; ?>
</tbody>
</table>

<div align="right"><?php echo button_to ('New Client', 'client/create') ?></div>

<ul>
	<li class="inactive_client">Inactive clients</li>
	<li style="color:blue">active clients</li>
</ul>
