<?php

/**
 * reportGather actions.
 *
 * @package    syndigate
 * @subpackage reportGather
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class reportGatherActions extends autoreportGatherActions
{
	/**
	 * Deletion Not allowed
	 *
	 */
	public function executeDelete()
	{
		$this->redirect('reportGather/list');
	}
}
