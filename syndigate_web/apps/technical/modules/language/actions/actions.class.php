<?php

/**
 * language actions.
 *
 * @package    syndigate
 * @subpackage language
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class languageActions extends autolanguageActions
{
	public function executeDelete()
	{
		$this->redirect('language/list');
	}
}
