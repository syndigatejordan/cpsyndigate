<?php use_helper('Object') ?>
<?php include_partial('menu'); ?>
<hr />

<h3>Clients FTP push</h3>
<font color='red'><h3>This feature is not checked yet, let mohammad check it when he has time</h3></font>
<form action="" method="GET">
	<table>
            
            <tr>
                <td><select name='filter' />
                       <option value="CREATION_TIME">Creation time</option>
                        <option value="SENT_TIME" <?php echo isset($filter) && 'SENT_TIME' == $filter ? "SELECTED" : '' ?> >Sent time</option>
                    </select>
                </td>
            </tr>
            <tr><td><?php echo input_date_tag('dateFrom', isset($dateFrom) ? $dateFrom : null, array ( 'rich' => true, 'calendar_button_img' => '/sf/sf_admin/images/date.png',)) ?></td></tr>
            <tr><td><?php echo input_date_tag('dateTo', isset($dateTo) ? $dateTo : null, array ( 'rich' => true, 'calendar_button_img' => '/sf/sf_admin/images/date.png',)) ?></td></tr>
            <tr><td><?php echo select_tag('titleId', options_for_select($titles,  $titleId  )); ?></td></tr>
            <tr><td><?php echo select_tag('clientId',options_for_select($clients, $clientId )); ?></td></tr>
            <tr><td><input type="submit" value="Submit" /></td></tr>
	</table>
</form>


<table cellspacing="0" class="sf_admin_list" border="1">
<thead>
<tr>
        <th>ID</th>
	<th>Creation time</th>        
        <th>Sent time</th>	
	<th>Client Id</th>
        <th>Connection Id</th>
        <th>Client Connection</th>
	<th>Title Id</th>
	<th>Revision</th>
	<th>Local file</th>	
        <th>Sent</th>
        <th>Retries</th>
</tr>
</thead>
<tbody>
	<?php
	$i=1;
	foreach ($files as $file): 
            $odd = fmod(++$i, 2);
            $connection = ClientConnectionPeer::retrieveByPK($file->getClientConnectionId());            
                ?>
            <tr class="sf_admin_row_<?php echo $odd ?>">
                <td align="center"><?php echo $file->getId() ?></td>
                <td align="center"><?php echo date('Y-m-d H:i', $file->getCreationTime())?></td>	
                <td align="center"><?php echo date('Y-m-d H:i', $file->getSentTime())?></td>                        
                <td align="center"><?php echo $file->getClientId()?></td>
                <td align="center"><?php echo $file->getClientConnectionId()?></td>
                <td align="center"><?php echo $connection->getUsername() ?>@<?php echo $connection->getUrl() ?></td>                        
                <td align="center"><?php echo $file->getTitleId()?></td>
                <td align="center"><?php echo $file->getRevision()?></td>
                <td align="center"><?php echo $file->getLocalFile()?></td>                        			
                <td align="center"><?php echo $file->getIsSent() ? 'Yes' : 'No'?></td>
                <td align="center"><?php echo $file->getNumRetry()?></td>		
            </tr>
	<?php endforeach; ?>
</tbody>
<tfoot>
<tr></tr>
</tfoot>
</table>
