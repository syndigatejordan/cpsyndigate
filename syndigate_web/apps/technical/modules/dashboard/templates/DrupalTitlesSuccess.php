<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" ></script>
<script>
	function change_status(title_id) {				
		value = $('#chk_' + title_id).attr('checked')		
		if( 1 == value) {			
			ajax_change_status(title_id, 1);
		} else {			
			ajax_change_status(title_id, 0);
		}
	}
	
	function ajax_change_status(title_id, status_code) {
		
		
		
		data = {};
		data.title_id = title_id;
		data.status_code = status_code;
		
		$.ajax({
  			url: '/index.php/dashboard/DrupalTitleUpdate',
  			data: data,
  			type: "POST",
  			dataType: "json",
  			success: function(json) {
  				
    			if(1 == json.code) {
    				alert('saved.');
    			} else {
    				alert('Error:' + json.message);
    			}
  			},
  			error: function(data) {
  				alert('could not do the request, try again');
  			}
		});
	}
</script>

<?php include_partial('menu'); ?>

<h1>Syndigate Drupal Titles</h1>
<hr>

<table border="1">
	<tr>
		<td align="center">#</td>
		<td>Title</td>
		<td>Publisher</td>
		<td>language</td>
		<td>Type</td>
		<td>Frequency</td>
		<td>Active</td>
		
	</tr>
	<?php 
	foreach ($titles as $title) {
	?>
	<tr>
		<td width="1 px" align="center"><input type="checkbox" id="chk_<?php echo $title->getId()?>" name="chk[<?php echo $title->getId()?>]" <?php echo in_array($title->getId(), $drupalIds) ? 'checked' : '' ?>  onchange="change_status(<?php echo $title->getId(); ?>);" /></td>
		<td width="30%"><?php echo $title->getName()?></td>
		<td width="20%"><?php echo $publishers[$title->getPublisherId()]?></td>
		<td width="20 px"><?php $lang = @$title->getLanguage() ; echo $lang ? $lang->getName() : '' ?></td>
		<td width="20 px"><?php $type = @$title->getTitleType(); echo $type ? $type->getType() : ''?></td>
		<td width="20 px"><?php $freq = @$title->getTitleFrequency(); echo $freq ? $freq->getFrequencyType(): ''?></td>
		<td width="20 px"><?php echo $title->getIsActive() ? "<font color='green'>Yes</font>" : "<font color='red'>No</font>"?></td>		
	</tr>
	<?php	
	}
	?>
</table>


