<?php include_partial('menu'); ?>

<h1>Titles frequency (Root)</h1>
<hr>

<div>
  <table border="1" align="left" width="70%" style="font-size: 14px; text-align: center;">
    <tr>
      <th></th>
      <th>PUBLISHER ID</th>
      <th>TITLE ID</th>
      <th>Frequency</td>
      <th>Time</th>

    </tr>
    <?php
    $count = 1;
    foreach ($frequencyRoot as $row) {
      ?>
      <tr>
        <td><?php echo $count ?></td>
        <td><?php echo $row ["PUBLISHER_ID"]; ?></td>
        <td><?php echo $row["id"]; ?></td>
        <td><?php echo $row["title_frequency_id"]; ?></td>
        <td><?php echo $row ["frq"]; ?></td>
      </tr>
      <?php
      $count++;
    }
    ?>
  </table>
</div>