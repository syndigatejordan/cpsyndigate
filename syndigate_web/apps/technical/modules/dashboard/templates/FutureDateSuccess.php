<?php include_partial('menu'); ?>

<h1>Titles having Future dates</h1>
<hr>
<?php echo form_tag('dashboard/UpdateDate'); ?>

<div>
  <table border="1" align="left" width="100%">
    <tr>
      <th></th>
      <th>ID</th>
      <th>TITLE ID</th>
      <th>Date</td>
      <th>Parsed at</th>
      <th>Headline</th>
      <th>Link</th>

    </tr>
    <?php foreach ($future_dates as $future_date) { ?>
      <tr>
        <td><input type="checkbox" name="checkbox[]" value="<?php echo $future_date["id"]; ?>" /></td>
        <td><?php echo $future_date ["id"]; ?></td>
        <td><?php echo $future_date["title_id"]; ?></td>
        <td><?php echo $future_date["date"]; ?></td>
        <td><?php echo $future_date ["parsed_at"]; ?></td>
        <td><a href="/index.php/article/show/id/<?php echo $future_date ["id"]; ?>" onmouseover="setObj(description[<?php echo $future_date ["id"]; ?>], 'override', 800, 200)"  onmouseout="clearTimeout(openTimer);
              stopIt()"><?php echo $future_date ["headline"]; ?></a></td>
          <?php
          if (!empty($future_date["reference"])) {
            ?>
          <td><a href="<?php echo $future_date["reference"]; ?>">Link</a></td>
        <?php } else { ?>
          <td></td>
        <?php } ?>
      </tr>
    <?php } ?>
  </table>
</div>
<div>
  <?php echo input_date_tag('date', date("Y-m-d"), 'rich=true readonly=readonly ') . " " ?>
  <input type="submit" name="Update" value="Submit" />
</div>
</form>


