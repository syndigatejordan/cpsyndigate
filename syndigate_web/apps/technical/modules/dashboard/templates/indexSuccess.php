<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" ></script>
<style>
.tabset_tabs	{ margin:0; padding:0; list-style-type:none; position:relative; z-index:2; white-space:nowrap }
.tabset_tabs li	{ margin:0; padding:0; display:inline }
.tabset_tabs a	{ color:#339 ! important; background-color:#def ! important; border:1px solid #99c; text-decoration:none; padding:0 0.6em; border-left-width:0; border-bottom:none }
.tabset_tabs a:hover	{ color:#00c ! important; background-color:#eff ! important }
.tabset_tabs a.active	{ color:black ! important; background-color:white ! important; border-color:black; border-left-width:1px; cursor:default; border-bottom:white; padding-top:1px; padding-bottom:1px }
.tabset_tabs li.firstchild a	{ border-left-width:1px }
.tabset_content	{ border:1px solid black; background-color:white; position:relative; z-index:1; padding:0.5em 1em; display:none }
.tabset_label	{ display:none }
.tabset_content_active	{ display:block }
@media aural{
	.tabset_content,
	.tabset_label	{ display:block }
}
</style>
<script type="text/javascript" src="http://cp.syndigate.info/js/tab_static/addclasskillclass.js"></script>
<script type="text/javascript" src="http://cp.syndigate.info/js/tab_static/attachevent.js"></script>
<script type="text/javascript" src="http://cp.syndigate.info/js/tab_static/addcss.js"></script>
<script type="text/javascript" src="http://cp.syndigate.info/js/tab_static/tabtastic.js"></script>

<?php use_helper('Object') ?>

<?php include_partial('menu'); ?>
<hr />

<ul class="tabset_tabs">
	<li><a href="#tabid1" class="active">Svn Errors</a></li>
	<li><a href="#tabid2">not sent today (for Daily)</a></li>
	<li><a href="#tabid3">Not sent from previous span</a></li>
	<li><a href="#tabid4">Push files not sent</a></li>
	<li><a href="#tabid5">Titles not licenced</a></li>
	<li><a href="#tabid6">Clients not licenced</a></li>

</ul>
<div id="tabid1" class="tabset_content">
   <h2 class="tabset_label">Tab Title1</h2>
		<div>
			<h3>Titles having errors in svn</h3>
			<table>
				<thead>
					<th>title Id</th>
					<th>Error</th>
				</thead>
				
				<tbody>
					<?php
					foreach ($svnErrors as $titleId => $error) {
						?><tr>
							<td valign="top"><?php echo $titleId ;?></td>
							<td><?php echo nl2br($error); ?></td>
						  </tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
</div>

<div id="tabid2" class="tabset_content">
   <h2 class="tabset_label">Tab Title2</h2>   
		<div>
			<h3>Daily titles did not send today</h3>
			<table>
				<thead>
					<th>Id</th>
					<th>Name</th>
				</thead>
				
				<tbody>
					<?php
					foreach ($dailyTitlesLate as $title) {
						?><tr><td><?php echo $title->getId()?></td><td><a href="<?php echo DOMAIN_NAME ?>/title/edit/id/<?php echo $title->getId()?>" ><?php echo $title->getName()?></a></td></tr><?php
					}
					?>
				</tbody>
			</table>
		</div>
</div>

<div id="tabid3" class="tabset_content">
   <h2 class="tabset_label">Tab Title3</h2>
		<div>
			<h3>Titles did not send since previous span</h3>
			<table>
				<thead>
					<th>Id</th>
					<th>Name</th>
					
				</thead>
				
				<tbody>
					<?php
					foreach ( $titlesSpanLate as $title) {
						?><tr><td><?php echo $title->getId()?></td><td><a href="<?php echo DOMAIN_NAME ?>/title/edit/id/<?php echo $title->getId()?>" ><?php echo $title->getName()?></a></td></tr><?php
					}
					?>
				</tbody>
			</table>
		</div>
</div>



<div id="tabid4" class="tabset_content">
   <h2 class="tabset_label">Tab Title4</h2>
		<div>
			<h3>Ftp push files that has not been send yet</h3>
			<br />
	
			<?php
			foreach ($pushFtpFilesNotSent as $clientId => $filesList) {
				?>
				<div>
					<h3>Client id : <?php echo $clientId?></h3>
					
					<table border="1">
						<tr>
							<td>connection id</td>
							<td>Title id</td>
							<td>Revision</td>
							<td>local file</td>
							<td>Retry no</td>									
							<td>First created</td>
							<td>Err msg</td>
							<td>Actions</td>
						</tr>
					
						<?php
						foreach ($filesList as $file) {
							?>
							<tr>
								<td><?php echo $file->getClientConnectionId()?></td>
								<td><?php echo $file->getTitleId()?></td>
								<td><?php echo $file->getRevision()?></td>
								<td><?php echo $file->getLocalFile() ?></td>
								<td id="td_connection_retry_<?php echo $file->getId();?>"><?php echo $file->getNumRetry()?></td>										
								<td><?php echo date('Y-m-d H:i', $file->getCreationTime())?></td>									
								<td><?php echo $file->getErrMsg()?></td>
								<td id="td_connection_reset_<?php echo $file->getId();?>" >
									<?php if($file->getNumRetry() >= 20) { ?>
										<a href="javascript:void(0);" onclick="reset_retry_counter(<?php echo $file->getId(); ?>);">Reset retry</a>
									<?php } else {
										?>&nbsp;<?php
									}
										?>
									</td>
							</tr>
							<?php
						}
						?>	
					</table>
					<br />
					<br />
					
				</div>
				<?php
			}
			?>				
		</div>
</div>




<div id="tabid5" class="tabset_content">
   <h2 class="tabset_label">Tab Title5</h2>
		<div>
			<h3>Titles not licenced to any client (except test client) </h3>
			<br />
				
				<table border="1">
					<tr>
						<td>Title Id</td>
						<td>Is Active</td>	
                                          <td>Title</td>	
					</tr>
				
					<?php
					foreach ($titlesNotLicened as $title) {
						?>
						<tr>
							<td><?php echo $title->getId()?></td>
                                                 <td><?php echo $title->getIsActive()?></td>
							<td><a href="<?php echo DOMAIN_NAME ?>/title/edit/id/<?php echo $title->getId()?>" ><?php echo $title->getName()?></a></td>							
						</tr>
						<?php
					}
					?>	
				</table>
					
		</div>
</div>




<div id="tabid6" class="tabset_content">
   <h2 class="tabset_label">Tab Title6</h2>
		<div>
			<h3>Clients not licenced to any title</h3>
			<br />
				
				<table border="1">
					<tr>
						<td>Client Id</td>
						<td>Client</td>						
					</tr>
				
					<?php
					foreach ( $clientsNotLicened as $client) {
						?>
						<tr>
							<td><?php echo $client->getId()?></td>
							<td><a href="<?php echo DOMAIN_NAME ?>/client/edit/id/<?php echo $client->getId()?>" ><?php echo $client->getClientName()?></a></td>							
						</tr>
						<?php
					}
					?>	
				</table>
					
		</div>
</div>

<script>
	function reset_retry_counter(client_connection_file_id) {
			
		data = {};
		data.client_connection_file_id = client_connection_file_id;
		
		$.ajax({
  			url: '/index.php/dashboard/ResetConnectionFileCounter',
  			data: data,
  			type: "POST",
  			dataType: "json",
  			success: function(json) {
  				
    			if(1 == json.code) {    				
    				$('#td_connection_retry_' + client_connection_file_id ).html('0');
    				$('#td_connection_reset_' + client_connection_file_id ).html('');
    			} else {
    				alert('Error:' + json.message);
    			}
  			},
  			error: function(data) {
  				alert('could not do the request, try again');
  			}
		});
	}
</script>