<?php use_helper('Object') ?>
<?php include_partial('menu'); ?>
<hr />

<h3>Clients FTP pull sent files</h3>
<form action="" method="GET">
	<table>
		<tr><td><?php echo input_date_tag('date', isset($date) ? $date : null, array ( 'rich' => true, 'calendar_button_img' => '/sf/sf_admin/images/date.png',)) ?></td></tr>
		<tr><td><?php echo select_tag('titleId', options_for_select($titles,  $titleId  )); ?></td></tr>
		<tr><td><?php echo select_tag('clientId',options_for_select($clients, $clientId )); ?></td></tr>
		<tr><td><input type="submit" value="Submit" /></td></tr>
	</table>
</form>


<table cellspacing="0" class="sf_admin_list" border="1">
<thead>
<tr>
	<th>Creation time</th>
	<th>Client Id</th>
	<th>Title Id</th>
	<th>Revision</th>
	<th>Local file</th>
	<th>report id</th>
	<th>articles count</th>	
</tr>
</thead>
<tbody>
	<?php
	$i=1;
	foreach ($clientPullFiles as $file): 
		$odd = fmod(++$i, 2) ?>
		<tr class="sf_admin_row_<?php echo $odd ?>">
			<td align="center"><?php echo date('Y-m-d H:i', $file->getCreationTime())?></td>	
			<td align="center"><?php echo $file->getClientId()?></td>
			<td align="center"><?php echo $file->getTitleId()?></td>
			<td align="center"><?php echo $file->getRevision()?></td>
			<td align="center"><?php echo $file->getLocalFile()?></td>
			<td align="center"><?php echo $file->getReportId()?></td>
			<td align="center"><?php echo $file->getArticleCount()?></td>
		</tr>
	<?php endforeach; ?>
</tbody>
<tfoot>
<tr></tr>
</tfoot>
</table>
