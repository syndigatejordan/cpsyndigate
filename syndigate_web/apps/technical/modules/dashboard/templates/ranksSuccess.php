<?php include_partial('menu'); ?>

<div>
	<form method="GET" action="" >
	<table>
		<tr>
			<td>Rank</td>
			<td>
				<select name="rank">
					<?php					
					for($i=1 ; $i<= 10 ; $i++) {
						$selected = $rank == $i ? 'selected' :'';
						?>
						<option value="<?php echo $i?>" <?php echo $selected?>><?php echo $i?></option>
						<?php
					}					
					?>
				</select>
			</td>
			
			<td>&nbsp;</td>
			
			<td>Span</td>
			<td>
				<select name="span">
					<option value="1" >Current</option>
					<option value="2" <?php echo $span==2 ? 'selected' : ''?> >Current & prev</option>
				</select>
			</td>
			
			<td>Frequency</td>
			<td>
				<select name="freq_id">
					<?php					
					foreach ($freqs as $index => $frequency) {
						$selected = isset($freq_id) && $freq_id == $frequency->getId() ? 'selected' : '';
						?>
						<option value="<?php echo $frequency->getId()?>" <?php echo $selected?> ><?php echo $frequency->getFrequencyType()?></a>
						<?php
					}
					?>
				</select>
			</td>
			
			
			<td>&nbsp;</td>
		
			<td><input type="submit" value="submit" /></td>
		</tr>
		
		
	</table>
	</form>
</div>

<div>
<table>
	<thead>
		<th>Title Id</th>
		<th>Title</th>
		<th>Language</th>
		<th>Rank</th>
		<th>Frequency</th>
		<th>Current span</th>		
		<th>Prev span</th>
		<th>Last content</th>
		<th>Last article date</th>
		<th>Comments</th>
	</thead>
	
	<tbody>
	<?php
	foreach ($titles as $title) {
		if(is_object($title)){
			$c = new Criteria();
			$c->add(ArticlePeer::TITLE_ID, $title->getId());
			$c->addDescendingOrderByColumn(ArticlePeer::ID);
			$article = ArticlePeer::doSelectOne($c);
		?>
		<tr>
		<td><?php echo $title->getId()?></td>
		<td><?php echo $title->getName()?></td>
		<td><?php echo $langs[$title->getLanguageId()]->getName();?></td>
		<td><?php echo $title->getRank()?></td>
		<td><?php echo $freqs[$title->getTitleFrequencyId()]->getFrequencyType();?></td>
		<td><?php echo $title->getCachedCurrentSpan()?></td>
		<td><?php echo $title->getCachedPrevSpan()?></td>
		<td><?php echo $title->getLastContentTimestamp()?></td>
		<td><?php echo is_object($article) ? $article->getDate() : '0000-00-00' ?></td>
		<td><?php echo $title->getReceivingStatusComment()?></td>
		</tr>
		<?php
		}
	}
	?>
	</tbody>
</table>
</div>

