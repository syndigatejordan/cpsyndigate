<?php include_partial('menu'); ?>

<h1>Titles having wrong dates</h1>
<hr>
<?php echo form_tag('dashboard/UpdateWrongDate'); ?>

<div>
  <table border="1" align="left" width="100%">
    <tr>
      <th></th>
      <th>ID</th>
      <th>TITLE ID</th>
      <th>Date</td>
      <th>Parsed at</th>
      <th>Headline</th>
      <th>Link</th>

    </tr>
    <?php foreach ($wrongDates as $wrongDate) { ?>
      <tr>
        <td><input type="checkbox" name="checkbox[]" value="<?php echo $wrongDate ["id"]; ?>" /></td>
        <td><?php echo $wrongDate ["id"]; ?></td>
        <td><?php echo $wrongDate["title_id"]; ?></td>
        <td><?php echo $wrongDate["date"]; ?></td>
        <td><?php echo $wrongDate ["parsed_at"]; ?></td>
        <td><a href="/index.php/article/show/id/<?php echo $wrongDate ["id"]; ?>" onmouseover="setObj(description[<?php echo $wrongDate ["id"]; ?>], 'override', 800, 200)"  onmouseout="clearTimeout(openTimer);
              stopIt()"><?php echo $wrongDate ["headline"]; ?></a></td>
          <?php
          if (!empty($wrongDate["reference"])) {
            ?>
          <td><a href="<?php echo $wrongDate["reference"]; ?>">Link</a></td>
        <?php } else { ?>
          <td></td>
        <?php } ?>
      </tr>
    <?php } ?>
  </table>
</div>
<div>
  <?php echo input_date_tag('date', date("Y-m-d"), 'rich=true readonly=readonly ') . " " ?>
  <input type="submit" name="Update" value="Submit" />
</div>
</form>