<?php

class dashboardActions extends sfActions {

  /**
   * Executes index action
   *
   */
  public function executeIndex() {

    /**
     * Get daily titles did not send today content 
     */
    $dailyTitlesC = new Criteria();
    $dailyTitlesC->add(TitlePeer::IS_ACTIVE, 1);
    $dailyTitlesC->add(TitlePeer::TITLE_FREQUENCY_ID, 1);
    $dailyTitlesC->add(TitlePeer::CACHED_ARTICLES_TODAY, 0);
    $this->dailyTitlesLate = TitlePeer::doSelect($dailyTitlesC);

    /**
     * Get titles did not send in the previous span
     */
    $titlesSpanC = new Criteria();
    $titlesSpanC->add(TitlePeer::IS_ACTIVE, 1);
    $titlesSpanC->add(TitlePeer::CACHED_CURRENT_SPAN, 0);
    $titlesSpanC->add(TitlePeer::CACHED_PREV_SPAN, 0);
    $this->titlesSpanLate = TitlePeer::doSelect($titlesSpanC);

    /**
     * Get titles svn errors
     */
    $svnErrFile = '/usr/local/syndigate/tmp/svn_err.tmp';
    if (!is_file($svnErrFile)) {
      $this->svnErrors = array();
    } else {
      $this->svnErrors = (array) json_decode(file_get_contents($svnErrFile));
    }

    /**
     * Gettig FTP push files not sent 
     */
    $tmpArr = array();
    $pushFtpC = new Criteria();
    $pushFtpC->add(ClientConnectionFilePeer::IS_SENT, 0);
    $pushFtpFilesNotSent = ClientConnectionFilePeer::doSelect($pushFtpC);

    foreach ($pushFtpFilesNotSent as $file) {
      $tmpArr[$file->getClientId()][] = $file;
    }
    $this->pushFtpFilesNotSent = $tmpArr;

    /**
     * getting titles that is not licenced to any client
     */
    $cTitlesClientsRules = new Criteria();
    $cTitlesClientsRules->add(TitlesClientsRulesPeer::CLIENT_ID, 24, Criteria::NOT_EQUAL);
    $cTitlesClientsRules->add(TitlesClientsRulesPeer::TITLE_ID, 254, Criteria::NOT_EQUAL);
    $titlesClientsRules = TitlesClientsRulesPeer::doSelect($cTitlesClientsRules);

    $titlesIds = array();
    $clientsIds = array(); // for clients that do not recive section
    foreach ($titlesClientsRules as $title) {
      $titlesIds[$title->getTitleId()] = 'on';
      $clientsIds[$title->getClientId()] = 'on'; // for clients that do not recive section
    }

    $titlesIds = array_keys($titlesIds);
    array_push($titlesIds, 254);


    $cTitles = new Criteria();
    $cTitles->add(TitlePeer::ID, $titlesIds, Criteria::NOT_IN);
    $this->titlesNotLicened = TitlePeer::doSelect($cTitles);

    /**
     * Getting Clients that do not recive from any title
     */
    $clientsIds = array_keys($clientsIds);
    array_push($clientsIds, 24);
    $tClients = new Criteria();
    $tClients->add(ClientPeer::ID, $clientsIds, Criteria::NOT_IN);
    $this->clientsNotLicened = ClientPeer::doSelect($tClients);

    // select count(*) from article where date>'2014-03-17'
    $conn = propel::getConnection();
    $date = date('Y-m-d');
    $future_c = array();
    $query = "";
    $query = mysql_query("select a.id,title_id,headline,date,parsed_at,reference "
            . "from article a inner join article_original_data d on a.article_original_data_id=d.id"
            . " where date>'$date'order by title_id;");
    while ($row = mysql_fetch_assoc($query)) {
      $future_c[] = $row;
    }
    $this->future_dates = $future_c;

    /*
     * Get articles with date 1970
     */

    $wrongDate = array();
    $query = "";
    $query = mysql_query("select a.id,title_id,headline,date,parsed_at,reference "
            . "from article a inner join article_original_data d on a.article_original_data_id=d.id"
            . " where date='1970-01-01' or date='0000-00-00' order by title_id;");
    while ($row = mysql_fetch_assoc($query)) {
      $wrongDate[] = $row;
    }
    $this->wrongDates = $wrongDate;
  }

  public function executeClientPullFtpFiles() {

    $this->clientId = $this->getRequestParameter('clientId');
    $this->titleId = $this->getRequestParameter('titleId');
    $this->date = $this->getRequestParameter('date', date('Y-m-d'));


    $timestampFrom = strtotime($this->date);
    $timestampTo = $timestampFrom + 86400;

    //Getting files and number of articles for files sent in FTP pull
    $pullFtpC = new Criteria();
    $pullFtpC->add(ClientFilePeer::CREATION_TIME, $timestampFrom, Criteria::GREATER_EQUAL);
    $pullFtpC->addAnd(ClientFilePeer::CREATION_TIME, $timestampTo, Criteria::LESS_THAN);
    if ($this->clientId) {
      $pullFtpC->add(ClientFilePeer::CLIENT_ID, $this->clientId);
    }

    if ($this->titleId) {
      $pullFtpC->add(ClientFilePeer::TITLE_ID, $this->titleId);
    }

    $this->clientPullFiles = ClientFilePeer::doselect($pullFtpC);

    $c = new Criteria();
    $c->add(TitlePeer::IS_ACTIVE, 1);
    $titles = TitlePeer::doSelect($c);
    $this->titles = array(0 => 'Select title');
    foreach ($titles as $title) {
      $this->titles[$title->getId()] = $title->getName();
    }

    $c = new Criteria();
    $c->add(ClientPeer::IS_ACTIVE, 1);
    $clients = ClientPeer::doSelect($c);
    $this->clients = array(0 => 'Select client');
    foreach ($clients as $client) {
      $this->clients[$client->getId()] = $client->getClientName();
    }
  }

  public function executeDrupalTitles() {

    $this->titles = TitlePeer::doSelect(new Criteria());
    $publishers = PublisherPeer::doSelect(new Criteria());
    $drupals = DrupalIdsPeer::doSelect(new Criteria());
    $drupalIds = array();

    if (!empty($drupals)) {
      foreach ($drupals as $record) {
        $drupalIds[] = $record->getTitleId();
      }
    }

    $this->publishers = array();
    foreach ($publishers as $publiser) {
      $this->publishers[$publiser->getId()] = $publiser;
    }
    $this->drupalIds = $drupalIds;
  }

  public function executeFutureDate() {
    // select count(*) from article where date>'2014-03-17'
    // select count(*) from article where date>'2014-03-17'
    $conn = propel::getConnection();
    $date = date('Y-m-d');
    $future_c = array();
    $query = "";
    $query = mysql_query("select a.id,title_id,headline,date,parsed_at,reference "
            . "from article a inner join article_original_data d on a.article_original_data_id=d.id"
            . " where date>'$date'order by title_id;");
    while ($row = mysql_fetch_assoc($query)) {
      $future_c[] = $row;
    }
    $this->future_dates = $future_c;
  }

  public function executeUpdateDate() {
    $date = $this->getRequestParameter('date');
    $Ids = $this->getRequestParameter('checkbox');
    foreach ($Ids as $id) {
      // New obj
      $obj = ArticlePeer::retrieveByPK($id);
      $obj->setDate($date);
      $obj->save();
    }
    $this->redirect("dashboard/FutureDate");
  }

  public function executeWrongDate() {
    /*
     * Get articles with date 1970
     */
    $conn = propel::getConnection();
    $wrongDate = array();
    $query = mysql_query("select a.id,title_id,headline,date,parsed_at,reference "
            . "from article a inner join article_original_data d on a.article_original_data_id=d.id"
            . " where date='1970-01-01' or date='0000-00-00' order by title_id;");
    while ($row = mysql_fetch_assoc($query)) {
      $wrongDate[] = $row;
    }
    $this->wrongDates = $wrongDate;
  }

  public function executeTitleFrequencyDate() {
      $cURLConnection = curl_init();
      curl_setopt($cURLConnection, CURLOPT_URL, 'http://cms.syndigate.info/rest/cms-titles');
      curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
      $phoneList = curl_exec($cURLConnection);
      curl_close($cURLConnection);
      $ids= json_decode($phoneList,true);
      $ids=$ids["ids"];

    $conn = propel::getConnection();
    $frequencyDate = array();
    $query = mysql_query("select PUBLISHER_ID,title.id
  ,title_frequency_id
  ,(select sum(f.frequency) from orders_frequency f where title.id=f.title_id group by title_id limit 1) frq 
  from title 
  where 
  title.is_active=1 and directory_layout !='root'and publisher_id not in (1184,1185,1186,1356) 
  title.id not in ($ids) 
  order by title_frequency_id asc, frq asc , title.id asc;");
    while ($row = mysql_fetch_assoc($query)) {
      $frequencyDate[] = $row;
    }
    $this->frequencyDate = $frequencyDate;
  }

  public function executeTitleFrequencyRoot() {

    $conn = propel::getConnection();
    $frequencyRoot = array();
    $query = mysql_query("select PUBLISHER_ID,title.id
  ,title_frequency_id
  ,(select sum(f.frequency) from orders_frequency f where title.id=f.title_id group by title_id limit 1) frq 
  from title 
  where 
  title.is_active=1 and directory_layout ='root'
  order by title_frequency_id asc, frq asc , title.id asc;");
    while ($row = mysql_fetch_assoc($query)) {
      $frequencyRoot[] = $row;
    }
    $this->frequencyRoot = $frequencyRoot;
  }

  public function executeUpdateWrongDate() {
    $date = $this->getRequestParameter('date');
    $Ids = $this->getRequestParameter('checkbox');
    foreach ($Ids as $id) {
      // New obj
      $obj = ArticlePeer::retrieveByPK($id);
      $obj->setDate($date);
      $obj->save();
    }
    $this->redirect("dashboard/WrongDate");
  }

  public function executeDrupalTitleUpdate() {

    $code = 0;
    $message = '';

    //all small letters
    $authorized_users = array('hani', 'khaled');
    $username = $this->getUser()->getAttribute('username');

    if (!in_array(strtolower($username), $authorized_users)) {
      $message = 'Not autorized';
    } else {

      if ('POST' == $_SERVER['REQUEST_METHOD']) {
        $titleId = $this->getRequestParameter('title_id');
        $status_code = $this->getRequestParameter('status_code');

        $title = TitlePeer::retrieveByPK($titleId);
        if ($title) {

          $drupals = DrupalIdsPeer::doSelect(new Criteria());
          $drupalIds = array();

          if (!empty($drupals)) {
            foreach ($drupals as $record) {
              $drupalIds[] = $record->getTitleId();
            }
          }

          if (1 == $status_code) {
            if (!in_array($title->getId(), $drupalIds)) {

              //add the title to the table
              $titleDrupal = new DrupalIds();
              $titleDrupal->setTitleId($title->getId());
              $titleDrupal->save();
              $code = 1;
            } else {
              $message = 'Title already licenced';
            }
          } elseif (0 == $status_code) {
            if (in_array($title->getId(), $drupalIds)) {

              //remove the title from the table
              $c = new Criteria();
              $c->add(DrupalIdsPeer::TITLE_ID, $title->getId());
              DrupalIdsPeer::doDelete($c);
              $code = 1;
            } else {
              $message = 'Title already not licenced';
            }
          }
        }
      } else {
        $message = "not post";
      }
    } // End if authorization

    echo json_encode(array('code' => $code, 'message' => $message));
    die();
  }

  public function executeResetConnectionFileCounter() {
    $message = '';
    $code = 0;

    if ('POST' == $_SERVER['REQUEST_METHOD']) {
      $id = $this->getRequestParameter('client_connection_file_id');
      $file = ClientConnectionFilePeer::retrieveByPK($id);

      if (!$file) {
        $message = 'invalid id';
      } else {


        if (!$file->getIsSent() && $file->getNumRetry() >= 20) {
          $file->setNumRetry(0);
          $file->save();
          $code = 1;
        } else {
          $message = 'file has not reach the maximum retry num to reset';
        }
      }
    } else {
      $message = 'not post';
    }

    echo json_encode(array('code' => $code, 'message' => $message));
    die();
  }

// End function 

  public function executeRanks() {

    $rank = $this->getRequestParameter('rank');
    $span = $this->getRequestParameter('span');
    $freq_id = $this->getRequestParameter('freq_id');

    if (!$span) {
      $span = 1; // current
    } else {
      if ($span != 1) {
        $span = 2;
      }
    }

    if (!$rank) {
      $rank = 10;
    }

    /**
     * Get titles did not send in the previous span
     */
    $titlesSpanC = new Criteria();
    $titlesSpanC->add(TitlePeer::IS_ACTIVE, 1);
    $titlesSpanC->add(TitlePeer::RANK, $rank);
    $titlesSpanC->add(TitlePeer::CACHED_CURRENT_SPAN, 0);
    $titlesSpanC->add(TitlePeer::CACHED_CURRENT_SPAN, 0);

    if ($span == 2) {
      $titlesSpanC->add(TitlePeer::CACHED_PREV_SPAN, 0);
    }

    if ($freq_id) {
      $titlesSpanC->add(TitlePeer::TITLE_FREQUENCY_ID, $freq_id);
    }

    $this->titles = TitlePeer::doSelect($titlesSpanC);
    $this->rank = $rank;
    $this->span = $span;
    $this->freq_id = $freq_id;

    //getting some info
    $languages = array();
    $langs = LanguagePeer::doSelect(new Criteria());
    foreach ($langs as $lang) {
      $languages[$lang->getId()] = $lang;
    }
    $this->langs = $languages;


    $frequencies = array();
    $freqs = TitleFrequencyPeer::doSelect(new Criteria());
    foreach ($freqs as $freq) {
      $frequencies[$freq->getId()] = $freq;
    }
    $this->freqs = $frequencies;
  }

  public function executeClientPushFtpFiles() {


    $this->clientId = $this->getRequestParameter('clientId');
    $this->titleId = $this->getRequestParameter('titleId');
    $this->filter = $this->getRequestParameter('filter') ? strtoupper($this->getRequestParameter('filter')) : 'CREATION_TIME';
    $this->dateFrom = $this->getRequestParameter('dateFrom', date('Y-m-d')) ? $this->getRequestParameter('dateFrom', date('Y-m-d')) : date('Y-m-d');
    $this->dateTo = $this->getRequestParameter('dateTo', date('Y-m-d'));

    $timestampFrom = strtotime($this->dateFrom);
    $timestampTo = $this->dateTo ? strtotime($this->dateTo) + 86400 : '';


    $FtpC = new Criteria();

    if ($this->filter == 'SENT_TIME') {
      $FtpC->add(ClientConnectionFilePeer::SENT_TIME, $timestampFrom, Criteria::GREATER_EQUAL);
      if ($this->dateTo) {
        $FtpC->addAnd(ClientConnectionFilePeer::SENT_TIME, $timestampTo, Criteria::LESS_THAN);
      }
    } else {
      $FtpC->add(ClientConnectionFilePeer::CREATION_TIME, $timestampFrom, Criteria::GREATER_EQUAL);
      if ($this->dateTo) {
        $FtpC->addAnd(ClientConnectionFilePeer::CREATION_TIME, $timestampTo, Criteria::LESS_THAN);
      }
    }

    if ($this->clientId) {
      $FtpC->add(ClientConnectionFilePeer::CLIENT_ID, $this->clientId);
    }

    if ($this->titleId) {
      $FtpC->add(ClientConnectionFilePeer::TITLE_ID, $this->titleId);
    }

    $this->files = ClientConnectionFilePeer::doselect($FtpC);

    $c = new Criteria();
    $titles = TitlePeer::doSelect($c);
    $this->titles = array(0 => 'Select title');
    foreach ($titles as $title) {
      $this->titles[$title->getId()] = $title->getName();
    }

    $c = new Criteria();
    $clients = ClientPeer::doSelect($c);
    $this->clients = array(0 => 'Select client');
    foreach ($clients as $client) {
      $this->clients[$client->getId()] = $client->getClientName();
    }
  }

}
