<?php

/**
 * publisherContacts actions.
 *
 * @package    syndigate
 * @subpackage publisherContacts
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 3335 2007-01-23 16:19:56Z fabien $
 */
class publisherContactsActions extends sfActions
{
	/**
	 * Forword to execute list 
	 * Needs publisher_id
	 */
	public function executeIndex()
	{
		$this->forward('publisherContacts', 'list');
	}
	
	/**
	 * list publisher Contacts 
	 * The request needs publisher_id
	 */
	public function executeList()
	{
		$publisher = PublisherPeer::retrieveByPK($this->getRequestParameter('publisher_id'));
		$this->forward404Unless($publisher);
		$this->setLayout('emptyLayout');
		$this->contacts    = $publisher->getContacts();
		$this->publisherId = $this->getRequestParameter('publisher_id');
		$this->msg         = $this->getRequestParameter('msg');
	}
	
	/**
	 * Show create contact form
	 * Needs publisher_id
	 *
	 */
	public function executeCreate()
	{
		$this->setLayout(false);
		$publisherId = $this->getRequestParameter('publisher_id');
		$this->forward404Unless($publisherId);
		$this->publisherId = $publisherId;
		$this->contact = new Contact();
	}
	
	/**
	 * Show Edit contact form
	 * Needs id parameter [the contact id] & publisher_id
	 * 
	 */
	public function executeEdit()
	{
		$this->contact 		= ContactPeer::retrieveByPk($this->getRequestParameter('id'));
		$this->publisherId 	= $this->getRequestParameter('publisher_id');
		$this->forward404Unless($this->contact);
		
		if($this->getRequestParameter('msg')) {
			$this->msg = $this->getRequestParameter('msg');
		}
	}
	
	
	/**
	 * Execute update contact 
	 *
	 */
	public function executeUpdate()
	{
  	if (!$this->getRequestParameter('id')) {
    	
      $contact = new Contact();
      $this->forward404Unless($this->getRequestParameter('publisher_id'));
      $isNew = true;
      
    } else {
      $contact = ContactPeer::retrieveByPk($this->getRequestParameter('id'));
      $this->forward404Unless($contact);
      $isNew = false;
    }

    $contact->setId($this->getRequestParameter('id'));
    $contact->setFullName($this->getRequestParameter('full_name'));
    $contact->setEmail($this->getRequestParameter('email'));
    $contact->setAdditionalEmail($this->getRequestParameter('additional_email'));
    $contact->setTitle($this->getRequestParameter('title'));
    $contact->setDepartment($this->getRequestParameter('department'));
    $contact->setZipcode($this->getRequestParameter('zipcode'));
    $contact->setPobox($this->getRequestParameter('pobox'));
    $contact->setStreetAddress($this->getRequestParameter('street_address'));
    $contact->setCityName($this->getRequestParameter('city_name'));
    $contact->setCountryId($this->getRequestParameter('country_id') ? $this->getRequestParameter('country_id') : null);
    $contact->setMobileNumber($this->getRequestParameter('mobile_number'));
    $contact->setWorkNumber($this->getRequestParameter('work_number'));
    $contact->setHomeNumber($this->getRequestParameter('home_number'));
    $contact->setNotificationSms($this->getRequestParameter('notification_sms', 0));
    $contact->setNotificationEmail($this->getRequestParameter('notification_email', 0));
    $contact->setType($this->getRequestParameter('type'));
    
    $errMeg = $contact->getErrorMsg();
   
    if($errMeg) {
    	$this->msg 			= $errMeg;
    	$this->publisherId 	= $this->getRequestParameter('publisher_id');
    	$this->contact		= $contact;
    	$this->setTemplate($isNew ? 'create' : 'edit');
    } else {
    	
    	$contact->saveDataAndNagios('publisher');
    	
    	if($isNew) {
    		
    		$publisherContactLink = new PublisherContactLink();
    		$publisherContactLink->setContactId($contact->getId());
    		$publisherContactLink->setPublisherId($this->getRequestParameter('publisher_id'));
    		$publisherContactLink->save();
    		
    		//generating nagios 
    		$publisherId = $publisherContactLink->getPublisherId();
    		$nagiosPath  = $publisherContactLink->getPublisher()->getNagiosFilePath();
    		require LINUX_SCRIPTS_PATH . 'syndGenerateNagiosConf.php';
    		
    		    		    		
    		$this->publisherId = $this->getRequestParameter('publisher_id');
    		$this->setLayout(false);
    		//$this->setTemplate('creation');
    		return $this->redirect('publisherContacts/list?publisher_id='. $publisherId . "&msg=contact Added") ;
  		} else {
    		
    		$c = new Criteria();
    		$c->add(PublisherContactLinkPeer::CONTACT_ID , $contact->getId());
    		$link = PublisherContactLinkPeer::doSelectone($c); //check it later
    		$publisher   = $link->getPublisher();
    		$publisherId = $publisher->getId(); 
    		$nagiosPath  = $publisher->getNagiosFilePath();
    		
    		require LINUX_SCRIPTS_PATH . 'syndGenerateNagiosConf.php';
    		
    		return $this->redirect('publisherContacts/edit?id='.$contact->getId() . '&publisher_id=' . $publisherId . "&msg=Updated") ;
    	}
    }
        
  } // end function

  
  /**
   * Delete a contact and return to the publisher page
   * Needs parameter id [the contact id]
   * 
   */
  public function executeDelete()
  {
  	$contactId = $this->getRequestParameter('id');
  	$this->forward404Unless($contactId);
  	
  	$c = new Criteria();
  	$c->add(PublisherContactLinkPeer::CONTACT_ID , $contactId);
  	$publisherLink = PublisherContactLinkPeer::doSelectOne($c);
  	  	
  	PublisherContactLink::deletePublisherContact($publisherLink->getPublisherId(), $contactId);
  	
  	return $this->redirect('publisher/edit?id=' . $publisherLink->getPublisherId() . '#contacts');
  	//return $this->redirect('publisher/edit?id=' . $publisherLink->getPublisherId());
  }
  
  
  
    
}
