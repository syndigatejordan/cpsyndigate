<div id="bank_details_item">
<?php 
use_helper('Object');
use_helper("Javascript"); 

echo form_remote_tag(array('update' => 'bank_details_item', 'url' => 'publisherBankDetails/update',));
echo object_input_hidden_tag($bank_details, 'getId');
echo object_input_hidden_tag($bank_details, 'getPublisherId');
?>		


<h2>Bank Details</h2>
<div> <b><font color="Red"><?php echo isset($msg)? $msg : '';?></font></b> <br><br> </div>
<table>
<tbody>

<tr>
	<th>IBAN:</th>
	<td><?php echo object_input_tag($bank_details, 'getIban', array (  'size' => 80,)) ?></td>
</tr>

<tr>
	<th>SWIFT:</th>
	<td><?php echo object_input_tag($bank_details, 'getSwift', array (  'size' => 80,)) ?></td>
</tr>

<tr>
	<th>Bank name:</th>
	<td><?php echo object_input_tag($bank_details, 'getBankName', array (  'size' => 80,)) ?></td>
</tr>

<tr>
	<th>Branch:</th>
  	<td><?php echo object_input_tag($bank_details, 'getBranch', array (  'size' => 80,)) ?></td>
</tr>

<tr>
	<th>Country:</th>
	<td><?php echo object_select_tag($bank_details, 'getCountryId', array ( 'related_class' => 'Country',  'include_blank' => true,)) ?></td>
</tr>

<tr>
	<th>Bank city:</th>
	<td><?php echo object_input_tag($bank_details, 'getBankCity', array (  'size' => 80,)) ?></td>
</tr>

<tr>
	<th>Account name:</th>
  	<td><?php echo object_input_tag($bank_details, 'getAccountName', array (  'size' => 80,)) ?></td>
</tr>

<tr>
	<th>Account number:</th>
	<td><?php echo object_input_tag($bank_details, 'getAccountNumber', array (  'size' => 80,)) ?></td>
</tr>

<tr>
	<th>Notes:</th>
	<td><?php echo object_textarea_tag($bank_details, 'getNotes', array (  'size' => '92x3',)) ?></td>
</tr>





</tbody>
</table>

<hr />
<?php echo submit_tag('Update Bank Details') ?>
</form>

</div>