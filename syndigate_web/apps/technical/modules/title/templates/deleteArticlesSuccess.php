<?php 
use_helper('Object');
use_helper('Javascript');
?>
<h1><a name="del-artticles">Delete articles</a></h1>
<br />
<br />
<br />

<center><b><font color="Red"><label id="lbl-load">This may take some times</label></font></b></center>
<div> <b><font color="Red"><?php echo isset($delArtclMsg)? $delArtclMsg : '';?></font></b> <br><br> </div>

<br />
<br />
<br />

<?php 
if($title->getArticlesCount()!=0) {
	echo form_tag('title/deleteTitleArticles'); 
	echo object_input_hidden_tag($title, 'getId'); ?>
	<center>
	<table> 
		<tr><td>Articles count:</td>  <td><?php echo $title->getArticlesCount();?></td></tr>
		<tr><td>Current Revision</td> <td><?php echo $title->getMaxRevision(); ?></td></tr> 
		<tr><td>Options</td>          <td><input type="radio" id="del-article" name="del-article" value="all" 		 checked onclick ="javascript: document.getElementById('rev1').style.display='none'; document.getElementById('rev2').style.display='none'; document.getElementById('rev3').style.display='none'; document.getElementById('rev4').style.display='none'; document.getElementById('dat1').style.display='none'; document.getElementById('ids').style.display='none'">all Article</option></td>  </tr>
		<tr><td>&nbsp;</td>           <td><input type="radio" id="del-article" name="del-article" value="by-ids" 		  	 onclick ="javascript: document.getElementById('rev1').style.display='none'; document.getElementById('rev2').style.display='none'; document.getElementById('rev3').style.display='none'; document.getElementById('rev4').style.display='none'; document.getElementById('dat1').style.display='none'; document.getElementById('ids').style.display='block'">By ID(s)</option></td> <td id="ids" style="display:none;"> <textarea cols="40" rows="3" name="ids"></textarea> </td> <td><b>comma seperated</b></td>   </tr>
		<tr><td>&nbsp;</td>           <td><input type="radio" id="del-article" name="del-article" value="revision" 	         onclick ="javascript: document.getElementById('rev1').style.display='block';document.getElementById('rev2').style.display='block';document.getElementById('rev3').style.display='block';document.getElementById('rev4').style.display='block';document.getElementById('dat1').style.display='none'; document.getElementById('ids').style.display='none'">By revision</option></td>   <td id="rev1" style="display:none;">From</td>                                                                                     <td id="rev2" style="display:none;" ><?php echo select_tag('rev_from', options_for_select($title->getRevisionsNums(), $title->getMinRevision() ));?></td>   <td id="rev3" style="display:none;">To</td> <td id="rev4" style="display:none;"><?php echo select_tag('rev_to', options_for_select($title->getRevisionsNums(), $title->getMaxRevision() ));?></td></tr>
		<tr><td>&nbsp;</td>           <td><input type="radio" id="del-article" name="del-article" value="parese-date"        onclick ="javascript: document.getElementById('rev1').style.display='none'; document.getElementById('rev2').style.display='none'; document.getElementById('rev3').style.display='none'; document.getElementById('rev4').style.display='none'; document.getElementById('dat1').style.display='block';document.getElementById('ids').style.display='none'">By parse date</option></td>     <td id="dat1" style="display:none;"><?php echo input_date_tag('date', date("Y-m-d"), 'rich=true readonly=readonly ')." " ?></td>                                                                                                                                                                                                                                                                                                                                     </tr>
		<tr><td>&nbsp;</td>	 		  <td><input type="submit" value="Delete articles" onclick="javascript: result = checkString(); if (result ==false) {return false;}"> <?php echo button_to('Return to title', "title/edit?id=" . $title->getId()); ?></td></tr>
	</table>
	</center>
<?php // echo submit_tag('Delete', array('confirm'  => 'Are you sure you want to delete the articles ?')); ?>
<?php 
	
	?></form>
<?php 
} else { 
	echo '<b>No Articles for this title</b> <br /> <br /> <br />' ;
	echo button_to('Return to title', "title/edit?id=" . $title->getId());
} 
?>

<script>
	document.getElementById('lbl-load').style.display="none";
	
	function checkString() {
		
		var answer;
		answer = prompt ('You are About Deleting title articles, enter this String : <?php echo $confirmString ?> ','');
		
		if(answer == null) {
			return false;
		} else {
			
			if( answer != '<?php echo $confirmString ?>') {
				alert('Incorrect ....'); 
				return false; 
			} else {
				return true;
			}
		}
	}
</script>