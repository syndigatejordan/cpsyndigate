<?php 
use_helper('Object'); 
use_helper('Javascript');  
include_once LINUX_SCRIPTS_PATH . 'standard.php'; 
include_once LINUX_SCRIPTS_PATH . 'database_helper.php';

$standard = new Standard();
$dbHelper = new dataBaseHelper();
?>
 
	<h2>Create Title</h2>
	<div> <b><font color="Red"><?php echo isset($msg)? $msg : '';?></font></b> <br><br> </div>
	<br>

	<?php 
	echo form_tag('title/update');
	echo object_input_hidden_tag($title, 'getId');
	echo input_hidden_tag('fix_publisher', ((isset($fixPublisher))&&($fixPublisher==true))? true : '');
	?>
	
	<table>
	<tbody>
	<tr>
		<th>Publisher</th>
		<td><?php
				if((isset($fixPublisher))&&($fixPublisher==true)) {
					echo input_hidden_tag('publisher_id', $title->getPublisherId());
					echo link_to($title->getPublisher(), 'publisher/edit?id=' . $title->getPublisherId() );  echo '&nbsp;&nbsp;[# publisher id ='; echo link_to($title->getPublisherId(), 'publisher/edit?id=' . $title->getPublisherId() ) . ']'; 
				} else {
					echo object_select_tag($title, 'getPublisherId', array());
				}
			 ?>
		 </td>
	</tr>
	
	<tr>
  		<th>Name:</th>
  		<td><?php echo object_input_tag($title, 'getName', array (  'size' => 80, 'onkeyup'=>"javascript: theName = document.getElementById('name').value;  ajaxSuggestUsername(theName, 'suggestedTitleName')")) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Rank:</th>
  		<td>
  		<select name="rank" >
  		<?php
  		for ($i=1 ; $i<=10; $i++) {
  			$selected = (int)$title->getRank() == $i ? "selected" : '';
  			?>
  			<option value="<?php echo $i ?>" <?php echo $selected ?> ><?php echo $i ; echo $i ==1 ? ' min proiority' :'' ; echo $i ==10 ? ' max proiority' :'';?></option>
  			<?php
  		}
  		?>  
  		</select>			
		</td>
  	</tr>
  	
  	<tr>
  		<th>Syndigate username:</th>
  		<td><?php echo object_input_tag($title, 'getFtpUsername', array ( 'maxlength'=>8)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Syndigate password:</th>
  		<td><?php echo '' !=  trim($title->getFtpPassword()) ? object_input_tag($title, 'getFtpPassword', array (  'size' => 30, 'maxlength'=>8)) : input_tag('ftp_password', $standard->getRandomPassword(6), 'maxlength=8') ?></td>
  	</tr>
  	
  	<tr>
  		<th>Cron job:</th> 
  		<td><?php echo select_tag('cron_definition', options_for_select( GatherCrontab::getDefinitions() , !isset($cronDefinition) ? '' : $cronDefinition ));?></td>
  	</tr>
  	
	<tr>
		<th>Title frequency*:</th>
  		<td><?php echo object_select_tag($title, 'getTitleFrequencyId', array ( 'related_class' => 'TitleFrequency',)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Title type*:</th>
  		<td><?php echo object_select_tag($title, 'getTitleTypeId', array (  'related_class' => 'TitleType',)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Language:</th>
  		<td><?php echo object_select_tag($title, 'getLanguageId', array ('related_class' => 'Language',)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Country:</th>
  		<td><?php echo object_select_tag($title, 'getCountryId', array ('related_class' => 'Country',)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>City :</th>
  		<td><?php echo object_input_tag($title, 'getCity'); ?></td>
  	</tr>
  	  	
  	<tr>
  		<th>Website:</th>
  		<td><?php echo object_input_tag($title, 'getWebsite', array (  'size' => 80,)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Copyright:</th>
  		<td><?php echo object_input_tag($title, 'getCopyright', array (  'size' => 80,)) ?></td>
  	</tr>
  	
  	
  	<tr>
  		<th>Articles threshold:</th>
  		<td><?php echo object_input_tag($title, 'getArticlesThreshold', array (  'size' => 7,)) ?></td>
  	</tr>
  	  	
  	<tr>  	
  		<th>Active:</th>
  		<td><?php echo object_checkbox_tag($title, 'getIsActive', array (  'size' => 7,)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Photo feed:</th>
  		<td><?php echo select_tag('photo_feed', options_for_select($dbHelper->getEnum('title', 'photo_feed' ), $title->getPhotoFeed() ));?></td>
  	</tr>
  	
  	<tr>
  		<th>Dir layout:</th>
  		<td><?php echo select_tag('directory_layout', options_for_select($dbHelper->getEnum('title', 'directory_layout' ), $title->getDirectoryLayout() ));?></td>
  	</tr>
  	
  	<tr>
		<th>Logo path</th>
		<td><?php echo object_input_tag($title, 'getLogoPath', array('readonly'=>'true')); ?> <input type="button" value="Clear" onclick="javaScript: document.getElementById('logo_path').value=''">  <input type="button" value="change" onClick="window.open('<?php echo DOMAIN_NAME; ?>/<?php echo  SF_APP . '.php/upload/index' ?>','mywindow','width=400,height=200, left=400, top=300')">	</td>
	</tr>
  	
  	<tr valign="top">
  		<th>ConnectionType:</th>
  		<td><?php echo select_tag('connection_type', options_for_select($dbHelper->getEnum('title_connection', 'connection_type' ), $titleConnection->getConnectionType() ), array('onchange'=>"Javascript:  if( document.getElementById('connection_type').value == 'local') { document.getElementById('ftp_http_type').style.display='none'; }else {document.getElementById('ftp_http_type').style.display='block';}  "));?>
  			<div id="ftp_http_type" style="display:<?php echo $titleConnection->getConnectionType() == 'local' ? 'none' : 'block'; ?>">
  				<table>
  					<tr>
  						<th>URL :</th>
  						<td><?php echo object_input_tag($titleConnection, 'getUrl', array('size'=>30,)) ?></td>
  					</tr>
  					
  					<tr>
  						<th>Auth :</th>
  						<td><?php echo object_checkbox_tag($titleConnection, 'getAuthintication', array('size'=>7, 'onchange' =>"javascript: if( document.getElementById('authintication').checked ){ document.getElementById('tbl-auth').style.display='block'; }else{ document.getElementById('tbl-auth').style.display='none'; }")) ?></td>
  					</tr>
  					
  					<tr>
  						<th>&nbsp;</th>
  						<td>
  							&nbsp;
  							<table id="tbl-auth" style="display:<?php echo !$titleConnection->getAuthintication()? 'none' : 'block'; ?>">
  								<tr id="tr-username">
  									<th>Username</th>
  									<td><?php echo object_input_tag($titleConnection, 'getUsername', array('size'=>30,)) ?></td>
  								</tr>
  								
  								<tr id="tr-password">
  									<th>Password</th>
  									<td><?php echo object_input_tag($titleConnection, 'getPassword', array('size'=>30,)) ?></td>
  								</tr>
  							</table>
  						</td>
  					</tr>
  					
  				</table>
  			</div>
  		</td>
  	</tr>
   	
  	<tr>
  		<th valign="top">Extras</th>
  		<td><?php echo object_textarea_tag($titleConnection, 'getExtras', array('size'=>67,)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Has Images</th>
  		<td><?php echo object_checkbox_tag($titleConnection, 'getHasImages', array()) ?></td>
  	</tr>
  	
  	
  	
  	
  	<tr>
  		<th>Technical Intro</th>
  		<td><?php echo object_checkbox_tag($title, 'getHasTechnicalIntro', array()) ?> <input type="button" value="Send Intro"> </td>
  	</tr>
  	
  	<tr>
  		<th>Receiving</th>
  		<td><?php echo object_checkbox_tag($title, 'getIsReceiving', array()) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Parsing</th>
  		<td><?php echo object_checkbox_tag($title, 'getIsParsing', array()) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Technical Notes</th>
  		<td><?php echo object_textarea_tag($title, 'getTechnicalNotes', array('size'=>80,)) ?></td>
  	</tr>
  	

  	<tr>
  		<th>Timeliness:</th>
  		<td><?php echo object_textarea_tag($title, 'getTimeliness', array (  'size' => 80,)) ?></td>
  	</tr>
  	 	
  	<tr>
  		<th>Source description:</th>
  		<td><?php echo object_textarea_tag($title, 'getSourceDescription', array (  'size' => 80,)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Story count</th>
  		<td><?php echo object_textarea_tag($title, 'getStoryCount', array (  'size' => 80,)); ?></td>
  	</tr>

	<tr>
		<th>Notes</th>
		<td><?php echo object_textarea_tag($title, 'getNotes', array (  'size' => 80,)); ?></td>
	</tr>
  	
  
  	
   	  	
</tbody>
</table>
	
<hr />

<?php echo submit_tag('Add') ?>

<?php echo button_to('Cancel', 'adminTitle/list?filter=filter');?>
	
	
</form>



<script>
	function ajaxSuggestUsername(name, method) {
		
		var xmlHttp;

		try {
			xmlHttp=new XMLHttpRequest();
		} catch (e) {
			try {
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
					xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {
					alert("Your browser does not support AJAX!");
					return false;
				}
			}
		}
		
		xmlHttp.onreadystatechange = function() {
			document.getElementById('ftp_username').value = xmlHttp.responseText;;
		}
		
		xmlHttp.open("GET", "<?php echo DOMAIN_NAME ; ?>/ajax.php?method=" + method + "&text=" + name, true);
		xmlHttp.send(null);
	}
</script>


 
    