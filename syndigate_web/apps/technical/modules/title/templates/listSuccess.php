<h1>Titles</h1>
<div align="right"><?php echo (isset($fixPublisher) && ($fixPublisher==true)) ? button_to('New Title', 'title/create?publisher_id=' . $publisherId) : button_to ('New Title', 'title/create') ?></div>
<table border="1" align="center" width="100%">
<thead>
<tr>
  <th>Id</th>
  <th>Name</th>
  <th>Publisher</th>
  <th>Frequency</th>
  <th>Type</th>
  <th>Language</th>
  <th>Articles</th>
</tr>
</thead>
<tbody>
<?php foreach ($titles as $title): ?>
<tr>
      <td><?php echo link_to($title->getId(),   	'title/edit?id='.$title->getId()) ?></td>
      <td><?php echo link_to($title->getName(), 	'title/edit?id='.$title->getId()) ?></td>
      <td><?php echo link_to($title->getPublisher(), 'publisher/edit?id='.$title->getPublisherId())?></td>
      <td><?php echo $title->getTitleFrequency() ?></td>
      <td><?php echo $title->getTitleType() ?></td>
      <td><?php echo LanguagePeer::retrieveByPK($title->getLAnguageId()) ?></td>
      <td><?php //echo link_to($title->getArticlesCount(),   	'title/edit?id='.$title->getId()) ?></td>
  </tr>
<?php endforeach; ?>
</tbody>
</table>

<div align="right"><?php echo (isset($fixPublisher) && ($fixPublisher==true)) ? button_to('New Title', 'title/create?publisher_id=' . $publisherId) : button_to ('New Title', 'title/create') ?></div>