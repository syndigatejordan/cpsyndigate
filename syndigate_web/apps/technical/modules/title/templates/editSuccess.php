<?php 
use_helper('Object');
use_helper('Javascript'); 
use_helper('Date'); 

//include_once LINUX_SCRIPTS_PATH . 'standard.php'; 
include_once LINUX_SCRIPTS_PATH . 'database_helper.php';

//$standard = new Standard();
$dbHelper = new dataBaseHelper(); 
?>

	<h1>Edit Title <?php echo $title; ?></h1>
	<div> <b><font color="Red"><?php echo isset($msg)? $msg : '';?></font></b> <br><br> </div>
	
	<div align="right">
		<a href="/index.php/title/gather?title_id=<?php echo $title->getId() ?>">Gather</a> | 
              <a href="/index.php/title/setCmsParser?title_id=<?php echo $title->getId() ?>">Set CMS Parser</a> | 
	    <a href="/index.php/title/storyCount?title_id=<?php echo $title->getId() ?>&num_of_spans=10&exclude-zeros=1">Stroy count</a> | 
	    <a href="/index.php/titleArticles?filters[title_id]=<?php echo $title->getId() ?>&filter=filter">Articles</a> |
		<a href="/index.php/title/reprocess?title_id=<?php echo $title->getId() ?>">Reprocess</a> | 
		<a href="/index.php/admin/resend?title_id=<?php echo $title->getId() ?>">Resend content</a> | 
		<a href="/index.php/title/deleteTitleArticlesForm?title_id=<?php echo $title->getId() ?>">Delete articles</a> |
		<a href="/index.php/title/deleteTitleForm?title_id=<?php echo $title->getId() ?>"><font color="Red">Delete Title</font></a>
	</div>
	<br />

	<?php
		echo form_tag('title/update');
		echo object_input_hidden_tag($title, 'getId');
		echo object_input_hidden_tag($title, 'getPublisherId');
	?>

<table>
	<tbody>
	<tr>
		<th>Publisher</th>
		<td><?php echo link_to($title->getPublisher(), 'publisher/edit?id=' . $title->getPublisherId() );  echo '&nbsp;&nbsp;[# publisher id ='; echo link_to($title->getPublisherId(), 'publisher/edit?id=' . $title->getPublisherId() ); ?> ] </td>
	</tr>
	
	<tr>
  		<th>Name:</th>
  		<td><?php echo object_input_tag($title, 'getName', array (  'size' => 80,)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Rank:</th>
  		<td>
  		<select name="rank" >
  		<?php
  		for ($i=1 ; $i<=10; $i++) {
  			$selected = (int)$title->getRank() == $i ? "selected" : '';
  			?>
  			<option value="<?php echo $i ?>" <?php echo $selected ?> ><?php echo $i ; echo $i ==1 ? ' min proiority' :'' ; echo $i ==10 ? ' max proiority' :'';?></option>
  			<?php
  		}
  		?>  
  		</select>			
		</td>
  	</tr>
	
  	<tr>
  		<th>Syndigate username:</th>
  		<td><?php echo $title->getFtpUsername(); ?></td>
  	</tr>
  	
  	<tr>
  		<th>Syndigate password:</th>
  		<td><?php echo $title->getFtpPassword(); ?></td>
  	</tr>
  	
  	<tr>
  		<th>Cron job:</th>
  		<td><?php echo select_tag('cron_definition', options_for_select( GatherCrontab::getDefinitions() , !isset($cronDefinition) ? '' : $cronDefinition ));?></td>
  	</tr>	
  	
  		
   	
  	  	
	<tr>
		<th>Title frequency*:</th>
  		<td><?php echo object_select_tag($title, 'getTitleFrequencyId', array ( 'related_class' => 'TitleFrequency',)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Title type*:</th>
  		<td><?php echo object_select_tag($title, 'getTitleTypeId', array (  'related_class' => 'TitleType',)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Language:</th>
  		<td><?php echo object_select_tag($title, 'getLanguageId', array ('related_class' => 'Language',)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Country:</th>
  		<td><?php echo object_select_tag($title, 'getCountryId', array ('related_class' => 'Country',)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>City :</th>
  		<td><?php echo object_input_tag($title, 'getCity'); ?></td>
  	</tr>
  	  	
  	<tr>
  		<th>Website:</th>
  		<td><?php echo object_input_tag($title, 'getWebsite', array (  'size' => 80,)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Copyright:</th>
  		<td><?php echo object_input_tag($title, 'getCopyright', array (  'size' => 80,)) ?></td>
  	</tr>
  	
  	
  	<tr>
  		<th>Articles threshold:</th>
  		<td><?php echo object_input_tag($title, 'getArticlesThreshold', array (  'size' => 7,)) ?></td>
  	</tr>
  	  	
  	<tr>  	
  		<th>Active:</th>
  		<td><?php echo object_checkbox_tag($title, 'getIsActive', array (  'size' => 7,)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Photo feed:</th>
  		<td><?php echo select_tag('photo_feed', options_for_select($dbHelper->getEnum('title', 'photo_feed' ), $title->getPhotoFeed() ));?></td>
  	</tr>
  	
  	<tr>
  		<th>Dir layout:</th>
  		<td><?php echo select_tag('directory_layout', options_for_select($dbHelper->getEnum('title', 'directory_layout' ), $title->getDirectoryLayout() ));?></td>
  	</tr>
  	  	
  	<tr>
		<th>Logo path</th>
		<td><?php echo object_input_tag($title, 'getLogoPath', array('readonly'=>'true')); ?><input type="button" value="Reset" onclick="javaScript: document.getElementById('logo_path').value='<?php echo $title->getLogoPath(); ?>'"> <input type="button" value="Clear" onclick="javaScript: document.getElementById('logo_path').value=''">  <input type="button" value="change" onClick="window.open('<?php echo DOMAIN_NAME; ?>/<?php echo  SF_APP . '.php/upload/index' ?>','mywindow','width=400,height=200, left=400, top=300')">	</td>
	</tr>
  	
  	<tr valign="top">
  		<th>ConnectionType:</th>
  		<td><?php echo select_tag('connection_type', options_for_select($dbHelper->getEnum('title_connection', 'connection_type' ), $titleConnection->getConnectionType() ), array('onchange'=>"Javascript:  if( document.getElementById('connection_type').value == 'local') { document.getElementById('ftp_http_type').style.display='none'; }else {document.getElementById('ftp_http_type').style.display='block';}  "));?>
  			<div id="ftp_http_type" style="display:<?php echo $titleConnection->getConnectionType() == 'local' ? 'none' : 'block'; ?>">
  				<table>
  					<tr>
  						<th>URL :</th>
  						<td><?php echo object_input_tag($titleConnection, 'getUrl', array('size'=>30,)) ?></td>
  					</tr>
  					<tr>
  						<th>Port :</th>
  						<td><?php echo object_input_tag($titleConnection, 'getFtpPort', array('size'=>30,)) ?></td>
  					</tr>
  					
  					<tr>
  						<th>Auth :</th>
  						<td><?php echo object_checkbox_tag($titleConnection, 'getAuthintication', array('size'=>7, 'onchange' =>"javascript: if( document.getElementById('authintication').checked ){ document.getElementById('tbl-auth').style.display='block'; }else{ document.getElementById('tbl-auth').style.display='none'; }")) ?></td>
  					</tr>
  					
  					<tr>
  						<th>&nbsp;</th>
  						<td>
  							&nbsp;
  							<table id="tbl-auth" style="display:<?php echo !$titleConnection->getAuthintication()? 'none' : 'block'; ?>">
  								<tr id="tr-username">
  									<th>Username</th>
  									<td><?php echo object_input_tag($titleConnection, 'getUsername', array('size'=>30,)) ?></td>
  								</tr>
  								
  								<tr id="tr-password">
  									<th>Password</th>
  									<td><?php echo object_input_tag($titleConnection, 'getPassword', array('size'=>30,)) ?></td>
  								</tr>
  							</table>
  						</td>
  					</tr>
  					
  				</table>
  			</div>
  		</td>
  	</tr>
  	
  	<tr>
  		<th>Has Images</th>
  		<td><?php echo object_checkbox_tag($titleConnection, 'getHasImages', array()) ?></td>
  	</tr>
   	
  	<tr>
  		<th valign="top">Extras</th>
  		<td><?php echo object_textarea_tag($titleConnection, 'getExtras', array('size'=> '92x3',)) ?></td>
  	</tr>
  	
  	  	
  	<tr>
  		<th>Technical Intro</th>
  		<td><?php echo object_checkbox_tag($title, 'getHasTechnicalIntro', array()) ?> <input type="button" value="Send Intro"> </td>
  	</tr>
  	
  	<tr>
  		<th>Receiving</th>
  		<td><?php echo object_checkbox_tag($title, 'getIsReceiving', array()) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Parsing</th>
  		<td><?php echo object_checkbox_tag($title, 'getIsParsing', array()) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Technical Notes</th>
  		<td><?php echo object_textarea_tag($title, 'getTechnicalNotes', array('size'=>80,)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Timeliness:</th>
  		<td><?php echo object_textarea_tag($title, 'getTimeliness', array (  'size' => 80,)) ?></td>
  	</tr>
  	 	
  	<tr>
  		<th>Source description:</th>
  		<td><?php echo object_textarea_tag($title, 'getSourceDescription', array (  'size' => 80,)) ?></td>
  	</tr>
  	
  	<tr>
  		<th>Story count</th>
  		<td><?php echo object_textarea_tag($title, 'getStoryCount', array (  'size' => 80,)); ?></td>
  	</tr>

	<tr>
  		<th>Notes</th>
  		<td><?php echo object_textarea_tag($title, 'getNotes', array (  'size' => 80,)); ?></td>
  	</tr>

  	<tr>
  		<th>Contract :</th>
  		<td><?php echo $title->getContract(); ?></td>
  	</tr>
  	
  	<tr>
  		<th>Contract Start :</th>
  		<td><?php echo $title->getContractStartDate(); ?></td>
  	</tr>
  	
  	<tr>
  		<th>Contract End :</th>
  		<td><?php echo $title->getContractEndDate(); ?></td>
  	</tr>
  	
  	<tr>
  		<th>Renewal Period :</th>
  		<td><?php echo $title->getRenewalPeriod(); ?></td>
  	</tr>
  	
  	<tr>
  		<th>Royalty Rate</th>
  		<td><?php echo $title->getRoyaltyRate();?> <b>%</b></td>
  	</tr>
  	
  	<tr>
  		<th>Is Receiving status Error:</th>
  		<td><?php echo object_checkbox_tag($title, 'getReceivingStatus', array()); ?><input type="hidden" name="prev-receiving-status" value="<?php echo $title->getReceivingStatus();?>"></td>
  	</tr>
  	
  	<tr>
  		<th valign="top">Receiving status Comment :</th>
  		<td><?php echo object_textarea_tag($title, 'getReceivingStatusComment', array (  'size' => 80,)); ?></td>
  	</tr>
  	
  	<tr>
  		<th>Receiving Error Time:</th>
  		<td><?php echo $title->getReceivingStatusTimestamp() != '1970-01-01 02:00:00' ? $title->getReceivingStatusTimestamp() : '';?></td>
  	</tr>
  	
</tbody>
</table>

<hr />
<?php echo submit_tag('save') ?> &nbsp; <?php echo button_to('Cancel', 'adminTitle/list?filter=filter'); ?>

</form>

<br />
<br />
<br />
<br />


<h1 id="rules">Title clients</h1>
<hr>

<div id="title_clients" style=" padding:20px; background-color: 	#FFFFFF " >
	<?php 
	echo javascript_tag(remote_function(array( 'update'  => "title_clients", 'url'    => "titlesClientsRules/showtitleclients?title_id=" . $title->getId() ,   )) );
    ?>
</div>

<br />
<br />
<br />
<br />

<!--
  <div id="articles_span_report">
    <h1 id="rules">Articles Count</h1>
    <table>
      <tr>
        <td>Today</td>
        <td>Current Span</td>
        <td>Previous Span</td>
      </tr>
    <tr>
      <td> <?php echo link_to($title->getCachedArticlesToday(), "titleArticles/list?filters[title_id]=".$title->getId()."&filters[parsed_at][from]=" . date('Y-m-d') . "&filter=filter"); ?></td>
      <td><?php echo link_to((int)$title->getCachedCurrentSpan(), "titleArticles/list?filters[title_id]=".$title->getId()."&filters[parsed_at][from]=" . date('Y-m-d',$title->spanFromToDate(0)) ."&filter=filter"); ?></td>
      <td><?php echo link_to((int)$title->getCachedPrevSpan(), "titleArticles/list?filters[title_id]=".$title->getId()."&filters[parsed_at][from]=" . date('Y-m-d',$title->spanFromToDate(-1)) . "&filters[parsed_at][to]=" . date('Y-m-d',$title->spanToToDate(-1)+1) ."&filter=filter"); ?></td>
    </tr></table>
  </div>
-->
<br />
<br />
<br />
<br />

<div id="category_mapping"></div>
<?php
	echo javascript_tag(remote_function(array(
								'update'  => "category_mapping",
				 				'url'    => "categoryMapping/listTitleMapping?title_id=" . $title->getId(), )) );
?>

<br />
<br />
<br />
<br />
<div id="report_gather"></div>
<?php
	echo javascript_tag(remote_function(array(
								'update'  => "report_gather",
				 				'url'    => "titleReportGather/list?title_id=" . $title->getId(), )) );
?>




<br />
<br />
<br />
<br />
<div id="report_parse"></div>
<?php
	echo javascript_tag(remote_function(array(
								'update'  => "report_parse",
				 				'url'    => "titleReportParse/list?title_id=" . $title->getId(), )) );
?>


<!-- JavaScript alters  -->
<?php
//for deleting title articles 
if(isset($delArtclMsg) && (''!=$delArtclMsg)) {
	?>
	<script>alert('<?php echo $delArtclMsg; ?>');</script>
	<?php
}
?>

i
<?php // javascript for adding row to title category mapping ?>
<script>
 function addRowToTable(tableId)
{
  var tbl 		= document.getElementById(tableId);
  var lastRow 	= tbl.rows.length;
  var iteration = lastRow;
  var row 		= tbl.insertRow(lastRow);
  row.setAttribute('id',"new-row-mapp-" + iteration );
  
  // left cell
  var cellLeft 		= row.insertCell(0);
  var textNode 		= document.createElement('input');
  textNode.type 	= 'text';
  textNode.name		= 'new-original['+ iteration +']';
  textNode.value	='';
  cellLeft.appendChild(textNode);
    
  
  // middel cell
  var cellRightSel = row.insertCell(1);
  var sel = document.createElement('select');
  sel.name = 'new-mapping['+ iteration +']' ;
  <?php 
  $mapping = Iptc::getCategories();
  
  $count = -1;
  foreach ($mapping as $index =>$value) {
  	$count++;
  	echo "sel.options[$count] = new Option('$value', '$index');";
  }
  
  ?>
  cellRightSel.appendChild(sel);
  
  
  //action cell
  var cellDel = row.insertCell(2);
  var linkNode = document.createElement('a');
  linkNode.setAttribute('name', 'lnkdel' + iteration);
  linkNode.appendChild(document.createTextNode("Delete"));
  linkNode.onclick=function() {removeRecordFromTable(tableId, "new-row-mapp-" + iteration)};
   cellDel.appendChild(linkNode);
}

function removeRecordFromTable( tableName,rowId )
{   
    var table 	= document.getElementById(tableName);
	var row 	= document.getElementById(rowId);
	var tbody 	= table.getElementsByTagName("tbody")[0];
	tbody.removeChild(row); 
}



</script>

