<?php

/**
 * publisherTitles actions.
 *
 * @package    syndigate
 * @subpackage titles
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 3335 2007-01-23 16:19:56Z fabien $
 */
class titleActions extends sfActions {

  public function executeIndex() {
    return $this->forward('publisherTitles', 'list');
  }

  public function executeList() {
      //sleep(10);
    $c = new Criteria();
    if ($this->getRequestParameter('publisher_id')) {
      $c->add(TitlePeer::PUBLISHER_ID, $this->getRequestParameter('publisher_id'));
      $this->publisherId = $this->getRequestParameter('publisher_id');
      $this->fixPublisher = true;
    }
    $this->titles = TitlePeer::doSelect($c);
  }

  public function executeListAdmin() {
    if ($this->getRequestParameter('publisher_id')) {
      $publisherId = $this->getRequestParameter('publisher_id');
      $this->redirect("adminTitle/list?filters[publisher_id]=$publisherId&filter=filter");
    } else {
      $this->redirect('adminTitle/list?filter=filter');
    }
  }

  public function executeShow() {
    
  }

  public function executeCreate() {
    $this->title = new Title();

    if ($this->getRequestParameter('publisher_id')) {
      $this->fixPublisher = true;
      $publisherId = $this->getRequestParameter('publisher_id');
      $publisher = PublisherPeer::retrieveByPK($publisherId);
      $this->forward404Unless($publisher, "Incorrect Publisher Id $publisherId");
      $this->title->setPublisherId($publisherId);
    }

    $this->titleConnection = $this->title->getTitleConnection(); // try to remove
  }

  public function executeEdit() {
    $this->title = TitlePeer::retrieveByPk($this->getRequestParameter('id'));
    $this->delArtclMsg = $this->getRequestParameter('delArtclMsg'); // from other redirect
    $this->msg = $this->getRequestParameter('msg'); // from other redirect
    $this->forward404Unless($this->title);

    $this->titleConnection = $this->title->getTitleConnection(); // try to remove

    require_once LINUX_SCRIPTS_PATH . 'standard.php';
    $standard = new Standard();
    $this->cronDefinition = $standard->getCrontab($this->title->getCrontabId());
  }

  public function executeUpdate() {

    if (!$this->getRequestParameter('id')) {

      $title = new Title();
      $isNew = true;
    } else {

      $title = TitlePeer::retrieveByPk($this->getRequestParameter('id'));
      $isNew = false;
      $this->forward404Unless($title);
    }

    if ($this->getRequestParameter('fix_publisher')) {
      $this->fixPublisher = true;
    }


    $title->setId($this->getRequestParameter('id'));
    $title->setTitleFrequencyId($this->getRequestParameter('title_frequency_id') ? $this->getRequestParameter('title_frequency_id') : null);
    $title->setTitleTypeId($this->getRequestParameter('title_type_id') ? $this->getRequestParameter('title_type_id') : null);
    $title->setPublisherId($this->getRequestParameter('publisher_id') ? $this->getRequestParameter('publisher_id') : null);
    $title->setName($this->getRequestParameter('name'));
    $title->setWebsite(trim($this->getRequestParameter('website')));
    $title->setLanguageId($this->getRequestParameter('language_id'));
    $title->setArticlesThreshold($this->getRequestParameter('articles_threshold'));

    $title->setFtpUsername($isNew ? $title->getPublisher()->getFtpUsername() . '_' . $this->getRequestParameter('ftp_username') : $title->getFtpUsername());
    $title->setFtpPassword($isNew ? $this->getRequestParameter('ftp_password') : $title->getFtpPassword());
    $old_is_active = $title->getIsActive();
    $title->setIsActive((int) $this->getRequestParameter('is_active'));
    if ($this->getRequestParameter('is_active') == 1 && $old_is_active == 0) {
      $title->setActivationDate(date('Y-m-d'));
    }
    $title->setCopyright($this->getRequestParameter('copyright'));
    $title->setDirectoryLayout($this->getRequestParameter('directory_layout'));
    //$title->setIsNewswire((int)$this->getRequestParameter('is_newswire'));
    $title->setCountryId($this->getRequestParameter('country_id'));
    $title->setPhotoFeed($this->getRequestParameter('photo_feed'));
    $title->setLogoPath($this->getRequestParameter('logo_path'));

    if ($isNew)
      $title->setContractStartDate(date('Y-m-d'));

    $title->setHasTechnicalIntro((int) $this->getRequestParameter('has_technical_intro'));
    $title->setIsReceiving((int) $this->getRequestParameter('is_receiving'));
    $title->setIsParsing((int) $this->getRequestParameter('is_parsing'));
    $title->setTechnicalNotes($this->getRequestParameter('technical_notes'));
    $title->setTimeliness($this->getRequestParameter('timeliness'));
    $title->setSourceDescription($this->getRequestParameter('source_description'));
    $title->setStoryCount($this->getRequestParameter('story_count'));
    $title->setNotes($this->getRequestParameter('notes'));
    $title->setCity($this->getRequestParameter('city'));
    $title->setAdminsId($title->getPublisher()->getAdminsId());

    $title->setReceivingStatus((int) $this->getRequestParameter('receiving_status'));
    $title->setReceivingStatusComment($this->getRequestParameter('receiving_status_comment'));
    $title->setRank($this->getRequestParameter('rank'));

    //the changes just will take effect when changing the status value not on any update
    if ($title->getReceivingStatus() != $this->getRequestParameter('prev-receiving-status')) {
      if ((int) $this->getRequestParameter('receiving_status') == 1) {
        $title->setReceivingStatusTimestamp(time());
      } else {
        $title->setReceivingStatusTimestamp(0);
      }
    }

    $this->titleConnection = $title->getTitleConnection();
    $this->titleConnection->setConnectionType($this->getRequestParameter('connection_type'));
    $this->titleConnection->setUrl($this->getRequestParameter('url'));
    $this->titleConnection->setFtpPort($this->getRequestParameter('ftp_port') ? $this->getRequestParameter('ftp_port') : '');
    if ('ftp' == $this->titleConnection->getConnectionType() && !$this->titleConnection->getFtpPort()) {
      $this->titleConnection->setFtpPort(21);
    }
    $this->titleConnection->setFilePattern($this->getRequestParameter('file_pattern'));
    $this->titleConnection->setAuthintication($this->getRequestParameter('authintication'));
    $this->titleConnection->setUsername($this->getRequestParameter('username'));
    $this->titleConnection->setPassword($this->getRequestParameter('password'));
    $this->titleConnection->setExtras($this->getRequestParameter('extras'));
    $this->titleConnection->setHasImages($this->getRequestParameter('has_images'));



    $cronDefinition = $this->getRequestParameter('cron_definition');
    $this->cronDefinition = $cronDefinition;

    require_once LINUX_SCRIPTS_PATH . 'standard.php';
    $standard = new Standard();
    $pub = new Publisher();
    $pub = PublisherPeer::retrieveByPK($this->getRequestParameter('publisher_id'));




    //-- validation --\\

    $errMsg = '';
    $errMsg = $standard->getNameErrMsg($this->getRequestParameter('name'));

    if (!$errMsg && $isNew) {
      $errMsg = $standard->getTitleFtpErrMsg($this->getRequestParameter('ftp_username'));
    }

    if (!$errMsg && $isNew) {
      $errMsg = $standard->getPasswordErrorV1($title->getFtpPassword());
    }

    if (!$errMsg) {

      if (!Publisher::hasRealLinuxAccount($pub->getFtpUsername())) {
        //$errMsg = "The Publisher has not a valid linux account";
      } //elseif (!$title->getPublisher()->hasContacts()) {
      //$errMsg = "The publisher has No contacts yet, create publisher contact first.";
      //}	
    }

    if (!$errMsg && $isNew) {
      if (Publisher::hasRealLinuxAccount($title->getFtpUsername())) {
        $errMsg = 'The username is in use, select another one.';
      }
    }

    if (!$errMsg && !$isNew) {
      if (!Publisher::hasRealLinuxAccount($title->getFtpUsername())) {
        //$errMsg = 'The title you are updating has no account [Just available in Database].';
      }
    }


    if (!$errMsg) {
      $errMsg = $standard->getCronDefinitionErrMsg($cronDefinition);
    }


    if (!$errMsg) {
      if (!$title->getCountryId()) {
        $errMsg = 'Country Not set';
      }
    }

    if (!$errMsg) {
      $errMsg = $this->titleConnection->doValidation();
    }

    if (!$errMsg) {
      if (('' != $title->getLogoPath()) && (!file_exists($standard->getAbsolutePathFromSystem($title->getLogoPath())))) {
        $errMsg = 'Logo path Image can not be found, reupload it';
      }
    }

    if (!$errMsg) {
      if ((!$this->isValidUrl($title->getWebsite()) && ( trim($title->getWebsite()) != '' ))) {
        $errMsg = "Invalid website ... it must start with http:// and must have a proper structure";
      }
    }



    //check params
    $errTemplate = !$isNew ? 'edit' : 'create';

    if (isset($errMsg) && $errMsg != '') {
      if ($isNew) {
        $title->setFtpUsername($this->getRequestParameter('ftp_username')); // return the title ftp_username without the publisher_ftp_username to be viewd in template in case of error 
      }
      $this->raisPhisycalError($title, $errMsg, $errTemplate);
    } else {
      $title->save();
      $logoProcess = $this->processLogoPath($title->getId());

      if (!$this->getRequestParameter('id')) {

        //if this is a new title then run the script
        $username = $title->getFtpUsername();
        $pass = $this->getRequestParameter('ftp_password');
        $synId = $title->getId();
        $homePath = HOMEPATH . '/' . $this->getRequestParameter('publisher_id');
        $wcPath = WCPATH . '/' . $this->getRequestParameter('publisher_id');
        $pwcPath = PWCPATH . '/' . $this->getRequestParameter('publisher_id');
        $pubUsername = $title->getPublisher()->getFtpUsername();
        $svnRepo = SVN_REPO_PATH . $this->getRequestParameter('publisher_id') . '/' . $title->getId();


        //run the script to create the account
        $exe = LINUX_USER_MOD . " " . LINUX_SCRIPTS_PATH . "syndAddSource.sh TITLE $username $pass $synId $homePath $wcPath $pubUsername $svnRepo " . $this->getRequestParameter('publisher_id') . ' ' . $pwcPath;
        shell_exec($exe);

        $accountArray = posix_getpwnam($username); //getting the real unix_gid , unix_uid and update the database

        if (!is_array($accountArray)) {
          $title->setFtpUsername($this->getRequestParameter('ftp_username'));
          $this->raisPhisycalError($title, 'URGENT NOTE: Please The IT team as the FTP wasn\'t created.', $errTemplate);
          // FORCE ACTIVATE THE TITLE AND SEND EMAIL TO IT TEAM, TITLE CREATED WITHOUT HOME DIR
          $this->sendEmailNotification($synId);
          $title->setIsActive(0);
        } else {
          $publisherId = $title->getPublisherId();
          $nagiosPath = NAGIOSPATH . $publisherId;

          require LINUX_SCRIPTS_PATH . 'syndGenerateNagiosConf.php';
          if (false) {
            $this->raisPhisycalError($title, 'Could Not Create nagios, try again', 'edit');
          } else {
            $title->setFtpUsername($username);
            $title->setFtpPassword($pass);
            $title->setUnixGid($accountArray['gid']);
            $title->setUnixUid($accountArray['uid']);
            $title->setHomeDir($homePath . '/' . $title->getId() . '/');
            $title->setWorkDir($wcPath . '/' . $title->getId() . '/');
            $title->setParseDir($pwcPath . '/' . $title->getId() . '/');
            $title->setNagiosConfigFile(NAGIOSPATH . $title->getPublisherId() . '/');
            $title->setSvnRepo($svnRepo . '/');
            $title->save();

            $this->titleConnection->update($title->getId());
            $standard->updateTitleCrontab($title->getId(), $title->getName(), false, $cronDefinition, APP_PATH . 'classes/gather/syndGather.php ' . $title->getId());
            $this->useTemplate($title, 'edit', 'Title Created');
          }
        }
      } else {
        $URGENT = false;
        if (is_null($title->getHomeDir()) || is_null($title->getWorkDir()) || is_null($title->getParseDir())) {
          $URGENT = TRUE;
          $title->setIsActive(0);
          $title->save();
        }
        $this->titleConnection->update($title->getId());
        $standard->updateTitleCrontab($title->getId(), $title->getName(), false, $cronDefinition, APP_PATH . 'classes/gather/syndGather.php ' . $title->getId());
        $this->useTemplate($title, 'edit', 'Title Updated');
        if ($URGENT) {
          $this->useTemplate($title, 'edit', 'URGENT NOTE: This title have problem on the FTP directory.');
        }
      }
    }
  }

// end function execute update

  public function executeDeleteTitleForm() {
    require_once LINUX_SCRIPTS_PATH . 'standard.php';
    $standard = new Standard();

    $this->title = TitlePeer::retrieveByPK($this->getRequestParameter('title_id'));
    $this->forward404Unless($this->title);
    $this->delArtclMsg = $this->getRequestParameter('delMsg');
    $this->confirmString = strtolower($standard->getRandomPassword(8));
    $this->setTemplate('deleteTitle');
  }

  public function executeDelete() {
    $titleId = $this->getRequestParameter('id');
    $title = TitlePeer::retrieveByPK($titleId);

    if (!$title instanceof Title) {
      die('wrong title id');
    } else {

      $username = $title->getFtpUsername();
      $homePath = $title->getHomeDir();
      $wcPath = $title->getWorkDir();
      $pwcPath = $title->getParseDir();
      $svnPath = $title->getSvnRepo();
      $nagiosPath = $title->getNagiosConfigFile();

      $shellCmd = LINUX_USER_MOD . ' ' . LINUX_SCRIPTS_PATH . "syndDeleteTitle.sh $titleId $username $homePath $wcPath $pwcPath $svnPath $nagiosPath";
      $msg = shell_exec($shellCmd);
      $msg = '';
      if (!$msg) {
        Title::deleteTitle($titleId);
        $this->redirect("adminTitle/list");
      } else {
        die('error deleting title');
      }
    }
  }

// end executeDelete

  public function raisPhisycalError(&$titleObject, $errMsg, $templateSet) {
    $this->title = clone $titleObject;

    $temp = TitlePeer::retrieveByPK($titleObject->getId());
    if (!is_object($temp)) {
      $this->title->setId('');
    }

    if ($errMsg == 'create') {
      $titleObject->delete();
    }

    $this->msg = $errMsg;
    $this->setTemplate($templateSet);
  }

  public function useTemplate($title, $template, $msg) {
    $this->setTemplate($template);
    $this->msg = $msg;
    $this->title = $title;
  }

  public function executeDeleteTitleArticlesForm() {
    require_once LINUX_SCRIPTS_PATH . 'standard.php';
    $standard = new Standard();

    $this->title = TitlePeer::retrieveByPK($this->getRequestParameter('title_id'));
    $this->forward404Unless($this->title);
    $this->delArtclMsg = $this->getRequestParameter('delArtclMsg');
    $this->confirmString = $standard->getRandomPassword(8);
    $this->setTemplate('deleteArticles');
  }

  public function executeDeleteTitleArticles() {
    $titleId = $this->getRequestParameter('id');
    $title = TitlePeer::retrieveByPK($titleId);
    $this->forward404Unless($title, 'Incorrect title id');

    $params = array();
    $deleteCriteria = $this->getRequestParameter('del-article');

    $params['ids'] = $this->getRequestParameter('ids');  // $deleteCriteria='by-ids'
    $params['revisionFrom'] = $this->getRequestParameter('rev_from');
    $params['revisionTo'] = $this->getRequestParameter('rev_to');
    $params['parseDate'] = $this->getRequestParameter('date');

    try {
      $msg = 'Articles Deleted';
      $title->deleteArticles($deleteCriteria, $params);
    } catch (Exception $e) {
      $msg = $e->getMessage();
    }

    $this->redirect("title/deleteTitleArticlesForm?title_id=$titleId&delArtclMsg=$msg");
  }

  public function executeReprocess() {
    $this->titleId = $this->getRequestParameter('title_id');
    $this->msg = $this->getRequestParameter('msg');
    $row = new syndSvnDataGathering($this->titleId);
    $this->maxRevision = $row->getLastRevision();
  }

  public function executeDoReprocess() {
    $titleId = $this->getRequestParameter('title_id');
    $minRevisoin = $this->getRequestParameter('rev-from');
    $maxRevision = $this->getRequestParameter('rev-to');

    for ($i = $minRevisoin; $i <= $maxRevision; $i ++) {
      $row = new ParsingState();
      $row->setTitleId($titleId);
      $row->setRevisionNum($i);
      $row->save();
    }

    $this->redirect("title/reprocess?title_id=$titleId&msg=done");
  }

  public function executeStoryCount() {
    $titleId = $this->getRequestParameter('title_id');
    $this->title = TitlePeer::retrieveByPK($titleId);

    if (!$this->title) {
      $this->forward404('Invaild title_id enterd into title stroyCount action');
    } else {

      $this->excludeZeros = $this->getRequestParameter('exclude-zeros');
      $this->numOfSpans = $this->getRequestParameter('num_of_spans');
      $this->storyCount = $this->title->getStoryCounting($this->numOfSpans, $this->excludeZeros);
    }
  }

//End function executeStoryCount()

  public function executeGather() {

    $titleId = $this->getRequestParameter('title_id');
    $title = TitlePeer::retrieveByPK($titleId);

    if (!$title) {
      $this->forward404('Invaild title_id enterd into title stroyCount action');
    } else {
      /*
        // This is the old way to gather

        $crontab = new GatherCrontab();
        $crontab->setTitle($title->getId());
        $crontab->setDescription($title->getName());
        $crontab->setScript('/usr/local/syndigate/classes/gather/syndGather.php ' . $title->getId());
        $crontab->setConcurrent(0);
        $crontab->setImplementationId($title->getId());
        $crontab->setCronDefinition(date('i H d m w', time() + 60));
        $crontab->setRunOnce(1);
        $crontab->save();
       */

      require_once APP_PATH . 'conf/pheanstalk.conf.php';
      try {
        $pheanstalk = new Pheanstalk(PHEANSTALK_HOST . ':' . PHEANSTALK_PORT);
        $jobData = json_encode(array(
            'title_id' => $title->getId(),
            'starting_at' => time(),
            'timestamp' => time(),
                )
        );

        $beanstalkedJobId = $pheanstalk->useTube(GATHER_TUBE)->put($jobData, 100, 0, GATHER_TIME_TO_RUN);
        if ($beanstalkedJobId) {
          $msg = urlencode("A gather job raised, check parsing state in moments");
        } else {
          $msg = urlencode("Error putting job");
        }
      } catch (Exception $e) {
        $msg = urlencode("Error in beanstalkd with msg :" . $e->getMessage());
        $this->redirect("title/edit?id=" . $title->getId() . "&msg=$msg");
      }

      $this->redirect("title/edit?id=" . $title->getId() . "&msg=$msg");
    }
  }

  public function executeSetCmsParser() {
    $DEFAULT_SYNDIGATE_DIR = '/usr/local/syndigate/classes/parse/custom/';
    $titleId = $this->getRequestParameter('title_id');
    $title = TitlePeer::retrieveByPK($titleId);


    if (!$title) {
      $this->forward404('Invaild title_id enterd into title stroyCount action');
    } else {



      try {
        $title_name = $title->getName();
        $publisherId = $title->getPublisherId();
        $publisher = PublisherPeer::retrieveByPK($publisherId);
        $publisherName = $publisher->getName();
        $languageId = $title->getLanguageId();
        $language = LanguagePeer::retrieveByPK($languageId);
        $languageName = $language->getName();
        $languageCode = $language->getCode();
        $fileName = $DEFAULT_SYNDIGATE_DIR . "syndParser_$titleId.php";
        $date = date('M d, Y, g:i:s A');
        $user = $_SESSION["symfony/user/sfUser/attributes"]["symfony/user/sfUser/attributes"] ["username"];
        $fileContent = "<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : $publisherName  [# publisher id = $publisherId]
//Title      : $title_name [ $languageName ] 
//Created on : $date
//Author     : $user
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_{$titleId} extends syndParseCms { 
\t public function customInit() { 
\t\t parent::customInit(); 
\t\t \$this->defaultLang = \$this->model->getLanguageId('$languageCode'); 
\t} 
}";



        if (file_exists($fileName)) {
          $msg = urlencode("Failure in implementation, Please Check it With Technical guy.");
        } else {
          file_put_contents($fileName, $fileContent);
          chmod($fileName, 0777);
          if (file_exists($fileName)) {
            $msg = urlencode("It was successfully implemented");
          } else {
            $msg = urlencode("Failure in implementation");
          }
        }
      } catch (Exception $e) {
        $this->redirect("title/edit?id=" . $title->getId() . "&msg=$msg");
      }

      $this->redirect("title/edit?id=" . $title->getId() . "&msg=$msg");
    }
  }

  public function isValidUrl($url) {
    $urlregex = "^(https?|ftp)\:\/\/([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?[a-z0-9+\$_-]+(\.[a-z0-9+\$_-]+)*(\:[0-9]{2,5})?(\/([a-z0-9+\$_-]\.?)+)*\/?(\?[a-z+&\$_.-][a-z0-9;:@/&%=+\$_.-]*)?(#[a-z_.-][a-z0-9+\$_.-]*)?\$";
    if (eregi($urlregex, $url)) {
      return true;
    }

    return false;
  }

  private function processLogoPath($titleId) {

    require_once APP_PATH . 'conf/pheanstalk.conf.php';
    try {
      $pheanstalk = new Pheanstalk(PHEANSTALK_HOST . ':' . PHEANSTALK_PORT);
      $jobData = json_encode(array(
          'title_id' => $titleId,
          'starting_at' => time(),
          'timestamp' => time(),
              )
      );

      $beanstalkedJobId = $pheanstalk->useTube(LOGO_TUBE)->put($jobData, 100, 0, GATHER_TIME_TO_RUN);

      if (!$beanstalkedJobId) {
        return false;
      }
      return true;
    } catch (Exception $e) {
      return false;
    }
  }

  private function sendEmailNotification($id) {
    $body = "Title id $id have problem when created directory ,This issue needs to be fixed ASAP please.";
    $subject = "Title id $id have problem when created [URGENT],";
    $admins = array("khaled@syndigate.info","eyad@syndigate.info");
    //$admins = array("eyad@syndigate.info");

    $mail = new ezcMailComposer();
    $mail->from = new ezcMailAddress('titleCreate@syndigate.info', 'Syndigate no reply');
    foreach ($admins as $admin) {
      $mail->addTo(new ezcMailAddress($admin));
    }
    $mail->subject = $subject;
    $mail->htmlText = nl2br($body);
    $mail->build();
    $transport = new ezcMailMtaTransport();
    $transport->send($mail);
  }

}
