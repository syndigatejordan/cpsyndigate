<h1>Generate and resend articles to cliets</h1>
<p>Generate the NewsML fiels from the articles in the database and send them to the clients</p>
<br />


<?php 
use_helper('Object');  
use_helper("Javascript"); 

$checkedClients = array_keys($sf_request->getParameter('chk'));
?>

<font color="Red"><b><?php echo isset($msg) ? urldecode($msg) : '' ?></b></font><br />

<?php echo form_tag('admin/doresend') ?>
<input type="hidden" name="title_id" value="<?php echo $title->getId() ?>" />
<table width="100%">
	<tr>
		<td><b>Title :</b></td>
		<td colspan="4"><b><?php echo $title->getId() . ' - ' .$title->getName() . ' - ' .  $title->getLanguage()->getName() ?></b></td>
	</tr>
	
	<tr>
		<td><b>Parsed at from :</b></td>
		<td><?php echo input_date_tag('from', $from, 'rich=true readonly=readonly ')." " ?><a onclick="javascript: document.getElementById('from').value=''">clear</a</td>
		<td><b>Parsed at to :</b></td>
		<td><?php echo input_date_tag('to', $to, 'rich=true readonly=readonly ')." " ?><a onclick="javascript: document.getElementById('to').value=''">clear</a</td>
	</tr>
	
	<tr>
		<td valign="top"><b>Clients :</b></td>
		<td colspan="4">
			<div style="border-style:solid" class="scroll">
				<?php
				$class = "#F0F0F0"; 
				foreach ($titleClients as $client) {
					$color = $color == '#F0F0F0' ? 'white' : '#F0F0F0'; 
					$checked = in_array($client->getId(), $checkedClients) ? 'checked' : '';
					?>
					<div style="background-color:<?php echo $color ?>">
						<input type="checkbox"  name="chk[<?php echo $client->getId()?>]" value="<?php echo $client->getId(); ?>" <?php echo $checked ?> /><b><label for="chk[<?php echo $client->getId()?>]"><?php echo $client->getClientName(); ?><font color="Red"><?php echo !$client->getIsActive() ? ' -Inactive' : ''?></font></b></label>
					</div>	
					<?php
				}
				?>
			</div>
			</td>
	</tr>
	
	<tr>
		<td colspan="5"><input type="submit" value="Submit"></td>
	</tr>
</table>
</form>


