<?php


class adminActions extends sfActions
{
  
  public function executeIndex() {
    $this->forward('admin', 'resend'); // Go to resend content page    
  }
  
  
  public function executeResend() {
  	
  	$this->msg 		= $this->getRequestParameter('msg');  	  	  	  	  	
  	$this->titleId 	= $this->getRequestParameter('title_id');
  	  	
  	$this->title 	= TitlePeer::retrieveByPK($this->titleId);
  	if(!$this->title) {
  		$this->msg = "Invalid title"; return;
  	}
  	
  	$this->titleClients 	 = TitlesClientsRulesPeer::getTitleClients($this->title->getId());
  	//$this->titleClientsIds = TitlesClientsRulesPeer::getTitleClientsIds($this->title->getId());
  }
  
  public function executeDoresend() {  	
  	
  	$this->setTemplate('resend');
  	
  	$this->titleId 	= $this->getRequestParameter('title_id');
  	$this->from    	= $this->getRequestParameter('from');
  	$this->to  		= $this->getRequestParameter('to');
  	$this->clients  = $this->getRequestParameter('chk');
  	
  	$this->title 	= TitlePeer::retrieveByPK($this->titleId);
  	
  	$this->titleClients 	= TitlesClientsRulesPeer::getTitleClients($this->title->getId());
  	//$this->titleClientsIds 	= TitlesClientsRulesPeer::getTitleClientsIds($this->title->getId());
  	
  	 
  	//Validation 	
  	
  	$err = false;  	
  	if(!$this->title) {
  		$this->msg = "Invalid title";  	$err = true;	
  	}
  	
  	if((!$this->from) || (!$this->to)) {
  		$this->msg = "Please enter both from & to dates";  		
  		$err = true;
  	} else {
  		if($this->from > $this->to) {
  			$this->msg = "From must be smaller than to date ...";  		
  			$err = true;	
  		}
  	}
  	
  	$clientsIds = $this->getRequestParameter('chk');
  	if(!$clientsIds) {
  		$this->msg = "Please select the clients to resend the data to ...";
  		$err = true;
  	}
  	
  	if(!$err) {
  		$cmd = 'php ' .APP_PATH . "classes/resend.php {$this->title->getId()} " . implode(',', $clientsIds)  . " {$this->from} {$this->to}";
  		$this->msg = shell_exec($cmd);
  		//$this->msg = "The resend script has been executed please check the client ftp.";
  	}
  	  	
  	
  	
  	
  	
  }
  
  
  
}
