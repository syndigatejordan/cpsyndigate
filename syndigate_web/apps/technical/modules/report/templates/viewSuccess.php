<script language="javascript">
function text_to_links(text_has_commas) {
	var myString = new String(text_has_commas);
	var arr = myString.split(','); 
	
	for(var i=0; i<arr.length; i++){
		to_text = '<a href= \'http://syndigate/index.php/article/show/id/' + arr[i]  +  '\' >' + arr[i] + '</a>, ' ;
		document.write(to_text);
	}
}
</script>	


<h2>Gather process</h2>
<?php
if($reportGather) {
	?>
	<table>
		<tr>
			<td>id</td>
			<td><?php echo $reportGather->getId(); ?></td>
		</tr>
		<tr>
			<td>Title</td>
			<td><?php echo $reportGather->getTitleId() ?></td>
		</tr>
		<tr>
			<td>Level</td>
			<td><?php echo $reportGather->getLevel()?></td>
		</tr>
		<tr>
			<td>Num of files</td>
			<td><?php echo $reportGather->getNumberFiles(); ?></td>
		</tr>
		<tr>
			<td>Latest Status</td>
			<td><?php echo $reportGather->getLatestStatus() ?></td>
		</tr>
		<tr>
			<td>Error</td>
			<td><?php echo $reportGather->getError(); ?></td>
		</tr>
		<tr>
			<td>Time</td>
			<td><?php echo $reportGather->getTime(); ?></td>
		</tr>
		
		
		<tr>
			<td>&nbsp;</td>
			<td>
				<b>Report gather logs</b>
				<table>
							<thead>
								<th>Time</th>
								<th>severity</th>
								<th>Category</th>
								<th>Message</th>	
							</thead>
							
							<tbody>
								<?php
									if($logGather) {
										foreach ($logGather as $record) {
											?>
											<tr>
											<td><?php echo date('Y-m-d H:i:s', $record['time']) ?></td>
											<td><?php echo $record['severity'] ?></td>
											<td><?php echo $record['category'] ?></td>
											<td><?php echo $record['message'] ?></td>
											</tr>
											<?php
										}
									}
								?>
							</tbody>
						</table>
					</td>
				</tr>
				
			</table>
			<?php
} // Enf if report gather
?>


<br />
<br />
<h2>Parse process</h2>
<?php 

if($reportParse) {
	?>
	<table>
		<tr>
			<td>Date : </td>
			<td><?php echo $reportParse->getDate(); ?></td>
		</tr>
		<tr>
			<td>Title Id : </td>
			<td><?php echo $reportParse->getTitleId(); ?></td>
		</tr>
		<tr>
			<td>Parser status : </td>
			<td><?php echo $reportParse->getParserStatus() ?></td>
		</tr>
		<tr>
			<td>Parser error : </td>
			<td><?php echo $reportParse->getParserError() ?></td>
		</tr>
		<tr>
			<td>Articles num : </td>
			<td><?php echo $reportParse->getArticleNum()?></td>
		</tr>
		<tr>
			<td>Articles Ids :</td>
			<td><script>text_to_links('<?php echo $reportParse->getArticlesIds() ?>')</script></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<b>Report Parse logs</b>
				<table>
					<thead>
						<th>Time</th>
						<th>Severity</th>
						<th>Category</th>
						<th>Source</th>
						<th>Message</th>
					</thead>
					<tbody>
						<?php
						if($logParse) {
							
							foreach ($logParse as $record) {
								?>
								<tr>
								<td><?php echo date('Y-m-d H:i:s', $record['time']); ?>&nbsp;</td>
								<td><?php echo $record['severity']; ?>&nbsp;</td>
								<td><?php echo $record['category']; ?>&nbsp;</td>
								<td><?php echo $record['source']; ?>&nbsp;</td>
								<td><?php echo $record['message']; ?>&nbsp;</td>
								</tr>
								<?php
							}
						}
						?>
					</tbody>
				</table>
			</td>
		</tr>
	</table>
	<?php
}
?>


<br />
<br />
<h2>Generate process</h2>
<?php 
if($reportGenerate) {
	?>
	<table>
		<tr>
			<td>Time</td>
			<td><?php echo $reportGenerate->getTime()?></td>	
		</tr>
		<tr>
			<td>Title</td>
			<td><?php echo $reportGenerate->getTitleId()?></td>	
		</tr>
		<tr>
			<td>Last Message</td>
			<td><?php $color = !$reportGenerate->getStatus() ? 'red' : 'green'; ?><font color="<?php echo $color ?>"><?php echo $reportGenerate->getError() ?></font></td>
		</tr>
		<tr>
			<td>Articles count</td>
			<td><?php echo $reportGenerate->getArticlesCount()?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<b>Generate logs</b>
				<table>
					<thead>
						<th>Time</th>
						<th>Message</th>
						<th>Severity</th>
					</thead>
					<tbody>
					<?php
						foreach ($logGenerate as $record) {
							?>
							<tr>
							<td><?php echo date('Y-m-d H:i:s', $record['time']) ?></td>
							<td><?php echo $record['message'] ?></td>
							<td><?php echo $record['severity']?></td>
							</tr>
							<?php
						}
					?>
					</tbody>
				</table>
			</td>
		</tr>
	</table>
	<?php
}
?>

<br />
<br />
<h2>Repor Send</h2>
<?php 
if($reportSend) {
	?>
	<table>
		<tr>
			<td>Id : </td>
			<td><?php echo $reportSend->getId() ?></td>
		</tr>
		<tr>
			<td>&nbsp; </td>
			<td>Send logs
				<table>
					<thead>
						<td>Time</td>
						<td>Message</td>
						<td>severity</td>
					</thead>
					<tbody>
						<?php
						foreach ($logSend as $record) {
							?>
							<tr>
								<td><?php echo date('Y-m-d H:i:s', $record['time']) ?></td>
								<td><?php echo $record['message'] ?></td>
								<td><?php echo $record['severity']?></td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</td>			
		</tr>
	</table>
	<?php
}
?>
	
	
	
