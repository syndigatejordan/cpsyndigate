<?php
/**
 * report actions.
 *
 * @package    syndigate
 * @subpackage report
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class reportActions extends autoreportActions
{
	public function executeEdit() {
		$this->forward('report', 'view');
	}

	 public function executeView() {
	 	$this->report = ReportPeer::retrieveByPK($this->getRequestParameter('id'));
	 	if($this->report) {
	 		$this->title			= TitlePeer::retrieveByPK($this->report->getTitleId());
	 		$this->reportGather 	= ReportGatherPeer::retrieveByPK($this->report->getReportGatherId());
	 		$this->reportParse  	= ReportParsePeer::retrieveByPK($this->report->getReportParseId());
	 		$this->reportGenerate 	= ReportGeneratePeer::retrieveByPK($this->report->getReportGenerateId());
	 		$this->reportSend     	= ReportSendPeer::retrieveByPK($this->report->getReportSendId());
	 	
	 		$syndModelMongo 	= syndModelMongo::getInstance();
	 		if($this->reportGather) {
		    	$this->logGather	= $syndModelMongo->getGatherLogs($this->reportGather->getId());
	 		}
	 		
	 		if($this->reportParse) {
	 			$this->logParse    	= $syndModelMongo->getParseLogs($this->reportParse->getId());
	 		}
	 		
	 		if($this->reportGenerate) {
	 			$this->logGenerate 	= $syndModelMongo->getGenerateLogs($this->reportGenerate->getId());
	 		}
	 		
	 		if($this->reportSend ) {
	 			$this->logSend 		= $syndModelMongo->getSendLogs($this->reportSend->getId());
	 		}
 	 	}
	 }
}
