<?php use_helper('I18N', 'Date') ?>
<?php use_stylesheet('/sf/sf_admin/css/main') ?>

<div id="sf_admin_container">

<h1><?php echo __('logParse list', array()) ?></h1>

<div id="sf_admin_header">
<?php include_partial('logParse/list_header', array()) ?>
<?php include_partial('logParse/list_messages', array()) ?>
</div>

<div id="sf_admin_bar">
</div>

<div id="sf_admin_content">
<?php if (!$records): ?>
<?php echo __('no result') ?>
<?php else: ?>
<?php include_partial('logParse/list', array('records'=>$records)) ?>
<?php endif; ?>
<?php include_partial('list_actions') ?>
</div>

<div id="sf_admin_footer">
<?php include_partial('logParse/list_footer', array()) ?>
</div>

</div>
