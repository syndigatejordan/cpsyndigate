<?php

/**
 * logParse actions.
 *
 * @package    syndigate
 * @subpackage logParse
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class logParseActions extends autologParseActions
{
	public function executeList() {
		require_once APP_PATH . 'classes/syndModelMongo.php';
		
		$reportId = (int)$this->getRequestParameter('report_id');
		$syndModelMongo = syndModelMongo::getInstance();
		$this->records	= $syndModelMongo->getParseLogs($reportId);	
	}
}
