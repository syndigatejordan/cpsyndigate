<?php

/**
 * sheet actions.
 *
 * @package    syndigate
 * @subpackage sheet
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class sheetActions extends sfActions
{
	public function executeIndex()
	{
		$this->redirect('sheet/title');
	}
  
	public function executeTitle() {}
	public function executeClient() {}
	public function executeTitlesClients() {}
	
	public function executeStoryCount() {
			
	}
	
	public function executeEmails() {
		
		$this->publishers = PublisherPeer::doSelect(new Criteria());
		$this->clients    = ClientPeer::doSelect(new Criteria());
	}
	
	
	
	/**
	 * Titles Section
	 *
	 *
	 */
	
	public function getTitlesFromFilters($filterType, $filterParams)
	{
		$languageId = $filterParams['languageId'];
		$country    = $filterParams['countryId'];
		
		require_once LINUX_SCRIPTS_PATH . 'standard.php';
		$standard = new Standard();
		
		$filedsToExport   = array();
		$filedsToExport[] = 'Publisher';
		$filedsToExport[] = 'Title';
		
		$c = new Criteria();
		$c->addAscendingOrderByColumn(TitlePeer::PUBLISHER_ID);
		
		if($filterType == 'ACTIVE') {
			$c->add(TitlePeer::IS_ACTIVE, 1);
		}elseif ($filterType == 'LANG') {
			$c->add(TitlePeer::LANGUAGE_ID, $languageId);
			
		} elseif ($filterType == 'COUNTRY') {
			$c->add(TitlePeer::COUNTRY_ID, $countryId);
			 
		} elseif ($filterType == 'CONTRACT') {
			$c->add(TitlePeer::CONTRACT, array('No', 'Sent'), Criteria::IN);
		} elseif ($filterType == 'NO-TECH') {
			$c->add(TitlePeer::HAS_TECHNICAL_INTRO , false);
			
		} elseif ($filterType == 'NOT-RECEIVING') {
			
			$c->add(TitlePeer::IS_RECEIVING, false);
		} elseif ($filterType == 'RECEIVE-NO-PARSE') {
			
			$c->add(TitlePeer::IS_RECEIVING, true);
			$c->add(TitlePeer::IS_PARSING, false);
			
		} elseif ($filterType == 'CONTRACT-END-SOON') {
			$period = 30; //30 day
			$CriticalDate = date('Y-m-d', (time() + ($period * 24 * 60 * 60)));
			$c->add(TitlePeer::CONTRACT_END_DATE , $CriticalDate, Criteria::LESS_EQUAL);
		}
		
		
		
		$titles = TitlePeer::doSelect($c);
		$records= array();
		$record = array();
		
		foreach ($titles as $title) {
			$record['Publisher'] 	= $title->getPublisher()->getName();
			$record['Title'] 		= $title->getName();
			
			if($this->getRequestParameter('chk-active')) {
				$record['Active']	= !$title->getIsActive() ? 'NO' : 'YES';
			}
			
			if($this->getRequestParameter('chk-contract')) {
				$record['Contract']	= $title->getContract();
			}
			
						
			if($this->getRequestParameter('chk-technical')) {
				$record['Tech Intro'] = !$title->getHasTechnicalIntro() ? 'NO' : 'YES';
				$record['Tech Notes'] = $title->getTechnicalNotes();
			}

			if($this->getRequestParameter('chk-account')) {
				$isAccount = $standard->isFtpAccount($title->getFtpUsername());
				$record['Account'] = !$isAccount ? 'NO' : 'YES';
			}
			
			if($this->getRequestParameter('chk-reciving')) {
				$record['Receiving'] = !$title->isTitleSending() ? 'NO' : 'YES';
			}
			
			if($this->getRequestParameter('chk-parsing')) {
				$record['Parsing'] = !$title->isTitleParsing() ? 'NO' : 'YES';
			}
			
			if($this->getRequestParameter('chk-country')) {
				$record['Country'] = $title->getCountry()->getPrintableName();
			}
			
			if($this->getRequestParameter('chk-city')) {
				$record['City'] = $title->getCity();
			}
			
			if($this->getRequestParameter('chk-type')) {
				$record['Type'] = $title->getTitleType()->getType();
			}
			
			if($this->getRequestParameter('chk-language')) {
				$record['Language'] = $title->getTheLanguage();
			}
			
			if($this->getRequestParameter('chk-website')) {
				$record['Website'] = $title->getWebsite();
			}
			
			if($this->getRequestParameter('chk-frequency')) {
				$record['Frequency'] = $title->getTitleFrequency()->getFrequencyType();
			}
			
			if($this->getRequestParameter('chk-newswire')) {
				$record['Newswire'] = !$title->getIsNewswire() ? 'NO' : 'YES';
			}
			
			if($this->getRequestParameter('chk-photofeed')) {
				$record['Photo Feed'] = $title->getPhotoFeed();
			}
			
			if($this->getRequestParameter('chk-bank')) {
				$record['Bank Details'] = !$title->getPublisher()->hasBankDetails() ? 'NO' : 'YES';
			}
									
			if($this->getRequestParameter('chk-timeliness')) {
				$record['Timeliness'] = $title->getTimeLiness();
			}
			
			if($this->getRequestParameter('chk-cntrct-start')) {
				$record['Contract Start'] = $title->getContractStartDate() ;
			}
			
			if($this->getRequestParameter('chk-cntrct-end')) {
				$record['Contract End'] = $title->getContractEndDate();
			}
			
			if($this->getRequestParameter('chk-renewal-period')) {
				$record['Renewal Period'] = $title->getRenewalPeriod();
			}
			
			if($this->getRequestParameter('chk-royalty-rate')) {
				$record['Royalty Rate %'] = $title->getRoyaltyRate();
			}
			
			if($this->getRequestParameter('chk-story-count')) {
				$record['Story Count'] = $title->getStoryCount();
			}
			
			if($this->getRequestParameter('chk-copyright')) {
				$record['Copyright'] = $title->getCopyright();
			}
			
			if($this->getRequestParameter('chk-description')) {
				$record['Description'] = $title->getSourceDescription();
			}
			
			$records[] = $record;
		}
		
		return $records;
	}
	
	
	
	public function executeExportTitle()
	{	
		if($this->getRequestParameter('records')) {
			$records = urldecode($this->getRequestParameter('records'));
			$records = unserialize($records);
		} else {
			$filterType = $this->getRequestParameter('filter-type');
			$languageId = (int)$this->getRequestParameter('lst-language');
			$countryId  = (int)$this->getRequestParameter('lst-country');
		
			$records = $this->getTitlesFromFilters($filterType, array('languageId'=>$languageId, 'countryId'=>$country));
		}
		
		require_once LINUX_SCRIPTS_PATH . "excel.php";
		$export_file = "xlsfile://tmp/Titles " . date('M-d-Y', time()) . ".xls";
		$FP = file_put_contents($export_file, serialize($records));
		
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"" . basename($export_file) . "\"" );
		header ("Content-Description: PHP/INTERBASE Generated Data" );
		readfile($export_file);
		exit;
	}
	
	
	public function executeTitleReport()
	{
		$filterType = $this->getRequestParameter('filter-type');
		$languageId = (int)$this->getRequestParameter('lst-language');
		$countryId  = (int)$this->getRequestParameter('lst-country');
		
		$this->records = $this->getTitlesFromFilters($filterType, array('languageId'=>$languageId, 'countryId'=>$country));
	}
	
	
	
///////////////////////////////////////////////////////////////////////////////////////////////////	
	
	

	/**
	 * Clients Section
	 *
	 *
	 */
	
	
	public function getClientsFromFilters($filterType, $filterParams)
	{
		require_once LINUX_SCRIPTS_PATH . 'standard.php';
		$standard = new Standard();
		
		$filedsToExport   = array();
		$filedsToExport[] = 'Client';
		
		
		$c = new Criteria();
		$c->addAscendingOrderByColumn(ClientPeer::ID);
		
		$clients = ClientPeer::doSelect($c);
		$records = array();
		$record  = array();
		
		foreach ($clients as $client) {
			
			$record['Client'] 	= $client->getClientName();
			
			if($this->getRequestParameter('chk-active')) {
				$record['Active']	= !$client->getIsActive() ? 'No' : 'Yes';
			}
			
			if($this->getRequestParameter('chk-connection-type')) {
				$record['Connection']	= $client->getConnectionType();
			}
			
			if($this->getRequestParameter('chk-contract')) {
				$record['Contract']	= $client->getContract();
			}
									
			if($this->getRequestParameter('chk-cntrct-start')) {
				$record['Contract Start'] = $client->getContractStartDate() ;
			}
			
			if($this->getRequestParameter('chk-cntrct-end')) {
				$record['Contract End'] = $client->getContractEndDate();
			}
			
			if($this->getRequestParameter('chk-renewal-period')) {
				$record['Renewal Period'] = $client->getRenewalPeriod();
			}
			
			if($this->getRequestParameter('chk-royalty-rate')) {
				$record['Royalty Rate %'] = $client->getRoyaltyRate();
			}
			
			$records[] = $record;
		}
		
		return $records;
	}
	
	
	
	public function executeExportClient()
	{	
		if($this->getRequestParameter('records')) {
			$records = urldecode($this->getRequestParameter('records'));
			$records = unserialize($records);
		} else {
			$filterType = $this->getRequestParameter('filter-type');
			$records    = $this->getTitlesFromFilters($filterType, array());
		}
		
		require_once LINUX_SCRIPTS_PATH . "excel.php";
		$export_file = "xlsfile://tmp/Clients " . date('M-d-Y', time()) . ".xls";
		$FP = file_put_contents($export_file, serialize($records));
		
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"" . basename($export_file) . "\"" );
		header ("Content-Description: PHP/INTERBASE Generated Data" );
		readfile($export_file);
		exit;
	}
	
	
	public function executeClientReport()
	{
		$filterType   = $this->getRequestParameter('filter-type');
		$this->records = $this->getClientsFromFilters($filterType, array());
	}
  




//////////////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Titles and clients Deals Section
	 *
	 *
	 */


	public function getTitlesClientsFromFilters($filterType, $filterParams)
	{
		require_once LINUX_SCRIPTS_PATH . 'standard.php';
		require_once LINUX_SCRIPTS_PATH . 'database_helper.php';
		$standard = new Standard();
		$db = new dataBaseHelper();

		$filedsToExport   = array();
		$filedsToExport[] = 'Publisher';
		$filedsToExport[] = 'Title';

		$c = new Criteria();
		$c->addAscendingOrderByColumn(TitlePeer::PUBLISHER_ID);

		$titles 	= TitlePeer::doSelect($c);
		$theClients = ClientPeer::doSelect(new Criteria());

		$records= array();
		$record = array();

		$clientsTitles = array();
		foreach ($theClients as $client) {
			//$clientsTitles[$client->getId()] = $db->getClientDealingTitlesIds($client->getId());
			$clientsTitles[$client->getId()] = $db->getAllTitlesIdAssignedtoClientId($client->getId());
		}
		
		foreach ($titles as $title) {
			
			$record['Publisher'] 	= $title->getPublisher()->getName();
			$record['Title'] 		= $title->getName();
                        $record['Title ID'] 		= $title->getId();
			$record['Language']     = $title->getTheLanguage();
			
			$clients    = array();
			
			foreach ($theClients as $client) {
				
				$name  = $client->getClientName();
				//$clnt ['rules'] = $model->getClientRules($client->getId());
				
				
				//if(!in_array($title->getId(), $db->getClientDealingTitlesIds($client->getId()))) {
				if(!in_array($title->getId(), $clientsTitles[$client->getId()])) {
					$record[$name] = '';
				} else {
					$record[$name] = 'X';
				}
			}
			
			$records[] = $record;
		} //End foreach 

		return $records;
	} // End function
	
	
	
	public function executeExportTitlesClients()
	{	
		
		if($this->getRequestParameter('records')) {
			$records = urldecode($this->getRequestParameter('records'));
			$records = unserialize($records);
		} else {
			$filterType = $this->getRequestParameter('filter-type');
			//$languageId = (int)$this->getRequestParameter('lst-language');
			//$countryId  = (int)$this->getRequestParameter('lst-country');
		
			$records = $this->getTitlesClientsFromFilters($filterType, array('languageId'=>$languageId, 'countryId'=>$country));
		}
		
		//require_once LINUX_SCRIPTS_PATH . "excel.php";
		//$export_file = "xlsfile://tmp/TitlesClients " . date('M-d-Y', time()) . ".xls";
		//$FP = file_put_contents($export_file, serialize($records));
		
		
		// Mohammad Mousa
		$export_file = $this -> addHTML2XLS ( $records ); 
		
		
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"" . basename($export_file) . "\"" );
		header ("Content-Description: PHP/INTERBASE Generated Data" );
		header ('Content-Length: ' . filesize($export_file));
		readfile($export_file);
		exit;
	}

	public function addHTML2XLS ($records) {
		$arrayKeys   = array();
		$arrayValues = array();

		$exportFile = '/tmp/TitlesClientsWithHTML_' . date('Y-M-d H:i:s', time()) . ".xls";
		if (!$handle = fopen($exportFile, 'a')) {
			echo "Cannot open file ($exportFile)";
         	exit;
    	}

		fwrite($handle, '<TABLE CELLSPACING="10" CELLPADDING="10" BORDER="0">');

		$counter = 0;
		$toggle = true;
		foreach ($records as $record) {

			$arrayKeys 	 = array_keys($record);
			$arrayValues = array_values($record);

			if ( $counter == 0 ) {
				fwrite($handle, '<TR>');
				foreach ( $arrayKeys as $client ) {
					fwrite($handle, '<TD STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" HEIGHT="72" ALIGN="CENTER" VALIGN="MIDDLE" BGCOLOR="#004586"><B><FONT SIZE=4 COLOR="#FFFFFF">'. $client .'</FONT></B></TD>');
				}
				fwrite($handle, '</TR>');
			}

			if ( $toggle = !$toggle ) {
				fwrite($handle, '<TR BORDER="2" BGCOLOR="#B3B3B3">');
			} else {
				fwrite($handle, '<TR BORDER="2" BGCOLOR="#FF950E">');
			}

			foreach ( $arrayValues as $value ) {
				//if ( $toggle = !$toggle ) {
				//	fwrite($handle, '<TD STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" HEIGHT="30" BGCOLOR="#B3B3B3"><FONT SIZE=3>'. $value .'</FONT></TD>');
				//} else {
				//	fwrite($handle, '<TD STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" HEIGHT="30" BGCOLOR="#FF950E"><FONT SIZE=3>'. $value .'</FONT></TD>');	
				//}
				fwrite($handle, '<TD STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" HEIGHT="30"><FONT SIZE=3>'. $value .'</FONT></TD>');
			} 
			fwrite($handle, '</TR>');

			$counter++;
		}

		fwrite($handle, '</table>');
		fclose($handle);

		return $exportFile;
	}
	
	
	public function executeTitlesClientsReport()
	{
		$filterType = $this->getRequestParameter('filter-type');
		$languageId = (int)$this->getRequestParameter('lst-language');
		$countryId  = (int)$this->getRequestParameter('lst-country');
		
		$this->records = $this->getTitlesClientsFromFilters($filterType, array('languageId'=>$languageId, 'countryId'=>$country));
		
		// Added By Mohammad Mousa
		echo file_get_contents( $this -> addHTML2XLS ( $this->records ) );
		exit;
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	
	/**
	 * Emails Section
	 *
	 *
	 */
	
	public function getEmailsFromFilters($filterType, $ids, $emailType)
	{		
		require_once LINUX_SCRIPTS_PATH . 'standard.php';
		$standard = new Standard();
		
		$filedsToExport   = array();
		
		if($filterType == 'publisher') {
			$filedsToExport[] = 'Publisher';
		} elseif ($filterType == 'client') {
			$filedsToExport[] = 'Client';
		}
		
		$filedsToExport[] = 'Name';
		$filedsToExport[] = 'Email';
		$filedsToExport[] = 'Type';
		
		
		$records= array();
		$record = array();
		
		foreach ($ids as $id => $on) {
			$object;
			$record = array();
						
			if($filterType == 'publisher') {
				$object = PublisherPeer::retrieveByPK($id);
			} elseif ($filterType == 'client') {
				$object = ClientPeer::retrieveByPK($id);
			}
			
			$contacts = $object->getContacts();
			
			
			foreach ($contacts as $contact) {
				if (!$emailType) {
					
					if($filterType == 'publisher') {
						$record['Publisher'] = $object->getName();
					}elseif ($filterType == 'client') {
						$record['Client'] = $object->getClientName();
					}
					$record['Name']  = $contact->getFullName();
					$record['Email'] = $contact->getEmail();
					$record['Type']  = $contact->getType();
					
				} else {
					if($contact->getType() == $emailType) {
						
						if($filterType == 'publisher') {
							$record['Publisher'] = $object->getName();
						}elseif ($filterType == 'client') {
							$record['Client'] = $object->getClientName();
						}
						$record['Name']  = $contact->getFullName();
						$record['Email'] = $contact->getEmail();
						$record['Type']  = $contact->getType();
					}
				}
				
				if ($record['Publisher'] || $record['Client']) {
					$records[] = $record;
				}
			}
			
			
		}
		return $records;
	}
	
	
	
	public function executeExportEmails()
	{	
		if($this->getRequestParameter('records')) {
			$records = urldecode($this->getRequestParameter('records'));
			$records = unserialize($records);
		} else {
			$filterType 	= $this->getRequestParameter('filter-type');
			$publishersIds  = $this->getRequestParameter('publisher');
			$clientsIds  	= $this->getRequestParameter('client');
			$emailType      = $this->getRequestParameter('email-type');
			
			if($filterType == 'publisher') {
				$records = $this->getEmailsFromFilters($filterType, $publishersIds, $emailType);
			} else {
				$records = $this->getEmailsFromFilters($filterType, $clientsIds, $emailType);
			}
		}
		
		require_once LINUX_SCRIPTS_PATH . "excel.php";
		$export_file = "xlsfile://tmp/Emails " . date('M-d-Y', time()) . ".xls";
		$FP = file_put_contents($export_file, serialize($records));
		
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"" . basename($export_file) . "\"" );
		header ("Content-Description: PHP/INTERBASE Generated Data" );
		readfile($export_file);
		exit;
	}
	
	
	public function executeEmailsReport()
	{
		$filterType 	= $this->getRequestParameter('filter-type');
		$publishersIds  = $this->getRequestParameter('publisher');
		$clientsIds  	= $this->getRequestParameter('client');
		$emailType      = $this->getRequestParameter('email-type');
	
		if ($filterType == 'publisher') {
			$this->records = $this->getEmailsFromFilters($filterType, $publishersIds, $emailType);
			
		} elseif ($filterType == 'client') {
			$this->records = $this->getEmailsFromFilters($filterType, $clientsIds, $emailType);
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Story count section
	 * 
	 */
	
	public function getTitlesFromFiltersForStroyCount($frequency)
	{
		require_once LINUX_SCRIPTS_PATH . 'standard.php';
		$standard = new Standard();
		
		$filedsToExport   = array();
		$filedsToExport[] = 'Publisher';
		$filedsToExport[] = 'Title';
		$filedsToExport[] = 'Language';
		$filedsToExport[] = 'Story Avg';
		
		
		$c = new Criteria();
		$c->addAscendingOrderByColumn(TitlePeer::PUBLISHER_ID);
		$c->add(TitlePeer::IS_ACTIVE , 1);
		$c->add(TitlePeer::TITLE_FREQUENCY_ID , $frequency);		
		
		
		
		$titles = TitlePeer::doSelect($c);
		$records= array();
		$record = array();
		
		foreach ($titles as $title) {
			$record['Publisher'] 	= $title->getPublisher()->getName();
			$record['Title'] 		= $title->getName();
			$record['Language'] 	= $title->getTheLanguage();
			
			$storyReport = $title->getStoryCounting(10, true);
			$storyCount  = array_sum(array_values($storyReport));
			
			if($storyCount !=0) {
				$record['Story Avg'] = (int)($storyCount/count($storyReport));
			} else {
				$record['Story Avg'] =0;
			}
						
						
			$records[] = $record;
		}
		
		return $records;
	}
	
	
	
	public function executeExportStoryCount()
	{	
		if($this->getRequestParameter('records')) {
			$records = urldecode($this->getRequestParameter('records'));
			$records = unserialize($records);
		} else {
			$frequency = $this->getRequestParameter('frequency');
		
			$records = $this->getTitlesFromFiltersForStroyCount($frequency);
		}
		
		require_once LINUX_SCRIPTS_PATH . "excel.php";
		$export_file = "xlsfile://tmp/StroyCount " . date('M-d-Y', time()) . ".xls";
		$FP = file_put_contents($export_file, serialize($records));
		
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"" . basename($export_file) . "\"" );
		header ("Content-Description: PHP/INTERBASE Generated Data" );
		readfile($export_file);
		exit;
	}
	
	
	public function executeStoryCountReport()
	{
		$frequency = $this->getRequestParameter('frequency');
		$this->records = $this->getTitlesFromFiltersForStroyCount($frequency);
	}
	
	
	

	
	
}//end class 
