<?php echo form_tag('sheet/exportTitle'); ?>

<h1>Export Titles to Excel</h1>


<div id="filters">
	<table>
	<tr>
	<td>
	<b>Filters</b>
	<select id="filter-type" name="filter-type" onchange="javascript: changeFilterCriteria()">
		<option value="ALL">ALL</option>
		<option value="ACTIVE">Active users</option>
		<option value="LANG">By Language</option>
		<option value="CONTRACT">Titles without contracts</option>
		<option value="NO-TECH">Titles without tech-Intro</option>
		<option value="NOT-RECEIVING">Not Receiving</option>
		<option value="RECEIVE-NO-PARSE">Receiving but not parsing</option>
		<option value="CONTRACT-END-SOON">Contracts end in less than a month</option>
	</select>
	</td>
	<td>
	<select id="lst-language" name="lst-language" style="display:none">
		<?php 
		$languages = LanguagePeer::doSelect(new Criteria());
		foreach ($languages as $language) {
			echo "<option value='{$language->getId()}'>{$language->getName()}</option>";
		}
		?>
	</select>
	</td>
	
	</tr>
	</table>
	
</div>

<br>
<input type="radio" name="grp-col" value="All"   checked onclick="javascript: hideAndSelect();">All &nbsp; &nbsp; 
<input type="radio" name="grp-col" value="Custom"        onclick="javascript: showDiv();"> Custom<br>
<br>

<div id="columns" style="display:none">

	<br />
    <input type="radio" value = 'all'  name="grp-col-check" checked onclick="javascript: checkAll();">Check All &nbsp; &nbsp; 
    <input type="radio" value = 'none' name="grp-col-check"         onclick="javascript: checkNon();">Check None<br>
    <br />

	<div style="width:30%; float:left;">
		<input id="chk-publisher" name="chk-publisher" type="checkbox" checked disabled><label for="chk-publisher">Publisher</label> <br><br>
		<input id="chk-title"     name="chk-title"     type="checkbox" checked disabled><label for="chk-title">Title</label><br><br>
		<input id="chk-contract"  name="chk-contract"  type="checkbox" checked><label for="chk-contract">Contract</label><br><br>
		<input id="chk-technical" name="chk-technical" type="checkbox" checked><label for="chk-technical">Technical Intro</label><br><br>
		<input id="chk-reciving"  name="chk-reciving"  type="checkbox" checked><label for="chk-reciving">Recieving feeds</label><br><br>
		<input id="chk-parsing"   name="chk-parsing"   type="checkbox" checked><label for="chk-parsing">Parsing</label><br><br>
		<input id="chk-photofeed" name="chk-photofeed" type="checkbox" checked><label for="chk-photofeed">Photo Feed</label><br><br>
		<input id="chk-story-count" name="chk-story-count" type="checkbox" checked><label for="chk-story-county">Story Count</label><br><br>
		<input id="chk-active"      name="chk-active" 	    type="checkbox" checked  ><label for="chk-active">Active</label><br><br>
	</div>

	<div style="width:30%;  float:right;" >
		<input id="chk-country"   	name="chk-country"   	type="checkbox" checked><label for="chk-country">Country</label><br><br>
		<input id="chk-city"      	name="chk-city"      	type="checkbox" checked><label for="chk-city">City</label><br><br>
		<input id="chk-type"      	name="chk-type"      	type="checkbox" checked><label for="chk-type">Type</label><br><br>
		<input id="chk-language"  	name="chk-language"  	type="checkbox" checked><label for="chk-language">language</label><br><br>
		<input id="chk-website"   	name="chk-website"   	type="checkbox" checked><label for="chk-website">Website</label><br><br>
		<input id="chk-frequency" 	name="chk-frequency" 	type="checkbox" checked><label for="chk-frequency">Frequency</label><br><br>
		<input id="chk-copyright"	name="chk-copyright"	type="checkbox" checked><label for="chk-chk-copyright">Copyright</label><br><br>
		<input id="chk-description" name="chk-description" 	type="checkbox" checked><label for="chk-description">Description</label><br><br>
		
	</div>

	<div style="width:30%; float:right">
		<input id="chk-bank"        name="chk-bank"        type="checkbox" checked><label for="chk-bank">Bank Details</label><br><br>
		<input id="chk-newswire"       name="chk-newswire"       type="checkbox" checked><label for="chk-nreswire">Newswire</label><br><br>
		<input id="chk-timeliness"     name="chk-timeliness"     type="checkbox" checked><label for="chk-timeliness">Timeliness</label><br><br>
		<input id="chk-cntrct-start"   name="chk-cntrct-start"   type="checkbox" checked><label for="chk-cntrct-start">Contract Start Date</label><br><br>
		<input id="chk-cntrct-end"     name="chk-cntrct-end"     type="checkbox" checked><label for="chk-cntrct-end">Contract End Date</label><br><br>
		<input id="chk-renewal-period" name="chk-renewal-period" type="checkbox" checked><label for="chk-renewal-period">Renewal period</label><br><br>
		<input id="chk-royalty-rate"   name="chk-royalty-rate"   type="checkbox" checked><label for="chk-royalty-rate">Royalty Rate</label><br><br>
		<input id="chk-account"   	name="chk-account"   	type="checkbox" checked><label for="chk-account">Has account</label><br><br>
	</div>

</div>


<div  align="right" style="width:95%; clear:left"><br><?php echo submit_tag('Export to Excel'); ?> <input type="button" value="Generate" onclick= "javascript: this.form.action='<?php echo $sf_request->getSCRIPTNAME(); ?>/sheet/titleReport' ; this.form.submit();"></div>
</form>


<script>
	function hideAndSelect() {
		checkAll();
		document.getElementById('columns').style.display = 'none';
	}
	
	function showDiv() {
		document.getElementById('columns').style.display = 'block';
	}
	
	
	function changeFilterCriteria() {
		filterType = document.getElementById('filter-type').value;
		
		document.getElementById('lst-language').style.display = 'none';
		
		if(filterType== 'LANG') {
			document.getElementById('lst-language').style.display = 'block';
		}
		
	}
	
	function checkValue(value)
	{
		document.getElementById('chk-contract').checked = value ;
		document.getElementById('chk-technical').checked = value ;
		document.getElementById('chk-reciving').checked = value ;
		document.getElementById('chk-parsing').checked = value ;
		document.getElementById('chk-photofeed').checked = value ;
		document.getElementById('chk-story-count').checked = value ;
		document.getElementById('chk-active').checked = value ; //
		document.getElementById('chk-account').checked = value ;
		document.getElementById('chk-country').checked = value ;
		document.getElementById('chk-city').checked = value ;
		document.getElementById('chk-type').checked = value ;
		document.getElementById('chk-language').checked = value ;
		document.getElementById('chk-website').checked = value ;
		document.getElementById('chk-frequency').checked = value ;
		document.getElementById('chk-copyright').checked = value ;
		document.getElementById('chk-description').checked = value ;
		document.getElementById('chk-bank').checked = value ;
		document.getElementById('chk-newswire').checked = value ;
		document.getElementById('chk-timeliness').checked = value ;
		document.getElementById('chk-cntrct-start').checked = value ;
		document.getElementById('chk-cntrct-end').checked = value ;
		document.getElementById('chk-renewal-period').checked = value ;
		document.getElementById('chk-royalty-rate').checked = value ;
		
	}
	
	function checkAll()
	{
		checkValue(true);
	}
	
	function checkNon()
	{
		checkValue(false);
	}
</script>