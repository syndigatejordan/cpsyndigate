<?php 
use_helper('Object');
use_helper('Javascript');
include_once LINUX_SCRIPTS_PATH . 'database_helper.php';
$dbHelper = new dataBaseHelper();
?>

<?php echo form_tag('sheet/exportEmails'); ?>

<h1>Export Emails Excel</h1>


<div id="filters">
	<table>
	<tr>
	<td>
	<b>Filters</b>
	<select id="filter-type" name="filter-type" onchange="javascript: changeFilterCriteria()">
		<option value="publisher" selected>Publishers</option>
		<option value="client">Clients</option>
	</select>
	</td>
	<td>
		<?php  
		$emailTypeArray = $dbHelper->getEnum('contact', 'type');
		array_unshift($emailTypeArray, 'ALL');
		echo select_tag('email-type', options_for_select($emailTypeArray, 0 ));?>
	</td>
	
	</tr>
	</table>
	
</div>


<div id="custom">
	<br>
	<input type="radio" id="grp-all" name="grp" value="All"    checked onclick="javascript: hideAndSelect();">All<br>
	<input type="radio"              name="grp" value="Custom"         onclick="javascript: changeFilterCriteria();"> Custom<br>
	<br>
</div>




<div id="div-publishers" style="display:none">
	
		<?php 
		foreach ($publishers as $publisher) {
			?> <input id="publisher[<?php echo $publisher->getId() ?>]" name="publisher[<?php echo $publisher->getId() ?>]" type="checkbox" checked ><label for="publisher[<?php echo $publisher->getId() ?>]"><?php echo $publisher->getName() ?></label><br><br> <?php
		}
		?>
</div>



<div id="div-clients" style="display:none">
		<?php 
		foreach ($clients as $client) {
			?> <input id="client[<?php echo $client->getId() ?>]" name="client[<?php echo $client->getId() ?>]" type="checkbox" checked ><label for="client[<?php echo $client->getId() ?>]"><?php echo $client->getClientName() ?></label><br><br> <?php
		}
		?>
</div>


<div  align="right" style="width:95%; clear:left"><br><?php echo submit_tag('Export to Excel'); ?> <input type="button" value="Generate" onclick= "javascript: this.form.action='<?php echo $sf_request->getSCRIPTNAME(); ?>/sheet/emailsReport' ; this.form.submit();"></div>
</form>


<script>
	function hideAndSelect() {
		
		<?php 
		foreach ($publishers as $publisher) {
			?>document.getElementById('publisher[<?php echo $publisher->getId() ?>]').checked = true ;	<?
		}
		
		foreach ($clients as $client) {
			?>document.getElementById('client[<?php echo $client->getId() ?>]').checked = true ;	<?
		}
		?>
		
		document.getElementById('div-publishers').style.display = 'none';
		document.getElementById('div-clients').style.display 	= 'none';
	}
	
	
	function changeFilterCriteria()
	{
		var filterType = document.getElementById('filter-type').value;
		var all        = document.getElementById('grp-all').checked;
		
		if(filterType == 'publisher') {
			document.getElementById('div-clients').style.display = 'none';
			if(all == false) {
				document.getElementById('div-publishers').style.display = 'block';
			}
			
		} else if (filterType == 'client') {
			document.getElementById('div-publishers').style.display = 'none';
			if(all == false) {
				document.getElementById('div-clients').style.display = 'block';
			}
			
		}
	}
</script>