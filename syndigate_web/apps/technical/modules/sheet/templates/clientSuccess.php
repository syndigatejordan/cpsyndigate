<?php echo form_tag('sheet/exportClient'); ?>


<h1>Export Clients to Excel</h1>


<div id="filters">
	<table>
	<tr>
	<td>
	<b>Filters</b>
	<select id="filter-type" name="filter-type" onchange="javascript: changeFilterCriteria()">
		<option value="ALL">ALL</option>
	</select>
	</td>
	<td>
	<select id="lst-language" name="lst-language" style="display:none">
		<?php 
		$languages = LanguagePeer::doSelect(new Criteria());
		foreach ($languages as $language) {
			echo "<option value='{$language->getId()}'>{$language->getName()}</option>";
		}
		?>
	</select>
	</td>
	
	</tr>
	</table>
	
</div>

<br>
<input type="radio" name="grp-col" value="All"   checked onclick="javascript: hideAndSelect();">All<br>
<input type="radio" name="grp-col" value="Custom"        onclick="javascript: showDiv();"> Custom<br>
<br>

<div id="columns" style="display:none">

	<div style="width:30%; float:left;">
		<input id="chk-client"     			name="chk-client"     		type="checkbox" checked disabled><label for="chk-client">Client</label><br><br>
		<input id="chk-connection-type" 	name="chk-connection-type"  type="checkbox" checked><label for="chk-connection-type">Connection Type</label><br><br>
		<input id="chk-active" 				name="chk-active"  			type="checkbox" checked><label for="chk-active">Active</label><br><br>
		<input id="chk-contract"  			name="chk-contract"  		type="checkbox" checked><label for="chk-contract">Contract</label><br><br>
		<input id="chk-cntrct-start"   		name="chk-cntrct-start"   	type="checkbox" checked><label for="chk-cntrct-start">Contract Start Date</label><br><br>
		<input id="chk-cntrct-end"     		name="chk-cntrct-end"     	type="checkbox" checked><label for="chk-cntrct-end">Contract End Date</label><br><br>
		<input id="chk-renewal-period" 		name="chk-renewal-period" 	type="checkbox" checked><label for="chk-renewal-period">Renewal period</label><br><br>
		<input id="chk-royalty-rate"   		name="chk-royalty-rate"   	type="checkbox" checked><label for="chk-royalty-rate">Royalty Rate</label><br><br>
	</div>

</div>


<div  align="right" style="width:95%; clear:left"><br><?php echo submit_tag('Export to Excel'); ?> <input type="button" value="Generate" onclick= "javascript: this.form.action='<?php echo $sf_request->getSCRIPTNAME(); ?>/sheet/clientReport' ; this.form.submit();"></div>
</form>


<script>
	function hideAndSelect() {
		
		document.getElementById('chk-connection-type').checked = true ;
		document.getElementById('chk-active').checked = true ;
		document.getElementById('chk-contract').checked = true ;
		document.getElementById('chk-cntrct-start').checked = true ;
		document.getElementById('chk-cntrct-end').checked = true ;
		document.getElementById('chk-renewal-period').checked = true ;
		document.getElementById('chk-royalty-rate').checked = true ;
			
		document.getElementById('columns').style.display = 'none';
	}
	
	function showDiv() {
		document.getElementById('columns').style.display = 'block';
	}


</script>