<?php echo form_tag('sheet/exportStoryCount'); ?>

<h1>Export Story count to Excel</h1>
<br />
<br />
<b>Story count for last 10 spans</b>

<br />
<br />
<br />
<div id="filters">
	<table>
		<tr>
			<td><b>Frequency</b></td>
			<td><select name="frequency" id="frequency">
			<?php $frequencies = TitleFrequencyPeer::doSelect(new Criteria());
						foreach ($frequencies as $freq) {
							echo "<option value='{$freq->getId()}'>{$freq->getFrequencyType()}</option>";
						}
				?></select></td>
		</tr>
	</table>
	
</div>


<div  align="right" style="width:95%; clear:left"><br><?php echo submit_tag('Export to Excel'); ?> <input type="button" value="Generate" onclick= "javascript: this.form.action='<?php echo $sf_request->getSCRIPTNAME(); ?>/sheet/storyCountReport' ; this.form.submit();"></div>
</form>