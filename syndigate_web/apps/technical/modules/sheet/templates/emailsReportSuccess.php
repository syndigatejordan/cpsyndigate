<pre>
<?php echo form_tag('sheet/exportEmails'); ?><input type="hidden" name="records" value='<?php echo urlencode( serialize($records) ); ?>'><input type="submit" value="Export to Excel"></form>
<?php 
if(0==count($records)) {
	echo '<b>No Records matches this criteria</b>';
} else {
	// get the table header [array keys][need just on record]
	$tableHeaders = array_keys($records[0]);
	
	echo '<br><b>' . count($records) . ' Records Found. </b>';
	
	echo "<table align='center' border='1'>";
	
		echo "<thead>";
		foreach ($tableHeaders as $header) {
			echo "<th>" . $header ."</th>";
		}	
		echo "</thead>";
		
		echo "<tbody>";
		foreach ($records as $record) {
			echo '<tr>';
			foreach ($record as $field) {
				if('' == trim($field)) {
					$field = '&nbsp;';
				}
				echo '<td>' . $field . '</td>';
			}
			echo '</tr>';
			
		}
		echo "</tbody>";
		
	
	echo "</table>";
	
	
	
}

?>
<?php echo form_tag('sheet/exportEmail'); ?><input type="hidden" name="records" value='<?php echo urlencode( serialize($records) ); ?>'><input type="submit" value="Export to Excel"></form>
</pre>