<?php

/**
 * titlesClientsRules actions.
 *
 * @package    syndigate
 * @subpackage titlesClientsRules
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class titlesClientsRulesActions extends sfActions
{
  /**
   * Executes index action
   *
   */
  public function executeIndex() {  	
    $this->forward('default', 'module');
  }
  
  
   public function executeShowclienttitles() {
   	
  		$this->clientId	= $this->getRequestParameter('client_id');
  		$this->msg 		= $this->getRequestParameter('msg');
  		
  		if(!$this->getRequestParameter('titles')) {
  			$c = new Criteria();
  			$c->addAscendingOrderByColumn(TitlePeer::NAME );
  			$this->titles = TitlePeer::doSelect($c);
  		} else {
  			$this->titles = $this->getRequestParameter('titles');
  		}
  		
  		$client = ClientPeer::retrieveByPK($this->clientId);
  		
  		if($client) {  			  			
  			$clientTitles = TitlesClientsRulesPeer::getClientTitles($client->getId());  			
  			
  			foreach ($clientTitles as $title) {  			  				
  				$ct[] =  $title->getId();  				
  			}
  			$this->clientTitles = $ct;  			
  		} else {
  			die('not a client');
  		}
  }
  
  public function executeUpdate() {
  	
  	$this->clientId = $this->getRequestParameter('client_id');
  	$this->msg 		= $this->getRequestParameter('msg');
  	
  	
  	$client = ClientPeer::retrieveByPK($this->clientId);
  	
  	if($client) {
  		$newClientTitles = array_keys($this->getRequestParameter('chk'));
  		
  		if($newClientTitles) {
  			  			
  			$c = new Criteria();
  			$c->add(TitlesClientsRulesPeer::CLIENT_ID, $client->getId(), Criteria::EQUAL);
  			$oldRecords = TitlesClientsRulesPeer::doselect($c);
  			TitlesClientsRulesPeer::dodelete($c);
  			
  			//getting old titles to know whitch deleted and whitch added to send an email
  			$oldIds  = array();
  			$deleted = array();
  			$added   = array();
  			
  			foreach ($oldRecords as $oldRecord) {
  				$oldIds[] = $oldRecord->getTitleId();
  				if(!in_array($oldRecord->getTitleId(), $newClientTitles)) {
  					$deleted[] = $oldRecord->getTitleId();
  				}
  			}
  			
  			
  			foreach ($newClientTitles as $titleId) {
  				$rec = new TitlesClientsRules();
  				$rec->setClientId($client->getId());
  				$rec->setTitleId($titleId);
  				$rec->save();
  				
  				// get the added titles to send it via email
  				if(!in_array($titleId, $oldIds)) {
  					$added[] = $titleId;
  				}
  			}
  			
  			$this->sendClientLicenceChangeEmail($this->clientId, $added, $deleted);
  			$this->redirect("titlesClientsRules/showclienttitles?client_id={$client->getId()}&msg=updated");
  		} else {
  			$msg = urldecode('Error ... You must select titles at least one title.');
  			$this->redirect("titlesClientsRules/showclienttitles?client_id={$client->getId()}&msg=$msg");
  		}
  		
  	} else {
  		$this->redirect('adminTitle/list');
  	}
  }
  
  
  
  public function executeShowtitleclients() {
   	
  		$this->titleId	= $this->getRequestParameter('title_id');
  		$this->msg 		= $this->getRequestParameter('msg');
  		
  		if(!$this->getRequestParameter('clients')) {
  			$c = new Criteria();
  			$c->addAscendingOrderByColumn(ClientPeer::CLIENT_NAME);
  			$this->clients = ClientPeer::doSelect($c);
  		} else {
  			$this->clients = $this->getRequestParameter('clients');
  		}
  		
  		$title = TitlePeer::retrieveByPK($this->titleId);
  		
  		if($title) {  			  			
  			$titleClients = TitlesClientsRulesPeer::getTitleClients($this->titleId);
  			
  			foreach ($titleClients as $client) {  			  				
  				$tc[] =  $client->getId();  				
  			}
  			$this->titleClients = $tc;  			
  		} else {
  			die('not a title');
  		}
  }
  
  
  public function executeUpdatetitleclients() {
  	
  	$this->titleId  = $this->getRequestParameter('title_id');
  	$this->msg 		= $this->getRequestParameter('msg');
  	
  	
  	$title = TitlePeer::retrieveByPK($this->titleId);
  	
  	if($title) {
  		$newTitleClients = array_keys($this->getRequestParameter('chk'));
  		
  		if($newTitleClients) {
  			  			
  			$c = new Criteria();
  			$c->add(TitlesClientsRulesPeer::TITLE_ID, $title->getId(), Criteria::EQUAL);
  			$oldRecords = TitlesClientsRulesPeer::doselect($c);
  			TitlesClientsRulesPeer::dodelete($c);
  			
  			//getting old titles to know whitch deleted and whitch added to send an email
  			$oldIds  = array();
  			$deleted = array();
  			$added   = array();
  			
  			foreach ($oldRecords as $oldRecord) {
  				$oldIds[] = $oldRecord->getClientId();
  				if(!in_array($oldRecord->getClientId(), $newTitleClients)) {
  					$deleted[] = $oldRecord->getClientId();
  				}
  			}
  			
  			foreach ($newTitleClients as $clientId) {
  				$rec = new TitlesClientsRules();
  				$rec->setTitleId($title->getId());
  				$rec->setClientId($clientId);
  				$rec->save();
  				
  				// get the added titles to send it via email
  				if(!in_array($clientId, $oldIds)) {
  					$added[] = $clientId;
  				}
  			}
  			
  			$this->sendTitleLicenceChangeEmail($this->titleId, $added, $deleted);
  			$this->redirect("titlesClientsRules/showtitleclients?title_id={$title->getId()}&msg=updated");
  		} else {
  			$msg = urldecode('Error ... You must select titles at least one client from the list.');
  			$this->redirect("titlesClientsRules/showtitleclients?title_id={$title->getId()}&msg=$msg");
  		}
  		
  	} else {
  		$this->redirect('adminTitle/list');
  	}
  }
  
  
  private function sendTitleLicenceChangeEmail($titleId= 0, $addedIds = array(), $deletedIds = array()) {
  	
  	$title = TitlePeer::retrieveByPK($titleId);
  	$addedClients 	= array();
  	$deletedClients = array();
  	
  	if(!empty($addedIds)) {
  		$addedClients = ClientPeer::retrieveByPKs($addedIds);
  	}
  	
  	if(!empty($deletedIds)) {
  		$deletedClients = ClientPeer::retrieveByPKs($deletedIds);
  	}
  	
  	$template = "Title Licencing changed \n";
  	$template.= "Title #$titleId, " . $title->getName() . "\n\n";
  	
  	if(!empty($addedIds)) {
  		$template.= "\nThe following clients added to the title licensing \n";
  		foreach ($addedClients as $client) {
  			$template .= "\t#" . $client->getId() . ", " . $client->getClientName() ."\n";
  		}
  	}
  	
  	if(!empty($deletedIds)) {
  		$template.= "\nThe following clients removed from the title licensing \n";
  		foreach ($deletedClients as $client) {
  			$template .= "\t#" . $client->getId() . ", " . $client->getClientName() ."\n";
  		}
  	}
  	
  	$this->sendEmailNotification("Title Licencing changed", $template);
  	
  }
  
  
  
  private function sendClientLicenceChangeEmail($clientId= 0, $addedIds = array(), $deletedIds = array()) {
  	
  	$client = ClientPeer::retrieveByPK($clientId);
  	$addedTitles 	= array();
  	$deletedTitles  = array();
  	
  	if(!empty($addedIds)) {
  		$addedTitles = TitlePeer::retrieveByPKs($addedIds);
  	}
  	
  	if(!empty($deletedIds)) {
  		$deletedTitles = TitlePeer::retrieveByPKs($deletedIds);
  	}
  	
  	$template = "Client Licencing changed \n";
  	$template.= "Client #$clientId, " . $client->getClientName() . "\n\n";
  	
  	if(!empty($addedIds)) {
  		$template.= "\nThe following titles added to the client licensing \n";
  		foreach ($addedTitles as $title) {
  			$template .= "\t#" . $title->getId() . ", " . $title->getName() ."\n";
  		}
  	}
  	
  	if(!empty($deletedIds)) {
  		$template.= "\nThe following titles removed from the client licensing \n";
  		foreach ($deletedTitles as $title) {
  			$template .= "\t#" . $title->getId() . ", " . $title->getName() ."\n";
  		}
  	}
  	
  	$this->sendEmailNotification("Client Licencing changed", $template);
  	
  }
  
  private function sendEmailNotification($subject, $body) {
  	$admins = AdminsPeer::doSelect(new Criteria());
  	
  	$mail = new ezcMailComposer();
    $mail->from = new ezcMailAddress( 'licencing@syndigate.info', 'Syndigate no reply ' );
    foreach($admins as $admin) {
    	$mail->addTo( new ezcMailAddress($admin->getEmail()) );
    }    
    
    $mail->subject   = $subject;
    $mail->htmlText  = nl2br($body . "\n\nThis action done by " . $this->getUser()->getAttribute("username")) ;
    $mail->build();
	$transport = new ezcMailMtaTransport();
    $transport->send( $mail ); 
  }
  
  
  
}

