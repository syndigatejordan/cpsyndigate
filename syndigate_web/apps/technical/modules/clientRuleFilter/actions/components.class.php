<?php

/**
 * clientRuleFilter actions.
 *
 * @package    syndigate
 * @subpackage clientRuleFilter
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 3335 2007-01-23 16:19:56Z fabien $
 */
class clientRuleFilterComponents extends sfComponents
{
	public function executeLanguageFilter()
	{	
		if(!$this->filter->getId()) {
			$this->clientRuleFilter = new ClientRuleFilter();
			$this->clientRuleFilter->setFilterType('LANG');
			$this->clientRuleFilter->setClientRuleId($this->filter->getClientRuleId());
		} else {
			$this->clientRuleFilter = ClientRuleFilterPeer::retrieveByPK($this->filter->getId());
		}
		
		$c = new Criteria();
		$c->addAscendingOrderByColumn(LanguagePeer::NAME);
		$this->all = LanguagePeer::doselect($c);
	}
	
	
	public function executeTitleFilter()
	{
		if(!$this->filter->getId()) {
			$this->clientRuleFilter = new ClientRuleFilter();
			$this->clientRuleFilter->setFilterType('TITLE');
			$this->clientRuleFilter->setClientRuleId($this->filter->getClientRuleId());
		} else {
			$this->clientRuleFilter = ClientRuleFilterPeer::retrieveByPK($this->filter->getId());			
		}
		
		$this->all = TitlePeer::doselect(new Criteria());
	}
	
	
	public function executeCategoryFilter()
	{	
		if(!$this->filter->getId()) {
			$this->clientRuleFilter = new ClientRuleFilter();
			$this->clientRuleFilter->setFilterType('CATEGORY');
			$this->clientRuleFilter->setClientRuleId($this->filter->getClientRuleId());
		} else {
			$this->clientRuleFilter = ClientRuleFilterPeer::retrieveByPK($this->filter->getId());			
		}
		
		$this->all = array();
	}
	
	
	public function executeCountryFilter()
	{
		if(!$this->filter->getId()) {
			$this->clientRuleFilter = new ClientRuleFilter();
			$this->clientRuleFilter->setFilterType('COUNTRY');
			$this->clientRuleFilter->setClientRuleId($this->filter->getClientRuleId());
		} else {
			$this->clientRuleFilter = ClientRuleFilterPeer::retrieveByPK($this->filter->getId());			
		}
		
		
		
		$c = new Criteria();
		$titles = TitlePeer::doSelect($c);
		/**
		 * Need to be edited deu to the bad performance here 
		 */
		$this->all = array();
		$countriesIds = array();
		
		foreach ($titles as $title) {
			if( !in_array($title->getCountryId(), $countriesIds) && null != $title->getCountryId() && 0!=$title->getCountryId()) {
				$this->all[] = CountryPeer::retrieveByPK($title->getCountryId());
				$countriesIds[] = $title->getCountryId();
			}
		}
		
	}
	
	

}
