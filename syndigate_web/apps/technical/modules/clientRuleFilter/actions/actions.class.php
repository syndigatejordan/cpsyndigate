<?php
class clientRuleFilterActions extends sfActions
{
  public function executeIndex()
  {
    return $this->forward('clientRuleFilter', 'list');
  }

  public function executeList()
  {
    $this->client_rule_filters = ClientRuleFilterPeer::doSelect(new Criteria());
  }

  public function executeShow()
  {
  	$this->setLayout(false);
    $this->client_rule_filter = ClientRuleFilterPeer::retrieveByPk($this->getRequestParameter('id'));
    $this->forward404Unless($this->client_rule_filter);
  }

  public function executeCreate()
  {
  	$filterType   = $this->getRequestParameter('filter_type') ? $this->getRequestParameter('filter_type') : 'LANG' ;
  	$clientRule   = ClientRulePeer::retrieveByPK($this->getRequestParameter('client_rule_id'));
  	
  	if(!is_object($clientRule)) {
  		$this->forward404Unless($clientRule, 'Invalid client rule');
  	}
  	
  	$this->setLayout('emptyLayout');
  	  	  	
    $this->client_rule_filter = new ClientRuleFilter();
    $this->client_rule_filter->setFilterType($filterType);
    $this->client_rule_filter->setClientRuleId($this->getRequestParameter('client_rule_id'));
    $this->setTemplate('create');
  }

  public function executeEdit()
  {
  	$this->setLayout(false);
    $this->client_rule_filter = ClientRuleFilterPeer::retrieveByPk($this->getRequestParameter('id'));
    
    if( '' != $this->getRequestParameter('filter_type')) {
    	$this->client_rule_filter->setFilterType($this->getRequestParameter('filter_type'));
    }
         
    $this->forward404Unless($this->client_rule_filter);
  }


  public function executeUpdate()
  {
  	$this->setLayout(false);
  	  	
  	if (!$this->getRequestParameter('id')) {
    	
    	$client_rule_filter = new ClientRuleFilter();
    	$isNew = true;
    	$errTemplate = 'create';
    } else {
    	
    	$client_rule_filter = ClientRuleFilterPeer::retrieveByPk($this->getRequestParameter('id'));
      	$this->forward404Unless($client_rule_filter, 'Invalid client rule filter id ');
      	$isNew = false;
      	$errTemplate = 'edit';
    }

    $client_rule_filter->setId($this->getRequestParameter('id'));
    $client_rule_filter->setFilterType($this->getRequestParameter('filter_type_') . $client_rule_filter->getId());
    $client_rule_filter->setClientRuleId($this->getRequestParameter('client_rule_id') ? $this->getRequestParameter('client_rule_id') : null);
    
    if(!is_array($this->getRequestParameter('items'))) {
    	
    	$this->client_rule_filter = $client_rule_filter;
    	$this->msg = 'Select at least one option';
    	
    	$this->setTemplate($errTemplate);
    	    	
    } else {
    	
    	$client_rule_filter->setFilterValue(implode(',', $this->getRequestParameter('items')));
    	$client_rule_filter->save();
    	if(!$isNew) {
    		return $this->redirect('clientRuleFilter/edit?id='.$client_rule_filter->getId());
    	} else {
    		return sfView::NONE;
    	}
    }
       
  }
  
 


  public function executeDelete()
  {
    $client_rule_filter = ClientRuleFilterPeer::retrieveByPk($this->getRequestParameter('id'));

    $this->forward404Unless($client_rule_filter);

    $client_rule_filter->delete();

    return $this->redirect('clientRuleFilter/list');
  }
  
  
  
  /*
  public function raisError(&$object, $errMsg, $templateSet) 
  {
  	
  	$this->client_rule_filter = clone $titleObject;
  	
  	$temp = ClientRulePeer::retrieveByPK($titleObject->getId());
  	if(!is_object($temp)) {
  		$this->title->setId('');
  	}
  	
  	if($errMsg=='create') {
  		$titleObject->delete();
  	}
  	  	
	$this->msg = $errMsg;
	$this->setTemplate($templateSet);
  }
  */
 
  
}
