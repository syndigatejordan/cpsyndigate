<td><?php echo link_to($title->getId(),   	'title/edit?id='.$title->getId()) ?></td>
<td><?php echo link_to($title->getName(), 	'title/edit?id='.$title->getId()) ?></td>
<td><?php echo LanguagePeer::retrieveByPK($title->getLAnguageId()) ?></td>
<td><?php echo link_to($title->getPublisher(), 'publisher/edit?id='.$title->getPublisherId())?></td>
<td><?php echo $title->getTitleFrequency() ?></td>
<td><?php echo TitleTypePeer::retrieveByPK($title->getTitleTypeId())->getType() ?></td>
<!--<td><?php //echo link_to($title->getArticlesCount(), "titleArticles/list?filters[title_id]=".$title->getId()."&filter=filter"); ?></td>-->
<td><?php echo link_to('Articles', "titleArticles/list?filters[title_id]=".$title->getId()."&filter=filter"); ?></td>
<td><?php echo link_to($title->getCachedArticlesToday(), "titleArticles/list?filters[title_id]=".$title->getId()."&filters[parsed_at][from]=" . date('Y-m-d') . "&filter=filter"); ?></td>
<td><?php echo link_to((int)$title->getCachedCurrentSpan(), "titleArticles/list?filters[title_id]=".$title->getId()."&filters[parsed_at][from]=" . date('Y-m-d',$title->spanFromToDate(0)) ."&filter=filter"); ?></td>
<td><?php echo link_to((int)$title->getCachedPrevSpan(), "titleArticles/list?filters[title_id]=".$title->getId()."&filters[parsed_at][from]=" . date('Y-m-d',$title->spanFromToDate(-1)) . "&filters[parsed_at][to]=" . date('Y-m-d',$title->spanToToDate(-1)+1) ."&filter=filter"); ?></td>
<td><?php echo $title->getIsActive() ? image_tag(sfConfig::get('sf_admin_web_dir').'/images/tick.png') : '&nbsp;' ?></td>


<td>
<?php 
if($title->getReceivingStatus() || trim($title->getTechnicalNotes())!='' ) {
	$baloon;
	
	if($title->getReceivingStatus()) { $baloon = "<b>ReceivingStatus</b> : " . $title->getReceivingStatusComment() . '<br />'; }
	if(trim($title->getTechnicalNotes())!='') { $baloon.= "<b>Technical Notes</b> : " .  $title->getTechnicalNotes(); }
	
	?>
	<script type="text/javascript"> description[<?php echo $title->getId() ?>] = '<?php echo $baloon;?>';</script>
	<div style="float:left";>
		<a href="#" onmouseover="setObj(description[<?php echo $title->getId() ?>],'override',550,100)"  onmouseout="clearTimeout(openTimer);stopIt()"><?php echo image_tag(sfConfig::get('sf_admin_web_dir').'/images/cancel.png') ?></a>
	</div>
	<?php
}


if($title->getNotes() ) {

	$notesBaloon = "<b>Notes</b> : " . $title->getNotes() . '<br />'; 
	
	?>
	<script type="text/javascript"> notes[<?php echo $title->getId() ?>] = '<?php echo $notesBaloon;?>';</script>
	<div style="float:right">
		<a href="#" onmouseover="setObj(notes[<?php echo $title->getId() ?>],'override',550,100)"  onmouseout="clearTimeout(openTimer);stopIt()"><?php echo image_tag(sfConfig::get('sf_admin_web_dir').'/images/error.png') ?></a>
	</div>
	<?php
}
?>
</td>