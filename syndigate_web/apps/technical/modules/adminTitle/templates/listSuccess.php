<?php use_helper('I18N', 'Date') ?>
<?php use_stylesheet('modyfiedMain') ?>
<?php require 'js/baloon.js.php' ?>

<div id="sf_admin_container">

<h1>Titles</h1>


<div id="sf_admin_header">
	<?php include_partial('adminTitle/list_header', array('pager' => $pager)) ?>
	<?php include_partial('adminTitle/list_messages', array('pager' => $pager)) ?>
</div>

<div id="sf_admin_bar" >
	<?php include_partial('filters', array('filters' => $filters)) ?>
</div>

<div id="sf_admin_content">

	<div align="right"> <ul class="sf_admin_actions"> <li><?php $publisherId = (int)$filters['publisher_id']; echo ($publisherId >0) ? button_to('New Title', 'title/create?publisher_id=' . $publisherId , array ('class' => 'sf_admin_action_create',)) : button_to ('New Title', 'title/create', array ('class' => 'sf_admin_action_create',)) ?></li> </ul>	
	</div>		
	
	<?php 
	if (!$pager->getNbResults()) {
		echo __('no result');
	}else {
		include_partial('adminTitle/list', array('pager' => $pager));
	}
	?>
	
	<div align="right"> <ul class="sf_admin_actions"> <li><?php $publisherId = (int)$filters['publisher_id']; echo ($publisherId >0) ? button_to('New Title', 'title/create?publisher_id=' . $publisherId , array ('class' => 'sf_admin_action_create',)) : button_to ('New Title', 'title/create', array ('class' => 'sf_admin_action_create',)) ?></li> </ul>	
	</div>	
	
	<?php include_partial('list_actions'); ?>
	
</div>

<div id="sf_admin_footer">
	<?php include_partial('adminTitle/list_footer', array('pager' => $pager)) ?>
</div>



</div>
