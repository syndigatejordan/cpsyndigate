<?php
class adminTitleActions extends autoadminTitleActions
{
	
	public function executeCreate()
	{
		$this->redirect('title/create');
	}
	
	public function executeEdit()
	{
		$this->redirect('title/edit?id=' . $this->getRequestParameter('id'));
	}
	
	public function executeShow()
	{
		$this->redirect('title/edit?id=' . $this->getRequestParameter('id'));
	}
	
	/**
	 * Delete not allowed
	 *
	 */
	public function executeDelete()
	{
		$this->redirect('title/edit?id=' . $this->getRequestParameter('id'));
	}
	
	
}
