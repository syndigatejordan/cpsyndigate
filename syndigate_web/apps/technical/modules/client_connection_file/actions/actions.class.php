<?php

/**
 * client_connection_file actions.
 *
 * @package    syndigate
 * @subpackage client_connection_file
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class client_connection_fileActions extends autoclient_connection_fileActions {

  public function executeFirstDate() {
    $client_id = $this->getRequestParameter('client_id');
    //$title_id = $this->getRequestParameter('title_id');
    $result = array();
    $conn = propel::getConnection();
    if ($this->getRequest()->getMethod() == sfRequest::GET && !empty($client_id)) {
      $this->client_name = ClientPeer::retrieveByPK($client_id);
      //$q = mysql_query("select date(from_unixtime(min(creation_time))) as first,title_id,(select name from title where id=title_id)as title_name from client_connection_file where client_id=$client_id and creation_time!=0 group by title_id order by title_name");      
      $q = mysql_query("select date(from_unixtime(min(creation_time))) as first,
        title_id
        ,title.name as title_name
        ,is_active
        ,language.name as lang_name,
        (select client_id from titles_clients_rules where titles_clients_rules.title_id=client_connection_file.title_id and client_id=$client_id) as rule_id
       from client_connection_file 
       inner join title on title_id=title.id 
       inner join language on language_id=language.id 
       where client_id=$client_id and creation_time!=0 
       group by title_id order by title.name");
      $title_ids = array();
      while ($row = mysql_fetch_assoc($q)) {
        $result[] = $row;
        $title_ids[] = $row['title_id'];
      }
      $q2 = mysql_query("select 'Never' as first,title_id
                        ,title.name as title_name
                        ,is_active
                        ,language.name as lang_name,
                        10 as rule_id
                        from titles_clients_rules 
                        inner join title on title_id=title.id
                        inner join language on language_id=language.id
                        where client_id=$client_id and title_id 
                        NOT IN ('" . join("','", $title_ids) . "')");
      while ($row = mysql_fetch_assoc($q2)) {
        $result[] = $row;
      }
    }
    $this->result = $result;
    $this->count = count($result);
  }

}
