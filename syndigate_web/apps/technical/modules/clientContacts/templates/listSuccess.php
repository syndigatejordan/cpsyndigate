<b><font color="Red"><?php echo isset($msg)? $msg : '';?></font></b><br />
<?php 
use_helper('Object'); 
use_helper("Javascript");  
?>

<table border="1" width="100%">
<thead>
<tr>
  <th>&nbsp;</th>
  <th>Full name</th>
  <th>Title</th>
  <th>Email</th>
  <th>Country</th>
  <th>Type</th>
  <th>Mobile</th>
  <th>SMS</th>
  <th>Email</th>
</tr>
</thead>
<tbody>
<?php foreach ($contacts as $contact): ?>
<tr>
      <td><a href="/technical.php/clientContacts/edit?id=<?php echo $contact->getId()?>&client_id=<?php echo $clientId ?>"><?php echo '#' 							?></a></td>
      <td><a href="/technical.php/clientContacts/edit?id=<?php echo $contact->getId()?>&client_id=<?php echo $clientId ?>"><?php echo $contact->getFullName(); 	?></a></td>
      <td><a href="/technical.php/clientContacts/edit?id=<?php echo $contact->getId()?>&client_id=<?php echo $clientId ?>"><?php echo $contact->getTitle() 		?></a></td>
      <td><a href="/technical.php/clientContacts/edit?id=<?php echo $contact->getId()?>&client_id=<?php echo $clientId ?>"><?php echo $contact->getEmail() 		?></a></td>
      <td><a href="/technical.php/clientContacts/edit?id=<?php echo $contact->getId()?>&client_id=<?php echo $clientId ?>"><?php echo $contact->getCountry() 		?></a></td>
      <td><a href="/technical.php/clientContacts/edit?id=<?php echo $contact->getId()?>&client_id=<?php echo $clientId ?>"><?php echo $contact->getType()   		?></a></td>
      <td><a href="/technical.php/clientContacts/edit?id=<?php echo $contact->getId()?>&client_id=<?php echo $clientId ?>"><?php echo $contact->getMobileNumber() 	?></a></td>
      <td align="center"><?php echo object_checkbox_tag($contact, 'getNotificationSms', array('disabled' => true, 'readonly' => 'yes'));?></td>
      <td align="center"><?php echo object_checkbox_tag($contact, 'getNotificationEmail', array('disabled' => true, 'readonly' => 'yes')); ?></td>
  </tr>
<?php endforeach; ?>
</tbody>
</table>

<?php 
echo form_remote_tag(array('update' => "contacts_table", 'url' => 'clientContacts/create',));
echo input_hidden_tag("client_id", $clientId);
echo submit_tag('Add Contact', 'technical/clientContacts/create?client_id=' . $clientId);
?>
</form>