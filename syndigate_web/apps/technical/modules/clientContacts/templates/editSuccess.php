<div id="contact_<?php echo $contact->getId(); ?>">
<h2>Client Contact</h2>
<br>
<b><font color="Red"><?php echo isset($msg)? $msg : '';?></font></b><br />
<?php 
include_once LINUX_SCRIPTS_PATH . 'database_helper.php';
$dbHelper = new dataBaseHelper(); 
use_helper('Object');
use_helper("Javascript"); 


echo form_tag('clientContacts/update');
echo object_input_hidden_tag($contact, 'getId');
echo input_hidden_tag('client_id', $clientId);
?>

<table>
<tbody>
<tr>
  <th>Contact Type:</th>
  <td><?php echo select_tag('type', options_for_select($dbHelper->getEnum('contact', 'type' ), $contact->getType() ));?></td>
</tr>
<tr>
  <th>Full name:</th>
  <td><?php echo object_input_tag($contact, 'getFullName', array (
  'size' => 80,
)) ?></td>
</tr>
<tr>
  <th>Email:</th>
  <td><?php echo object_input_tag($contact, 'getEmail', array ('size' => 80,)) ?></td>
</tr>
<tr>
  <th>Additional email:</th>
  <td><?php echo object_input_tag($contact, 'getAdditionalEmail', array ('size' => 80,)) ?></td>
</tr>
<tr>
  <th>Title:</th>
  <td><?php echo object_input_tag($contact, 'getTitle', array ('size' => 80,)) ?></td>
</tr>
<tr>
  <th>Department:</th>
  <td><?php echo object_input_tag($contact, 'getDepartment', array ('size' => 7,)) ?></td>
</tr>
<tr>
  <th>Zipcode:</th>
  <td><?php echo object_input_tag($contact, 'getZipcode', array ('size' => 80, )) ?></td>
</tr>
<tr>
  <th>Pobox:</th>
  <td><?php echo object_input_tag($contact, 'getPobox', array ('size' => 7,)) ?></td>
</tr>
<tr>
  <th>Street address:</th>
  <td><?php echo object_textarea_tag($contact, 'getStreetAddress', array ('size' => '30x3',)) ?></td>
</tr>
<tr>
  <th>City:</th>
  <td><?php echo object_input_tag($contact, 'getCityName', array ('size' => 80,)) ?></td>
</tr>
<tr>
  <th>Country:</th>
  <td><?php echo object_select_tag($contact, 'getCountryId', array ('related_class' => 'Country',  'include_blank' => true,)) ?></td>
</tr>
<tr>
  <th>Mobile:</th>
  <td><?php echo object_input_tag($contact, 'getMobileNumber', array ('size' => 80,)) ?></td>
</tr>
<tr>
  <th>Work tele:</th>
  <td><?php echo object_input_tag($contact, 'getWorkNumber', array ('size' => 80,)) ?></td>
</tr>
<tr>
  <th>Home tele:</th>
  <td><?php echo object_input_tag($contact, 'getHomeNumber', array ('size' => 80,)) ?></td>
</tr>
<tr>
  <th>Notify sms:</th>
  <td><?php echo object_checkbox_tag($contact, 'getNotificationSms', array ()) ?></td>
</tr>
<tr>
  <th>Notify email:</th>
  <td><?php echo object_checkbox_tag($contact, 'getNotificationEmail', array ()) ?></td>
</tr>
</tbody>
</table>
<hr />
<?php echo submit_tag('save') ?>
&nbsp; <?php echo button_to('Delete', 'clientContacts/delete?id=' . $contact->getId()) ?>
&nbsp; <?php echo button_to('Cancel', 'client/edit?id=' . $clientId . '#contacts');?>

</form>
</div>