<?php
// auto-generated by sfPropelCrud
// date: 2008/03/04 15:01:05
?>
<?php

/**
 * clientContacts actions.
 *
 * @package    syndigate
 * @subpackage clientContacts
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 3335 2007-01-23 16:19:56Z fabien $
 */
class clientContactsActions extends sfActions
{
  public function executeIndex()
  {
    return $this->forward('clientContacts', 'list');
  }

  public function executeList()
  {
    $this->forward404Unless($this->getRequestParameter('client_id'));
  	
  	$c = new Criteria();
  	$c->add(ClientContactLinkPeer::CLIENT_ID  , $this->getRequestParameter('client_id'));
  	$clientContactLinks = CLientContactLinkPeer::doSelect($c);
  	
  	$contacts = array();
  	foreach ($clientContactLinks as $clientContactLink) {
  		$contacts[] = $clientContactLink->getContact();
  	}
  	
  	$this->setLayout('emptyLayout');
    $this->contacts = $contacts;
    $this->clientId = $this->getRequestParameter('client_id');
    $this->msg      = $this->getRequestParameter('msg');
  }

  public function executeShow()
  {
    $this->contact = ContactPeer::retrieveByPk($this->getRequestParameter('id'));
    $this->forward404Unless($this->contact);
  }

  public function executeCreate()
  {
  	$this->setLayout(false);
  	$clientId = $this->getRequestParameter('client_id');
  	$this->forward404Unless($clientId);
  	$this->clientId = $clientId;
    $this->contact = new Contact();
  }
  
  public function executeEdit()
  {
  	$this->contact 	= ContactPeer::retrieveByPk($this->getRequestParameter('id'));
    $this->clientId = $this->getRequestParameter('client_id');
    $this->forward404Unless($this->contact);
    
  }

  public function executeUpdate()
  {
    if (!$this->getRequestParameter('id')) {
    	$contact = new Contact();
    	$isNew   = true;
    	$client = ClientPeer::retrieveByPK($this->getRequestParameter('client_id'));
    	$this->forward404Unless($client);
    	
    } else {
    	
      $contact = ContactPeer::retrieveByPk($this->getRequestParameter('id'));
      $this->forward404Unless($contact);
      $isNew = false;
    }

    $contact->setId($this->getRequestParameter('id'));
    $contact->setFullName($this->getRequestParameter('full_name'));
    $contact->setEmail($this->getRequestParameter('email'));
    $contact->setAdditionalEmail($this->getRequestParameter('additional_email'));
    $contact->setTitle($this->getRequestParameter('title'));
    $contact->setDepartment($this->getRequestParameter('department'));
    $contact->setZipcode($this->getRequestParameter('zipcode'));
    $contact->setPobox($this->getRequestParameter('pobox'));
    $contact->setStreetAddress($this->getRequestParameter('street_address'));
    $contact->setCityName($this->getRequestParameter('city_name'));
    $contact->setCountryId($this->getRequestParameter('country_id') ? $this->getRequestParameter('country_id') : null);
    $contact->setMobileNumber($this->getRequestParameter('mobile_number'));
    $contact->setWorkNumber($this->getRequestParameter('work_number'));
    $contact->setHomeNumber($this->getRequestParameter('home_number'));
    $contact->setNotificationSms($this->getRequestParameter('notification_sms', 0));
    $contact->setNotificationEmail($this->getRequestParameter('notification_email', 0));
    $contact->setType($this->getRequestParameter('type'));
    
       
    $errMeg = $contact->getErrorMsg();
    
   
    if($errMeg) {
    	$this->msg 			= $errMeg;
    	$this->clientId 	= $this->getRequestParameter('client_id');
    	$this->contact		= $contact;
    	$this->setTemplate($isNew ? 'create' : 'edit');
    } else {
    	
    	$contact->saveDataAndNagios('client');
    	
    	if($isNew) {
    		
    		$clientContactLink = new ClientContactLink();
    		$clientContactLink->setContactId($contact->getId());
    		$clientContactLink->setClientId($this->getRequestParameter('client_id'));
    		$clientContactLink->save();
    		
    		//generating nagios 
    		$clientId 	= $clientContactLink->getClientId();
    		$nagiosPath = $clientContactLink->getClient()->getNagiosFilePath();
    		require LINUX_SCRIPTS_PATH . 'syndGenerateNagiosConfClient.php';
    		
    		    		    		
    		$this->clientId = $this->getRequestParameter('client_id');
    		return $this->redirect('clientContacts/list?client_id='. $clientId . "&msg=contact Added") ;
    		//$this->setTemplate('creation');
  		} else {
    		
    		$c = new Criteria();
    		$c->add(ClientContactLinkPeer::CONTACT_ID , $contact->getId());
    		$link = ClientContactLinkPeer::doSelectone($c);
    		$client   	= $link->getClient();
    		$clientId 	= $client->getId(); 
    		$nagiosPath = $client->getNagiosFilePath();
    		
    		require LINUX_SCRIPTS_PATH . 'syndGenerateNagiosConfClient.php';
    		
    		return $this->redirect('clientContacts/edit?id='.$contact->getId() . '&client_id=' . $clientId . "&msg=Updated") ;
    	}
    }
  }

  
 /**
   * Delete a contact and return to the client page
   * Needs parameter id [the contact id]
   * 
   */
  public function executeDelete()
  {
  	$contactId = $this->getRequestParameter('id');
  	$this->forward404Unless($contactId);
  	
  	$c = new Criteria();
  	$c->add(ClientContactLinkPeer::CONTACT_ID , $contactId);
  	$clientLink = ClientContactLinkPeer::doSelectOne($c);
  	  	
  	ClientContactLink::deleteClientContact($clientLink->getClientId(), $contactId);
  	
  	return $this->redirect('client/edit?id=' . $clientLink->getClientId() . '#contacts');
  }
  
  
   private function getContactErrMsg($contactObj) {
   	return false;
   }
}
