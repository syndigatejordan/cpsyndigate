<?php


class clientRuleActions extends sfActions
{
	
  public function executeIndex() {
  	return $this->forward('clientRule', 'list');
  }

  public function executeList() {
    $this->client_rules = ClientRulePeer::doSelect(new Criteria());
  }

  public function executeShow() {
    $this->client_rule = ClientRulePeer::retrieveByPk($this->getRequestParameter('id'));
    $this->forward404Unless($this->client_rule);
  }

  public function executeCreate() {
  	$this->setLayout(false);
    $this->client_rule = new ClientRule();
    
    if($this->getRequestParameter('client_id')) {
    	$this->client_rule->setClientId($this->getRequestParameter('client_id'));
    	//$this->client_rule->save();
    	//$this->redirect('clientRule/edit?id=' . $this->client_rule->getId() . '&client_id=' . $this->client_rule->getClient()->getId());
    }
    $this->setTemplate('create');
  }
  

  public function executeEdit() {
  	
  	$this->setLayout('emptyLayout');
    $this->client_rule = ClientRulePeer::retrieveByPk($this->getRequestParameter('id'));
    $this->forward404Unless($this->client_rule);
  }

  public function executeUpdate() {
  	
  	$log = new LogClientRule($this->getRequestParameter('client_id'), $this->getUser()->getAttribute("userId"));
  	$log->logPreviousStatus();
  	$logMsg;
  	
  	  	  	
    if (!$this->getRequestParameter('id')) {
    	$client_rule = new ClientRule();
    	$isNew = true;
    	$logMsg = 'Creating new client rule';
    } else {
    	$client_rule = ClientRulePeer::retrieveByPk($this->getRequestParameter('id'));
      	$this->forward404Unless($client_rule);
      	$isNew = false;
      	$logMsg = 'updating client rule with id' . $this->getRequestParameter('id');
    }

    if($this->getRequestParameter('id')) {
    	$client_rule->setId($this->getRequestParameter('id'));
    }
    $client_rule->setClientId($this->getRequestParameter('client_id'));
    $this->forward404If($client_rule->getClientId() == null);
    
    $client_rule->save();
    $log->logCurrentStatus();
    $log->Log($logMsg);
    $log->save();
    
    if($isNew) {
    	return $this->redirect("/client/edit?id=" . $client_rule->getClientId() . "#rules");
    } else {
    	return $this->redirect('clientRule/edit?id='.$client_rule->getId());
    }
    
  }

  public function executeDelete() {
  	
  	
  	$client_rule 	= ClientRulePeer::retrieveByPk($this->getRequestParameter('id'));
  	$this->forward404Unless($client_rule);
  	$clientId 		= $client_rule->getClientId();
  	
  	$log = new LogClientRule($client_rule->getClientId(), $this->getUser()->getAttribute("userId"));
  	$log->logPreviousStatus();
  	  	  	
  	ClientRule::deleteRuleCascade($client_rule->getId());
  	$log->logCurrentStatus();
  	$log->Log('Deleting rule #' . $this->getRequestParameter('id'));
  	$log->save();
  	
  	return $this->redirect('client/edit?id=' . $clientId . '#rules');
  }
  
  
  
  public function executeUpdateFilters()
  {
  	$id 			= $this->getRequestParameter('id');
  	$client_id 		= $this->getRequestParameter('client_id');
  	$filtersCounter = $this->getRequestParameter('filtersCounter');
  	$filter			= $this->getRequestParameter('filter');
  	$items 			= $this->getRequestParameter('items');
  	$errMsg;
  	$errCounter = 0; 
  	
  	//Logiing 
  	$log = new LogClientRule($client_id, $this->getUser()->getAttribute("userId"));
  	$log->logPreviousStatus();
  	$logMsg = "Changing filters ";
  	
  	  	
  	$this->client_rule = ClientRulePeer::retrieveByPK($id);
  	$this->forward404Unless($this->client_rule);
  	
  	$ruleFilters = array();
  	foreach ($filter as $fltrIndex => $flter) {
  		
  		$theFilter;
  		
  		if(''!=$flter['id']) {
  			$theFilter = ClientRuleFilterPeer::retrieveByPK($flter['id']);
  		} else {
  			$theFilter = new ClientRuleFilter();
		}
  		
  		$theFilter->setClientRuleId($flter['client_rule_id']);
  		$theFilter->setFilterType($flter['filter_type_' . $flter['id']]);
  		$theFilter->setFilterValue(implode(',', $items[$fltrIndex][strtolower($theFilter->getFilterType())]));
  		
  		if(!is_array($items[$fltrIndex][strtolower($theFilter->getFilterType())])) {
  			$errCounter++; // action will be deletion
  			$theFilter->delete();
  		} else {
  			$theFilter->save();
  		}
  		
  		
  		
  	}
  	
  	if($errCounter>0) {
  		$errMsg = ", But $errCounter filter[s] has been deleted because not values has been set.";
  	}
  	
  	$log->logCurrentStatus();
  	$log->Log($logMsg);
  	$log->save();
  	
  	$this->setLayout('emptyLayout');
  	$this->msg = 'Rule Updated' . $errMsg;
  	$this->setTemplate('edit');  	
  }
  
  
  public function executeCreateFilter()
  {
  	$this->filterType     = $this->getRequestParameter('filter_type') ? $this->getRequestParameter('filter_type') : 'LANG' ;
  	$this->filtersCounter = $this->getRequestParameter('filtersCounter');
  	$this->client_rule_id = $this->getRequestParameter('client_rule_id');
  	
  	$this->clientRule     = ClientRulePeer::retrieveByPK($this->getRequestParameter('client_rule_id'));

  	if(!is_object($this->clientRule)) {
  		$this->forward404Unless($clientRule, 'Invalid client rule');
  	}
  	$this->setTemplate('createFilter');
  }
  
  
}
