<?php
class clientRuleComponents extends sfComponents
{
	public function executeEditFilter()
	{
		$this->client_rule_filter = ClientRuleFilterPeer::retrieveByPk($this->id);
	
		if(!$this->client_rule_filter) {
			echo 'Error No filter with id ' . $this->id . '<br>';
			return sfView::NONE;
		}
	}
	
	
	public function executeCreateFilter()
	{	
		$filterType   = $this->filter_type ? $this->filter_type : 'LANG' ;
		$clientRule   = ClientRulePeer::retrieveByPK($this->client_rule_id);
		
		if(!is_object($clientRule)) {
  			echo 'Invalid client rule';
  			die();
		} else {
			
			$this->client_rule_filter = new ClientRuleFilter();
    		$this->client_rule_filter->setFilterType($filterType);
    		$this->client_rule_filter->setClientRuleId($this->client_rule_id);
    	}
    	
	}
	
	
	
	
	
	
	public function executeLanguageFilter()
	{	
		if(!$this->filter->getId()) {
			$this->clientRuleFilter = new ClientRuleFilter();
			$this->clientRuleFilter->setFilterType('LANG');
			$this->clientRuleFilter->setClientRuleId($this->filter->getClientRuleId());
		} else {
			$this->clientRuleFilter = ClientRuleFilterPeer::retrieveByPK($this->filter->getId());
		}
		
		$c = new Criteria();
		$c->addAscendingOrderByColumn(LanguagePeer::NAME);
		$this->all = LanguagePeer::doselect($c) ;
	}
	
	
	public function executeTitleFilter()
	{
		if(!$this->filter->getId()) {
			$this->clientRuleFilter = new ClientRuleFilter();
			$this->clientRuleFilter->setFilterType('TITLE');
			$this->clientRuleFilter->setClientRuleId($this->filter->getClientRuleId());
		} else {
			$this->clientRuleFilter = ClientRuleFilterPeer::retrieveByPK($this->filter->getId());			
		}
		
		$c = new Criteria();
		$c->addAscendingOrderByColumn(TitlePeer::NAME);
		$this->all = TitlePeer::doselect($c);
	}
	
	
	public function executeCategoryFilter()
	{	
		if(!$this->filter->getId()) {
			$this->clientRuleFilter = new ClientRuleFilter();
			$this->clientRuleFilter->setFilterType('CATEGORY');
			$this->clientRuleFilter->setClientRuleId($this->filter->getClientRuleId());
		} else {
			$this->clientRuleFilter = ClientRuleFilterPeer::retrieveByPK($this->filter->getId());			
		}
		
		$this->all = array();
	}
	
	
	public function executeCountryFilter()
	{
		if(!$this->filter->getId()) {
			$this->clientRuleFilter = new ClientRuleFilter();
			$this->clientRuleFilter->setFilterType('COUNTRY');
			$this->clientRuleFilter->setClientRuleId($this->filter->getClientRuleId());
		} else {
			$this->clientRuleFilter = ClientRuleFilterPeer::retrieveByPK($this->filter->getId());			
		}
		
		
		
		$c = new Criteria();
		$titles = TitlePeer::doSelect($c);
		/**
		 * Need to be edited deu to the bad performance here 
		 */
		$all = array();
		$countriesIds = array();
		
		foreach ($titles as $title) {
			if( !in_array($title->getCountryId(), $countriesIds) && null != $title->getCountryId()) {
				$all[] = CountryPeer::retrieveByPK($title->getCountryId());
				$countriesIds[] = $title->getCountryId();
			}
		}
		$this->all = $all;
		
	}
	
}