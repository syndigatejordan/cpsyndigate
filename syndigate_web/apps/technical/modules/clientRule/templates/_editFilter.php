<div id="new_filter_for_rule_<?php echo $client_rule_filter->getClientRuleId()?>">
<b><font color="Red"><?php echo isset($msg)? $msg : ''; ?></font></b>


<?php
echo input_hidden_tag("filter[$filtersCounter][id]", $client_rule_filter->getId());
echo input_hidden_tag("filter[$filtersCounter][client_rule_id]", $client_rule_filter->getClientRuleId());

?>

<table>
	<tbody>
		<tr>
			<th>Filter :</th>
			<?php $thePath =  $sf_request->getScriptName() . '/' . sfContext::getInstance()->getModuleName(); ?>
			<?php $selectName  = "filter[$filtersCounter][filter_type_{$client_rule_filter->getId()}]";?>
			<td><?php echo select_tag($selectName, options_for_select( $dbHelper->getEnum('client_rule_filter', 'filter_type' ), strtolower($client_rule_filter->getFilterType())), array('onchange'=>"Javascript: document.getElementById('div_{$filtersCounter}_country_{$client_rule_filter->getClientRuleId()}_{$client_rule_filter->getId()}').style.display = \"none\"; document.getElementById('div_{$filtersCounter}_lang_{$client_rule_filter->getClientRuleId()}_{$client_rule_filter->getId()}').style.display = \"none\"; document.getElementById('div_{$filtersCounter}_title_{$client_rule_filter->getClientRuleId()}_{$client_rule_filter->getId()}').style.display = \"none\"; document.getElementById('div_{$filtersCounter}_category_{$client_rule_filter->getClientRuleId()}_{$client_rule_filter->getId()}').style.display = \"none\"; document.getElementById('div_{$filtersCounter}_' + document.getElementById('filter_{$filtersCounter}_filter_type_{$client_rule_filter->getId()}').value + '_{$client_rule_filter->getClientRuleId()}_{$client_rule_filter->getId()}').style.display = \"block\";"));?></td> 
		</tr>
		
	</tbody>
</table>

<?php
include_component('clientRule', 'languageFilter', array("filter"=>$client_rule_filter, "filtersCounter"=>$filtersCounter ));
include_component('clientRule', 'titleFilter',    array("filter"=>$client_rule_filter, "filtersCounter"=>$filtersCounter ));
include_component('clientRule', 'countryFilter',  array("filter"=>$client_rule_filter, "filtersCounter"=>$filtersCounter ));
include_component('clientRule', 'categoryFilter', array("filter"=>$client_rule_filter, "filtersCounter"=>$filtersCounter ));
?>
</div>