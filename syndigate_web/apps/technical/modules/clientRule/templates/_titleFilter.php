<div class="scroll" id='div_<?php echo $filtersCounter ?>_title_<?php echo $clientRuleFilter->getClientRuleId(); ?>_<?php echo $clientRuleFilter->getId() ?>' style="display:<?php echo strtoupper($clientRuleFilter->getFilterType())=='TITLE' && $clientRuleFilter->getId() ? 'block':'none' ?>">
	
<?php
foreach ($all as $item) {
	
	if(in_array($item->getId(), $clientRuleFilter->getSelectedValues())) {
		$select = true;
	} else {
		$select = false;
	}
	
	echo checkbox_tag("items[$filtersCounter][title][" .$item->getId() . ']' , $item->getId(), $select, array());
	echo "<label for='{$item->getId()}'>{$item->getName()} -- {$item->getTheLanguage()}" .  (!$item->getIsActive() ? " -- <font color='red'>InActive</font>" : '' ) ."</label><br/>";

}
?>
	
</div>
