<b><font color="Red"><?php echo isset($msg) ? $msg : '';  ?></font></b><br>
<?php 
use_helper('Object');
use_helper('Javascript');

require_once LINUX_SCRIPTS_PATH . 'database_helper.php';
$dbHelper = new dataBaseHelper();
?>


<?php
echo form_remote_tag(array('update' => "rule_" . $client_rule->getId() , 'url' => 'clientRule/updateFilters',));
echo object_input_hidden_tag($client_rule, 'getId');
echo object_input_hidden_tag($client_rule, 'getClientId');
?>
<table width="100%">
	<tr>
		<td align="right"><input type="button" value="Add filter" onclick="Javascript: document.getElementById('filtersCounter_' + <?php echo $client_rule->getId(); ?>).value = parseInt(document.getElementById('filtersCounter_' + <?php echo $client_rule->getId(); ?>).value) + 1;  var newdiv = document.createElement('div');  newdiv.id='div_' + document.getElementById('filtersCounter_<?php echo $client_rule->getId(); ?>').value;  newdiv.innerHTML = ''; document.getElementById('inner_rule_<?php echo $client_rule->getId(); ?>').appendChild(newdiv); new Ajax.Updater('div_' + document.getElementById('filtersCounter_' + <?php echo $client_rule->getId(); ?> ).value, '/technical.php/clientRule/createFilter/client_rule_id/<?php echo $client_rule->getId(); ?>/filtersCounter/' + document.getElementById('filtersCounter_' + <?php echo $client_rule->getId(); ?> ).value, {asynchronous:true, evalScripts:false}) ;"></td>
	</tr>
</table>



<br>
<div id="inner_rule_<?php echo $client_rule->getId(); ?>"></div>


<?php 
//Rule Filters

$ruleFilters = $client_rule->getClientRuleFilters();
$filtersCounter =0;

if(count($ruleFilters)==0) {
	
} else {
	
	foreach ($ruleFilters as $ruleFilter) { 
		
		$filtersCounter = $filtersCounter + 1;
		$filterId = $ruleFilter->getId();
		include_component('clientRule', 'editFilter', array("id"=>$filterId, 'filtersCounter'=>$filtersCounter, 'dbHelper'=>$dbHelper ));
		echo '<br /><br />';
		
	}
}
	
?>


	<hr />
	<?php echo input_hidden_tag('filtersCounter_' . $client_rule->getId(), $filtersCounter); ?>
	<div style="float:left">
		<?php echo submit_tag('Save Rule'); ?>
	</div>
</form>



<div style="float:right">
	<?php echo button_to('Delete Rule', 'clientRule/delete?id='.$client_rule->getId(), 'post=true&confirm=Delete Rule?') ?>
</div>

<br>
