<div id="new_rule" style="border-style:solid;">
<h3> &nbsp;New Rule</h3>
<?php 
use_helper('Object');
use_helper('Javascript');

//echo form_remote_tag(array('update' => "new_rule", 'url' => 'clientRule/update',));
echo form_tag('clientRule/update?client_id=' . $sf_request->getparameter('client_id'), array('id'=>'myform', 'name' => 'myform'));
//echo object_input_hidden_tag($client_rule, 'getId');


if(!$sf_request->hasParameter('client_id')) {
	echo "<b>Client : </b> &nbsp; &nbsp;"; 
	echo object_select_tag($client_rule, 'getClientId', array ('related_class' => 'Client',));
} else {
	echo object_input_hidden_tag($client_rule, 'getClientId');
}
?>
<hr>		

&nbsp; &nbsp; &nbsp; <b>Create New Rule?</b> &nbsp; &nbsp; 

<?php echo submit_tag('Create'); ?> &nbsp; &nbsp; 
<?php echo button_to_remote('Cancel', array(  'url'	   => '',
											  'loading'  => "Element.show('show_add_rule')", 	
											  'complete' => "Element.hide('new_rule')",)); ?>
<br>
<br>											  
</form>
</div>
