<div id="cat-mapp"> &nbsp;
<h1>Category Mapping</h1>
<b><font color="red"><?php echo isset($msg)?$msg:'&nbsp;' ?></font></b>

<?php 
use_helper('Javascript');
echo form_remote_tag(array('update' => 'category_mapping', 'url' => 'categoryMapping/updateTitleList?title_id=' . $title->getId(),));
?>
<input type="button" value="add" onclick="javascript:addRowToTable('tbl-categories')">

<table id="tbl-categories" width="90%">
<tbody>
	<tr>
			<th><h2> Title Categories</h2></th>
			<th><h2> IPTC Categories</h2></th>
			<th><h2> Action </h2></th>
	</tr>
	<?php
	
	    foreach ($titleCategories as $category) {
	    	echo '<tr><td>' . htmlspecialchars($category->getCatName()) . '</td>';
			echo     '<td>' .  select_tag('category['  .$category->getId() . ']' , options_for_select(Iptc::getCategories(), $category->getMappedCategoryObj()->getId())) . '</td>' ;
			echo     '<td>' .  link_to_remote('Delete', array('url'=>'categoryMapping/deleteCategoryMapping?title_id=' .$title->getId() . '&original_cat_id=' . $category->getId(), 'update'=>'cat-mapp', 'confirm'  => 'Are you sure you want to delete the mapping ?',)).'<td>';
			echo '</tr>';
		}
	?>
</tbody>
</table>

<?php echo submit_tag('Update') ?>
</div>