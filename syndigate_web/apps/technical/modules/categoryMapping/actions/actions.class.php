<?php

/**
 * categoryMapping actions.
 *
 * @package    syndigate
 * @subpackage categoryMapping
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 3335 2007-01-23 16:19:56Z fabien $
 */
class categoryMappingActions extends sfActions
{
  
  public function executeListTitleMapping()
  {
  	
  	$this->forward404Unless($this->getRequestParameter('title_id'), 'No Title id found');
   	$this->title = TitlePeer::retrieveByPK($this->getRequestParameter('title_id'));
   	$this->forward404Unless($this->title, 'Invalid title id');
   	
   	$this->setLayout(false);
   	$this->titleCategories = CategoryMapping::getTitleCategories($this->title->getId());
   	$this->msg 				  = $this->getRequestParameter('msg');
  }
  
  
  public function executeUpdateTitleList()
  {
  	$title = TitlePeer::retrieveByPK($this->getRequestParameter('title_id'));
  	$this->forward404Unless($title);
  	
  	//check each new category mapping names 
  	if(is_array($this->getRequestParameter('new-original'))) {
  		$newOriginalCategories = $this->getRequestParameter('new-original');
  		
  		foreach ($newOriginalCategories as $neworiginal) {
  			if ($neworiginal == '') {
  				$this->redirect("categoryMapping/listTitleMapping?title_id=" . $this->getRequestParameter('title_id') . '&msg=Error : Empty Original Category');
  			}
  		}
  	}
  	//end check 

  	
  	//update modified values   	
  	foreach ($this->getRequestParameter('category') as $original => $iptc)  {
  		
  		$c = new Criteria();
  		$c->add(CategoryMappingPeer::ORIGINAL_ARTICLE_CATEGORY_ID , $original);
  		$categoryMapping = CategoryMappingPeer::doSelectOne($c);
  		
  		if(!is_object($categoryMapping)) {
  			$categoryMapping = new CategoryMapping();
  			$categoryMapping->setOriginalArticleCategoryId($original);
  		}

  		$categoryMapping->setIptcId($iptc);
  		$categoryMapping->save(); 		
  	}
  	
  	
  	//add new values 
  	if(is_array($newOriginalCategories)) {
  		$newMapping = $this->getRequestParameter('new-mapping');
  		
  		foreach ($newOriginalCategories as $index => $value) {
  			  			  			
  			$originalCategory = new OriginalArticleCategory();
  			$originalCategory->setCatName($value);
  			$originalCategory->setTitleId($this->getRequestParameter('title_id'));
  			$originalCategory->setAddedAt(date('Y-m-d H:i:s'));
  			$originalCategory->save();
  			
  			$categoryMapp = new CategoryMapping();
  			$categoryMapp->setIptcId($newMapping[$index]);
  			$categoryMapp->setOriginalArticleCategoryId($originalCategory->getId());
  			$categoryMapp->save();
  		}
  	}
  	
  	$this->redirect("categoryMapping/listTitleMapping?title_id=" . $this->getRequestParameter('title_id') . '&msg=Mapping Updated');
  	  	
  }
  
  
  
  public function executeDeleteCategoryMapping()
  {
  	$titleId 			= $this->getRequestParameter('title_id');
  	$originalCategoryId = $this->getRequestParameter('original_cat_id');
  	$originalCategory	= OriginalArticleCategoryPeer::retrieveByPK($originalCategoryId);
  	
  	if($originalCategory->getTitleId() != $titleId) {
  		$this->forward404('Invalid Title Id');
  	} else {
  		CategoryMapping::deleteOriginalCategory($originalCategoryId); // unlink map & delete the original category
  		$this->redirect("categoryMapping/listTitleMapping?title_id=$titleId");
  	}
  }
 

}
