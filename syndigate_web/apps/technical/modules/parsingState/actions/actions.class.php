<?php

/**
 * parsingState actions.
 *
 * @package    syndigate
 * @subpackage parsingState
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class parsingStateActions extends autoparsingStateActions
{
	/**
	 * Deny Deleting a record
	 *
	 */
	public function executeDelete()
	{
		$this->redirect('parsingState/list');
	}
	
}
