<?php

/**
 * article  actions.
 *
 * @package    syndigate
 * @subpackage article 
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class articleActions extends autoarticleActions {

  public function executeUpdate() {
    $this->goRedirect();
  }

  public function executeDelete() {
    $this->goRedirect();
  }

  public function executeEdit() {
    $this->goRedirect();
  }

  public function goRedirect() {
    if ($this->getRequestParameter('id')) {
      $this->redirect('article/show/id/' . $this->getRequestParameter('id'));
    } else {
      $this->redirect('article/show/list');
    }
  }

}
