<?php

/**
 * upload actions.
 *
 * @package    syndigate
 * @subpackage upload
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class uploadActions extends sfActions
{
  
  public function executeIndex()
  {
  	//$this->setLayout(false);
  	$this->setLayout('emptyLayout');
    $this->uploadErrMsg 	= $this->getRequestParameter('uploadErrMsg');
  }
  

  
 
  
  public function executeUploadImage()
  {
  	require_once LINUX_SCRIPTS_PATH . 'standard.php';
  	$standard = new Standard();
  	
  	//for uploading images [ for logo path]
  	$allowedMimes = array ('image/gif', 'image/jpeg', 'image/jpg', 'image/pjpeg', 'image/x-png', 'image/png'); 
  	
  	
  	if(trim($this->getRequest()->getFileName('file')) =='') {
  		$msg = 'Invalid file';
  		$this->redirect("upload/index?uploadErrMsg=$msg");
  		
  	} 
  	
  		
  	if(($this->getRequest()->getFileSize('file') > MAX_FILE_SIZE)||($this->getRequest()->getFileSize('file')==0) ) {
  		$msg = "File size must be less than or equal  " . MAX_FILE_SIZE / 1024  . " KB";
  		$this->redirect("upload/index?uploadErrMsg=$msg");
  	} else {
  		if (!in_array($this->getRequest()->getFileType('file'), $allowedMimes)) {
  			$msg = 'Image type is not JPEG or GIF or PNG';
  			$this->redirect("upload/index?uploadErrMsg=$msg");
  		} else {
  			
  			$fileName = $this->getRequest()->getFileName('file');
  			
  			if(is_file(sfConfig::get('sf_upload_dir').'/'.$fileName)) {

  				$fileExploding        =  explode('.', $fileName);
  				$fileWithoutExtension = implode('.',explode('.', $fileName, -1));
  				$extension			  = $fileExploding[count($fileExploding)-1];
  					
  				$counter =1;
  				$newFileName;
  					
  				while (true) {
  					$newFileName = $fileWithoutExtension . "[$counter]" . ".$extension";

  					if(!file_exists(sfConfig::get('sf_upload_dir').'/'. $newFileName)) {
  						break;
  					}
  					$counter++;
  				}
  				$fileName = $newFileName;
  			}
  			@$this->getRequest()->moveFile('file', sfConfig::get('sf_upload_dir').'/'.$fileName);
  			 
  			if(file_exists(sfConfig::get('sf_upload_dir').'/'.$fileName)) {
  				$this->setLayout('emptyLayout');
  				$this->uploadFileName   = $standard->getSystemPathFromAbsolute(sfConfig::get('sf_upload_dir').'/'.$fileName);
  				$this->uploadSuccessMsg = "File Uploaded";
  				$this->setTemplate('index');
  			} else {
  				$msg = 'Could not upload the file';
  				$this->redirect("upload/index?uploadErrMsg=$msg");
  			}
  			
  			
  		}
  		 				
  	}
  		
  }//end Controller
  	
  
 
  
}
