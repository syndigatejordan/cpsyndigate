<?php

/**
 * country actions.
 *
 * @package    syndigate
 * @subpackage country
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class countryActions extends autocountryActions
{
	/**
	 * Deny Deleting a country
	 *
	 */
	public function executeDelete()
	{
		throw new Exception('Invalid operation ... Delete a country not allowed');
	}
	
}
