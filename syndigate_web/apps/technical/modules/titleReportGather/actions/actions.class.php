<?php

/**
 * titleReportGather actions.
 *
 * @package    syndigate
 * @subpackage titleReportGather
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class titleReportGatherActions extends autotitleReportGatherActions
{
	protected function addFiltersCriteria($c)
	{
		$c->add(ReportGatherPeer::TITLE_ID, $this->getRequestParameter('title_id'));
	}
}
