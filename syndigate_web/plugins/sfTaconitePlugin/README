= Taconite Plugin =

The [http://www.malsup.com/jquery/taconite/, jQuery Taconite Plugin] allows you to easily make multiple DOM  updates using the results of a single AJAX call. It processes an XML command document that contain instructions for updating the DOM. So you can extend the sfUJSPlugin by a very powerful feature.

''Warning'': This plugin is in Alpha state, so expect changes to be made in the near future.

== Introduction ==
A command document looks like this:
{{{
<taconite> 
    <append select="#status"> 
        Your order has shipped! 
    </append> 
</taconite> 
}}}

The document above instructs Taconite to append the message Your order has shipped! to the element with the id of status. When Taconite processes this document it will generate and execute the following code:
{{{
$('#status').append('Your order has shipped');
}}}
And this is all initiated using any of the jQuery AJAX methods. For example:
{{{
$.get('orderStatus.php');
}}}

Look closely at the line above. There are no special arguments, options or functions needed to process the command document. The Taconite Plugin automatically detects and processes the server response for you.

That's the basic principle behind the Taconite Plugin - you make a server request, the server returns an XML command document, and the plugin processes the commands within that document. Simple!

=== So What? ===
So far we haven't seen evidence that the Taconite Plugin provides any capabilities beyond what jQuery can do very well on its own. So why use it? The power of Taconite is in its ability to process more than one element in the response document. In fact the command document can have an unlimited number of commands.

Consider the following hypothetical: A user submits an order for 1 dozen red roses and in response you would like to make the following page udpates:

    * Replace the "promotion" div with a "Thank you" message
    * Remove the "emptyMsg" row from the shopping cart and remove any elements with the class "preOrder"
    * Add a row to the shopping cart with the quantity, descption and amount of the order
    * Update the "cartTotal" element with the new shopping cart total

The following command document could effect all of these changes at once:
{{{
<taconite> 
    <replace select="#promotion"> 
        <div>Thank you for your order!</div> 
    </replace> 
 
    <remove select="#emptyMsg, .preOrder" /> 
 
    <append select="#cartTable tbody"> 
        <tr><td>1</td><td>Dozen Red Roses</td><td>$18.99</td></tr> 
    </append> 
 
    <replaceContent select="#cartTotal"> 
        $18.99 
    </replaceContent> 
</taconite> 
}}}

As you see you can always define valid XHTML in the response as usual. And as you're using a view you can use everything that is normally useful in a view including variables, helpers, partials and components. 


== Installation ==
The sfTaconite makes use of jQuery so be sure to have the [wiki:sfUJSPlugin] installed.

  * Install the plugin
{{{
$ symfony plugin-install http://plugins.symfony-project.com/sfTaconitePlugin
}}}
  
  * If it is not installed yet, install the [wiki:sfUJSPlugin], which is necessary to run this plugin.

  * Enable the `sfTaconite` module in the `settings.yml`:
{{{
all:
  .settings:
    enabled_modules:        [default, sfTaconite]
}}}

  * Clear the cache to enable the autoloading to find the new classes
{{{
  $ symfony cc
}}}

=== The sfTaconitePlugin ===
So what does the sfTaconitePlugin does for you ?[[BR]]
First of all it will restructure your code since you have to do some copy & paste action: For every module, you will have to move all of your AJAX-Actions from your {{{actions.class.php}}} to the {{{components.class.php}}}.[[BR]]
Speaking in symfony terms: You will from now on use components for a AJAX-Response, since you always return a XML-Response (even if's empty). And your {{{actions.class.php}}} looks very clean after this move.

To describe the work of the sfTaconite-Plugin, I will use following example and describe it fully to you:
We have following HTML Code in our view
{{{
 <ul id="list">
  <li id="item_1"> First Item - <a class="delete_link">Delete</a></li>
  <li id="item_2"> Second Item - <a class="delete_link">Delete</a></li>
 </ul>
 <a id="add_link">Add Item</a>
}}}
We want to simulate the normal functionality : the add-link shall add an item, an delete-link shall delete the whole item.

 1. For each task we write a own jquery-code using the sfUJSPLugin:
    The delete code works for all defined delete links since we use a class selector but sends the parent's (<li>) id-attribute with the ajax-call.
    You can see how we used the `taco_for(...)` helper-function of the TaconiteHelper : We just specify module (`manage`) and component (`add` rsp.   
    `delete`). [[BR]]
    The `taco_for(...)` generates the taco-URL for you : {{{/taconite/:moduleName/:componentName}}}.
{{{
<?php 
UJS("
  jQuery('#add_link').click(add_item = function(){
    jQuery.get('".taco_for('manage','add')."');
  });
");
UJS("
  jQuery('.delete_link').click(delete_item = function(){
    jQuery.post('".taco_for('manage','delete')."', {id : jQuery(this}.parent().attr('id')});
  });
");
?>
}}}
 1. All taconite URL-Requests will be handled by the Taconite-Module shipped with the sfTaconitePlugin.[[BR]]
 This Module has 
  * one action (briefly it just changes the response-type to ''text/xml'' and passes the request variables ''moduleName'' and ''componentName'') and 
  * one view (the enclosing taconite-tags) which just uses {{{include_component(...)}}} to execute the requested component.
 1. Back in the module it just works like we expect our component to work.
{{{
 class manageComponents extends sfComponents {
   public function add(){
     // code for adding an item in the db, fetchin the new generated ID and passing it to the view
   }
   public function delete(){
     // code for deleting an item in the db, passing the deleted ID into the View
   }
 }
}}}
 1. Since we have two components, each one has its partial `_add.php` and `_delete.php`
{{{
  // _delete.php
  <remove select="#item_<?php echo $id?>"/>

  // _add.php
  <append select="#list">
    <li id="item_<?php echo $id">Another Item - <a class="delete_link">Delete</a></li>
  </append>
  <eval>
   jQuery('.delete_link').click(delete_item); // just amazing: we reassign a defined 'delete_item'-function to all new items using the <eval>-command
  </eval>
}}}
 1. The XML-Responses get parsed by the taconite parser and all jquery-functions will be executed. We didn't define multiple DOM updates but as you see you'll only have to add few more XML-Tags to do so.

==== Questions ====
'''Why a extra plugin for that ?'''[[BR]]
Because so you have all the common code (currently the enclosing taconite-tags) in one place, allowing you to add a functionality that will be used for every ajax-request (e.g. you could log your ajax-requests and analyse your)
Another point is that if you're using undefined entities in your response (e.g. the german umlauts or french accents) you can easily add a entity file to all of your responses to avoid parser errors when the response is parsed by the clients[[BR]]

'''Why do we use components ?'''[[BR]]
As I already said, from now on all of your ajax-actions will return a response, written in XML and enriched by XHTML. We use a view for that since we want to use partials, other components and helpers in it. Server Logic + View is described as a "component" in symfony, so we will use components.


=== The Helper ===
I recommend you to take the time to read throug the whole description of the [http://www.malsup.com/jquery/taconite/, Taconite Plugin] in order to understand how taconite extends your possibilities.
 *  The sfTaconite-Helper just provides one function {{{taco_for()}}} that is just a wrapper (talking of tacos and wraps make one hungry :) )
    for the {{{url_for()}}} function.
 *  The Javscript itself is included when the helper is called for the first time. It will be included to the last position in order to load all other 
    jquery plugins before it.
 *  That gives us the possibility to use every function, defined in all the other previously included jquery plugins, in our taconite responses.
    For further reading take a look at "[http://www.malsup.com/jquery/taconite/#extend, how to extend taconite]"
== Bugs ==
If you find any bugs (and I'm sure there are plenty of them) feel free to mail me at taconite-plugin [at] nikpour [dot] com

== TODOs ==
 * Find a way to define a extra file {{{ajax.class.php}}} or {{{taconite.class.php}}} only for Ajax Actions (in order to separate them from the normal components)
 
== Changelog ==
=== 2007-10-02 | 0.0.1 Alpha ===
 * Initial Release
 
 
 
 
 
 
 
 
 
