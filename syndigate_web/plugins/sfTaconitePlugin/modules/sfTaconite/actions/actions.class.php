<?php

/**
 * taconite actions.
 *
 * @package    cardtrainer.plugins.taconite
 * @author     Navid Nikpour
 */
class sfTaconiteActions extends sfActions
{
  public function executeIndex(){
    // pass the requested module and component
    $this->moduleName = $this->getRequestParameter('target_module'); 
    $this->componentName = $this->getRequestParameter('target_component');
    
    $this->setLayout(false);
    $response->setContentType("text/xml");
  }
}