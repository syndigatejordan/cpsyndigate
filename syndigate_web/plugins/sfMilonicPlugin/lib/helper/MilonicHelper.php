<?php
use_helper('Javascript', 'I18N');

function milonic_options(){
  include(sfConfigCache::getInstance()->checkConfig(sfConfig::get('sf_app_config_dir_name').'/menu.yml'));
  $path = sfContext::getInstance()->getRequest()->getRelativeUrlRoot().'/'.sfConfig::get('app_sf_milonic_dir');
  $js = javascript_include_tag($path.'/js/milonic_src');
  $files = 'if(ns4)_d.write("<scr"+"ipt type=text/javascript src='.javascript_path($path.'/js/mmenuns4').'><\/scr"+"ipt>");';
  $files .='else _d.write("<scr"+"ipt type=text/javascript src='.javascript_path($path.'/js/mmenudom').'><\/scr"+"ipt>");';
  $js .= javascript_tag($files);  
  $text = '';
  foreach (sfConfig::get('mm_milonic_styles') as $style => $values) {
  	$text .= "with($style=new mm_style()){ \n";
  	$text .= _get_milonic_options($values)."\n";
  	$text .= "}\n";
  }
  $text .= _get_milonic_options(sfConfig::get('mm_milonic_config'));
  $js .= javascript_tag($text);
  return $js;
}

function milonic_menu($menu){
  $text = '';
   $user = sfContext::getInstance()->getUser();
   
  foreach ($menu as $nombre => $values) {
    if (!isset($values['credentials']) || $user->hasCredential($values['credentials'])) {	
      $text .= "with(milonic=new menuname(\"$nombre\")){\n";
      $text .= _get_milonic_options($values['params'])."\n";
      foreach ($values['items'] as $item) {
        if (!isset($item['credentials'])|| $user->hasCredential($item['credentials'])) {	
          $text .= 'aI("';
          $text .= _get_milonic_options($item,"","");
      	  $text .= "\")\n";
        }
      }
      $text .= "}\n";
    }
  }
  $text .= 'drawMenus();';
  return javascript_tag($text);
}

function _get_milonic_options($options,$br = "\n",$string_separator = "\"",$item_separator= ";",$add_keys = true)
{
  $text = '';
  
  if (is_array($options)) {
    $is_first = true;
    foreach ($options as $key => $value) {
      if (!$is_first) {
      	$text.= "$item_separator$br";
      }else {
        $is_first = false;
      }
      if ($add_keys) {
        $text .= "$key=";	
      }
  		if ($key === 'url'||$key === 'link') {
  		  if (is_array($value)) {
  		    if (isset($value['function'])) {
  		      $text .= "javascript:{$value['function']}(".(isset($value['params'])?_get_milonic_options($value['params'],'','\'',',',false):'').")$item_separator";
  		      continue;
  		    }
  		  }
  		  else {
  		  	$value = url_for($value);	
  		  }
  		}
  		if (preg_match('/image$/',$key)){
  			$value = image_path($value);
  		}
  		if ($key === 'text') {
  			$value = __($value);
  		}
  		if (is_integer($value)) {
  			$text .= "$value;$br";
  		} elseif (is_bool($value)) {
  		  $text .= $value?"true;$br":"false";
  		} elseif (is_array($value)) {
  		  if (isset($value['var'])) {
  		    $text .= $value['var'];
  		  } else {
  		    $text .= "false";
  		  }
  		} else {    
  		  $text .= "$string_separator$value$string_separator";
  		}
  	}	
  }
  return $text;
}