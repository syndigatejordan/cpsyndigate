<?php

// symfony directories
$sf_symfony_lib_dir  = '/usr/share/php/symfony';
$sf_symfony_data_dir = '/usr/share/php/data/symfony';

define('LINUX_SCRIPTS_PATH', SF_ROOT_DIR . '/batch/');
//define('INCLUDE_PATH', 	'../batch/');
//define('BATCH_PATTH', 	'../batch/');



// for mk_user
define('LINUX_USER_MOD',	'/usr/bin/sudo');
define('HOMEPATH', 			'/syndigate/sources/home');
define('WCPATH', 			'/syndigate/sources/wc');
define('PWCPATH', 			'/syndigate/sources/pwc');
define('SVN_REPO_PATH', 	'/var/svnserve/repos/');
define('CLIENT_HOMEPATH', 	'/syndigate/clients');


// for nagios 
define('NAGIOSPATH', 		'/etc/nagios/sources/');
define('CLIENT_NAGIOSPATH', '/etc/nagios/clients/');
define('NAGIOS_CFG_FILE',	'/usr/local/nagios/etc/nagios.cfg');


//for uploading images [ for logo path]
define('uploadsLogo',	'/usr/local/syndigate/syndigate_web/web');
define('downloadLogo',	'/syndigate/pub/logos/');

$array  		= explode('M', strtoupper(ini_get('upload_max_filesize')));
$maxUploadSize  = $array[0] * 1024 * 1024 ;
  		
$array  		=  explode('M', strtoupper(ini_get('post_max_size')));
$maxPostSize 	= $array[0] * 1024 * 1024;

define('MAX_FILE_SIZE', min($maxPostSize,$maxUploadSize));
define('TITLE_IMAGES_PATH', '/syndigate/imgs/');
define('TITLE_VIDEOS_PATH', '/syndigate/videos/');
define('DOMAIN_NAME', 'http://cp.syndigate.info'); // with http://
