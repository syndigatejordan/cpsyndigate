
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

#-----------------------------------------------------------------------------
#-- accountant
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `accountant`;


CREATE TABLE `accountant`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(50) default '' NOT NULL,
	`password` VARCHAR(50) default '' NOT NULL,
	`last_login` INTEGER default 0 NOT NULL,
	`name` VARCHAR(255) default '' NOT NULL,
	`email` VARCHAR(255) default '' NOT NULL,
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- admins
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `admins`;


CREATE TABLE `admins`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(50) default '' NOT NULL,
	`password` VARCHAR(50) default '' NOT NULL,
	`last_login` INTEGER default 0 NOT NULL,
	`email` VARCHAR(255) default '' NOT NULL,
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- article
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `article`;


CREATE TABLE `article`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`iptc_id` INTEGER  NOT NULL,
	`title_id` INTEGER,
	`article_original_data_id` INTEGER  NOT NULL,
	`language_id` INTEGER  NOT NULL,
	`headline` VARCHAR(255),
	`summary` TEXT,
	`body` TEXT,
	`author` VARCHAR(255),
	`date` DATE,
	`parsed_at` DATETIME,
	`updated_at` DATETIME,
	`has_time` TINYINT,
	`is_Calias_called` INTEGER default 0 NOT NULL,
	`sub_feed` VARCHAR(255) default '' NOT NULL,
	PRIMARY KEY (`id`),
	KEY `article_FKIndex11`(`language_id`),
	KEY `article_FKIndex21`(`article_original_data_id`),
	KEY `article_FKIndex31`(`iptc_id`),
	KEY `article_FKIndex41`(`title_id`),
	KEY `parsed_at`(`parsed_at`),
	KEY `date`(`date`),
	KEY `index_calias`(`is_Calias_called`),
	CONSTRAINT `article_FK_1`
		FOREIGN KEY (`language_id`)
		REFERENCES `language` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT,
	CONSTRAINT `article_FK_2`
		FOREIGN KEY (`article_original_data_id`)
		REFERENCES `article_original_data` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT,
	CONSTRAINT `article_FK_3`
		FOREIGN KEY (`iptc_id`)
		REFERENCES `iptc` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT,
	CONSTRAINT `article_FK_4`
		FOREIGN KEY (`title_id`)
		REFERENCES `title` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- article_calias_files
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `article_calias_files`;


CREATE TABLE `article_calias_files`
(
	`article_id` INTEGER default 0 NOT NULL,
	`file_path` VARCHAR(255) default '' NOT NULL,
	`processed` TINYINT default 0 NOT NULL,
	KEY `index_article_calias_files_processed`(`processed`),
	KEY `index_article_calias_files_article_id`(`article_id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- article_cl_24
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `article_cl_24`;


CREATE TABLE `article_cl_24`
(
	`id` INTEGER default 0 NOT NULL,
	`title_id` INTEGER default 0 NOT NULL,
	`headline` VARCHAR(255) default '' NOT NULL,
	`summary` TEXT  NOT NULL,
	`date` DATE,
	`category` VARCHAR(255) default '' NOT NULL,
	`body` TEXT  NOT NULL,
	`report_date` DATE
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- article_original_data
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `article_original_data`;


CREATE TABLE `article_original_data`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`original_article_id` VARCHAR(255),
	`original_source` VARCHAR(255),
	`issue_number` INTEGER,
	`page_number` INTEGER,
	`reference` VARCHAR(255),
	`extras` VARCHAR(255),
	`hijri_date` VARCHAR(255),
	`original_cat_id` INTEGER,
	`revision_num` INTEGER,
	PRIMARY KEY (`id`),
	KEY `id1`(`id`),
	KEY `the_original_article_id1`(`original_article_id`),
	KEY `original_cat_id_index1`(`original_cat_id`),
	KEY `indx_aod_rev1`(`revision_num`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- article_view
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `article_view`;


CREATE TABLE `article_view`
(
	`id` INTEGER default 0 NOT NULL,
	`title_id` INTEGER,
	`language_id` INTEGER  NOT NULL,
	`headline` VARCHAR(255),
	`date` DATE,
	`parsed_at` DATETIME,
	KEY `title_id`(`title_id`),
	KEY `date`(`date`),
	KEY `parsed_at`(`parsed_at`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- bank_details
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `bank_details`;


CREATE TABLE `bank_details`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`publisher_id` INTEGER  NOT NULL,
	`branch` VARCHAR(255),
	`country_id` INTEGER  NOT NULL,
	`notes` TEXT,
	`account_name` VARCHAR(255),
	`account_number` VARCHAR(255),
	`swift` VARCHAR(255),
	`iban` VARCHAR(255),
	`bank_name` VARCHAR(255),
	`bank_city` VARCHAR(255),
	PRIMARY KEY (`id`),
	KEY `bank_details_FKIndex1`(`publisher_id`),
	KEY `bank_details_FKIndex2`(`country_id`),
	CONSTRAINT `bank_details_FK_1`
		FOREIGN KEY (`publisher_id`)
		REFERENCES `publisher` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT,
	CONSTRAINT `bank_details_FK_2`
		FOREIGN KEY (`country_id`)
		REFERENCES `country` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- category_mapping
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `category_mapping`;


CREATE TABLE `category_mapping`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`iptc_id` INTEGER  NOT NULL,
	`original_article_category_id` INTEGER,
	PRIMARY KEY (`id`),
	KEY `category_mapping_FKIndex1`(`iptc_id`),
	KEY `category_mapping_FKIndex2`(`original_article_category_id`),
	CONSTRAINT `category_mapping_FK_1`
		FOREIGN KEY (`iptc_id`)
		REFERENCES `iptc` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT,
	CONSTRAINT `category_mapping_FK_2`
		FOREIGN KEY (`original_article_category_id`)
		REFERENCES `original_article_category` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- client
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `client`;


CREATE TABLE `client`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`client_name` VARCHAR(255)  NOT NULL,
	`url` VARCHAR(255)  NOT NULL,
	`connection_type` CHAR  NOT NULL,
	`home_dir` VARCHAR(255)  NOT NULL,
	`username` VARCHAR(255)  NOT NULL,
	`password` VARCHAR(255)  NOT NULL,
	`ftp_username` VARCHAR(255)  NOT NULL,
	`ftp_password` VARCHAR(255)  NOT NULL,
	`dir_layout` CHAR default 'dates' NOT NULL,
	`is_active` TINYINT default 0 NOT NULL,
	`logo_path` VARCHAR(255),
	`ftp_port` INTEGER default 21,
	`remote_dir` VARCHAR(255),
	`contract` CHAR default 'No' NOT NULL,
	`contract_start_date` DATE,
	`contract_end_date` DATE,
	`renewal_period` TEXT,
	`royalty_rate` FLOAT default 0 NOT NULL,
	`nagios_file_path` VARCHAR(255),
	`zipped` TINYINT default 0 NOT NULL,
	`generate_type` CHAR,
	`notes` TEXT,
	`xml_type` CHAR default 'newsml' NOT NULL,
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- client_connection
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `client_connection`;


CREATE TABLE `client_connection`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`client_id` INTEGER  NOT NULL,
	`connection_type` CHAR  NOT NULL,
	`url` VARCHAR(255)  NOT NULL,
	`username` VARCHAR(255)  NOT NULL,
	`password` VARCHAR(255)  NOT NULL,
	`ftp_port` INTEGER default 21,
	`remote_dir` VARCHAR(255),
	PRIMARY KEY (`id`),
	KEY `client_connection_FKIndex1`(`client_id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- client_connection_file
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `client_connection_file`;


CREATE TABLE `client_connection_file`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`client_id` INTEGER default 0 NOT NULL,
	`client_connection_id` INTEGER default 0 NOT NULL,
	`title_id` INTEGER default 0 NOT NULL,
	`revision` INTEGER default 0 NOT NULL,
	`local_file` VARCHAR(255) default '' NOT NULL,
	`is_sent` TINYINT default 0 NOT NULL,
	`num_retry` INTEGER default 0 NOT NULL,
	`err_msg` VARCHAR(255) default '' NOT NULL,
	`report_id` INTEGER default 0 NOT NULL,
	`creation_time` INTEGER default 0 NOT NULL,
	`sent_time` INTEGER default 0 NOT NULL,
	PRIMARY KEY (`id`),
	KEY `inx_ccf_is_sent`(`is_sent`),
	KEY `inx_ccf_num_retry`(`num_retry`),
	KEY `sent_time`(`sent_time`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- client_connection_sent
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `client_connection_sent`;


CREATE TABLE `client_connection_sent`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`connection_id` INTEGER  NOT NULL,
	`sent_files` TEXT,
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- client_contact_link
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `client_contact_link`;


CREATE TABLE `client_contact_link`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`contact_id` INTEGER  NOT NULL,
	`client_id` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	KEY `client_contact_link_FKIndex1`(`client_id`),
	KEY `client_contact_link_FKIndex2`(`contact_id`),
	CONSTRAINT `client_contact_link_FK_1`
		FOREIGN KEY (`client_id`)
		REFERENCES `client` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT,
	CONSTRAINT `client_contact_link_FK_2`
		FOREIGN KEY (`contact_id`)
		REFERENCES `contact` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- client_file
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `client_file`;


CREATE TABLE `client_file`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`client_id` INTEGER default 0 NOT NULL,
	`title_id` INTEGER default 0 NOT NULL,
	`revision` INTEGER default 0 NOT NULL,
	`local_file` VARCHAR(255) default '0' NOT NULL,
	`report_id` INTEGER default 0 NOT NULL,
	`creation_time` INTEGER default 0 NOT NULL,
	`article_count` INTEGER default 0 NOT NULL,
	PRIMARY KEY (`id`),
	KEY `inx_client_file_client_id`(`client_id`),
	KEY `inx_client_file_title_id`(`title_id`),
	KEY `inx_client_file_revision`(`revision`),
	KEY `inx_client_file_report_id`(`report_id`),
	KEY `inx_client_file_creation_time`(`creation_time`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- client_rule
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `client_rule`;


CREATE TABLE `client_rule`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`client_id` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	KEY `client_id`(`client_id`),
	CONSTRAINT `client_rule_FK_1`
		FOREIGN KEY (`client_id`)
		REFERENCES `client` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- client_rule_filter
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `client_rule_filter`;


CREATE TABLE `client_rule_filter`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`filter_type` CHAR,
	`filter_value` TEXT  NOT NULL,
	`client_rule_id` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	KEY `client_rule_id`(`client_rule_id`),
	CONSTRAINT `client_rule_filter_FK_1`
		FOREIGN KEY (`client_rule_id`)
		REFERENCES `client_rule` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- client_sent
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `client_sent`;


CREATE TABLE `client_sent`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`client_id` INTEGER  NOT NULL,
	`sent_files` TEXT,
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- connection_sent
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `connection_sent`;


CREATE TABLE `connection_sent`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`connection_id` INTEGER  NOT NULL,
	`sent_files` TEXT,
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- contact
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `contact`;


CREATE TABLE `contact`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`type` CHAR default 'technical' NOT NULL,
	`full_name` VARCHAR(255),
	`email` VARCHAR(255),
	`additional_email` VARCHAR(255),
	`title` VARCHAR(255),
	`department` INTEGER,
	`zipcode` VARCHAR(255),
	`pobox` INTEGER,
	`street_address` TEXT,
	`city_name` VARCHAR(255),
	`country_id` INTEGER default 0 NOT NULL,
	`mobile_number` VARCHAR(255),
	`work_number` VARCHAR(255),
	`home_number` VARCHAR(255),
	`notification_sms` TINYINT,
	`notification_email` TINYINT,
	PRIMARY KEY (`id`),
	KEY `contact_FKIndex1`(`country_id`),
	CONSTRAINT `contact_FK_1`
		FOREIGN KEY (`country_id`)
		REFERENCES `country` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- content_sources
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `content_sources`;


CREATE TABLE `content_sources`
(
	`country` VARCHAR(50),
	`city` VARCHAR(50),
	`type` VARCHAR(50),
	`title` VARCHAR(255),
	`url` VARCHAR(50),
	`language` VARCHAR(50),
	`frequancy` VARCHAR(50),
	`timeliness` VARCHAR(50),
	`publisher` VARCHAR(255),
	`copyright_notice` VARCHAR(255),
	`source_description` VARCHAR(255),
	`territory` VARCHAR(50),
	`mode` VARCHAR(50),
	`publisher_type` VARCHAR(50),
	`channels` VARCHAR(50),
	`Genre` VARCHAR(50),
	`biz_contact` VARCHAR(50),
	`biz_email` VARCHAR(50),
	`biz_phone` VARCHAR(50),
	`tech_contact` VARCHAR(50),
	`tech_email` VARCHAR(50),
	`ech_phone` VARCHAR(50),
	`is_org_from_syndigate` INTEGER default 0,
	`title_id` INTEGER default 0,
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- country
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `country`;


CREATE TABLE `country`
(
	`id` INTEGER  NOT NULL,
	`iso` VARCHAR(2)  NOT NULL,
	`name` VARCHAR(80)  NOT NULL,
	`printable_name` VARCHAR(80)  NOT NULL,
	`iso3` VARCHAR(3),
	`numcode` SMALLINT,
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- drupal_ids
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `drupal_ids`;


CREATE TABLE `drupal_ids`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`title_id` INTEGER default 0 NOT NULL,
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- gather_cronjob
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `gather_cronjob`;


CREATE TABLE `gather_cronjob`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`crontab_id` INTEGER default 0 NOT NULL,
	`start_timestamp` DATETIME  NOT NULL,
	`end_timestamp` DATETIME default '0000-00-00 00:00:00' NOT NULL,
	`script` TEXT  NOT NULL,
	`concurrent` TINYINT default 0 NOT NULL,
	`implementation_id` INTEGER  NOT NULL,
	`results` TEXT  NOT NULL,
	`pid` INTEGER default 0 NOT NULL,
	`is_exited_normally` TINYINT default 0 NOT NULL,
	PRIMARY KEY (`id`),
	KEY `startTimestamp`(`start_timestamp`),
	KEY `crontabId`(`crontab_id`),
	KEY `index_pid`(`pid`),
	KEY `index_end_timestamp`(`end_timestamp`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- gather_crontab
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `gather_crontab`;


CREATE TABLE `gather_crontab`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`title` INTEGER,
	`description` TEXT  NOT NULL,
	`script` TEXT  NOT NULL,
	`concurrent` TINYINT  NOT NULL,
	`implementation_id` INTEGER,
	`cron_definition` TEXT  NOT NULL,
	`last_actual_timestamp` INTEGER default 0 NOT NULL,
	`run_once` TINYINT default 0 NOT NULL,
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- generation_queue
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `generation_queue`;


CREATE TABLE `generation_queue`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`report_id` INTEGER default 0 NOT NULL,
	`timestamp` INTEGER default 0 NOT NULL,
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- image
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `image`;


CREATE TABLE `image`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`article_id` INTEGER  NOT NULL,
	`img_name` VARCHAR(255),
	`image_caption` TEXT  NOT NULL,
	`is_headline` TINYINT default 0 NOT NULL,
	`image_type` VARCHAR(255),
	`mime_type` VARCHAR(255),
	`original_name` VARCHAR(1000)  NOT NULL,
	`image_original_key` VARCHAR(1000)  NOT NULL,
	PRIMARY KEY (`id`),
	KEY `image_FKIndex11`(`article_id`),
	CONSTRAINT `image_FK_1`
		FOREIGN KEY (`article_id`)
		REFERENCES `article` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- image_path
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `image_path`;


CREATE TABLE `image_path`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`connnection_id` INTEGER  NOT NULL,
	`image_path` VARCHAR(255),
	`image_type` CHAR,
	`image_container_type` CHAR,
	`image_containers_file` CHAR,
	`image_head` VARCHAR(255),
	`image_extra` VARCHAR(255),
	`image_file_patterns` VARCHAR(255),
	PRIMARY KEY (`id`),
	KEY `image_path_FKIndex1`(`connnection_id`),
	CONSTRAINT `image_path_FK_1`
		FOREIGN KEY (`connnection_id`)
		REFERENCES `title_connection` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- iptc
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `iptc`;


CREATE TABLE `iptc`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`iptc_english` VARCHAR(255),
	`iptc_arabic` VARCHAR(255),
	`iptc_french` VARCHAR(255),
	`parent_id` INTEGER,
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- language
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `language`;


CREATE TABLE `language`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255),
	`code` VARCHAR(255),
	`dir` VARCHAR(3),
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- log_client_rule
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `log_client_rule`;


CREATE TABLE `log_client_rule`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`timestamp` INTEGER default 0 NOT NULL,
	`admin_id` INTEGER default 0 NOT NULL,
	`client_id` INTEGER default 0 NOT NULL,
	`previous_status` TEXT  NOT NULL,
	`operation` TEXT  NOT NULL,
	`current_status` TEXT  NOT NULL,
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- log_gather
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `log_gather`;


CREATE TABLE `log_gather`
(
	`id` BIGINT  NOT NULL AUTO_INCREMENT,
	`report_gather_id` INTEGER  NOT NULL,
	`category` VARCHAR(255)  NOT NULL,
	`message` VARCHAR(255)  NOT NULL,
	`severity` VARCHAR(255)  NOT NULL,
	`source` VARCHAR(255)  NOT NULL,
	`time` DATETIME  NOT NULL,
	`report_id` INTEGER default 0 NOT NULL,
	PRIMARY KEY (`id`),
	KEY `log_gather_FKIndex1`(`report_gather_id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- log_parse
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `log_parse`;


CREATE TABLE `log_parse`
(
	`id` BIGINT  NOT NULL AUTO_INCREMENT,
	`report_parse_id` INTEGER  NOT NULL,
	`category` VARCHAR(255)  NOT NULL,
	`message` VARCHAR(255)  NOT NULL,
	`severity` VARCHAR(255)  NOT NULL,
	`source` VARCHAR(255)  NOT NULL,
	`time` DATETIME  NOT NULL,
	PRIMARY KEY (`id`),
	KEY `log_parse_FKIndex1`(`report_parse_id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- max
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `max`;


CREATE TABLE `max`
(
	`title_id` INTEGER,
	`client_id` INTEGER,
	`article_id` INTEGER,
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- office
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `office`;


CREATE TABLE `office`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`phone_1` VARCHAR(255),
	`phone_2` VARCHAR(255),
	`fax` VARCHAR(255),
	`pobox` VARCHAR(255),
	`zipcode` INTEGER,
	`street_address` TEXT,
	`city_name` VARCHAR(255),
	`country_id` INTEGER,
	PRIMARY KEY (`id`),
	KEY `office_FKIndex1`(`country_id`),
	CONSTRAINT `office_FK_1`
		FOREIGN KEY (`country_id`)
		REFERENCES `country` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- original_article_category
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `original_article_category`;


CREATE TABLE `original_article_category`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`parent_id` INTEGER,
	`cat_name` VARCHAR(255),
	`title_id` INTEGER,
	`added_at` DATETIME,
	PRIMARY KEY (`id`),
	KEY `original_article_category_FKIndex1`(`title_id`),
	CONSTRAINT `original_article_category_FK_1`
		FOREIGN KEY (`title_id`)
		REFERENCES `title` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- parsing_state
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `parsing_state`;


CREATE TABLE `parsing_state`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`title_id` INTEGER,
	`revision_num` INTEGER,
	`completed` TINYINT default 0 NOT NULL,
	`locked` TINYINT default 0 NOT NULL,
	`created_at` DATETIME default '0000-00-00 00:00:00' NOT NULL,
	`parser_started_at` DATETIME default '0000-00-00 00:00:00' NOT NULL,
	`parser_finished_at` DATETIME default '0000-00-00 00:00:00' NOT NULL,
	`pid` INTEGER  NOT NULL,
	`report_id` INTEGER default 0 NOT NULL,
	`report_gather_id` INTEGER default 0 NOT NULL,
	`report_parse_id` INTEGER default 0 NOT NULL,
	PRIMARY KEY (`id`),
	KEY `parsing_state_FKIndex1`(`title_id`),
	KEY `index_pid`(`pid`),
	KEY `parsing_state_FKIndex2`(`locked`),
	KEY `parsing_state_FKIndex3`(`completed`),
	CONSTRAINT `parsing_state_FK_1`
		FOREIGN KEY (`title_id`)
		REFERENCES `title` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- parsing_state_backup
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `parsing_state_backup`;


CREATE TABLE `parsing_state_backup`
(
	`id` INTEGER default 0 NOT NULL,
	`title_id` INTEGER,
	`revision_num` INTEGER,
	`completed` TINYINT default 0 NOT NULL,
	`locked` TINYINT default 0 NOT NULL,
	`created_at` DATETIME default '0000-00-00 00:00:00' NOT NULL,
	`parser_started_at` DATETIME default '0000-00-00 00:00:00' NOT NULL,
	`parser_finished_at` DATETIME default '0000-00-00 00:00:00' NOT NULL,
	`pid` INTEGER  NOT NULL
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- public_users
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `public_users`;


CREATE TABLE `public_users`
(
	`user_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`user_name` VARCHAR(50)  NOT NULL,
	`password` VARCHAR(16)  NOT NULL,
	`creation_time_stamp` DATETIME  NOT NULL,
	`period` INTEGER  NOT NULL,
	`company_name` VARCHAR(50)  NOT NULL,
	`email` VARCHAR(50)  NOT NULL,
	`user_status` CHAR default 'NOTACTIVE',
	`first_login` DATETIME default '0000-00-00 00:00:00' NOT NULL,
	`expiry_date` DATETIME default '0000-00-00 00:00:00' NOT NULL,
	PRIMARY KEY (`user_id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- publisher
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `publisher`;


CREATE TABLE `publisher`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255),
	`website` VARCHAR(255),
	`notes` TEXT,
	`logo_path` VARCHAR(255),
	`ftp_username` VARCHAR(255),
	`ftp_password` VARCHAR(255),
	`unix_gid` INTEGER,
	`unix_uid` INTEGER,
	`nagios_file_path` VARCHAR(255),
	`country_id` INTEGER,
	`contract` CHAR default 'No' NOT NULL,
	`royalty_rate` FLOAT default 0 NOT NULL,
	`contract_start_date` DATE,
	`contract_end_date` DATE,
	`renewal_period` TEXT,
	`admins_id` INTEGER default 0 NOT NULL,
	`accountant_notes` TEXT  NOT NULL,
	PRIMARY KEY (`id`),
	KEY `country_id`(`country_id`),
	KEY `publisher_ibfk_3`(`admins_id`),
	CONSTRAINT `publisher_FK_1`
		FOREIGN KEY (`country_id`)
		REFERENCES `country` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT,
	CONSTRAINT `publisher_FK_2`
		FOREIGN KEY (`admins_id`)
		REFERENCES `admins` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- publisher_contact_link
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `publisher_contact_link`;


CREATE TABLE `publisher_contact_link`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`contact_id` INTEGER  NOT NULL,
	`publisher_id` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	KEY `publisher_contact_link_FKIndex1`(`publisher_id`),
	KEY `publisher_contact_link_FKIndex2`(`contact_id`),
	CONSTRAINT `publisher_contact_link_FK_1`
		FOREIGN KEY (`publisher_id`)
		REFERENCES `publisher` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT,
	CONSTRAINT `publisher_contact_link_FK_2`
		FOREIGN KEY (`contact_id`)
		REFERENCES `contact` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- publisher_office_link
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `publisher_office_link`;


CREATE TABLE `publisher_office_link`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`office_id` INTEGER  NOT NULL,
	`publisher_id` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	KEY `publisher_office_link_FKIndex1`(`publisher_id`),
	KEY `publisher_office_link_FKIndex2`(`office_id`),
	CONSTRAINT `publisher_office_link_FK_1`
		FOREIGN KEY (`publisher_id`)
		REFERENCES `publisher` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT,
	CONSTRAINT `publisher_office_link_FK_2`
		FOREIGN KEY (`office_id`)
		REFERENCES `office` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- report
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `report`;


CREATE TABLE `report`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`title_id` INTEGER default 0 NOT NULL,
	`revision_num` INTEGER default 0 NOT NULL,
	`report_gather_id` INTEGER default 0 NOT NULL,
	`report_parse_id` INTEGER default 0 NOT NULL,
	`report_generate_id` INTEGER default 0 NOT NULL,
	`report_send_id` INTEGER default 0 NOT NULL,
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- report_gather
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `report_gather`;


CREATE TABLE `report_gather`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`title_id` INTEGER  NOT NULL,
	`level` CHAR,
	`number_files` INTEGER,
	`file_size` INTEGER,
	`latest_status` TINYINT,
	`error` TEXT,
	`images_status` TINYINT,
	`number_images` INTEGER,
	`time` DATETIME  NOT NULL,
	PRIMARY KEY (`id`),
	KEY `report_FKIndex1`(`title_id`),
	CONSTRAINT `report_gather_FK_1`
		FOREIGN KEY (`title_id`)
		REFERENCES `title` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- report_generate
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `report_generate`;


CREATE TABLE `report_generate`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`title_id` INTEGER default 0 NOT NULL,
	`file` VARCHAR(100) default '' NOT NULL,
	`articles_count` INTEGER default 0 NOT NULL,
	`error` TEXT  NOT NULL,
	`time` DATETIME default '0000-00-00 00:00:00' NOT NULL,
	`status` TINYINT default 0 NOT NULL,
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- report_parse
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `report_parse`;


CREATE TABLE `report_parse`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`articles_ids` TEXT  NOT NULL,
	`title_id` INTEGER,
	`date` DATE,
	`parser_status` VARCHAR(255),
	`parser_error` TEXT,
	`article_num` INTEGER,
	PRIMARY KEY (`id`),
	KEY `parser_report_FKIndex1`(`title_id`),
	CONSTRAINT `report_parse_FK_1`
		FOREIGN KEY (`title_id`)
		REFERENCES `title` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- report_send
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `report_send`;


CREATE TABLE `report_send`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`sender_status` VARCHAR(255),
	`sender_msg` TEXT,
	`start_time` INTEGER  NOT NULL,
	`end_time` INTEGER  NOT NULL,
	`files_count` INTEGER  NOT NULL,
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- schedule
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `schedule`;


CREATE TABLE `schedule`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`title_id` INTEGER  NOT NULL,
	`minute` VARCHAR(10),
	`hour` VARCHAR(10),
	`day` VARCHAR(10),
	`month` VARCHAR(10),
	`weekday` VARCHAR(10),
	`script_path` VARCHAR(255),
	PRIMARY KEY (`id`),
	KEY `schedule_FKIndex1`(`title_id`),
	CONSTRAINT `schedule_FK_1`
		FOREIGN KEY (`title_id`)
		REFERENCES `title` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- statment
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `statment`;


CREATE TABLE `statment`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`path` VARCHAR(255) default '' NOT NULL,
	`status` CHAR default 'completed' NOT NULL,
	`notes` TEXT  NOT NULL,
	`update_at` DATETIME default '0000-00-00 00:00:00' NOT NULL,
	`file_name` VARCHAR(255) default '' NOT NULL,
	`publisher_id` INTEGER default 0 NOT NULL,
	`created` INTEGER default 0 NOT NULL,
	`accountant_id` INTEGER default 0 NOT NULL,
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- tenders_info_article
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `tenders_info_article`;


CREATE TABLE `tenders_info_article`
(
	`id` INTEGER default 0 NOT NULL,
	`iptc_id` INTEGER  NOT NULL,
	`title_id` INTEGER,
	`article_original_data_id` INTEGER  NOT NULL,
	`language_id` INTEGER  NOT NULL,
	`headline` VARCHAR(255),
	`summary` TEXT,
	`body` TEXT,
	`author` VARCHAR(255),
	`date` DATE,
	`parsed_at` DATETIME,
	`updated_at` DATETIME,
	`has_time` TINYINT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- tenders_info_duplications
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `tenders_info_duplications`;


CREATE TABLE `tenders_info_duplications`
(
	`file_name` VARCHAR(255) default '' NOT NULL,
	`post_id` INTEGER default 0 NOT NULL,
	`status` VARCHAR(255) default '' NOT NULL,
	`original_id` INTEGER default 0 NOT NULL,
	`parse_date` DATE default '0000-00-00' NOT NULL,
	`article_id` INTEGER default 0 NOT NULL,
	`article_date` DATE default '0000-00-00' NOT NULL
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- title
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `title`;


CREATE TABLE `title`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`title_frequency_id` INTEGER  NOT NULL,
	`title_type_id` INTEGER  NOT NULL,
	`publisher_id` INTEGER  NOT NULL,
	`name` VARCHAR(255),
	`website` VARCHAR(255),
	`language_id` INTEGER,
	`ftp_username` VARCHAR(255),
	`ftp_password` VARCHAR(255),
	`articles_threshold` INTEGER,
	`unix_uid` INTEGER,
	`unix_gid` INTEGER,
	`nagios_config_file` VARCHAR(255),
	`home_dir` VARCHAR(255),
	`work_dir` VARCHAR(255),
	`svn_repo` VARCHAR(255),
	`gather_retries` INTEGER  NOT NULL,
	`default_charset` VARCHAR(255),
	`parse_dir` VARCHAR(255),
	`next_try_timer` INTEGER  NOT NULL,
	`copyright` VARCHAR(255),
	`copyright_arabic` VARCHAR(255) default '' NOT NULL,
	`is_active` TINYINT default 0 NOT NULL,
	`activation_date` DATE,
	`logo_path` VARCHAR(255),
	`directory_layout` CHAR default 'issues' NOT NULL,
	`is_newswire` TINYINT default 0 NOT NULL,
	`is_generate` TINYINT,
	`country_id` INTEGER,
	`photo_feed` CHAR default 'No' NOT NULL,
	`contract` CHAR default 'No' NOT NULL,
	`has_technical_intro` TINYINT default 0 NOT NULL,
	`is_receiving` TINYINT default 0 NOT NULL,
	`is_parsing` TINYINT default 0 NOT NULL,
	`technical_notes` TEXT,
	`timeliness` VARCHAR(255),
	`contract_start_date` DATE,
	`contract_end_date` DATE,
	`royalty_rate` FLOAT default 0 NOT NULL,
	`source_description` TEXT,
	`renewal_period` TEXT,
	`city` VARCHAR(255) default '',
	`story_count` INTEGER default 0,
	`receiving_status` TINYINT default 0 NOT NULL,
	`receiving_status_comment` TEXT,
	`receiving_status_timestamp` DATETIME  NOT NULL,
	`notes` TEXT,
	`has_new_parsed_content` TINYINT default 0,
	`last_content_timestamp` DATETIME default '0000-00-00 00:00:00' NOT NULL,
	`cached_articles_today` INTEGER default 0 NOT NULL,
	`cached_current_span` INTEGER default 0 NOT NULL,
	`cached_prev_span` INTEGER default 0 NOT NULL,
	`cached_articles_count` INTEGER default 0 NOT NULL,
	`admins_id` INTEGER default 0 NOT NULL,
	`rank` TINYINT default 1 NOT NULL,
	PRIMARY KEY (`id`),
	KEY `title_FKIndex1`(`publisher_id`),
	KEY `title_FKIndex2`(`title_type_id`),
	KEY `title_FKIndex3`(`title_frequency_id`),
	KEY `title_ibfk_4`(`country_id`),
	KEY `language_id`(`language_id`),
	KEY `language_id_2`(`language_id`),
	KEY `title_ibfk_6`(`admins_id`),
	CONSTRAINT `title_FK_1`
		FOREIGN KEY (`publisher_id`)
		REFERENCES `publisher` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT,
	CONSTRAINT `title_FK_2`
		FOREIGN KEY (`title_type_id`)
		REFERENCES `title_type` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT,
	CONSTRAINT `title_FK_3`
		FOREIGN KEY (`title_frequency_id`)
		REFERENCES `title_frequency` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT,
	CONSTRAINT `title_FK_4`
		FOREIGN KEY (`country_id`)
		REFERENCES `country` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT,
	CONSTRAINT `title_FK_5`
		FOREIGN KEY (`language_id`)
		REFERENCES `language` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT,
	CONSTRAINT `title_FK_6`
		FOREIGN KEY (`admins_id`)
		REFERENCES `admins` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- title_connection
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `title_connection`;


CREATE TABLE `title_connection`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`title_id` INTEGER  NOT NULL,
	`connection_type` CHAR,
	`url` VARCHAR(255),
	`file_pattern` VARCHAR(255),
	`authintication` TINYINT,
	`username` VARCHAR(255),
	`password` VARCHAR(255),
	`extras` TEXT,
	`has_images` TINYINT,
	`ftp_port` INTEGER default 21,
	PRIMARY KEY (`id`),
	KEY `title_connection_FKIndex1`(`title_id`),
	CONSTRAINT `title_connection_FK_1`
		FOREIGN KEY (`title_id`)
		REFERENCES `title` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- title_contact_link
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `title_contact_link`;


CREATE TABLE `title_contact_link`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`contact_id` INTEGER  NOT NULL,
	`title_id` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	KEY `title_contact_link_FKIndex1`(`title_id`),
	KEY `title_contact_link_FKIndex2`(`contact_id`),
	CONSTRAINT `title_contact_link_FK_1`
		FOREIGN KEY (`title_id`)
		REFERENCES `title` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT,
	CONSTRAINT `title_contact_link_FK_2`
		FOREIGN KEY (`contact_id`)
		REFERENCES `contact` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- title_frequency
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `title_frequency`;


CREATE TABLE `title_frequency`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`frequency_type` VARCHAR(255),
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- title_office_link
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `title_office_link`;


CREATE TABLE `title_office_link`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`office_id` INTEGER  NOT NULL,
	`title_id` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	KEY `title_office_link_FKIndex1`(`title_id`),
	KEY `title_office_link_FKIndex2`(`office_id`),
	CONSTRAINT `title_office_link_FK_1`
		FOREIGN KEY (`title_id`)
		REFERENCES `title` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT,
	CONSTRAINT `title_office_link_FK_2`
		FOREIGN KEY (`office_id`)
		REFERENCES `office` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- title_type
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `title_type`;


CREATE TABLE `title_type`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`type` VARCHAR(255),
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- titles_clients_rules
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `titles_clients_rules`;


CREATE TABLE `titles_clients_rules`
(
	`title_id` INTEGER default 0 NOT NULL,
	`client_id` INTEGER default 0 NOT NULL,
	`last_updated` DATETIME
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- video
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `video`;


CREATE TABLE `video`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`article_id` INTEGER  NOT NULL,
	`video_name` VARCHAR(255)  NOT NULL,
	`video_caption` VARCHAR(255)  NOT NULL,
	`original_name` VARCHAR(1000)  NOT NULL,
	`video_type` VARCHAR(255),
	`bit_rate` VARCHAR(255),
	`added_time` DATETIME  NOT NULL,
	`mime_type` VARCHAR(255)  NOT NULL,
	PRIMARY KEY (`id`),
	KEY `video_FKIndex11`(`article_id`),
	CONSTRAINT `video_FK_1`
		FOREIGN KEY (`article_id`)
		REFERENCES `article` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- orders_frequency
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `orders_frequency`;


CREATE TABLE `orders_frequency`
(
	`o_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`title_id` INTEGER  NOT NULL,
	`frequency` INTEGER  NOT NULL,
	`start` DATETIME,
	`end` DATETIME,
	PRIMARY KEY (`o_id`),
	INDEX `orders_frequency_FI_1` (`title_id`),
	CONSTRAINT `orders_frequency_FK_1`
		FOREIGN KEY (`title_id`)
		REFERENCES `title` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- embin_custom
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `embin_custom`;


CREATE TABLE `embin_custom`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`article_id` INTEGER  NOT NULL,
	`publisher` VARCHAR(255),
	`date` DATE,
	PRIMARY KEY (`id`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- article_archive
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `article_archive`;


CREATE TABLE `article_archive`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`iptc_id` INTEGER  NOT NULL,
	`title_id` INTEGER,
	`article_original_data_id` INTEGER  NOT NULL,
	`language_id` INTEGER  NOT NULL,
	`headline` VARCHAR(255),
	`summary` TEXT,
	`body` TEXT,
	`author` VARCHAR(255),
	`date` DATE,
	`parsed_at` DATETIME,
	`updated_at` DATETIME,
	`has_time` TINYINT,
	`is_Calias_called` INTEGER default 0 NOT NULL,
	`sub_feed` VARCHAR(255) default '' NOT NULL,
	PRIMARY KEY (`id`),
	KEY `article_FKIndex1`(`language_id`),
	KEY `article_FKIndex2`(`article_original_data_id`),
	KEY `article_FKIndex3`(`iptc_id`),
	KEY `article_FKIndex4`(`title_id`),
	KEY `parsed_at`(`parsed_at`),
	KEY `date`(`date`),
	KEY `index_calias`(`is_Calias_called`),
	CONSTRAINT `article_archive_FK_1`
		FOREIGN KEY (`language_id`)
		REFERENCES `language` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT,
	CONSTRAINT `article_archive_FK_2`
		FOREIGN KEY (`article_original_data_id`)
		REFERENCES `article_original_data_archive` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT,
	CONSTRAINT `article_archive_FK_3`
		FOREIGN KEY (`iptc_id`)
		REFERENCES `iptc` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT,
	CONSTRAINT `article_archive_FK_4`
		FOREIGN KEY (`title_id`)
		REFERENCES `title` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- article_original_data_archive
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `article_original_data_archive`;


CREATE TABLE `article_original_data_archive`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`original_article_id` VARCHAR(255),
	`original_source` VARCHAR(255),
	`issue_number` INTEGER,
	`page_number` INTEGER,
	`reference` VARCHAR(255),
	`extras` VARCHAR(255),
	`hijri_date` VARCHAR(255),
	`original_cat_id` INTEGER,
	`revision_num` INTEGER,
	PRIMARY KEY (`id`),
	KEY `id`(`id`),
	KEY `the_original_article_id`(`original_article_id`),
	KEY `original_cat_id_index`(`original_cat_id`),
	KEY `indx_aod_rev`(`revision_num`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- image_archive
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `image_archive`;


CREATE TABLE `image_archive`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`article_id` INTEGER  NOT NULL,
	`img_name` VARCHAR(255),
	`image_caption` TEXT  NOT NULL,
	`is_headline` TINYINT default 0 NOT NULL,
	`image_type` VARCHAR(255),
	`mime_type` VARCHAR(255),
	`original_name` VARCHAR(1000)  NOT NULL,
	`image_original_key` VARCHAR(1000)  NOT NULL,
	PRIMARY KEY (`id`),
	KEY `image_FKIndex1`(`article_id`),
	CONSTRAINT `image_archive_FK_1`
		FOREIGN KEY (`article_id`)
		REFERENCES `article_archive` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- video_archive
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `video_archive`;


CREATE TABLE `video_archive`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`article_id` INTEGER  NOT NULL,
	`video_name` VARCHAR(255)  NOT NULL,
	`video_caption` VARCHAR(255)  NOT NULL,
	`original_name` VARCHAR(1000)  NOT NULL,
	`video_type` VARCHAR(255),
	`bit_rate` VARCHAR(255),
	`added_time` DATETIME  NOT NULL,
	`mime_type` VARCHAR(255)  NOT NULL,
	PRIMARY KEY (`id`),
	KEY `video_FKIndex1`(`article_id`),
	CONSTRAINT `video_archive_FK_1`
		FOREIGN KEY (`article_id`)
		REFERENCES `article_archive` (`id`)
		ON UPDATE RESTRICT
		ON DELETE RESTRICT
)Type=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
