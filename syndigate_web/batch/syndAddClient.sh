#!/bin/bash -x

#arg 1 : Client ID
#arg 2 : Safe Username (System username)
#arg 3 : Password
#arg 4 : Home path

CLIENTID="$1"
PASSWORD="$3"
HOMEPATH="$4"

USERMOD="/usr/sbin/usermod"
USERADD="/usr/sbin/useradd"
LN="/bin/ln"
CHOWN="/bin/chown"
MKDIR="/bin/mkdir"
ID="/usr/bin/id"

#arg 2 is the Safe Username (System Username)
if [[ "$2" =~ ^[a-z][a-z0-9_]*[a-z0-9]$ ]]; then
	USERNAME="$2"
else
	echo -n "Illegal username $2"
	exit 1
fi

#now the steps to create a client
# does this user already exist?
$ID $USERNAME
EXITSTATUS="$?"
if [[ $EXITSTATUS != 0 ]]; then
	#User does not exist
	ENCPASS=`mkpasswd $PASSWORD`
	$USERADD --password $ENCPASS $USERNAME
	# Validates that the Exist Status = 0 (Success)
	EXITSTATUS="$?"
	if [[ $EXITSTATUS != 0 ]]; then
		exit 1
	fi
fi

$USERMOD -d $HOMEPATH/$CLIENTID $USERNAME
# Validates that the Exist Status = 0 (Success)
EXITSTATUS="$?"
if [[ $EXITSTATUS != 0 ]]; then
	exit 1
fi

$MKDIR -p $HOMEPATH/$CLIENTID
if [ ! -h $HOMEPATH/$USERNAME ] ; then
	$LN -s $HOMEPATH/$CLIENTID $HOMEPATH/$USERNAME
fi	
$CHOWN -R $USERNAME: $HOMEPATH/$CLIENTID $HOMEPATH/$USERNAME
