#!/bin/bash -x

#arg 1 : [PUBLISHE|TITLE]
#arg 2 : Safe Username (System username)
#arg 3 : Password for either Publisher or Title
#arg 4 : Title ID
#arg 5 : Home Path for either Publisher or Title
#arg 6 : SVN Working Copy for Title
#arg 7 : Publisher Safe Name if Applicable (System Username)
#arg 8 : SVN Repository Path for Title
#arg 9 : Publisher ID
#arg 10: Parser Working Copy Path for Title (This is another checkout of the same Repository)

PASSWORD="$3"
HOMEPATH="$5"
WCPATH="$6"
REPOPATH="$8"
PUBLISHERID="$9"
PARSERWC="${10}"

USERMOD="/usr/sbin/usermod"
USERADD="/usr/sbin/useradd"
LN="/bin/ln"
CHOWN="/bin/chown"
SVNADMIN="/usr/bin/svnadmin"
SVNLOOK="/usr/bin/svnlook"
MKDIR="/bin/mkdir"
SVN="/usr/bin/svn"
CP="/bin/cp"
ID="/usr/bin/id"


#arg 1 determines whether we have a publisher or a title account
if [[ "$1" == "PUBLISHER" ]]; then
	TYPE="PUBLISHER"
elif [[ "$1" == "TITLE" ]]; then
	TYPE="TITLE"
else
	echo -n "Illegal account type $1"
	ext 1
fi

#arg 2 is the Safe Username (System Username)
if [[ "$2" =~ ^[a-z][a-z0-9_]*[a-z0-9]$ ]]; then
	USERNAME="$2"
else
	echo -n "Illegal username $2"
	exit 1
fi


# arg 4 is the uid
if [[ "$4" =~ ^[0-9]*$ ]]; then
	SYNDUID="$4"
else
	echo -n "Illegal syndigate UID $4"
	exit 1
fi

#if we are a title then the fifth argument is the publisher safename
if [[ "$TYPE" == "TITLE" ]]; then
	if [[ "$7" =~ ^[a-z][a-z0-9_]*[a-z0-9]$ ]]; then
		PUBLISHERNAME="$7"
	else
		echo -n "Illegal or missing publisher safename $7"
	fi
fi

#now the steps to create a publisher
if [[ "$TYPE" == "PUBLISHER" ]]; then
	# does this user already exist?
	$ID $USERNAME
	EXITSTATUS="$?"
	if [[ $EXITSTATUS != 0 ]]; then
       		#User does not exist
       		ENCPASS=`mkpasswd $PASSWORD`
       		$USERADD --password $ENCPASS $USERNAME
       		# Validates that the Exist Status = 0 (Success)
       		EXITSTATUS="$?"
       		if [[ $EXITSTATUS != 0 ]]; then
       			exit 1
       		fi
	fi

	$USERMOD -d $HOMEPATH/$SYNDUID $USERNAME
	# Validates that the Exist Status = 0 (Success)
	EXITSTATUS="$?"
	if [[ $EXITSTATUS != 0 ]]; then
		exit 1
	fi

	$MKDIR -p $HOMEPATH/$SYNDUID
	if [ ! -h $HOMEPATH/$USERNAME ] ; then
		$LN -s $HOMEPATH/$SYNDUID $HOMEPATH/$USERNAME
	fi	
	$CHOWN -R $USERNAME: $HOMEPATH/$SYNDUID $HOMEPATH/$USERNAME

	$MKDIR -p $WCPATH/$SYNDUID
	if [ ! -h $WCPATH/$USERNAME ] ; then
		$LN -s $WCPATH/$SYNDUID $WCPATH/$USERNAME
	fi	
	$CHOWN -R $USERNAME: $WCPATH/$SYNDUID $HOMEPATH/$USERNAME

fi


#now the steps to create a title
if [[ "$TYPE" == "TITLE" ]]; then

	PUBLISHERUID=`id -u $PUBLISHERNAME`
	PUBLISHERGID=`id -g $PUBLISHERNAME`
	PUBLISHERHOME="$5"
	PUBLISHERWC="$6"

	# does this user already exist?
	$ID $USERNAME
	EXITSTATUS="$?"
	if [[ $EXITSTATUS != 0 ]]; then
	        #User does not exist
		ENCPASS=`mkpasswd $PASSWORD`
		$USERADD --password $ENCPASS --gid $PUBLISHERGID $USERNAME
       		# Validates that the Exist Status = 0 (Success)
       		EXITSTATUS="$?"
       		if [[ $EXITSTATUS != 0 ]]; then
       		        exit 1
       		fi
	fi

	$USERMOD -d $PUBLISHERHOME/$SYNDUID $USERNAME
	# Validates that the Exist Status = 0 (Success)
	EXITSTATUS="$?"
	if [[ $EXITSTATUS != 0 ]]; then
		exit 1
	fi
	
	$MKDIR -p $PUBLISHERHOME/$SYNDUID
	if [ ! -h $PUBLISHERHOME/$USERNAME ] ; then
		$LN -s $PUBLISHERHOME/$SYNDUID $PUBLISHERHOME/$USERNAME
	fi	
	$CHOWN -R $USERNAME: $PUBLISHERHOME/$SYNDUID $PUBLISHERHOME/$USERNAME

	$MKDIR -p ${REPOPATH}
	$SVNLOOK info ${REPOPATH} 
        EXITSTATUS="$?"
	# if the repo does not exist (error) then create it
        if [[ $EXITSTATUS != 0 ]]; then
		$SVNADMIN create ${REPOPATH}
        fi

    if [ ! -h ${REPOPATH}/hooks/post-commit ] ; then    
		$LN -s /usr/local/syndigate/svnhooks/post-commit ${REPOPATH}/hooks/post-commit
	fi	
	$CHOWN -R svnserve: ${REPOPATH}
	$CP -f /usr/local/syndigate/syndigate_web/batch/svnserve.conf ${REPOPATH}/conf	
	
        $MKDIR -p $PUBLISHERWC
	cd $PUBLISHERWC/
        $SVN co svn://localhost/$PUBLISHERID/$SYNDUID
        if [ ! -h $PUBLISHERWC/$USERNAME ] ; then
        	$LN -s $PUBLISHERWC/$SYNDUID $PUBLISHERWC/$USERNAME
        fi	
        $CHOWN -R $USERNAME: $PUBLISHERWC/$SYNDUID $PUBLISHERWC/$USERNAME

	$MKDIR -p $PARSERWC
        #$LN -s $PARSERWC /syndigate/sources/pwc/$PUBLISHERNAME
        #$CHOWN -R $USERNAME: $PARSERWC /syndigate/sources/pwc/$PUBLISHERNAME
	cd $PARSERWC
	$SVN co svn://localhost/$PUBLISHERID/$SYNDUID
	if [ ! -h $PARSERWC/$USERNAME ] ; then
		$LN -s $PARSERWC/$SYNDUID $PARSERWC/$USERNAME
	fi	
	$CHOWN -R $USERNAME: $PARSERWC/$SYNDUID $PARSERWC/$USERNAME
fi

