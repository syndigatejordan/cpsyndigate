<?php 
/**
 * Generate sources (Publishers and titles) from an existing database
 * It will get the account info from DB then
 *   1 - create the publishers (FTP, username, etc ...)
 *   2 - create the titles (FTP, svn repo, wc , pwc, etc )
 * 
 * Generally this script is used when moving from a server to another one when you have the database
 *   and want to create new accounts on it
 * 
 * 
 * if any part of the account exist it will not modify it, so it is safe to use it, (i.e it will not override the svn repo, wc, pwc, etc )
 *    however it will create it if ot exist
 *  
 */

require_once '/usr/local/syndigate/scripts/propel_include_config.php';
require_once '/usr/local/syndigate/syndigate_web/config/config.php';


$publishers = PublisherPeer::doSelect(new Criteria());

$c = new Criteria();
//$c->add(PublisherPeer::ID, 304, Criteria::GREATER_EQUAL);
$c->add(PublisherPeer::ID, 214);
$publishers = PublisherPeer::doselect($c);
//print_r($publishers); die();

foreach ($publishers as $publisher) {
	
	fixPublisher($publisher);
	
	$titles = $publisher->getTitles();
	foreach ($titles as $title) {
		fixTitle($title);
		
	}
	
	
}




function fixPublisher($publisher) {
	$username 	= $publisher->getFtpUsername();
	$pass		= $publisher->getFtpPassword();
	$synId		= $publisher->getId();
	$homePath	= HOMEPATH;
	$wcPath		= WCPATH;
				
	$exe    = LINUX_USER_MOD . " " . LINUX_SCRIPTS_PATH . "syndAddSource.sh PUBLISHER $username $pass $synId $homePath $wcPath";
	$res = shell_exec($exe);
	echo $res . PHP_EOL . str_repeat("-", 150) . PHP_EOL. PHP_EOL; 
}

function fixTitle($title) {
	$username 	 = $title->getFtpUsername();
	$pass		 = $title->getFtpPassword();
	$synId		 = $title->getId();
	$homePath	 = HOMEPATH .'/'. $title->getPublisherId();
	$wcPath		 = WCPATH   .'/'. $title->getPublisherId();
	$pwcPath 	 = PWCPATH  .'/'. $title->getPublisherId();
	$pubUsername = $title->getPublisher()->getFtpUsername();
	$svnRepo	 = SVN_REPO_PATH .$title->getPublisherId() . '/' . $title->getId();
			
	$exe    = LINUX_USER_MOD . " " . LINUX_SCRIPTS_PATH . "syndAddSource.sh TITLE $username $pass $synId $homePath $wcPath $pubUsername $svnRepo " . $title->getPublisherId() . ' ' . $pwcPath;
  	$res = shell_exec($exe);
  	echo $res . PHP_EOL . str_repeat("-", 150) . PHP_EOL. PHP_EOL; 
}
