<?php
define('SF_ROOT_DIR',    realpath(dirname(__FILE__).'/..'));
define('SF_APP',         'technical');
define('SF_ENVIRONMENT', 'dev');
define('SF_DEBUG',       false);
require_once(SF_ROOT_DIR.DIRECTORY_SEPARATOR.'apps'.DIRECTORY_SEPARATOR.SF_APP.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.php');

$map = array('suggestedPubName'=>'getFtpSuggestedPublisherName',
			 'suggestedTitleName'=>'getFtpSuggestedTitleName',
			 'passwordErr'=>'getPasswordErr',
			 'usernameErr'=>'getUsernameErr'
		);

if(array_key_exists($_REQUEST['method'], $map)) {
	$map[$_REQUEST['method']]();
} else {
	echo 'Invalid method';
}


function getFtpSuggestedPublisherName()
{
	require_once LINUX_SCRIPTS_PATH . 'standard.php';
	$standard = new Standard();
	echo $standard->getFtpSuggestedPublisherName($_REQUEST['text']);
}

function getFtpSuggestedTitleName()
{
	require_once LINUX_SCRIPTS_PATH . 'standard.php';
	$standard = new Standard();
	echo $standard->getFtpSuggestedTitleName($_REQUEST['text']);
}

function getPasswordErr()
{
	require_once LINUX_SCRIPTS_PATH . 'standard.php';
	$standard = new Standard();
	$reuslt   = $standard->getPasswordErrorV1($_REQUEST['text']);
	echo !$result ? 'Acceptable Password' : $result;
}

/*
function getUsernameErr()
{
	require_once LINUX_SCRIPTS_PATH . 'standard.php';
	$standard = new Standard();
	$result   = $standard->getPasswordErrorV1($_REQUEST['text']);
	echo !$result ? 'Acceptable username' : $result;
	 
}
*/
?>