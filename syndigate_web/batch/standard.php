<?php 
require_once LINUX_SCRIPTS_PATH . 'database_helper.php';

class Standard 
{
	protected $databaseHelper;
	
	public function __construct()
	{
		$this->databaseHelper = new dataBaseHelper();
	}
	
	
	public function getPasswordErrorV1($password, $passwordLength=6, $displayFieldName ='Password')
	{
		if(!$password || $password =='') {
			return "$displayFieldName not provided";
		} else {
			if((strlen($password)<$passwordLength)) {
				return "$displayFieldName must be at least $passwordLength characters";
			} else {
				if(!preg_match_all('/^[^ \t\n\x0B\r]*$/',$password, $out)) {
					return "$displayFieldName must not contain special characters";
				} else {
					return false;
				}
			}
		}
	}
	
	
	public function getPasswordErrorV2($password, $confirmPassword, $passwordLength=12)
	{
		$msg = $this->getPasswordErrorV1($password, $passwordLength);
		
		if(!$msg) { 
			if($password != $confirmPassword) {
				return 'Passwords dose not matches.';
			} else {
				return false;
			}
		} else{
			return $msg;
		}
		
	}	
	
	
	/**
	 * returning false means that there is not error 
	 */
	public function getNameErrMsg($name, $minLength=4, $maxLength=255) 
	{
		if(!$name) {
			return 'Name Not provided';
		} else {
			if( strlen($name)< $minLength ) {
				return "Name Must be least $minLength characters";
			} else {
				if(strlen($name)> $maxLength) {
					return "Name must not be more that $maxLength Characters";
				} else {
					return false;
				}
			} 
		}
	}
	
	
	
	public function getRandomPassword($length = 8)
	{
		$all  = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h','i','j','k','l','m','n','o','p','q','r','s','t', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z','0','1', '2', '3','4','5','6','7','8','9');
		shuffle($all);
		
		$pass='';
		for($i=0; $i < $length; $i++) {
			$pass.= $all[$i];
		}
		return $pass;
	}
	
	
	public function getFtpSuggestedPublisherName($publisherName)
	{
		$publisherName = trim($publisherName);	
		$nextId  = $this->databaseHelper->getTableNextInsertId('publisher');
		$ftpName = strtolower(substr($publisherName, 0, 6) . $nextId);
				
		if(Publisher::hasRealLinuxAccount($ftpName)) {
			return $this->getFtpSuggestedPublisherName($ftpName . rand(1,9));
		} else {
			return $ftpName;
		}
		
	}
	
		
		
	
	public function getFtpSuggestedTitleName($titleName) {
		return 'TEST standard Line 102';
		/*
		$titleName = trim($titleName);
		$nextId    = $this->databaseHelper->getTableNextInsertId('title');
		$ftpName   = strtolower(substr($titleName, 0, 6) . $nextId);
		
		$c = new Criteria();
		$c->add(TitlePeer::FTP_USERNAME , $ftpName, Criteria::like);
		$count = TitlePeer::doCount($c);
		
		if(Publisher::hasRealLinuxAccount($ftpName)) {
			return $this->getFtpSuggestedPublisherName($ftpName . rand(1,9) . $count+1);
		} else {
			return $ftpName;
		}
		*/
	
	}
	
	
	
	function updateTitleCrontab($titleId, $titleName, $concurrent, $cronDefinition, $script)
	{
		$title 		= TitlePeer::retrieveByPK($titleId);
		$crontabId  = $title->getCrontabId(0);
		
		$tab = new Crontab($crontabId);
		$tab->setTitle($titleId);
		$tab->setConcurrent($concurrent);
		$tab->setDescription($titleName);
		$tab->setCronDefinition($cronDefinition);
		$tab->setScript($script);
		$tab->setImplementationId($titleId);
		$tab->update(); 
		
		$title->setTimerAndRetriesFromCronDef($cronDefinition);
		$title->save();
	}
	
	
		
	function getCrontab($id)
	{
		global $db;
		$db = newDb();
		
		$tab = new Crontab($id);
		return $tab->getCronDefinition();
	}
	
	
	public function getUrlErrMsg($url)
	{
		if(!$url || $url=='') {
			return 'URL not provided.';
		} else {
			return false;
		}
	}
	
	
	public function isFtpAccount($ftpUsername)
	{
		$userArray = posix_getpwnam($ftpUsername);
		
		if(!is_array($userArray)) {
			return false;
		} else {
			return true;
		}
	}
	
	
	
	public function getFtpErrMsg($name, $minLength=4, $maxLength=8, $fieldDisplayNanme = '', $allowUnderScore=false)
	{		
		$fieldDisplayNanme = !$fieldDisplayNanme ? 'Username' : $fieldDisplayNanme;
		
		if(!$allowUnderScore && (strpos($name, '_')>0)) {
			return "$fieldDisplayNanme must not contain spiceal characters";
		}
		 
		if(!$name || trim($name) =='') {
			return "$fieldDisplayNanme Not provided";
		} else {
			if( strlen($name)< $minLength ) {
				return "$fieldDisplayNanme Must be least $minLength characters";
			} else {
				if(strlen($name)> $maxLength) {
					return "$fieldDisplayNanme must not be more that $maxLength Characters";
				} else {
					
					if(preg_match('/^[a-z\d_]*$/', $name)) {
						return false;
					} else {
						//return "Error $fieldDisplayNanme, No upper cases allowed or spiceal characters";
						return false;
					}
					
					return false;
				}
			} 
		}
	}
	
	
	
	public function getPublisherFtpErrMsg($name, $minLength=4, $maxLength=8, $fieldDisplayNanme = '')
	{
		return $this->getFtpErrMsg($name, $minLength, $maxLength, $fieldDisplayNanme, false);
	}
	
	
	public function getClientFtpErrMsg($name, $minLength=2, $maxLength=8, $fieldDisplayNanme = '')
	{
		$name = strtolower($name); // because default dosent allow capital case we will consider it in lower case but it is actually check and will not make the reall name with small letters
		return $this->getFtpErrMsg($name, $minLength, $maxLength, $fieldDisplayNanme, true);
	}
	
	/**
	 * Note that the a second part of $publisherUsername . '_' max of 9 characters will be 
	 * added before the titlename [ But the validation is Not here ]
	 * 
	 * this is just validate the title ftpusername [maximum 8 characters], the publisher part
	 * including the underscore is already validated when the publisher created
	 *
	 * @param String $titleName The title name part of the title ftpusername [without the publisher]
	 * @return String of error, or empty string if no error 
	 */
	public function getTitleFtpErrMsg($titleName) {
		return $this->getFtpErrMsg($titleName, 4, 8, 'Title part of title ftp username', false);
	}
	
	
	
	/*
	public function getTitleFtpErrMsg($titleName) 
	{
		
		$minLength=4; $maxLength=8;
		//$publisherName = $publisherName  . '_';
		$publisherName = '';

		if(!$titleName || trim($titleName) =='') {
			return 'Username Not provided';
		} else {
			if( strlen($titleName)< $minLength ) {
				return "Username Must be least $minLength characters";
			} else {
				if(strlen($titleName)> $maxLength) {
					return "Username must not be more that $maxLength Characters";
				} else {
					if(preg_match('/^'.$publisherName.'[a-z\d_]*$/', $titleName)) {
						return ''; // name acceptable
					} else {
						return 'Error username, No upper cases allowed or spiceal characters';
					}
					return false;
				}
			} 
		}
	}
	*/
	
	
	public function getCronDefinitionErrMsg($cronDefinition) {
		return ''==trim($cronDefinition) ? 'You must set the cron definition' : '';
	}
	
	
	
	public function getSystemPathFromAbsolute($absolutePath) 
	{
		$tmp = str_replace(SF_ROOT_DIR, '', $absolutePath);
		$tmp = str_replace("/web", "", $tmp);
		return $tmp;
	}
	
	public function getAbsolutePathFromSystem($systemPath)
	{
		return  SF_ROOT_DIR . '/web' . $systemPath;
	}
	
	
	

}
?>
