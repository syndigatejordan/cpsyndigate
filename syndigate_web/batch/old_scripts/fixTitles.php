<?php
if(!defined('SF_ROOT_DIR')) 
	define('SF_ROOT_DIR',    realpath(dirname(__file__).'/..'));
if(!defined('SF_APP'))
	define('SF_APP',         (isset($argv['app'])?$argv['app']:'backend'));
if(!defined('SF_ENVIRONMENT'))	
	define('SF_ENVIRONMENT', (isset($argv['env'])?$argv['env']:'dev'));
if(!defined('SF_DEBUG'))
	define('SF_DEBUG',       (isset($argv['debug'])?1:1));
 
require_once(SF_ROOT_DIR.DIRECTORY_SEPARATOR.'apps'.DIRECTORY_SEPARATOR.SF_APP.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.php');

$databaseManager = new sfDatabaseManager();
$databaseManager->initialize();
  
$a = array();
//===============================================================================================\\

require_once LINUX_SCRIPTS_PATH . 'standard.php';
$standard = new Standard();

$titles = TitlePeer::doSelect(new Criteria());

foreach ($titles as $title) {
			
	$id  		  	= $title->getId();
	$name 		  	= $title->getName();
	$publisherId  	= $title->getPublisherId();
	$publisherFtp 	= $title->getPublisher()->getFtpusername();
	$username	  	= $standard->getFtpSuggestedTitleName($publisherFtp, $name);
	$oldUsername	= $title->getFtpUsername();
	$pass		  	= $title->getFtpPassword();
	$synId			= $id;
	$homePath	 	= HOMEPATH .'/'. $publisherId;
	$wcPath		 	= WCPATH   .'/'. $publisherId;
	$pwcPath 	 	= PWCPATH  .'/'. $publisherId;
	$pubUsername 	= $title->getPublisher()->getFtpUsername();
	$svnRepo	 	= SVN_REPO_PATH . $publisherId . '/' . $title->getId();
		
	$prefix = $publisherFtp . '_';

	
	$a[] = $username;
	
	if($prefix==substr($oldUsername,0,strlen($prefix))) {
		
		echo "<font color='blue'>the ftp account : $oldUsername is in standard , th3 name correction must be ignored but check if the name is ftp account.</font><br>";
		
		if($standard->isFtpAccount($oldUsername)) {
			echo "<font color='Green'>$oldUsername is ftp account.</font><br>";
		} else {
			echo "<font color='red'>$oldUsername is <b>NOT</b> ftp account.</font><br>";
		}
		
	} else {
		echo "<font color='red'>current username $oldUsername must begin with $prefix because the Publisher username is $publisherFtp </font><br>";
		echo "sugessted username : $username <br>";
		echo $standard->isFtpAccount($username) ? "<font color='red'>The username $username is taken ... check th3 standard suggested username function for increment</font><br>": "Th3 suggested username is not taken yet <br>";
	}
	
	
	if(!($standard->isFtpAccount($username)) && !($prefix==substr($oldUsername,0,strlen($prefix)))) {
		
		echo '<b>Entering the creation of new username area ------------------</b><br>';
		
		$exe = "/usr/bin/sudo /bin/rm  -r " . NAGIOSPATH  	."$publisherId/*";   	exec($exe);   echo $exe . '<br>';
		$exe = "/usr/bin/sudo /bin/rm  -r " . 	  		"$homePath/$oldUsername";  	exec($exe);   echo $exe . '<br>';
		$exe = "/usr/bin/sudo /bin/rm  -r " .   	   	"$wcPath/$oldUsername";  	exec($exe);   echo $exe . '<br>';
		$exe = "/usr/bin/sudo /bin/rm  -r " .     		"$pwcPath/$oldUsername";  	exec($exe);   echo $exe . '<br>';
				
		$exe    = LINUX_USER_MOD . " " . LINUX_SCRIPTS_PATH . "syndAddSource.sh TITLE $username $pass $synId $homePath $wcPath $pubUsername $svnRepo " . $publisherId . ' ' . $pwcPath;
		//echo $exe . '<br><br>';
		exec($exe);
		
		$nagiosPath   = NAGIOSPATH . $publisherId;
		require LINUX_SCRIPTS_PATH  . 'syndGenerateNagiosConf.php';
				
		if($standard->isFtpAccount($username)) {
			echo "<font color='green'>Account created $username </font><br>";
			$title->setFtpUsername($username);
			$title->save();
			$exe = "/usr/bin/sudo /usr/sbin/userdel " . $oldUsername;
			exec($exe);
			
			if($standard->isFtpAccount($oldUsername)) {
				echo "<font color='red'>old account ftp account $oldUsername could not be deleted </font><br>";
			} 
		}else {
			echo "<font color='red'>could not create the account account </font><br>";
		}
		
	} else {
		echo 'Ignoring name correction';
	}
	echo '<br><br><br><br>';
	
}




echo '<br><br><br><br>IF you want to delete the users manually:<br><br>';
foreach ($a as $b) {
	echo "/usr/bin/sudo /usr/sbin/userdel $b <br>" ;
}

?>