<?php
/*
	Fixing titles accounts according to given names in array 
	the array name is $newNames = array( $titleId, $newFtpUsername)
	ex. $newNames = array(1=> 'newName1', 3=> 'newName3', ....... );
	
	Note that the publisher ftpUsername will be added with the character _
	as a prefix to the title ftpUsername.
*/

if(!defined('SF_ROOT_DIR')) 
	define('SF_ROOT_DIR',    realpath(dirname(__file__).'/..'));
if(!defined('SF_APP'))
	define('SF_APP',         (isset($argv['app'])?$argv['app']:'technical'));
if(!defined('SF_ENVIRONMENT'))	
	define('SF_ENVIRONMENT', (isset($argv['env'])?$argv['env']:'dev'));
if(!defined('SF_DEBUG'))
	define('SF_DEBUG',       (isset($argv['debug'])?1:1));
 
require_once(SF_ROOT_DIR.DIRECTORY_SEPARATOR.'apps'.DIRECTORY_SEPARATOR.SF_APP.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.php');

$databaseManager = new sfDatabaseManager();
$databaseManager->initialize();
  

$a = array();
$newNames = array(
  1 => 'theweek',              
  2 => 'addustour',            
  3 => 'thestar',              
  4 => 'thepost',           
  5 => 'gulfda',            
  6 => 'tradea',          
  7 => 'thekur',            
  8 => 'photop',          
  9 => 'arabnews',             
 10 => '7days',            
 11 => 'dailyn',         
 12 => 'alliwa',                 
 13 => 'yement',          
 14 => 'horizon',           
 15 => 'somadi',          
 16 => 'elousb',            
 17 => 'hawade',             
 18 => 'taslia',               
 19 => 'daralh',           
 20 => 'daralhi',         
 21 => 'bahrain', 
 22 => 'alayam',       
 23 => 'thedai',   
 24 => 'watan',    
 25 => 'qtrtrbn', 
 26 => 'gmhria', 
 27 => 'messa',      
 28 => 'aqidati',      
 29 => 'egyptml',
 30 => 'gazette',
 31 => 'progegp',
 32 => 'koura',
 33 => 'alyaoum',
 34 => 'albayane',
 35 => 'plstcrnl',
 36 => 'news',
 37 => 'mojen',
 38 => 'mojfa',    
 39 => 'alwasat',      
 41 => 'aswatar',
 42 => 'aswaten',
 43 => 'aswatku',
 44 => 'khaleej',
 45 => 'assafir',
 46 => 'letemps',
 47 => 'exprsion'
  
);
//===============================================================================================\\

require_once LINUX_SCRIPTS_PATH . 'standard.php';
$standard = new Standard();

$titles = TitlePeer::doSelect(new Criteria());

foreach ($titles as $title) {
		
	if(!array_key_exists($title->getId(), $newNames) || ($title->getFtpUsername()==$newNames[$title->getId()])) {
		continue;
	}
			
	$id  		  	= $title->getId();
	$name 		  	= $title->getName();
	$publisherId  	= $title->getPublisherId();
	$publisherFtp 	= $title->getPublisher()->getFtpusername();
	$username	  	= $title->getPublisher()->getFtpUsername() . '_' .$newNames[$title->getId()];
	$oldUsername	= $title->getFtpUsername();
	$pass		  	= $title->getFtpPassword();
	$synId			= $id;
	$homePath	 	= HOMEPATH .'/'. $publisherId;
	$wcPath		 	= WCPATH   .'/'. $publisherId;
	$pwcPath 	 	= PWCPATH  .'/'. $publisherId;
	$pubUsername 	= $title->getPublisher()->getFtpUsername();
	$svnRepo	 	= SVN_REPO_PATH . $publisherId . '/' . $title->getId();
		
	$prefix = $publisherFtp . '_';

	
	$a[] = $username;
	
	echo '<b>Entering the creation of new username area ------------------</b><br>';
	
	$exe = "/usr/bin/sudo /bin/rm  -r " . NAGIOSPATH  	."$publisherId/*";   	exec($exe);   echo $exe . '<br>';
	$exe = "/usr/bin/sudo /bin/rm  -r " . 	  		"$homePath/$oldUsername";  	exec($exe);   echo $exe . '<br>';
	$exe = "/usr/bin/sudo /bin/rm  -r " .   	   	"$wcPath/$oldUsername";  	exec($exe);   echo $exe . '<br>';
	$exe = "/usr/bin/sudo /bin/rm  -r " .     		"$pwcPath/$oldUsername";  	exec($exe);   echo $exe . '<br>';
	
	$exe = "/usr/bin/sudo /bin/chown -R $username:$publisherFtp " . HOMEPATH . "/$username";  	exec($exe);   echo $exe . '<br>';
	$exe = "/usr/bin/sudo /bin/chown -R $username:$publisherFtp " . PWCPATH  . "/$username";  	exec($exe);   echo $exe . '<br>';
	$exe = "/usr/bin/sudo /bin/chown -R $username:$publisherFtp " . WCPATH   . "/$username";  	exec($exe);   echo $exe . '<br>';
			
	$exe = "/usr/bin/sudo /bin/chown -R $username:$publisherFtp " . HOMEPATH . "/$id";  	exec($exe);   echo $exe . '<br>';
	$exe = "/usr/bin/sudo /bin/chown -R $username:$publisherFtp " . PWCPATH  . "/$id";  	exec($exe);   echo $exe . '<br>';
	$exe = "/usr/bin/sudo /bin/chown -R $username:$publisherFtp " . WCPATH   . "/$id";  
				
	$exe    = LINUX_USER_MOD . " " . LINUX_SCRIPTS_PATH . "syndAddSource.sh TITLE $username $pass $synId $homePath $wcPath $pubUsername $svnRepo " . $publisherId . ' ' . $pwcPath;
	exec($exe);
	
	$nagiosPath   = NAGIOSPATH . $publisherId;
	require LINUX_SCRIPTS_PATH  . 'syndGenerateNagiosConf.php';
	
	if($standard->isFtpAccount($username)) {
		echo "<font color='green'>Account created $username </font><br>";
		$title->setFtpUsername($username);
		$title->save();
		$exe = "/usr/bin/sudo /usr/sbin/userdel " . $oldUsername;
		exec($exe);
			
		if($standard->isFtpAccount($oldUsername)) {
			echo "<font color='red'>old account ftp account $oldUsername could not be deleted </font><br>";
		} 
	}else {
		echo "<font color='red'>could not create the account account </font><br>";
	}

	echo '<br><br><br><br>';
	
}




echo '<br><br><br><br>IF you want to delete the users manually:<br><br>';
foreach ($a as $b) {
	echo "/usr/bin/sudo /usr/sbin/userdel $b <br>" ;
}

?>