<?php

// Old Problem when we named the clients FTP without cl_ , this script will rename all client with brifex cl_


/**
 * This script will search for all clients that thier names is not under the standard names 
 * 		cl_ftpUsername and replace them, It will not affect the content but it will replace the 
 * 		names of the symbolic links
 */

if(!defined('SF_ROOT_DIR')) 
	define('SF_ROOT_DIR',    realpath(dirname(__file__).'/..'));
if(!defined('SF_APP'))
	define('SF_APP',         (isset($argv['app'])?$argv['app']:'backend'));
if(!defined('SF_ENVIRONMENT'))	
	define('SF_ENVIRONMENT', (isset($argv['env'])?$argv['env']:'dev'));
if(!defined('SF_DEBUG'))
	define('SF_DEBUG',       (isset($argv['debug'])?1:1));
 
require_once(SF_ROOT_DIR.DIRECTORY_SEPARATOR.'apps'.DIRECTORY_SEPARATOR.SF_APP.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.php');

$databaseManager = new sfDatabaseManager();
$databaseManager->initialize();

//----------------------------------------------------------------------------------------------\\

require_once LINUX_SCRIPTS_PATH . 'standard.php';
$standard = new Standard();

$clients = ClientPeer::doSelect(new Criteria());

foreach ($clients as $client) {
	
	$oldUsername	= $client->getFtpUSername();
	$username 		= substr($oldUsername, 0,2)== 'cl' ? $oldUsername : 'cl_' . $oldUsername;
	
	if(!$standard->isFtpAccount($username)) {
		
		$exe = "/usr/bin/sudo /bin/rm " . CLIENT_HOMEPATH . "/$oldUsername ";  
		exec($exe);   
		echo $exe . '<br>';
		
		$exe    = LINUX_USER_MOD . " " . LINUX_SCRIPTS_PATH . "syndAddClient.sh " . $client->getId() . ' ' . $username . ' ' . $client->getFtpPassword() . ' ' . CLIENT_HOMEPATH;
		exec($exe);
		echo $exe . '<br>';
    									
		if($standard->isFtpAccount($username)) {
					
			$client->setFtpUsername($username);
			$client->save();
			
			echo "<font color='green'>Client account created $username</font> <br>";
			$exe = "/usr/bin/sudo /usr/sbin/userdel " . $oldUsername;
			exec($exe);
						
			if($standard->isFtpAccount($oldUsername)) {
				echo "<font color='red'>old client account ftp account $oldUsername could not be deleted </font><br>";
			} 
		}else {
			echo "<font color='red'>could not create the account $username </font><br>";
		}
		
	} else {
		echo "<font color='blue'>The account $oldUsername is in standard format</font><br>";
	}
	echo '<br><br>';
}
?>