<?php 

class dataBaseHelper 
{
	public $conn;
	
	public function __construct()
	{
		$databaseManager = new sfDatabaseManager();
		$databaseManager->initialize();
		$this->conn	= Propel::getConnection(PublisherPeer::DATABASE_NAME );
	}
	
	
	/**
	 * It Will Not Give you the next insert id but the max [Right now]
	 *
	 * @param String $tableName
	 * @param String $idName
	 * @return Integer
	 */
	public function getTableNextInsertId($tableName, $idName = 'id')
	{		
		$query 		= "SELECT MAX($idName) AS max FROM $tableName";
		$statement 	= $this->conn->prepareStatement($query);
		$resultset 	= $statement->executeQuery();
		$resultset->next();
		return $resultset->getInt('max') + 1;
	}

	
	function getEnum($table_name, $field_name)
	{
		$propelConf = Propel::getConfiguration();
		$dbConf 	= $propelConf['datasources']['propel']['connection'];
		
		$conn = mysql_connect($dbConf['hostspec'], $dbConf['username'], $dbConf['password']) or die('Can not connect to mysql');
		$db   = mysql_select_db($dbConf['database']);
		
		$query = " SHOW COLUMNS FROM `$table_name` LIKE '$field_name' ";
		$result = mysql_query($query);
		$row = mysql_fetch_array( $result , MYSQL_NUM );
		$regex = "/'(.*?)'/";
		preg_match_all( $regex , $row[1], $enum_array );
		$enum_fields = $enum_array[1];
		
		foreach ($enum_fields as $k => $v) {
			$array[$v] = $v;
		}
		return $array;
		//return( $enum_fields );
	}
	
	
	
	function getClientDealingTitlesIds($clientId) 
	{
		$model     = new syndModel();
		$generator = new syndGenerate();
		
		$clientRules   = $model->getClientRules($clientId);
		print_r( $clientRules );
		echo '<br /><br /><br /><br />';
		$whereStatment = $generator->getTitleFilters($clientRules);
		print_r( $whereStatment );
		exit;
		
		if(!$whereStatment) {
			return array();
		} else {
			$query 		= "SELECT id FROM title WHERE $whereStatment";
			$statement 	= $this->conn->prepareStatement($query);
			$resultset 	= $statement->executeQuery();
			$resultset->next();
		
			$result = array();
			foreach ($resultset as $r ) {
				$result[] = $r['id'];
			}
			return $result;
		}
		
	}
	
	
	function getAllTitlesIdAssignedtoClientId ( $clientId ) {
		$model     = new syndModel();
		$generator = new syndGenerate();

		$clientRules   = $model->getTitlesClientsRules($clientId);
		$whereStatment = $generator->getTitleFilters($clientRules);
		if(!$whereStatment) {
			return array();
		} else {
			$query 		= "SELECT id FROM title WHERE $whereStatment";
			$statement 	= $this->conn->prepareStatement($query);
			$resultset 	= $statement->executeQuery();
			$resultset->next();
		
			$result = array();
			foreach ($resultset as $r ) {
				$result[] = $r['id'];
			}
			return $result;
		}
	}
	
}
?>