#!/bin/bash -x

# This script is used to change the password for
# a given syndigate user

#arg 1 safe username
#arg 2 new password

USERMOD="/usr/sbin/usermod"
GREP="/bin/grep"
USERNAME="$1"
PASSWORD="$2"

# Validates if the sent username exists in the system
USERVALUE=`$GREP $USERNAME /etc/passwd | awk -F\: '{print $1}'`
if [[ $USERNAME != $USERVALUE ]]; then
	exit 1
fi

ENCPASS=`mkpasswd $PASSWORD`
$USERMOD --password $ENCPASS $USERNAME
# Validates that the Exist Status = 0 (Success)
EXITSTATUS="$?"
if [[ $EXITSTATUS != 0 ]]; then
	exit 1
fi
