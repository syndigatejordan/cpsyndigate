<?php
/*
This script will regenerate all nagios files for all titles.

It will recall another script since that script can only generate the nagios for one publishers title.
*/

if(!defined('SF_ROOT_DIR')) 
	define('SF_ROOT_DIR',    realpath(dirname(__file__).'/..'));
	if(!defined('SF_APP'))
		define('SF_APP',         (isset($argv['app'])?$argv['app']:'technical'));
	if(!defined('SF_ENVIRONMENT'))	
		define('SF_ENVIRONMENT', (isset($argv['env'])?$argv['env']:'prod'));
	if(!defined('SF_DEBUG'))
		define('SF_DEBUG',       (isset($argv['debug'])?1:1));
				 
require_once(SF_ROOT_DIR.DIRECTORY_SEPARATOR.'apps'.DIRECTORY_SEPARATOR.SF_APP.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.php');

$databaseManager = new sfDatabaseManager();
$databaseManager->initialize();


require_once '../config/config.php';
$publishers = PublisherPeer::doSelect(new Criteria()); 
foreach ($publishers as $publisher)  {
	$publisherId = $publisher->getId();
	$nagiosPath  = NAGIOSPATH . $publisher->getId() ;
	require 'syndGenerateNagiosConf.php';
	echo "Done for publisher id : $publisherId \n";
}
echo "\n" . count($publishers) . " publishers executed. \n\n";
