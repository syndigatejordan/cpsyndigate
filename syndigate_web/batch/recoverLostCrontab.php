<?php

if(!defined('SF_ROOT_DIR')) 
	define('SF_ROOT_DIR',    realpath(dirname(__file__).'/..'));
if(!defined('SF_APP'))
	define('SF_APP',         (isset($argv['app'])?$argv['app']:'backend'));
if(!defined('SF_ENVIRONMENT'))	
	define('SF_ENVIRONMENT', (isset($argv['env'])?$argv['env']:'dev'));
if(!defined('SF_DEBUG'))
	define('SF_DEBUG',       (isset($argv['debug'])?1:1)); // in dev
 
require_once(SF_ROOT_DIR.DIRECTORY_SEPARATOR.'apps'.DIRECTORY_SEPARATOR.SF_APP.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.php');

$databaseManager = new sfDatabaseManager();
$databaseManager->initialize();
////////////////////////////////////////////////


require_once SF_ROOT_DIR . '/../conf/vcrond.conf.php';
require_once SF_ROOT_DIR . '/../vcrond/common.php';

require_once LINUX_SCRIPTS_PATH . 'standard.php';
$standard = new Standard();


echo '<pre>';

$titles = TitlePeer::doSelect(new Criteria());

foreach ($titles as $title) {
	$pid		 = $title->getPublisherId();
	$username 	 = $title->getFtpusername();
	$pass		 = $title->getFtpPassword();
	$synId		 = $title->getId();
	$homePath	 = HOMEPATH .'/'. $pid;
	$wcPath		 = WCPATH   .'/'. $pid;
	$pwcPath 	 = PWCPATH  .'/'. $pid;
	$pubUsername = $title->getPublisher()->getFtpUsername();
	$svnRepo	 = SVN_REPO_PATH . $pid . '/' . $title->getId();
	
	$c = new Criteria();
	$c->add(GatherCrontabPeer::TITLE, $title->getId());
	$crontab = GatherCrontabPeer::doSelectOne($c);
	
	if(!is_object($crontab)) {
		$standard->updateTitleCrontab($title->getId(), $title->getName(), false, '0 1 * * *', APP_PATH.'classes/gather/syndGather.php '.$title->getId());
	}
	
	
}