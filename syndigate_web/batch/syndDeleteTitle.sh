#!/bin/bash -x

#arg 1 : Title ID
#arg 2 : Safe Username (System username)
#arg 3 : Home path
#arg 4 : Working copy path
#arg 5 : Parser working copy path
#arg 6 : SVN repo path
#arg 7 : Nagios path

TITLEID="$1"
USERNAME="$2"
HOMEPATH="$3"
WCPATH="$4"
PWCPATH="$5"
SVNPATH="$6"
NAGIOSPATH="$7"


NAGIOS_CONFIG_FILE="/usr/local/nagios/etc/nagios.cfg"
NAGIOS_INIT="/etc/init.d/nagios-server"
USERDEL="/usr/sbin/userdel"
RM="/bin/rm"
GROUPDEL="/usr/sbin/groupdel"
GREP="/bin/grep"
ECHO="/bin/echo"
SED="/bin/sed"
LS="/bin/ls"

###############################
#delete user and group
###############################

$GREP -iw $USERNAME /etc/passwd
EXITSTATUS="$?"
if [[ $EXITSTATUS != 0 ]]; then
	$ECHO "User does not exist"
	exit 1
	else
		$USERDEL $USERNAME
		EXITSTATUS="$?"
		if [[ $EXITSTATUS != 0 ]]; then
        	$ECHO "There was an error deleting user"
		exit 1
		fi
fi

###############################
#delete home, WC, PWC and SVN
###############################

if [ -d $HOMEPATH ]
	then
	SYM_HOMEPATH=`$ECHO $HOMEPATH | $SED "s/$TITLEID\///"`
	$RM -rf $SYM_HOMEPATH/$USERNAME $HOMEPATH
	EXITSTATUS="$?"
	if [[ $EXITSTATUS != 0 ]]; then
         	$ECHO "This title has no home path"
	fi
fi

if [ -d $WCPATH ]
        then
	SYM_WCPATH=`$ECHO $WCPATH | $SED "s/$TITLEID\///"`
        $RM -rf $SYM_WCPATH/$USERNAME $WCPATH
        EXITSTATUS="$?"
        if [[ $EXITSTATUS != 0 ]]; then
                $ECHO "This title has no working copy path"
        fi
fi

if [ -d $PWCPATH ]
        then
	SYM_PWCPATH=`$ECHO $PWCPATH | $SED "s/$TITLEID\///"`
        $RM -rf $SYM_PWCPATH/$USERNAME $PWCPATH
        EXITSTATUS="$?"
        if [[ $EXITSTATUS != 0 ]]; then
	        $ECHO "This title has no parser working directory"
        fi
fi

if [ -d $SVNPATH ]
        then
        $RM -rf $SVNPATH
        EXITSTATUS="$?"
        if [[ $EXITSTATUS != 0 ]]; then
	        $ECHO "This title has no SVN path directory"
        fi
fi
									

###############################
#delete nagios
###############################

$LS -l $NAGIOSPATH/*$TITLEID*
FILESEXIST="$?"

if [ $FILESEXIST = 0 ]
    then
		$RM -f $NAGIOSPATH/*$TITLEID*
		EXITSTATUS="$?"
        if [[ $EXITSTATUS != 0 ]]; then
                $ECHO "There was an error deleting nagios files"
        fi
        $NAGIOS_INIT reload
fi

$ECHO $?
