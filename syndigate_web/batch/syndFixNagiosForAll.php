<?php

if(!defined('SF_ROOT_DIR')) 
	define('SF_ROOT_DIR',    realpath(dirname(__file__).'/..'));
if(!defined('SF_APP'))
	define('SF_APP',         (isset($argv['app'])?$argv['app']:'backend'));
if(!defined('SF_ENVIRONMENT'))	
	define('SF_ENVIRONMENT', (isset($argv['env'])?$argv['env']:'dev'));
if(!defined('SF_DEBUG'))
	define('SF_DEBUG',       (isset($argv['debug'])?1:1)); // in dev
 
require_once(SF_ROOT_DIR.DIRECTORY_SEPARATOR.'apps'.DIRECTORY_SEPARATOR.SF_APP.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.php');

$databaseManager = new sfDatabaseManager();
$databaseManager->initialize();
////////////////////////////////////////////////

require_once SF_ROOT_DIR . '/../conf/vcrond.conf.php';
require_once SF_ROOT_DIR . '/../vcrond/common.php';

require_once LINUX_SCRIPTS_PATH . 'standard.php';
$standard = new Standard();


$publishers = PublisherPeer::doSelect(new Criteria());

foreach ($publishers as $publisher) {
	$username 	= $publisher->getFtpUsername();
	$pass		= $publisher->getFtpPassword();
	$synId		= $publisher->getId();
	$homePath	= HOMEPATH; 
	$wcPath		= WCPATH;
				
	$exe    = LINUX_USER_MOD . " " . LINUX_SCRIPTS_PATH . "syndAddSource.sh PUBLISHER $username $pass $synId $homePath $wcPath";
	shell_exec($exe);
}



$titles = TitlePeer::doSelect(new Criteria());

foreach ($titles as $title) {
	$pid		 = $title->getPublisherId();
	$username 	 = $title->getFtpusername();
	$pass		 = $title->getFtpPassword();
	$synId		 = $title->getId();
	$homePath	 = HOMEPATH .'/'. $pid;
	$wcPath		 = WCPATH   .'/'. $pid;
	$pwcPath 	 = PWCPATH  .'/'. $pid;
	$pubUsername = $title->getPublisher()->getFtpUsername();
	$svnRepo	 = SVN_REPO_PATH . $pid . '/' . $title->getId();
	
	$exe    = LINUX_USER_MOD . " " . LINUX_SCRIPTS_PATH . "syndAddSource.sh TITLE $username $pass $synId $homePath $wcPath $pubUsername $svnRepo " . $pid . ' ' . $pwcPath;
	shell_exec($exe);
			
	$publisherId  = $pid;
	$nagiosPath   = NAGIOSPATH . $publisherId;
	require LINUX_SCRIPTS_PATH  . 'syndGenerateNagiosConf.php';

	$standard->updateCrontab($title->getName(), false, 'Title Description', '0 1 * * *', APP_PATH.'classes/gather/syndGather.php '.$title->getId(),$title->getId());
	
	
}



$clients = ClientPeer::doSelect(new Criteria());

foreach ($clients as $client) {
	
	$exe    = LINUX_USER_MOD . " " . LINUX_SCRIPTS_PATH . "syndAddClient.sh " . $client->getId() . ' ' . $client->getFtpUsername() . ' ' . $client->getFtpPassword() . ' ' . CLIENT_HOMEPATH;
    exec($exe);
}



?>