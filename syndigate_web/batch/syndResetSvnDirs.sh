#!/bin/bash -x

# a simple script to delete svn contents for a given path

PATH="$*"

echo "$PATH"
SVN="/usr/bin/svn"

for directory in $PATH; do
        if [ ! -e $directory/* ]
        then
                echo "Direcotry is empty"
        else
                $SVN delete $directory/*
                EXITSTATUS="$?"
                        if [[ $EXITSTATUS != 0 ]]; then
                                echo "Error completing operation"
                                exit 1
                        fi
                $SVN commit $directory -m "deleted contents"
        fi
done

