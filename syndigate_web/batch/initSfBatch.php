<?php
define('SF_ROOT_DIR',    realpath(dirname(__file__).'/..'));
 
$argv = array();

for ($i = 1; $i < $_SERVER["argc"]; $i++) {
	if ($_SERVER["argv"][$i]{0} === '-') {
        // argument
        $value = ( isset($_SERVER["argv"][$i+1]) && $_SERVER["argv"][$i+1]{0} !== '-' ? $_SERVER["argv"][$i+1] : true );
        
        if ($_SERVER["argv"][$i]{1} === '-') {
            // long argument
            $argv[substr($_SERVER["argv"][$i], 2)] = $value;
        } else {
        	
        	foreach (str_split($_SERVER["argv"][$i]) as $arg) {
        		if (ereg('[a-zA-Z0-9]', $arg)) {
        			$last_arg   = $arg;
                    $argv[$arg] = true;
                }
        	}
        	$argv[$last_arg] = $value;
        }
    }
}
 
define('SF_APP',         (isset($argv['app'])?$argv['app']:'backend'));
define('SF_ENVIRONMENT', (isset($argv['env'])?$argv['env']:'dev'));
define('SF_DEBUG',       (isset($argv['debug'])?1:1)); // in dev
 
require_once(SF_ROOT_DIR.DIRECTORY_SEPARATOR.'apps'.DIRECTORY_SEPARATOR.SF_APP.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.php');
 
$databaseManager = new sfDatabaseManager();
$databaseManager->initialize();
/////////////////////////////////////////////////////////////////////////////////////////////////

 
?>

