<?php
/* This script will expect the following

1- clientId
2- nagiosPath, the path where nagios files is to be located
*/



$nagiosSuccess = false; //flag 



/**
 * If there is No Email for a contact, but the contact has a mobile [ Nagios must get an email]
 * So we define aletrnative Email for such case  
 */
$defaultEmail  = 'blackhole@syndigate.info';



/**
*  Remove the nagios dir
*/
exec(LINUX_USER_MOD . " /bin/rm -r $nagiosPath");
mkdir($nagiosPath);
	


/**
 * Generates Contact File
 */
$c   = new criteria();
$c->add(ClientContactLinkPeer::CLIENT_ID, $clientId);
$allCntcLnk   = clientContactLinkPeer::doSelect($c);
$clntCntctLnks = array();

/**
 * just contacts With at least sms or email notification will be send to nagios 
 */
foreach ($allCntcLnk as $cntkLnk) {
	if($cntkLnk->getContact()->getNotificationSms() || $cntkLnk->getContact()->getNotificationEmail()) {
		$clntCntctLnks[] = $cntkLnk;
	}
}










// Always Generateing  Service File

$ngsClient 		= ClientPeer::retrieveByPK($clientId);
$serviceToUse   = count($clntCntctLnks) > 0 ? $clientId . ','  : '';
$servicebody =
<<<FILEBODY
define service{
        host_name               synd01
        use                     passive-service
        service_description     CL_{$clientId}_SERVER
        check_command           check_ping
        check_period            24x7
        notifications_enabled   1
        notification_interval   60
        notification_period     24x7
        notification_options    c,r
        contact_groups          {$serviceToUse}albawaba
}

define service{
        host_name               synd01
        use                     passive-service
        service_description     CL_{$clientId}_LOGIN
        check_command           check_ping
        check_period            24x7
        notifications_enabled   1
        notification_interval   60
        notification_period     24x7
        notification_options    c,r
        contact_groups          {$serviceToUse}albawaba
}


FILEBODY;

$servicefile   = $nagiosPath . '/' . $clientId . '_services.cfg';
$servicehandle = file_put_contents($servicefile, $servicebody);









if(count($clntCntctLnks) == 0 ) {
	// Nothing
} else {
	
	
	
	/**
	 * Starting regeneration of nagios files 
	 */
	
	$contactids  = array();
	$contactbody = array();
	
	foreach ($clntCntctLnks as $clntCntctLnk) {
		
		$contact            = ContactPeer::retrieveByPK($clntCntctLnk->getContactId());
        $contactName        = $clientId . '_' . $contact->getId();
        $contactAlias       = $contact->getFullName();
        $contactEmail       = trim($contact->getEmail()) == '' ? $defaultEmail : $contact->getEmail();
        $notifyEmailValue   = $contact->getNotificationEmail();
        $notifySMSValue     = $contact->getNotificationSms();
                
        $contactMobile      = $contact->getMobileNumber();
        $address1String     = trim($contactMobile)=='' ? '' : 'address1'; 
        
        
        # Check values of notify by email, sms (either 0 or 1)
        if (1==(int)$notifyEmailValue ) {
        	$notifyEmailParameter="notify-by-email";
        } else {
            $notifyEmailParameter="";
        }

        if (1==(int)$notifySMSValue) {
        	$notifySMSParameter="notify-by-smsclient";
        } else {
            $notifySMSParameter="";
        }

        $contactids[]           =$contactName;
        $notifyParameters       =trim("$notifyEmailParameter,$notifySMSParameter", ',');


        $contactbody[] =
<<<FILEBODY
define contact{
        contact_name                    $contactName
        alias                           $contactAlias
        service_notification_period     24x7
        service_notification_options    c,r
        service_notification_commands   $notifyParameters
        host_notification_period        none
        host_notification_options       d,r
        host_notification_commands      host-notify-by-email
        email                           $contactEmail
        $address1String                 $contactMobile
}
FILEBODY;
}

$contactfile   = $nagiosPath . '/' . $clientId . '_contacts.cfg';
$contacthandle = file_put_contents($contactfile, implode("\n\n",$contactbody));





//----------------------------------------------------------------------------------------\\



/**
 * Generating contact group file
 */

$contactids = array();

foreach ($clntCntctLnks as $clntCntctLnk) {
	$contactids[]= $clientId . '_' . $clntCntctLnk->getContactId();
}

$contactids      = implode(',', $contactids);
$clientName      = $ngsClient->getClientName();

$contactgroupbody =
<<<FILEBODY
define contactgroup{
        contactgroup_name       $clientId
        alias                   $clientName
        members                 $contactids
}

FILEBODY;

$contactgroupfile   = $nagiosPath . '/' . $clientId . '_contactgroups.cfg';
$contactgrouphandle = file_put_contents($contactgroupfile, $contactgroupbody);
}





exec("/bin/grep -i \"cfg_dir=$nagiosPath\" " . NAGIOS_CFG_FILE, $r, $r);

if($r) {
        $c = "/bin/echo \"cfg_dir=$nagiosPath\" >>" . NAGIOS_CFG_FILE;
        exec($c);
        exec("/usr/bin/sudo /etc/init.d/nagios restart");
}

$nagiosSuccess = true;



?>