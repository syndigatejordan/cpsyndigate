<?php 


require_once '/usr/local/syndigate/scripts/propel_include_config.php';
require_once '/usr/local/syndigate/syndigate_web/config/config.php';


$clients = ClientPeer::doSelect(new Criteria());

foreach ($clients as $client) {
	$exe  = LINUX_USER_MOD . " " . LINUX_SCRIPTS_PATH . "syndAddClient.sh " . $client->getId() . ' ' . $client->getFtpUsername() . ' ' . $client->getFtpPassword() . ' ' . CLIENT_HOMEPATH;
    $res  = shell_exec($exe);
	echo $res . PHP_EOL . str_repeat("-", 150) . PHP_EOL. PHP_EOL; 
}
