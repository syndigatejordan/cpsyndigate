<?php

define('SF_ROOT_DIR',    realpath(dirname(__FILE__).'/..'));
define('SF_APP',         'backend');
define('SF_ENVIRONMENT', 'dev');
define('SF_DEBUG',       true);

require_once(SF_ROOT_DIR.DIRECTORY_SEPARATOR.'apps'.DIRECTORY_SEPARATOR.SF_APP.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.php');

require_once LINUX_SCRIPTS_PATH . 'syndFixNagiosForAll.php';

/*

require_once SF_ROOT_DIR . '/../conf/vcrond.conf.php';
require_once SF_ROOT_DIR . '/../vcrond/common.php';
*/

//sfContext::getInstance()->getController()->dispatch();

