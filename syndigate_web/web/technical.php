<?php
if(!defined('SF_ROOT_DIR'))
	define('SF_ROOT_DIR',    realpath(dirname(__FILE__).'/..'));
if(!defined('SF_APP'))
	define('SF_APP',         'technical');
if(!defined('SF_ENVIRONMENT'))	
	define('SF_ENVIRONMENT', 'prod');
if(!defined('SF_DEBUG'))	
	define('SF_DEBUG',       false);



require_once(SF_ROOT_DIR.DIRECTORY_SEPARATOR.'apps'.DIRECTORY_SEPARATOR.SF_APP.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.php');
require_once SF_ROOT_DIR . '/../conf/vcrond.conf.php';
require_once SF_ROOT_DIR . '/../vcrond/common.php';


sfContext::getInstance()->getController()->dispatch();
