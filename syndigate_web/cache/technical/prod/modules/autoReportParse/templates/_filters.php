<?php
// auto-generated by sfAdvancedAdmin
// date: 2020/06/22 07:52:31
?>
<?php use_helper('Object') ?>

<div class="sf_admin_filters">
<?php echo form_tag('reportParse/list', array('method' => 'get')) ?>

  <fieldset>
    <h2><?php echo __('filters') ?></h2>
    <div class="form-row">
    <label for="date"><?php echo __('Date:') ?></label>
    <div class="content">
    <?php echo input_date_range_tag('filters[date]', isset($filters['date']) ? $filters['date'] : null, array (
  'rich' => true,
  'calendar_button_img' => '/sf/sf_admin/images/date.png',
)) ?>
    </div>
    </div>

        <div class="form-row">
    <label for="title_id"><?php echo __('Title:') ?></label>
    <div class="content">
    <?php echo object_select_tag(isset($filters['title_id']) ? $filters['title_id'] : null, null, array (
  'include_blank' => true,
  'related_class' => 'Title',
  'text_method' => 'getUniqueName',
  'control_name' => 'filters[title_id]',
  'peer_method' => 'getActiveTitles',
)) ?>
    </div>
    </div>

        <div class="form-row">
    <label for="parser_status"><?php echo __('Parser status:') ?></label>
    <div class="content">
    <?php echo input_tag('filters[parser_status]', isset($filters['parser_status']) ? $filters['parser_status'] : null, array (
  'size' => 15,
)) ?>
    </div>
    </div>

      </fieldset>

  <ul class="sf_admin_actions">
    <li><?php echo button_to(__('reset'), 'reportParse/list?filter=filter', 'class=sf_admin_action_reset_filter') ?></li>
    <li><?php echo submit_tag(__('filter'), 'name=filter class=sf_admin_action_filter') ?></li>
  </ul>

</form>
</div>
