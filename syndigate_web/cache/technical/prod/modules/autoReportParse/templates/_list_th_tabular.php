<?php
// auto-generated by sfAdvancedAdmin
// date: 2020/06/22 07:52:31
?>
  <th id="sf_admin_list_th_id">
          <?php if ($sf_user->getAttribute('sort', null, 'sf_admin/report_parse/sort') == 'id'): ?>
      <?php echo link_to(__('Id'), 'reportParse/list?sort=id&type='.($sf_user->getAttribute('type', 'asc', 'sf_admin/report_parse/sort') == 'asc' ? 'desc' : 'asc')) ?>
      (<?php echo __($sf_user->getAttribute('type', 'asc', 'sf_admin/report_parse/sort')) ?>)
      <?php else: ?>
      <?php echo link_to(__('Id'), 'reportParse/list?sort=id&type=asc') ?>
      <?php endif; ?>
          </th>
  <th id="sf_admin_list_th_date">
          <?php if ($sf_user->getAttribute('sort', null, 'sf_admin/report_parse/sort') == 'date'): ?>
      <?php echo link_to(__('Date'), 'reportParse/list?sort=date&type='.($sf_user->getAttribute('type', 'asc', 'sf_admin/report_parse/sort') == 'asc' ? 'desc' : 'asc')) ?>
      (<?php echo __($sf_user->getAttribute('type', 'asc', 'sf_admin/report_parse/sort')) ?>)
      <?php else: ?>
      <?php echo link_to(__('Date'), 'reportParse/list?sort=date&type=asc') ?>
      <?php endif; ?>
          </th>
  <th id="sf_admin_list_th_title">
        <?php echo __('Title') ?>
          </th>
  <th id="sf_admin_list_th_parser_status">
          <?php if ($sf_user->getAttribute('sort', null, 'sf_admin/report_parse/sort') == 'parser_status'): ?>
      <?php echo link_to(__('Parser status'), 'reportParse/list?sort=parser_status&type='.($sf_user->getAttribute('type', 'asc', 'sf_admin/report_parse/sort') == 'asc' ? 'desc' : 'asc')) ?>
      (<?php echo __($sf_user->getAttribute('type', 'asc', 'sf_admin/report_parse/sort')) ?>)
      <?php else: ?>
      <?php echo link_to(__('Parser status'), 'reportParse/list?sort=parser_status&type=asc') ?>
      <?php endif; ?>
          </th>
  <th id="sf_admin_list_th_article_num">
          <?php if ($sf_user->getAttribute('sort', null, 'sf_admin/report_parse/sort') == 'article_num'): ?>
      <?php echo link_to(__('Article num'), 'reportParse/list?sort=article_num&type='.($sf_user->getAttribute('type', 'asc', 'sf_admin/report_parse/sort') == 'asc' ? 'desc' : 'asc')) ?>
      (<?php echo __($sf_user->getAttribute('type', 'asc', 'sf_admin/report_parse/sort')) ?>)
      <?php else: ?>
      <?php echo link_to(__('Article num'), 'reportParse/list?sort=article_num&type=asc') ?>
      <?php endif; ?>
          </th>
  <th id="sf_admin_list_th_articles_ids">
          <?php if ($sf_user->getAttribute('sort', null, 'sf_admin/report_parse/sort') == 'articles_ids'): ?>
      <?php echo link_to(__('Articles ids'), 'reportParse/list?sort=articles_ids&type='.($sf_user->getAttribute('type', 'asc', 'sf_admin/report_parse/sort') == 'asc' ? 'desc' : 'asc')) ?>
      (<?php echo __($sf_user->getAttribute('type', 'asc', 'sf_admin/report_parse/sort')) ?>)
      <?php else: ?>
      <?php echo link_to(__('Articles ids'), 'reportParse/list?sort=articles_ids&type=asc') ?>
      <?php endif; ?>
          </th>
  <th id="sf_admin_list_th_parser_error">
          <?php if ($sf_user->getAttribute('sort', null, 'sf_admin/report_parse/sort') == 'parser_error'): ?>
      <?php echo link_to(__('Parser error'), 'reportParse/list?sort=parser_error&type='.($sf_user->getAttribute('type', 'asc', 'sf_admin/report_parse/sort') == 'asc' ? 'desc' : 'asc')) ?>
      (<?php echo __($sf_user->getAttribute('type', 'asc', 'sf_admin/report_parse/sort')) ?>)
      <?php else: ?>
      <?php echo link_to(__('Parser error'), 'reportParse/list?sort=parser_error&type=asc') ?>
      <?php endif; ?>
          </th>
  <th id="sf_admin_list_th_logs_link">
        <?php echo __('Logs link') ?>
          </th>
