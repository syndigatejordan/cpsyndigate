<?php
// auto-generated by sfPropelAdmin
// date: 2020/06/15 08:27:43
?>
<?php

/**
 * autoTitleReportParse actions.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage autoTitleReportParse
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: actions.class.php 7997 2008-03-20 12:29:34Z noel $
 */
class autoTitleReportParseActions extends sfActions
{
  public function executeIndex()
  {
    return $this->forward('titleReportParse', 'list');
  }

  public function executeList()
  {
    $this->processSort();

    $this->processFilters();


    // pager
    $this->pager = new sfPropelPager('ReportParse', 10);
    $c = new Criteria();
    $this->addSortCriteria($c);
    $this->addFiltersCriteria($c);
    $this->pager->setCriteria($c);
    $this->pager->setPage($this->getRequestParameter('page', 1));
    $this->pager->init();
  }

  public function executeCreate()
  {
    return $this->forward('titleReportParse', 'edit');
  }

  public function executeSave()
  {
    return $this->forward('titleReportParse', 'edit');
  }

  public function executeEdit()
  {
    $this->report_parse = $this->getReportParseOrCreate();

    if ($this->getRequest()->getMethod() == sfRequest::POST)
    {
      $this->updateReportParseFromRequest();

      $this->saveReportParse($this->report_parse);

      $this->setFlash('notice', 'Your modifications have been saved');

      if ($this->getRequestParameter('save_and_add'))
      {
        return $this->redirect('titleReportParse/create');
      }
      else if ($this->getRequestParameter('save_and_list'))
      {
        return $this->redirect('titleReportParse/list');
      }
      else
      {
        return $this->redirect('titleReportParse/edit?id='.$this->report_parse->getId());
      }
    }
    else
    {
      $this->labels = $this->getLabels();
    }
  }

  public function executeDelete()
  {
    $this->report_parse = ReportParsePeer::retrieveByPk($this->getRequestParameter('id'));
    $this->forward404Unless($this->report_parse);

    try
    {
      $this->deleteReportParse($this->report_parse);
    }
    catch (PropelException $e)
    {
      $this->getRequest()->setError('delete', 'Could not delete the selected Report parse. Make sure it does not have any associated items.');
      return $this->forward('titleReportParse', 'list');
    }

    return $this->redirect('titleReportParse/list');
  }

  public function handleErrorEdit()
  {
    $this->preExecute();
    $this->report_parse = $this->getReportParseOrCreate();
    $this->updateReportParseFromRequest();

    $this->labels = $this->getLabels();

    return sfView::SUCCESS;
  }

  protected function saveReportParse($report_parse)
  {
    $report_parse->save();

  }

  protected function deleteReportParse($report_parse)
  {
    $report_parse->delete();
  }

  protected function updateReportParseFromRequest()
  {
    $report_parse = $this->getRequestParameter('report_parse');

    if (isset($report_parse['articles_ids']))
    {
      $this->report_parse->setArticlesIds($report_parse['articles_ids']);
    }
    if (isset($report_parse['title_id']))
    {
    $this->report_parse->setTitleId($report_parse['title_id'] ? $report_parse['title_id'] : null);
    }
    if (isset($report_parse['date']))
    {
      if ($report_parse['date'])
      {
        try
        {
          $dateFormat = new sfDateFormat($this->getUser()->getCulture());
                              if (!is_array($report_parse['date']))
          {
            $value = $dateFormat->format($report_parse['date'], 'i', $dateFormat->getInputPattern('d'));
          }
          else
          {
            $value_array = $report_parse['date'];
            $value = $value_array['year'].'-'.$value_array['month'].'-'.$value_array['day'].(isset($value_array['hour']) ? ' '.$value_array['hour'].':'.$value_array['minute'].(isset($value_array['second']) ? ':'.$value_array['second'] : '') : '');
          }
          $this->report_parse->setDate($value);
        }
        catch (sfException $e)
        {
          // not a date
        }
      }
      else
      {
        $this->report_parse->setDate(null);
      }
    }
    if (isset($report_parse['parser_status']))
    {
      $this->report_parse->setParserStatus($report_parse['parser_status']);
    }
    if (isset($report_parse['parser_error']))
    {
      $this->report_parse->setParserError($report_parse['parser_error']);
    }
    if (isset($report_parse['article_num']))
    {
      $this->report_parse->setArticleNum($report_parse['article_num']);
    }
  }

  protected function getReportParseOrCreate($id = 'id')
  {
    if (!$this->getRequestParameter($id))
    {
      $report_parse = new ReportParse();
    }
    else
    {
      $report_parse = ReportParsePeer::retrieveByPk($this->getRequestParameter($id));

      $this->forward404Unless($report_parse);
    }

    return $report_parse;
  }

  protected function processFilters()
  {
  }

  protected function processSort()
  {
    if ($this->getRequestParameter('sort'))
    {
      $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/report_parse/sort');
      $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/report_parse/sort');
    }

    if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/report_parse/sort'))
    {
      $this->getUser()->setAttribute('sort', 'id', 'sf_admin/report_parse/sort');
      $this->getUser()->setAttribute('type', 'desc', 'sf_admin/report_parse/sort');
    }
  }

  protected function addFiltersCriteria($c)
  {
  }

  protected function addSortCriteria($c)
  {
    if ($sort_column = $this->getUser()->getAttribute('sort', null, 'sf_admin/report_parse/sort'))
    {
      $sort_column = ReportParsePeer::translateFieldName($sort_column, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);
      if ($this->getUser()->getAttribute('type', null, 'sf_admin/report_parse/sort') == 'asc')
      {
        $c->addAscendingOrderByColumn($sort_column);
      }
      else
      {
        $c->addDescendingOrderByColumn($sort_column);
      }
    }
  }

  protected function getLabels()
  {
    return array(
      'report_parse{id}' => 'Id:',
      'report_parse{articles_ids}' => 'Articles ids:',
      'report_parse{title_id}' => 'Title:',
      'report_parse{date}' => 'Date:',
      'report_parse{parser_status}' => 'Parser status:',
      'report_parse{parser_error}' => 'Parser error:',
      'report_parse{article_num}' => 'Article num:',
    );
  }
}
