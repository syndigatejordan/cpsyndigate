<?php
// auto-generated by sfAdvancedAdmin
// date: 2020/06/15 08:44:15
?>
<?php use_helper('Object', 'Validation', 'ObjectAdmin', 'I18N', 'Date') ?>

<?php use_stylesheet('modyfiedMain') ?>

<div id="sf_admin_container">

<h1><?php echo __('create article', 
array()) ?></h1>

<div id="sf_admin_header">
<?php include_partial('article/create_header', array('article' => $article)) ?>
</div>

<div id="sf_admin_content">
<?php include_partial('article/create_messages', array('article' => $article, 'labels' => $labels)) ?>
<?php include_partial('article/create_form', array('article' => $article, 'labels' => $labels)) ?>
</div>

<div id="sf_admin_footer">
<?php include_partial('article/create_footer', array('article' => $article)) ?>
</div>

</div>
