<?php
// auto-generated by sfAdvancedAdmin
// date: 2020/06/15 08:44:15
?>
<fieldset id="sf_fieldset_none" class="">

<div class="form-row">
  <?php echo label_for('article[id]', __($labels['article{id}']), 'class="required" ') ?>
  <div class="content">
  <?php $value = $article->getId(); echo $value ? $value : '&nbsp;' ?>
    </div>
</div>

<div class="form-row">
  <?php echo label_for('article[iptc_id]', __($labels['article{iptc_id}']), 'class="required" ') ?>
  <div class="content">
  <?php $value = $article->getIptcId(); echo $value ? $value : '&nbsp;' ?>
    </div>
</div>

<div class="form-row">
  <?php echo label_for('article[title_id]', __($labels['article{title_id}']), 'class="required" ') ?>
  <div class="content">
  <?php $value = $article->getTitleId(); echo $value ? $value : '&nbsp;' ?>
    </div>
</div>

<div class="form-row">
  <?php echo label_for('article[article_original_data_id]', __($labels['article{article_original_data_id}']), 'class="required" ') ?>
  <div class="content">
  <?php $value = $article->getArticleOriginalDataId(); echo $value ? $value : '&nbsp;' ?>
    </div>
</div>

<div class="form-row">
  <?php echo label_for('article[language_id]', __($labels['article{language_id}']), 'class="required" ') ?>
  <div class="content">
  <?php $value = $article->getLanguageId(); echo $value ? $value : '&nbsp;' ?>
    </div>
</div>

<div class="form-row">
  <?php echo label_for('article[headline]', __($labels['article{headline}']), 'class="required" ') ?>
  <div class="content">
  <?php $value = $article->getHeadline(); echo $value ? $value : '&nbsp;' ?>
    </div>
</div>

<div class="form-row">
  <?php echo label_for('article[summary]', __($labels['article{summary}']), 'class="required" ') ?>
  <div class="content">
  <?php $value = $article->getSummary(); echo $value ? $value : '&nbsp;' ?>
    </div>
</div>

<div class="form-row">
  <?php echo label_for('article[body]', __($labels['article{body}']), 'class="required" ') ?>
  <div class="content">
  <?php $value = $article->getBody(); echo $value ? $value : '&nbsp;' ?>
    </div>
</div>

<div class="form-row">
  <?php echo label_for('article[author]', __($labels['article{author}']), 'class="required" ') ?>
  <div class="content">
  <?php $value = $article->getAuthor(); echo $value ? $value : '&nbsp;' ?>
    </div>
</div>

<div class="form-row">
  <?php echo label_for('article[date]', __($labels['article{date}']), 'class="required" ') ?>
  <div class="content">
  <?php $value = ($article->getDate() !== null && $article->getDate() !== '') ? format_date($article->getDate(), "D") : ''; echo $value ? $value : '&nbsp;' ?>
    </div>
</div>

<div class="form-row">
  <?php echo label_for('article[parsed_at]', __($labels['article{parsed_at}']), 'class="required" ') ?>
  <div class="content">
  <?php $value = ($article->getParsedAt() !== null && $article->getParsedAt() !== '') ? format_date($article->getParsedAt(), "f") : ''; echo $value ? $value : '&nbsp;' ?>
    </div>
</div>

<div class="form-row">
  <?php echo label_for('article[updated_at]', __($labels['article{updated_at}']), 'class="required" ') ?>
  <div class="content">
  <?php $value = ($article->getUpdatedAt() !== null && $article->getUpdatedAt() !== '') ? format_date($article->getUpdatedAt(), "f") : ''; echo $value ? $value : '&nbsp;' ?>
    </div>
</div>

<div class="form-row">
  <?php echo label_for('article[has_time]', __($labels['article{has_time}']), 'class="required" ') ?>
  <div class="content">
  <?php $value = $article->getHasTime(); echo $value ? $value : '&nbsp;' ?>
    </div>
</div>

<div class="form-row">
  <?php echo label_for('article[is_calias_called]', __($labels['article{is_calias_called}']), 'class="required" ') ?>
  <div class="content">
  <?php $value = $article->getIsCaliasCalled(); echo $value ? $value : '&nbsp;' ?>
    </div>
</div>

<div class="form-row">
  <?php echo label_for('article[sub_feed]', __($labels['article{sub_feed}']), 'class="required" ') ?>
  <div class="content">
  <?php $value = $article->getSubFeed(); echo $value ? $value : '&nbsp;' ?>
    </div>
</div>

</fieldset>

<?php include_partial('show_actions', array('article' => $article)) ?>
