<?php
	
	define('APP_PATH', dirname(__FILE__).'/');
	define('IMGS_PATH', '/syndigate/imgs/');
	define('IMGS_HOST', 'http://imgs.syndigate.info/');
	define('VIDEOS_PATH', '/syndigate/videos/');
	define('VIDEOS_HOST', 'http://videos.syndigate.info/');

	//Include path fixation for eZ Components
	set_include_path( APP_PATH . "ezcomponents-2007.2" . PATH_SEPARATOR  . get_include_path() );
	
	//Include path fixation for PEAR
	set_include_path( APP_PATH . "PEAR" . PATH_SEPARATOR  . get_include_path() );
	
	/*
	 * Add any custom classes as follows:
	 * 
	 * key: Class Name
	 * val: File Path
	 */
	$_autoload_custom = array(
		'syndLog'              			=> APP_PATH . 'classes/syndLog.php',
		'syndAlert'              		=> APP_PATH . 'classes/syndAlert.php',
		'syndGatherContent'      		=> APP_PATH . 'classes/gather/content/syndGatherContent.php',
		'syndGatherContentFtp'  		=> APP_PATH . 'classes/gather/content/syndGatherContentFtp.php',
		'syndGatherContentHttp' 		=> APP_PATH . 'classes/gather/content/syndGatherContentHttp.php',
		'syndGatherContentLocalRss'  	=> APP_PATH . 'classes/gather/content/syndGatherContentLocalRss.php',
		'syndGatherContentLocal' 		=> APP_PATH . 'classes/gather/content/syndGatherContentLocal.php',				
		'syndGatherCheck'				=> APP_PATH . 'classes/gather/content/syndGatherCheck.php',
		'syndModel'						=> APP_PATH . 'classes/syndModel.php',
		'syndParseContent'				=> APP_PATH . 'classes/parse/syndParseContent.php',
		'syndParseRss'					=> APP_PATH . 'classes/parse/syndParseRss.php',
		'syndSvnDataGathering'			=> APP_PATH . 'classes/parse/syndSvnDataGathering.php',
		'syndParseXmlContent'  			=> APP_PATH . 'classes/parse/syndParseXmlContent.php',
		'syndParse'						=> APP_PATH . 'classes/syndParse.php',
		'syndReport'					=> APP_PATH . 'classes/syndReport.php',
		'syndGatherSvn'					=> APP_PATH . 'classes/gather/content/syndGatherSvn.php',
		'Crontab'						=> APP_PATH . 'vcrond/lib/cron/tab.php',
		'mysqldb'						=> APP_PATH . 'vcrond/lib/miniDb.php',
		'syndParseCms'					=> APP_PATH . 'classes/parse/syndParseCms.php',
		'syndGenerate'					=> APP_PATH . 'classes/syndGenerate.php',
		'syndGenerateTemplate'			=> APP_PATH . 'classes/generate/syndGenerateTemplate.php',
		'syndGenerateTemplatesManager'	=> APP_PATH . 'classes/generate/syndGenerateTemplatesManager.php',
		'syndGenerateMySqlIterator'		=> APP_PATH . 'classes/generate/syndGenerateMySqlIterator.php',
		'syndGenerateArrayFormater'		=> APP_PATH . 'classes/generate/syndGenerateArrayFormater.php',
		'syndParseHelper'				=> APP_PATH . 'classes/parse/syndParseHelper.php',
		'syndParseParser'				=> APP_PATH . 'classes/parse/syndParseParser.php',
		'syndSend'    				    => APP_PATH . 'classes/send/syndSend.php',
		'syndParseAbTxtSample'			=> APP_PATH . 'classes/parse/syndParseAbTxtSample.php', 
		
		//custom parsers							
		'syndParseHtMedia'			    => APP_PATH .'classes/parse/syndParseHtMedia.php',
		'syndParseWMCCM'				=> APP_PATH .'classes/parse/syndParseWMCCM.php',
		'syndParsePlusCommunications'	=> APP_PATH . 'classes/parse/syndParsePlusCommunications.php',
		
		'Pheanstalk'				=> APP_PATH . 'lib/pheanstalk/pheanstalk_init.php',
		
		'syndReGenerate'				=> APP_PATH . 'classes/syndReGenerate.php',
		'syndModelMongo'				=> APP_PATH . 'classes/syndModelMongo.php',
	);
	
	
	require_once "Base/src/base.php";
	spl_autoload_register('__autoload');
	function __autoload( $className )
	{
		if (array_key_exists($className, $GLOBALS['_autoload_custom'])) {
			require_once $GLOBALS['_autoload_custom'][$className];
		}
		
		ezcBase::autoload( $className );
	}
		  
	ezcBaseInit::setCallback( 
		'ezcInitLog',
		'syndLog'
	);

	
	/*
	 * Symfony models initiating
	 */
	if (!defined('SF_ROOT_DIR')) {
		define('SF_ROOT_DIR',    APP_PATH . 'syndigate_web/');        // Inside symfony project dir
	}
	
	if(!defined('SF_APP')) {
		define('SF_APP',         'technical');
	}
	if(!defined('SF_ENVIRONMENT')) {
		define('SF_ENVIRONMENT', 'prod');
	}
	if(!defined('SF_DEBUG')) {
		define('SF_DEBUG',       false);
	}
        
	require_once SF_ROOT_DIR.'/apps/'.SF_APP.'/config/config.php';
    $databaseManager = new sfDatabaseManager();
    $databaseManager->initialize();
