<?php

class syndSend_112 extends syndSend {

  /**
   * Copy the new content to the home directory of the title
   * 
   */
  public function copyContent() {

    $checkContentDirs = $this->checkContent();

    $settings = array();
    $conns = $this->client->getClientConnections();

    if (count($conns) > 0) {

      foreach ($conns as $conn) {
        $connections[] = array("ftpServer" => $conn->getUrl(),
            "ftpPort" => $conn->getFtpPort(),
            "ftpUsername" => $conn->getUsername(),
            "ftpUserPass" => $conn->getPassword(),
            "remoteDir" => $conn->getRemoteDir(),
            "id" => $conn->getId()
        );
      }
    } else {
      return;
    }


    foreach ($connections as $connection) {

      $c = new Criteria();
      $c->add(ClientConnectionSentPeer::CONNECTION_ID, $connection['id']);
      $sentFiles = ClientConnectionSentPeer::doSelectOne($c);

      if (!is_object($sentFiles)) {
        $sentFiles = new ClientConnectionSent();
        $sentFiles->setConnectionId($connection['id']);
        $sentFiles->save();
      }
      $sentFiles->init();

      $this->ftpServer = $connection["ftpServer"];
      $this->ftpPort = $connection["ftpPort"];
      $this->ftpUsername = $connection["ftpUsername"];
      $this->ftpUserPass = $connection["ftpUserPass"];
      $this->remoteDir = $connection["remoteDir"];

      $this->log->log('Custem syndSend_112', ezcLog::DEBUG, $this->logParams);
      //login to the FTP server
      $this->login();
      $upload;

      if (is_array($checkContentDirs)) {

        // Sending to root without adding timestamp to the files, so the files will be replaced
        $days = $checkContentDirs;
        $latestDay = array_shift($days);
        $todayPath = $this->homeDir . $latestDay;

        foreach (scandir($this->homeDir . $latestDay) as $file) {

          if ($file != '.' && $file != '..') {
            $localFile = substr($this->homeDir, 0, strrpos($this->homeDir, '/')) . $latestDay . $file;
            $this->log->log('Started copying File: ' . $file . ' to  ' . $this->ftpServer . ' title.', ezcLog::DEBUG, $this->logParams);
            $remoteFile = $this->remoteDir . DIRECTORY_SEPARATOR . pathinfo($todayPath . $file, PATHINFO_FILENAME) . '.' . pathinfo($todayPath . $file, PATHINFO_EXTENSION);

            $upload = $this->ftp->put($localFile, $remoteFile, true);


            if (PEAR::isError($upload)) {
              $this->processState = ezcLog::FAILED_AUDIT;
              $this->reportParams['SenderStatus'] = 0;
              $this->reportParams['SenderMsg'] = $upload->getMessage() . ' to ' . $this->ftpServer . ' title. Failed.';
              $this->report->updateReport($this->reportParams);
              $this->alert->send($upload->getMessage() . ' to ' . $this->clientName . ' Client. Failed.', syndAlert::CRITICAL, 'SERVER', $this->clientId, 'client');
              $this->log->log($upload->getMessage() . ' to ' . $this->clientName . ' Client. Failed.', ezcLog::FATAL, $this->logParams);
            } else {

              if (($this->contentSendingMode == 'updates') && !is_dir($localFile)) {
                if (!$sentFiles->getIsSentBefore($file)) {
                  $sentFiles->addToSent($file);
                }
              }
              $this->alert->send('OK. New Content Found.', syndAlert::OK, 'SERVER', $this->clientId, 'client');
              $this->log->log('Copying File ' . ' to ' . $this->clientName . " [{$this->ftpUsername}] " . ' client. Succeeded.', ezcLog::DEBUG, $this->logParams);
            }
          }
        }
      } else {
        $this->log->log('No content found for ' . $this->clientName . ' Client.', ezcLog::WARNING, $this->logParams);
      }

      $this->ftp->disconnect();
      $sentFiles->saveSettings();
    }//End foreach connections

    return true;
  }

  public function login() {
    $this->connect();
    $login = $this->ftp->login($this->ftpUsername, $this->ftpUserPass);
    $this->log->log('FTP login PASSIVE MODE', ezcLog::DEBUG, $this->logParams);
    $login->setPassive();
    if (PEAR::isError($login)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['SenderStatus'] = 0;
      $this->reportParams['SenderMsg'] = $login->getMessage() . ' for: ' . $this->ftpServer;
      $this->report->updateReport($this->reportParams);
      $this->log->log($login->getMessage() . ' for: ' . $this->clientName . 'with connection [' . $this->ftpUsername . ']', ezcLog::FATAL, $this->logParams);
      $this->alert->send("Server is down.", syndAlert::CRITICAL, 'SERVER', $this->clientId, 'client');
    } else {
      $this->log->log('FTP login to ' . $this->clientName . ' with connection : [' . $this->ftpUsername . '] Succeeded', ezcLog::DEBUG, $this->logParams);
      $this->alert->send("Server is Up, we can upload the new content.", syndAlert::OK, 'SERVER', $this->clientId, 'client');
    }
  }

}
