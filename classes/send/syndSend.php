<?php
require_once dirname(__FILE__) . '/../../ez_autoload.php';
require_once 'Net/FTP.php';
class syndSend
{
    /**
     * Set the host URL
     *
     * @var string
     */
    protected $ftpServer;
    /**
     * Set the host post
     *
     * @var int
     */
    protected $ftpPort;
    /**
     * The host username
     *
     * @var string
     */
    protected $ftpUsername;
    /**
     * The host password
     *
     * @var string
     */
    protected $ftpUserPass;
    /**
     * The home directory
     *
     * @var sting
     */
    protected $homeDir;
    /**
     * The remote directory
     *
     * @var sting
     */
    protected $remoteDir;
    /**
     * creat an Object from the Model class
     *
     * @var object
     */
    protected $model;
    /**

     * Check if the Title is new or not
     *
     * @var bool
     */
    protected $new;
    /**
     * Get the one and only instance of the ezcLog class
     *
     * @var object
     */
    protected $log;
    /**
     * Get the one and only instance of the syndAlert class
     *
     * @var object
     */
    protected $alert;
    /**
     * Object of the NET_FTP pear package
     *
     * @var object
     */
    protected $ftp;
    /**
     * The Title ID
     *
     * @var int
     */
    protected $clientId;
    /**
     * The Client Name
     *
     * @var int
     */
    protected $clientName;
    /**
     *creat an Object from the syndReport class
     *
     * @var object
     */
    protected $report;
    /**
     * The report ID for the gather_report table
     *
     * @var int
     */
    protected $reportId;
    /**
     * Store the report parameter
     *
     * @var array
     */
    protected $reportParams;
    /**
     * number of folders to copy
     *
     * @var int
     */
    protected $daysNum;
    /**
     * The status of the proccess
     *
     * @var string
     */
    protected $processState = ezcLog::SUCCESS_AUDIT;
    /**
     * Get the Directory Layout of this Client 
     *
     * @var string
     */
    protected $dirLayout;
    
    /*
     * Source for Log
     * 
     * @var string
     */
    protected $logSource = 'send';
    
    /*
     * client object
     *
     * @var Client
     */
    protected $client;
    
    
    
    protected $logParams;
    
    /*
     * Content sending mod updates or all 
     *
     */
    protected $contentSendingMode;
    
    /**
     * Constructor for the FTP class 
     *
     * @param int $clientId
     * @param int $daysNum
     */
    public function __construct ($clientId, $daysNum)
    {
        $this->log 			= ezcLog::getInstance();
        $this->alert 		= syndAlert::getInstance();
        $this->log->source 	= $this->logSource;
        $this->log->category= 'FTP Push';
        
        $this->model 		= new syndModel();
        $this->ftp 			= new Net_FTP();
        $this->report 		= new syndReport('send');
        $this->client 		= $this->model->getClientFields($clientId);
        $this->homeDir 		= $this->client->getHomeDir();
        $this->dirLayout	= $this->client->getDirLayout();
        $this->clientName 	= $this->client->getClientName();
        $this->clientId 	= $clientId;
        $this->daysNum 		= $daysNum;
        $this->contentSendingMode = $this->client->getGenerateType();
        
        //initiate the report parameters
        $this->reportParams['ClientId'] 	= $clientId;
        $this->reportParams['FilesCount'] 	= 0;
        $this->reportParams['SenderStatus'] = 0;
        $this->reportParams['StartTime'] 	= time();
        $this->reportParams['EndTime'] 		= 0;
        $this->reportParams['SenderMsg'] 	= 'Start Copying the Content from the Home Directory to the Client FTP server.';
        
        $reportId = $this->report->addReport($this->reportParams);
        $this->reportParams['Id'] = $reportId;
        $this->reportId 		  = $reportId;
        
        $this->logParams = array('report_id' => $this->reportId, 'source' => $this->logSource);
        
    }
    
    
    /**
     * Connect to the Title's FTP server
     *
     */
    public function connect ()
    {
        //Note:  FTP URL MOST NOT HAVE [ FTP:// ] prefix		
        $connect = $this->ftp->connect($this->ftpServer, $this->ftpPort);
        if (PEAR::isError($connect)) {
            $this->processState = ezcLog::FAILED_AUDIT;
            $this->reportParams['SenderStatus'] = 0;
            $this->reportParams['SenderMsg'] 	= $connect->getMessage() . ' for: ' . $this->ftpServer;
            $this->report->updateReport($this->reportParams);
            $this->log->log($connect->getMessage() . ' for: ' . $this->clientName . 'with connection [' . $this->ftpUsername .']', ezcLog::FATAL, $this->logParams);
            $this->alert->send("Server is down.", syndAlert::CRITICAL, 'SERVER', $this->clientId, 'client');
        } else {
            $this->log->log('FTP connection to ' . $this->clientName . ' with connection : [' . $this->ftpUsername . '] Succeeded', ezcLog::INFO, $this->logParams);
        }
    }
    
    /**
     * Login to the Title's FTP server
     *
     */
    public function login ()
    {
        $this->connect();
        $login = $this->ftp->login($this->ftpUsername, $this->ftpUserPass);
        if (PEAR::isError($login)) {
            $this->processState = ezcLog::FAILED_AUDIT;
            $this->reportParams['SenderStatus'] = 0;
            $this->reportParams['SenderMsg'] 	= $login->getMessage() . ' for: ' . $this->ftpServer;
            $this->report->updateReport($this->reportParams);
            $this->log->log($login->getMessage() . ' for: ' . $this->clientName . 'with connection [' . $this->ftpUsername .']', ezcLog::FATAL, $this->logParams);
            $this->alert->send("Server is down.", syndAlert::CRITICAL, 'SERVER', $this->clientId, 'client');
        } else {
            $this->log->log('FTP login to ' . $this->clientName . ' with connection : [' . $this->ftpUsername .  '] Succeeded', ezcLog::DEBUG, $this->logParams);
            $this->alert->send("Server is Up, we can upload the new content.", syndAlert::OK, 'SERVER', $this->clientId, 'client');
        }
    }
    
    /**
     * Get the Newest Directories to copy
     *
     * @return array
     */
    public function checkContent ()
    {
        $numDaysSync = $this->daysNum;
        for ($i = 0; $i < $numDaysSync; $i ++) {
            if (file_exists($this->homeDir . date('Y/m/d/', strtotime("-{$i} day")))) {
                $folders[] = date('/Y/m/d/', strtotime("-{$i} day"));
            }
        }
        if ($folders) {
            $this->log->log('Getting new directories for ' . $this->clientName . ' Succeeded', ezcLog::DEBUG, array('report_id' => $this->reportId));
            return $folders;
        } else {
            $this->processState = ezcLog::FAILED_AUDIT;
            $this->reportParams['SenderStatus'] = 0;
            $this->reportParams['SenderMsg'] 	= 'Getting new directories for ' . $this->ftpServer . ' Failed';
            $this->report->updateReport($this->reportParams);
            $this->log->log('Getting new directories for ' . $this->clientName . 'with connection [' . $this->ftpUsername .']' . ' Failed', ezcLog::FATAL, $this->logParams);
            return false;
        }
    }
    
    /**
     * Copy the new content to the home directory of the title
     *
     */
    public function copyContent()
    {    	
   		$checkContentDirs = $this->checkContent();
   		
   		$settings   = array();
    	$conns 		= $this->client->getClientConnections();
   		if(count($conns)>0) {
   			   			
   			foreach ($conns as $conn) {
   				$connections[] = array(	"ftpServer"		=> $conn->getUrl(), 
   										"ftpPort"  		=> $conn->getFtpPort(),
   										"ftpUsername"	=> $conn->getUsername(),
   										"ftpUserPass" 	=> $conn->getPassword(),
   										"remoteDir" 	=> $conn->getRemoteDir(), 
   										"id"			=> $conn->getId()
   										);
   			}
   		} else {
   			return;
   		}
   		
   		mail('alaa@corp.albawaba.com',"TEST", var_export($connections,true));		
   		foreach ($connections as $connection) {
   			
   			$c = new Criteria();
   			$c->add(ClientConnectionSentPeer::CONNECTION_ID, $connection['id']);
   			$sentFiles = ClientConnectionSentPeer::doSelectOne($c);
   			
   			if(!is_object($sentFiles)) {
   				$sentFiles = new ClientConnectionSent();
   				$sentFiles->setConnectionId($connection['id']);
   				$sentFiles->save();
   			}
   			$sentFiles->init();
   			    		
    		$this->ftpServer 	= $connection["ftpServer"];
    		$this->ftpPort 		= $connection["ftpPort"];
    		$this->ftpUsername 	= $connection["ftpUsername"];
    		$this->ftpUserPass 	= $connection["ftpUserPass"];
    		$this->remoteDir 	= $connection["remoteDir"];
    		
    		
    		//login to the FTP server
    		$this->login();
    		$upload;
    		
    		if (is_array($checkContentDirs)) {
    			
    			if ($this->dirLayout == 'dates') {

    				foreach ($checkContentDirs as $folder) {
    					$this->ftp->cd($this->remoteDir);
    					$cd = $this->ftp->cd($folder);
    					
    					if (true !== $cd) {
    						$this->ftp->mkdir($this->remoteDir . $folder, true);
    					}
    					    					
    					if($this->contentSendingMode == 'all') {
    						$this->log->log('Started copying Directory ' . $folder . ' to  ' . $this->ftpServer . ' title.', ezcLog::DEBUG, $this->logParams);
    						$upload = $this->ftp->putRecursive($this->homeDir . $folder, $this->remoteDir . $folder, true);
    					} else {
    						// if $this->contentSendingMode == 'updates'
    						
    						if ($handle = opendir($this->homeDir . $folder)) {
    							$this->log->log('New directory found.', ezcLog::DEBUG, $this->logParams);
    							while (false !== ($file = readdir($handle))) {
    								if($file != '.' && $file != '..') {
    									$fileToSend = $this->homeDir . $folder .  $file;
    									    									    									
    									if(!$sentFiles->getIsSentBefore($file)) {
    										
    										$this->log->log('Started copying file ' . $file . ' to  ' . $this->ftpServer . ' title.', ezcLog::DEBUG, $this->logParams);
    										$upload = $this->ftp->put($fileToSend, $this->remoteDir . $folder . $file,true);
    										
    										if(!PEAR::isError($upload)) {
    											$this->log->log("file $file  before", ezcLog::DEBUG, $this->logParams);
    											$sentFiles->addToSent($file);
    										} else {
    											$this->log->log("[failed]  can not copy file : $file ", ezcLog::DEBUG, $this->logParams);
    										}
    									} else {
    										$this->log->log('file sent before', ezcLog::DEBUG, $this->logParams);
    									}
    								} 
    							}	
    							closedir($handle);
    						}
    					}//End contentSendingMode == 'all'
    					
    					
    					if (PEAR::isError($upload)) {
                        	$this->processState = ezcLog::FAILED_AUDIT;
                        	$this->reportParams['SenderStatus'] = 0;
                        	$this->reportParams['SenderMsg'] 	= $upload->getMessage() . ' to ' . $this->ftpServer . ' title. Failed.';
                        	$this->report->updateReport($this->reportParams);
                        	$this->alert->send($upload->getMessage() . ' to ' . $this->clientName . ' Client. Failed.', syndAlert::CRITICAL, 'SERVER', $this->clientId, 'client');
                        	$this->log->log($upload->getMessage() 	 . ' to ' . $this->clientName . ' Client. Failed.', ezcLog::FATAL, $this->logParams);
                        } else {
                        	$this->alert->send('OK. New Content Found.', syndAlert::OK, 'SERVER', $this->clientId, 'client');
                        	$this->log->log('Copying Directory ' . ' to ' . $this->clientName . ' client. Succeeded.', ezcLog::DEBUG, $this->logParams);
                        }
                        
    				}//End foreach
    			    				
    			} else {
    				// $this->dirLayout == 'root'
    				
    				$days 		= $checkContentDirs;
                	$latestDay 	= array_shift($days);
                	$todayPath 	= $this->homeDir . $latestDay;
                	
                	foreach (scandir($this->homeDir . $latestDay) as $file) {
                		
                    	if ($file != '.' && $file != '..') {
							                    		
                        	$localFile  = substr($this->homeDir, 0, strrpos($this->homeDir, '/')) . $latestDay . $file;
             	
                        	if($this->contentSendingMode == 'all') {
                        		$this->log->log('Started copying File: ' . $file . ' to  ' . $this->ftpServer . ' title.', ezcLog::DEBUG, $this->logParams);
                        		$remoteFile = $this->remoteDir . DIRECTORY_SEPARATOR . pathinfo($todayPath . $file, PATHINFO_FILENAME) . '_' . date('dmY', time()) . '.' . pathinfo($todayPath . $file, PATHINFO_EXTENSION);                        	
								if(is_dir($localFile .'/')) {
									$upload = $this->ftp->putRecursive($localFile . '/', $this->remoteDir . DIRECTORY_SEPARATOR . pathinfo($todayPath . $file, PATHINFO_FILENAME) . '/', true);
								} else {
									$upload 	= $this->ftp->put($localFile, $remoteFile, true);
								}
                        	} else {
                        		
                        		//$this->contentSendingMode == 'updates'
                        		$remoteFile = $this->remoteDir . DIRECTORY_SEPARATOR . pathinfo($todayPath . $file, PATHINFO_FILENAME) . '.' . pathinfo($todayPath . $file, PATHINFO_EXTENSION);                        	
                        		if(!$sentFiles->getIsSentBefore($file)) {
                        			$this->log->log('Started copying File: ' . $localFile . ' to  ' . $this->ftpServer . ' title.', ezcLog::DEBUG, $this->logParams);
                        			if(is_dir($localFile)) {
                        				//the commited line was to send the folder with timeStamp but we recommend to sent all the file and the content of the file is the updated
                        				//$upload 	= $this->ftp->putRecursive($localFile . '/', $this->remoteDir . DIRECTORY_SEPARATOR . pathinfo($todayPath . $file, PATHINFO_FILENAME) . '_' . date('dmY', time()) .'/', true);
										
                        				$upload = $this->ftp->putRecursive($localFile . '/', $this->remoteDir . DIRECTORY_SEPARATOR . pathinfo($todayPath . $file, PATHINFO_FILENAME) . '/', true);
                      				} else {
                        				$upload = $this->ftp->put($localFile, $remoteFile, false);
                        			}
								} else {
                        			$this->log->log('File aleary sent.', ezcLog::DEBUG, $this->logParams);
                        		}
                        	}
                        	
                        	if (PEAR::isError($upload)) {
                            	$this->processState = ezcLog::FAILED_AUDIT;
                            	$this->reportParams['SenderStatus'] = 0;
                            	$this->reportParams['SenderMsg'] 	= $upload->getMessage() . ' to ' . $this->ftpServer . ' title. Failed.';
                            	$this->report->updateReport($this->reportParams);
                            	$this->alert->send($upload->getMessage() . ' to ' . $this->clientName . ' Client. Failed.', syndAlert::CRITICAL, 'SERVER', $this->clientId, 'client');
                            	$this->log->log($upload->getMessage() 	 . ' to ' . $this->clientName . ' Client. Failed.', ezcLog::FATAL, $this->logParams);
                            } else {
                           
								if(($this->contentSendingMode == 'updates')&& !is_dir($localFile)) {
                        			if(!$sentFiles->getIsSentBefore($file)) {
                            			$sentFiles->addToSent($file);
                        			}
                            	}
                            	$this->alert->send('OK. New Content Found.', syndAlert::OK, 'SERVER', $this->clientId, 'client');
                            	$this->log->log('Copying File ' . ' to ' . $this->clientName . " [{$this->ftpUsername}] " . ' client. Succeeded.', ezcLog::DEBUG, $this->logParams);
                        	}
                    	}
                	}
    			}
    		} else {
    			$this->log->log('No content found for ' . $this->clientName . ' Client.', ezcLog::WARNING, $this->logParams);
    		}
    		
    		if ($this->ftp->disconnect())
			$this->log->log('FTP Connection has been closed Normally.' . $this->clientName, ezcLog::SUCCESS_AUDIT, $this->logParams);
		else $this->log->log('FTP Connection has NOT been closed!!!!!' . $this->clientName, ezcLog::FAILED_AUDIT, $this->logParams);
    		$sentFiles->saveSettings();
   		}//End foreach connections
   		
   		return true;
    }
    /**
     * Close the Connection after uploading the content
     *
     */
    public function __destruct ()
    {
        if ($this->processState == ezcLog::FAILED_AUDIT) {
            $this->reportParams['SenderStatus'] = 0;
            $this->reportParams['EndTime'] = time();
            $this->reportParams['SenderMsg'] = 'Could not upload the Content.';
            $this->report->updateReport($this->reportParams);
            $this->log->log('Could not upload the Content.' . $this->clientName, ezcLog::FAILED_AUDIT, $this->logParams);
        } else {
            $this->reportParams['SenderStatus'] = 1;
            $this->reportParams['EndTime'] = time();
            $this->reportParams['SenderMsg'] = 'Uploaded the Content successfully.' . $this->ftpServer;
            $this->report->updateReport($this->reportParams);
            $this->log->log('Uploaded the Content successfully.' . $this->clientName, ezcLog::SUCCESS_AUDIT, $this->logParams);
        }
	if ($this->ftp)
	{
		mail('alaa@corp.albawaba.com, mohammad@syndigate.info',"FTP wasn't Closed", var_export($this->ftp,true));
        	$this->ftp->disconnect();
	}
    }
   
}
