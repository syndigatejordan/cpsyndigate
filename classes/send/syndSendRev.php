<?php
require_once dirname(__FILE__) . '/../../ez_autoload.php';
require_once 'Net/FTP.php';

class syndSendRev
{
    /**
     * Set the host URL
     *
     * @var string
     */
    protected $ftpServer;
    
    /**
     * Set the host post
     *
     * @var int
     */
    protected $ftpPort;
    
    /**
     * The host username
     *
     * @var string
     */
    protected $ftpUsername;
    
    /**
     * The host password
     *
     * @var string
     */
    protected $ftpUserPass;
    
    /**
     * The home directory
     *
     * @var sting
     */
    protected $homeDir;
    
    /**
     * The remote directory
     *
     * @var sting
     */
    protected $remoteDir;
    
    /**
     * creat an Object from the Model class
     *
     * @var object
     */
    protected $model;
    
 
    /**
     * Get the one and only instance of the ezcLog class
     *
     * @var object
     */
    protected $log;
    
    /**
     * Get the one and only instance of the syndAlert class
     *
     * @var object
     */
    protected $alert;
    
    /**
     * Object of the NET_FTP pear package
     *
     * @var object
     */
    protected $ftp;
    
    /**
     * The Title ID
     *
     * @var int
     */
    protected $clientId;
    
    /**
     * The Client Name
     *
     * @var int
     */
    protected $clientName;
    
    /**
     *creat an Object from the syndReport class
     *
     * @var object
     */
    protected $report;
    
    /**
     * The report ID for the gather_report table
     *
     * @var int
     */
    protected $reportId;
    
    /**
     * Store the report parameter
     *
     * @var array
     */
    protected $reportParams;
    
    /**
     * The status of the proccess
     *
     * @var string
     */
    protected $processState = ezcLog::SUCCESS_AUDIT;
    
    /**
     * Get the Directory Layout of this Client 
     *
     * @var string
     */
    protected $dirLayout;
    
    /*
     * client object
     *
     * @var Client
     */
    protected $client;
    
    /*
    * array parameters for the log
    *
    * @var array
    */
    protected $logParams;
    
    /**
     * try to resen the files with maximum number of retries, files more that will consedered as error in server 
     *
     * @var Integer
     */
    public $max_file_num_retries;
    
    public $titleId, $revision;
    
    /**
     * Constructor
     *
     */
    public function __construct () 
    {	
    	
    	$this->max_file_num_retries = 20; // can be overrider after the constructor for caller script
    	
    	
    	//Generating objects
        $this->log 			= ezcLog::getInstance();
        $this->alert 		= syndAlert::getInstance();
        $this->model 		= new syndModel();
        $this->ftp 			= new Net_FTP();
        
        $c = new Criteria();
        $c->add(ClientConnectionFilePeer::IS_SENT, 0);
        if($this->max_file_num_retries) {
        	$c->add(ClientConnectionFilePeer::NUM_RETRY , $this->max_file_num_retries, Criteria::LESS_EQUAL);
        }
        
        if($this->titleId) {
        	$c->add(ClientConnectionFilePeer::TITLE_ID , $this->titleId);
        }
        
        if($this->revision) {
        	$c->add(ClientConnectionFilePeer::REVISION, $this->revision);
        }
        
        $c->addAscendingOrderByColumn(ClientConnectionFilePeer::NUM_RETRY);
        $filesToSend = ClientConnectionFilePeer::doSelect($c);
        
        foreach ($filesToSend as $file) {
        	$this->processState = ezcLog::SUCCESS_AUDIT;
        	echo str_repeat(PHP_EOL, 5);
        	$this->copyContent($file);
        }
    }
    
   
    
    private function setSettingsFromConnectionFile(ClientConnectionFile $clientConnectionFile) {
    	
    	$this->clientId = $clientConnectionFile->getClientId();
    	$this->titleId  = $clientConnectionFile->getTitleId();
    	$this->revision = $clientConnectionFile->getRevision();
    	$this->files    = array($clientConnectionFile->getLocalFile());
    	$this->client 	= $this->model->getClientFields($this->clientId);
    	
    	$this->homeDir 		= $this->client->getHomeDir();
        $this->dirLayout	= $this->client->getDirLayout();
        $this->clientName 	= $this->client->getClientName();
        
        
        //Reset settings  
        $this->reportId     = '';
        $this->reportParams = array();
        $this->report 		= new syndReport('send');
        $generalReport      = '';
        
        //Joining reports 
        if($clientConnectionFile->getReportId()) {
        	$generalReport = ReportPeer::retrieveByPK($clientConnectionFile->getReportId());
			if(is_object($generalReport)) {
        	if($generalReport->getReportSendId()) {
        		$reportSend = ReportSendPeer::retrieveByPK($generalReport->getReportSendId());
        		if($reportSend) {
        			$this->reportId 		            = $reportSend->getId();
        			$this->reportParams['Id']           = $this->reportId;
        			//$this->reportParams['ClientId'] 	= $this->clientId;
        			//$this->reportParams['ClientId'] 	= 0; // it dose not relate to the client anymore
        			$this->reportParams['FilesCount'] 	= $reportSend->getFilesCount();
        			$this->reportParams['SenderStatus'] = $reportSend->getSenderStatus();
        			$this->reportParams['StartTime'] 	= $reportSend->getStartTime();
        			$this->reportParams['EndTime'] 		= $reportSend->getEndTime();
        			$this->reportParams['SenderMsg'] 	= $reportSend->getSenderMsg();	
        		} 
        	}
			}
        }
        
        if(!$this->reportId) {
        	
        	//$this->reportParams['ClientId'] 	= $this->clientId;
        	$this->reportParams['FilesCount'] 	= count($this->files);
        	$this->reportParams['SenderStatus'] = 0;
        	$this->reportParams['StartTime'] 	= time();
        	$this->reportParams['EndTime'] 		= 0;
        	$this->reportParams['SenderMsg'] 	= 'Preparing copying the Content to the Client FTP server.';
        	
        	$this->reportId 		  = $this->report->addReport($this->reportParams);
        	$this->reportParams['Id'] = $this->reportId;
        }
        
        if($clientConnectionFile->getReportId()) {
        	if(is_object($generalReport)) {
        		$generalReport->setReportSendId($this->reportId);
        		$generalReport->save();
        	}
        }
        
        $this->logParams = array('report_send_id' => $this->reportId, 'source' => 'send', 'category' => 'FTP Push');
        $this->log->log('Preparing copying the Content to the Client FTP server', ezcLog::DEBUG , $this->logParams);
    }
    
    /**
     * Connect to the Title's FTP server
     *
     */
    public function connect ()
    {
        //Note:  FTP URL MOST NOT HAVE [ FTP:// ] prefix		
        $connect = $this->ftp->connect($this->ftpServer, $this->ftpPort);
        
        if (PEAR::isError($connect)) {
            $this->processState = ezcLog::FAILED_AUDIT;
            $this->reportParams['SenderStatus'] = 0;
            $this->reportParams['SenderMsg'] 	= $connect->getMessage() . ' for: ' . $this->ftpServer;
            $this->report->updateReport($this->reportParams);
            $this->log->log($connect->getMessage() . ' for: ' . $this->clientName . 'with connection [' . $this->ftpUsername .']', ezcLog::FATAL, $this->logParams);
            $this->alert->send("Server is down.", syndAlert::CRITICAL, 'SERVER', $this->clientId, 'client');
        } else {
            $this->log->log('FTP connection to ' . $this->clientName . ' with connection : [' . $this->ftpUsername . '] Succeeded', ezcLog::INFO, $this->logParams);
        }
    }
    
    /**
     * Login to the Title's FTP server
     *
     */
    public function login ()
    {
        $this->connect();
        $login = $this->ftp->login($this->ftpUsername, $this->ftpUserPass);
	if($this->clientId == 112){ //PASSIVE MODE: Thomson Reuters- Emerging Businesses
          $this->ftp->setPassive();
        }
        if (PEAR::isError($login)) {
            $this->processState = ezcLog::FAILED_AUDIT;
            $this->reportParams['SenderStatus'] = 0;
            $this->reportParams['SenderMsg'] 	= $login->getMessage() . ' for: ' . $this->ftpServer;
            $this->report->updateReport($this->reportParams);
            $this->log->log($login->getMessage() . ' for: ' . $this->clientName . 'with connection [' . $this->ftpUsername .']', ezcLog::FATAL, $this->logParams);
            $this->alert->send("Server is down.", syndAlert::CRITICAL, 'SERVER', $this->clientId, 'client');
        } else {
            $this->log->log('FTP login to ' . $this->clientName . ' with connection : [' . $this->ftpUsername .  '] Succeeded', ezcLog::DEBUG, $this->logParams);
            $this->alert->send("Server is Up, we can upload the new content.", syndAlert::OK, 'SERVER', $this->clientId, 'client');
        }
    }
    
    /**
     * Login to the Title's SFTP server
     *
     */
    public function loginFTPS ()
    {
        // set up basic ssl connection
        /*$conn_id = ftp_ssl_connect('dropbox.yahoo.com');
        $login_result = ftp_login($conn_id, 'Saudi_Gazette', 'NuGt96Tk');
        var_dump($login_result);echo chr(10);exit;
        echo ftp_pwd($conn_id);exit;*/
        $this->connect();
        $out = $this->ftp->execute('AUTH TLS');
        $login = $this->ftp->login($this->ftpUsername, $this->ftpUserPass);
        if (PEAR::isError($login)) {
            $this->processState = ezcLog::FAILED_AUDIT;
            $this->reportParams['SenderStatus'] = 0;
            $this->reportParams['SenderMsg'] 	= $login->getMessage() . ' for: ' . $this->ftpServer;
            $this->report->updateReport($this->reportParams);
            $this->log->log($login->getMessage() . ' for: ' . $this->clientName . 'with connection [' . $this->ftpUsername .']', ezcLog::FATAL, $this->logParams);
            $this->alert->send("Server is down.", syndAlert::CRITICAL, 'SERVER', $this->clientId, 'client');
        } else {
            $this->log->log('FTP login to ' . $this->clientName . ' with connection : [' . $this->ftpUsername .  '] Succeeded', ezcLog::DEBUG, $this->logParams);
            $this->alert->send("Server is Up, we can upload the new content.", syndAlert::OK, 'SERVER', $this->clientId, 'client');
        }
    }
  
    
    /**
     * Copy the new content to the home directory of the title
     *
     */
    public function copyContent( ClientConnectionFile $file )
    {   
    	$file->setNumRetry( (int)$file->getNumRetry() + 1);
        $file->setIsSent(1);
    	$file->save();
    	
    	$this->setSettingsFromConnectionFile($file);
    	$conn = ClientConnectionPeer::retrieveByPK($file->getClientConnectionId());
      
    	$this->ftpServer 	= $conn->getUrl();
    	$this->ftpPort 		= $conn->getFtpPort();
    	$this->ftpUsername 	= $conn->getUsername();
    	$this->ftpUserPass 	= $conn->getPassword();
    	$this->remoteDir 	= $conn->getRemoteDir();
      
    	//login to the FTP server
      echo $this->clientId;
      if($this->clientId == 73){ // Custom for Yahoo client ID(73) Using SFTP connection.
        $this->loginFTPS();
      }else{
        $this->login();
      }
    	$upload = null;
    		
    	if (is_array($this->files)) {
    		
    		$this->log->log("files to be uploaded " . print_r($this->files, true), ezcLog::DEBUG , $this->logParams);

    		foreach ($this->files as $theFile) {
    			
    			if ($this->dirLayout == 'root') {    	
    				$this->log->log('directory layou is root', ezcLog::DEBUG , $this->logParams);
    				$remotePaths = array_slice( explode('/', $theFile), 3);
    			} else {
    				$this->log->log('directory layou is date', ezcLog::DEBUG , $this->logParams);
    				$remotePaths  = explode('/', $theFile);
    			}
    				
    			$remoteFile  = $this->remoteDir . DIRECTORY_SEPARATOR . implode($remotePaths, DIRECTORY_SEPARATOR);
    			$remoteFile  = str_replace(DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, $remoteFile);
    			$folder = dirname($remoteFile);
    			$cd     = $this->ftp->cd($folder);
    			
    			if (true !== $cd) {
    				$this->log->log("Directory " . $folder . " dose not exist", ezcLog::DEBUG , $this->logParams);
    				$rez = $this->ftp->mkdir($folder, true);
    				if($rez) {    						
    					$this->log->log("Dir $folder created", ezcLog::DEBUG, $this->logParams);
    				} else {
    					$this->log->log("Dir $folder could not be created", ezcLog::WARNING , $this->logParams);
    				}
    			}
    			
    			$this->log->log("Started copying file " . $this->homeDir . $theFile . ' to ' .$this->ftpServer . ' title : ' . $remoteFile , ezcLog::DEBUG, $this->logParams);			
    			$upload = @$this->ftp->put(str_replace(DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, $this->homeDir . $theFile), $remoteFile, true);    
    	
    			if (PEAR::isError($upload)) {
    				$this->processState = ezcLog::FAILED_AUDIT;
                    $this->reportParams['SenderStatus'] = 0;
                    $this->reportParams['SenderMsg'] 	= $upload->getMessage() . ' to ' . $this->ftpServer . ' title. Failed.';
                    $this->report->updateReport($this->reportParams);
                    
                    $this->alert->send($upload->getMessage() . ' to ' . $this->clientName . ' Client. Failed.', syndAlert::CRITICAL, 'SERVER', $this->clientId, 'client');
                    $this->log->log($upload->getMessage() 	 . ' to ' . $this->clientName . ' Client. Failed.', ezcLog::FATAL, $this->logParams);
                    
                    $file->setErrMsg($upload->getMessage());
                    $file->setIsSent(0);
                    $file->save();
                    
    			} else {
                    $this->alert->send('OK. New Content Found.', syndAlert::OK, 'SERVER', $this->clientId, 'client');
                    $this->log->log('Copying Directory ' . ' to ' . $this->clientName . ' client. Succeeded.', ezcLog::DEBUG, $this->logParams);
                    
                    $file->setIsSent(1);
					$file->setSentTime(time());
                    $file->save();
    			}
    		}
    	} else {
            $file->setIsSent(0);
            $file->save();
    		$this->log->log('No content found for ' . $this->clientName . ' Client.', ezcLog::WARNING, $this->logParams);
    	}
    	
	$this->log->log('FTP CONNECTION BEFORE DISCONNECT. for client: ' . $this->clientName . '\n\n' . var_export($this->ftp, true), ezcLog::DEBUG, $this->logParams);	
    	$this->ftp->disconnect();

	if ($this->ftp) {
        	$this->log->log('FTP CONNECTION STILL ALIVE. for client: ' . $this->clientName, ezcLog::DEBUG, $this->logParams);
                //mail('alaa@corp.albawaba.com, mohammad@syndigate.info', "FTP CONNECTION STILL ALIVE", $this->clientName . "\n\n" . var_export($this->ftp,true));
             	//$this->ftp->disconnect();
        } else {
		//mail('alaa@corp.albawaba.com, mohammad@syndigate.info', "FTP CONNECTION CLOSED SUCCESSFULLY", $this->clientName . "\n\n" . var_export($this->ftp,true));
          	$this->log->log('FTP connection closed successfully.' . $this->clientName, ezcLog::DEBUG, $this->logParams);
	}

    	
    	if ($this->processState == ezcLog::FAILED_AUDIT) {
            $this->reportParams['SenderStatus'] = 0;
            $this->reportParams['EndTime'] = time();
            $this->reportParams['SenderMsg'] = 'Could not upload the Content.';
            $this->report->updateReport($this->reportParams);
            $this->log->log('Could not upload the Content.' . $this->clientName, ezcLog::FAILED_AUDIT, $this->logParams);
        } else {
            $this->reportParams['SenderStatus'] = 1;
            $this->reportParams['EndTime'] = time();
            $this->reportParams['SenderMsg'] = 'Uploaded the Content successfully.' . $this->ftpServer;
            $this->report->updateReport($this->reportParams);
            $this->log->log('Uploaded the Content successfully.' . $this->clientName, ezcLog::SUCCESS_AUDIT, $this->logParams);
        }
        
    	return true;
    }
	public function __destruct()
	{	
		if ($this->ftp)
		{
			$this->log->log('FTP CONNECTION STILL ALIVE. for client: ' . $this->clientName, ezcLog::FAILED_AUDIT, $this->logParams);
			mail('khaled@corp.albawaba.com', "FTP CONNECTION STILL ALIVE", $this->clientName . "\n\n" . var_export($this->ftp,true));
			$this->ftp->disconnect();
		}
		else 
			$this->log->log('FTP connection closed successfully.' . $this->clientName, ezcLog::SUCCESS_AUDIT, $this->logParams);
	}
   
}
