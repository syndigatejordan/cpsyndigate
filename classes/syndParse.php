<?php
	define('OUTPUT_CHARSET', 'UTF-8');
	require_once dirname(__FILE__).'/../'.'ez_autoload.php';
	require_once APP_PATH . 'conf/pheanstalk.conf.php';
	

	class syndParse
	{
		private $revision;
		private $titleId;
		private $rowId;
		private $log;
		private $memcache, $memcacheConnected = false;
		private $generalReport;
		
		public function __construct($params)
		{
			$this->log = ezcLog::getInstance();
			$this->rowId = $params['rowId'];
			//$this->log->log("Parser called. RowID [{$this->rowId}]", ezcLog::DEBUG, array('source' => 'parse') );
		}

		public function process()
		{
			try {
				$model = syndModel::getModelInstance();
				
				$parsingState = $model->getParsingStateById($this->rowId);
				
				if($parsingState) {
					$this->titleId  = $parsingState->getTitleId();
					$this->revision = $parsingState->getRevisionNum();
					
					//get the general report 
					$c = new Criteria();
					$c->add(ReportPeer::TITLE_ID , $parsingState->getTitleId());
					$c->add(ReportPeer::REVISION_NUM, $parsingState->getRevisionNum());
          $c->addDescendingOrderByColumn(ReportPeer::ID);
					$this->generalReport = ReportPeer::doSelectOne($c);
					
				} else {
					//$this->log->log("No parsing state record with title id : " . $this->titleId . " & revision : " . $this->revision, ezcLog::ERROR , array('source' => 'parse') );
					throw new Exception('Porcess state does not exists.');
				}
				
				$svn   = new syndSvnDataGathering($this->titleId);
				$files = $svn->update($this->revision);
				if(empty($files)) {
					// Re processing revision
					$files = $svn->getRevLog($this->revision);
				}
				
				$parserClass = "syndParser_{$this->titleId}";
				require_once dirname(__FILE__)."/parse/custom/$parserClass.php";
				$model->parseStartUpdateState($this->rowId);
				$parser      = new $parserClass($files, $model->getTitleById($this->titleId), $this->revision);
				$parser->beforeParse();
				$parser->addLog("Files to parse" . print_r($files, true));
				$articles    = $parser->parse();
				$parser->afterParse(array('articles' => &$articles));
				$parser->addLog("After parse function finished");
			


			
				$articlesIds = array();
				$contentChanged =false;
				
				$this->memcache 	= new Memcache;	
				try {	
					$this->memcacheConnected = $this->memcache->connect(MEMCACHE_HOST, MEMCACHE_PORT);
				} catch (Exception $e) {
					$parser->addLog("Could not connect to memcachd server", ezcLog::ERROR);
				}
				
				foreach($articles as $article) {
					$memcachdOriginalArticleId = '';
					$memcachd_check_sum = $article['title_id'] . '_' . sha1($article['headline']) . sha1($article['story']);
					
					if($this->memcacheConnected) {
						if($this->memcache->get($memcachd_check_sum)) {
							$parser->addLog("article original key found in memcached ... ignore");
							continue;
						} else {
							//$this->log->log("Not found in article original key ...", ezcLog::DEBUG, array('source' => 'parse') );
						}
					}
					
					// Set the revision of the article
                    $article['original_data']['revision_num']   = $this->revision;
                    
                    //check if not the category exist create new one and assing the iptc to nothing
					if(!$model->isOriginalArticleCategory($this->titleId, $article['original_data']['original_category'])) {
						$model->addOriginalArticleCategory($this->titleId, $article['original_data']['original_category']);
					}
					
					//set the original category id
					$article['original_data']['original_cat_id'] = $model->getOriginalCategoryId($article['original_data']['original_category'], $this->titleId);

					
					// add original articles
					$originalArticleData    = $model->getArticleByOriginalId($article['original_data']['original_article_id']); // get original article
					$oldOriginalArticleData = $model->getArticleByOriginalId($article['original_data']['old_original_article_id']);				

					$articleObj = null;
					if($originalArticleData) {
						$originalId  = $originalArticleData->getId();
						$articleObj  = $model->getArticleByOriginalDataId($originalId);
						if($articleObj) {
							$parser->addLog("article original key found by new key with article id " . $articleObj->getId(), ezcLog::DEBUG);
                       } else {
                       }

					} else {
						if($oldOriginalArticleData) {
							$originalId  = $oldOriginalArticleData->getId();
							$articleObj  = $model->getArticleByOriginalDataId($originalId);
							if($articleObj) {
								$parser->addLog("Found by old key with article id " . $articleObj->getId());
							}	
						} else {
							echo "no found " . $article['headline'];
						}

					} 
					
					
					if((!$originalArticleData && !$oldOriginalArticleData) || !$articleObj) {
						$parser->addLog("Add article with original id [".$article['original_data']['original_article_id']."]");
						if(!$originalArticleData) {
							$originalId = $model->addOriginalArticle($article['original_data']);
						} else {
							$model->updateOriginalArticle($originalId, $article['original_data']);
						}
						// add article
						$article['original_data_id'] = $originalId;
						$articleId     = $model->addArticle($article);
						$articlesIds[] = $articleId;						
						$parser->addLog("article added ...  ($articleId) with orginal " . $article['original_data']['original_article_id']);
						if($this->memcacheConnected) {
							$this->memcache->set($memcachd_check_sum, 1, false, 864000);
						}
						$contentChanged = true;
					} else {
						$parser->addLog("Update article with original id [".$article['original_data']['original_article_id']."]");
						$model->updateOriginalArticle($originalId, $article['original_data']);

						// update article
						$article['original_data_id'] = $originalId;
						$articleId  	= $articleObj->getId();
						$updated    	= $model->updateArticle($articleId, $article);
						$contentChanged	= !$updated	? $contentChanged : true;
						
						if($updated) {
							echo "Updated ... ($articleId) with orginal " . $article['original_data']['original_article_id'] . PHP_EOL;	
							$articlesIds[] = $articleId;
							if($this->memcacheConnected) {
								$this->memcache->set($memcachd_check_sum, 1, false, 864000);
							}
						} else {
							echo "Not updated ... ($articleId) with orginal " . $article['original_data']['original_article_id'] . PHP_EOL;	
						}
						
					}
				

					//update article videos 
					foreach ($article['videos'] as $video) {
						$model->addOrUpdateArticleVideo($articleId, $video);
					}
					
					//update articel images
					foreach ($article['images'] as $image) {
						$model->addOrUpdateArticleImage($articleId, $image);
					}

					//$articlesIds[] = $articleId;
				}
				
				
				//Update title and set it to has new content if available								
				if($contentChanged) {
					
					$parser->title->setHasNewParsedContent(1);
					$parser->title->setLastContentTimestamp(time());

					// Updating cahced articles to improve performance to the frontend list titles page
				//	$parser->title->setCachedArticlesToday((int)$parser->title->getArticlesToday());
			//		$parser->title->setCachedCurrentSpan((int)$parser->title->getArticlesSpanCount(0));
			//		$parser->title->setCachedPrevSpan((int)$parser->title->getArticlesSpanCount(-1, -1));

					$parser->title->save();
                    if (count($articlesIds) < 200) {
                        require_once APP_PATH . 'lib/pheanstalk/pheanstalk_init.php';
                        $pheanstalk = new Pheanstalk(PHEANSTALK_HOST . ':' . PHEANSTALK_PORT);
                        $jobData = json_encode(array(
                                'articles_ids' => $articlesIds,
                                'title_id' => $parser->title->getId(),
                                'timestamp' => time(),
                            )
                        );
                        $pheanstalk->useTube(POST_PROCESS_ARTICLES_TUBE)->put($jobData, 100);
                    }
				}
				
				//updating parser report
				if(count($articlesIds) > 0) {
					$parser->parseReport['ArticlesIds'] = implode(',', $articlesIds);
					$parser->parseReport['ArticleNum']  = count($articlesIds);
					$model->parseUpdateReport($parser->parseReport);
					$parser->addLog("starting generation");

					$r = '';
					if(is_object($this->generalReport)) {
						$r = ' report_id=' . $this->generalReport->getId();
					}
					
					//$cmd = 'nohup /usr/bin/php /usr/local/syndigate/classes/syndGenerateRevDriver.php ' . $this->titleId . ' ' . $this->revision . ' ' . $r . '  > /dev/null &';
              $cmd = 'nohup /usr/bin/php /usr/local/syndigate/classes/syndGenerateRevDriver.php title_id=' . $this->titleId . ' revision=' . $this->revision . ' ' . $r . '  > /dev/null &';
					//$parser->addLog($cmd);
					//exec($cmd);

					require_once APP_PATH . 'lib/pheanstalk/pheanstalk_init.php';
					$pheanstalk = new Pheanstalk(PHEANSTALK_HOST . ':' . PHEANSTALK_PORT);
					$jobData        = json_encode(array(
										'title_id' => $this->titleId,
										'revision'=>  $this->revision,
										'report_id'=> $r,
										'timestamp'    =>time(), 
									)
								);
					@$pheanstalk->useTube('generate_driver')->put($jobData, 100);
					$parser->addLog("finsihed generation");
				} else {
					$parser->addLog("No articles added in this process");
				}
				
				$model->parseFinishUpdateState($this->rowId);
				
				
				

			} catch (Exception $e) {
				$parser->addLog($e->getMessage(), ezcLog::ERROR);
				//$this->log->log($e->getMessage(), ezcLog::ERROR, array('source' => 'parse'));
			}
		}

	}
