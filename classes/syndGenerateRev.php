<?php
    require_once dirname(__FILE__).'/../ez_autoload.php';

	////////////////////////////////////////////////////////////////////////////////////////
	// configuraion
	///////////////////////////////////////////////////////////////////////////////////////
	//defined('COPYRIGHT')      OR define('COPYRIGHT', " Provided by <a href='http://syndigate.info'>Syndigate.info</a>, an <a href='http://albawaba.com'>Albawaba.com</a> company");
defined('COPYRIGHT')      OR define('COPYRIGHT', " Provided by SyndiGate Media Inc. (<a href='http://syndigate.info'>Syndigate.info</a>).");
	defined('STRIPPING_TAGS') OR define('STRIPPING_TAGS', false);
	//defined('TMP_DIR')        OR define('TMP_DIR', APP_PATH.'tmp/');
	// Adding by Khaled for TMP solution to fix I/O issue.
	defined('TMP_DIR')        OR define('TMP_DIR', APP_PATH.'tmp_xml_generate/');		
  
	class syndGenerateRev
	{
		static public $DB_NAME;
		static public $DB_USER;
		static public $DB_PASS;
		static public $DB_HOST;
		
		private $model, $ModelMongo;
		private $tmpFiles, $filesToSend = array(), $tmpFilesCount = array();
		private $timeStamp, $version;
                public $clientsIds = array() ,$force_resend = 0;
		
		private $report, $reportId, $reportParams, $log, $logParams;
		
		public function __construct( $timeStamp='', $reportId = 0, $argv = '' )
		{
                        $argv = implode(',',$argv);
			$this->model      = syndModel::getModelInstance();
			$this->ModelMongo = syndModelMongo::getInstance(); 
				
			$this->report    				= new syndReport('generate');
			$this->reportParams['Error'] 	= 'Starting generate';
			$this->reportParams['Status']   = 0; // Default error until set the success msg
			$this->reportId                 = $this->report->addReport($this->reportParams);
			$this->reportParams['Id']		= $this->reportId;
			
			$this->log 			= ezcLog::getInstance();
			$this->logParams 	= array('report_generate_id' => $this->reportId, 'source' => 'generate');
			
			//Joining the report generate with report table 
			if($reportId) {
				$this->generalReportId = $reportId;
				$report = ReportPeer::retrieveByPK($reportId);
				if($report) {
					$this->log->log("I have General Report ID: $reportId", Ezclog::DEBUG, $this->logParams);
					$report->setReportGenerateId($this->reportId);
					$report->save();
				}else{
					$this->log->log("I DON't have OBJECT FOR General Report ID: $reportId", Ezclog::DEBUG, $this->logParams);
				}
			}else{
                                       // $this->log->log("ARGV: $argv", Ezclog::DEBUG, $this->logParams);
					$this->log->log("I DON't have General Report ID: $reportId FOR $argv", Ezclog::DEBUG, $this->logParams);
			}
			
			$propelConf  = Propel::getConfiguration();
			$dbConf 	 = $propelConf['datasources']['propel']['connection'];
			
			self::$DB_USER = $dbConf['username'];
			self::$DB_PASS = $dbConf['password'];
			self::$DB_HOST = $dbConf['hostspec'];
			self::$DB_NAME = $dbConf['database'];			

			if($timeStamp) {
				$this->timeStamp  = $timeStamp;
			} else {
				$this->timeStamp  = time();
			}
			
			$this->version = date('dhis');
		}
		
		
		public function generateByRev($titleId, $revision) {
			
			$this->reportParams['TitleId'] = $titleId;
			$this->report->updateReport($this->reportParams);
			
			$this->log->log("Starting generating process for title $titleId with revision $revision ", Ezclog::DEBUG, $this->logParams);
			
			$this->titleId 	= $titleId;
			$this->revision	= $revision;
			
			$c = new Criteria();
			$c->add(ParsingStatePeer::TITLE_ID, $titleId);
			$c->add(ParsingStatePeer::REVISION_NUM, $revision);
                     $c->addDescendingOrderByColumn(ParsingStatePeer::ID);
			$parsingState = ParsingStatePeer::doSelectOne($c);
			if(!$parsingState) {
				$msg = "No parsing state records for title $titleId and revision $revision";
				$this->reportParams['Error'] = $msg;
				$this->report->updateReport($this->reportParams); 
				$this->log->log($msg, Ezclog::ERROR, $this->logParams);
				die($msg . PHP_EOL);
			} else {
				$this->log->log("Getting parsing state with id : " . $parsingState->getId(), EzcLog::DEBUG , $this->logParams);
			}
			
			$reportParse = ReportParsePeer::retrieveByPK($parsingState->getReportPArseId());
                   
			if(!$reportParse) {
				$msg = 'No report parse with id ' . $reportParse->getReportPArseId() ;
				$this->reportParams['Error'] = $msg; 
				$this->report->updateReport($this->reportParams);
				$this->log->log($msg, Ezclog::ERROR, $this->logParams);
				die($msg . PHP_EOL);
			} else {
				$this->log->log("Getting report parse with id : " . $reportParse->getId(), EzcLog::DEBUG , $this->logParams);
			}
			
			$template = syndGenerateTemplatesManager::factory('NewsMl');
		
			// Regenerate the file that contain all articles and its name will be title_id.xml
			$filter	= array();
			$titles   = $this->getArticlesTree($filter, 'all'); 
                     $mic_titles = $titles;
                     $mrss_titles = $titles;
			$file     = $template->generate($titles);
		
			if(is_array($titles) && count($titles) > 0) {
				$articlesCount  = (int) $titles[$titleId]['articles']->getCount();
				$this->tmpFilesCount['all'] = $articlesCount;
				$this->log->log("File $file generated containing all articles for this day ( $articlesCount articles )", EzcLog::DEBUG, $this->logParams);
			} else {
				$this->tmpFilesCount['all'] = 0;
				$this->log->log("No articles found for this day, No file generated with default filters ( today's timestamp )", EzcLog::DEBUG, $this->logParams);
			}
			
			// Generate a file contain just the updates and its name will be title_id_*********.xml
			$filter  		= array();
			$titles   		= $this->getArticlesTree($filter, 'updates', $reportParse->getArticlesIds());
			$updatesFile 	= $template->generate($titles);
			
			if(!isset($titles[$titleId])) {
				$this->log->log("No articles was found for this revision.", EzcLog::DEBUG, $this->logParams);
				return false;
			} else {
				$this->tmpFilesCount['updates'] = (int) $titles[$titleId]['articles']->getCount();
			}
			
                    $mic_template = syndGenerateTemplatesManager::factory('NewsMlMicrosoft');
                    $mrss_template = syndGenerateTemplatesManager::factory('MRSS');
      
			$microsoft_file     = $mic_template->generate($mic_titles);
			$mrss_file     = $mrss_template->generate($mrss_titles);
      
			$articlesCount  = (int) $titles[$titleId]['articles']->getCount(); 
			$this->log->log("File $updatesFile generated containing just the updated articles for revision $revision  ( $articlesCount article )", EzcLog::DEBUG, $this->logParams);
			$this->reportParams['ArticlesCount'] = $articlesCount;
			$this->report->updateReport($this->reportParams);
				
			$this->tmpFiles['all'] = $file;
			$this->tmpFiles['updates'] = $updatesFile;			
			$this->tmpFiles['microsoft_all'] = $microsoft_file;			
			$this->tmpFiles['mrss_all'] = $mrss_file;			
			chmod($file, 0775);
			chmod($updatesFile, 0775);
			chmod($microsoft_file, 0775);
			chmod($mrss_file, 0775);
			
			if( $this->copyFilesToClientsDir() ) {
				$this->clearTmp();
				$this->reportParams['Error']  = "Finished Generating";
				$this->reportParams['Status'] = 1;
				$this->report->updateReport($this->reportParams);	
				$this->log->log('Finsied generating ', ezcLog::DEBUG , $this->logParams);
			} else {
				$this->log->log('Finsied generating with Errors ', ezcLog::DEBUG , $this->logParams);
			}
			
			$this->addGeneratedFilesTobeSend($this->filesToSend);
			return $this->filesToSend;

		}
		
		private function clearTmp() {
			
			if(is_array($this->tmpFiles)) {
				foreach($this->tmpFiles as $file) {
					unlink($file);
				}
			}
		}
	
		
		/**
		 * Prepares the dir to write files to, and returns the full path
		 *
		 * @param string $clientHomeDir
		 */
		private function prepareDir($clientHomeDir) {
			
			if($this->timeStamp) {
				$ftpDir = rtrim($clientHomeDir, '/').'/'.date('Y/m/d/', $this->timeStamp);
			} else {
				$ftpDir = rtrim($clientHomeDir, '/').'/'.date('Y/m/d/');
			}
			
			if(!is_dir($ftpDir)) {
				$rez = mkdir($ftpDir, 0755, true);
				if(!$rez) {
					$this->log->log("Could not create client directory $ftpDir", ezcLog::ERROR, $this->logParams);
				} else {
					$this->log->log("Directory created $ftpDir", ezcLog::DEBUG, $this->logParams);
				}
			}			
			return $ftpDir;
		}

		

		/**
		* get articles sql where statement for client filters
        *
        * @param $filters array
        * @return string sql where statement but without "WHERE"
        */
        private function getArticleFilters($filters=array())
		{
			if(!count($filters)) {
				$filters = array(array());
			}
			
			$articlesRules   = array();
			
			foreach($filters as $filter) {
				
				$articlesFilters = array();
				if(!is_array($filter)) {
					$filter = array();
				}
				
				if($this->timeStamp) {
					$filter['date'] = date('Y-m-d', $this->timeStamp);
				}
				
				$filter = array_merge(array('category'=>'', 'date'=>''), $filter);
				
				if($filter['category']) {
					$articlesFilters[] = ArticlePeer::IPTC_ID." IN ({$filter['category']})";	// get client article catigory filter
				}
				
				if($filter['date']) {
					
					$minDate = date('Y-m-d H:i', strtotime($filter['date']));
                    $minDate = date('Y-m-d H:i', strtotime("$minDate - 6 hours")); // By Khaled Abbad GET anything added after 18PM Yesterday articles for ALL
                    $maxDate = date('Y-m-d H:i', strtotime($filter['date'].' 23:59'));		// get client article date filter
                    $articlesFilters[] = ArticlePeer::PARSED_AT. " >= '$minDate'";
                    $articlesFilters[] = ArticlePeer::PARSED_AT. " <= '$maxDate'";
				}
				
				if(count($articlesFilters)) {
					$articlesRules[] = '('.implode(' AND ', $articlesFilters).')';			// mirge all client filters in one rule
				}
				
			} // End foreach

			
			if(count($articlesRules)) {
				return ' ('.implode(' OR ', $articlesRules).')';
			}
			return '';
			
		} // End function getArticleFilters
		
		
		
		/**
		 * get all title id from article table which parsed in specific date
		 * and put the results in tree
		 *
		 * @param string $date
		 * @return array
		 */
		private function &getArticlesTree($filters=array(), $mode = 'updates',$article_ids='')
		{			
			$conn = mysql_connect(self::$DB_HOST, self::$DB_USER, self::$DB_PASS, true) or die("\n\nCannot connect to database\n\n");
			mysql_select_db (self::$DB_NAME) or die("\n\nDatabase doesn't exists\n\n");
	
	
			$articlesRules = $this->getArticleFilters($filters);
			
			$sql =  'SELECT * FROM '.TitlePeer::TABLE_NAME . ' WHERE id = ' . $this->titleId ;
			$titlesResult 	= mysql_query($sql, $conn);
			while ($titleRow = mysql_fetch_assoc($titlesResult)) {
								
				$id = $titleRow['id'];
				$titles[$id]['id']           = $id;
				$titles[$id]['providerName'] = $titleRow['name'];
				$titles[$id]['copyright']    = $titleRow['copyright'];
				$titles[$id]['webSite']      = $titleRow['website'];
				
				// get title's articles
				$sql = 'SELECT article.*, original_article_category.cat_name, article_original_data.reference FROM article left join article_original_data on article_original_data.id = article.article_original_data_id left join original_article_category on article_original_data.original_cat_id = original_article_category.id WHERE ' . ArticlePeer::TITLE_ID.' = '.$id;
                                if(!empty($article_ids)){
                                    $sql .=  ' AND article.id in('.$article_ids.')';
                                }
				if($mode == 'updates') {					
					$sql .=  ' AND ' . ArticleOriginalDataPeer::REVISION_NUM  . ' = ' . $this->revision ;
					$sql .=  ' AND ' . ArticlePeer::PARSED_AT . ' > ' .  "'" . date('Y-m-d',  strtotime('-30 days') ) .  "'";
				} else {
					
					if($articlesRules) {
						$sql .= ' AND '. $articlesRules;
					}
				}
				$this->log->log($sql, ezcLog::DEBUG , $this->logParams);	
				$articlesResult = mysql_query($sql, $conn) ;
				$titles[$id]['articles']  = new syndGenerateMySqlIterator($articlesResult, new ArticleArrayFormater($this->model));
				
				if(!mysql_num_rows($articlesResult)) {
					unset($titles[$id]);
				}

			}
			
			return $titles;
		}
		
		
		/**
		 * Coping the tmp files to clients directories
		 *
		 * @return true if every thing goes right false otherwise
		 */
		protected function copyFilesToClientsDir() {
			
			$this->log->log('starting copying files to clients directories ', EzcLog::DEBUG , $this->logParams);
			
			if(!empty($this->clientsIds)) {
                            $clients = ClientPeer::retrieveByPKs($this->clientsIds);
                        } else {
                            $clients   = TitlesClientsRulesPeer::getTitleClients($this->titleId);
                        }
			
			$hasErrors = 0;
			
			foreach ($clients as $client) {
				
				if(!$client->getIsActive()) {
					$this->log->log("Client # " . $client->getId() . ' --' . $client->getClientName() . ' is inactive, ignoring copying file', ezcLog::DEBUG , $this->logParams);
					continue;
				}
				
				if('entitied_newsml' == $client->getXmlType()) {					
					$this->log->log("Client # " . $client->getId() . ' --' . $client->getClientName() . ' has another format (entitied_newsml) and will be sent from the entitied application ', ezcLog::DEBUG , $this->logParams);
					continue;
				}

			
                                
                                if(!$this->force_resend) {
                                       
                                   $sentBefore = new Criteria();
                                   $sentBefore->add(ClientFilePeer::TITLE_ID, $this->titleId);;
                                   $sentBefore->add(ClientFilePeer::CLIENT_ID, $client->getId());
                                   $sentBefore->add(ClientFilePeer::REVISION, $this->revision);
                                   $sentBefore->add(ClientFilePeer::CREATION_TIME, strtotime("-2 day"), Criteria::GREATER_EQUAL);
                                   $file_before = ClientFilePeer::doSelectOne($sentBefore);
                                   
                                   if($file_before) {
                                        $this->log->log('The file ' . $file_before->getLocalFile() . ' is sent before' , ezcLog::DEBUG , $this->logParams);
                                        continue;
                                   }
                                   
                                }
                                 
                                
                                
				$ftpDir = $this->prepareDir($client->getHomeDir());
				
				if($client->getGenerateType() == 'updates') {
					
					$fromFile   = $this->tmpFiles['updates'];
					$targetFile = $ftpDir . $this->titleId . '_' . sprintf("%08s", $this->revision ) .'.xml';

					if(!$this->tmpFilesCount['updates']) {
						$this->log->log("File $targetFile will not be send, articles count is 0", ezcLog::DEBUG , $this->logParams);
						continue;
					}
					
				} else {
					
          
					$fromFile   = $this->tmpFiles['all'];
          if('microsoft_newsml' == $client->getXmlType()) {
            $fromFile   = $this->tmpFiles['microsoft_all'];  
          }
          if('mrss' == $client->getXmlType()) {
            $fromFile   = $this->tmpFiles['mrss_all'];  
          }
					$targetFile = $ftpDir . $this->titleId . '.xml';
					
					if(!$this->tmpFilesCount['all']) {
						$this->log->log("File $targetFile will not be send, articles count is 0", ezcLog::DEBUG , $this->logParams);
						continue;
					}
				}
				
				$clientPullFile = explode($client->getHomeDir(), $targetFile);
				$clientPullFile = $clientPullFile[1];	
								
				$rez = copy($fromFile, $targetFile);
                                chown($targetFile, $client->getFtpUsername());
                                chgrp($targetFile, $client->getFtpUsername());
                                chmod($targetFile, 0777);

				if(!$rez) {
					$hasErrors = 1;
					$this->log->log("Could not copy $fromFile to $targetFile", ezcLog::ERROR , $this->logParams);
				} else {
					$this->log->log("Copied : $fromFile to $targetFile", ezcLog::DEBUG, $this->logParams);
					
					if($client->getGenerateType() == 'updates') {
						$clientFile = new ClientFile();
						$clientFile->setClientId($client->getId());
						$clientFile->setTitleId($this->titleId);
						$clientFile->setRevision($this->revision);
						$clientFile->setLocalFile($clientPullFile);
						$clientFile->setReportId((int)$this->generalReportId);
						$clientFile->setCreationTime(time());
						$clientFile->setArticleCount($this->tmpFilesCount['updates']);
						$clientFile->save();
					} else {
						
						//get if there an existing record for today report to update it
						$todaytimeStamp = strtotime(date('Y-m-d', $this->timeStamp));
						$c = new Criteria();
						$c->Add(ClientFilePeer::CLIENT_ID, $this->titleId);
						$c->Add(ClientFilePeer::CREATION_TIME, $todaytimeStamp, Criteria::GREATER_EQUAL);
						$c->AddAND(ClientFilePeer::CREATION_TIME, $this->timeStamp, Criteria::LESS_THAN);
						$clientFile = ClientFilePeer::doSelectOne($c);
						
						if(!is_object($clientFile)) {
							$clientFile = new ClientFile();							
						}
						$clientFile->setClientId($client->getId());
						$clientFile->setTitleId($this->titleId);
						$clientFile->setRevision($this->revision);
						$clientFile->setLocalFile($clientPullFile);
						$clientFile->setReportId((int)$this->generalReportId);
						$clientFile->setCreationTime(time());
						$clientFile->setArticleCount($this->tmpFilesCount['all']);
						$clientFile->save();
						
					}
					
					if($client->getConnectionType() == 'ftp_push') {
						$slugs = explode($client->getHomeDir(), $targetFile);
						$this->filesToSend[$client->getId()] = $slugs[1];
					}
				}
					
				
				$afterGenerateFile = dirname(__FILE__) . '/generate/customClient/syndGenerate_' . $client->getId() . '.php';
					
				if(file_exists($afterGenerateFile)) {
					
					$this->log->log("Calling after generate file $afterGenerateFile", ezcLog::DEBUG , $this->logParams);
					
					require_once $afterGenerateFile;
					$params    = array("ftpDir"=> $ftpDir, "generateType"=>$client->getGenerateType());
					$className = "syndGenerate_" . $client->getId();
					$obj       = new $className($params);
					$obj->afterGenerate();
					
					if($client->getConnectionType() == 'ftp_push') {
						if($obj->files[0]) {
							$slugs = explode($client->getHomeDir(), $obj->files[0]);
							$this->filesToSend[$client->getId()] = $slugs[1] ;
						}
					}
					
					$this->log->log("after generate function finsihed", ezcLog::DEBUG , $this->logParams);
				}
			}
			
			return $hasErrors ? 0 : 1;
		}
		
		
		public function addGeneratedFilesTobeSend($files = array()) {
			
    		if( $files ) {
				$this->log->log("files to be send" . print_r($files, true), ezcLog::DEBUG , $this->logParams);
    	
    			// Insert files to the database report so if an error happened in the send the files will not be lost 
    			foreach ($files as $clientId => $file) {
    				$client 	 = ClientPeer::retrieveByPK($clientId);
    				$connections = $client->getClientConnections();
    		
    				foreach ($connections as $connection) {
    					$connectionFile = new ClientConnectionFile();
    			
    					if(isset($this->generalReportId)) {
    						$connectionFile->setReportId($this->generalReportId);
    					}
    					$connectionFile->setClientId($clientId);
    					$connectionFile->setClientCOnnectionId($connection->getId());
    					$connectionFile->setTitleId($this->titleId);
    					$connectionFile->setRevision($this->revision);
    					$connectionFile->setLocalFile($file);
						$connectionFile->setCreationTime(time());
    					$connectionFile->save();
    				}
    			}
    		} else {
				$this->log->log("No files to be send to FTP account", ezcLog::DEBUG , $this->logParams);
			}
		}

	} // End class syndGenerate

	
	class ArticleArrayFormater implements syndGenerateArrayFormater
	{
		private $model;
		public function __construct($model)
		{
			$this->model = $model;
		}

		private function getLanguage($langId)
		{
			static $languages = array();
			if(isset($languages[$langId])) {
				return $languages[$langId];
			}
			$lang = $this->model->getLanguage($langId);
			if($lang) {
				$languages[$lang->getId()] = $lang->getName();
			}
			return ($lang ? $lang->getName() : '');
                }
		
		public function format($articleRow)
		{
			$artId = $articleRow['id'];
			
			$returnVal['id'] = $artId;
			$returnVal['headline'] 		= $articleRow['headline'];
			$returnVal['byLine']   		= $articleRow['author'];
			$returnVal['language'] 		= $this->getLanguage($articleRow['language_id']);
			$returnVal['category'] 		= $articleRow['iptc_id'];
			$returnVal['abstract'] 		= $articleRow['summary'];
			$returnVal['text'] 			=& $articleRow['body'];
			$returnVal['date'] 			= trim($articleRow['date']) ? $articleRow['date'] : $articleRow['parsed_at'];
			$returnVal['parsed_at'] 			= $articleRow['parsed_at'];
			$returnVal['originalCategory'] = trim($articleRow['cat_name']);
			$returnVal['reference'] = trim($articleRow['reference']);

			$headlineImage    	  		= $this->model->getArticleHeadlineImage($artId);
			$returnVal['headlineImageCaption'] = $headlineImage ? $headlineImage->getImageCaption() : '';
			$returnVal['headlineImage'] = $headlineImage ? $headlineImage->getImgName() : '';
			$returnVal['videos'] 		= $this->model->getArticleVideos($artId);
			$returnVal['images'] 		= $this->model->getArticleImages($artId);
			return $returnVal;
		}
		
		
		
	} // End class 
