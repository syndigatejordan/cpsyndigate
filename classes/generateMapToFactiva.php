<?php

require_once dirname(__FILE__) . '/../ez_autoload.php';

////////////////////////////////////////////////////////////////////////////////////////
// configuraion This script Generate XMLs and push it to Factiva FTP, Should be run each 10 minutes.
///////////////////////////////////////////////////////////////////////////////////////
//	define('COPYRIGHT', " Provided by <a href='http://syndigate.info'>Syndigate.info</a>, an <a href='http://albawaba.com'>Albawaba.com</a> company");
defined('COPYRIGHT') OR define('COPYRIGHT', " Provided by SyndiGate Media Inc. (<a href='http://syndigate.info'>Syndigate.info</a>).");
define('STRIPPING_TAGS', false);
define('TMP_DIR', APP_PATH . 'tmp/resend/');

class syndGenerateFactiva {

  static public $DB_NAME;
  static public $DB_USER;
  static public $DB_PASS;
  static public $DB_HOST;
  private $model;
  private $tmpFiles;
  private $titlesIds, $clientsIds, $titles, $clients;

  public function __construct() {

    $this->model = syndModel::getModelInstance();

    $propelConf = Propel::getConfiguration();
    $dbConf = $propelConf['datasources']['propel']['connection'];

    self::$DB_USER = $dbConf['username'];
    self::$DB_PASS = $dbConf['password'];
    self::$DB_HOST = $dbConf['hostspec'];
    self::$DB_NAME = $dbConf['database'];

    $this->timestamp = time();
    $this->version = date('dhis');
  }

  public function generate() {
    $title_map = array(
        1492 => array('title_id' => 1492, 'max_per_patch' => 9, 'articles_count' => 0),
        363 => array('title_id' => 363, 'max_per_patch' => 15, 'articles_count' => 0),
        223 => array('title_id' => 223, 'max_per_patch' => 15, 'articles_count' => 0),
        55 => array('title_id' => 55, 'max_per_patch' => 9, 'articles_count' => 0),
        1310 => array('title_id' => 1310, 'max_per_patch' => 16, 'articles_count' => 0),
        1388 => array('title_id' => 1388, 'max_per_patch' => 11, 'articles_count' => 0),
        243 => array('title_id' => 243, 'max_per_patch' => 8, 'articles_count' => 0),
        367 => array('title_id' => 367, 'max_per_patch' => 9, 'articles_count' => 0),
        1007 => array('title_id' => 1007, 'max_per_patch' => 10, 'articles_count' => 0),
        368 => array('title_id' => 368, 'max_per_patch' => 13, 'articles_count' => 0),
        1390 => array('title_id' => 1390, 'max_per_patch' => 6, 'articles_count' => 0),
        365 => array('title_id' => 365, 'max_per_patch' => 7, 'articles_count' => 0),
        9 => array('title_id' => 9, 'max_per_patch' => 9, 'articles_count' => 0),
        21 => array('title_id' => 21, 'max_per_patch' => 7, 'articles_count' => 0),
        70 => array('title_id' => 70, 'max_per_patch' => 9, 'articles_count' => 0),
        44 => array('title_id' => 44, 'max_per_patch' => 6, 'articles_count' => 0),
        387 => array('title_id' => 387, 'max_per_patch' => 8, 'articles_count' => 0),
        1609 => array('title_id' => 1609, 'max_per_patch' => 5, 'articles_count' => 0),
        716 => array('title_id' => 716, 'max_per_patch' => 6, 'articles_count' => 0),
        392 => array('title_id' => 392, 'max_per_patch' => 8, 'articles_count' => 0),
        11 => array('title_id' => 11, 'max_per_patch' => 12, 'articles_count' => 0),
        23 => array('title_id' => 23, 'max_per_patch' => 11, 'articles_count' => 0),
        787 => array('title_id' => 787, 'max_per_patch' => 8, 'articles_count' => 0),
        775 => array('title_id' => 775, 'max_per_patch' => 10, 'articles_count' => 0),
        878 => array('title_id' => 878, 'max_per_patch' => 8, 'articles_count' => 0),
        183 => array('title_id' => 183, 'max_per_patch' => 7, 'articles_count' => 0),
        399 => array('title_id' => 399, 'max_per_patch' => 7, 'articles_count' => 0),
        369 => array('title_id' => 369, 'max_per_patch' => 7, 'articles_count' => 0),
        240 => array('title_id' => 240, 'max_per_patch' => 9, 'articles_count' => 0),
        1386 => array('title_id' => 1386, 'max_per_patch' => 10, 'articles_count' => 0),
        1436 => array('title_id' => 1436, 'max_per_patch' => 6, 'articles_count' => 0),
        364 => array('title_id' => 364, 'max_per_patch' => 6, 'articles_count' => 0),
        701 => array('title_id' => 701, 'max_per_patch' => 5, 'articles_count' => 0),
        391 => array('title_id' => 391, 'max_per_patch' => 4, 'articles_count' => 0),
        386 => array('title_id' => 386, 'max_per_patch' => 5, 'articles_count' => 0),
        159 => array('title_id' => 159, 'max_per_patch' => 6, 'articles_count' => 0),
        185 => array('title_id' => 185, 'max_per_patch' => 5, 'articles_count' => 0),
        598 => array('title_id' => 598, 'max_per_patch' => 4, 'articles_count' => 0),
        1442 => array('title_id' => 1442, 'max_per_patch' => 3, 'articles_count' => 0),
        62 => array('title_id' => 62, 'max_per_patch' => 8, 'articles_count' => 0),
        200 => array('title_id' => 200, 'max_per_patch' => 6, 'articles_count' => 0),
        1026 => array('title_id' => 1026, 'max_per_patch' => 3, 'articles_count' => 0),
        393 => array('title_id' => 393, 'max_per_patch' => 8, 'articles_count' => 0),
        398 => array('title_id' => 398, 'max_per_patch' => 7, 'articles_count' => 0),
        10 => array('title_id' => 10, 'max_per_patch' => 4, 'articles_count' => 0),
        164 => array('title_id' => 164, 'max_per_patch' => 6, 'articles_count' => 0),
        789 => array('title_id' => 789, 'max_per_patch' => 7, 'articles_count' => 0),
        389 => array('title_id' => 389, 'max_per_patch' => 8, 'articles_count' => 0),
        36 => array('title_id' => 36, 'max_per_patch' => 4, 'articles_count' => 0),
        247 => array('title_id' => 247, 'max_per_patch' => 3, 'articles_count' => 0),
        176 => array('title_id' => 176, 'max_per_patch' => 9, 'articles_count' => 0),
        713 => array('title_id' => 713, 'max_per_patch' => 7, 'articles_count' => 0),
        172 => array('title_id' => 172, 'max_per_patch' => 6, 'articles_count' => 0),
        1 => array('title_id' => 1, 'max_per_patch' => 5, 'articles_count' => 0),
        888 => array('title_id' => 888, 'max_per_patch' => 6, 'articles_count' => 0),
        788 => array('title_id' => 788, 'max_per_patch' => 7, 'articles_count' => 0),
        729 => array('title_id' => 729, 'max_per_patch' => 8, 'articles_count' => 0),
        13 => array('title_id' => 13, 'max_per_patch' => 4, 'articles_count' => 0),
        1387 => array('title_id' => 1387, 'max_per_patch' => 9, 'articles_count' => 0),
        425 => array('title_id' => 425, 'max_per_patch' => 8, 'articles_count' => 0),
        37 => array('title_id' => 37, 'max_per_patch' => 6, 'articles_count' => 0),
        781 => array('title_id' => 781, 'max_per_patch' => 5, 'articles_count' => 0),
        25 => array('title_id' => 25, 'max_per_patch' => 4, 'articles_count' => 0),
        600 => array('title_id' => 600, 'max_per_patch' => 5, 'articles_count' => 0),
        889 => array('title_id' => 889, 'max_per_patch' => 8, 'articles_count' => 0),
        348 => array('title_id' => 348, 'max_per_patch' => 3, 'articles_count' => 0),
        1223 => array('title_id' => 1223, 'max_per_patch' => 5, 'articles_count' => 0),
        217 => array('title_id' => 217, 'max_per_patch' => 6, 'articles_count' => 0),
        1244 => array('title_id' => 1244, 'max_per_patch' => 8, 'articles_count' => 0),
        1086 => array('title_id' => 1086, 'max_per_patch' => 3, 'articles_count' => 0),
        35 => array('title_id' => 35, 'max_per_patch' => 5, 'articles_count' => 0),
        402 => array('title_id' => 402, 'max_per_patch' => 7, 'articles_count' => 0),
        366 => array('title_id' => 366, 'max_per_patch' => 8, 'articles_count' => 0),
        1366 => array('title_id' => 1366, 'max_per_patch' => 4, 'articles_count' => 0),
        408 => array('title_id' => 408, 'max_per_patch' => 5, 'articles_count' => 0),
        338 => array('title_id' => 338, 'max_per_patch' => 3, 'articles_count' => 0),
        1054 => array('title_id' => 1054, 'max_per_patch' => 6, 'articles_count' => 0),
        7 => array('title_id' => 7, 'max_per_patch' => 7, 'articles_count' => 0),
        1084 => array('title_id' => 1084, 'max_per_patch' => 5, 'articles_count' => 0),
        717 => array('title_id' => 717, 'max_per_patch' => 3, 'articles_count' => 0),
        432 => array('title_id' => 43, 'max_per_patch' => 5, 'articles_count' => 0),
        825 => array('title_id' => 825, 'max_per_patch' => 5, 'articles_count' => 0),
        853 => array('title_id' => 853, 'max_per_patch' => 6, 'articles_count' => 0),
        880 => array('title_id' => 880, 'max_per_patch' => 4, 'articles_count' => 0),
        851 => array('title_id' => 851, 'max_per_patch' => 7, 'articles_count' => 0),
        1075 => array('title_id' => 1075, 'max_per_patch' => 8, 'articles_count' => 0),
        230 => array('title_id' => 230, 'max_per_patch' => 9, 'articles_count' => 0),
        1446 => array('title_id' => 1446, 'max_per_patch' => 4, 'articles_count' => 0),
        918 => array('title_id' => 918, 'max_per_patch' => 6, 'articles_count' => 0),
        919 => array('title_id' => 919, 'max_per_patch' => 7, 'articles_count' => 0),
        496 => array('title_id' => 496, 'max_per_patch' => 8, 'articles_count' => 0),
        780 => array('title_id' => 780, 'max_per_patch' => 9, 'articles_count' => 0),
        784 => array('title_id' => 784, 'max_per_patch' => 5, 'articles_count' => 0),
        419 => array('title_id' => 419, 'max_per_patch' => 6, 'articles_count' => 0),
        1048 => array('title_id' => 1048, 'max_per_patch' => 7, 'articles_count' => 0),
        602 => array('title_id' => 602, 'max_per_patch' => 6, 'articles_count' => 0),
        1152 => array('title_id' => 1152, 'max_per_patch' => 4, 'articles_count' => 0),
        76 => array('title_id' => 76, 'max_per_patch' => 3, 'articles_count' => 0),
        401 => array('title_id' => 401, 'max_per_patch' => 10, 'articles_count' => 0),
        233 => array('title_id' => 233, 'max_per_patch' => 4, 'articles_count' => 0),
        461 => array('title_id' => 461, 'max_per_patch' => 7, 'articles_count' => 0),
        163 => array('title_id' => 163, 'max_per_patch' => 8, 'articles_count' => 0),
        407 => array('title_id' => 407, 'max_per_patch' => 4, 'articles_count' => 0),
        4 => array('title_id' => 4, 'max_per_patch' => 5, 'articles_count' => 0),
    );
    $conn = mysql_connect(self::$DB_HOST, self::$DB_USER, self::$DB_PASS, true) or die("\n\nCannot connect to database\n\n");
    mysql_select_db(self::$DB_NAME) or die("\n\nDatabase doesn't exists\n\n");
    $template = syndGenerateTemplatesManager::factory('NewsMl');
    $titles = array();
    $q = mysql_query("select * from article_jumper where is_sent=0 order by mapped_title,article_id", $conn);
    $this->titles = array();
    $article_ids = array();
    $generated_history = array();
    while ($row = mysql_fetch_assoc($q)) {
      $article_ids[$row['mapped_title']][] = $row['article_id'];
    }
    foreach ($article_ids as $title => $articles) {
      $this->version = date('dhis');
      $max_per_patch = $title_map[$title]['max_per_patch'];
      $limited_articles = array_slice($articles, 0, $max_per_patch);
      $titles = $this->getArticlesByIds($title, $limited_articles);
      $fileName = $template->generate($titles);
      $this->tmpFiles[$fileName] = $fileName;
      $client = ClientPeer::retrieveByPK(6);
      $ftpDir = $this->prepareDir($client->getHomeDir());
      $result = copy($fileName, $ftpDir . $title . '_' . $this->version . '.xml');
      $generated_history[] = $ftpDir . $title . '_' . $this->version . '.xml';
      chown($ftpDir . $title . '_' . $this->version . '.xml', $client->getFtpUsername());
      chgrp($ftpDir . $title . '_' . $this->version . '.xml', $client->getFtpUsername());
      chmod($ftpDir . $title . '_' . $this->version . '.xml', 0777);
      // Upload XML to the Client Local and FTP
      $success = $this->uploadFileViaFTP($ftpDir, $title . '_' . $this->version . '.xml');
      if ($success) {
        // Mark articles as sent.
        $this->updateArticles($limited_articles);
      }
      sleep(3);
    }
    $this->clearTmp();
    var_dump($generated_history);
    
  }

  public function getArticlesByIds($mapped_title, $article_ids = null) {
    if (!$article_ids)
      return;
    $titleRow = TitlePeer::retrieveByPK($mapped_title);
    if (!is_object($titleRow)) {
      echo "Can't find the title $mapped_title" . chr(10);
    }
    $id = $titleRow->getId();
    $titles[$id]['id'] = $id;
    $titles[$id]['providerName'] = $titleRow->getName();
    $titles[$id]['copyright'] = $titleRow->getCopyright();
    $titles[$id]['webSite'] = $titleRow->getWebsite();
// get title's articles				
    $sql = 'SELECT article.*, original_article_category.cat_name FROM article left join article_original_data on article_original_data.id = article.article_original_data_id left join original_article_category on article_original_data.original_cat_id = original_article_category.id WHERE 1';
    if (is_array($article_ids) && $article_ids) {
      $aids = implode(',', $article_ids);
      $sql.= " AND article.id in ($aids)";
    }
    $conn = mysql_connect(self::$DB_HOST, self::$DB_USER, self::$DB_PASS, true) or die("\n\nCannot connect to database\n\n");
    mysql_select_db(self::$DB_NAME) or die("\n\nDatabase doesn't exists\n\n");
    $articlesResult = mysql_query($sql, $conn);
    $titles[$id]['articles'] = new syndGenerateMySqlIterator($articlesResult, new ArticleArrayFormater($this->model));
    if (!mysql_num_rows($articlesResult)) {
      unset($titles[$id]);
    }
    return $titles;
  }

  private function prepareDir($clientHomeDir) {

    $ftpDir = rtrim($clientHomeDir, '/') . '/' . date('Y/m/d/');

    if (!is_dir($ftpDir)) {
      mkdir($ftpDir, 0777, true);
      //mkdir($ftpDir, 0755, true);
    }
    return $ftpDir;
  }

  private function clearTmp() {
    if (is_array($this->tmpFiles)) {
      foreach ($this->tmpFiles as $file) {
        unlink($file);
      }
    }
  }

  public function updateArticles($ids) {
    $conn = mysql_connect(self::$DB_HOST, self::$DB_USER, self::$DB_PASS, true) or die("\n\nCannot connect to database\n\n");
    mysql_select_db(self::$DB_NAME) or die("\n\nDatabase doesn't exists\n\n");
    $date = date('Y-m-d H:i:s', time());
    $aids = implode(',', $ids);
    $sql = "update article_jumper set is_sent=1,send_date ='$date' where article_id in ($aids)";
    mysql_query($sql, $conn);
  }

  public function uploadFileViaFTP($ftpDir, $localFile) {
    $c = new Criteria();
    $c->add(ClientConnectionPeer::CLIENT_ID, 6);
    $ftp = ClientConnectionPeer::doSelectOne($c);
// set up basic connection
    $conn_id = ftp_connect($ftp->getUrl());

// login with username and password
    $login_result = ftp_login($conn_id, $ftp->getUsername(), $ftp->getPassword());
// upload a file
    if (ftp_put($conn_id, $localFile, $ftpDir . $localFile, FTP_ASCII)) {
      $success = true;
      // echo "successfully uploaded $file\n";
    } else {
      $success = false;
      // echo "There was a problem while uploading $file\n";
    }

// close the connection
    ftp_close($conn_id);
    return $success;
  }

}

class ArticleArrayFormater implements syndGenerateArrayFormater {

  private $model;

  public function __construct($model) {
    $this->model = $model;
  }

  private function getLanguage($langId) {
    static $languages = array();
    if (isset($languages[$langId])) {
      return $languages[$langId];
    }
    $lang = $this->model->getLanguage($langId);
    if ($lang) {
      $languages[$lang->getId()] = $lang->getName();
    }
    return ($lang ? $lang->getName() : '');
  }

  public function format($articleRow) {
    $artId = $articleRow['id'];

    $returnVal['id'] = $artId;
    $returnVal['headline'] = $articleRow['headline'];
    $returnVal['byLine'] = $articleRow['author'];
    $returnVal['language'] = $this->getLanguage($articleRow['language_id']);
    $returnVal['category'] = $articleRow['iptc_id'];
    $returnVal['abstract'] = $articleRow['summary'];
    $returnVal['text'] = & $articleRow['body'];
    $returnVal['date'] = trim($articleRow['date']) ? $articleRow['date'] : $articleRow['parsed_at'];
    $returnVal['parsed_at'] = $articleRow['parsed_at'];
    $returnVal['originalCategory'] = trim($articleRow['cat_name']);

    $headlineImage = $this->model->getArticleHeadlineImage($artId);
    $returnVal['headlineImageCaption'] = $headlineImage ? $headlineImage->getImageCaption() : '';
    $returnVal['headlineImage'] = $headlineImage ? $headlineImage->getImgName() : '';
    $returnVal['videos'] = $this->model->getArticleVideos($artId);
    $returnVal['images'] = $this->model->getArticleImages($artId);
    return $returnVal;
  }

}

$obj = new syndGenerateFactiva();
$obj->generate();

// End class 
?>
