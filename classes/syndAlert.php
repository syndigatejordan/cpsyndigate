<?php

	/**
	 * This is the main alerts class in the system. Mainly used for CLI
	 * The class interacts with Nagios to send alerts (email, SMS)
	 * 
	 * Example:
	 * 
	 * <code>
	 *	// Use log
	 *	$alert = syndAlert::getInstance();
	 *	$alert->send("Server is down", syndAlert::OK, 'SERVER', 5);
	 * </code>
	 * 
	 * @todo Add logging where possible
	 */
	class syndAlert {
		
		
		public static $definedServices = array(
			'LOGIN',
			'SERVER',
			'ARTICLES_COUNT',
			'NEW_CONTENT',
		);
		
		/**
		 * Nagios Return Codes (severity)
		 *
		 */
		const OK = 0;
		const WARNING = 1;
		const CRITICAL = 2;
		const UNKNOWN = 3;
		
		
		/**
		 * Singletone instance
		 *
		 * @var syndAlert
		 */
		private static $instance;
		
		/**
		 * Full path to Nagios command pipe
		 *
		 * @var string
		 */
		protected $nagiosCmd = '/usr/local/nagios/var/rw/nagios.cmd';
		
		/**
		 * Nagios CMD FIFO handle 
		 *
		 * @var resource
		 */
		protected $handle;
		
		/**
		 * Nagios Service
		 *
		 * @var string
		 */
		public $service;
		
		/**
		 * Nagios Host
		 *
		 * @var string
		 */
		public $host = 'synd01';

		/**
		 * log params
		 * 
		 * @var array
		 */
		public $logParams;
		/**
		 * Singleton stuff
		 *
		 * @return syndAlert
		 */
		public static function getInstance() {
			if (empty(self::$instance)) {
				self::$instance = new syndAlert();
			}
			
			return self::$instance;
		}
		
		public function __construct() {
			
		}
		
		
		/**
		 * Sends a passive check to Nagios
		 *
		 * @param string $msg
		 * @param int $severity
		 * @param string $service
		 * @param int $id Client or Title Id
		 * @param string $type (client | title)
		 */
		public function send($msg, $severity, $service, $id, $type='title') {
			return true;

			if (empty($service)) {
				trigger_error("syndAlert: no service specified.", E_USER_ERROR);
			}

			if (!in_array($service, self::$definedServices)) {
				trigger_error("syndAlert: [$service] is not a valid service");
			}
			
			if (!$id) {
				trigger_error("syndAlert: you must specify a {$type} id");
			}			

			//get service name based on type 
			$fullSeviceName = $type == 'client' ? 'CL_' . $id . '_' . $service : $id . '_' . $service;

			//Nayef: change this to dynamic function
			$model = new syndModel();
			$msg = "[" . ucfirst($type) . ': ' . $model->getTypeName($id, $type) . "]: " . $msg;
            $log = ezcLog::getInstance();
            $log->source = 'alert';
            $this->logParams = array('source'=>'alert');
            $log->log($this->host . ' ' . $fullSeviceName . ' ' .  $severity . ' ' . $msg, ezcLog::DEBUG, $this->logParams);                        
			return $this->writeAlert($this->host, $fullSeviceName, $severity, $msg);
		}
		

		
		/**
		 * Sends a passive check to Nagios
		 * This is specific for Syndigate administrators
		 *
		 * @param string $msg
		 * @param int $severity
		 * @param string $service
		 */
		public function sendSynd($msg, $severity, $service) {

			if (empty($service)) {
				trigger_error("syndAlert: no service specified.", E_USER_ERROR);
			}

			//All synd alerts are prefixed with synd_
			$fullSeviceName = 'synd_' . $service;			
					
			return $this->writeAlert($this->host, $fullSeviceName, $severity, $msg);
		}

		/**
		 * writes the actual alert to the nagios FIFO
		 *
		 * @param string $checkResult
		 * @param string $time
		 * @param string $host
		 * @param string $service
		 * @param int $severity
		 * @param string $msg
		 */
		protected function writeAlert($host, $service, $severity, $msg) {

			if (($severity < 0) || ($severity > 3) || !is_int($severity)) {
				trigger_error("syndAlert: severity must be an integer in the range [0-3]", E_USER_ERROR);	
			}
			
			/*
			 * Lazy initialization
			 */
			if (!$this->handle) {
				$this->openFifo();
			}
									
			$result = fwrite($this->handle, 
				sprintf("[%d] PROCESS_SERVICE_CHECK_RESULT;%s;%s;%d;%s", time(), $host, $service, $severity, $msg)
				);
				
			if (!$result) {
				trigger_error("syndAlert: could not send passive check to nagios.cmd");
				return false;
			} else {
				return true;
			}
			
			
		}		
		
		
		private function openFifo() {
			$this->handle = fopen($this->nagiosCmd, 'w');
			if (!$this->handle) {
				trigger_error("syndAlert: Could not open FIFO to nagios.cmd", E_USER_WARNING);
			}
			
		}
		
		private function closeFifo() {
			if ($this->handle) {
				fclose($this->handle);
			}
		}
		
		public function __destruct() {
			$this->closeFifo();
		}
		
	}
