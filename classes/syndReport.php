<?php


class syndReport
{
	private $type;
	
	public static $allowedTypes = array('gather','parse', 'generate', 'send');
	
	private $model;
	
	/**
	 * class constructor
	 */
	public function __construct($type)
	{
		if(!in_array($type,self::$allowedTypes)){
			throw new Exception('Type does not exists.');
		} else {
			$this->type = $type;
			$this->model = new syndModel();
		}
	}
	
	/**
	 * add report
	 * @param array $params
	 */
	public function addReport($params)
	{
		$methodName = "{$this->type}AddReport";
		return $this->model->$methodName($params);
	}
	
	/**
	 * update report
	 * @param array $params
	 */
	public function updateReport($params)
	{
		$methodName = "{$this->type}UpdateReport";
		$this->model->$methodName($params);
	}
}


