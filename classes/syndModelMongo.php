<?php 

defined('MONGO_ENABLED') OR define('MONGO_ENABLED', 0);


defined ('MONGO_DB_NAME') OR define('MONGO_DB_NAME', 'syndigate');
defined ('MONGO_DB_USER') OR define('MONGO_DB_USER', '');
defined ('MONGO_DB_PASS') OR define('MONGO_DB_PASS', '');
defined ('MONGO_DB_HOST') OR define('MONGO_DB_HOST', '10.200.200.58');

class syndModelMongo {
	
	private $connection, $db;
	
	
	private $logGatherTable 	= 'log_gather';
	private $logParseTable 		= 'log_parse';
	private $logGenerateTable   = 'log_generate';
	private $logSendTable       = 'log_send';
	
	const DEBUG          = 1;
    const SUCCESS_AUDIT  = 2;
    const FAILED_AUDIT   = 4;
    const INFO           = 8;
    const NOTICE         = 16;
    const WARNING        = 32;
    const ERROR          = 64;
    const FATAL          = 128;
		
	public function __construct() {
		if(MONGO_ENABLED) {
			try {
				$this->connection 	= new Mongo(MONGO_DB_HOST);
				
				if($this->connection) {
					$this->db 			= $this->connection->{MONGO_DB_NAME};
			
					if(MONGO_DB_USER) {
						$this->db->authenticate(MONGO_DB_USER, MONGO_DB_PASS);
					}
				}
			} catch (Exception $e) {
				// Nothing just do not stop syndigate because mongo logs down.
				echo $e->getMessage() . PHP_EOL;
			}
		}
	}
	
	public static function getInstance() {
		
		static $instance;
		if(!$instance) {
			$instance = new syndModelMongo();
		}

		return $instance;
	}
	
	private function insert($collection, $params) {
		if(MONGO_ENABLED) {		
			try {
				if($collection) {
					$collection = $this->db->{$collection};
					$result = $collection->insert($params, true);
				
					if(!$result) {
						return false;
					} else {
						$vars  = get_object_vars($params['_id']);
						return $vars['$id'];
					}
				}
			} catch (Exception $e) {
				echo $e->getMessage() . PHP_EOL;
				return false;
			}
		}
	}
	
	public function log ( $message, $severity, array $attributes = array() ) {
		
		try {
			if(!isset($attributes['source']) || !$message) {
				//echo "SyndModelMongo : Not loggin (source or message empty ) : \n\t source : {$attributes['source']} \n\t message : $message " .PHP_EOL ;
echo "SyndModelMongo : Not loggin (source or message empty ) : \n\t source : message : $message " .PHP_EOL ;
				return false;
			}
			if(!$severity) {
				$severity = 1; //Debug mod 
			}
			
			$params['message']  = $message;
			$params['severity'] = ezcLog::translateSeverityName($severity);
		
			$this->insertLog('log_' . $attributes['source'], array_merge($attributes, $params));
		} catch (Exception $e) {
			echo $e->getMessage() . PHP_EOL;
		}
	}
	

	private function insertLog($table, $params) {
		//return ;
		$params['category'] 		= isset($params['category'])  ? $params['category'] : '';
		$params['message'] 			= isset($params['message'])   ? $params['message'] : '';
		$params['severity'] 		= isset($params['severity'])  ? $params['severity'] : '';
		$params['source'] 			= isset($params['source'])    ? $params['source'] : '';
		$params['time'] 			= time();
		echo $params['message'] . PHP_EOL;
		
		return $this->insert($table, $params);
	}
	
	public function insertLogGather($params) {
		$params['report_gather_id'] = $params['report_gather_id'] ? (int)$params['report_gather_id'] :0;
		return $this->insertLog($this->logGatherTable, $params);
	}
	
	public function insertLogParse($params) {
		$params['report_parse_id'] = $params['report_parse_id'] ? (int)$params['report_parse_id'] :0;
		return $this->insertLog($this->logParseTable, $params);
	}
	
	public function insertLogGenerate($params) {
		$params['report_generate_id'] = $params['report_generate_id'] ? (int)$params['report_generate_id'] :0;
		return $this->insertLog($this->logGatherTable, $params);
	}
	
	public function getGatherLogs($reportId) {
		if(MONGO_ENABLED) {
			try {
				$collection = $this->db->{$this->logGatherTable};
				if($collection) {
					$query  = array('report_gather_id'=> $reportId);
					$cursor = $collection->find($query);
					return iterator_to_array($cursor);
				}
			} catch (Exception $e) {
				echo $e->getMessage() . PHP_EOL;
				return false;
			}
		}
		
		return false;
	}
	
	public function getParseLogs($reportId) {
		if(MONGO_ENABLED) {
			try {
				$collection = $this->db->{$this->logParseTable};
				if($collection) {
					$query  = array('report_parse_id'=> $reportId);
					$cursor = $collection->find($query);
					return iterator_to_array($cursor);
				}
			} catch (Exception $e) {
				echo $e->getMessage() . PHP_EOL;
				return false;
			}
		}
		return false;
	}
	
	public function getGenerateLogs($reportId) {
		if(MONGO_ENABLED) {
			try {
				$collection = $this->db->{$this->logGenerateTable};
				if($collection) {
					$query  = array('report_generate_id'=> $reportId);
					$cursor = $collection->find($query);
					return iterator_to_array($cursor);
				}
			} catch (Exception $e) {
				echo $e->getMessage() . PHP_EOL;
				return false;
			}
		}
		return false;
	}
	
	public function getSendLogs($reportId) {
		if(MONGO_ENABLED) {
			try {
				$collection = $this->db->{$this->logSendTable};
				if($collection) {
					$query  = array('report_send_id'=> $reportId);
					$cursor = $collection->find($query);
					return iterator_to_array($cursor);
				}
			} catch (Exception $e) {
				echo $e->getMessage() . PHP_EOL;
				return false;
			}
		}
		return false;
	}
}

