<?php
   require_once dirname(__FILE__) . '/../../ez_autoload.php';

	////////////////////////////////////////////////////////////////////////////////////////
	// configuraion
	///////////////////////////////////////////////////////////////////////////////////////
	//defined('COPYRIGHT')      OR define('COPYRIGHT', " Provided by <a href='http://syndigate.info'>Syndigate.info</a>, an <a href='http://albawaba.com'>Albawaba.com</a> company");
defined('COPYRIGHT')      OR define('COPYRIGHT', " Provided by SyndiGate Media Inc. (<a href='http://syndigate.info'>Syndigate.info</a>).");
	defined('STRIPPING_TAGS') OR define('STRIPPING_TAGS', false);
	defined('TMP_DIR')        OR define('TMP_DIR', APP_PATH.'tmp/');
		
  
	class syndGenerateTR
	{
		static public $DB_NAME;
		static public $DB_USER;
		static public $DB_PASS;
		static public $DB_HOST;
		
		private $model, $ModelMongo;
		private $tmpFiles, $filesToSend = array(), $tmpFilesCount = array();
		private $timeStamp, $version;
		
		private $log, $logParams;
		private $clientId = 1 ;
		private $local_file_id;
		
		public function __construct()
		{
			$this->model      = syndModel::getModelInstance();
			$this->ModelMongo = syndModelMongo::getInstance(); 
			
			$propelConf  = Propel::getConfiguration();
			$dbConf 	 = $propelConf['datasources']['propel']['connection'];
			
			self::$DB_USER = $dbConf['username'];
			self::$DB_PASS = $dbConf['password'];
			self::$DB_HOST = $dbConf['hostspec'];
			self::$DB_NAME = $dbConf['database'];			
		}
		
		
		public function generate($titleId, $articlesIds) {
			
			$this->titleId = $titleId;
			$this->articlesIds = $articlesIds;
			$template = syndGenerateTemplatesManager::factory('NewsMl');
					
			$titles   = $this->getArticlesTree(); 
			$file     = $template->generate($titles);
			
			if(is_array($titles) && count($titles) > 0) {
				$articlesCount  = (int) $titles[$titleId]['articles']->getCount();
				$this->tmpFileCount = $articlesCount;
				//$this->log->log("File $file generated containing all articles for this day ( $articlesCount articles )", EzcLog::DEBUG, $this->logParams);
			} else {
				$this->tmpFileCount = 0;
				//$this->log->log("No articles found for this day, No file generated with default filters ( today's timestamp )", EzcLog::DEBUG, $this->logParams);
			}
			
			$articlesCount  = $this->tmpFileCount;
			$this->tmpFile  = $file;			
			chmod($file, 0775);
			
			
			if( $this->copyFilesToClientsDir() ) {
				$this->clearTmp();				
				//$this->log->log('Finsied generating ', ezcLog::DEBUG , $this->logParams);
			} else {
				//$this->log->log('Finsied generating with Errors ', ezcLog::DEBUG , $this->logParams);
			}
			
			
			$this->addGeneratedFilesTobeSend($this->filesToSend);
			return $this->filesToSend;

		}
		
		private function clearTmp() {
			
			if(is_array($this->tmpFiles)) {
				foreach($this->tmpFiles as $file) {
					unlink($file);
				}
			}
		}
	
		
		/**
		 * Prepares the dir to write files to, and returns the full path
		 *
		 * @param string $clientHomeDir
		 */
		private function prepareDir($clientHomeDir) {
			
			if($this->timeStamp) {
				$ftpDir = rtrim($clientHomeDir, '/').'/'.date('Y/m/d/', $this->timeStamp);
			} else {
				$ftpDir = rtrim($clientHomeDir, '/').'/'.date('Y/m/d/');
			}
			
			if(!is_dir($ftpDir)) {
				$rez = mkdir($ftpDir, 0755, true);
				if(!$rez) {
					//$this->log->log("Could not create client directory $ftpDir", ezcLog::ERROR, $this->logParams);
				} else {
					//$this->log->log("Directory created $ftpDir", ezcLog::DEBUG, $this->logParams);
				}
			}			
			return $ftpDir;
		}

		

		/**
		 * get all title id from article table which parsed in specific date
		 * and put the results in tree
		 *
		 * @param string $date
		 * @return array
		 */
		private function &getArticlesTree()
		{			
			$conn = mysql_connect(self::$DB_HOST, self::$DB_USER, self::$DB_PASS, true) or die("\n\nCannot connect to database\n\n");
			mysql_select_db (self::$DB_NAME) or die("\n\nDatabase doesn't exists\n\n");
			
			$sql 			=  'SELECT * FROM '.TitlePeer::TABLE_NAME . ' WHERE id = ' . $this->titleId ;
			$titlesResult 	= mysql_query($sql, $conn);
			while ($titleRow = mysql_fetch_assoc($titlesResult)) {
								
				$id = $titleRow['id'];
				$titles[$id]['id']           = $id;
				$titles[$id]['providerName'] = $titleRow['name'];
				$titles[$id]['copyright']    = $titleRow['copyright'];
				$titles[$id]['webSite']      = $titleRow['website'];
				
				// get title's articles
				$sql = 'SELECT article.*, original_article_category.cat_name FROM article left join article_original_data on article_original_data.id = article.article_original_data_id left join original_article_category on article_original_data.original_cat_id = original_article_category.id WHERE article.id in (' . implode(',', $this->articlesIds)  .')';
				$articlesResult = mysql_query($sql, $conn) ;
				$titles[$id]['articles']  = new syndGenerateMySqlIterator($articlesResult, new ArticleArrayFormater($this->model));
				
				if(!mysql_num_rows($articlesResult)) {
					unset($titles[$id]);
				}

			}
			
			return $titles;
		}
		
		
		/**
		 * Coping the tmp files to clients directories
		 *
		 * @return true if every thing goes right false otherwise
		 */
		protected function copyFilesToClientsDir() {
			
			//$this->log->log('starting copying file to clients directories ', EzcLog::DEBUG , $this->logParams);
			
			$client    = ClientPeer::retrieveByPK($this->clientId);
			$hasErrors = 0;
			
			if(!$client->getIsActive()) {
				$this->log->log("Client # " . $client->getId() . ' --' . $client->getClientName() . ' is inactive, ignoring copying file', ezcLog::DEBUG , $this->logParams);
				continue;
			}
			
			$ftpDir     = $this->prepareDir($client->getHomeDir());							
			$fromFile   = $this->tmpFile;			
			$targetFile = $ftpDir . $this->titleId . '_' . time() . '_legal.xml';			
			$clientPullFile = explode($client->getHomeDir(), $targetFile);			
			$clientPullFile = $clientPullFile[1];	
										
			$rez = copy($fromFile, $targetFile);			
			if(!$rez) {				
				//$this->log->log("Could not copy $fromFile to $targetFile", ezcLog::ERROR , $this->logParams);
				return false;
			} else {
				//$this->log->log("Copied : $fromFile to $targetFile", ezcLog::DEBUG, $this->logParams);
				

				$clientFile = new TrFile();
				$clientFile->setClientId($client->getId());
				$clientFile->setTitleId($this->titleId);				
				$clientFile->setLocalFile($clientPullFile);				
				$clientFile->setCreationTime(time());
				$clientFile->setArticleCount($this->tmpFileCount);
				$clientFile->save();
				$this->local_file_id = $clientFile->getId();
				
				if($client->getConnectionType() == 'ftp_push') {				
					$slugs = explode($client->getHomeDir(), $targetFile);
					$this->filesToSend[$client->getId()] = $slugs[1];
				}
			}
			return $hasErrors ? 0 : 1;
		}
		
		
		public function addGeneratedFilesTobeSend($files = array()) {
			
    		if( $files ) {
    			
				//$this->log->log("files to be send" . print_r($files, true), ezcLog::DEBUG , $this->logParams);
    	
    			// Insert files to the database report so if an error happened in the send the files will not be lost 
    			foreach ($files as $clientId => $file) {
    				$client 	 = ClientPeer::retrieveByPK($clientId);
    				$connections = $client->getClientConnections();
    		
    				foreach ($connections as $connection) {
    					$connectionFile = new TrConnectionFile();
    			
    					//if(isset($this->generalReportId)) {
    						//$connectionFile->setReportId($this->generalReportId);
    					//}
    					$connectionFile->setClientId($clientId);
    					$connectionFile->setClientConnectionId($connection->getId());
    					$connectionFile->setTrFileId($this->local_file_id);
    					$connectionFile->setTitleId($this->titleId);    					
    					$connectionFile->setLocalFile($file);
						$connectionFile->setCreationTime(time());
    					$connectionFile->save();
    				}
    			}
    		} else {
				//$this->log->log("No files to be send to FTP account", ezcLog::DEBUG , $this->logParams);
			}
		}

	} // End class syndGenerate

	
	class ArticleArrayFormater implements syndGenerateArrayFormater
	{
		private $model;
		public function __construct($model)
		{
			$this->model = $model;
		}

		private function getLanguage($langId)
		{
			static $languages = array();
			if(isset($languages[$langId])) {
				return $languages[$langId];
			}
			$lang = $this->model->getLanguage($langId);
			if($lang) {
				$languages[$lang->getId()] = $lang->getName();
			}
			return ($lang ? $lang->getName() : '');
                }
		
		public function format($articleRow)
		{
			$artId = $articleRow['id'];
			
			$returnVal['id'] = $artId;
			$returnVal['headline'] 		= $articleRow['headline'];
			$returnVal['byLine']   		= $articleRow['author'];
			$returnVal['language'] 		= $this->getLanguage($articleRow['language_id']);
			$returnVal['category'] 		= $articleRow['iptc_id'];
			$returnVal['abstract'] 		= $articleRow['summary'];
			$returnVal['text'] 			=& $articleRow['body'];
			$returnVal['date'] 			= trim($articleRow['date']) ? $articleRow['date'] : $articleRow['parsed_at'];
			$returnVal['originalCategory'] = trim($articleRow['cat_name']);
			
			$headlineImage    	  		= $this->model->getArticleHeadlineImage($artId);
			$returnVal['headlineImageCaption'] = $headlineImage ? $headlineImage->getImageCaption() : '';
			$returnVal['headlineImage'] = $headlineImage ? $headlineImage->getImgName() : '';
			$returnVal['videos'] 		= $this->model->getArticleVideos($artId);
			$returnVal['images'] 		= $this->model->getArticleImages($artId);
			return $returnVal;
		}
		
		
		
	} // End class 
