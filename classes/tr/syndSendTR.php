<?php
require_once dirname(__FILE__) . '/../../ez_autoload.php';
require_once 'Net/FTP.php';

class syndSendTR
{
    // FTP Details
    protected $ftp;
    protected $ftpServer;
    protected $ftpPort;
    protected $ftpUsername;
    protected $ftpUserPass;
    protected $is_connected = false;
    
    protected $homeDir;
    protected $remoteDir;
    
    protected $model;    
    protected $error;
    
    protected $clientId;
    protected $clientName;
    protected $dirLayout;   
    protected $client;
    
    public $max_file_num_retries;    
    public $titleId;
    
    
    public function __construct () {	
    	
    	$this->max_file_num_retries = 20; // can be overrider after the constructor for caller script    	
        $this->model 		= new syndModel();
        $this->ftp 			= new Net_FTP();
        
        $c = new Criteria();
        $c->add(TrConnectionFilePeer::IS_SENT, 0);
        if($this->max_file_num_retries) {
        	$c->add(TrConnectionFilePeer::NUM_RETRY , $this->max_file_num_retries, Criteria::LESS_EQUAL);
        }        
        $c->addAscendingOrderByColumn(TrConnectionFilePeer::NUM_RETRY);
        $filesToSend = TrConnectionFilePeer::doSelect($c);
        
        foreach ($filesToSend as $file) {        	
        	$this->copyContent($file);
        }
    }
    
   
    
    private function setSettingsFromConnectionFile(TrConnectionFile $trConnectionFile) {
    	
    	$this->clientId = $trConnectionFile->getClientId();
    	$this->titleId  = $trConnectionFile->getTitleId();    	
    	$this->files    = array($trConnectionFile->getLocalFile());
    	$this->client 	= $this->model->getClientFields($this->clientId);
    	
    	$this->homeDir 		= $this->client->getHomeDir();
        $this->dirLayout	= $this->client->getDirLayout();
        $this->clientName 	= $this->client->getClientName();
        
    }
    
    
    public function connect ()
    {
        //Note:  FTP URL MOST NOT HAVE [ FTP:// ] prefix		
        $connect = $this->ftp->connect($this->ftpServer, $this->ftpPort);
        
        if (PEAR::isError($connect)) {
        	$this->error = 'Server is down for: ' . $this->clientName . 'with connection [' . $this->ftpUsername .']'  ;
        }
    }
    
    
    public function login ()
    {
        $this->connect();        
	    $login = $this->ftp->login($this->ftpUsername, $this->ftpUserPass);
        if (PEAR::isError($login) && !$this->error) {
        	$this->error = $login->getMessage() . ' for: ' . $this->clientName . 'with connection [' . $this->ftpUsername .']';
        }
        
    }
  
    
    
    public function copyContent( TrConnectionFile $file )
    {   
    	$this->error = null;
    	
    	$file->setNumRetry( (int)$file->getNumRetry() + 1);
    	$file->save();
    	
    	$this->setSettingsFromConnectionFile($file);
    	$conn = ClientConnectionPeer::retrieveByPK($file->getClientConnectionId());
    			
    	$this->ftpServer 	= $conn->getUrl();
    	$this->ftpPort 		= $conn->getFtpPort();
    	$this->ftpUsername 	= $conn->getUsername();
    	$this->ftpUserPass 	= $conn->getPassword();
    	$this->remoteDir 	= $conn->getRemoteDir();
    	
    		
    	//login to the FTP server
    	$this->login();
    	$upload = null;
    		
    			
		if($this->error) {
			$file->setErrMsg($this->error);
            $file->save();
			continue;
		}
		
		$theFile = $file->getLocalFile();
		if ($this->dirLayout == 'root') {    	    				
			$remotePaths = array_slice( explode('/', $theFile), 3);
		} else {    				
			$remotePaths  = explode('/', $theFile);
		}
		
		
			
		$remoteFile  = $this->remoteDir . DIRECTORY_SEPARATOR . implode($remotePaths, DIRECTORY_SEPARATOR);
		$remoteFile  = str_replace(DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, $remoteFile);
		$folder      = dirname($remoteFile);
		$cd          = $this->ftp->cd($folder);
		echo $theFile . PHP_EOL;
		echo $remoteFile . PHP_EOL;
		echo $folder . PHP_EOL;
		if (true !== $cd) {			
			$rez = $this->ftp->mkdir($folder, true);
			print_r($rez);
			die();
			if(!$rez) { 
				echo "could not create " . PHP_EOL;   						
				$this->error = "Dir $folder could not be created";  
				$file->setErrMsg($this->error);
            	$file->save();  					
            	continue;
			}
		}
		
		echo $this->homeDir . $theFile  . PHP_EOL; 
		$upload = @$this->ftp->put(str_replace(DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, $this->homeDir . $theFile), $remoteFile, true);    

		if (PEAR::isError($upload)) {
			
            $file->setErrMsg($upload->getMessage());
            $file->save();
            
		} else {
			
            $file->setIsSent(1);
			$file->setSentTime(time());
            $file->save();
		}
    		
    	
    	
    		
    	$this->ftp->disconnect();
    	return true;
    }
   
}
