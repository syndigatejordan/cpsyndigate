<?php
    require_once dirname(__FILE__).'/../ez_autoload.php';

	////////////////////////////////////////////////////////////////////////////////////////
	// configuraion
	///////////////////////////////////////////////////////////////////////////////////////
//	define('COPYRIGHT', " Provided by <a href='http://syndigate.info'>Syndigate.info</a>, an <a href='http://albawaba.com'>Albawaba.com</a> company");
defined('COPYRIGHT')      OR define('COPYRIGHT', " Provided by SyndiGate Media Inc. (<a href='http://syndigate.info'>Syndigate.info</a>).");
	define('STRIPPING_TAGS', false);
	define('TMP_DIR', APP_PATH.'tmp/resend/');		

	class syndReGenerate
	{
		static public $DB_NAME;
		static public $DB_USER;
		static public $DB_PASS;
		static public $DB_HOST;
		
		private $model;
		private $tmpFiles;
		
		private $titlesIds, $clientsIds, $titles, $clients;

		public function __construct() {
			
			$this->model = syndModel::getModelInstance();
						
			$propelConf  = Propel::getConfiguration();
			$dbConf 	 = $propelConf['datasources']['propel']['connection'];
			
			self::$DB_USER = $dbConf['username'];
			self::$DB_PASS = $dbConf['password'];
			self::$DB_HOST = $dbConf['hostspec'];
			self::$DB_NAME = $dbConf['database'];			
			
			$this->timestamp = time();
			$this->version = date('dhis');			
		}
		
		
		public function generate($titlesIds, $clientsIds, $fromDateTime, $toDateTime,$article_ids = null)
		{
			// Assining variables
			
			$this->titlesIds = $titlesIds;
			$this->clientsIds =$clientsIds;
			
			$c = new Criteria();
                        $c->add(ClientPeer::ID , $clientsIds, Criteria::IN);
                        $this->clients = ClientPeer::doSelect($c);
    
                        $c = new Criteria();
                        $c->add(TitlePeer::ID , $titlesIds, Criteria::IN );
                        $this->titles = TitlePeer::doSelect($c);
    		
                        //Getting articles group by title id with extra info			
      if(is_array($article_ids) && !empty($article_ids)){
        $titles   = $this->getArticlesByIds($article_ids);                        
      }else{
        $titles   = $this->getArticles($fromDateTime, $toDateTime);                                            
      }
			$template = syndGenerateTemplatesManager::factory('NewsMl');
			$mic_template = syndGenerateTemplatesManager::factory('NewsMlMicrosoft');
			$mrss_template = syndGenerateTemplatesManager::factory('MRSS');
		
			//Generating NewsML files for each title articles [each title will have a single file containing the generated articles ]
			
			if(!is_dir(TMP_DIR)) {
				mkdir(TMP_DIR, 0777, true);
			}
			
			foreach ($titles as $title) {
                            if( isset($titles[$title['id']])) {                                
                                $xml[$this->timestamp][$title['id']] = $template->generate(array($title));                                                                                                
                                $mic_xml[$this->timestamp][$title['id']] = $mic_template->generate(array($title));                                                                                                
                                $mrss_xml[$this->timestamp][$title['id']] = $mrss_template->generate(array($title));                                                                                                
                            }
			}	
                        print_r($xml);
                        			
			foreach ($this->clients as $client) {				
								
				$ftpDir = $this->prepareDir($client->getHomeDir());
								
				foreach($xml[$this->timestamp] as $titleId => $fileName) {
                                    echo $fileName . PHP_EOL;
                                    echo $ftpDir. $titleId. '_' . $this->version .'.xml' . PHP_EOL;
					chmod($fileName, 0777);
                                    
					$result = copy($fileName, $ftpDir. $titleId. '_' . $this->version .'.xml');					
					if (!$result)
						{echo "<pre>";var_dump(error_get_last());echo "</pre>";}
					$this->tmpFiles[$titleId . '_' . $this->timestamp] = $fileName;
                                        
                                        chown($ftpDir. $titleId. '_' . $this->version .'.xml', $client->getFtpUsername());
                                        chgrp($ftpDir. $titleId. '_' . $this->version .'.xml', $client->getFtpUsername());
                                        chmod($ftpDir. $titleId. '_' . $this->version .'.xml', 0777);
                                        
                                        $clientFile = new ClientFile();
					$clientFile->setClientId($client->getId());
					$clientFile->setTitleId($titleId);
					$clientFile->setRevision(0);
					$clientFile->setLocalFile($ftpDir. $titleId. '_' . $this->version .'.xml');
					$clientFile->setReportId(0);
					$clientFile->setCreationTime(time());
					$clientFile->setArticleCount((int)$titles[$title['id']]['articles']->getCount());
					$clientFile->save();
                                        
                                        
                                        $connections = $client->getClientConnections();
    		
                                        foreach ($connections as $connection) {
                                            
                                            $connectionFile = new ClientConnectionFile();
    					    $connectionFile->setReportId(0);                                            
                                            $connectionFile->setClientId($client->getId());                                            
                                            $connectionFile->setClientConnectionId($connection->getId());
                                            $connectionFile->setTitleId($titleId);
                                            $connectionFile->setRevision(0);
                                            $connectionFile->setLocalFile($ftpDir. $titleId. '_' . $this->version .'.xml');
                                            $connectionFile->setCreationTime(time());
                                            $connectionFile->save();
                                        }
                                        
				}
				
				
				
				$afterGenerateFile = dirname(__FILE__) . '/generate/customClient/syndGenerate_' . $client->getId() . '.php';
					
				if(file_exists($afterGenerateFile)) {
					require_once $afterGenerateFile;
					$params = array("ftpDir"=> $ftpDir, "generateType"=>$client->getGenerateType());
					$className = "syndGenerate_" . $client->getId();
					$obj = new $className($params);
					$obj->afterGenerate();
				}
                            
        if('microsoft_newsml' == $client->getXmlType()) {
          foreach($mic_xml[$this->timestamp] as $titleId => $fileName) {
            $ftpDir = $this->prepareDir($client->getHomeDir());
            chmod($fileName, 0777);
            $result = copy($fileName, $ftpDir. $titleId.'_'.$this->version.'.xml');
          }
        }
         if('mrss' == $client->getXmlType()) {
          foreach($mrss_xml[$this->timestamp] as $titleId => $fileName) {
            $ftpDir = $this->prepareDir($client->getHomeDir());
            chmod($fileName, 0777);
            $result = copy($fileName, $ftpDir. $titleId.'_'.$this->version.'.xml');
          }
        }
        
			}
			
			$this->clearTmp();
			
		} // End function generateByTitle
		
		
		private function clearTmp() {
			
			if(is_array($this->tmpFiles)) {
				foreach($this->tmpFiles as $file) {
					//unlink($file);
				}
			}
		}
	
		
		/**
		 * Prepares the dir to write files to, and returns the full path
		 *
		 * @param string $clientHomeDir
		 */
		private function prepareDir($clientHomeDir) {
		
			$ftpDir = rtrim($clientHomeDir, '/').'/'.date('Y/m/d/');
			
			if(!is_dir($ftpDir)) {
				mkdir($ftpDir, 0777, true);
				//mkdir($ftpDir, 0755, true);
			}			
			return $ftpDir;
		}


		
		
		public function getArticles($fromDateTime, $toDateTime) {	
					
			$conn = mysql_connect(self::$DB_HOST, self::$DB_USER, self::$DB_PASS, true) or die("\n\nCannot connect to database\n\n");
			mysql_select_db (self::$DB_NAME) or die("\n\nDatabase doesn't exists\n\n");
	
			$titles = array();
			foreach ($this->titles as $titleRow) {
								
				$id 						= $titleRow->getId();
				$titles[$id]['id']          = $id;
				$titles[$id]['providerName']= $titleRow->getName();
				$titles[$id]['copyright']   = $titleRow->getCopyright();
				$titles[$id]['webSite']     = $titleRow->getWebsite();
					
				
				// get title's articles				
				$sql = 'SELECT article.*, original_article_category.cat_name, article_original_data.reference FROM article left join article_original_data on article_original_data.id = article.article_original_data_id left join original_article_category on article_original_data.original_cat_id = original_article_category.id WHERE ' . ArticlePeer::TITLE_ID.' = '.$id; ;
				$sql.= " AND parsed_at BETWEEN '$fromDateTime' AND '$toDateTime' ";

				$articlesResult = mysql_query($sql, $conn) ;				
				$titles[$id]['articles']  = new syndGenerateMySqlIterator($articlesResult, new ArticleArrayFormater($this->model));
								
				if(!mysql_num_rows($articlesResult)) {					
					unset($titles[$id]);
				}

			}
			return $titles;
		}
   		public function getArticlesByIds($article_ids= null) {
        if(!$article_ids)
          return;
			$conn = mysql_connect(self::$DB_HOST, self::$DB_USER, self::$DB_PASS, true) or die("\n\nCannot connect to database\n\n");
			mysql_select_db (self::$DB_NAME) or die("\n\nDatabase doesn't exists\n\n");
	
			$titles = array();
			foreach ($this->titles as $titleRow) {
								
				$id 						= $titleRow->getId();
				$titles[$id]['id']          = $id;
				$titles[$id]['providerName']= $titleRow->getName();
				$titles[$id]['copyright']   = $titleRow->getCopyright();
				$titles[$id]['webSite']     = $titleRow->getWebsite();
					
				
				// get title's articles				
				$sql = 'SELECT article.*, original_article_category.cat_name FROM article left join article_original_data on article_original_data.id = article.article_original_data_id left join original_article_category on article_original_data.original_cat_id = original_article_category.id WHERE ' . ArticlePeer::TITLE_ID.' = '.$id; ;
        if(is_array($article_ids) && $article_ids){
          $aids = implode(',', $article_ids);
          $sql.= " AND article.id in ($aids)";
        }
				$articlesResult = mysql_query($sql, $conn) ;				
				$titles[$id]['articles']  = new syndGenerateMySqlIterator($articlesResult, new ArticleArrayFormater($this->model));
								
				if(!mysql_num_rows($articlesResult)) {					
					unset($titles[$id]);
				}

			}
			return $titles;
		}             
                		
	} // End class syndGenerate

	
	class ArticleArrayFormater implements syndGenerateArrayFormater
	{
		private $model;
		public function __construct($model)
		{
			$this->model = $model;
		}

		private function getLanguage($langId)
		{
			static $languages = array();
			if(isset($languages[$langId])) {
				return $languages[$langId];
			}
			$lang = $this->model->getLanguage($langId);
			if($lang) {
				$languages[$lang->getId()] = $lang->getName();
			}
			return ($lang ? $lang->getName() : '');
		}
		
		public function format($articleRow)
		{
			$artId = $articleRow['id'];
			
			$returnVal['id'] = $artId;
			$returnVal['headline'] 		= $articleRow['headline'];
			$returnVal['byLine']   		= $articleRow['author'];
			$returnVal['language'] 		= $this->getLanguage($articleRow['language_id']);
			$returnVal['category'] 		= $articleRow['iptc_id'];
			$returnVal['abstract'] 		= $articleRow['summary'];
			$returnVal['text'] 			=& $articleRow['body'];
			$returnVal['date'] 			= trim($articleRow['date']) ? $articleRow['date'] : $articleRow['parsed_at'];
			$returnVal['parsed_at'] 			= $articleRow['parsed_at'];
			$returnVal['originalCategory'] = trim($articleRow['cat_name']);
			$returnVal['reference'] = trim($articleRow['reference']);

			$headlineImage    	  		= $this->model->getArticleHeadlineImage($artId);
			$returnVal['headlineImageCaption'] = $headlineImage ? $headlineImage->getImageCaption() : '';
			$returnVal['headlineImage'] = $headlineImage ? $headlineImage->getImgName() : '';
			$returnVal['videos'] 		= $this->model->getArticleVideos($artId);
			$returnVal['images'] 		= $this->model->getArticleImages($artId);
			return $returnVal;
		}
		
		
		
	} // End class 
	
	
	
	class resend extends syndSend {
		
	}
