<?php
	//require_once dirname(__FILE__).'/../ez_autoload.php';
	//$mod = new syndModel();
	//print_r($mod->getArticlesTree(array('title'=>'11', 'lang'=>'1, 2')));die();

	class syndModel
	{
		/**
		 * get a single object for syndModel (Singlton)
		 *
		 * @return syndModel object
		 */
		public static function getModelInstance()
		{
			static $instance;
			if(!$instance) {
				$instance = new syndModel();
			}
			return $instance;
		}

		/**
		 * get IPTC category id from maped category name
		 * 
		 * @param String $mapCategoryName
		 * @return int IPTC category id
		 */
		public function getIptcCategoryId($mapCategoryName)
		{
			$c1 = new Criteria();
            $c1->add(OriginalArticleCategoryPeer::CAT_NAME, $mapCategoryName);
            $category = OriginalArticleCategoryPeer::doSelectOne($c1);
			if($category) {
				//get category mapping object
				$c2 = new Criteria();
				$c2->add(CategoryMappingPeer::IPTC_ID, $category->getId());
				$iptcCategory = CategoryMappingPeer::doSelectOne($c2);
				if($iptcCategory) {
		            return $iptcCategory->getIptcId();
				} else {
					return 0;
				}
			}
			return 0;
		}
		
		/**
		 *  get original article category id from its name
		 *
		 * @param String $originalCategoryString
		 * @param Integer $titleId
		 * @return Integer category id 
		 */
		public function getOriginalCategoryId($originalCategoryString, $titleId)
		{
			$c = new Criteria();
			$c->add(trim(OriginalArticleCategoryPeer::CAT_NAME), trim($originalCategoryString), Criteria::EQUAL);
			$c->add(OriginalArticleCategoryPeer::TITLE_ID, $titleId);
			
			$result = OriginalArticleCategoryPeer::doSelectOne($c);
			
			if(is_object($result)){
				return $result->getId();
			} else {
				return false;
			}
		}

		/**
		 * get parsing state
		 *
		 * @param int $rowId
		 * @return ParsingState Object
		 */
		public function getParsingStateById($rowId)
		{
			$rowId = (int)$rowId;
			$parsingState = ParsingStatePeer::retrieveByPK($rowId);
			return $parsingState;
		}
		

		/**
		 * get languge by id
		 *
		 * @param int $id
		 * @return language object
		 */
		public function getLanguage($id)
		{
			return LanguagePeer::retrieveByPK($id);
		}

		/**
		 * get language id by language code
		 *
		 * @param string $langCode
		 * @return int language id
		 */
		public function getLanguageId($langCode)
		{
			$c = new Criteria();
			$c->add(LanguagePeer::CODE, $langCode);
			$lang = LanguagePeer::doSelectOne($c);
			if($lang) {
				return $lang->getId();
			}
			return 0;
		}
		
		/**
		 * get images replacements paths after copying the image from it's location 
		 * to this server location
		 * 
		 * @param array $imgs images original paths
		 * @param string $hostUrl images host to be used if there is no host specified in image path
		 * @param int $titleId
		 * @return array()
		 */
		public function getImgsReplacements($imgs, $hostUrl, $titleId)
		{
			$returnVal = array();
			foreach($imgs as $img) {
				$dirName = trim(dirname($img), '.');
				if(!$hostUrl) {
					$hostUrl = $dirName;
				} else {
					$parsed = parse_url($img);
					if(!isset($parsed['host'])) {
						$hostUrl  = rtrim($hostUrl, '/').'/';
						$hostUrl .= ltrim($dirName, '/');
					} else {
						$hostUrl  = $dirName;
					}
				}
				$imgName  = basename($img);
				$returnVal[$img] = $this->getImgReplacement($imgName, $hostUrl, $titleId);
			}
			return $returnVal;
		}

		/**
		 * get single image replacement
		 * @param string $img image base name 
		 * @param string $path full image path
		 * @param int $titleId
		 * @return string $image replacement path
		 */
		public function getImgReplacement($img, $path, $titleId)
		{
			$pubId      = $this->getPublisherIdByTitleId($titleId);
			$storingDir = "$pubId/$titleId/".((abs(crc32($img)) % 100)+1).'/';
			if(!is_dir(IMGS_PATH.$storingDir)) {
				mkdir(IMGS_PATH.$storingDir, 0777, true);
				//mkdir(IMGS_PATH.$storingDir, 0770, true);
			}
			$sourcePath = "$path/$img";
			$pathInfo   = pathinfo($sourcePath);

			do {
				$destPath   = IMGS_PATH.$storingDir.$this->getUniqueName();
				if(isset($pathInfo['extension'])) {
					$destPath .= '.'.$pathInfo['extension'];
				}
			} while(file_exists($destPath));
			
			if(copy($sourcePath, $destPath)) {
				return IMGS_PATH.$storingDir.basename($destPath);
			}
			return '';
		}
		
		/**
		 * @author Mohammad Mousa
		 * get single video replacement
		 * @param string $vid video base name 
		 * @param string $path full video path
		 * @param int $titleId 
		 * @return string $video replacement path
		 */
		public function getVidReplacement($vid, $path, $titleId)
		{
			$pubId      = $this->getPublisherIdByTitleId($titleId);
			$storingDir = "$pubId/$titleId/".((abs(crc32($vid)) % 100)+1).'/';
			if(!is_dir(VIDEOS_PATH.$storingDir)) {
				mkdir(VIDEOS_PATH.$storingDir, 0777, true);
			}
			$sourcePath = "$path/$vid";
			$pathInfo   = pathinfo($sourcePath);
			do {
				$destPath   = VIDEOS_PATH.$storingDir.$this->getUniqueName();
				if(isset($pathInfo['extension'])) {
					$destPath .= '.'.$pathInfo['extension'];
				}
			} while(file_exists($destPath));

			if(copy($sourcePath, $destPath)) {
				return VIDEOS_PATH.$storingDir.basename($destPath);
			}
			return '';
		}

		/**
		 * get publisher id by title id
		 * 
		 * @param int $titleId
		 * @return int publisher id
		 */
		public function getPublisherIdByTitleId($titleId)
		{
			$c = new Criteria();
			$c->add(TitlePeer::ID, $titleId);
			$title = TitlePeer::doSelectOne($c);
			return $title->getPublisherId();
		}

		/**
		 * add image
		 * 
		 * @param $params record fields
		 * @return int row id
		 */
		public function addImage($params)
		{
			$img = new Image();
			$img->setArticleId($params['article_id']);
            $img->setImgName($params['img_name']);
            $img->setImageCaption($params['image_caption']);
            $img->setOriginalName($params['original_name']);
			$img->setIsHeadline($params['is_headline']);
			$img->setImageType(syndParseHelper::getFileExtension($params['img_name']));
			$img->setMimeType(mime_content_type( str_replace(IMGS_HOST, IMGS_PATH, $params['img_name'])));
            $img->save();
			return $img->getId();
		}
		
		public function addOrUpdateArticleVideo($articleId, $video)
		{
			$params['article_id']    = $articleId;
			$params = array_merge($params, $video);
			
			//check if there exist a same video
			$oldVideo = Video::getVideoByOriginalName($articleId, $video['original_name']);
			
			if(!$oldVideo) {
				$this->addVideo($params);
			} else {
				
			}
		}
		
		public function addOrUpdateArticleImage($articleId, $image)
		{
			$params['article_id']    = $articleId;
			$params = array_merge($params, $image);
			
			//check if there exist a same image
			$oldImage = Image::getImageByOriginalName($articleId, $image['original_name']);
			
			if(!$oldImage) {
				$this->addImage($params);
			} else {
				
			}
		}
		
		/**
		 * add video
		 * 
		 * @param $params record fields
		 * @return int row id
		 */
		public function addVideo($params)
		{			
			$vid = new Video();
			$vid->setArticleId($params['article_id']);
            $vid->setVideoName($params['video_name']);
            $vid->setVideoCaption($params['video_caption']);
            $vid->setOriginalName($params['original_name']);
            $vid->setBitRate($params['bit_rate']);
            $vid->setAddedTime($params['added_time']);
            $vid->setVideoType(syndParseHelper::getFileExtension($params['original_name']));
            $vid->setMimeType(mime_content_type($params['original_name']));
            $vid->save();
            
			return $vid->getId();
		}
		

		/**
         * get article by original article id
         *
         * @param $originId
         * @return Article object
         */
         public function getArticleByOriginalDataId($originId)
         {
         	$c = new Criteria();
            $c->add(ArticlePeer::ARTICLE_ORIGINAL_DATA_ID, $originId);
            $article = ArticlePeer::doSelectOne($c);
            return $article;
         }

		/**
         * get original article by article original id
         *
         * @param $id
         * @return ArticleOriginalData object
         */
         public function getArticleByOriginalId($id)
		 {
		 	$c = new Criteria();
            $c->add(ArticleOriginalDataPeer::ORIGINAL_ARTICLE_ID, $id);
            $article = ArticleOriginalDataPeer::doSelectOne($c);
            return $article;
		 }

		/**
		 * add original article
		 * 
		 * @param $params record fields
		 * @return int row id
		 */
		public function addOriginalArticle($params)
		{
			$defaultArt = array(	'original_article_id'=>'',
						'original_source'=>'',
						'issue_number'=>'',
						'page_number'=>'',
						'reference'=>'',
						'extras'=>'',
						'hijri_date'=>'',
						'revision_num'=>''
						);
			$params = array_merge($defaultArt, $params);

			$article = new ArticleOriginalData();
			$article->setOriginalArticleId($params['original_article_id']);
			$article->setOriginalSource($params['original_source']);
			$article->setIssueNumber($params['issue_number']);
			$article->setPageNumber($params['page_number']);
			$article->setReference($params['reference']);
			$article->setExtras(serialize($params['extras']));
			$article->setHijriDate($params['hijri_date']);
			$article->setRevisionNum($params['revision_num']);
			$article->setOriginalCatId($params['original_cat_id']);
			$article->save();
			
			return $article->getId();
		}
		
		

		/**
         * update original article
         *
         * @param $params record fields
         * @return int row id
         */
         public function updateOriginalArticle($id, $params)
         {
         	$defaultArt = array(    'original_article_id'=>'',
                                    'original_source'=>'',
                                    'issue_number'=>'',
                                    'page_number'=>'',
                                    'reference'=>'',
                                    'extras'=>'',
                                    'hijri_date'=>'',
                                    'revision_num'=>''
                                    );
            $params = array_merge($defaultArt, $params);

            $article = ArticleOriginalDataPeer::retrieveByPK($id);
			if($article) {
				$article->setOriginalArticleId($params['original_article_id']);
        	    $article->setOriginalSource($params['original_source']);
                $article->setIssueNumber($params['issue_number']);
	            $article->setPageNumber($params['page_number']);
        	    $article->setReference($params['reference']);
                $article->setExtras(serialize($params['extras']));
	            $article->setHijriDate($params['hijri_date']);
	            //$article->setRevisionNum($params['revision_num']);
				$article->setOriginalCatId($params['original_cat_id']);
				return $article->save();
        	}
			return false;
         }

		/**
		 * add article
		 * 
		 * @param $params record fields
		 * @return int row id
		 */
		public function addArticle($params)
		{
			$defaultArt = array(	'iptc_id' => '',
						'title_id' => '',
						'original_data_id'=>'',
						'lang_id'=>'',
						'headline'=>'',
						'abstract'=>'',
						'story'=>'',
						'author'=>'',
						'date'=>'',
						'parsed_at'=>'',
						'has_time'=>''
						);
			$params = array_merge($defaultArt, $params);
		//	print_r($params);
			$article = new Article();
			$article->setIptcId($params['iptc_id']);
			$article->setTitleId($params['title_id']);
			$article->setArticleOriginalDataId($params['original_data_id']);
			$article->setLanguageId($params['lang_id']);
			$article->setHeadline($params['headline']);
			$article->setSummary($params['abstract']);
			$article->setBody($params['story']);
			$article->setAuthor($params['author']);
			if($params['date']) {
				$article->setDate($params['date']);
			}
			$article->setParsedAt($params['parsed_at']);
			$article->setHasTime($params['has_time']);

			try{
				$article->save();
			} catch (Exception $e) {
				
				return false;
			}
			return $article->getId();
		}

		/**
         * update article
         *
         * @param $params record fields
         * @return int row id
         */
         public function updateArticle($id, $params)
         {
         	$defaultArt = array(    'iptc_id' => '',
                                     'title_id' => '',
                                     'original_data_id'=>'',
                                     'lang_id'=>'',
                                     'headline'=>'',
                                     'abstract'=>'',
                                     'story'=>'',
                                     'author'=>'',
                                     'date'=>'',
                                     'parsed_at'=>'',
                                     'has_time'=>''
              );
              
              $params = array_merge($defaultArt, $params);

              $article = ArticlePeer::retrieveByPK($id);
              
              if(($article->getHeadline()==$params['headline']) && ($article->getSummary()==$params['abstract']) && ($article->getBody()==$params['story'])&& ($article->getAuthor()==$params['author'])) {
              	return false;
              }
              
              if($article) {
              	$article->setIptcId($params['iptc_id']);
        	    $article->setTitleId($params['title_id']);
                $article->setArticleOriginalDataId($params['original_data_id']);
                $article->setLanguageId($params['lang_id']);
	            $article->setHeadline($params['headline']);
        	    $article->setSummary($params['abstract']);
                $article->setBody($params['story']);
	            $article->setAuthor($params['author']);

	            if($params['date']) {
                	$article->setDate($params['date']);
                }
                
                $article->setParsedAt($params['parsed_at']);
        	    $article->setHasTime($params['has_time']);
                return $article->save();
              }
              return false;
         }

		/**
		 * get article headline image
		 *
		 * @param int $articleId
		 */
		public function getArticleHeadlineImage($articleId)
		{
			$c = new Criteria();
            $c->add(ImagePeer::ARTICLE_ID, $articleId);
			$c->add(ImagePeer::IS_HEADLINE, 1);
            $image = ImagePeer::doSelectOne($c);
            return $image;
		}
		
		/**
		 * get article images without the headline
		 *
		 * @param int $articleId
		 * @return Image Object array
		 */
		public function getArticleImages($articleId)
		{
			$c = new Criteria();
            $c->add(ImagePeer::ARTICLE_ID, $articleId);
			$c->add(ImagePeer::IS_HEADLINE, 0);
            $images = ImagePeer::doSelect($c);
            return $images;
		}
		
		/**
		 * get article videos
		 *
		 * @param int $articleId
		 * @return Video Object array
		 */
		public function getArticleVideos($articleId)
		{
			$c = new Criteria();
            $c->add(VideoPeer::ARTICLE_ID, $articleId);
			return VideoPeer::doSelect($c);
		}

		/**
		 * get all clients
		 *
		 */
		public function getAllClients()
		{
			return ClientPeer::doSelect(new Criteria());
		}
		
		/**
		 * get all active clients
		 *
		 */
		public function getActiveClients()
		{
			$c = new Criteria();
			$c->add(ClientPeer::IS_ACTIVE, 1);
			return ClientPeer::doSelect($c);
		}
		
		/**
		 * get all active PUSH clients
		 *
		 */
		public function getPushClients()
		{
			$c = new Criteria();
			$c->add(ClientPeer::IS_ACTIVE, 1);
			$c->add(ClientPeer::CONNECTION_TYPE , 'ftp_push');
			return ClientPeer::doSelect($c);
		}
		
		
		/**
		 * get unique image file name
		 *
		 * @return string
		 */
		private function getUniqueName()
		{
			return microtime(1) * 100;
		}

		
		/**
		 * Get the URL of this title
		 *
		 * @param int $titleId
		 * @return string
		 */
		public function getUrl($titleId)
		{
			$c = new Criteria();
			$c->add(TitleConnectionPeer::TITLE_ID , $titleId);
			if (!is_object($title = TitleConnectionPeer::doSelectOne($c))) {
				return false;
			} else {
				return $title->getUrl();				
			}
		}

		/**
		 * Get title by id
		 *
		 * @param int $titleId
		 * @return title object
		 */
		public function getTitleById($titleId)
		{
			return TitlePeer::retrieveByPK($titleId);
		}
		
		
		
		/**
		 * Get the Title name of this title
		 *
		 * @param int $titleId
		 * @return string
		 */
		public function getTitleName($titleId)
		{			
			$c = new Criteria();		
			$c->add(TitlePeer::ID , $titleId);
			if (!is_object($title = TitlePeer::doSelectOne($c))) {
				return false;
			} else {
				return $title->getName();
			}
		}
		
	 	public function getTypeName ($id, $type)
	    {
	    	if ($type == 'title') {
	    		$c = new Criteria();
				$c->add(TitlePeer::ID, $id);
					
				if (! is_object($title = TitlePeer::doSelectOne($c))) {
					return false;
				} else {
					return $title->getName();
				}
				
				
	    	} else {
	    		
	    		$c = new Criteria();
				$c->add(ClientPeer::ID, $id);

				if (! is_object($client = ClientPeer::doSelectOne($c))) {
					return false;
				} else {
					return $client->getClientName();
				}
	    	}
	    }
	     
		/**
		 * Get the Client name of this title
		 *
		 * @param int $clientId
		 * @return string
		 */
		public function getClientName($clientId)
		{			
			$c = new Criteria();		
			$c->add(ClientPeer::ID , $clientId);
			if (!is_object($client = ClientPeer::doSelectOne($c))) {
				return false;
			} else {
				return $client->getName();
			}
		}
		
		
		
		/**
		 * Get the Home directory of the title
		 *
		 * @param int $titleId
		 * @return string
		 */
		public function getHomeDir($titleId)
		{
			$c = new Criteria();
			$c->add(TitlePeer::ID, $titleId);
			if (!is_object($title = TitlePeer::doSelectOne($c))) {
				return false;
			} else {
				return $title->getHomeDir();
			}
		}
		
		
		/**
		 * Get the Working directory of the title
		 *
		 * @param int $titleId
		 * @return string
		 */
		public function getWorkDir($titleId)
		{
			$c = new Criteria();
			$c->add(TitlePeer::ID, $titleId);
			if (!is_object($title = TitlePeer::doSelectOne($c))) {
				return false;
			} else {
				return $title->getWorkDir();
			}
		}
		
		
		/**
		 * Check whether the Title is active or not
		 *
		 * @param int $titleId
		 * @return boolean
		 */
		public function isTitleActive($titleId)
		{
			$c = new Criteria();
			$c->add(TitlePeer::ID, $titleId);
			if (!is_object($title = TitlePeer::doSelectOne($c))) {
				return false;
			} else {
				return (bool) $title->getIsActive();
			}
		}
		
		
		/**
		 * Check whether the Title is newswire or not
		 *
		 * @param int $titleId
		 * @return boolean
		 */
		public function isTitleNewswire($titleId)
		{
			$c = new Criteria();
			$c->add(TitlePeer::ID, $titleId);
			if (!is_object($title = TitlePeer::doSelectOne($c))) {
				return false;
			} else {
				return (bool) $title->getIsNewswire();
			}
		}
		
		/**
		 * Check whether the Client is active or not
		 *
		 * @param int $titleId
		 * @return boolean
		 */
		public function isClientActive($titleId)
		{
			$c = new Criteria();
			$c->add(ClientPeer::ID, $titleId);
			if (!is_object($title = ClientPeer::doSelectOne($c))) {
				return false;
			} else {
				return (bool) $title->getIsActive();
			}
		}
		
		/**
		 * Get the Username for connection of this title
		 *
		 * @param int $titleId
		 * @return string
		 */
		public function getUsername($titleId)
		{			
			$c = new Criteria();
			$c->add(TitleConnectionPeer::TITLE_ID , $titleId);
			if(!is_object($title = TitleConnectionPeer::doSelectOne($c))) {
				return false;
			} else {
				return $title->getUsername();
			}
		}
		
		
		/**
		 * Get the Password for connection of this title
		 *
		 * @param int $titleId
		 * @return string
		 */
		public function getConnectionType($titleId)
		{		
			$c = new Criteria();
			$c->add(TitleConnectionPeer::TITLE_ID , $titleId);
			if(!is_object($title = TitleConnectionPeer::doSelectOne($c))){
				return false;
			} else  {			
				return $title->getConnectionType();
			}
		}
		
		
		/**
		 * Get the Password for the connection of this title
		 *
		 * @param int $titleId
		 * @return string
		 */
		public function getPassword($titleId)
		{		
			$c = new Criteria();
			$c->add(TitleConnectionPeer::TITLE_ID , $titleId);
			if (!is_object($title = TitleConnectionPeer::doSelectOne($c))) {
				return false;
			} else {
				return $title->getPassword();
			}
		}
		
		/**
		 * Get the ftp port for the connection of this title
		 *
		 * @param int $titleId
		 * @return Integer
		 */
		public function getFtpPort($titleId)
		{		
			$c = new Criteria();
			$c->add(TitleConnectionPeer::TITLE_ID , $titleId);
			if (!is_object($title = TitleConnectionPeer::doSelectOne($c))) {
				return false;
			} else {
				$ftpPort = $title->getFtpPort();
				return !$ftpPort ? 21 : $ftpPort;
			}
		}
		
		/**
		 * add gather report
		 * 
		 * @param $params record fields
		 * @return int row id
		 */
		public function gatherAddReport($params)
		{
			$report = new ReportGather();
			$report->setTitleId($params['TitleId']);
			$report->setNumberFiles($params['NumberFiles']);
			$report->setLatestStatus($params['LatestStatus']);
			$report->setFileSize($params['FileSize']);
			$report->setError($params['Error']);
			$report->save();
			$id = $report->getId();
			
			//Adding the gather report to table "report"
			$report = new Report();
			$report->setTitleId($params['TitleId']);
			$report->setReportGatherId($id);
			$report->save();

			return $id; 
		}
		
		/**
		 * update gather report
		 * 
		 * @param $params record fields
                 */
		public function gatherUpdateReport($params)
		{
			$report = ReportGatherPeer::retrieveByPK($params['Id']);
			if ($report) {
				$report->setTitleId($params['TitleId']);
				//$report->setNumberFiles($params['NumberFiles']);
				$report->setLatestStatus($params['LatestStatus']);
				//$report->setFileSize($params['FileSize']);
				$report->setError($params['Error']);
				$report->save();
			}
		}
		
		/**
		 * add parse report
		 * 
		 * @param $params record fields
		 * @return int row id
		 */
		public function parseAddReport($params)
		{
			$report = new ReportParse();
			$report->setArticlesIds($params['ArticlesIds']);
			$report->setDate($params['Date']);
			$report->setParserError($params['ParserError']);
			$report->setParserStatus($params['ParserStatus']);
			$report->setTitleId($params['TitleId']);
			$report->setArticleNum($params['ArticleNum']);
			$report->save();
			return $report->getId();
		}
		
		/**
		 * update generate report
		 * 
		 * @param $params record fields
         */
		public function generateUpdateReport($params)
		{
			$report = ReportGeneratePeer::retrieveByPK($params['Id']);
			if ($report) {
				$report->setTitleId($params['TitleId']);
				$report->setFile(isset($params['File']) ? $params['File'] : '');
				$report->setArticlesCount(isset($params['ArticlesCount']) ? $params['ArticlesCount'] : 0);
				$report->setError($params['Error']);
				$report->setStatus($params['Status']);
				$report->save();
			}
		}
		
		/**
		 * add generate report
		 * 
		 * @param $params record fields
		 * @return int row id
		 */
		public function generateAddReport($params)
		{
			$report = new ReportGenerate();
			$report->setError($params['Error']);
			$report->setStatus($params['Status']);
			$report->save();
			
			return $report->getId();
		}
		
		
		/**
		 * update parse report
		 *
		 * @param $params record fields
		 */
		public function parseUpdateReport($params)
		{
			$report = ReportParsePeer::retrieveByPK($params['Id']);
			if ($report) {
				$report->setArticlesIds($params['ArticlesIds']);
				$report->setDate($params['Date']);
				$report->setParserError($params['ParserError']);
				$report->setParserStatus($params['ParserStatus']);
				$report->setTitleId($params['TitleId']);
				$report->setArticleNum($params['ArticleNum']);
				return $report->save();
				
			}			
			return false;
		}
		
		/**
		 * updating "parse start" status
		 * 
		 * @param int $rowId
		 */
		public function parseStartUpdateState($rowId) {
			//Mark when this parser started
			$parseRecord = ParsingStatePeer::retrieveByPK($rowId);
			if ($parseRecord) {
				$parseRecord->setParserStartedAt(date('Y-m-d H:i:s'));
				$parseRecord->save();
			}
		}
		
		/**
                 * updating "parse finish" status
                 * 
                 * @param int $rowId
                 */
		public function parseFinishUpdateState($rowId) {
			//Mark that this record has been processed
			$parseRecord = ParsingStatePeer::retrieveByPK($rowId);
			if ($parseRecord) {
				$parseRecord->setParserFinishedAt(date('Y-m-d H:i:s'));
				$parseRecord->setCompleted(1);
				$parseRecord->setLocked(0);
				$parseRecord->save();
			}
		}
		
		/**
		 * Check Title copy status if first time or not
		 *
		 * @param int $titleId
		 * @return bool
		 */		
		public function isTitleFirstGather($titleId)
		{	
			$c = new Criteria();
			$c->add(ParsingStatePeer::TITLE_ID , $titleId);
			$title = ParsingStatePeer::doSelectOne($c);
			if (!is_object($title)) {
				//this title is new			
				return true;
			} else {
				//this title is not new			
				return false;
			}
		}
		
		/**
		 * Check Title if is an Issue
		 *
		 * @param int $titleId
		 * @return bool
		 */		
		public function dirLayout($titleId)
		{	
			$c = new Criteria();
			$c->add(TitlePeer::ID, $titleId);
			if (!is_object($title = TitlePeer::doSelectOne($c))) {
				return false;
			} else {
				return $title->getDirectoryLayout();
			}			
		}

		/**
		 * Get the number of retries for this Title
		 *
		 * @param int $titleId
		 * @return int
		 */
		public function getGatherRetries($titleId)
		{		
			$c = new Criteria();
			$c->add(TitlePeer::ID, $titleId);
			if (!is_object($title = TitlePeer::doSelectOne($c))) {
				return false;
			} else {
				return $title->getGatherRetries();
			}
		}
		
		/**
		 * Get the Parser Working directory of the title
		 *
		 * @param int $titleId
		 * @return string
		 */
		public function getParseWorkDir($titleId)
		{
			$c = new Criteria();
			$c->add(TitlePeer::ID, $titleId);
			if (!is_object($title = TitlePeer::doSelectOne($c))) {
				return false;
			} else {
				return $title->getParseDir();
			}
		}
		
		/**
		 * Get the number of retries for this Title
		 *
		 * @param int $titleId
		 * @return int
		 */
		public function getNextTryTimer($titleId)
		{		
			$c = new Criteria();
			$c->add(TitlePeer::ID, $titleId);
			if (!is_object($title = TitlePeer::doSelectOne($c))) {
				return false;
			} else {
				return $title->getNextTryTimer();
			}
		}	

		/**
		 * get client filter
		 * 
		 * @param int $clientId
		 * @return array
		 */
		public function getClientRules($clientId)
		{
			$c = new Criteria();
			$c->add(ClientRulePeer::CLIENT_ID, $clientId);
			$clientRules = ClientRulePeer::doSelect($c);
			if($clientRules) {
				$rules = array();
				foreach($clientRules as $clientRule) {
					$ruleFilters = $clientRule->getClientRuleFilters();
					if($ruleFilters) {
						$filters = array();
						foreach($ruleFilters as $ruleFilter) {
							$filters[$ruleFilter->getFilterType()] = $ruleFilter->getFilterValue();
						}
						if(count($filters)) {
							$rules[] = $filters;
						}
					}
				}
				return $rules;
			}
			return array();
		}



		public function getTitlesClientsRules($clientId)
		{
			$c = new Criteria();
			$c->add(TitlesClientsRulesPeer::CLIENT_ID, $clientId);
			$titlesClientsRules = TitlesClientsRulesPeer::doSelect($c);

			if( $titlesClientsRules ) {
				$rules = array();
				$filters = array();
				$titlesClientTakeFromThem = array();

				foreach($titlesClientsRules as $clientRule) {
					array_push( $titlesClientTakeFromThem, $clientRule->getTitleId() );
				}
				$titlesClientTakeFromThem = implode(',', $titlesClientTakeFromThem );
				$filters['title'] = $titlesClientTakeFromThem;
				
				if(count($titlesClientTakeFromThem)) {
					$rules[] = $filters;
				}

			return $rules;
			}
			return array();
		}
		
		
		/***************************************************************************
		 * get all title id from article table which parsed in specific date
		 * and put the results in tree
		 *
		 * @param string $date
		 * @return array
		public function getArticlesTree($rules=array())
		{
			$result = array();
			$con = Propel::getConnection(ArticlePeer::DATABASE_NAME);
			//////////////////////////////////////////////////////////////////////////////////
			// create filters
			//////////////////////////////////////////////////////////////////////////////////
			$articlesFilters = array();
			$titlesFilters   = array();
			$titlesFilter    = '';
			$articlesFilter  = '';

			
			if($filter['title']) {
				$titlesFilters[]   = TitlePeer::ID." IN ({$filter['title']})";
			}

			if($filter['category']) {
				$articlesFilters[] = ArticlePeer::IPTC_ID." IN ({$filter['category']})";
			}
			if($filter['lang']) {
				$articlesFilters[] = ArticlePeer::LANGUAGE_ID." IN ({$filter['lang']})";
			}
			if($filter['date']) {
				$minDate = date('Y-m-d', strtotime($date)). ' 00:00';
				$maxDate = date('Y-m-d', strtotime($date)). ' 23:59';
				$articlesFilters[] = ArticlePeer::PARSED_AT. " >= '$minDate'";
				$articlesFilters[] = ArticlePeer::PARSED_AT. " <= '$maxDate'";
			}

			if(count($articlesFilters)) {
				$articlesFilter = implode(' AND ', $articlesFilters);
			}

			if(count($titlesFilters)) {
				$titlesFilter   = implode(' AND ', $titlesFilters);
			}



			//////////////////////////////////////////////////////////////////////////////////
			// get articles from articles table
			//////////////////////////////////////////////////////////////////////////////////
			$sql =  'SELECT * FROM '.TitlePeer::TABLE_NAME;
			if($titlesFilter) {
				$sql .= ' WHERE '.$titlesFilter;
			}

			$titlesResult = $con->executeQuery($sql, ResultSet::FETCHMODE_ASSOC);
			while ($titlesResult->next()) {
				$id = $titlesResult->getString('id');
				$titles[$id]['id']           = $id;
				$titles[$id]['providerName'] = $titlesResult->getString('name');
				$titles[$id]['copyright']    = $titlesResult->getString('copyright');
				$titles[$id]['webSite']      = $titlesResult->getString('website');

				//get title articles
				$sql = 'SELECT * FROM '.ArticlePeer::TABLE_NAME .' WHERE '.ArticlePeer::TITLE_ID.' = '.$id;
				if($articlesFilters) {
					$sql .= ' AND '.$articlesFilter;
				}

				$articlesResult = $con->executeQuery($sql, ResultSet::FETCHMODE_ASSOC);
				while($articlesResult->next()) {
					$artId = $articlesResult->getString('id');
					$titles[$id]['articles'][$artId]['headline'] = $articlesResult->getString('headline');
					$titles[$id]['articles'][$artId]['byLine']   = $articlesResult->getString('author');
					$lang = $this->getLanguage($articlesResult->getString('language_id'));
					$titles[$id]['articles'][$artId]['language'] = $lang ? $lang->getName() : '';
					$titles[$id]['articles'][$artId]['category'] = $articlesResult->getString('iptc_id');
					$headlineImage    = $this->getArticleHeadlineImage($artId);
					$titles[$id]['articles'][$artId]['headlineImage'] = $headlineImage ? $headlineImage->getImageName() : '';
					$titles[$id]['articles'][$artId]['headlineImageCaption'] = $headlineImage ? $headlineImage->getImageCaption() : '';
					$titles[$id]['articles'][$artId]['abstract'] = $articlesResult->getString('summary');
					$titles[$id]['articles'][$artId]['text'] = $articlesResult->getString('body');
					$titles[$id]['articles'][$artId]['date'] = $articlesResult->getString('date');
				}

				if(!isset($titles[$id]['articles'])) {
					unset($titles[$id]);
				}
			}
			
			return $titles;

		}*********************************************************/
		
		/***********   Methods for the clients   *****************/
		
		/**
		 * Get the Data of this Client
		 *
		 * @param int $clientId
		 * @return array
		 */
		public function getClientFields($clientId)
		{		
			if (!is_object($client = ClientPeer::retrieveByPk($clientId))) {
				return false;
			} else {
				return $client;
			}
		}
		
		
		/**
		 * add gather report
		 * 
		 * @param $params record fields
		 * @return int row id
		 */
		public function sendAddReport($params)
		{
			$report = new ReportSend();
			//$report->setClientId($params['ClientId']);
			$report->setFilesCount($params['FilesCount']);
			$report->setSenderStatus($params['SenderStatus']);
			$report->setSenderMsg($params['SenderMsg']);
			$report->setStartTime($params['StartTime']);
			$report->setEndTime($params['EndTime']);		
			$report->save();
			return $report->getId();
		}
		
		/**
		 * update gather report
		 * 
		 * @param $params record fields
                 */
		public function sendUpdateReport($params)
		{
			$report = ReportSendPeer::retrieveByPK($params['Id']);
			if ($report) {
				//$report->setClientId($params['ClientId']);
				$report->setFilesCount($params['FilesCount']);
				$report->setSenderStatus($params['SenderStatus']);
				$report->setSenderMsg($params['SenderMsg']);
				$report->setStartTime($params['StartTime']);
				$report->setEndTime($params['EndTime']);				
				$report->save();
			}
		}
		
		public function isOriginalArticleCategory($titleId, $categoryString)
		{
			
			$c = new Criteria();
			$c->add(OriginalArticleCategoryPeer::TITLE_ID, $titleId);
			$c->add(trim(OriginalArticleCategoryPeer::CAT_NAME), trim($categoryString));
			try {
				$result = OriginalArticleCategoryPeer::doSelectOne($c);
			} catch(Exception $e) {
				return false;	
			}
			return is_object($result);			
		}
		
		public function addOriginalArticleCategory($titleId, $categegoryString)
		{
			if(!$titleId || !trim($categegoryString)) {
				return false;
			} else {
				
				$originalCategory = new OriginalArticleCategory();
  				$originalCategory->setCatName(trim($categegoryString));
  				$originalCategory->setTitleId($titleId);
  				$originalCategory->setAddedAt(date('Y-m-d H:i:s'));
  				return $originalCategory->save();
			}
		}
		
	 public function getTitleClients($titleId)
                {
                        $c = new Criteria();
                        $c->add(TitlesClientsRulesPeer::TITLE_ID, $titleId);
                        $titlesClientsRules = TitlesClientsRulesPeer::doSelect($c);
					$titlesClientTakeFromThem=array();
                        if( $titlesClientsRules ) {
                                foreach($titlesClientsRules as $clientRule) {
                                        array_push( $titlesClientTakeFromThem, $clientRule->getClientId() );
                                }
					return $titlesClientTakeFromThem;
                        }
                        return array();
                }	
	}

