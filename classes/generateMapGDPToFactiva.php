<?php

require_once dirname(__FILE__) . '/../ez_autoload.php';

////////////////////////////////////////////////////////////////////////////////////////
// configuraion This script Generate XMLs and push it to Factiva FTP, Should be run each 10 minutes.
///////////////////////////////////////////////////////////////////////////////////////
//	define('COPYRIGHT', " Provided by <a href='http://syndigate.info'>Syndigate.info</a>, an <a href='http://albawaba.com'>Albawaba.com</a> company");
defined('COPYRIGHT') OR define('COPYRIGHT', " Provided by SyndiGate Media Inc. (<a href='http://syndigate.info'>Syndigate.info</a>).");
define('STRIPPING_TAGS', false);
define('TMP_DIR', APP_PATH . 'tmp/resend/');

class syndGenerateFactiva {

  static public $DB_NAME;
  static public $DB_USER;
  static public $DB_PASS;
  static public $DB_HOST;
  private $model;
  private $tmpFiles;
  private $titlesIds, $clientsIds, $titles, $clients;

  public function __construct() {

    $this->model = syndModel::getModelInstance();

    $propelConf = Propel::getConfiguration();
    $dbConf = $propelConf['datasources']['propel']['connection'];

    self::$DB_USER = $dbConf['username'];
    self::$DB_PASS = $dbConf['password'];
    self::$DB_HOST = $dbConf['hostspec'];
    self::$DB_NAME = $dbConf['database'];

    $this->timestamp = time();
    $this->version = date('dhis');
  }

  public function generate() {
    $title_map = array(
        1492 => array('title_id' => 1492, 'max_per_patch' => 8, 'articles_count' => 0),
        223 => array('title_id' => 223, 'max_per_patch' => 16, 'articles_count' => 0),
        1007 => array('title_id' => 1007, 'max_per_patch' => 7, 'articles_count' => 0),
        1390 => array('title_id' => 1390, 'max_per_patch' => 9, 'articles_count' => 0),
        716 => array('title_id' => 716, 'max_per_patch' => 14, 'articles_count' => 0),
        399 => array('title_id' => 399, 'max_per_patch' => 11, 'articles_count' => 0),
        369 => array('title_id' => 369, 'max_per_patch' => 6, 'articles_count' => 0),
        55 => array('title_id' => 55, 'max_per_patch' => 11, 'articles_count' => 0),
        1310 => array('title_id' => 1310, 'max_per_patch' => 9, 'articles_count' => 0),
        243 => array('title_id' => 243, 'max_per_patch' => 13, 'articles_count' => 0),
        363 => array('title_id' => 363, 'max_per_patch' => 12, 'articles_count' => 0),
        365 => array('title_id' => 365, 'max_per_patch' => 8, 'articles_count' => 0),
        368 => array('title_id' => 368, 'max_per_patch' => 11, 'articles_count' => 0),
        1597 => array('title_id' => 367, 'max_per_patch' => 10, 'articles_count' => 0),
    );
    $conn = mysql_connect(self::$DB_HOST, self::$DB_USER, self::$DB_PASS, true) or die("\n\nCannot connect to database\n\n");
    mysql_select_db(self::$DB_NAME) or die("\n\nDatabase doesn't exists\n\n");
    $template = syndGenerateTemplatesManager::factory('NewsMl');
    $titles = array();
    $q = mysql_query("select * from article_jumperGDP where is_sent=0 order by mapped_title,article_id", $conn);
    $this->titles = array();
    $article_ids = array();
    $generated_history = array();
    while ($row = mysql_fetch_assoc($q)) {
      $article_ids[$row['mapped_title']][] = $row['article_id'];
    }
    foreach ($article_ids as $title => $articles) {
      $this->version = date('dhis');
      $max_per_patch = 5; //$title_map[$title]['max_per_patch'];
      $limited_articles = array_slice($articles, 0, $max_per_patch);
      $titles = $this->getArticlesByIds($title, $limited_articles);
      $fileName = $template->generate($titles);
      $this->tmpFiles[$fileName] = $fileName;
      // Send XML to Client ID 6
      $client = ClientPeer::retrieveByPK(6);
      $ftpDir = $this->prepareDir($client->getHomeDir());
      $result = copy($fileName, $ftpDir . $title . '_' . $this->version . '.xml');
      $generated_history[] = $ftpDir . $title . '_' . $this->version . '.xml';
      chown($ftpDir . $title . '_' . $this->version . '.xml', $client->getFtpUsername());
      chgrp($ftpDir . $title . '_' . $this->version . '.xml', $client->getFtpUsername());
      chmod($ftpDir . $title . '_' . $this->version . '.xml', 0777);
      // Upload XML to the Client Local and FTP
      $success = $this->uploadFileViaFTP(6, $ftpDir, $title . '_' . $this->version . '.xml');

      // Send XML to Client ID 1
      $client = ClientPeer::retrieveByPK(1);
      $ftpDir = $this->prepareDir($client->getHomeDir());
      $result = copy($fileName, $ftpDir . $title . '_' . $this->version . '.xml');
      chown($ftpDir . $title . '_' . $this->version . '.xml', $client->getFtpUsername());
      chgrp($ftpDir . $title . '_' . $this->version . '.xml', $client->getFtpUsername());
      chmod($ftpDir . $title . '_' . $this->version . '.xml', 0777);
      // Upload XML to the Client Local and FTP
      $success1 = $this->uploadFileViaFTP(1, $ftpDir, $title . '_' . $this->version . '.xml');
      if ($success) {
        // Mark articles as sent.
        $this->updateArticles($limited_articles);
      }
      sleep(3);
    }
    $this->clearTmp();
    var_dump($generated_history);
  }

  public function getArticlesByIds($mapped_title, $article_ids = null) {
    if (!$article_ids)
      return;
    $titleRow = TitlePeer::retrieveByPK($mapped_title);
    if (!is_object($titleRow)) {
      echo "Can't find the title $mapped_title" . chr(10);
    }
    $id = $titleRow->getId();
    $titles[$id]['id'] = $id;
    $titles[$id]['providerName'] = $titleRow->getName();
    $titles[$id]['copyright'] = $titleRow->getCopyright();
    $titles[$id]['webSite'] = $titleRow->getWebsite();
// get title's articles				
    $sql = 'SELECT article.*, original_article_category.cat_name FROM article left join article_original_data on article_original_data.id = article.article_original_data_id left join original_article_category on article_original_data.original_cat_id = original_article_category.id WHERE 1';
    if (is_array($article_ids) && $article_ids) {
      $aids = implode(',', $article_ids);
      $sql.= " AND article.id in ($aids)";
    }
    $conn = mysql_connect(self::$DB_HOST, self::$DB_USER, self::$DB_PASS, true) or die("\n\nCannot connect to database\n\n");
    mysql_select_db(self::$DB_NAME) or die("\n\nDatabase doesn't exists\n\n");
    $articlesResult = mysql_query($sql, $conn);
    $titles[$id]['articles'] = new syndGenerateMySqlIterator($articlesResult, new ArticleArrayFormater($this->model));
    if (!mysql_num_rows($articlesResult)) {
      unset($titles[$id]);
    }
    return $titles;
  }

  private function prepareDir($clientHomeDir) {

    $ftpDir = rtrim($clientHomeDir, '/') . '/' . date('Y/m/d/');

    if (!is_dir($ftpDir)) {
      mkdir($ftpDir, 0777, true);
      //mkdir($ftpDir, 0755, true);
    }
    return $ftpDir;
  }

  private function clearTmp() {
    if (is_array($this->tmpFiles)) {
      foreach ($this->tmpFiles as $file) {
        unlink($file);
      }
    }
  }

  public function updateArticles($ids) {
    $conn = mysql_connect(self::$DB_HOST, self::$DB_USER, self::$DB_PASS, true) or die("\n\nCannot connect to database\n\n");
    mysql_select_db(self::$DB_NAME) or die("\n\nDatabase doesn't exists\n\n");
    $date = date('Y-m-d H:i:s', time());
    $aids = implode(',', $ids);
    $sql = "update article_jumperGDP set is_sent=1,send_date ='$date' where article_id in ($aids)";
    mysql_query($sql, $conn);
  }

  public function uploadFileViaFTP($client_id, $ftpDir, $localFile) {
    $c = new Criteria();
    $c->add(ClientConnectionPeer::CLIENT_ID, $client_id);
    $ftp = ClientConnectionPeer::doSelectOne($c);
// set up basic connection
    $conn_id = ftp_connect($ftp->getUrl());

// login with username and password
    $login_result = ftp_login($conn_id, $ftp->getUsername(), $ftp->getPassword());
// upload a file
    if (ftp_put($conn_id, $localFile, $ftpDir . $localFile, FTP_ASCII)) {
      $success = true;
      // echo "successfully uploaded $file\n";
    } else {
      $success = false;
      // echo "There was a problem while uploading $file\n";
    }

// close the connection
    ftp_close($conn_id);
    return $success;
  }

}

class ArticleArrayFormater implements syndGenerateArrayFormater {

  private $model;

  public function __construct($model) {
    $this->model = $model;
  }

  private function getLanguage($langId) {
    static $languages = array();
    if (isset($languages[$langId])) {
      return $languages[$langId];
    }
    $lang = $this->model->getLanguage($langId);
    if ($lang) {
      $languages[$lang->getId()] = $lang->getName();
    }
    return ($lang ? $lang->getName() : '');
  }

  public function format($articleRow) {
    $artId = $articleRow['id'];

    $returnVal['id'] = $artId;
    $returnVal['headline'] = $articleRow['headline'];
    $returnVal['byLine'] = $articleRow['author'];
    $returnVal['language'] = $this->getLanguage($articleRow['language_id']);
    $returnVal['category'] = $articleRow['iptc_id'];
    $returnVal['abstract'] = $articleRow['summary'];
    $returnVal['text'] = & $articleRow['body'];
    $returnVal['date'] = trim($articleRow['date']) ? $articleRow['date'] : $articleRow['parsed_at'];
    $returnVal['parsed_at'] = $articleRow['parsed_at'];
    $returnVal['originalCategory'] = trim($articleRow['cat_name']);

    $headlineImage = $this->model->getArticleHeadlineImage($artId);
    $returnVal['headlineImageCaption'] = $headlineImage ? $headlineImage->getImageCaption() : '';
    $returnVal['headlineImage'] = $headlineImage ? $headlineImage->getImgName() : '';
    $returnVal['videos'] = $this->model->getArticleVideos($artId);
    $returnVal['images'] = $this->model->getArticleImages($artId);
    return $returnVal;
  }

}

$obj = new syndGenerateFactiva();
$obj->generate();

// End class 
?>