#!/usr/bin/env php
<?php

require_once dirname(__FILE__) . '/../../ez_autoload.php';
require_once dirname(__FILE__) . '/../../vcrond/common.php';


 
//Get the Title ID to the variable from first argument
$titleId		= $_SERVER['argv'][1];

//Get the try number from the second agrument
$try	    	= isset($_SERVER['argv'][2]) ? $_SERVER['argv'][2] : 1;

$model 			=  new syndModel();
$titleName 		= $model->getTitleName($titleId);
$isNewswire		= $model->isTitleNewswire($titleId);
$alert = syndAlert::getInstance();


/**
 * Check to see if the title is active
 * 
 * @todo move checking activity to vcrond
 */
if (!$model->isTitleActive($titleId)) {
	echo 'Title is not active. Exiting...';
	exit;
}



//added the status to the gather_report table
$report  = new syndReport('gather');
$reportParams['TitleId'] 	  = $titleId;
$reportParams['NumberFiles']  = 0;
$reportParams['LatestStatus'] = 0;
$reportParams['FileSize']     = 0;
$reportParams['Error']	 	  = 'Start Gathering the Title Content for '.$titleName;
$reportId				 	  = $reportParams['Id'] = $report->addReport($reportParams);
	
//get new instance of the gathering class for this Title
$connection = syndGatherContent::factory($titleId, $reportId);


//check if the title exists
if (is_object($connection)) {
	
	//check if there is new content or not
	if ( $connection->checkContent('','',true) === false ) {		
		$reportParams['TitleId']	= $titleId;
		$reportParams['Error']	    = 'No Content found, Sending alert.';
		$report->updateReport($reportParams);
		
		if (!$isNewswire) {
		//get the number of retries
		$gather_retries = $model->getGatherRetries($titleId);
		$nextTryTimer   = $model->getNextTryTimer($titleId);
		//Wearning level
		$wlevel 		= ($try >= $gather_retries) ? syndAlert::CRITICAL : syndAlert::WARNING ;

		//send alert to inform if that there is not content
		$alert->send('No new Content Found.', $wlevel, 'SERVER', $titleId);	 	
	 	//Insert a new crontab
		if ($try <= $gather_retries) {		 						
			/*	
			$tab 	= new Crontab();
			$tab->setTitle($titleId);
			$tab->setConcurrent(false);
			$tab->setDescription($titleName);
			$tab->setLastActualTimestamp(time());

			
			$cronDefinition = date('i H d m w', time() + $nextTryTimer);

			$tab->setCronDefinition($cronDefinition);
			$tab->setScript('/usr/local/syndigate/classes/gather/syndGather.php ' . $titleId . ' ' .($try+1));
			$tab->setRunOnce(true);
			$tab->setImplementationId($titleId);
			$tab->update();
			*/
			
        }
		}
	} else {
		//start copying the new content
		$alert->send('OK. New Content Found.', syndAlert::OK, 'SERVER', $titleId);
		$connection->copyContent();

	}
}

