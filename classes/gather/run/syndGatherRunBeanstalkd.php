#!/usr/bin/env php
<?php

require_once dirname(__FILE__) . '/../../../ez_autoload.php';
require_once APP_PATH . 'misc/pheanstalk_workers/worker.php';
require_once APP_PATH . 'scripts/propel_include_config.php';

$pheanstalk = new Pheanstalk(PHEANSTALK_HOST . ':' . PHEANSTALK_PORT );

$memcache = new Memcache;
$memcache->connect(MEMCACHE_HOST, MEMCACHE_PORT) or die ("Could not connect");


class SyndGatherRunBeanstalkd extends Worker {
	
	public function __construct($conf) {	
		parent::__construct($conf);
	}
	
	
	public function processJob($job) {	
		
		global $memcache;
		
		$this->log('Processing job');
		$this->job 	= (array)json_decode($job);	
		$startingAt = isset($this->job['starting_at']) ? $this->job['starting_at'] : time()-1 ;
		
		echo 'trying title : ' . $this->job['title_id'] . PHP_EOL; 		
		echo 'must start at ' . date('Y-m-d H:i:s', $startingAt) . ' and now is ' . date('Y-m-d H:i:s') . PHP_EOL;
		
		if($startingAt > time()) {
			echo "NOT time to run " . PHP_EOL;
			return false;
		}
		
		$key = 'gather_beanstalkd_' . $this->job['title_id'];
		if($memcache->get($key)) {
			$memcache->delete($key, 0);
		}
		
		//echo $this->job['title_id'] . PHP_EOL;
		$cmd = APP_PATH . 'classes/gather/syndGather.php ' . (int) $this->job['title_id'];
		echo $cmd . PHP_EOL;		
		$rez = shell_exec($cmd);
		echo $rez . PHP_EOL;
		echo "done . " . PHP_EOL;

		
		sleep(10);
		
		return true;
	}
}




$conf = array(		
	'tube'=> GATHER_TUBE,
	'sleep_interval'=>GATHER_SLEEP_INTERVAL,	
	'max_concurrent_jobs'=> GATHER_MAX_CONCURRENT_JOBS
);




$children = array();

while (true) {
	sleep(5); //Sleep in every loop
	
	if( count($children) < GATHER_MAX_CONCURRENT_JOBS) {
		
		$pid = pcntl_fork();
		if($pid == -1) {
			die("Could not fork!");
         } elseif($pid == 0) {
         	$worker = "worker". rand();
         	echo "---- startung $worker " . PHP_EOL;
         	$$worker = new SyndGatherRunBeanstalkd($conf);
         	$$worker->run();
         	exit();
         } else {
         	$children[] = $pid;
         }		
	}
	
	
	 while(pcntl_wait($status, WNOHANG OR WUNTRACED) > 0) {
	 	usleep(1000);
	 }

     while(list($key, $val) = each($children)) {
     	if(!posix_kill($val, 0)) { // This detects if the child is still running or not
     		unset($children[$key]);
     	}
     }
     
     $children = array_values($children); // Reindex the array
}
