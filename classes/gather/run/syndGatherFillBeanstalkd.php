#!/usr/bin/env php
<?php

require_once dirname(__FILE__) . '/../../../ez_autoload.php';
require_once APP_PATH . 'scripts/propel_include_config.php';
require_once APP_PATH . 'conf/pheanstalk.conf.php';


class syndGatherFillBeanstalkd {
	
	protected $pheanstalk;
	protected $frequencies;
	protected $memcache;
	
	public function __construct() {
		
		$this->pheanstalk 	= new Pheanstalk( PHEANSTALK_HOST . ':' . PHEANSTALK_PORT);
		$this->frequencies 	= array_flip(GatherCrontab::getDefinitions());
		$this->memcache 	= new Memcache;		
		$this->memcache->connect(MEMCACHE_HOST, MEMCACHE_PORT) or die ("Could not connect to memcache");		
	}
	
	public function fillJobs() {
		$titles = $this->getTitles();
		$titlesHavingIncompleteParsingState = $this->getTitlesIdsHavingTodayIncompleteParsingState();
		$cmsIds = $this->getCmsTitlesIds();
		echo "titles having incomplete parseing state records today are \n";
		print_r($titlesHavingIncompleteParsingState);
				
		foreach ($titles as $title) {
			
			// Escaping tiles in cms because the trigger will be raised from there
			// but Leave some checks in some times
			if( ( in_array($title->id, $cmsIds) ) && (date('H') > 10)) {
				continue;
			}
			
			if(in_array($title->id, $titlesHavingIncompleteParsingState)) {
					echo $title->id . " Having incomplete parsing state records  .. escaping this one \n";
					continue;
			}
			
			$time = time();
	
			if($title->cron_definition == '9 * * * *') {				
				
				// Daily newswwire
				$proiority = 200;
				$numOfRuns = 2; //  number of runs per hour
				$spanIncrement = 3600 / $numOfRuns;
				//for($i=0; $i< $numOfRuns; $i++) {
					$i=0;
					$this->PutJob($title->id, $time + ($i * $spanIncrement), 200, true);
				//}
				
			} else	 {
				if($title->cron_definition == $this->frequencies['Daily']) {
					$this->PutJob($title->id, $time + 60, 100, false);
				} else {
					//Weekly, monthly, etc 
					$this->PutJob($title->id, $time + 60, 400, false);
				}
			}
		}			
	}
	
	function getTitles() {
		$query = "SELECT title.id, title.title_frequency_id, gather_crontab.cron_definition, title_connection.connection_type 
			FROM title INNER JOIN gather_crontab ON title.id = gather_crontab.implementation_id 
					INNER JOIN title_connection ON title.id = title_connection.title_id  
			 WHERE title.is_active = 1 
			 AND gather_crontab.run_once = 0 ";
			// AND title_connection.connection_type != 'local' ";
		
		$rez = mysql_query($query);
		while ($row = mysql_fetch_object($rez)) {
			if(is_object($row)) {
				$titles[] = $row;
			} else {
				echo "not object " . PHP_EOL;
			}
		}		
		return $titles;		
	}
	
	function getCmsTitlesIds() {
		try {
			include_once '/var/www/cms/config/config.class.php';
			$cmsConn = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
			$cmsDb   = mysql_select_db(DB_NAME);
			$query   = "SELECT DISTINCT title_id FROM publications_titles ";
			$rez	 = mysql_query($query);
			$ids     = array();
			while ($row = mysql_fetch_assoc($rez)) {
				$ids[] = $row['title_id'];
			}
			return $ids;
		} catch (Exception $e) {
			return array();
		}
	}
	
	/**
	 * Enter description here...
	 *
	 * @param Integer $titleId
	 * @param Integer $startingAt : the timestamp when the job can be run
	 * @param int $proiority
	 * @param boolean $concurrent : can the job be concurrent
	 */
	function PutJob($titleId, $startingAt,$proiority =100, $concurrent = false) {
		
		$beanstalkdKey  = 'gather_beanstalkd_' . $titleId;		
		$jobData    	= json_encode(array(
                            'title_id'     => $titleId, 
                            'starting_at'  => $startingAt,
                            'timestamp'    => time(),
                           )
                        );                        
                    $key = 'toto'    ;

		if(!$concurrent) {
			if( !$this->memcache->get($key) && !$this->memcache->get($beanstalkdKey) ) {				
				$beanstalkedJobId = $this->pheanstalk->useTube(GATHER_TUBE)->put($jobData, $proiority);                        		
				$this->memcache->set($beanstalkdKey, $beanstalkedJobId, false, 7200) or die ("Failed to save data at the server");
				echo "New Job for title $titleId ( not concurrent ) with Id $beanstalkedJobId will start at $startingAt" . PHP_EOL; 
			} else {
				echo "previous job for title $titleId " . PHP_EOL; 
			}
		} else {
			$beanstalkedJobId = $this->pheanstalk->useTube(GATHER_TUBE)->put($jobData, $proiority);
			echo "New Job for title $titleId ( concurrent ) with Id $beanstalkedJobId will start at $startingAt" . PHP_EOL; 
		}
		
	}
	
	
	function getTitlesIdsHavingTodayIncompleteParsingState() {
		$day       = date('Y-m-d 00:00:00');
		$query     = "SELECT DISTINCT title_id FROM parsing_state WHERE completed = '0' AND created_at > '$day'";
		$rez       = mysql_query($query);
		$titlesIds = array();
		 
		while ($row = mysql_fetch_object($rez)) {
			$titlesIds[] = $row->title_id;
		}
		return $titlesIds;		
	}
}


$gatherBeanstalkd = new syndGatherFillBeanstalkd();
$gatherBeanstalkd->fillJobs();
