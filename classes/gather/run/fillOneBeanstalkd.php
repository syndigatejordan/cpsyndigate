#!/usr/bin/env php
<?php
require_once dirname(__FILE__) . '/../../../ez_autoload.php';
require_once APP_PATH . 'scripts/propel_include_config.php';
require_once APP_PATH . 'conf/pheanstalk.conf.php';


class syndGatherFillBeanstalkd {
	
	protected $pheanstalk;
	protected $frequencies;
	
	public function __construct() {
		$this->pheanstalk 	= new Pheanstalk( PHEANSTALK_HOST . ':' . PHEANSTALK_PORT);
		$this->frequencies 	= array_flip(GatherCrontab::getDefinitions());
	}
	
	public function fillJobs($titleId) {
		$title  = TitlePeer::retrieveByPk($titleId);
		$titleId = $title->getId();
		$time = time();
			

			$beanstalkdKey  = 'gather_beanstalkd_' . $titleId;		
			$jobData    	= json_encode(array(
                            'title_id'     => $titleId, 
                            'starting_at'  => $time,
                            'timestamp'    => $time,
                           )
                        );                        
			$beanstalkedJobId = $this->pheanstalk->useTube(GATHER_TUBE)->put($jobData, 100);
                        echo "New Job for title $titleId  with beanstalkd job Id $beanstalkedJobId will start at $time" . PHP_EOL;

				
	}
	
	
}

if(!$argv[1]) {
	die( 'Enter title id '. PHP_EOL);
}



$gatherBeanstalkd = new syndGatherFillBeanstalkd();
$gatherBeanstalkd->fillJobs( $argv[1]);
