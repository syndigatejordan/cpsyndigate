#!/usr/bin/env php
<?php

require_once dirname(__FILE__) . '/../../../ez_autoload.php';
require_once APP_PATH . 'scripts/propel_include_config.php';
//require_once APP_PATH . 'lib/pheanstalk/pheanstalk_init.php';
require_once APP_PATH . 'conf/pheanstalk.conf.php';

$pheanstalk = new Pheanstalk(PHEANSTALK_HOST . ':' . PHEANSTALK_PORT );

$memcache   = new Memcache;
$memcache->connect(MEMCACHE_HOST, MEMCACHE_PORT) or die ("Could not connect");


$fd 		= inotify_init();
$watchList 	= array();
$titles 	= getTitles();


foreach ($titles as $title) {	

	echo PHP_EOL . str_repeat('-', 150) . PHP_EOL;	
	
	//Always watch the root directory		
	$homeDir = $title->home_dir;
	
	watchDir($homeDir, $fd, $title->id, $watchList);
	
	if('issues' == $title->directory_layout) {		
		$dirs = scandir($homeDir);		
		natsort($dirs);
		$dirs = array_reverse($dirs);
		$dirs = array_slice($dirs, 0, 3);
		print_r($dirs);
		foreach ($dirs as $index => $path) {
			if($path != '.' && $path != '..') {				
				watchDir($homeDir . $path, $fd, $title->id, $watchList);							
			}
		}

		
	} elseif ('date' == $title->directory_layout ) {		
		$dirs 	= array();
		$dirs[] = $homeDir . date('Y/');
		$dirs[] = $homeDir . date('Y/m/');
		$dirs[] = $homeDir . date('Y/m/', strtotime("-1 month"));
		$dirs[] = $homeDir . date('Y/m/', strtotime("+1 month"));
		$dirs[] = $homeDir . date('Y/m/d/', strtotime("-1 days"));
		$dirs[] = $homeDir . date('Y/m/d/', strtotime("-2 days"));
		$dirs[] = $homeDir . date('Y/m/d/', strtotime("-3 days"));		
		$dirs[] = $homeDir . date('Y/m/d/');
		
		$dir = $homeDir . date('Y/m/');
		if( is_dir( $homeDir . date('Y/m/') ) ) {
			$days = scandir($dir);
			natsort($days);
			$days = array_reverse($days);
			$days = array_slice($days, 0, 3);
			foreach ($days as $index =>$day) {
				if('.' != $day && '..' != $day) {
					$dirs[] = $dir . $day . '/';
				}
			}
		}		
		
		foreach ($dirs as $dir) {
			watchDir($dir, $fd, $title->id, $watchList);			
		}
	}
}



while (true) {
	sleep(1);
	$events = inotify_read($fd);	
	foreach ($events as $event) {		
		addGatherJob($watchList[$event['wd']]);
	}
}


echo PHP_EOL . "finished" . PHP_EOL;




//--- functions -------//

function watchDir($dir, $fd, $titleId, &$watchList) {
	if(is_dir($dir)) {
		echo "adding $dir to watch list" . PHP_EOL;
		$wd = inotify_add_watch($fd, $dir, IN_MODIFY | IN_MOVED_TO | IN_CLOSE_WRITE | IN_CREATE | IN_MOVE_SELF );
		
		if($wd) {			
			$watchList[$wd] = $titleId; 
			return true;
		}
	} elseif (is_file($dir)) {
		echo "----- $dir is a file " . PHP_EOL;
	}
	
	return false;
}



function getTitles() {
	$query = "SELECT title.*, title_connection.connection_type 
			  FROM title 
			  INNER JOIN title_connection ON title.id = title_connection.title_id  
			  WHERE title.is_active = 1 			 
			  AND title_connection.connection_type = 'local' ";

	$rez = mysql_query($query);
	while ($obj = mysql_fetch_object($rez)) {
		$titles[] = $obj;
	}	
	return $titles;
}



function addGatherJob($titleId) {
	
	global $memcache, $pheanstalk;
		
	// Key : to store title key in memcache for some time to make sure that all files has been put in the directory before rais the inotify event to beanstalkd	
	$key		 	= 'gather_fill_' . $titleId;
	
	//beanstalkdKey : job in beanstaked to start gathering the content
	$beanstalkdKey  = 'gather_beanstalkd_' . $titleId;

	if( !$memcache->get($key) && !$memcache->get($beanstalkdKey) ) {
		$jobData    = json_encode(array(
                            'title_id'     => $titleId,
                            'starting_at'  => time() + 180,
                            'timestamp'    => time(),
                           )
                        );
        
        $beanstalkedJobId = $pheanstalk->useTube(GATHER_TUBE)->put($jobData, 100);                
        $memcache->set($beanstalkdKey, $beanstalkedJobId, false, 3600) or die ("Failed to save data at the server");
        echo "adding job to title " . $titleId . " with id $beanstalkedJobId " .  PHP_EOL;
	}
}
