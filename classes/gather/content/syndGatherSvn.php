<?php
class syndGatherSvn
{
    /**
     * creat an Object from the Model class
     *
     * @var object
     */
    private $model;
    /**
     * Get the one and only instance of the ezLog class
     *
     * @var object
     */
    protected $log;
    /**
     * Create an Object of the SyndReport Class
     *
     * @var obejct
     */
    protected $report;
    /**
     * The report ID for the gather_report table
     *
     * @var int
     */
    protected $reportId;
    /**
     * Array of the of the added files
     *
     * @var array
     */
    protected $addedContent = array();
    /**
     * Array of the of the deleted files
     *
     * @var array
     */
    protected $deletedContent = array();
    /**
     * The Title ID
     *
     * @var int
     */
    protected $titleId;
    /**
     * Title Name
     *
     * @var string
     */
    protected $titleName;
    /**
     * Title's Working directory
     *
     * @var string
     */
    protected $titleWorkDir;
    /**
     * The Connection type
     *
     * @var string
     */
    protected $titleType;
    /**
     * Store the report parameter
     *
     * @var array
     */
    protected $reportParams;
    
    /**
     * Store the log parameter
     *
     * @var array
     */
    protected $logParams;
    
    /**
     * The Class constructor
     * 
     * @param string $titleId
     */
    public function __construct ($titleId, $reportId)
    {
        $this->model = new syndModel();
        $this->log = ezcLog::getInstance();
        $this->log->source = 'gather';
        $this->log->category = 'SVN';
        $this->report = new syndReport('gather');
        $this->reportId = $this->reportParams['Id'] = $reportId;
        $this->titleId = $titleId;
        $this->titleName = $this->model->getTitleName($titleId);
        $this->titleType = $this->model->getConnectionType($titleId);
        $this->reportParams['Error'] = 'Star Commiting the changes to the SVN.';
        
        $this->logParams 		= array('report_gather_id' => $reportId, 'source' => 'gather', 'category'=> 'SVN');
    }
    
    /**
     * Add the new files to the SVN
     *
     * @param array $addedContent
     */
    public function svnAdd ($addedContent)
    {
        foreach ($addedContent as $value) {
            unset($output);
            exec(escapeshellcmd('svn st ' . escapeshellarg($value)), $output);
            if(!empty($output)) {
            	foreach ($output as $outputLine) {
            		if('?' == $outputLine{0}) {      
            			$adding = explode('?', $outputLine,2);
            			$adding = trim($adding[1]);
            			      			
            			$this->log->log('Adding ' . $adding . ' to the SVN for ' . $this->titleName, ezcLog::DEBUG, $this->logParams);
            			exec(escapeshellcmd('svn add ' . escapeshellarg($adding)), $addingOutput);
            			if(!empty($addingOutput)) {
            				foreach ($addingOutput as $outputLine) {
            					$this->log->log($outputLine . ' for ' . $this->titleName, ezcLog::DEBUG, $this->logParams);
            				}
            			}
            			
            		}
            	}
            	
            }
            
        }
    }
    
    /*
    
    old one 
    -------------
    public function svnAdd ($addedContent)
    {
        foreach ($addedContent as $value) {
            unset($output);
            exec(escapeshellcmd('svn st ' . escapeshellarg($value)), $output);
            if (isset($output[0]) && $output[0]{0} == '?') {
                $this->log->log('Adding ' . $value . ' to the SVN for ' . $this->titleName, ezcLog::DEBUG, $this->logParams);
                exec(escapeshellcmd('svn add ' . escapeshellarg($value)), $output);
                if (! empty($output)) {
                    foreach ($output as $row) {
                        $this->log->log($row . ' for ' . $this->titleName, ezcLog::DEBUG, $this->logParams);
                    }
                }
            }
        }
    }
    */
    
    /**
     * Delete the deleted files from SVN
     *
     *  @param array $deletedContent
     */
    public function svnDel ($deletedContent)
    {
        foreach ($deletedContent as $value) {
            unset($output);
            exec(escapeshellcmd('svn st ' . $value), $output);
            if ($output[0]{0} != 'D') {
                $this->log->log('Deleting ' . $value . ' from the SVN for ' . $this->titleName, ezcLog::DEBUG, $this->logParams);                
                exec(escapeshellcmd('svn del ' . escapeshellarg($value)), $output);
                if (! empty($output)) {
                    foreach ($output as $row) {
                        $this->log->log($row . ' for ' . $this->titleName, ezcLog::DEBUG, $this->logParams);
                    }
                }
            }
        }
    }
    /**
     * Commit the new changes to the SVN
     *
     */
    public function svnCommit ($addedContent, $deletedContent, $workDirPath)
    {    	
    	$this->titleWorkDir 	= $workDirPath;
        $this->addedContent 	= $addedContent;
        $this->deletedContent 	= $deletedContent;
        
        if (chdir($this->titleWorkDir)) {
        	$this->log->log('Changing to the Title Work Directory: ' . $this->titleWorkDir, ezcLog::DEBUG, $this->logParams);
            if (is_array($this->addedContent) && ! empty($this->addedContent)) {
            	$this->svnAdd($addedContent);
            }
            if (is_array($this->deletedContent) && ! empty($this->deletedContent)) {
                $this->svnDel($deletedContent);
            }
            $this->log->log('Commiting the changes to the SVN for: ' . $this->titleName, ezcLog::DEBUG, $this->logParams);
            exec('svn ci -m "Title Name:' . $this->titleName . date('Y-m-d h:i:s', time()) . '"', $output);
            $returnValue = true;
            if(!empty($output)) {
            	
	            foreach ($output as $row) {
	                $this->log->log($row, ezcLog::DEBUG, $this->logParams);
	                                
	                if( preg_match('/Committed/i', $row) ) {                	
	                	$revs = explode(' ', $row);                	
	                	$returnValue = (int)$revs[count($revs)-1];                	                	
	                }                
	            }            
	            
	            if(0 == (int)$returnValue) {
	            	$this->log->log("Error commiting, returned revision number 0", ezcLog::ERROR , $this->logParams);
	            } else {
	            	$this->log->log("comitted with revision " . $returnValue, ezcLog::DEBUG, $this->logParams);
	            }
	            return (int)$returnValue;
            	
            } else {
            	$this->log->log("Nothing to commit....", ezcLog::ERROR , $this->logParams);
            	return 0;
            }
            
        } else {
            $this->reportParams['LatestStatus'] = 0;
            $this->reportParams['Error'] = 'Could not Change to the Title Work Directory: ' . $this->titleWorkDir;
            $this->report->updateReport($this->reportParams);
            $this->log->log('Could not Change to the Title Work Directory: ' . $this->titleWorkDir, ezcLog::FATAL, $this->logParams);
            throw new Exception('Could not Change to the Title Work Directory: ' . $this->titleWorkDir);
        }
    }
}
