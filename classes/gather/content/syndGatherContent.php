<?php
//require_once dirname(__FILE__) . '/../../../ez_autoload.php';
/**
 * An abstraction class for the Gather Content
 *
 */
abstract class syndGatherContent
{
    /**
     * A factory to include and create a the class depened on the connection type
     *
     * @param 	string  	$type
     * @param 	integer 	$titleId
     * @return 	mixed
     */
    public static function factory ($titleId, $reportId)
    {
        $model = new syndModel();
        $log = ezcLog::getInstance();
        $log->source = 'gather';
        $report = new syndReport('gather');

        try {
        	
            if (! $type = ucfirst($model->getConnectionType($titleId))) {            	
                throw new Exception('Title does not exists.');
            }
            if (file_exists(APP_PATH . 'classes/gather/content/custom/syndGatherContent' . $type . 'C' . $titleId . '.php')) {
            	// include a custom content gatherer
                include_once APP_PATH . '/classes/gather/content/custom/syndGatherContent' . $type . 'C' . $titleId . '.php';
                $classname = "syndGatherContent" . $type . 'C' . $titleId;
            } else {
            	require_once APP_PATH . '/classes/gather/content/syndGatherContent' . $type . '.php';
                $classname = 'syndGatherContent' . $type;
            }
            if (! class_exists($classname)) {
                throw new Exception('Class name: [' . $classname . '] does not exist ');
            }
            $obj = new $classname($titleId, $reportId);
            return $obj;
        } catch (Exception $e) {
            $reportParams['Id'] = $reportId;
            $reportParams['LatestStatus'] = 0;
            $reportParams['TitleId'] = $titleId;
            $reportParams['Error'] = $e->getMessage();
            $report->updateReport($reportParams);
            $log->log($e->getMessage(), ezcLog::FATAL, array('report_id' => $reportId));
        }
    }
    /**
     * A method to make the connection needed
     *
     */
    //	abstract public function checkContent();
    /**
     * A method to copy the content from the client to our server
     *
     */
    abstract public function copyContent ();


    /**
     * Format a file name to be safe
     *
     * @param    string $file   The string file name
     * @param    int    $maxlen Maximun permited string lenght
     * @return   string Formatted file name
     * @see HTTP_Upload_File::setName()
     */
    public function nameToSafe($name, $maxlen=70)
    {
        $noalpha = 'ÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÀÈÌÒÙàèìòùÄËÏÖÜäëïöüÿÃãÕõÅåÑñÇç@°ºª';
        $alpha   = 'AEIOUYaeiouyAEIOUaeiouAEIOUaeiouAEIOUaeiouyAaOoAaNnCcaooa';


        $name = substr($name, 0, $maxlen);
        $name = strtr($name, $noalpha, $alpha);

        // not permitted chars are replaced with "_"
        return preg_replace('/[^a-zA-Z0-9\._]/', '_', $name);
    }




}
