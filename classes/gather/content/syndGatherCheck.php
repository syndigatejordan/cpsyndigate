<?php
class syndGatherCheck
{
    public static function getBiggestYear ($homePath)
    {
        $alert = syndAlert::getInstance();
        foreach (scandir($homePath, 1) as $year) {
            if ($year != '.' && $year != '..' && $year != '.svn') {
                $years[] = $year;
            }
        }
        $year = array($homePath . $years[0] , $years[0]);
        return $year;
    }
    public static function getBiggestMonth ($homePath, $titleId)
    {
        $alert = syndAlert::getInstance();
        $year = self::getBiggestYear($homePath, $titleId);
        $path = $year[0];
	
        if (is_dir($path)) {
            foreach (scandir($path, 1) as $month) {
                if ($month != '.' && $month != '..' && $month != '.svn') {
                    $months[] = $month;
                }
            }
//            $alert->send("Title is naming the folders in the right way. $titleId", syndAlert::OK, 'SERVER', $titleId);
        } else {
            $alert->send("Title is not naming the folders in the right way, check his home directory. $titleId", syndAlert::CRITICAL, 'SERVER', $titleId);
            throw new Exception('Title is not naming the folders in the right way, check his home directory (Month).' . $titleId.' HomePAth:'.$homePath);
        }

		natsort($months);
		$months = array_reverse($months);

        $month = array($path . '/' . $months[0] , $months[0]);
        return $month;
    }
    public static function getNewContent ($homePath, $titleId)
    {
        $alert = syndAlert::getInstance();
        $model = new syndModel();
        $firstTime = $model->isTitleFirstGather($titleId);
        $titleType = $model->getConnectionType($titleId);
        $isIssue = $model->dirLayout($titleId);
        $folders = array();


        if ($firstTime || $titleType == 'http' || /*$titleType == 'ftp' ||*/ $isIssue == 'root') {
            if (is_dir($homePath)) {
                foreach (scandir($homePath, 1) as $folder) {
                    if ($folder != '.' && $folder != '..' && $folder != '.svn') {
                        $folders[] = $folder;
                    }
                }
//                $alert->send("Title is naming the folders in the right way. $titleId", syndAlert::OK, 'SERVER', $titleId);
            } else {
                $alert->send("Title is not naming the folders in the right way, check his home directory. $titleId", syndAlert::CRITICAL, 'SERVER', $titleId);
                throw new Exception('Title is not naming the folders in the right way, check his home directory.' . $titleId);
            }
            //$folders = is_array($folders) ? $folders : array();							
            return $folders;
        } elseif ('issues' == $isIssue && ! $firstTime) {
            $folders = array();
            if (is_dir($homePath)) {
                foreach (scandir($homePath, 1) as $folder) {
                    if ($folder != '.' && $folder != '..' && $folder != '.svn') {
                        $folders[] = $folder;
                    }
                }
//                $alert->send("Title is naming the folders in the right way. $titleId", syndAlert::OK, 'SERVER', $titleId);
            } else {
                $alert->send("Title is not naming the folders in the right way, check his home directory. $titleId", syndAlert::CRITICAL, 'SERVER', $titleId);
                throw new Exception('Title is not naming the folders in the right way, check his home directory. ' . $titleId);
            }
            natsort($folders);
            $folders = array_slice(array_reverse($folders), 0, 3);
            return $folders;
        } else { 
            $monthInfo = self::getBiggestMonth($homePath, $titleId); 
            $monthPath = $monthInfo[0];
            $year = self::getBiggestYear($homePath, $titleId);
            $yearPath = $year[0];
            if (is_dir($monthPath)) {
                foreach (scandir($monthPath, 1) as $day) {
                    if ($day != '.' && $day != '..' && $day != '.svn') {
                        $days[] = $monthPath . '/' . $day;
                    }
                }
//                $alert->send("Title is naming the folders in the right way. $titleId", syndAlert::OK, 'SERVER', $titleId);
            } else {
                $alert->send("Title is not naming the folders in the right way, check his home directory. $titleId", syndAlert::CRITICAL, 'SERVER', $titleId);
                throw new Exception('Title is not naming the folders in the right way, check his home directory.' . $titleId);
            }
            natsort($days);
            $folders = array_slice(array_reverse($days), 0, 3);
            $daysCount = count($folders);
            if ($daysCount < 3) {
                $month = $monthInfo[1];
                $year = $year[1];
                for ($daysCount; $daysCount < 3; $daysCount ++) {
                    if ($month - 1 == 0) {
                        $month = 12;
                        $yearPath = $homePath . ($year - 1);
                    } else {
                        $month = $month - 1;
                    }
                    while (! is_dir($yearPath . '/' . $month) && $month >= 1) {
                        $month --;
                    }
                    $i = 31;
                    for ($i; $i >= 1; -- $i) {
                        if (! is_dir($yearPath . '/' . $month . '/' . $i)) {
                            continue;
                        } else {
                            foreach (scandir($yearPath . '/' . $month . '/' . $i) as $value) {
                                unset($scan);
                                if ($value != '.' && $value != '..' && $value != '.svn') {
                                    $scan[] = $value;
                                }
                            }
                            if (! empty($scan)) {
                                $folders[] = $yearPath . '/' . $month . '/' . $i;
                            }
                        }
                        //echo $folders[] = $yearPath . '/' . $month . '/' . $i;
                    }
                }
                foreach ($folders as $folder) {
                    $dir = substr($folder, strlen($homePath));
                    $dirs[] = $dir;
                }
                
                return (array) $folders = array_slice($dirs, 0, 3);
            } else {
                foreach ($folders as $folder) {
                    $dir = substr($folder, strlen($homePath));
                    $dirs[] = $dir;
                }
                //print_r($dirs);
                return (array) $folders = array_slice($dirs, 0, 3);
            }
        }
    }
}
?>
