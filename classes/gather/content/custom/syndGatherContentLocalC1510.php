<?php

/* Global Data Point */

class syndGatherContentLocalC1510 extends syndGatherContentLocal {

  public function __construct($titleId, $reportId = '') {

    $c = new Criteria();
    $c->add(TitlePeer::ID, $titleId);
    $currentTitle = TitlePeer::doSelectOne($c);
    $currentDirName = $currentTitle->getHomeDir();

    $directories = scandir($currentDirName);
    foreach ($directories as $TGZFile) {
      if (($TGZFile != '.') && ($TGZFile != '..') && (is_file("{$currentDirName}{$TGZFile}"))) {
        $command = 'unzip ' . $currentDirName.$TGZFile . " -d " . $currentDirName;
        $remove_command = 'rm ' . $currentDirName . $TGZFile;
        echo $command . chr(10);
        echo $remove_command . chr(10);
        if (shell_exec($command)) {
          shell_exec($remove_command);
        }
      }
    }
    $this->MoveFromRootToDateDir($titleId);
    parent::__construct($titleId, $reportId);
  }

}

?>
