<?php

class syndGatherContentHttpC3378 extends syndGatherContentHttp {

  protected function getRssUrl() {
    $urls = array(
        'Mantiqti1' => 'https://mantiqti.cairolive.com/category/%D8%A7%D9%84%D8%A3%D8%AE%D8%A8%D8%A7%D8%B1/feed/',
        'Mantiqti2' => 'https://mantiqti.cairolive.com/category/%D8%A7%D9%84%D8%AA%D9%82%D8%A7%D8%B1%D9%8A%D8%B1/feed/',
        'Mantiqti3' => 'https://mantiqti.cairolive.com/category/%D8%AD%D9%88%D8%A7%D8%B1%D8%A7%D8%AA-%D8%B5%D8%AD%D9%81%D9%8A%D8%A9/feed/',
        'Mantiqti4' => 'https://mantiqti.cairolive.com/category/%D9%83%D9%84-%D8%B4%D9%8A%D8%A1-%D8%B9%D9%86/feed/',
        'Mantiqti5' => 'https://mantiqti.cairolive.com/category/%D9%85%D9%84%D9%81%D8%A7%D8%AA/feed/',
        'Mantiqti6' => 'https://mantiqti.cairolive.com/category/%D9%85%D9%82%D8%A7%D9%84%D8%A7%D8%AA/feed/',
        'Mantiqti7' => 'https://mantiqti.cairolive.com/category/%D8%A7%D9%84%D8%AC%D8%A7%D9%84%D9%8A%D8%B1%D9%8A/feed/',
        'Mantiqti8' => 'https://mantiqti.cairolive.com/category/%D9%81%D9%86%D9%88%D9%86/feed/',
        'Mantiqti9' => 'https://mantiqti.cairolive.com/category/%D8%B4%D9%88%D8%A7%D8%B1%D8%B9%D9%86%D8%A7/feed/',
        'Mantiqti10' => 'https://mantiqti.cairolive.com/category/%D9%88%D8%AC%D9%87%D8%A9-%D9%86%D8%B8%D8%B1/feed/',
        'Mantiqti11' => 'https://mantiqti.cairolive.com/category/%D8%A5%D9%86%D9%81%D9%88%D8%AC%D8%B1%D8%A7%D9%81%D9%8A%D9%83/feed/',
        //'Mantiqti12' => 'https://mantiqti.cairolive.com/feed/',
    );
    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }

    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
  $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 60, // timeout on connect
          CURLOPT_TIMEOUT => 60, // timeout on response
          CURLOPT_MAXREDIRS => 5, // stop after 10 redirects
      );
      $ch = curl_init($subUrl);
      curl_setopt_array($ch, $options);
      $response = curl_exec($ch);

      curl_close($ch);
      $fileName = str_replace(' ', '_', $cat . '.xml');
      file_put_contents($fileName, $response);
    }
  }


}
