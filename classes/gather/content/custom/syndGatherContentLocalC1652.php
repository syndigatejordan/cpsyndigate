<?php

class syndGatherContentLocalC1652 extends syndGatherContentLocal {

  public $soapUrl = 'services.countrywatch.com';
  public $token = "CWPARTNER7GK0MSH5FUEP3X";
    public $countryISO = array('Afghanistan' => 'AFG', 'Albania' => 'ALB', 'Algeria' => 'DZA', 'Andorra' => 'AND', 'Angola' => 'AGO', 'Antigua' => 'ATG', 'Argentina' => 'ARG', 'Armenia' => 'ARM', 'Australia' => 'AUS', 'Austria' => 'AUT', 'Azerbaijan' => 'AZE', 'Bahamas' => 'BHS', 'Bahrain' => 'BHR', 'Bangladesh' => 'BGD', 'Barbados' => 'BDS', 'Belarus' => 'BLR'
    , 'Belgium' => 'BEL', 'Belize' => 'BLZ', 'Benin' => 'BEN', 'Bhutan' => 'BTN', 'Bolivia' => 'BOL', 'Bosnia-Herzegovina' => 'BIH', 'Botswana' => 'BWA'
    , 'Brazil' => 'BRA', 'Brunei' => 'BRN', 'Bulgaria' => 'BGR', 'Burkina-Faso' => 'BFA', 'Burma' => 'MMR', 'Burundi' => 'BDI', 'Cambodia' => 'KHM'
    , 'Cameroon' => 'CMR', 'Canada' => 'CAN', 'Cape-Verde' => 'CPV', 'Central-African-Republic' => 'CAF', 'Chad' => 'TCD', 'Chile' => 'CHL', 'China' => 'CHN'
    , 'China-Hong-Kong' => 'HKG', 'China-Taiwan' => 'TWN', 'Colombia' => 'COL', 'Comoros' => 'COM', 'Congo-DRC' => 'COD', 'Congo-RC' => 'COG', 'Costa-Rica' => 'CRI'
    , 'Cote-d-Ivoire' => 'CIV', 'Croatia' => 'HRV', 'Cuba' => 'CUB', 'Cyprus' => 'CYP', 'Czech-Republic' => 'CZE', 'Denmark' => 'DNK', 'Djibouti' => 'DJI'
    , 'Dominica' => 'DMA', 'Dominican-Republic' => 'DOM', 'East-Timor' => 'TLS', 'Ecuador' => 'ECU', 'Egypt' => 'EGY', 'El-Salvador' => 'SLV', 'Equatorial-Guinea' => 'GNQ'
    , 'Eritrea' => 'ERI', 'Estonia' => 'EST', 'Ethiopia' => 'ETH', 'Fiji' => 'FJI', 'Finland' => 'FIN', 'Former-Yugoslav-Rep-of-Macedonia' => 'MKD', 'France' => 'FRA'
    , 'Gabon' => 'GAB', 'Gambia' => 'GMB', 'Georgia' => 'GEO', 'Germany' => 'DEU', 'Ghana' => 'GHA', 'Greece' => 'GRC', 'Grenada' => 'GRD', 'Guatemala' => 'GTM', 'Guinea' => 'GIN'
    , 'Guinea-Bissau' => 'GNB', 'Guyana' => 'GUY', 'Haiti' => 'HTI', 'Holy-See' => 'VAT', 'Honduras' => 'HND', 'Hungary' => 'HUN', 'Iceland' => 'ISL', 'India' => 'IND'
    , 'Indonesia' => 'IDN', 'Iran' => 'IRN', 'Iraq' => 'IRQ', 'Ireland' => 'IRL', 'Israel' => 'ISR', 'Italy' => 'ITA', 'Jamaica' => 'JAM', 'Japan' => 'JPN', 'Jordan' => 'JOR'
      , 'Kazakhstan' => 'KAZ', 'Kenya' => 'KEN', 'Kiribati' => 'KIR', 'Korea-North' => 'PRK', 'Korea-South' => 'KOR', 'Kuwait' => 'KWT', 'Kyrgyzstan' => 'KGZ', 'Laos' => 'LAO', 'Latvia' => 'LVA'
      , 'Lebanon' => 'LBN', 'Lesotho' => 'LSO', 'Liberia' => 'LBR', 'Libya' => 'LBY', 'Liechtenstein' => 'LIE', 'Lithuania' => 'LTU', 'Luxembourg' => 'LUX', 'Madagascar' => 'MDG'
      , 'Malawi' => 'MWI', 'Malaysia' => 'MYS', 'Maldives' => 'MDV', 'Mali' => 'MLI', 'Malta' => 'MLT', 'Malta' => 'MLT', 'Marshall-Islands' => 'MHL', 'Mauritania' => 'MRT', 'Mauritius' => 'MUS'
    , 'Mexico' => 'MEX', 'Micronesia' => 'FSM', 'Moldova' => 'MDA', 'Monaco' => 'MCO', 'Mongolia' => 'MNG', 'Morocco' => 'MAR', 'Mozambique' => 'MOZ', 'Namibia' => 'NAM', 'Nauru' => 'NRU'
    , 'Nepal' => 'NPL', 'Netherlands' => 'NLD', 'New-Zealand' => 'NZL', 'Nicaragua' => 'NIC', 'Niger' => 'NER', 'Nigeria' => 'NGA', 'Norway' => 'NOR', 'Oman' => 'OMN'
    , 'Pakistan' => 'PAK', 'Palau' => 'PLW', 'Panama' => 'PAN', 'Papua-New-Guinea' => 'PNG', 'Paraguay' => 'PRY', 'Peru' => 'PER', 'Philippines' => 'PHL', 'Poland' => 'POL'
    , 'Portugal' => 'PRT', 'Qatar' => 'QAT', 'Romania' => 'ROU', 'Russia' => 'RUS', 'Rwanda' => 'RWA', 'Saint-Kitts-and-Nevis' => 'KNA', 'Saint-Lucia' => 'LCA'
    , 'Saint-Vincent-and-Grenadines' => 'VCT', 'Samoa' => 'WSM', 'San-Marino' => 'SMR', 'Sao-Tome-and-Principe' => 'STP', 'Saudi-Arabia' => 'SAU', 'Senegal' => 'SEN'
    , 'Serbia' => 'SRB', 'Seychelles' => 'SYC', 'Sierra-Leone' => 'SLE', 'Singapore' => 'SGP', 'Slovakia' => 'SVK', 'Slovenia' => 'SVN', 'Solomon-Islands' => 'SLB'
    , 'Somalia' => 'SOM', 'South-Africa' => 'ZAF', 'Spain' => 'ESP', 'Sri-Lanka' => 'LKA', 'Sudan' => 'SDN', 'Suriname' => 'SUR', 'Swaziland' => 'SWZ', 'Sweden' => 'SWE'
    , 'Switzerland' => 'CHE', 'Syria' => 'SYR', 'Tajikistan' => 'TJK', 'Tanzania' => 'TZA', 'Thailand' => 'THA', 'Togo' => 'TGO', 'Tonga' => 'TON', 'Trinidad-Tobago' => 'TTO'
    , 'Tunisia' => 'TUN', 'Turkey' => 'TUR', 'Turkmenistan' => 'TKM', 'Tuvalu' => 'TUV', 'Uganda' => 'UGA', 'Ukraine' => 'UKR', 'United-Arab-Emirates' => 'ARE'
    , 'United-Kingdom' => 'GBR', 'United-States' => 'USA', 'Uruguay' => 'URY', 'Uzbekistan' => 'UZB', 'Vanuatu' => 'VUT', 'Venezuela' => 'VEN', 'Vietnam' => 'VNM', 'Yemen' => 'YEM', 'Zambia' => 'ZMB', 'Zimbabwe' => 'ZWE'
    );

    public function __construct($titleId, $reportId = '')
    {
        $model = new syndModel();
        $homeDirPath = $model->getHomeDir($titleId);

        foreach ($this->countryISO as $iso) {
            $Countryprofile = '<?xml version="1.0" encoding="utf-8"?><CountryWatchReviews>';
            //////////Get Demographic////////////
            /*$Countryprofile .="<subCountryWatch>";
            $Countryprofile .="<category>Demographic,Demographic</category>";
            $Countryprofile .="<text>";
            $Countryprofile .= $this->getCountryTemplate($iso, 6);
            $Countryprofile .="</text>";
            $Countryprofile .="</subCountryWatch>";*/
      //////////Get History////////////
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>History,History</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'POHIS');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";
      //////////Get Politics////////////
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Politics,Political Conditions</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'POPCO');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";
      
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Politics,Human Rights</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'POHRT');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";

      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Politics,Leader Biography</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'POBIO');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";

      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Politics,National Security</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'PONAS');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";

      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Politics,Defense Forces</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'PODEF');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>"; 
            //////////Get Government////////////
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Government,Government Effectiveness</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'POGEF');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";

      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Government,Government Functions</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'POGSY');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";

      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Government,Government Structure</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'POGST');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";

      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Government,Principal Government Officials</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'POPGO');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";
      //////////Get Culture////////////
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Social,People</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'CLPEO');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";
      //////////Get Foreign Policy////////////
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Foreign Policy,Foreign Policy</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'POFOR');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";
      //////////Get Culture////////////
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Culture,Culture and Arts</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'CLART');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Social,Cultural Etiquette</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'CLETQ');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";
      //////////Get Economic Conditions////////////
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Economic,Economic Conditions</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'MAOVR');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";

      //////////Get Investment Climate////////////
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Investment,Investment Climate</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'INFIC');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";     
      //////////Get Taxation////////////
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Investment,Taxation</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'INTAX');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";     
      //////////Get Energy Data////////////
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Economic,Energy Data</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getDatatable($iso, 6);
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";
      //////////Get Metals Data////////////
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Economic,Metals Data</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getDatatable($iso, 8);
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";
      //////////Get Agriculture Data////////////
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Economic,Agriculture Data</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getDatatable($iso, 7);
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";
      //////////Get Environmental Issues////////////
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Environmental,Environmental Issues</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'EVISS');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";
      //////////Get Environmental Policy////////////
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Environmental,Environmental Policy</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'EVPLC');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";
      //////////Get Stock Market////////////
      //////////Get Macroeconomic Data////////////
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Economic,Macroeconomic Data</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getDatatable($iso, 9);
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";
      //////////Get Macroeconomic Forecast////////////
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Data,Macroeconomic Forecast</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getDatatable($iso, 10);
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";
      
      $Countryprofile .="</CountryWatchReviews>";

      $Countryprofile = str_replace('<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body>', "", $Countryprofile);
      $Countryprofile = str_replace('</soap:Body></soap:Envelope>', "", $Countryprofile);
      $Countryprofile = str_replace('xmlns="http://tempuri.org/"', "", $Countryprofile);
      $Countryprofile = preg_replace('!\s+!', ' ', $Countryprofile);
      $Countryprofile = str_replace('<getDatatableResponse >', "", $Countryprofile);
      $Countryprofile = str_replace('<getDatatableResult>', "", $Countryprofile);
      $Countryprofile = str_replace('</getDatatableResult>', "", $Countryprofile);
      $Countryprofile = str_replace('</getDatatableResponse>', "", $Countryprofile);
      $Countryprofile = str_replace('<getCountryReviewSectionResult>', "", $Countryprofile);
      $Countryprofile = str_replace('<getCountryReviewSectionResponse >', "", $Countryprofile);
      $Countryprofile = str_replace('</getCountryReviewSectionResult>', "", $Countryprofile);
      $Countryprofile = str_replace('</getCountryReviewSectionResponse>', "", $Countryprofile);
      $Countryprofile = str_replace('<getCountryTemplateResponse >', "", $Countryprofile);
      $Countryprofile = str_replace('<getCountryTemplateResult>', "", $Countryprofile);
      $Countryprofile = str_replace('</getCountryTemplateResult>', "", $Countryprofile);
      $Countryprofile = str_replace('</getCountryTemplateResponse>', "", $Countryprofile);


      $this->saveNewsFile($Countryprofile, $iso, $homeDirPath);
    }
    $this->MoveFromRootToDateDir($titleId);
    parent::__construct($titleId, $reportId);
  }

  public function getCountryReviewSection($countryISO, $sectionID) {
      $cURLConnection = curl_init();
      curl_setopt($cURLConnection, CURLOPT_URL, 'https://services.countrywatch.com/ws/cwwebservice.asmx/LoginToken?Token=CWPARTNER7GK0MSH5FUEP3X');
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => true, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );
      curl_setopt($cURLConnection, CURLOPT_COOKIEJAR, '/tmp/getCountryReviewSection1652.txt');
      curl_setopt($cURLConnection, CURLOPT_COOKIEFILE, '/tmp/getCountryReviewSection1652.txt');
      curl_setopt_array($cURLConnection, $options);
      $response = curl_exec($cURLConnection);
      curl_close($cURLConnection);
      // var_dump($response);


      $cURLConnection = curl_init();
      curl_setopt($cURLConnection, CURLOPT_URL, "https://services.countrywatch.com/ws/cwwebservice.asmx/getCountryReviewSection?Token=CWPARTNER7GK0MSH5FUEP3X&CountryISO=$countryISO&SectionID=$sectionID");
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );
      curl_setopt($cURLConnection, CURLOPT_COOKIEJAR, '/tmp/getCountryReviewSection1652.txt');
      curl_setopt($cURLConnection, CURLOPT_COOKIEFILE, '/tmp/getCountryReviewSection1652.txt');
      curl_setopt_array($cURLConnection, $options);
      $response = curl_exec($cURLConnection);
      curl_close($cURLConnection);
      return $response;
  }


  public function getDatatable($countryISO, $Tid)
  {
      $cURLConnection = curl_init();
      curl_setopt($cURLConnection, CURLOPT_URL, 'https://services.countrywatch.com/ws/cwwebservice.asmx/LoginToken?Token=CWPARTNER7GK0MSH5FUEP3X');
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => true, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );
      curl_setopt($cURLConnection, CURLOPT_COOKIEJAR, '/tmp/getDataTableCountryWatch.txt');
      curl_setopt($cURLConnection, CURLOPT_COOKIEFILE, '/tmp/getDataTableCountryWatch.txt');
      curl_setopt_array($cURLConnection, $options);
      $response = curl_exec($cURLConnection);
      curl_close($cURLConnection);
      // var_dump($response);


      $cURLConnection = curl_init();
      curl_setopt($cURLConnection, CURLOPT_URL, "https://services.countrywatch.com/ws/cwwebservice.asmx/getDatatable?Token=CWPARTNER7GK0MSH5FUEP3X&CountryISO=$countryISO&TableCategoryID=$Tid");
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );
      curl_setopt($cURLConnection, CURLOPT_COOKIEJAR, '/tmp/getDataTableCountryWatch.txt');
      curl_setopt($cURLConnection, CURLOPT_COOKIEFILE, '/tmp/getDataTableCountryWatch.txt');
      curl_setopt_array($cURLConnection, $options);
      $response = curl_exec($cURLConnection);
      curl_close($cURLConnection);
      return $response;
  }

  public function saveNewsFile($str, $ISO, $homeDirPath) {
    $file_name = $ISO . ".xml";
    $file_name = $homeDirPath . DIRECTORY_SEPARATOR . $file_name;
    $fp = fopen($file_name, 'w');
    fwrite($fp, $str);
    fclose($fp);
  }


}
