<?php

class syndGatherContentLocalC1778 extends syndGatherContentLocal {

  public function __construct($titleId, $reportId = '') {

    $c = new Criteria();
    $c->add(TitlePeer::ID, $titleId);
    $currentTitle = TitlePeer::doSelectOne($c);
    $currentDirName = $currentTitle->getHomeDir();
    $publiserDirName = $currentTitle->getHomeDir() . "../";

    $directories = scandir($publiserDirName);
    foreach ($directories as $File) {
      $path = pathinfo($File);
      $path = $path['extension'];
      if (($File != '.') && ($File != '..') && (is_file("{$publiserDirName}{$File}")) && $path == "xml" && strpos($File, "BRUNCH_") !== FALSE) {
        var_dump($File);
        $move = "mv " . $publiserDirName . $File . " " . $currentDirName;
        echo $move . chr(10);
        shell_exec($move);
      }
    }
    $this->MoveFromRootToDateDir($titleId);
    parent::__construct($titleId, $reportId);
  }

}

?>
