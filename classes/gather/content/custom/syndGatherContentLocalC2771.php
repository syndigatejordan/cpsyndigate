<?php

class syndGatherContentLocalC2771 extends syndGatherContentLocal {

  public function __construct($titleId, $reportId = '') {
    $subUrl = 'http://www.ynet.co.il/iphone/json/0,,3083-ip,00.js';
/////////////////////////////////    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $subUrl);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
    curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
// This is what solved the issue (Accepting gzip encoding)
    curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    $fileContent = curl_exec($ch);
    curl_close($ch);
    $content = json_decode($fileContent, true);

//////////////////////////////
    foreach ($content["components"] as &$element) {
      if (!empty($element["link_data"]["id"])) {
        $id = $element["link_data"]["id"];
        echo "Get article id $id" . PHP_EOL;
        $article = $this->getArticles($id);
        $element["article"] = $article["rss"]["channel"]["item"];
        // creating object of SimpleXMLElement
        $xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');

        // function call to convert array to xml
        $this->array_to_xml($element, $xml_data);
        $date = date('Y/m/d');
        $directory = "/syndigate/sources/home/970/2771/$date";
        if (!is_dir($directory)) {
          mkdir($directory, 0777, true);
        }
        //saving generated xml file; 
        $result = $xml_data->asXML("$directory/article_$id.xml");
      } else {
        $this->getArrayArticles($element);
      }
    }
    parent::__construct($titleId, $reportId);
  }

  public function getArticles($id) {
    $subUrl = "http://www.ynet.co.il/Iphone/0,13257,L-Article-V7-$id,00.js";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $subUrl);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
    curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
// This is what solved the issue (Accepting gzip encoding)
    curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    $fileContent = curl_exec($ch);
    curl_close($ch);
    $content = json_decode($fileContent, true);
    return $content;
  }

  public function getArrayArticles($elements) {
    foreach ($elements["items"] as $element) {
      if (!empty($element["link_data"]["id"])) {
        $id = $element["link_data"]["id"];
        echo "Get article id $id" . PHP_EOL;
        $article = $this->getArticles($id);
        $element["article"] = $article["rss"]["channel"]["item"];
        // creating object of SimpleXMLElement
        $xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');

        // function call to convert array to xml
        $this->array_to_xml($element, $xml_data);
        $date = date('Y/m/d');
        $directory = "/syndigate/sources/home/970/2771/$date";
        if (!is_dir($directory)) {
          mkdir($directory, 0777, true);
        }
        //saving generated xml file; 
        $result = $xml_data->asXML("$directory/article_$id.xml");
      } else {
        $this->getArrayArticles($element);
      }
    }
  }

  public function array_to_xml($data, &$xml_data) {
    foreach ($data as $key => $value) {
      if (is_numeric($key)) {
        $key = 'item' . $key; //dealing with <0/>..<n/> issues
      }
      if (is_array($value)) {
        $subnode = $xml_data->addChild($key);
        $this->array_to_xml($value, $subnode);
      } else {
        $xml_data->addChild("$key", htmlspecialchars("$value"));
      }
    }
  }

}

?>