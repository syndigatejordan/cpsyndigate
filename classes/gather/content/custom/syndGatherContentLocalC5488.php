<?php

class syndGatherContentLocalC5488 extends syndGatherContentLocal {

  public function __construct($titleId, $reportId = '') {
    $currentDirName = "/syndigate/sources/home/1218/5488/";
    $publiserDirName = "/syndigate/sources/home/1218/5635/";
    $directories = scandir($publiserDirName);
    foreach ($directories as $file) {
      $path = pathinfo($file);
      $path = $path['extension'];
      if (($file != '.') && ($file != '..') && (is_file("{$publiserDirName}{$file}")) && $path == "xml") {
        $fileContent = file_get_contents($publiserDirName . $file);
        $matches = null;
        preg_match_all("/<item>(.*?)<\/item>/is", $fileContent, $matches);
        foreach ($matches[0] as $match) {
          $image_id = $this->getElementByName("image_id", $match);
          $file_name = $this->getElementByName("file_name", $match);

          var_dump($image_id);
          var_dump($file_name);
          $ext = explode(".", $file_name);
          $ext = end($ext);
          var_dump(is_file($publiserDirName . $file_name));
          if (is_file($publiserDirName . $file_name)) {
            rename($publiserDirName . $file_name, $publiserDirName . $image_id . ".$ext");
            $move = "mv " . $publiserDirName . $image_id . ".$ext" . " " . $currentDirName;
            echo $move . chr(10);
            shell_exec($move);
            file_put_contents($currentDirName . $image_id . ".xml", $match);
          }
        }
      }
    }
    $this->MoveFromRootToDateDir($titleId);
    parent::__construct($titleId, $reportId);
  }

  public function getElementsByName($name, $text) {
    $openTagRegExp = '<' . $name . '([^\>]|[\s])*>';
    $closeTagRegExp = '<\/' . $name . '[\s]*>';
    $elements = preg_split("/$openTagRegExp/i", $text);

    $elementsContents = array();
    $elementsCount = count($elements);
    for ($i = 1; $i < $elementsCount; $i++) {
      $element = preg_split("/$closeTagRegExp/i", $elements[$i]);
      $elementsContents[] = $element[0];
    }
    return $elementsContents;
  }

  /**
   * get first xml element contents
   *
   * @param String $name element name
   * @param String $text all xml text
   * @return string
   */
  public function getElementByName($name, $text) {
    $element = $this->getElementsByName($name, $text);
    return (isset($element[0]) ? $element[0] : null);
  }

}
