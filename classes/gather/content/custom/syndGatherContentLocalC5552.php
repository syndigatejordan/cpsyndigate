<?php

class syndGatherContentLocalC5552 extends syndGatherContentLocal {

  public function __construct($titleId, $reportId = '') {
    $c = new Criteria();
    $c->add(TitlePeer::ID, $titleId);
    $currentTitle = TitlePeer::doSelectOne($c);
    $currentDirName = $currentTitle->getHomeDir();

    $directories = scandir($currentDirName);
    foreach ($directories as $file) {
      $path = pathinfo($file);
      if (!empty($path['extension'])) {
        $path = $path['extension'];
      } else {
        $path = "";
      }

      if (($file != '.') && ($file != '..') && (is_file("{$currentDirName}{$file}")) && $path == "mp4") {
        $date = date("Ymd\THis");

        $newfile = sha1($file) . "_$date.mp4";
        $xmlfile = sha1($file) . "_$date.xml";

        $title = preg_replace("/\.(.*)/", "", $file);

        $guid = preg_replace("/\.(.*)/", "", $file) . " $date";
        $guid = sha1($guid);


        $xml = '<?xml version="1.0" encoding="utf-8"?>';
        $xml.="<item>";
        $xml.="<pubDate>$date</pubDate>";
        $xml.="<title>$title</title>";
        $xml.="<file>$newfile</file>";
        $xml.="<guid>$guid</guid>";
        $xml.="</item>";

        $path["basename"] = preg_replace("/[  ~&,:\-\)\(\']/", ' ', $path["basename"]);
        $path["basename"] = str_replace('..', '.', $path["basename"]);
        $path["basename"] = str_replace("'", "\'", $path["basename"]);
        $path["basename"] = str_replace("\'", "", $path["basename"]);
        $path["basename"] = str_replace("`", "\`", $path["basename"]);
        $path["basename"] = str_replace("\`", "", $path["basename"]);
        $path["basename"] = str_replace("\$", "\\$", $path["basename"]);
        $path["basename"] = str_replace("\$", "", $path["basename"]);
        $path["basename"] = str_replace('  ', ' ', $path["basename"]);
        $path["basename"] = preg_replace('!\s+!', '_', $path["basename"]);
        $path["basename"] = str_replace('._', '_', $path["basename"]);
        $path["basename"] = str_replace(' _', '_', $path["basename"]);
        $mvFile = str_replace(' ', '\ ', $file);
        $mvFile = str_replace('(', '\(', $mvFile);
        $mvFile = str_replace(')', '\)', $mvFile);
        $mvFile = str_replace("'", "\'", $mvFile);
        $mvFile = str_replace("`", "\`", $mvFile);
        $mvFile = str_replace("&", "\&", $mvFile);
        $mvFile = str_replace("\$", "\\$", $mvFile);
        $move = "mv " . $currentDirName . $mvFile . " " . $currentDirName . $newfile;
        if ($mvFile != $path["basename"]) {
          echo $move . chr(10);
          shell_exec($move);
        }
        file_put_contents($currentDirName . $xmlfile, $xml);
      }
    }
    $this->MoveFromRootToDateDir($titleId);
    parent::__construct($titleId, $reportId);
  }

}
