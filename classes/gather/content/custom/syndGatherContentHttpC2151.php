<?php

class syndGatherContentHttpC2151 extends syndGatherContentHttp {

  protected function getRssUrl() {
    $urls = array(
        'Abu Nawaf 1' => 'https://abunawaf.com/category/%d9%81%d9%8a%d8%af%d9%8a%d9%88/feed/',
        'Abu Nawaf 2' => 'https://abunawaf.com/category/%d9%85%d9%86%d9%88%d8%b9%d8%a7%d8%aa/feed/',
        'Abu Nawaf 3' => 'https://abunawaf.com/category/%d8%a7%d8%aa%d8%b5%d8%a7%d9%84%d8%a7%d8%aa-%d9%88-%d8%aa%d9%82%d9%86%d9%8a%d8%a9/feed/',
        'Abu Nawaf 4' => 'https://abunawaf.com/category/%d8%a7%d8%a8%d8%af%d8%a7%d8%b9%d8%a7%d8%aa-%d8%a8%d8%b5%d8%b1%d9%8a%d8%a9/feed/',
        'Abu Nawaf 5' => 'https://abunawaf.com/category/%d8%ad%d9%88%d9%84-%d8%a7%d9%84%d8%b9%d8%a7%d9%84%d9%85/feed/',
        'Abu Nawaf 6' => 'https://abunawaf.com/category/%d9%85%d8%ad%d9%84%d9%8a%d8%a7%d8%aa-%d8%b3%d8%b9%d9%88%d8%af%d9%8a%d8%a9/feed/',
        'Abu Nawaf 7' => 'https://abunawaf.com/category/%d8%ba%d8%b1%d8%a7%d8%a6%d8%a8-%d9%88-%d8%b9%d8%ac%d8%a7%d8%a6%d8%a8/feed/',
    );
    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $subUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $response = curl_exec($ch);
      curl_close($ch);
      $fileName = str_replace(' ', '_', $cat . '.xml');
      file_put_contents($fileName, $response);
    }
  }

}
