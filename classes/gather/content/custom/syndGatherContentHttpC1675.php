<?php

class syndGatherContentHttpC1675 extends syndGatherContentHttp {

  protected function getRssUrl() {
    $urls = array(
        "big-data" => "https://www.geektime.com/category/big-data/feed",
        "consumer" => "https://www.geektime.com/category/consumer/feed",
        "design" => "https://www.geektime.com/category/design/feed",
        "development" => "https://www.geektime.com/category/development/feed",
        "ecommerce" => "https://www.geektime.com/category/ecommerce/feed",
        "education" => "https://www.geektime.com/category/education/feed",
        "entrepreneurship" => "https://www.geektime.com/category/entrepreneurship/feed",
        "fintech" => "https://www.geektime.com/category/fintech/feed",
        "gaming" => "https://www.geektime.com/category/gaming/feed",
        "greentech" => "https://www.geektime.com/category/greentech/feed",
        "hardware" => "https://www.geektime.com/category/hardware/feed",
        "health" => "https://www.geektime.com/category/health/feed",
        "industry" => "https://www.geektime.com/category/industry/feed",
        "innovation" => "https://www.geektime.com/category/innovation/feed",
        "internet-of-things" => "https://www.geektime.com/category/internet-of-things/feed",
        "internet" => "https://www.geektime.com/category/internet/feed",
        "it" => "https://www.geektime.com/category/it/feed",
        "lists" => "https://www.geektime.com/category/lists/feed",
        "marketing" => "https://www.geektime.com/category/marketing/feed",
        "media" => "https://www.geektime.com/category/media/feed",
        "mobile" => "https://www.geektime.com/category/mobile/feed",
        "news" => "https://www.geektime.com/category/news/feed",
        "policy" => "https://www.geektime.com/category/policy/feed",
        "robotics" => "https://www.geektime.com/category/robotics/feed",
        "science" => "https://www.geektime.com/category/science/feed",
        "security" => "https://www.geektime.com/category/security/feed",
        "social-media" => "https://www.geektime.com/category/social-media/feed",
        "telecom" => "https://www.geektime.com/category/telecom/feed",
        "transportation" => "https://www.geektime.com/category/transportation/feed",
        "venture-capital" => "https://www.geektime.com/category/venture-capital/feed",
        "wearables" => "https://www.geektime.com/category/wearables/feed",
        "Geektime"=>"https://www.geektime.com/feed/",
    );

    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $subUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $response = curl_exec($ch);
      curl_close($ch);
      $fileName = str_replace(' ', '_', $cat . '.xml');
      file_put_contents($fileName, $response);
    }
  }

}
