<?php
class syndGatherContentLocalC394 extends syndGatherContentLocal
{    
	
    public function __construct ($titleId, $reportId = '') {    	
    	
    	$bbcSendingUrl 	= '/syndigate/sources/home/144/394/arabic/';
    	$dirName 		= '/syndigate/sources/home/144/394/' . date('Y/m/d/');
		$files   		= $this->directoryToArray($bbcSendingUrl, true);		
				
		if(count($files)>0) {	
			if(!is_dir($dirName)) {
				mkdir($dirName, 0777, true);				
			}			
			
			foreach ($files as $file) {	
				
				if(is_file($file)) {						
					$tmp = dirname($file);		
					$tmp = explode('/', $tmp);	
					$file2 = $dirName . $tmp[count($tmp)-1] . '.xml';			
					
					rename($file, $file2);		
					rmdir(dirname($file));		
				}
			}
		}

		if(is_dir($bbcSendingUrl)) {
			rmdir($bbcSendingUrl);
		}

		parent::__construct($titleId, $reportId);
    }
    
    
    function directoryToArray($directory, $recursive) {
		$array_items = array();
		if ($handle = opendir($directory)) {
			while (false !== ($file = readdir($handle))) {
				if ($file != "." && $file != "..") {
					if (is_dir($directory. "/" . $file)) {
						if($recursive) {
							$array_items = array_merge($array_items, $this->directoryToArray($directory. "/" . $file, $recursive));
						}
						$file = $directory . "/" . $file;
						$array_items[] = preg_replace("/\/\//si", "/", $file);
					} else {
						$file = $directory . "/" . $file;
						$array_items[] = preg_replace("/\/\//si", "/", $file);
					}
				}
			}
			closedir($handle);
		}
		return $array_items;
    }
    
    
}