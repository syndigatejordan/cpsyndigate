<?php

class syndGatherContentLocalC1753 extends syndGatherContentLocal {

public function __construct($titleId, $reportId = '') {
    //parent::__construct($titleId, $reportId);
    $custom_dirName = '/syndigate/sources/home/606/EmployersLaw/';
    $title_dir = '/syndigate/sources/home/606/1753/';
    chdir($custom_dirName);
    $files = scandir($custom_dirName);
    foreach ($files as $file) {
      if ($file != "." && $file != ".." && is_file($file)) {
        $file = str_replace(' ', "\ ", $file);
        $file = str_replace('&', "\&", $file);
        $file = str_replace("'", "\'", $file);
        $file = $custom_dirName . $file;
        $cond = "mv $file $title_dir";
        exec($cond);
      }
    }
    chdir("/usr/local/syndigate/classes/gather/content/custom");
    $this->MoveFromRootToDateDir($titleId);
    parent::__construct($titleId, $reportId);
  }

}
