<?php

class syndGatherContentLocalC1527 extends syndGatherContentLocal {

  public function __construct($titleId, $reportId = '') {
    //parent::__construct($titleId, $reportId);
    $custom_dirName = '/syndigate/sources/home/512/';
    chdir($custom_dirName);
    $dirs = scandir($custom_dirName);
    $dir_arr = array(1528 => 'BenefitsPro', 1532 => 'CreditUnionTimes', 1531 => 'InsideCounsel'
        , 1529 => 'LifeHealthPro', 1530 => 'PropertyCasualty360', 1527 => 'ThinkAdvisor');
    for ($days = 1; $days <= 3; $days++) {
      $date_dir = $this->getDateDir(date('Ymd', time()));
      if ($days == 2)
        $date_dir = $this->getDateDir(date('Ymd', strtotime("-1 days")));
      if ($days == 3)
        $date_dir = $this->getDateDir(date('Ymd', strtotime("-2 days")));
      foreach ($dirs as $dir) {
        if ($dir != "." && $dir != ".." && in_array($dir, $dir_arr)) {

          //echo $dir . ' ' . array_search($dir, $dir_arr) . chr(10);
          $tmp_title_dir = $dir . DIRECTORY_SEPARATOR . $date_dir;
          $title_id = array_search($dir, $dir_arr);
          if ($title_id == 1527) {
            $title_dir = 1529 . DIRECTORY_SEPARATOR . $date_dir;
            if (!is_dir($title_dir)) {
              mkdir($title_dir, 0777, true);
            }
            if (is_dir($tmp_title_dir)) {
              $files = scandir($tmp_title_dir);
              foreach ($files as $file) {
                $file_full_path = $tmp_title_dir . DIRECTORY_SEPARATOR . $file;
                if ($file != "." && $file != ".." && is_file($file_full_path)) {
                  // move file from TMP Title DIR to Title DIR
                  if (!is_file("$custom_dirName$title_dir/$file")) {
                    $command = "cp $file_full_path $custom_dirName$title_dir/";
                    shell_exec($command);
                  } else {
                    echo "File: $custom_dirName$title_dir/$file Already copied" . chr(10);
                  }
                }
              }
            }
          }
          $title_dir = $title_id . DIRECTORY_SEPARATOR . $date_dir;
          if (!is_dir($title_dir)) {
            mkdir($title_dir, 0777, true);
          }
          if (is_dir($tmp_title_dir)) {
            $files = scandir($tmp_title_dir);
            foreach ($files as $file) {
              $file_full_path = $tmp_title_dir . DIRECTORY_SEPARATOR . $file;
              if ($file != "." && $file != ".." && is_file($file_full_path)) {
                // move file from TMP Title DIR to Title DIR
                if (!is_file("$custom_dirName$title_dir/$file")) {
                  $command = "cp $file_full_path $custom_dirName$title_dir/";
                  shell_exec($command);
                } else {
                  echo "File: $custom_dirName$title_dir/$file Already copied" . chr(10);
                }
              }
            }
          }
        }
      }
    }
    chdir("/usr/local/syndigate/classes/gather/content/custom");
    parent::__construct($titleId, $reportId);
  }

  private function getDateDir($dateDir) {
    $year = substr($dateDir, 0, 4);
    $month = substr($dateDir, 4, 2);
    $day = substr($dateDir, 6, 2);
    //$directory = $destination . $year . DIRECTORY_SEPARATOR . $month . DIRECTORY_SEPARATOR . $day;
    $directory = $year . DIRECTORY_SEPARATOR . $month . DIRECTORY_SEPARATOR . $day;
    return $directory;
  }

}
