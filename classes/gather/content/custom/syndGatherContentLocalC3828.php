<?php

class syndGatherContentLocalC3828 extends syndGatherContentLocal {

  public function __construct($titleId, $reportId = '') {
    $this->MoveFromRootToDateDir($titleId);
    parent::__construct($titleId, $reportId);
  }

  /**
   * Function to copy the new and updated file from the home dir to the working dir
   * @return 	array		
   */
  public function copyContent() {

    $checkDirs = $this->checkContent();

    if (is_array($checkDirs) && !$this->firstTime) {
      unset($this->svnAdd);
      $this->log->log('Copying the content for ' . $this->titleName, ezcLog::DEBUG, $this->logParams);
      foreach ($checkDirs as $file) {

        if (is_dir($this->homeDirPath . $file)) {
          chdir($this->workDirPath);
          $fl = explode('/', $file);
          array_pop($fl);
          $path = $this->workDirPath;
          foreach ($fl as $folder) {
            if (is_dir($path . $folder)) {
              if (chdir($path . $folder)) {
                $path = $path . $folder . '/';
                $this->svnAdd[] = $path;
              }
            } else {
              if (mkdir($path . '/' . $folder)) {
                $path = $path . $folder . '/';
                $this->svnAdd[] = $path;
              }
            }
          }

          if (is_dir($this->homeDirPath . $file)) {
            $file.= '/';
          }
          ezcBaseFile::copyRecursive($this->homeDirPath . $file, $this->workDirPath . $file);
          $this->svnAdd[] = $this->workDirPath . $file;
        } else {
          $tmp = explode("/", $file);
          $newDir = $this->workDirPath;
          for ($count = 0; $count < count($tmp) - 1; $count++) {
            $newDir .= $tmp[$count] . "/";
            if (!is_dir($newDir)) {
              mkdir($newDir);
              $this->svnAdd[] = $newDir;
            }
          }

          $MIMEType = ezcBaseFile::getMimeContentType($this->homeDirPath . $file);

          if (strstr($MIMEType, "video") || strstr($MIMEType, "audio") || strstr($MIMEType, "image")) {
            $command = touch($this->workDirPath . $file);
          } else {
            $command = copy($this->homeDirPath . $file, $this->workDirPath . $file);
          }

          $this->log->log('coping ' . $this->homeDirPath . $file . ' to ' . $this->workDirPath . $file, ezcLog::DEBUG, $this->logParams);

          //if (copy($this->homeDirPath . $file, $this->workDirPath . $file)) {
          if ($command) {
            $this->svnAdd[] = $this->workDirPath . $file;
            $this->log->log('File ' . $file . ' copied successfully for Title: ' . $this->titleName, ezcLog::DEBUG, $this->logParams);
          } else {
            $this->errorMsg = 'File ' . $file . ' could not copied for Title: ' . $this->titleName;
            $this->reportParams['TitleId'] = $this->titleId;
            $this->reportParams['LatestStatus'] = 0;
            $this->reportParams['Error'] = $this->errorMsg;
            $this->report->updateReport($this->reportParams);
            $this->log->log($this->errorMsg, ezcLog::ERROR, $this->logParams);
          }
        }
      }
    } elseif ($this->firstTime) {
      foreach ($this->homeArray as $folder) {
        if (is_dir($this->homeDirPath . $folder)) {
          $folderName = substr($folder, strlen($this->homeDirPath));
          ezcBaseFile::copyRecursive($this->homeDirPath . $folder, $this->workDirPath . $folder);
          $this->svnAdd[] = $this->workDirPath . $folder;
        } else {
          if (copy($this->homeDirPath . $folder, $this->workDirPath . $folder)) {
            $this->svnAdd[] = $this->workDirPath . $folder;
            $this->log->log('File ' . $folder . ' copied successfully for Title: ' . $this->titleName, ezcLog::DEBUG, $this->logParams);
          } else {
            $this->errorMsg = 'File ' . $folder . ' could not copied for Title: ' . $this->titleName;
            $this->reportParams['TitleId'] = $this->titleId;
            $this->reportParams['LatestStatus'] = 0;
            $this->reportParams['Error'] = $this->errorMsg;
            $this->report->updateReport($this->reportParams);
            $this->log->log($this->errorMsg, ezcLog::ERROR, $this->logParams);
          }
        }
      }
    }


    //
    //Removing binary files from working copy
    //
	/*
      foreach ($this->svnAdd as $location) {
      if(is_dir($location)) {
      $files = ezcBaseFile::findRecursive($location);
      foreach ($files as $f) {
      $f = str_replace('//', '/', $f);
      if($this->isBinaryFile($f)) {
      unlink($f);
      }
      }
      } else{
      $loc = str_replace('//', '/', $location);
      if($this->isBinaryFile($loc)) {
      unlink($loc);
      }
      }
      }
     */

    // Commiting and return the revision number
    $rev = $this->svn->svnCommit($this->svnAdd, $this->deletedContent, $this->workDirPath);

    $parsingStateCriteria = new Criteria();
    $parsingStateCriteria->add(ParsingStatePeer::TITLE_ID, $this->title->getId());
    $parsingStateCriteria->add(ParsingStatePeer::REVISION_NUM, $rev);
    //$parsingStateCriteria->add(ParsingStatePeer::PID, 0);
    $parsingStateCriteria->addDescendingOrderByColumn(ParsingStatePeer::ID);
    $parsingState = ParsingStatePeer::doSelectOne($parsingStateCriteria);

    if (is_object($parsingState)) {
      $parsingState->setReportGatherId($this->logParams['report_gather_id']);
      $parsingState->save();

      //Also add the revision number to the report
      $c = new Criteria();
      $c->add(ReportPeer::TITLE_ID, $this->titleId);
      $c->add(ReportPeer::REPORT_GATHER_ID, $this->logParams['report_gather_id']);
      $report = ReportPeer::doSelectOne($c);
      $report->setRevisionNum($rev);
      $report->save();
    }

    $binPath = '/syndigate/sources/binary/' . $this->title->getPublisherId() . '/' . $this->titleId . '/' . ((abs(crc32($rev)) % 1000) + 1) . "/$rev";


    if ($rev && is_dir($this->binTmpDir)) {
      if (!is_dir(dirname($binPath))) {
        mkdir(dirname($binPath), 0777, true);
      }
      rename($this->binTmpDir, $binPath);
    }
  }

}
