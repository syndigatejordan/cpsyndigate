<?php

class syndGatherContentHttpC4810 extends syndGatherContentHttp {

  protected function getRssUrl() {
    $urls = array(
        "Bloomberg" => "https://api.bloomberg.com/syndication/rss/v1/news/a9700a84-27e5-4bc1-9e9a-f0f120935620?access_token=b40d40e0cb5628de94184c1adeafbe7e",
    );
    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    $username = 'techsupport@syndigate.info';
    $password = 'Bloom@123';
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $subUrl);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Host: mediasource.bloomberg.com',
          'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:63.0) Gecko/20100101 Firefox/63.0',
          'Accept: application/json, text/plain, */*',
          'Accept-Language: en-US,en;q=0.5',
          'Accept-Encoding: gzip, deflate, br',
          'Referer: https://mediasource.bloomberg.com/search/',
          'X-CSRF-TOKEN: Ao+66IgGOWYuq9YcCY/XrQi5cihkCWRiy1mRWsq+rG4=',
          'Connection: keep-alive',
          'Cookie: agent_id=fb1d396e-58ca-4b13-82a8-769aec89859f; session_id=f793557a-1a23-4d11-b4d3-623b198ee2de; session_key=6fd2b7d1a3c401eb0af8f6e2a7bf9e0c6b4592d2; _bmg_portal_session=Nk9lWlVsN2tGWSszODd0emtlUmdHd3Y5YUtBM1djbzI4WjEwR0N1ZlhHMTZHcXdnSWd3RG93Tnp4NkdKazBuYzN4WXZGRGx3U0cyVGdUUGhtWmN2eEVGcklDdkhiZTlIMk04ZWZXQUw2RVdQa01iS3EzbXZoQ1pKMzRRRGdmK3hqdnFCOTQxK1N2OElMeVJXemFpYnpCTG5WR21lbW9hZ2xCOHRuVUFrUzZMNmQ3emt3Q2NCK292aitnZk8yMXlENVVMYnM0RE92a0U4ZTd1OTJJNzYzOVVkMzc0L0wwdGgvdzRHbklyOEU2ekJhZ3NFWXk2aDFNTHNaeXJRcmVUM0FyR3RlVDFhb0dOQjRCVlFtZ2M5Q3c9PS0tMG9Gb2NXRm9RdDcwS1lLeDlNNXRXUT09--697fc71fd83815ae2a4224ce5c7fa87471b2ed06; intercom-session-w02gdylr=cGJQRC9kOHcxZStRYWg2SldFMjBzV0FLdUtqUXRkV1pUVGM5TE1QK2VYRXpFd3dJcTdyWTVWM0RKQXJQNE9VeS0tejdkMmsvZlNhZWV0bkphZWlOUy9ZQT09--b48db3e549c648142aecdc6b74040aaebb3c6be0',
          'If-None-Match: "3f59b2605933c1fdfdce35ec9b1b74e8"',
          'Cache-Control: max-age=0',
          'TE: Trailers'));
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      //  curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
// This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $response = curl_exec($ch);
      curl_close($ch);
      $fileName = str_replace(' ', '_', $cat . '.xml');
      file_put_contents($fileName, $response);
    }
  }

}
