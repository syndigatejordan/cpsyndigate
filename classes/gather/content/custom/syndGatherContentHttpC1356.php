<?php

/*
 * Al-Masry Al-Youm
 */

class syndGatherContentHttpC1356 extends syndGatherContentHttp {
	protected $urls = array(
		'1' => 'http://www.almasryalyoum.com/rss/rssfeeds', 
		'2' => 'http://www.almasryalyoum.com/rss/rssfeeds?sectionId=3', 
		'3' => 'http://www.almasryalyoum.com/rss/rssfeeds?sectionId=2', 
		'4' => 'http://www.almasryalyoum.com/rss/rssfeeds?sectionId=5', 
		'5' => 'http://www.almasryalyoum.com/rss/rssfeeds?sectionId=6', 
		'6' => 'http://www.almasryalyoum.com/rss/rssfeeds?sectionId=7', 
		'7' => 'http://www.almasryalyoum.com/rss/rssfeeds?sectionId=10', 
		'8' => 'http://www.almasryalyoum.com/rss/rssfeeds?sectionId=11', 
		'9' => 'http://www.almasryalyoum.com/rss/rssfeeds?sectionId=12', 
		'10' => 'http://www.almasryalyoum.com/rss/rssfeeds?sectionId=13', 
		'11' => 'http://www.almasryalyoum.com/rss/rssfeeds?sectionId=4', 
		'12' => 'http://www.almasryalyoum.com/rss/rssfeeds?sectionId=8', 
		'13' => 'http://www.almasryalyoum.com/rss/rssfeeds?sectionId=9', 
	);

	public function setOpt() {
		parent::setOpt();
		curl_setopt($this -> ch, CURLOPT_ENCODING, 'text');
	}

	public function checkContent($subUrl = '', $cat = '', $check = false) {
		$this -> check = $check;
		if ($check) {
			if (!$this -> ch = curl_init($subUrl)) {
				$this -> processState = ezcLog::FAILED_AUDIT;
				$this -> reportParams['Error'] = $this -> errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
				$this -> reportParams['LatestStatus'] = 0;
				$this -> report -> updateReport($this -> reportParams);
				$this -> log -> log($this -> errorMsg, ezcLog::ERROR, array('report_id' => $this -> reportId));
				return false;
			} else {
				$this -> errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
				$this -> log -> log($this -> errorMsg, ezcLog::DEBUG, array('report_id' => $this -> reportId));
				return true;
			}
		}
		$fileName = str_replace(' ', '_', $cat . '.xml');
		if (!$this -> fp = fopen($fileName, "w")) {
			//Log the proccess for debugging
			$this -> processState = ezcLog::FAILED_AUDIT;
			$this -> reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this -> title . ' Filename: ' . $fileName;
			$this -> reportParams['LatestStatus'] = 0;
			$this -> report -> updateReport($this -> reportParams);
			$this -> log -> log($this -> errorMsg, ezcLog::ERROR, array('report_id' => $this -> reportId));
			return false;
		} else {
			$this -> errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this -> title . ' Filename: ' . $fileName;
			$this -> log -> log($this -> errorMsg, ezcLog::DEBUG, array('report_id' => $this -> reportId));
			if (file_exists('.xml')) {
				unlink('.xml');
			}
		}

		if (!$this -> ch = curl_init($subUrl)) {
			$this -> processState = ezcLog::FAILED_AUDIT;
			$this -> reportParams['Error'] = $this -> errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
			$this -> reportParams['LatestStatus'] = 0;
			$this -> report -> updateReport($this -> reportParams);
			$this -> log -> log($this -> errorMsg, ezcLog::ERROR, array('report_id' => $this -> reportId));
			return false;
		} else {
			$this -> errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
			$this -> log -> log($this -> errorMsg, ezcLog::DEBUG, array('report_id' => $this -> reportId));
		}
	}

	public function copyContent() {
		foreach ($this->urls as $cat => $subUrl) {
			$this -> checkContent($subUrl, $cat);
			$this -> setOpt();
			//grab URL and pass it to the browser
			if (!curl_exec($this -> ch)) {
				$this -> processState = ezcLog::FAILED_AUDIT;
				$this -> reportParams['Error'] = $this -> errorMsg = 'Could not Perform the cURL session of ' . $subUrl;
				$this -> reportParams['LatestStatus'] = 0;
				$this -> report -> updateReport($this -> reportParams);
				$this -> log -> log($this -> errorMsg, ezcLog::ERROR, array('report_id' => $this -> reportId));
			} else {
				$this -> errorMsg = 'Perform the cURL session';
				$this -> log -> log($this -> errorMsg, ezcLog::DEBUG, array('report_id' => $this -> reportId));

				curl_close($this -> ch);
				fclose($this -> fp);
			}
		}
	}

}
?>
