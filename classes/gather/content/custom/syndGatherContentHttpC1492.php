<?php

class syndGatherContentHttpC1492 extends syndGatherContentHttp {

  protected function getRssUrl() {
    $urls = array(
        'Green Israel' => 'https://www.jpost.com/Rss/RssFeedsGreenIsrael.aspx?id=syndigate',
        'International Cooperation' => 'https://www.jpost.com/Rss/RssFeedsInternationalCooperation.aspx?id=syndigate',
        'Kkl Jnf World Wide' => 'https://www.jpost.com/Rss/RssFeedsKklJnfWorldWide.aspx?id=syndigate',
        'Premium' => 'https://www.jpost.com/Rss/RssFeedsPremium.aspx?id=syndigate',
        'Culture' => 'https://www.jpost.com/Rss/RssFeedsCulture.aspx?id=syndigate',
        'New Tech' => 'https://www.jpost.com/Rss/RssFeedsNewTech.aspx?id=syndigate',
        'Sports' => 'https://www.jpost.com/Rss/RssFeedsSports.aspx?id=syndigate',
        'Politics and Diplomacy' => 'https://www.jpost.com/Rss/RssFeedsPoliticsDiplomacy.aspx?id=syndigate',
        'Not Just News' => 'https://www.jpost.com/Rss/RssFeedsNotJustNews.aspx?id=syndigate',
        'Diaspora' => 'https://www.jpost.com/Rss/RssFeedsDiaspora.aspx?id=syndigate',
        'Middle East' => 'https://www.jpost.com/Rss/RssFeedsMiddleEastNews.aspx?id=syndigate',
        'Arab Israeli Conflict' => 'https://www.jpost.com/Rss/RssFeedsArabIsraeliConflict.aspx?id=syndigate',
        'Video' => 'https://www.jpost.com/Rss/RssFeedsVideo.aspx?id=syndigate',
        'Opinion' => 'https://www.jpost.com/Rss/RssFeedsOpinion.aspx?id=syndigate',
        'Israel News' => 'https://www.jpost.com/Rss/RssFeedsIsraelNews.aspx?id=syndigate',
        'Breaking News' => 'https://www.jpost.com/Rss/RssFeedsHeadlines.aspx?id=syndigate',
        'Home' => 'https://www.jpost.com/Rss/RssFeedsFrontPage.aspx?id=syndigate',
    );

    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $this->setOpt();
      //grab URL and pass it to the browser
      if (!curl_exec($this->ch)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not Perform the cURL session of ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      } else {
        $this->errorMsg = 'Perform the cURL session';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));

        curl_close($this->ch);
        fclose($this->fp);
      }
    }
  }

}
