<?php

class syndGatherContentLocalC1655 extends syndGatherContentLocal {

  public $soapUrl = 'services.countrywatch.com';
  public $token = "CWPARTNER7GK0MSH5FUEP3X";
  public $countryISO = array('Afghanistan'=>'AFG', 'Albania'=>'ALB','Algeria'=>'DZA','Andorra'=>'AND','Angola'=>'AGO','Antigua'=>'ATG','Argentina'=>'ARG','Armenia'=>'ARM','Australia'=>'AUS','Austria'=>'AUT','Azerbaijan'=>'AZE','Bahamas'=>'BHS','Bahrain'=>'BHR','Bangladesh'=>'BGD','Barbados'=>'BDS','Belarus'=>'BLR'
      ,'Belgium'=>'BEL','Belize'=>'BLZ','Benin'=>'BEN','Bhutan'=>'BTN','Bolivia'=>'BOL','Bosnia-Herzegovina'=>'BIH','Botswana'=>'BWA'
      ,'Brazil'=>'BRA','Brunei'=>'BRN','Bulgaria'=>'BGR','Burkina-Faso'=>'BFA','Burma'=>'MMR','Burundi'=>'BDI','Cambodia'=>'KHM'
      ,'Cameroon'=>'CMR','Canada'=>'CAN','Cape-Verde'=>'CPV','Central-African-Republic'=>'CAF','Chad'=>'TCD','Chile'=>'CHL','China'=>'CHN'
      ,'China-Hong-Kong'=>'HKG','China-Taiwan'=>'TWN','Colombia'=>'COL','Comoros'=>'COM','Congo-DRC'=>'COD','Congo-RC'=>'COG','Costa-Rica'=>'CRI'
      ,'Cote-d-Ivoire'=>'CIV','Croatia'=>'HRV','Cuba'=>'CUB','Cyprus'=>'CYP','Czech-Republic'=>'CZE','Denmark'=>'DNK','Djibouti'=>'DJI'
      ,'Dominica'=>'DMA','Dominican-Republic'=>'DOM','East-Timor'=>'TLS','Ecuador'=>'ECU','Egypt'=>'EGY','El-Salvador'=>'SLV','Equatorial-Guinea'=>'GNQ'
      ,'Eritrea'=>'ERI','Estonia'=>'EST','Ethiopia'=>'ETH','Fiji'=>'FJI','Finland'=>'FIN','Former-Yugoslav-Rep-of-Macedonia'=>'MKD','France'=>'FRA'
      ,'Gabon'=>'GAB','Gambia'=>'GMB','Georgia'=>'GEO','Germany'=>'DEU','Ghana'=>'GHA','Greece'=>'GRC','Grenada'=>'GRD','Guatemala'=>'GTM','Guinea'=>'GIN'
      ,'Guinea-Bissau'=>'GNB','Guyana'=>'GUY','Haiti'=>'HTI','Holy-See'=>'VAT','Honduras'=>'HND','Hungary'=>'HUN','Iceland'=>'ISL','India'=>'IND'
      ,'Indonesia'=>'IDN','Iran'=>'IRN','Iraq'=>'IRQ','Ireland'=>'IRL','Israel'=>'ISR','Italy'=>'ITA','Jamaica'=>'JAM','Japan'=>'JPN','Jordan'=>'JOR'
      ,'Kazakhstan'=>'KAZ','Kenya'=>'KEN','Kiribati'=>'KIR','Korea-North'=>'PRK','Korea-South'=>'KOR','Kuwait'=>'KWT','Kyrgyzstan'=>'KGZ','Laos'=>'LAO','Latvia'=>'LVA'
      ,'Lebanon'=>'LBN','Lesotho'=>'LSO','Liberia'=>'LBR','Libya'=>'LBY','Liechtenstein'=>'LIE','Lithuania'=>'LTU','Luxembourg'=>'LUX','Madagascar'=>'MDG'
      ,'Malawi'=>'MWI','Malaysia'=>'MYS','Maldives'=>'MDV','Mali'=>'MLI','Malta'=>'MLT','Malta'=>'MLT','Marshall-Islands'=>'MHL','Mauritania'=>'MRT','Mauritius'=>'MUS'
      ,'Mexico'=>'MEX','Micronesia'=>'FSM','Moldova'=>'MDA','Monaco'=>'MCO','Mongolia'=>'MNG','Morocco'=>'MAR','Mozambique'=>'MOZ','Namibia'=>'NAM','Nauru'=>'NRU'
      ,'Nepal'=>'NPL','Netherlands'=>'NLD','New-Zealand'=>'NZL','Nicaragua'=>'NIC','Niger'=>'NER','Nigeria'=>'NGA','Norway'=>'NOR','Oman'=>'OMN'
      ,'Pakistan'=>'PAK','Palau'=>'PLW','Panama'=>'PAN','Papua-New-Guinea'=>'PNG','Paraguay'=>'PRY','Peru'=>'PER','Philippines'=>'PHL','Poland'=>'POL'
      ,'Portugal'=>'PRT','Qatar'=>'QAT','Romania'=>'ROU','Russia'=>'RUS','Rwanda'=>'RWA','Saint-Kitts-and-Nevis'=>'KNA','Saint-Lucia'=>'LCA'
      ,'Saint-Vincent-and-Grenadines'=>'VCT','Samoa'=>'WSM','San-Marino'=>'SMR','Sao-Tome-and-Principe'=>'STP','Saudi-Arabia'=>'SAU','Senegal'=>'SEN'
      ,'Serbia'=>'SRB','Seychelles'=>'SYC','Sierra-Leone'=>'SLE','Singapore'=>'SGP','Slovakia'=>'SVK','Slovenia'=>'SVN','Solomon-Islands'=>'SLB'
      ,'Somalia'=>'SOM','South-Africa'=>'ZAF','Spain'=>'ESP','Sri-Lanka'=>'LKA','Sudan'=>'SDN','Suriname'=>'SUR','Swaziland'=>'SWZ','Sweden'=>'SWE'
      ,'Switzerland'=>'CHE','Syria'=>'SYR','Tajikistan'=>'TJK','Tanzania'=>'TZA','Thailand'=>'THA','Togo'=>'TGO','Tonga'=>'TON','Trinidad-Tobago'=>'TTO'
      ,'Tunisia'=>'TUN','Turkey'=>'TUR','Turkmenistan'=>'TKM','Tuvalu'=>'TUV','Uganda'=>'UGA','Ukraine'=>'UKR','United-Arab-Emirates'=>'ARE'
      ,'United-Kingdom'=>'GBR','United-States'=>'USA','Uruguay'=>'URY','Uzbekistan'=>'UZB','Vanuatu'=>'VUT','Venezuela'=>'VEN','Vietnam'=>'VNM','Yemen'=>'YEM','Zambia'=>'ZMB','Zimbabwe'=>'ZWE');
  public function __construct($titleId, $reportId = '') {
    $model = new syndModel();
		$homeDirPath = $model -> getHomeDir($titleId);
    foreach ($this->countryISO as $iso){
      $Countryprofile = $this->getCountryReviewSection($iso, 'PCPRF');
      $first_message = 'A transport-level error has occurred when receiving results from the server. (provider: TCP Provider, error: 0 - An existing connection was forcibly closed by the remote host.)';
      $second_message = 'The service has encountered an error processing your request. Please try again. Error code 40143.
A severe error occurred on the current command.  The results, if any, should be discarded.';
        $third_message = 'Connection Timeout Expired.  The timeout period elapsed during the post-login phase.  The connection could have timed out while waiting for server to complete the login process and respond; Or it could have timed out while attempting to create multiple active connections.  The duration spent while attempting to connect to this server was - [Pre-Login] initialization=1; handshake=10; [Login] initialization=0; authentication=0; [Post-Login] complete=14002;';
        if ((stripos($Countryprofile, $first_message) !== false) || (stripos($Countryprofile, $second_message) !== false) || (stripos($Countryprofile, $third_message) !== false))
            continue;
        //var_dump($Countryprofile);exit;
        $this->saveNewsFile($Countryprofile, $iso, $homeDirPath);
    }
      $this->MoveFromRootToDateDir($titleId);
      parent::__construct($titleId, $reportId);
  }


    public function getCountryReviewSection($countryISO, $sectionID)
    {
        $cURLConnection = curl_init();
        curl_setopt($cURLConnection, CURLOPT_URL, 'https://services.countrywatch.com/ws/cwwebservice.asmx/LoginToken?Token=CWPARTNER7GK0MSH5FUEP3X');
        $options = array(
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => true, // do not return headers
            CURLOPT_FOLLOWLOCATION => true, // follow redirects
            CURLOPT_USERAGENT => "spider", // who am i
            CURLOPT_AUTOREFERER => true, // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
            CURLOPT_TIMEOUT => 120, // timeout on response
            CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
        );
        curl_setopt($cURLConnection, CURLOPT_COOKIEJAR, '/tmp/getCountryReviewSection1655.txt');
        curl_setopt($cURLConnection, CURLOPT_COOKIEFILE, '/tmp/getCountryReviewSection1655.txt');
        curl_setopt_array($cURLConnection, $options);
        $response = curl_exec($cURLConnection);
        curl_close($cURLConnection);
        // var_dump($response);


        $cURLConnection = curl_init();
        curl_setopt($cURLConnection, CURLOPT_URL, "https://services.countrywatch.com/ws/cwwebservice.asmx/getCountryReviewSection?Token=CWPARTNER7GK0MSH5FUEP3X&CountryISO=$countryISO&SectionID=$sectionID");
        $options = array(
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => false, // do not return headers
            CURLOPT_FOLLOWLOCATION => true, // follow redirects
            CURLOPT_USERAGENT => "spider", // who am i
            CURLOPT_AUTOREFERER => true, // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
            CURLOPT_TIMEOUT => 120, // timeout on response
            CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
        );
        curl_setopt($cURLConnection, CURLOPT_COOKIEJAR, '/tmp/getCountryReviewSection1655.txt');
        curl_setopt($cURLConnection, CURLOPT_COOKIEFILE, '/tmp/getCountryReviewSection1655.txt');
        curl_setopt_array($cURLConnection, $options);
        $response = curl_exec($cURLConnection);
        curl_close($cURLConnection);
        return $response;
    }


    public function saveNewsFile($str, $ISO, $homeDirPath)
    {
        $file_name = $ISO . ".xml";
        $file_name = $homeDirPath . DIRECTORY_SEPARATOR . $file_name;
        $fp = fopen($file_name, 'w');
        fwrite($fp, $str);
        fclose($fp);
    }

}
