<?php


class syndGatherContentHttpC10978 extends syndGatherContentHttp
{
public $article_id = 0;
    public function copyContent()
    {
        $articleId = $this->getArticleId();
        $this->article_id = $articleId;
        for ($i = 1; $i < 30; $i++) {
            $cat = "Opoint Technology feed $i";
            $subUrl = "https://feed.opoint.com/safefeed.php?key=N9JvOuPB4VE8qqMlaS2Hposr&lastid=$articleId&num_art=5000&dry_run=true";
            $this->checkContent($subUrl, $cat);
           // var_dump($subUrl);
            $options = array(
                CURLOPT_RETURNTRANSFER => true, // return web page
                CURLOPT_HEADER => false, // do not return headers
                CURLOPT_FOLLOWLOCATION => true, // follow redirects
                CURLOPT_USERAGENT => "spider", // who am i
                CURLOPT_AUTOREFERER => true, // set referer on redirect
                CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
                CURLOPT_TIMEOUT => 120, // timeout on response
                CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
                // CURLOPT_USERPWD => "$username:$password",
                CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
                CURLOPT_FRESH_CONNECT => TRUE
            );
            $ch = curl_init($subUrl);
            curl_setopt_array($ch, $options);
            $response = curl_exec($ch);
            if (preg_match("/<opoint_search_start>(.*?)<\/opoint_search_start>/", $response, $articleId)) {
                echo "from response Id" . PHP_EOL;
                $articleId=(int) $articleId[1];
                if($this->article_id==$articleId){
                    continue;
                }else{
                    $this->article_id=$articleId;
                    $this->updateOpointId( $this->article_id);
                }
            } else {
                continue;
            }
            curl_close($ch);
            $fileName = str_replace(' ', '_', $cat . '.xml');
            file_put_contents($fileName, $response);
            sleep(10);
        }
    }

    private function getArticleId()
    {
        $q = mysql_query("SELECT variable_value FROM variable WHERE variable_name='opoint_id_10978'");
        $row = mysql_fetch_row($q);

        return $row[0];
    }

    public function updateOpointId($id)
    {
        $q = mysql_query("UPDATE variable SET variable_value=$id WHERE variable_name='opoint_id_10978'");
        if ($q)
            return true;
    }

    protected function getRssUrl()
    {
        $urls = array(
            "Opoint Technology feed" => "http://api-docs.opoint.com/references/firehose",
        );
        return $urls;
    }

    public function checkContent($subUrl = '', $cat = '', $check = false)
    {
        $this->check = $check;
        if ($check) {
            if (!$this->ch = curl_init($subUrl)) {
                $this->processState = ezcLog::FAILED_AUDIT;
                $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
                $this->reportParams['LatestStatus'] = 0;
                $this->report->updateReport($this->reportParams);
                $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
                return false;
            } else {
                $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
                $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
                return true;
            }
        }
        $fileName = str_replace(' ', '_', $cat . '.xml');
        if (!$this->fp = fopen($fileName, "w")) {
            //Log the proccess for debugging
            $this->processState = ezcLog::FAILED_AUDIT;
            $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
            $this->reportParams['LatestStatus'] = 0;
            $this->report->updateReport($this->reportParams);
            $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
            return false;
        } else {
            $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
            $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
            if (file_exists('.xml')) {
                unlink('.xml');
            }
        }
        if (!$this->ch = curl_init($subUrl)) {
            $this->processState = ezcLog::FAILED_AUDIT;
            $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
            $this->reportParams['LatestStatus'] = 0;
            $this->report->updateReport($this->reportParams);
            $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
            return false;
        } else {
            $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
            $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        }
    }

}