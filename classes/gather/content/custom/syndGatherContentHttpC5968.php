<?php

class syndGatherContentHttpC5968 extends syndGatherContentHttp {

  public $articles = array();
  public $startAt = 0;

  protected function getRssUrl() {
    $page = 1;
    $urls = array();
    for ($i = 1; $i < 3; $i++) {
      $urls["AFP $i"] = "https://api.afp.com/v1/api/search?wt=xml";
      $page++;
    }
    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);

      $contents = $this->getContant($subUrl);
      $fileName = $this->homeDir . str_replace(' ', '_', $cat . '.xml');
      $data = array();
      foreach ($contents as &$item) {
        $jsondata = array();
        foreach ($item as $k => $i) {
          $remove = array("title", "headline", "slug", "news", "summary", "href", "language");
          if (!in_array($k, $remove)) {
            $jsondata[$k] = $i;
          }
        }
        $item["jsondata"] = json_encode($jsondata);
        $data[]["fullArticle"] = $item;
      }

      // creating object of SimpleXMLElement
      $xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');
      // function call to convert array to xml
      $this->array_to_xml($data, $xml_data);
      //saving generated xml file; 
      $result = $xml_data->asXML($fileName);
    }
  }

  public function getContant($subUrl) {
    $accessToken = $this->getAccessToken();
    $jsonRequest = '{
   "dateRange":{
      "from":"now-1M","to":"now"
   },
   "dateGap":"day",
   "tz":"GMT",
   "sortOrder":"published desc",
   "maxRows":"1000",
   "startAt":"' . $this->startAt . '",
   "query":{}
}';
    // var_dump($jsonRequest);
    // Curl initiating
    $curl = curl_init($subUrl);
    // Curl connection settings
    curl_setopt_array($curl, array(
        CURLOPT_URL => "$subUrl",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_FRESH_CONNECT => true,
        CURLOPT_FORBID_REUSE => true,
        CURLOPT_POSTFIELDS => "$jsonRequest",
        CURLOPT_HTTPHEADER => array(// Adding accessToken to the HTTP headersettings
            "Accept: application/json",
            "Authorization: Bearer " . $accessToken,
            "Cache-Control: no-cache",
            "Content-Type: application/json")
            )
    );
    // Execution of the curl connection, reading and variable setting of the content of the response file in a string
    $searchDatas = curl_exec($curl);
    // Closing the curl instance
    curl_close($curl);
    // Decoding of the complete JSON string and variable setting
    $searchResponse = json_decode($searchDatas, true);
    // Extraction and variableization of documents
    $docs = $searchResponse['response']['docs'];
    // Displaying documents
    //var_dump($docs);
    // Extraction and variable setting of the request status
    $status = $searchResponse['response']['status']['reason'];
    // Displaying the status of the request
    //var_dump($status);
    // Extraction et mise en variable du nomnbre de documents trouvés
    $numFound = $searchResponse['response']['numFound'];
    // Affichage du nombre de documents trouvés
    //var_dump($numFound);exit;
    if ($numFound > 0 && $status == "Success") {

      $this->startAt+=1000;
      return $docs;
    } else {
      return FALSE;
    }
  }

  public function getMetadata($subUrl) {
    $date = array();
    $options = array(
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // do not return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_USERAGENT => "spider", // who am i
        CURLOPT_AUTOREFERER => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
        CURLOPT_TIMEOUT => 120, // timeout on response
        CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
    );

    $ch = curl_init($subUrl);
    curl_setopt_array($ch, $options);
    $response = curl_exec($ch);
    curl_close($ch);
    $response = json_decode($response, true);
    $date["collection"] = $response;

    $subUrl = str_replace("collection.json", "metadata.json", $subUrl);
    $response = "";
    $options = array(
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // do not return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_USERAGENT => "spider", // who am i
        CURLOPT_AUTOREFERER => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
        CURLOPT_TIMEOUT => 120, // timeout on response
        CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
    );

    $ch = curl_init($subUrl);
    curl_setopt_array($ch, $options);
    $response = curl_exec($ch);
    curl_close($ch);
    $response = json_decode($response, true);
    $date["metadata"] = $response;
    return $date;
  }

  public function getAccessToken() {
    $tokenEndpoint = 'https://api.afp.com/oauth/token';
    $grantType = 'password';
    $client_id = 'syndicate';
    $client_secret = 'acf9e909-29a0-481f-9390-06630eacf357';
    $username = 'techsupport';
    $password = 'a99709579a72';
    // Base64 encoding and variable setting of the entire Client ID "ClientID:ClientSecret"
    $clientCredentials = base64_encode($client_id . ':' . $client_secret);
    // Variable setting of an array with parameters to be included in the HTTP header
    $authHeaderOpts = array('http' => array('method' => 'POST', 'header' => "Authorization: Basic " . $clientCredentials . "\r\n" . "Accept: application/json\r\n"));
    // Creation and variable setting of the flow context
    $authContext = stream_context_create($authHeaderOpts);
    // Building the Authentication URL with the user ID ("myName" and "myPassword") into HTTP parameters in the URL
    $authUrl = $tokenEndpoint . '?grant_type=' . $grantType . '&username=' . $username . '&password=' . $password;
    // Reading the content of the response file in a string
    $authDatas = file_get_contents($authUrl, false, $authContext);
    // Decoding of the complete JSON chain
    $authResponse = json_decode($authDatas, true);
    // Extraction and variable setting of the "accessToken"
    return $authResponse['access_token'];
  }

  public function array_to_xml($item, &$xml_data) {
    foreach ($item as $key => $value) {
      if (is_numeric($key)) {
        $key = 'item';
      }
      if (is_array($value)) {
        $subnode = $xml_data->addChild($key);
        $this->array_to_xml($value, $subnode);
      } else {
        $xml_data->addChild("$key", htmlspecialchars("$value"));
      }
    }
  }

}
