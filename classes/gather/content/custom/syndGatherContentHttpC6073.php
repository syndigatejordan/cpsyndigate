<?php

class syndGatherContentHttpC6073 extends syndGatherContentHttp {

  public $articles = array();

  protected function getRssUrl() {

    $urls["Lebanon 24 3"] = "https://www.lebanon24.com/admin-24/public/services/section?id=3";
    $urls["Lebanon 24 11"] = "https://www.lebanon24.com/admin-24/public/services/section?id=11";
    $urls["Lebanon 24 12"] = "https://www.lebanon24.com/admin-24/public/services/section?id=12";

    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $response = $this->getContant($subUrl);
      $response = preg_replace('/\s+/', ' ', $response);
      $contents = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $response), true);
      $fileName = $this->homeDir . str_replace(' ', '_', $cat . '.xml');
      $contents = $contents["response"]["section"]["articles"]["all"];
      $data = array();
      foreach ($contents as $item) {
        $article = array();
        $json = $this->fullArticle($item['id']);

        $json = preg_replace('/\s+/', ' ', $json);


        $id = null;
        preg_match('/"id": (.*?),/', $json, $id);
        $id = $id[1];
        $article["id"] = $id;

        $date = null;
        preg_match('/"date": (.*?),/', $json, $date);
        $date = $date[1];
        $article["date"] = $date;

        $title = null;
        preg_match('/"title": "(.*?)",/', $json, $title);
        $title = $title[1];
        $article["title"] = $title;

        $summary = null;
        preg_match('/"summary": "(.*?)",/', $json, $summary);
        $summary = $summary[1];
        $summary = str_replace('\"', '"', $summary);
        $article["summary"] = $summary;

        $main_image = null;
        preg_match('/"main_image": "(.*?)",/', $json, $main_image);
        $main_image = $main_image[1];
        $article["main_image"] = $main_image;

        $web_url = null;
        preg_match('/"web_url": "(.*?)",/', $json, $web_url);
        $web_url = $web_url[1];
        $article["web_url"] = $web_url;

        $text = null;
        preg_match('/"text": "(.*?)",/', $json, $text);
        $text = $text[1];
        $text = str_replace('\"', '"', $text);
        $article["text"] = $text;

        $author = null;
        preg_match('/ "author": \[ "(.*?)" \]/', $json, $author);
        $author = $author[1];
        $author = str_replace('\"', '"', $author);
        $article["author"] = $author;

      
        $data[]["fullArticle"] = $article;
      }

      // creating object of SimpleXMLElement
      $xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');
      // function call to convert array to xml
      $this->array_to_xml($data, $xml_data);
      //saving generated xml file; 
      $result = $xml_data->asXML($fileName);
    }
  }

  public function getContant($subUrl) {
    /*   return '
      {
      "meta": {
      "success": true,
      "message_title": "",
      "message": "",
      "show_message": false
      },
      "response": {
      "section": {
      "id": "12",
      "date": 1508838039,
      "title": "رادار لبنان24",
      "articles": {
      "all": [
      {
      "id": "709381",
      "date": 1591089590,
      "views": 11090,
      "title": "هذه غرامة من لا يفتح من محلات \"المولات\"!",
      "summary": "",
      "main_image": "https://www.lebanon24.com/uploadImages/DocumentImages/Doc-T-709381-637266971925921303.jpg",
      "web_url": "",
      "has_details": 1,
      "text": ""
      },
      {
      "id": "709376",
      "date": 1591087779,
      "views": 1112,
      "title": "قانون قيصر لاعب رئاسي في لبنان",
      "summary": "",
      "main_image": "https://www.lebanon24.com/uploadImages/DocumentImages/Doc-T-709376-637266955395748837.jpg",
      "web_url": "",
      "has_details": 1,
      "text": ""
      },
      {
      "id": "709095",
      "date": 1591016661,
      "views": 13161,
      "title": "فنانة لبنانية شهيرة لم تدفع مرتبات العاملين لديها منذ 5 أشهر!",
      "summary": "",
      "main_image": "https://www.lebanon24.com/uploadImages/DocumentImages/Doc-T-709095-637266246139420910.jpg",
      "web_url": "",
      "has_details": 1,
      "text": ""
      },
      {
      "id": "709051",
      "date": 1591006800,
      "views": 5355,
      "title": "نواب ووزراء بين الحزب و\"التيار\"",
      "summary": "",
      "main_image": "https://www.lebanon24.com/uploadImages/DocumentImages/Doc-T-709051-637266146116588412.jpg",
      "web_url": "",
      "has_details": 1,
      "text": ""
      },
      {
      "id": "709036",
      "date": 1591002900,
      "views": 4143,
      "title": "تسوية سلعاتا.. ضمن تسوية شاملة",
      "summary": "",
      "main_image": "https://www.lebanon24.com/uploadImages/DocumentImages/Doc-T-709036-637266119043973858.jpg",
      "web_url": "",
      "has_details": 1,
      "text": ""
      },
      {
      "id": "709029",
      "date": 1591001633,
      "views": 7979,
      "title": "\"التيار الوطني\" إلى الشارع",
      "summary": "",
      "main_image": "https://www.lebanon24.com/uploadImages/DocumentImages/Doc-T-709029-637266093292350545.jpg",
      "web_url": "",
      "has_details": 1,
      "text": ""
      },
      {
      "id": "708546",
      "date": 1590844680,
      "views": 40886,
      "title": "خطة سلامة.. وتثبيت سعر الدولار",
      "summary": "",
      "main_image": "https://www.lebanon24.com/uploadImages/DocumentImages/Doc-T-708546-637264523173837379.jpg",
      "web_url": "",
      "has_details": 1,
      "text": ""
      },
      {
      "id": "708495",
      "date": 1590831781,
      "views": 12116,
      "title": "\"حزب الله\" يستعد للاسوأ",
      "summary": "",
      "main_image": "https://www.lebanon24.com/uploadImages/DocumentImages/Doc-T-708495-637264395314664493.jpg",
      "web_url": "",
      "has_details": 1,
      "text": ""
      },
      {
      "id": "708168",
      "date": 1590752349,
      "views": 6191,
      "title": "عون يطلب من زواره اعادة فتح العلاقة مع باسيل",
      "summary": "",
      "main_image": "https://www.lebanon24.com/uploadImages/DocumentImages/Doc-T-708168-637263601366380017.jpg",
      "web_url": "",
      "has_details": 1,
      "text": ""
      },
      {
      "id": "708154",
      "date": 1590749340,
      "views": 4764,
      "title": "هذا ما يريده ارسلان من بري في ذكرى قبرشمون",
      "summary": "",
      "main_image": "https://www.lebanon24.com/uploadImages/DocumentImages/Doc-T-708154-637263570863331770.jpg",
      "web_url": "",
      "has_details": 1,
      "text": ""
      }
      ],
      "sticky_key": null
      },
      "has_more": true
      }
      }
      }'; */
    // sleep(1);
    $options = array(
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // do not return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_USERAGENT => "spider", // who am i
        CURLOPT_AUTOREFERER => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
        CURLOPT_TIMEOUT => 120, // timeout on response
        CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
        // CURLOPT_USERPWD => "$username:$password",
        CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );

    $ch = curl_init($subUrl);
    curl_setopt_array($ch, $options);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }

  public function fullArticle($id) {
    $subUrl = "https://www.lebanon24.com/admin-24/public/services/article?id=$id";
    $options = array(
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // do not return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_USERAGENT => "spider", // who am i
        CURLOPT_AUTOREFERER => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
        CURLOPT_TIMEOUT => 120, // timeout on response
        CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
        // CURLOPT_USERPWD => "$username:$password",
        CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );

    $ch = curl_init($subUrl);
    curl_setopt_array($ch, $options);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }

  public function array_to_xml($item, &$xml_data) {
    foreach ($item as $key => $value) {
      if (is_numeric($key)) {
        $key = 'item';
      }
      if (is_array($value)) {
        $subnode = $xml_data->addChild($key);
        $this->array_to_xml($value, $subnode);
      } else {
        $xml_data->addChild("$key", htmlspecialchars("$value"));
      }
    }
  }

}
