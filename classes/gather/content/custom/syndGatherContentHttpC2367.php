<?php

class syndGatherContentHttpC2367 extends syndGatherContentHttp {

  protected $urls = array(
      'Salone News' => 'http://www.thepatrioticvanguard.com/spip.php?page=backend&id_rubrique=2',
      'African News' => 'http://www.thepatrioticvanguard.com/spip.php?page=backend&id_rubrique=3',
      'Africa Canada' => 'http://www.thepatrioticvanguard.com/spip.php?page=backend&id_rubrique=4',
      'Sports' => 'http://www.thepatrioticvanguard.com/spip.php?page=backend&id_rubrique=5',
      'Opinion' => 'http://www.thepatrioticvanguard.com/spip.php?page=backend&id_rubrique=6',
      'Letter to editor' => 'http://www.thepatrioticvanguard.com/spip.php?page=backend&id_rubrique=7',
      'Analysis' => 'http://www.thepatrioticvanguard.com/spip.php?page=backend&id_rubrique=8',
      'Literary Zone' => 'http://www.thepatrioticvanguard.com/spip.php?page=backend&id_rubrique=9',
      'Media' => 'http://www.thepatrioticvanguard.com/spip.php?page=backend&id_rubrique=13',
      'From the Editors Keyboard' => 'http://www.thepatrioticvanguard.com/spip.php?page=backend&id_rubrique=16',
      'World News' => 'http://www.thepatrioticvanguard.com/spip.php?page=backend&id_rubrique=21',
      'Business News' => 'http://www.thepatrioticvanguard.com/spip.php?page=backend&id_rubrique=22',
  );

  public function setOpt() {
    parent::setOpt();
    curl_setopt($this->ch, CURLOPT_ENCODING, 'text');
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }

    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->urls as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $this->setOpt();
      //grab URL and pass it to the browser
      if (!curl_exec($this->ch)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not Perform the cURL session of ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      } else {
        $this->errorMsg = 'Perform the cURL session';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));

        curl_close($this->ch);
        fclose($this->fp);
      }
    }
  }

}

?>
