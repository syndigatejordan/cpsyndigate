<?php

class syndGatherContentHttpC1222 extends syndGatherContentHttp {

  protected function getRssUrl() {
    $urls = array(

        'alborsanews ' => 'https://alborsaanews.com/category/%d8%a7%d9%82%d8%aa%d8%b5%d8%a7%d8%af-%d9%85%d8%b5%d8%b1/feed',
        'alborsanews 1' => 'https://alborsaanews.com/category/%d8%a8%d9%86%d9%88%d9%83/feed',
        'alborsanews 2' => 'https://alborsaanews.com/category/%d8%a7%d8%b3%d8%aa%d8%ab%d9%85%d8%a7%d8%b1-%d9%88%d8%a7%d8%b9%d9%85%d8%a7%d9%84/feed',
        'alborsanews 3' => 'https://alborsaanews.com/category/%d8%b9%d9%82%d8%a7%d8%b1%d8%a7%d8%aa/feed',
        'alborsanews 4' => 'https://alborsaanews.com/category/%d8%b7%d8%a7%d9%82%d8%a9/feed',
        'alborsanews 5' => 'https://alborsaanews.com/category/%d8%a5%d9%82%d8%aa%d8%b5%d8%a7%d8%af-%d9%88%d8%a3%d8%b3%d9%88%d8%a7%d9%82/feed',
        'alborsanews 6' => 'https://alborsaanews.com/category/%d8%a8%d9%88%d8%b1%d8%b5%d8%a9-%d9%88-%d8%b4%d8%b1%d9%83%d8%a7%d8%aa/feed',
        'alborsanews 7' => 'https://alborsaanews.com/category/%d8%aa%d8%a3%d9%85%d9%8a%d9%86/feed',
        'alborsanews 8' => 'https://alborsaanews.com/category/%d9%86%d9%82%d9%84-%d9%88%d9%85%d9%84%d8%a7%d8%ad%d8%a9/feed',
        'alborsanews 9' => 'https://alborsaanews.com/category/%d8%b3%d9%8a%d8%a7%d8%ad%d8%a9-%d9%88%d8%b3%d9%81%d8%b1/feed',
        'alborsanews 10' => 'https://alborsaanews.com/category/%d8%a7%d8%aa%d8%b5%d8%a7%d9%84%d8%a7%d8%aa-%d9%88%d8%aa%d9%83%d9%86%d9%88%d9%84%d9%88%d8%ac%d9%8a%d8%a7/feed',
        'alborsanews 11' => 'https://alborsaanews.com/category/%d8%b3%d9%8a%d8%a7%d8%b1%d8%a7%d8%aa/feed',
        'alborsanews 12' => 'https://alborsaanews.com/category/%d8%b5%d8%ad%d8%a9-%d9%88%d8%af%d9%88%d8%a7%d8%a1/feed',
        'alborsanews 13' => 'https://alborsaanews.com/category/%d9%85%d8%b3%d8%a4%d9%88%d9%84%d9%8a%d8%a9-%d9%85%d8%ac%d8%aa%d9%85%d8%b9%d9%8a%d8%a9/feed',
        'alborsanews 14' => 'https://alborsaanews.com/category/%d8%b9%d9%85%d9%84%d8%a7%d8%aa-%d9%88%d8%b3%d9%84%d8%b9/feed',
        'alborsanews 15' => 'https://alborsaanews.com/category/%d8%a7%d9%82%d8%aa%d8%b5%d8%a7%d8%af-%d8%a7%d9%84%d8%ae%d9%84%d9%8a%d8%ac/feed',
        'alborsanews 16' => 'https://alborsaanews.com/category/%d9%85%d9%82%d8%a7%d9%84%d8%a7%d8%aa-%d8%a7%d9%84%d8%a8%d9%88%d8%b1%d8%b5%d8%a9/feed',
        'alborsanews 17' => 'https://alborsaanews.com/category/%d8%a7%d9%82%d8%aa%d8%b5%d8%a7%d8%af-%d8%b9%d8%a7%d9%84%d9%85%d9%8a/feed',
        'alborsanews 18' => 'https://alborsanews.com/report/feed/',
        'alborsanews 19' => 'https://alborsanews.com/feed/',
    );

    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
          // CURLOPT_USERPWD => "$username:$password",
          CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
      );

      $ch = curl_init($subUrl);
      curl_setopt_array($ch, $options);
      $response = curl_exec($ch);
      curl_close($ch);
      $fileName = str_replace(' ', '_', $cat . '.xml');
      file_put_contents($fileName, $response);
    }
  }

}
