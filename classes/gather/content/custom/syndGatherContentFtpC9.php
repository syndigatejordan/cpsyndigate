<?php
require_once 'Net/FTP.php';
class syndGatherContentFtpC9 extends syndGatherContentFtp
{


    private $daysSpan = 3;

    private $checkDirs = array();	
    
    /**
     * Constructor for the FTP class
     *
     * @param int $titleId
     */
    public function __construct ($titleId, $reportId)
    {

	//get the list of directories to try and check
	for ($i = 0; $i < $this->daysSpan; $i++) {
		$this->checkDirs[] = date('Y/m/d/', strtotime("-{$i} days"));
	}
	

	parent::__construct($titleId, $reportId);


    }

    /**
     * Get the Newest Directories to copy
     *
     * @return array
     */
    public function checkContent()
    {
        $dirs = array();
        $folders = $this->ls('.');
		$pwd = $this->ftp->pwd();

        if ($folders) {
		//check if the dirs we want to copy exist
		foreach ($this->checkDirs as $checkDir) {
			$cdRes = $this->ftp->cd($pwd . '/' . $checkDir);
			
			if (!PEAR::isError($cdRes)) {
				
				//renaming all files to numerica names
				$ls = $this->ftp->ls($pwd . '/'. $checkDir, NET_FTP_FILES_ONLY);
				foreach($ls as $file) {
                	$filename = $file['name'];
                	$this->ftp->rename($pwd .'/'. $checkDir .'/'. $filename, $pwd .'/'. $checkDir .'/'. $filename . '.tmp');	
            	}

            	$ls = $this->ftp->ls($pwd . '/'. $checkDir, NET_FTP_FILES_ONLY);
            	for($count=0; $count<count($ls); $count++) { 
                	$filename = $ls[$count]['name'];
                	$this->ftp->rename($pwd .'/'. $checkDir .'/'. $filename, $pwd .'/'. $checkDir .'/'. $count.".txt");
            	}
            
				$dirs[] = $checkDir;
			} else {
				echo $cdRes->getMessage();	
			}

			//go back to root
			$this->ftp->cd($pwd);
		}

            $this->log->log('Getting new directories for ' . $this->ftpServer . ' Succeeded', ezcLog::DEBUG, $this->logParams);
            return $dirs;
        } else {
            $this->reportParams['LatestStatus'] = 0;
            $this->reportParams['Error'] = 'Getting new directories for ' . $this->ftpServer . ' Failed';
            $this->report->updateReport($this->reportParams);
            $this->log->log('Getting new directories for ' . $this->ftpServer . ' Failed', ezcLog::FATAL, $this->logParams);
            return false;
        }
    }
    
    /**
     * Copy the new content to the home directory of the title
     *
     */
    public function copyContent ()
    {
    	$checkContentDirs = $this->checkContent();

	  	if ($checkContentDirs) {
            foreach ($checkContentDirs as $folder) {
                $this->log->log('Started copying Directory ' . $folder . ' from  ' . $this->ftpServer . ' title.', ezcLog::DEBUG, $this->logParams);
		
                $download = $this->ftp->getRecursive($this->ftp->pwd() . '/' . $folder, $this->homeDir . $folder, true);
			
                if (PEAR::isError($download)) {
                    $this->reportParams['LatestStatus'] = 0;
                    $this->reportParams['Error'] = $download->getMessage() . $folder . ' from ' . $this->ftpServer . ' title. Failed.';
                    $this->report->updateReport($this->reportParams);
                    $this->log->log($download->getMessage() . $folder . ' from ' . $this->ftpServer . ' title. Failed.', ezcLog::FATAL, $this->logParams);
                    return false;
                } else {
                    $this->log->log('Copying Directory ' . $folder . ' from ' . $this->ftpServer . ' title. Succeeded.', ezcLog::DEBUG, $this->logParams);
                    //copy the new content to the working dirctory
                    $copyHomeToWork = new syndGatherContentLocal($this->titleId, $this->reportId);
                    $copyHomeToWork->copyContent();
                }
            }
        } else {
            $this->log->log('No content found from ' . $this->ftpServer . ' title.', ezcLog::WARNING, $this->logParams);
            return false;
        }
    }
    /**
     * Distroy the object after the proccess finished
     *
     */
    public function __destruct ()
    {
        $this->log->log('Closing FTP Connection for ' . $this->ftpServer, ezcLog::DEBUG, $this->logParams);
        $this->ftp->disconnect();
    }
}
