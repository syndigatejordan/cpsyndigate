<?php
require_once 'Net/FTP.php';
class syndGatherContentFtpC1584 extends syndGatherContentFtp {

	private $daysSpan = 3;

	private $checkDirs = array();

	/**
	 * Constructor for the FTP class
	 *
	 * @param int $titleId
	 */
	public function __construct($titleId, $reportId) {
			
		//get the list of directories to try and check
		for ($i = 0; $i < $this -> daysSpan; $i++) {
			$this -> checkDirs[] = date('Y/m/d/', strtotime("-{$i} days"));
		}
		parent::__construct($titleId, $reportId);
		$this -> GetFilesFromFTP();
		$copyHomeToWork = new syndGatherContentLocal($this -> titleId, $this -> reportId);
		$copyHomeToWork -> copyContent();
	}

	/**
	 * Get the Newest Directories to copy
	 *
	 * @return array
	 */
	public function GetFilesFromFTP() {
		// Getting all files
		$files = $this -> ftp -> ls($pwd . '/' . $checkDir, NET_FTP_FILES_ONLY);
		$date_dir = $this -> getDateDirCustom(date('Ymd', time()));
		$c = new Criteria();
		$c -> add(TitlePeer::ID, $this -> titleId);
		$currentTitle = TitlePeer::doSelectOne($c);
		$HomeDir = $currentTitle -> getHomeDir();
		$title_dir = $HomeDir . $date_dir . DIRECTORY_SEPARATOR;
		if (!is_dir($title_dir)) {
			mkdir($title_dir, 0777, true);
		}
		foreach ($files as $file) {
			$new_file_new = str_replace(' ', '_', $file['name']);
			// Copy the file from FTP server to Title home DIR
			if ($this -> ftp -> get($file['name'], $title_dir . $new_file_new, TRUE)) {
				echo "Copy file Successfully....." . chr(10);
				// move the file from FTP server root to backup DIR
				$this -> ftp -> rename($file['name'], 'backup/' . $file['name']);
			} else {
				echo "Fail copy files.....";
			}
		}
		$this -> ftp -> disconnect();
		return true;
	}

	/**
	 * Distroy the object after the proccess finished
	 *
	 */
	private function getDateDirCustom($dateDir) {
		$year = substr($dateDir, 0, 4);
		$month = substr($dateDir, 4, 2);
		$day = substr($dateDir, 6, 2);
		$directory = $destination . $year . DIRECTORY_SEPARATOR . $month . DIRECTORY_SEPARATOR . $day;
		return $directory;
	}

}
