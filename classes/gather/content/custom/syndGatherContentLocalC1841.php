<?php

class syndGatherContentLocalC1841 extends syndGatherContentLocal {

  public function __construct($titleId, $reportId = '') {

    $c = new Criteria();
    $c->add(TitlePeer::ID, $titleId);
    $currentTitle = TitlePeer::doSelectOne($c);
    $currentDirName = $currentTitle->getHomeDir();

    $directories = scandir($currentDirName);
    foreach ($directories as $TGZFile) {
      if (($TGZFile != '.') && ($TGZFile != '..') && (is_file("{$currentDirName}{$TGZFile}"))) {
        $name = pathinfo($TGZFile);
        $command = 'unzip ' . $currentDirName . $TGZFile . " -d " . $currentDirName . $name["filename"];
        $remove_command = 'rm ' . $currentDirName . $TGZFile;
        echo $command . chr(10);
        echo $remove_command . chr(10);
        if (shell_exec($command)) {
          shell_exec($remove_command);
        }
      }
    }
    $this->MoveFromRootToDateDir($titleId);
    parent::__construct($titleId, $reportId);
  }

  function isBinaryFile($file) {
    if (is_file($file)) {
      $mimeType = strtolower(mime_content_type($file));
      $this->mylog("checking file mime type for $file : " . $mimeType);
      if (!$mimeType) {
        return false;
      } else {

        $accepted = array('text', 'xml', 'html', 'empty', 'txt', 'jpg');
        foreach ($accepted as $pattern) {
          $rez = '';
          preg_match("/$pattern/i", $mimeType, $rez);

          if ($rez) {
            return false;
          }
        }

        //Some errors with mime types (i.e you got Audio instad of txt), so we also check the extension
        $ext = syndParseHelper::getFileExtension($file);
        $this->mylog("file extension : $ext");
        if (in_array($ext, $accepted)) {
          $this->mylog("extension in accepted xtensions");
          return false;
        }

        if (!is_dir($this->binTmpDir)) {
          mkdir($this->binTmpDir, 0777, true);
        }
        return true;
      }
      return false;
    }
  }

}

?>
