<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of syndGatherContentHttpC5959
 *
 * @author eyad
 */
class syndGatherContentHttpC5959 extends syndGatherContentHttp {

  protected function getRssUrl() {
    $urls = array(
        'Nature Picture Library Footage' => 'http://api.naturepl.com/bin/nplapi.dll/api?cmd=getsessionid&apikey=134B495E-4207-4602-9A21-C198922314A9&user=0&territory=0',
    );

    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $response = $this->curlFunction($subUrl);

      $fileName = $this->homeDir . str_replace(' ', '_', $cat . '.xml');
      $match = null;
      if (preg_match("/<sessionid>(.*?)<\/sessionid>/is", $response, $match)) {
        $contents = $this->getArticles($match[1]);
      } else {
        $contents = "";
      }
      // creating object of SimpleXMLElement
      $xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');
      // function call to convert array to xml
      $this->array_to_xml($contents, $xml_data);
      //saving generated xml file; 
      $result = $xml_data->asXML($fileName);
    }
  }

  public function getArticles($sessionid) {
    $data = array();
    $url = "http://api.naturepl.com/bin/nplapi.dll/api?cmd=search&query=Q20*+rf&parse=locationsxml&locations=1&reverse=1&max=100&si=$sessionid&csv=1&media=0";
        var_dump($url);
    $response = $this->curlFunction($url);
    $matches = null;
    if (preg_match("/<record>(.*?)<\/record>/is", $response, $matches)) {
      $matches = explode(",", $matches[1]);
      foreach ($matches as $match) {
        $url = "http://api.naturepl.com/bin/nplapi.dll/api?cmd=getlocationsdata&parse=locationsdataxml&elements=1,15,16,20,21,10,81,121&list=$match&si=$sessionid";
        var_dump($url);
        $response = $this->curlFunction($url);
        $id = $this->getElementByName("id", $response);
        $data[$match]["id"] = $id;
        $data[$match]["guid"] = $match;
        $data[$match]["caption"] = $this->getElementByName("caption", $response);
        $data[$match]["headline"] = $this->getElementByName("headline", $response);
        $data[$match]["byline"] = $this->getElementByName("byline", $response);
        $data[$match]["copyright"] = $this->getElementByName("copyright", $response);
        $data[$match]["datetimetext"] = $this->getElementByName("datetimetext", $response);
        $data[$match]["thumbnailpath1"] = $this->getElementByName("thumbnailpath1", $response);
        $url = "http://api.naturepl.com/bin/nplapi.dll/api?cmd=getfile&parse=getfilexml&ir=$id&si=$sessionid";
        $response = $this->curlFunction($url);
        $data[$match]["path"] = $this->getElementByName("path", $response);
      }
    }
    return $data;
  }

  public function array_to_xml($item, &$xml_data) {
    foreach ($item as $key => $value) {
      if (is_numeric($key)) {
        $key = 'item';
      }
      if (is_array($value)) {
        $subnode = $xml_data->addChild($key);
        $this->array_to_xml($value, $subnode);
      } else {
        $xml_data->addChild("$key", htmlspecialchars("$value"));
      }
    }
  }

  public function curlFunction($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
    curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
    // This is what solved the issue (Accepting gzip encoding)
    curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }

  /**
   * get all xml elements contents
   *
   * @param String $name element name
   * @param String $text all xml text
   * @return array of elements contents
   */
  public function getElementsByName($name, $text) {
    $openTagRegExp = '<' . $name . '([^\>]|[\s])*>';
    $closeTagRegExp = '<\/' . $name . '[\s]*>';
    $elements = preg_split("/$openTagRegExp/i", $text);

    $elementsContents = array();
    $elementsCount = count($elements);
    for ($i = 1; $i < $elementsCount; $i++) {
      $element = preg_split("/$closeTagRegExp/i", $elements[$i]);
      $elementsContents[] = $element[0];
    }
    return $elementsContents;
  }

  /**
   * get first xml element contents
   *
   * @param String $name element name
   * @param String $text all xml text
   * @return string
   */
  public function getElementByName($name, $text) {
    $element = $this->getElementsByName($name, $text);
    return (isset($element[0]) ? $element[0] : null);
  }

}
