<?php

class syndGatherContentHttpC2167 extends syndGatherContentHttp {

  protected function getRssUrl() {
    $urls = array(
        'Masrawy1' => 'https://www.masrawy.com/standardrssfeed?categoryid=35&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy2' => 'https://www.masrawy.com/standardrssfeed?categoryid=202&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy3' => 'https://www.masrawy.com/standardrssfeed?categoryid=209&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy4' => 'https://www.masrawy.com/standardrssfeed?categoryid=206&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy5' => 'https://www.masrawy.com/standardrssfeed?categoryid=205&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy6' => 'https://www.masrawy.com/standardrssfeed?categoryid=204&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy7' => 'https://www.masrawy.com/standardrssfeed?categoryid=207&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy8' => 'https://www.masrawy.com/standardrssfeed?categoryid=211&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy9' => 'https://www.masrawy.com/standardrssfeed?categoryid=208&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy10' => 'https://www.masrawy.com/standardrssfeed?categoryid=385&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy11' => 'https://www.masrawy.com/standardrssfeed?categoryid=383&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy12' => 'https://www.masrawy.com/standardrssfeed?categoryid=255&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy13' => 'https://www.masrawy.com/standardrssfeed?categoryid=235&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy14' => 'https://www.masrawy.com/standardrssfeed?categoryid=254&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy15' => 'https://www.masrawy.com/standardrssfeed?categoryid=582&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy16' => 'https://www.masrawy.com/standardrssfeed?categoryid=583&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy17' => 'https://www.masrawy.com/standardrssfeed?categoryid=220&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy18' => 'https://www.masrawy.com/standardrssfeed?categoryid=231&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy19' => 'https://www.masrawy.com/standardrssfeed?categoryid=223&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy20' => 'https://www.masrawy.com/standardrssfeed?categoryid=239&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy21' => 'https://www.masrawy.com/standardrssfeed?categoryid=219&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy22' => 'https://www.masrawy.com/standardrssfeed?categoryid=222&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy23' => 'https://www.masrawy.com/standardrssfeed?categoryid=402&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy24' => 'https://www.masrawy.com/standardrssfeed?categoryid=603&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy25' => 'https://www.masrawy.com/standardrssfeed?categoryid=577&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy26' => 'https://www.masrawy.com/standardrssfeed?categoryid=579&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy27' => 'https://www.masrawy.com/standardrssfeed?categoryid=578&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy28' => 'https://www.masrawy.com/standardrssfeed?categoryid=227&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy29' => 'https://www.masrawy.com/standardrssfeed?categoryid=375&providerid=61&feedcount=30&AttachGallerytoBody=0 ',
        'Masrawy30' => 'https://www.masrawy.com/standardrssfeed?categoryid=376&providerid=61&feedcount=30&AttachGallerytoBody=0',
        'Masrawy31' => 'https://www.masrawy.com/standardrssfeed?categoryid=598&providerid=61&feedcount=30&AttachGallerytoBody=0',
    );

    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $this->setOpt();
      //grab URL and pass it to the browser
      if (!curl_exec($this->ch)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not Perform the cURL session of ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      } else {
        $this->errorMsg = 'Perform the cURL session';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));

        curl_close($this->ch);
        fclose($this->fp);
      }
    }
  }

}
