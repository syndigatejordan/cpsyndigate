<?php

require_once 'Net/FTP.php';

class syndGatherContentFtpC896 extends syndGatherContentFtp {

  /**
   * Constructor for the FTP class
   *
   * @param int $titleId
   */
  public function __construct($titleId, $reportId) {

    parent::__construct($titleId, $reportId);
  }

  /**
   * List the given FTP directory
   *
   * @param string $dir
   * @return array
   */
  public function ls($dir) {
    $this->ftp->setActive();
    $folders = $this->ftp->ls($dir);
    if (PEAR::isError($folders)) {
      $this->reportParams['LatestStatus'] = 0;
      $this->reportParams['Error'] = $folders->getMessage() . ' for: ' . $this->ftpServer;
      $this->report->updateReport($this->reportParams);
      $this->log->log($folders->getMessage() . ' for: ' . $this->ftpUsername, ezcLog::FATAL, $this->logParams);
      return false;
    } else {
      $this->log->log('FTP Listing for ' . $this->ftpUsername . ' Succeeded.', ezcLog::DEBUG, $this->logParams);
      return $folders;
    }
  }

}
