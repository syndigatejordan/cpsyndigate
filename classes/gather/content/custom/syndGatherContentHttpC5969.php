<?php

class syndGatherContentHttpC5969 extends syndGatherContentHttp {

  public $articles = array();
  public $page = "";

  protected function getRssUrl() {

    for ($i = 1; $i < 5; $i++) {
      $urls["AYLIEN $i"] = "https://api.aylien.com/news/stories?language=en&sentiment.body.polarity=negative&sort_by=published_at&per_page=100";
    }
    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    $next_page_cursor = null;
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      sleep(2);
      $fileName = $this->homeDir . str_replace(' ', '_', $cat . '.xml');
      $response = $this->getContant($subUrl, $next_page_cursor);
      $contents = json_decode($response, true);

      $data = array();
      foreach ($contents["stories"] as $key => $item) {
        $jsondata = array();
        foreach ($item as $k => $i) {
          if ($k == "source" || $k == "entities" || $k == "keywords" ||
                  $k == "hashtags" || $k == "characters_count" || $k == "words_count" ||
                  $k == "sentences_count" || $k == "paragraphs_count" || $k == "categories" ||
                  $k == "social_shares_count" || $k == "sentiment" || $k == "clusters") {
            $jsondata[$k] = $i;
          }
        }
        $contents["stories"][$key]["jsondata"] = json_encode($jsondata);
        $data[]["fullArticle"] = $contents["stories"][$key];
      }
      $next_page_cursor = $contents["next_page_cursor"];
      // creating object of SimpleXMLElement
      $xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');
      // function call to convert array to xml
      $this->array_to_xml($data, $xml_data);
      //saving generated xml file; 
      $result = $xml_data->asXML($fileName);
    }
  }

  public function getContant($subUrl, $next_page_cursor = null) {
    if (!is_null($next_page_cursor)) {
      $subUrl.="&next_page_cursor=$next_page_cursor&cursor=$next_page_cursor";
    }
    echo "	 message : cURL session for " . $subUrl . PHP_EOL . PHP_EOL;
    $opts = array();
    $http_headers = array();
    $http_headers[] = 'X-AYLIEN-NewsAPI-Application-ID:8cd77f3b';
    $http_headers[] = 'X-AYLIEN-NewsAPI-Application-Key:cfb268ecb2a8b4549ffeeba0808aea18';

    $opts[CURLOPT_URL] = $subUrl;
    $opts[CURLOPT_HTTPHEADER] = $http_headers;
    $opts[CURLOPT_CONNECTTIMEOUT] = 10;
    $opts[CURLOPT_TIMEOUT] = 60;
    $opts[CURLOPT_HEADER] = false;
    $opts[CURLOPT_BINARYTRANSFER] = true;
    $opts[CURLOPT_VERBOSE] = false;
    $opts[CURLOPT_SSL_VERIFYPEER] = false;
    $opts[CURLOPT_SSL_VERIFYHOST] = 2;
    $opts[CURLOPT_RETURNTRANSFER] = true;
    $opts[CURLOPT_FOLLOWLOCATION] = true;
    $opts[CURLOPT_MAXREDIRS] = 2;
    // $opts[CURLOPT_IPRESOLVE] = CURL_IPRESOLVE_V4;
# Initialize PHP/CURL handle
    $ch = curl_init();
    curl_setopt_array($ch, $opts);
    $response = curl_exec($ch);

# Close PHP/CURL handle
    curl_close($ch);
    return $response;
  }

  public function array_to_xml($item, &$xml_data) {
    foreach ($item as $key => $value) {
      if (is_numeric($key)) {
        $key = 'item';
      }
      if (is_array($value)) {
        $subnode = $xml_data->addChild($key);
        $this->array_to_xml($value, $subnode);
      } else {
        $xml_data->addChild("$key", htmlspecialchars("$value"));
      }
    }
  }

}
