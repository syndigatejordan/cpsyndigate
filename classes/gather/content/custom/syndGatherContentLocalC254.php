<?php
class syndGatherContentLocalC254 extends syndGatherContentLocal {
	public $soapUrl = 'http://212.174.168.106/vykdataservice/service.asmx?wsdl';
	public $soapUser = "DV736";
	public $soapPassword = "P15379";
	public function __construct($titleId, $reportId = '') {
		$newsById_str = $this -> getSoapData('GetLastNews', '');
		$dom = new DOMDocument;
		$dom -> loadXML($newsById_str);
		$NewsId = $dom -> getElementsByTagName('NewsId');
		foreach ($NewsId as $new) {
			$max_news_id = $new -> nodeValue;
		}
		$min_news_id = $this -> getMinxNewsId();
		$model = new syndModel();
		$homeDirPath = $model -> getHomeDir($titleId);
		echo "Min News ID:$min_news_id Max news ID:$max_news_id".chr(10);
		while ($min_news_id <= $max_news_id) {
			$min_news_id++;
			$params = "<HaberId>" . $min_news_id . "</HaberId>";
			$news_str = $this -> getSoapData('GetNewsById', $params);
			$this -> saveNewsFile($news_str, $min_news_id, $homeDirPath);
			echo $min_news_id." Saved..".chr(10);
		}
		$this -> updateMaxNewsId($min_news_id);
		$this -> MoveFromRootToDateDir($titleId);
		parent::__construct($titleId, $reportId);
	}

	public function saveNewsFile($news_str, $news_id, $homeDirPath) {
		$xml_content = html_entity_decode($news_str);
		$file_name = $news_id . ".xml";
		$file_name = $homeDirPath . DIRECTORY_SEPARATOR . $file_name;
		$fp = fopen($file_name, 'w');
		fwrite($fp, $xml_content);
		fclose($fp);
	}

	public function getSoapData($function, $params) {
		$xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
                            <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
							  <soap12:Body>
							    <' . $function . ' xmlns="http://tempuri.org/">
							      <usr>' . $this -> soapUser . '</usr>
							      <pwd>' . $this -> soapPassword . '</pwd>
							      ' . $params . '
							    </' . $function . '>
							  </soap12:Body>
							</soap12:Envelope>';

		// data from the form, e.g. some ID number
		$headers = array("Content-type: text/xml;charset=\"utf-8\"", 'SOAPAction: "http://tempuri.org/' . $function . '"', "Content-length: " . strlen($xml_post_string), );
		// PHP cURL  for https connection with auth
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_URL, $this -> soapUrl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERPWD, $this -> soapUser . ":" . $this -> soapPassword);
		// username and password - declared at the top of the doc
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
		// the SOAP request
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		// converting
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}

	public function getMinxNewsId() {
		$q = mysql_query("select max_news_id from max_borsa_istanbul_news_id limit 1");
		$row = mysql_fetch_row($q);
		if (empty($row)) {
			$q = mysql_query("insert into max_borsa_istanbul_news_id set max_news_id=10000");
			$q = mysql_query("select max_news_id from max_borsa_istanbul_news_id limit 1");
			$row = mysql_fetch_row($q);
		}
		return $row[0];
	}

	public function updateMaxNewsId($min_news_id) {
		$q = mysql_query("update max_borsa_istanbul_news_id set max_news_id=$min_news_id");
		if ($q)
			return true;
	}

}
