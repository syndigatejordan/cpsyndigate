<?php

class syndGatherContentHttpC6008 extends syndGatherContentHttp {

  protected function getRssUrl() {
    $urls = array(
        "video" => "http://syndigate:1130461851@vfeed.synd.bloomberg.com/f/nJEtBC/syndigate01",
        "Quicktake video" => "http://syndigate:1130461851@vfeed.synd.bloomberg.com/f/nJEtBC/syndigateqtex",
    );
    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
          // CURLOPT_USERPWD => "$username:$password",
          CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
      );

      $ch = curl_init($subUrl);
      curl_setopt_array($ch, $options);
      $response = curl_exec($ch);
      curl_close($ch);
      $lastBuildDate = null;
      if (preg_match("/<lastBuildDate>(.*?)<\/lastBuildDate>/", $response, $lastBuildDate)) {
        $response = str_replace($lastBuildDate[0], "", $response);
        $response = str_replace("<pubDate>{$lastBuildDate[1]}</pubDate>", "", $response);
      }
      $response = $this->sortItem($response);
      $fileName = str_replace(' ', '_', $cat . '.xml');
      file_put_contents($fileName, $response);
    }
  }

  public function sortItem($response) {
    $matches = null;
    $articles = array();
    preg_match_all("/<item>(.*?)<\/item>/is", $response, $matches);
    foreach ($matches[0] as $item) {
      $guid = trim($this->getElementByName("guid", $item));
      $articles[$guid] = $item;
    }
    ksort($articles);
    $rss = '<rss xmlns:msn="https://catalog.video.msn.com" xmlns:plrelease="http://xml.theplatform.com/media/data/Release" xmlns:plfile="http://xml.theplatform.com/media/data/MediaFile" xmlns:plmedia="http://xml.theplatform.com/media/data/Media" xmlns:pla="http://xml.theplatform.com/data/object/admin" xmlns:pl="http://xml.theplatform.com/data/object" xmlns:pllist="http://xml.theplatform.com/data/list" xmlns:hulu="http://xml.hulu.com/fields" xmlns:bbg="http://xml.bloomberg.com/fields" xmlns:ytcp="http://www.youtube.com/schemas/cms/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ooyala="http://www.ooyala.com/mrss/" xmlns:bms="http://www.bloomberg.com/module/bms/" xmlns:media="http://search.yahoo.com/mrss/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:pl1="http://access.auth.theplatform.com/data/Account/2176074343" xmlns:os="http://a9.com/-/spec/opensearch/1.1/" version="2.0"><channel>';
    foreach ($articles as $article) {
      $rss.=$article;
    }
    $rss.="</channel></rss>";
    return $rss;
  }

  /**
   * get all xml elements contents
   *
   * @param String $name element name
   * @param String $text all xml text
   * @return array of elements contents
   */
  public function getElementsByName($name, $text) {
    $openTagRegExp = '<' . $name . '([^\>]|[\s])*>';
    $closeTagRegExp = '<\/' . $name . '[\s]*>';
    $elements = preg_split("/$openTagRegExp/i", $text);

    $elementsContents = array();
    $elementsCount = count($elements);
    for ($i = 1; $i < $elementsCount; $i++) {
      $element = preg_split("/$closeTagRegExp/i", $elements[$i]);
      $elementsContents[] = $element[0];
    }
    return $elementsContents;
  }

  /**
   * get first xml element contents
   *
   * @param String $name element name
   * @param String $text all xml text
   * @return string
   */
  public function getElementByName($name, $text) {
    $element = $this->getElementsByName($name, $text);
    return (isset($element[0]) ? $element[0] : null);
  }

}
