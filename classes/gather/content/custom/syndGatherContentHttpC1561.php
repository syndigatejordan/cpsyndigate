<?php

/* 7Days */

class syndGatherContentHttpC1561 extends syndGatherContentHttp
{
    protected function getRssUrl ()
    {
        $urls = array(
		'news' => 'https://www.rbr.com/category/radio-news/feed/',
		'tvcable_news' => 'https://www.rbr.com/category/tvcable-news/feed/',
		'media_news' => 'https://www.rbr.com/category/media-news/feed/',
		'media_info_bureau' => 'https://www.rbr.com/category/media-info-bureau/feed/',
		'adbiz' => 'https://www.rbr.com/category/adbiz/feed/',
		'broadcasting_stocks' => 'https://www.rbr.com/category/broadcasting-stocks/feed/',
		'internet' => 'https://www.rbr.com/category/internet/feed/',
		'media_moves' => 'https://www.rbr.com/category/media-moves/feed/',
		'washington_beat' => 'https://www.rbr.com/category/washington-beat/feed/',
		'Radio and Television Business Report'=>'https://www.rbr.com/feed/',
		
        );
        
        return $urls;
    }
    
    public function checkContent ($subUrl = '', $cat = '', $check = false)
    {
        $this->check = $check;
        if ($check) {
            if (! $this->ch = curl_init($subUrl)) {
                $this->processState = ezcLog::FAILED_AUDIT;
                $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
                $this->reportParams['LatestStatus'] = 0;
                $this->report->updateReport($this->reportParams);
                $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
                return false;
            } else {
                $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
                $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
                return true;
            }
        }
        $fileName = str_replace(' ', '_', $cat . '.xml');
        if (! $this->fp = fopen($fileName, "w")) {
            //Log the proccess for debugging
            $this->processState = ezcLog::FAILED_AUDIT;
            $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
            $this->reportParams['LatestStatus'] = 0;
            $this->report->updateReport($this->reportParams);
            $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
            return false;
        } else {
            $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
            $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
            if (file_exists('.xml')) {
                unlink('.xml');
            }
        }
        if (! $this->ch = curl_init($subUrl)) {
            $this->processState = ezcLog::FAILED_AUDIT;
            $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
            $this->reportParams['LatestStatus'] = 0;
            $this->report->updateReport($this->reportParams);
            $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
            return false;
        } else {
            $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
            $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        }
    }
    public function copyContent ()
    {
        foreach ($this->getRssUrl() as $cat => $subUrl) {
            $this->checkContent($subUrl, $cat);
            $this->setOpt();
            //grab URL and pass it to the browser
            if (! curl_exec($this->ch)) {
                $this->processState = ezcLog::FAILED_AUDIT;
                $this->reportParams['Error'] = $this->errorMsg = 'Could not Perform the cURL session of ' . $subUrl;
                $this->reportParams['LatestStatus'] = 0;
                $this->report->updateReport($this->reportParams);
                $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
            } else {
                $this->errorMsg = 'Perform the cURL session';
                $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));

		curl_close($this->ch);
		fclose($this->fp);
            }
        }
	//curl_close($this->ch);
	//fclose($this->fp);
	return true;

    }
}
