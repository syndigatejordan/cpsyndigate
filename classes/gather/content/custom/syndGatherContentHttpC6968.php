<?php

class syndGatherContentHttpC6968 extends syndGatherContentHttp {

  protected function getRssUrl() {
    $urls = array(
        '237online 1' => 'https://www.237online.com/author/237online/feed',
        '237online 2' => 'https://www.237online.com/author/alain-basile-djomo/feed',
        '237online 3' => 'https://www.237online.com/author/bossis-eboo/feed',
        '237online 4' => 'https://www.237online.com/author/communique-de-presse/feed',
        '237online 5' => 'https://www.237online.com/author/communique-officiel/feed',
        '237online 6' => 'https://www.237online.com/author/correspondance/feed',
        '237online 7' => 'https://www.237online.com/author/essingan/feed',
        '237online 8' => 'https://www.237online.com/author/franck-daniel-batassi/feed',
        '237online 9' => 'https://www.237online.com/author/lavenir-hebdo/feed',
        '237online 10' => 'https://www.237online.com/author/le-messager/feed',
        '237online 11' => 'https://www.237online.com/author/lessentiel-du-cameroun/feed',
        '237online 12' => 'https://www.237online.com/author/loeil-du-sahel/feed',
        '237online 13' => 'https://www.237online.com/author/marcel-ndi-237online/feed',
        '237online 14' => 'https://www.237online.com/author/martial-bourgeois/feed',
        '237online 15' => 'https://www.237online.com/author/ntsama/feed',
    );
    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
          // CURLOPT_USERPWD => "$username:$password",
          CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
      );

      $ch = curl_init($subUrl);
      curl_setopt_array($ch, $options);
      $response = curl_exec($ch);
      curl_close($ch);
      $fileName = str_replace(' ', '_', $cat . '.xml');
      file_put_contents($fileName, $response);
    }
  }

}
