<?php
class syndGatherContentHttpC1646 extends syndGatherContentHttp {

	protected $urls = array('Daily Feeds' => 'http://www.globalservicesmedia.com/content_feed/', 'Daily Feed' => 'http://www.globalservicesmedia.com/content_feed/');
	public function checkContent($subUrl = '', $cat = '', $check = false) {
		$this -> check = $check;
		if ($check) {
			if (!$this -> ch = curl_init($subUrl)) {
				$this -> processState = ezcLog::FAILED_AUDIT;
				$this -> reportParams['Error'] = $this -> errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
				$this -> reportParams['LatestStatus'] = 0;
				$this -> report -> updateReport($this -> reportParams);
				$this -> log -> log($this -> errorMsg, ezcLog::ERROR, array('report_id' => $this -> reportId));
				return false;
			} else {
				$this -> errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
				$this -> log -> log($this -> errorMsg, ezcLog::DEBUG, array('report_id' => $this -> reportId));
				return true;
			}
		}
		$fileName = str_replace(' ', '_', $cat . '.xml');
		if (!$this -> fp = fopen($fileName, "w")) {
			//Log the proccess for debugging
			$this -> processState = ezcLog::FAILED_AUDIT;
			$this -> reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this -> title . ' Filename: ' . $fileName;
			$this -> reportParams['LatestStatus'] = 0;
			$this -> report -> updateReport($this -> reportParams);
			$this -> log -> log($this -> errorMsg, ezcLog::ERROR, array('report_id' => $this -> reportId));
			return false;
		} else {
			$this -> errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this -> title . ' Filename: ' . $fileName;
			$this -> log -> log($this -> errorMsg, ezcLog::DEBUG, array('report_id' => $this -> reportId));
			if (file_exists('.xml')) {
				unlink('.xml');
			}
		}

		if (!$this -> ch = curl_init($subUrl)) {
			$this -> processState = ezcLog::FAILED_AUDIT;
			$this -> reportParams['Error'] = $this -> errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
			$this -> reportParams['LatestStatus'] = 0;
			$this -> report -> updateReport($this -> reportParams);
			$this -> log -> log($this -> errorMsg, ezcLog::ERROR, array('report_id' => $this -> reportId));
			return false;
		} else {
			$this -> errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
			$this -> log -> log($this -> errorMsg, ezcLog::DEBUG, array('report_id' => $this -> reportId));
		}
	}

	public function copyContent() {

		$username = 'syndigate';
		$password = '$zA9!bY1&xC8';

		$context = stream_context_create(array('http' => array('header' => "Authorization: Basic " . base64_encode("$username:$password"))));
		$i = 1;
		foreach ($this->urls as $cat => $subUrl) {
			if ($i % 2 == 0)
				$subUrl = $subUrl . Date('d-m-Y');
			else {
				$subUrl = $subUrl . Date('d-m-Y', strtotime("-1 days"));
				$i++;
			}
			var_dump($subUrl);
			$this -> checkContent($subUrl, $cat);

			$fileContent = file_get_contents($subUrl, false, $context);

			$fileName = str_replace(' ', '_', $cat . '.xml');
			file_put_contents($fileName, $fileContent);
		}

	}

}
?>
