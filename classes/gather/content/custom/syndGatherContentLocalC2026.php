<?php

class syndGatherContentLocalC2026 extends syndGatherContentLocal {

  public function __construct($titleId, $reportId = '') {

    $c = new Criteria();
    $c->add(TitlePeer::ID, $titleId);
    $currentTitle = TitlePeer::doSelectOne($c);
    $currentDirName = $currentTitle->getHomeDir();

    $directories = scandir($currentDirName);
    foreach ($directories as $TGZFile) {
      $path = pathinfo($TGZFile);
      if (!empty($path['extension'])) {
        $path = $path['extension'];
      } else {
        $path = "";
      }

      if (($TGZFile != '.') && ($TGZFile != '..') && (is_file("{$currentDirName}{$TGZFile}")) && $path == "rar") {
        $command = 'unrar e -r ' . $currentDirName . $TGZFile . "  " . $currentDirName;
        $remove_command = 'rm ' . $currentDirName . $TGZFile;
        echo $command . chr(10);
        echo $remove_command . chr(10);
        if (shell_exec($command)) {
          shell_exec($remove_command);
        }
      }
    }
    //Fix File name
    $publiserDirName = '/syndigate/sources/home/83/2026/';
    $directories = scandir($publiserDirName);
    foreach ($directories as $File) {
      $path = pathinfo($File);
      if (($File != '.') && ($File != '..') && (is_file("{$publiserDirName}{$File}"))) {
        $path["basename"] = preg_replace("/[  ~&,:\-\)\(\']/", ' ', $path["basename"]);
        $path["basename"] = str_replace('..', '.', $path["basename"]);
        $path["basename"] = str_replace("'", "\'", $path["basename"]);
        $path["basename"] = str_replace("\'", "", $path["basename"]);
        $path["basename"] = str_replace("`", "\`", $path["basename"]);
        $path["basename"] = str_replace("\`", "", $path["basename"]);
        $path["basename"] = str_replace("\$", "\\$", $path["basename"]);
        $path["basename"] = str_replace("\$", "", $path["basename"]);
        $path["basename"] = str_replace('  ', ' ', $path["basename"]);
        $path["basename"] = preg_replace('!\s+!', '_', $path["basename"]);
        $path["basename"] = str_replace('._', '_', $path["basename"]);
        $path["basename"] = str_replace(' _', '_', $path["basename"]);
        $File = str_replace(' ', '\ ', $File);
        $File = str_replace('(', '\(', $File);
        $File = str_replace(')', '\)', $File);
        $File = str_replace("'", "\'", $File);
        $File = str_replace("`", "\`", $File);
        $File = str_replace("&", "\&", $File);
        $File = str_replace("\$", "\\$", $File);
        $move = "mv " . $publiserDirName . $File . " " . $publiserDirName . $path["basename"];
        if ($File != $path["basename"]) {
          echo $move . chr(10);
          shell_exec($move);
        }
      }
    }
    $this->MoveFromRootToDateDir($titleId);
    parent::__construct($titleId, $reportId);
  }

}

?>