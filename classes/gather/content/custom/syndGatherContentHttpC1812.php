<?php

class syndGatherContentHttpC1812 extends syndGatherContentHttp {

  protected function getRssUrl() {
    $urls = array(
        "TheNewIndianExpress_1" => "https://www.newindianexpress.com/Auto/rssfeed/?id=210&getXmlFeed=true",
        "TheNewIndianExpress_2" => "https://www.newindianexpress.com/Business/rssfeed/?id=186&getXmlFeed=true",
        "TheNewIndianExpress_3" => "https://www.newindianexpress.com/Cities/Ahmedabad/rssfeed/?id=343&getXmlFeed=true",
        "TheNewIndianExpress_4" => "https://www.newindianexpress.com/Cities/Bengaluru/rssfeed/?id=182&getXmlFeed=true",
        "TheNewIndianExpress_5" => "https://www.newindianexpress.com/Cities/Chennai/rssfeed/?id=181&getXmlFeed=true",
        "TheNewIndianExpress_6" => "https://www.newindianexpress.com/Cities/Delhi/rssfeed/?id=340&getXmlFeed=true",
        "TheNewIndianExpress_7" => "https://www.newindianexpress.com/Cities/Kochi/rssfeed/?id=185&getXmlFeed=true",
        "TheNewIndianExpress_8" => "https://www.newindianexpress.com/Cities/Kolkata/rssfeed/?id=342&getXmlFeed=true",
        "TheNewIndianExpress_9" => "https://www.newindianexpress.com/Cities/Mumbai/rssfeed/?id=341&getXmlFeed=true",
        "TheNewIndianExpress_10" => "https://www.newindianexpress.com/Cities/Thiruvananthapuram/rssfeed/?id=184&getXmlFeed=true",
        "TheNewIndianExpress_11" => "https://www.newindianexpress.com/Education/Edex/rssfeed/?id=229&getXmlFeed=true",
        "TheNewIndianExpress_12" => "https://www.newindianexpress.com/Entertainment/English/rssfeed/?id=194&getXmlFeed=true",
        "TheNewIndianExpress_13" => "https://www.newindianexpress.com/Entertainment/Hindi/rssfeed/?id=195&getXmlFeed=true",
        "TheNewIndianExpress_14" => "https://www.newindianexpress.com/Entertainment/Kannada/rssfeed/?id=196&getXmlFeed=true",
        "TheNewIndianExpress_15" => "https://www.newindianexpress.com/Entertainment/Malayalam/rssfeed/?id=197&getXmlFeed=true",
        "TheNewIndianExpress_16" => "https://www.newindianexpress.com/Entertainment/Review/rssfeed/?id=199&getXmlFeed=true",
        "TheNewIndianExpress_17" => "https://www.newindianexpress.com/Entertainment/Tamil/rssfeed/?id=198&getXmlFeed=true",
        "TheNewIndianExpress_18" => "https://www.newindianexpress.com/Entertainment/Telugu/rssfeed/?id=320&getXmlFeed=true",
        "TheNewIndianExpress_19" => "https://www.newindianexpress.com/Galleries/Nation/rssfeed/?id=201&getXmlFeed=true",
        "TheNewIndianExpress_20" => "https://www.newindianexpress.com/Galleries/Other/rssfeed/?id=204&getXmlFeed=true",
        "TheNewIndianExpress_21" => "https://www.newindianexpress.com/Galleries/Sport/rssfeed/?id=203&getXmlFeed=true",
        "TheNewIndianExpress_22" => "https://www.newindianexpress.com/Galleries/World/rssfeed/?id=202&getXmlFeed=true",
        "TheNewIndianExpress_23" => "https://www.newindianexpress.com/Life-Style/Books/rssfeed/?id=216&getXmlFeed=true",
        "TheNewIndianExpress_24" => "https://www.newindianexpress.com/Life-Style/Food/rssfeed/?id=215&getXmlFeed=true",
        "TheNewIndianExpress_25" => "https://www.newindianexpress.com/Life-Style/Health/rssfeed/?id=213&getXmlFeed=true",
        "TheNewIndianExpress_26" => "https://www.newindianexpress.com/Life-Style/Spirituality/Astrology/rssfeed/?id=218&getXmlFeed=true",
        "TheNewIndianExpress_27" => "https://www.newindianexpress.com/Life-Style/Spirituality/rssfeed/?id=217&getXmlFeed=true",
        "TheNewIndianExpress_28" => "https://www.newindianexpress.com/Life-Style/Tech/rssfeed/?id=212&getXmlFeed=true",
        "TheNewIndianExpress_29" => "https://www.newindianexpress.com/Life-Style/Travel/rssfeed/?id=214&getXmlFeed=true",
        "TheNewIndianExpress_30" => "https://www.newindianexpress.com/Nation/rssfeed/?id=170&getXmlFeed=true",
        "TheNewIndianExpress_31" => "https://www.newindianexpress.com/Opinions/Columns/Karamathullah-K-Ghori/rssfeed/?id=228&getXmlFeed=true",
        "TheNewIndianExpress_32" => "https://www.newindianexpress.com/Opinions/Columns/Ravi-Shankar/rssfeed/?id=225&getXmlFeed=true",
        "TheNewIndianExpress_33" => "https://www.newindianexpress.com/Opinions/Columns/rssfeed/?id=221&getXmlFeed=true",
        "TheNewIndianExpress_34" => "https://www.newindianexpress.com/Opinions/Columns/S-Gurumurthy/rssfeed/?id=224&getXmlFeed=true",
        "TheNewIndianExpress_35" => "https://www.newindianexpress.com/Opinions/Columns/Shampa-Dhar-Kamath/rssfeed/?id=227&getXmlFeed=true",
        "TheNewIndianExpress_36" => "https://www.newindianexpress.com/Opinions/Columns/Shankkar-Aiyar/rssfeed/?id=226&getXmlFeed=true",
        "TheNewIndianExpress_37" => "https://www.newindianexpress.com/Opinions/Columns/Soli-J-Sorabjee/rssfeed/?id=322&getXmlFeed=true",
        "TheNewIndianExpress_38" => "https://www.newindianexpress.com/Opinions/Columns/T-J-S-George/rssfeed/?id=223&getXmlFeed=true",
        "TheNewIndianExpress_39" => "https://www.newindianexpress.com/Opinions/Columns/V-Sudarshan/rssfeed/?id=321&getXmlFeed=true",
        "TheNewIndianExpress_40" => "https://www.newindianexpress.com/Opinions/Debate/rssfeed/?id=344&getXmlFeed=true",
        "TheNewIndianExpress_41" => "https://www.newindianexpress.com/Opinions/Editorials/rssfeed/?id=219&getXmlFeed=true",
        "TheNewIndianExpress_42" => "https://www.newindianexpress.com/Prabhu-Chawla/Columns/rssfeed/?id=306&getXmlFeed=true",
        "TheNewIndianExpress_43" => "https://www.newindianexpress.com/Specials/rssfeed/?id=339&getXmlFeed=true",
        "TheNewIndianExpress_44" => "https://www.newindianexpress.com/Sport/Cricket/rssfeed/?id=188&getXmlFeed=true",
        "TheNewIndianExpress_45" => "https://www.newindianexpress.com/Sport/Cricket/Scorecard/rssfeed/?id=288&getXmlFeed=true",
        "TheNewIndianExpress_46" => "https://www.newindianexpress.com/Sport/Football/Fifa-U-17-World-Cup-2017/rssfeed/?id=367&getXmlFeed=true",
        "TheNewIndianExpress_47" => "https://www.newindianexpress.com/Sport/Football/rssfeed/?id=190&getXmlFeed=true",
        "TheNewIndianExpress_48" => "https://www.newindianexpress.com/Sport/IPL/rssfeed/?id=284&getXmlFeed=true",
        "TheNewIndianExpress_49" => "https://www.newindianexpress.com/Sport/Other/rssfeed/?id=191&getXmlFeed=true",
        "TheNewIndianExpress_50" => "https://www.newindianexpress.com/Sport/Tennis/rssfeed/?id=189&getXmlFeed=true",
        "TheNewIndianExpress_51" => "https://www.newindianexpress.com/States/Andhra-Pradesh/rssfeed/?id=173&getXmlFeed=true",
        "TheNewIndianExpress_52" => "https://www.newindianexpress.com/States/Karnataka/rssfeed/?id=175&getXmlFeed=true",
        "TheNewIndianExpress_53" => "https://www.newindianexpress.com/States/Kerala/rssfeed/?id=178&getXmlFeed=true",
        "TheNewIndianExpress_54" => "https://www.newindianexpress.com/States/Odisha/rssfeed/?id=179&getXmlFeed=true",
        "TheNewIndianExpress_55" => "https://www.newindianexpress.com/States/Tamil-Nadu/rssfeed/?id=177&getXmlFeed=true",
        "TheNewIndianExpress_56" => "https://www.newindianexpress.com/States/Telangana/rssfeed/?id=174&getXmlFeed=true",
        "TheNewIndianExpress_57" => "https://www.newindianexpress.com/Videos/Nation/rssfeed/?id=206&getXmlFeed=true",
        "TheNewIndianExpress_58" => "https://www.newindianexpress.com/Videos/Other/rssfeed/?id=209&getXmlFeed=true",
        "TheNewIndianExpress_59" => "https://www.newindianexpress.com/Videos/Sport/rssfeed/?id=208&getXmlFeed=true",
        "TheNewIndianExpress_60" => "https://www.newindianexpress.com/Videos/World/rssfeed/?id=207&getXmlFeed=true",
        "TheNewIndianExpress_61" => "https://www.newindianexpress.com/World/rssfeed/?id=171&getXmlFeed=true",
    );

    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['  Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id  ' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $this->setOpt();
      //grab URL and pass it to the browser
      if (!curl_exec($this->ch)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not Perform the cURL session of ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      } else {
        $this->errorMsg = 'Perform the cURL session';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));

        curl_close($this->ch);
        fclose($this->fp);
      }
    }
  }

}
