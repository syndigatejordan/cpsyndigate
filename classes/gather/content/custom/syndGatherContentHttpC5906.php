<?php

class syndGatherContentHttpC5906 extends syndGatherContentHttp {

  public $articles = array();

  protected function getRssUrl() {
    $page = 1;
    $year=date('Y');
    $urls = array();
    for ($i = 1; $i < 5; $i++) {
      $urls["NASA on The Commons $i"] = "https://images-api.nasa.gov/search?media_type=image&year_start=$year&year_end=$year&page=$page";
      $page++;
    }
    //86
   /* $page = 59;
    for ($i = 1; $i < 40; $i++) {
      $urls["NASA on The Commons 2019 $i"] = "https://images-api.nasa.gov/search?media_type=image&year_start=2019&year_end=2019&page=$page";
      $page++;
    }*/
    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);

      $response = $this->getContant($subUrl);
      $fileName = $this->homeDir . str_replace(' ', '_', $cat . '.xml');
      $contents = json_decode($response, true);
      $contents = $contents["collection"]["items"];
      $data = array();
      foreach ($contents as &$item) {
        $jsonData = $this->getMetadata($item["href"]);
        $item["collection"] = $jsonData["collection"];
        $item["metadata"] = $jsonData["metadata"];

        $data[]["fullArticle"] = $item;
      }

      // creating object of SimpleXMLElement
      $xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');
      // function call to convert array to xml
      $this->array_to_xml($data, $xml_data);
      //saving generated xml file; 
      $result = $xml_data->asXML($fileName);
    }
  }

  public function getContant($subUrl) {
    $options = array(
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // do not return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_USERAGENT => "spider", // who am i
        CURLOPT_AUTOREFERER => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
        CURLOPT_TIMEOUT => 120, // timeout on response
        CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
    );

    $ch = curl_init($subUrl);
    curl_setopt_array($ch, $options);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }

  public function getMetadata($subUrl) {
    $date = array();
    $options = array(
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // do not return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_USERAGENT => "spider", // who am i
        CURLOPT_AUTOREFERER => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
        CURLOPT_TIMEOUT => 120, // timeout on response
        CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
    );

    $ch = curl_init($subUrl);
    curl_setopt_array($ch, $options);
    $response = curl_exec($ch);
    curl_close($ch);
    $response = json_decode($response, true);
    $date["collection"] = $response;

    $subUrl = str_replace("collection.json", "metadata.json", $subUrl);
    $response = "";
    $options = array(
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // do not return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_USERAGENT => "spider", // who am i
        CURLOPT_AUTOREFERER => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
        CURLOPT_TIMEOUT => 120, // timeout on response
        CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
    );

    $ch = curl_init($subUrl);
    curl_setopt_array($ch, $options);
    $response = curl_exec($ch);
    curl_close($ch);
    $response = json_decode($response, true);
    $date["metadata"] = $response;
    return $date;
  }

  public function array_to_xml($item, &$xml_data) {
    foreach ($item as $key => $value) {
      if (is_numeric($key)) {
        $key = 'item';
      }
      if (is_array($value)) {
        $subnode = $xml_data->addChild($key);
        $this->array_to_xml($value, $subnode);
      } else {
        $xml_data->addChild("$key", htmlspecialchars("$value"));
      }
    }
  }

}
