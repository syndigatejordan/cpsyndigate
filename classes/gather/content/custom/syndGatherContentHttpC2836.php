<?php

class syndGatherContentHttpC2836 extends syndGatherContentHttp {

  protected function getRssUrl() {
    $urls = array(
        'asfaar_1' => 'http://www.asfaar.net/category/%D8%A3%D8%AE%D8%A8%D8%A7%D8%B1-%D8%B3%D9%8A%D8%A7%D8%AD%D9%8A%D8%A9/feed/',
        'asfaar_2' => 'http://www.asfaar.net/category/%D8%B1%D8%AD%D9%84%D8%A7%D8%AA/feed/',
        'asfaar_3' => 'http://www.asfaar.net/category/%D8%B3%D9%8A%D8%A7%D8%AD%D8%A9/feed/',
        'asfaar_4' => 'http://www.asfaar.net/category/%D9%85%D8%AC%D8%AA%D9%85%D8%B9/feed/',
        'asfaar_5' => 'http://www.asfaar.net/category/%D8%AE%D8%AF%D9%85%D8%A7%D8%AA-%D8%A7%D9%88%D9%86%D9%84%D8%A7%D9%8A%D9%86/feed/',
        'asfaar_6' => 'http://www.asfaar.net/category/%D9%81%D8%B9%D8%A7%D9%84%D9%8A%D8%A7%D8%AA-%D9%88%D8%A3%D8%AD%D8%AF%D8%A7%D8%AB/feed/',
        'asfaar_7' => 'http://www.asfaar.net/category/%D9%81%D9%86%D8%A7%D8%AF%D9%82/feed/',
    );

    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }


    public function copyContent() {
        foreach ($this->getRssUrl() as $cat => $subUrl) {
            $this->checkContent($subUrl, $cat);
            $options = array(
                CURLOPT_RETURNTRANSFER => true, // return web page
                CURLOPT_HEADER => false, // do not return headers
                CURLOPT_FOLLOWLOCATION => true, // follow redirects
                CURLOPT_USERAGENT => "spider", // who am i
                CURLOPT_AUTOREFERER => true, // set referer on redirect
                CURLOPT_CONNECTTIMEOUT => 10, // timeout on connect
                CURLOPT_TIMEOUT => 10, // timeout on response
                CURLOPT_MAXREDIRS => 1, // stop after 10 redirects
                // CURLOPT_USERPWD => "$username:$password",
                CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
            );

            $ch = curl_init($subUrl);
            curl_setopt_array($ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);
            $fileName = str_replace(' ', '_', $cat . '.xml');
            file_put_contents($fileName, $response);
        }
    }
}
