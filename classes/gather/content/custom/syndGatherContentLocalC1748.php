<?php

class syndGatherContentLocalC1748 extends syndGatherContentLocal {

  public function __construct($titleId, $reportId = '') {
    //parent::__construct($titleId, $reportId);
    $custom_dirName = '/syndigate/sources/home/606/FlightGlobal/';
    $title_dir = '/syndigate/sources/home/606/1748/';
    chdir($custom_dirName);
    $files = scandir($custom_dirName);
    foreach ($files as $file) {
      $path = pathinfo($file);
      if ($file != "." && $file != ".." && is_file($file)) {
        $path["basename"] = preg_replace("/[  ~&,:\-\)\(\']/", ' ', $path["basename"]);
        $path["basename"] = str_replace('..', '.', $path["basename"]);
        $path["basename"] = str_replace("'", "\'", $path["basename"]);
        $path["basename"] = str_replace("\'", "", $path["basename"]);
        $path["basename"] = str_replace("`", "\`", $path["basename"]);
        $path["basename"] = str_replace("\`", "", $path["basename"]);
        $path["basename"] = str_replace("\$", "\\$", $path["basename"]);
        $path["basename"] = str_replace("\$", "", $path["basename"]);
        $path["basename"] = str_replace('  ', ' ', $path["basename"]);
        $path["basename"] = preg_replace('!\s+!', '_', $path["basename"]);
        $path["basename"] = str_replace('._', '_', $path["basename"]);
        $path["basename"] = str_replace(' _', '_', $path["basename"]);
        $file = str_replace(' ', '\ ', $file);
        $file = str_replace('(', '\(', $file);
        $file = str_replace(')', '\)', $file);
        $file = str_replace("'", "\'", $file);
        $file = str_replace("`", "\`", $file);
        $file = str_replace("&", "\&", $file);
        $file = str_replace("\$", "\\$", $file);
        $move = "mv " . $custom_dirName . $file . " " . $title_dir . $path["basename"];
        if ($file != $path["basename"]) {
          echo $move . chr(10);
          shell_exec($move);
        }

        $file = $custom_dirName . $file;
        $cond = "mv $file $title_dir";
        exec($cond);
      }
    }
    chdir("/usr/local/syndigate/classes/gather/content/custom");
    $this->MoveFromRootToDateDir($titleId);
    parent::__construct($titleId, $reportId);
  }

}
