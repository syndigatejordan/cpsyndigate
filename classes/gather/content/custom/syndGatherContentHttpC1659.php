<?php

class syndGatherContentHttpC1659 extends syndGatherContentHttp {

  protected function getRssUrl() {
    $urls = array(
        'New_news' => 'http://www.arabsturbo.com/ixml/terms/syndigate/%D8%A3%D8%AE%D8%A8%D8%A7%D8%B1-%D8%AC%D8%AF%D9%8A%D8%AF%D8%A9', //
        'Test_drive' => 'http://www.arabsturbo.com/ixml/terms/syndigate/%D8%AA%D8%AC%D8%A7%D8%B1%D8%A8-%D9%82%D9%8A%D8%A7%D8%AF%D8%A9', //
        'Compare_and_reports' => 'http://www.arabsturbo.com/ixml/terms/syndigate/%D9%85%D9%82%D8%A7%D8%B1%D9%86%D8%A9-%D9%88-%D8%AA%D9%82%D8%A7%D8%B1%D9%8A%D8%B1', //
        'Hybrid_cars' => 'http://www.arabsturbo.com/ixml/terms/syndigate/%D8%B3%D9%8A%D8%A7%D8%B1%D8%A7%D8%AA-%D9%87%D8%AC%D9%8A%D9%86%D8%A9', //
        'Latest_videos' => 'http://www.arabsturbo.com/ixml/terms/syndigate/%D9%81%D9%8A%D8%AF%D9%8A%D9%88/%D8%A3%D8%AD%D8%AF%D8%AB-%D8%A7%D9%84%D9%81%D9%8A%D8%AF%D9%8A%D9%88%D9%87%D8%A7%D8%AA', //
        'Car_accidents' => 'http://www.arabsturbo.com/ixml/terms/syndigate/%D8%AD%D9%88%D8%A7%D8%AF%D8%AB-%D8%B3%D9%8A%D8%A7%D8%B1%D8%A7%D8%AA', //
        'Moto_G_B' => 'http://www.arabsturbo.com/ixml/terms/syndigate/%D9%85%D9%88%D8%AA%D9%88-%D8%AC%D9%8A-%D8%A8%D9%8A', //
        'Formula' => 'http://www.arabsturbo.com/ixml/terms/syndigate/%D9%81%D9%88%D8%B1%D9%85%D9%88%D9%84%D8%A7', //
        'Racing_speed_rally' => 'http://www.arabsturbo.com/ixml/terms/syndigate/%D8%B3%D8%A8%D8%A7%D9%82%D8%A7%D8%AA-%D8%A7%D9%84%D8%B3%D8%B1%D8%B9%D8%A9-%D9%88-%D8%A7%D9%84%D8%B1%D8%A7%D9%84%D9%8A%D8%A7%D8%AA', //
        'Drvat' => 'http://www.arabsturbo.com/ixml/terms/syndigate/%D8%AF%D8%B1%D9%81%D8%AA', //
        'Videos' => 'http://www.arabsturbo.com/ixml/terms/syndigate/%D9%81%D9%8A%D8%AF%D9%8A%D9%88%D9%87%D8%A7%D8%AA'
    );

    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $this->setOpt();
      //grab URL and pass it to the browser
      if (!curl_exec($this->ch)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not Perform the cURL session of ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      } else {
        $this->errorMsg = 'Perform the cURL session';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));

        curl_close($this->ch);
        fclose($this->fp);
      }
    }
  }

}
