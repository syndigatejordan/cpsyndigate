<?php

class syndGatherContentHttpC2382 extends syndGatherContentHttp {

  protected $urls = array(
      'alkhaleej_1' => 'https://www.alkhaleej.ae/section/1110/rss.xml',
      'alkhaleej_2' => 'https://www.alkhaleej.ae/section/1111/rss.xml',
      'alkhaleej_3' => 'https://www.alkhaleej.ae/section/1129/rss.xml',
      'alkhaleej_4' => 'https://www.alkhaleej.ae/section/1275/rss.xml',
      'alkhaleej_5' => 'https://www.alkhaleej.ae/section/1116/rss.xml',
      'alkhaleej_6' => 'https://www.alkhaleej.ae/section/1140/rss.xml',
      'alkhaleej_7' => 'https://www.alkhaleej.ae/section/1177/rss.xml',
      'alkhaleej_8' => 'https://www.alkhaleej.ae/section/6556/rss.xml',
      'alkhaleej_9' => 'https://www.alkhaleej.ae/section/6557/rss.xml',
      'alkhaleej_10' => 'https://www.alkhaleej.ae/section/966/rss.xml',
      'alkhaleej_11' => 'https://www.alkhaleej.ae/section/1156/rss.xml',
      'alkhaleej_12' => 'https://www.alkhaleej.ae/section/1165/rss.xml',
      'alkhaleej_13' => 'https://www.alkhaleej.ae/section/1142/rss.xml',
      'alkhaleej_14' => 'https://www.alkhaleej.ae/section/1390/rss.xml',
      'alkhaleej_15' => 'https://www.alkhaleej.ae/section/6559/rss.xml',
      'alkhaleej_16' => 'https://www.alkhaleej.ae/section/991/rss.xml',
      'alkhaleej_17' => 'https://www.alkhaleej.ae/section/979/rss.xml',
      'alkhaleej_18' => 'https://www.alkhaleej.ae/section/1022/rss.xml',
      'alkhaleej_19' => 'https://www.alkhaleej.ae/section/1004/rss.xml',
      'alkhaleej_20' => 'https://www.alkhaleej.ae/section/993/rss.xml',
      'alkhaleej_21' => 'https://www.alkhaleej.ae/section/1173/rss.xml',
      'alkhaleej_22' => 'https://www.alkhaleej.ae/section/1288/rss.xml',
      'alkhaleej_23' => 'https://www.alkhaleej.ae/section/1334/rss.xml',
      'alkhaleej_24' => 'https://www.alkhaleej.ae/section/1335/rss.xml',
      'alkhaleej_25' => 'https://www.alkhaleej.ae/section/1105/rss.xml',
      'alkhaleej_26' => 'https://www.alkhaleej.ae/section/1177/rss.xml',
      'alkhaleej_27' => 'https://www.alkhaleej.ae/section/967/rss.xml',
      'alkhaleej_28' => 'https://www.alkhaleej.ae/section/1179/rss.xml',
      'alkhaleej_29' => 'https://www.alkhaleej.ae/section/1191/rss.xml',
      'alkhaleej_30' => 'https://www.alkhaleej.ae/section/1181/rss.xml',
      'alkhaleej_31' => 'https://www.alkhaleej.ae/section/1192/rss.xml',
      'alkhaleej_32' => 'https://www.alkhaleej.ae/section/1186/rss.xml',
      'alkhaleej_33' => 'https://www.alkhaleej.ae/section/1189/rss.xml',
      'alkhaleej_34' => 'https://www.alkhaleej.ae/section/1183/rss.xml',
      'alkhaleej_35' => 'https://www.alkhaleej.ae/section/968/rss.xml',
      'alkhaleej_36' => 'https://www.alkhaleej.ae/section/1195/rss.xml',
      'alkhaleej_37' => 'https://www.alkhaleej.ae/section/1196/rss.xml',
      'alkhaleej_38' => 'https://www.alkhaleej.ae/section/1197/rss.xml',
      'alkhaleej_39' => 'https://www.alkhaleej.ae/section/1312/rss.xml',
      'alkhaleej_40' => 'https://www.alkhaleej.ae/section/1357/rss.xml',
      'alkhaleej_41' => 'https://www.alkhaleej.ae/section/1358/rss.xml',
      'alkhaleej_42' => 'https://www.alkhaleej.ae/section/1344/rss.xml',
      'alkhaleej_43' => 'https://www.alkhaleej.ae/section/1270/rss.xml',
      'alkhaleej_44' => 'https://www.alkhaleej.ae/section/1213/rss.xml',
  );

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }

    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->urls as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );
      $ch = curl_init($subUrl);
      curl_setopt_array($ch, $options);
      $response = curl_exec($ch);

      curl_close($ch);
      $fileName = str_replace(' ', '_', $cat . '.xml');
      file_put_contents($fileName, $response);
    }
  }

}

?>
