<?php

class syndGatherContentHttpC1658 extends syndGatherContentHttp {

  protected function getRssUrl() {
    $urls = array(
        'Beauty_care' => 'http://www.layalinaonline.com/ixml/terms/syndigate/%D8%A7%D9%84%D8%B9%D9%86%D8%A7%D9%8A%D8%A9-%D8%A8%D8%A7%D9%84%D8%A8%D8%B4%D8%B1%D8%A9-%D9%88%D8%A7%D9%84%D8%AC%D9%85%D8%A7%D9%84',
        'Hair_care' => 'http://www.layalinaonline.com/ixml/terms/syndigate/%D9%86%D8%B5%D8%A7%D8%A6%D8%AD-%D8%A7%D9%84%D8%B4%D8%B9%D8%B1',
        'Beauty_and_makeup' => 'http://www.layalinaonline.com/ixml/albums/syndigate/%D8%AC%D9%85%D8%A7%D9%84-%D9%88%D9%85%D9%83%D9%8A%D8%A7%D8%AC',
        'Makeup' => 'http://www.layalinaonline.com/ixml/terms/syndigate/%D8%AA%D8%AC%D9%85%D9%8A%D9%84-%D9%88%D9%85%D9%83%D9%8A%D8%A7%D8%AC',
        'Health_and_fitness' => 'http://www.layalinaonline.com/ixml/terms/syndigate/%D8%B5%D8%AD%D8%A9-%D9%88%D8%B1%D8%B4%D8%A7%D9%82%D8%A9',
        'Pregnancy_and_motherhood' => 'http://www.layalinaonline.com/ixml/terms/syndigate/%D8%AD%D9%85%D9%84-%D9%88%D8%A3%D9%85%D9%88%D9%85%D8%A9',
        'Technology' => 'http://www.layalinaonline.com/ixml/terms/syndigate/%D8%AA%D9%83%D9%86%D9%88%D9%84%D9%88%D8%AC%D9%8A%D8%A7',
        'Decor_articles' => 'http://www.layalinaonline.com/ixml/terms/syndigate/%D8%AF%D9%8A%D9%83%D9%88%D8%B1',
        'Decor_albums' => 'http://www.layalinaonline.com/ixml/albums/syndigate/%D8%AF%D9%8A%D9%83%D9%88%D8%B1',
        'Relationships' => 'http://www.layalinaonline.com/ixml/terms/syndigate/%D8%AD%D8%A8-%D9%88%D8%B2%D9%88%D8%A7%D8%AC',
        'Interviews_with_Arab' => 'http://www.layalinaonline.com/ixml/interviews/syndigate/%D8%B9%D8%B1%D8%A8%D9%8A',
        'Interviews_with_International' => 'http://www.layalinaonline.com/ixml/interviews/syndigate/%D8%B9%D8%A7%D9%84%D9%85%D9%8A',
        'Cinema' => 'http://www.layalinaonline.com/ixml/terms/syndigate/%D8%A3%D9%81%D9%84%D8%A7%D9%85-%D9%88%D8%B3%D9%8A%D9%86%D9%85%D8%A7',
        'Cooking' => 'http://www.layalinaonline.com/ixml/terms/syndigate/%D9%88%D8%B5%D9%81%D8%A7%D8%AA-%D8%B7%D8%A8%D8%AE',
        'Tourism_articles' => 'http://www.layalinaonline.com/ixml/terms/syndigate/%D8%B3%D9%8A%D8%A7%D8%AD%D8%A9',
        'Tourism_albums' => 'http://www.layalinaonline.com/ixml/albums/syndigate/%D8%B3%D9%8A%D8%A7%D8%AD%D8%A9',
        'Celebrity_albums' => 'http://www.layalinaonline.com/ixml/albums/syndigate/%D9%85%D8%B4%D8%A7%D9%87%D9%8A%D8%B1',
        'Celebrity_news' => 'http://www.layalinaonline.com/ixml/terms/syndigate/%D9%85%D8%B4%D8%A7%D9%87%D9%8A%D8%B1',
        'Fashion' => 'http://www.layalinaonline.com/ixml/terms/syndigate/%D8%A3%D8%B2%D9%8A%D8%A7%D8%A1',
        'Red_carpet_articles' => 'http://www.layalinaonline.com/ixml/terms/syndigate/%D8%A7%D9%84%D8%B3%D8%AC%D8%A7%D8%AF%D8%A9-%D8%A7%D9%84%D8%AD%D9%85%D8%B1%D8%A7%D8%A1',
        'Red_carpet_albums' => 'http://www.layalinaonline.com/ixml/albums/syndigate/%D8%A7%D9%84%D8%B3%D8%AC%D8%A7%D8%AF%D8%A9-%D8%A7%D9%84%D8%AD%D9%85%D8%B1%D8%A7%D8%A1',
        'Shoes' => 'http://www.layalinaonline.com/ixml/terms/syndigate/%D8%A3%D8%AD%D8%B0%D9%8A%D8%A9',
        'Bags' => 'http://www.layalinaonline.com/ixml/terms/syndigate/%D8%AD%D9%82%D8%A7%D8%A6%D8%A8',
        'All_fashion_and_accessories_albums' => 'http://www.layalinaonline.com/ixml/albums/syndigate/%D8%A3%D8%B2%D9%8A%D8%A7%D8%A1-%D9%88%D8%A7%D9%83%D8%B3%D8%B3%D9%88%D8%A7%D8%B1%D8%A7%D8%AA',
        'Bridal_articles' => 'http://www.layalinaonline.com/ixml/terms/syndigate/%D8%B9%D8%B1%D9%88%D8%B3',
        'Bridal_albums' => 'http://www.layalinaonline.com/ixml/albums/syndigate/%D8%B9%D8%B1%D9%88%D8%B3',
        'Miscellaneous_albums' => 'http://www.layalinaonline.com/ixml/albums/syndigate/%D8%A7%D8%AE%D8%AA%D8%B1%D9%86%D8%A7-%D9%84%D9%83%D9%85',
        'Miscellaneous_articles' => 'http://www.layalinaonline.com/ixml/terms/syndigate/%D8%A7%D8%AE%D8%AA%D8%B1%D9%86%D8%A7-%D9%84%D9%83%D9%85'

    );

    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $this->setOpt();
      //grab URL and pass it to the browser
      if (!curl_exec($this->ch)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not Perform the cURL session of ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      } else {
        $this->errorMsg = 'Perform the cURL session';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));

        curl_close($this->ch);
        fclose($this->fp);
      }
    }
  }

}
