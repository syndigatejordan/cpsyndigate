<?php
/*
 * This Gather save html page for titel CountryWatch Political Intelligence Briefing id 1651
 * and carla proses articles in cms for titel CountryWatch Political Intelligence Briefing id 1651
 */
class syndGatherContentLocalC1656 extends syndGatherContentLocal {

  public $soapUrl = 'services.countrywatch.com';
  public $token = "CWPARTNER7GK0MSH5FUEP3X";

  public function __construct($titleId, $reportId = '') {
    $model = new syndModel();
    $homeDirPath = $model->getHomeDir($titleId);
    
    $file= $this->getCountryReviewSection('WOR', 'FLASH');
    $file=htmlspecialchars_decode($file);
    //var_dump($file);
    $Countryprofile = '<html id="feedHandler" xmlns="http://www.w3.org/1999/xhtml"><head></head><body>';
    $Countryprofile .=$file;
    $Countryprofile .="</body></html>";

    $Countryprofile = str_replace('<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body>', "", $Countryprofile);
    $Countryprofile = str_replace('</soap:Body></soap:Envelope>', "", $Countryprofile);
    $Countryprofile = str_replace('xmlns="http://tempuri.org/"', "", $Countryprofile);
    $Countryprofile = preg_replace('!\s+!', ' ', $Countryprofile);
    $Countryprofile = str_replace('<getCountryReviewSectionResult>', "", $Countryprofile);
    $Countryprofile = str_replace('<getCountryReviewSectionResponse >', "", $Countryprofile);
    $Countryprofile = str_replace('</getCountryReviewSectionResult>', "", $Countryprofile);
    $Countryprofile = str_replace('</getCountryReviewSectionResponse>', "", $Countryprofile);
    $this->saveNewsFile($Countryprofile, 'WOR', $homeDirPath);
    $this->MoveFromRootToDateDir($titleId);
    parent::__construct($titleId, $reportId);
  }

  public function getCountryReviewSection($countryISO, $sectionID) {
      $cURLConnection = curl_init();
      curl_setopt($cURLConnection, CURLOPT_URL, 'https://services.countrywatch.com/ws/cwwebservice.asmx/LoginToken?Token=CWPARTNER7GK0MSH5FUEP3X');
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => true, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );
      curl_setopt($cURLConnection, CURLOPT_COOKIEJAR, '/tmp/getCountryReviewSection.txt');
      curl_setopt($cURLConnection, CURLOPT_COOKIEFILE, '/tmp/getCountryReviewSection.txt');
      curl_setopt_array($cURLConnection, $options);
      $response = curl_exec($cURLConnection);
      curl_close($cURLConnection);
      // var_dump($response);	  


      $cURLConnection = curl_init();
      curl_setopt($cURLConnection, CURLOPT_URL, "https://services.countrywatch.com/ws/cwwebservice.asmx/getCountryReviewSection?Token=CWPARTNER7GK0MSH5FUEP3X&CountryISO=$countryISO&SectionID=$sectionID");
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => true, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );
      curl_setopt($cURLConnection, CURLOPT_COOKIEJAR, '/tmp/getCountryReviewSection.txt');
      curl_setopt($cURLConnection, CURLOPT_COOKIEFILE, '/tmp/getCountryReviewSection.txt');
      curl_setopt_array($cURLConnection, $options);
      $response = curl_exec($cURLConnection);
      curl_close($cURLConnection);
      return $response;
  }

  public function saveNewsFile($str, $ISO, $homeDirPath) {
    $file_name = $ISO . ".html";
    $file_name = $homeDirPath . DIRECTORY_SEPARATOR . $file_name;
    $fp = fopen($file_name, 'w');
    fwrite($fp, $str);
    fclose($fp);
  }

}
