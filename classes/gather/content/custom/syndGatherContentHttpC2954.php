<?php

class syndGatherContentHttpC2954 extends syndGatherContentHttp {

  protected function getRssUrl() {
    $urls = array(
        'fijisun' => 'https://fijisun.com.fj/feed/',
        'News' => 'https://fijisun.com.fj/category/news/feed',
        'Nation' => 'https://fijisun.com.fj/category/nation/feed',
        'Suncity' => 'https://fijisun.com.fj/category/suncity/feed',
        'Sunbiz' => 'https://fijisun.com.fj/category/sunbiz/feed',
        'Entertainment' => 'https://fijisun.com.fj/category/entertainment/feed',
        'Magazines' => 'https://fijisun.com.fj/category/magazines/feed',
        'Tourism' => 'https://fijisun.com.fj/category/tourism/feed',
        'World' => 'https://fijisun.com.fj/category/world/feed',
        'Sports' => 'https://fijisun.com.fj/category/sports/feed',
        'Videos' => 'https://fijisun.com.fj/category/videos-multimedia/feed',
        'Sunvoice' => 'https://fijisun.com.fj/category/sunvoice/feed',
        'Opinion' => 'https://fijisun.com.fj/category/opinion/feed',
        'Coconut Wireless' => 'https://fijisun.com.fj/category/coconut_wireless/feed',
        'Technology' => 'https://fijisun.com.fj/tag/technology-2/feed',
        'Lifestyle' => 'https://fijisun.com.fj/category/entertainment/lifestyle/feed',
        'Travel' => 'https://fijisun.com.fj/category/entertainment/travel/feed',
        'Letters' => 'https://fijisun.com.fj/category/letters/feed',
    );

    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );
      $ch = curl_init($subUrl);
      curl_setopt_array($ch, $options);
      $response = curl_exec($ch);

      curl_close($ch);
      $fileName = str_replace(' ', '_', $cat . '.xml');
      file_put_contents($fileName, $response);
    }
  }

}
