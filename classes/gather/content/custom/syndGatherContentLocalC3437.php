<?php

class syndGatherContentLocalC3437 extends syndGatherContentLocal {

  protected function getRssUrl() {
    $urls = array(
        'Video' => 'http://api.nilefm.com/api/v1/videolibraries/GetVideoLibrariesVideos',
    );

    return $urls;
  }

  public function __construct($titleId, $reportId = '') {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $subUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $response = curl_exec($ch);
      curl_close($ch);
      $contents = json_decode($response, true);
      $vides = array();
      foreach ($contents as $content) {
        foreach ($content["Videos"] as $Vid) {
          array_push($vides, $Vid);
        }
      }
      foreach ($vides as $vide) {
        $vide["link"] = "http://nilefm.com/video/{$vide["ID"]}/{$vide["Alias"]}";
        $vide["cat"] = $cat;
        $id = $vide["ID"];
        // creating object of SimpleXMLElement
        $xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');

        // function call to convert array to xml
        $this->array_to_xml($vide, $xml_data);
        $date = date('Y/m/d');
        $directory = "/syndigate/sources/home/1165/3437/$date";
        if (!is_dir($directory)) {
          mkdir($directory, 0777, true);
        }
        //saving generated xml file; 
        $result = $xml_data->asXML("$directory/article_$id.xml");
      }
    }
    parent::__construct($titleId, $reportId);
  }

  public function array_to_xml($item, &$xml_data) {
    foreach ($item as $key => $value) {
      if (is_numeric($key)) {
        $key = 'item' . $key; //dealing with <0/>..<n/> issues
      }
      if (is_array($value)) {
        $subnode = $xml_data->addChild($key);
        $this->array_to_xml($value, $subnode);
      } else {
        $xml_data->addChild("$key", htmlspecialchars("$value"));
      }
    }
  }

}
