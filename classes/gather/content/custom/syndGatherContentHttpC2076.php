<?php

/* Global Data Point */

class syndGatherContentHttpC2076 extends syndGatherContentHttp {

  protected $urls = array(
      'Bidsinfo' => 'https://www.bidsinfo.com/xml/news.xml',
  );

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }

    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    $username = 'albawaba';
    $password = 'gdpnews123';
    //$context = stream_context_create(array('http' => array('header' => "Authorization: Basic " . base64_encode("$username:$password"))));
    foreach ($this->urls as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      //$fileContent = file_get_contents($subUrl, false, $context);
/////////////////////////////////    
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $subUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      $fileContent = curl_exec($ch);
      curl_close($ch);
//////////////////////////////
      $xml = simplexml_load_string($fileContent);
      $json = json_encode($xml);
      $content = json_decode($json, TRUE);
      $file = ' <?xml version="1.0"?> ';
      foreach ($content as $element) {
        foreach ($element as $key => $Item) {
          $Publication = trim($Item["Publication"]);
          if ($Publication == "Media & Entertainment Monitor Worldwide" || $Publication == "Advertising, Marketing & Public Relations Monitor Worldwide" ||
                  $Publication == "Automotive Monitor Worldwide" || $Publication == "Education Monitor Worldwide" || $Publication == "Agriculture Monitor Worldwide" ||
                  $Publication == "Consumer Goods Monitor Worldwide" || $Publication == "Basic Materials & Resources Monitor Worldwide" || $Publication =="Transportation Monitor Worldwide"
                  || $Publication == "Pharma & Healthcare Monitor Worldwide" || $Publication == "Industrial Goods Monitor Worldwide" || $Publication == "Utilities Monitor Worldwide"
                  || $Publication == "Defence Monitor Worldwide" || $Publication == "Leisure & Hospitality Monitor Worldwide") {
            $file .=" <articles> ";
            $category = trim($Item["Industry"]);
            $file .=" <category> {$category} </category> ";
            $date = date('F d Y', strtotime($Item["Post_Date"]));
            $file .=" <date> {$date} </date> ";
            $hedline = trim($Item["Headline"]);
            $file .=" <hedline> <HL1> {$hedline} </HL1> </hedline> ";
            $body = trim($Item["Description"]);
            $file .=" <body> <p> {$body} </p> </body> ";
            $file .=" </articles> ";
            $ref_no = $Item["ref_no"];
            $Post_Date = $Item["Post_Date"];
            $check = new Criteria();
            $check->add(EmbinCustomPeer::ARTICLE_ID, intval($ref_no));
            $article = EmbinCustomPeer::doSelect($check);
            if (empty($article)) {
              $c = new EmbinCustom();
              $c->setArticleId($ref_no);
              $c->setPublisher($Publication);
              $c->setDate($Post_Date);
              $c->save();
            }
          }
        }
      }
      $fileName = 'Syndigate_' . date('d_m_Y_H_i_s') . '.xml';
      file_put_contents($fileName, $file);
      $this->moveFileToDateCustem($fileName);
    }
  }

  private function moveFileToDateCustem($fileName) {
    $dateDir = date('Y/m/d', strtotime('-2day'));
    $directory = '/syndigate/sources/home/185/538/' . $dateDir;

    if (!is_dir($directory)) {
      mkdir($directory, 0777, true);
    }
    $move_command = 'mv /syndigate/sources/home/50/2076/' . $fileName . ' ' . $directory . '/' . $fileName;
    echo $move_command . chr(10);
    shell_exec($move_command);
  }

}

?>