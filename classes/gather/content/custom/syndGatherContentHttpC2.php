<?php

class syndGatherContentHttpC2 extends syndGatherContentHttp {

  protected function getRssUrl() {
    $urls = array(
        'addustour_1' => 'https://www.addustour.com/temp_rss.php?categoryid=1',
        'addustour_2' => 'https://www.addustour.com/temp_rss.php?categoryid=2',
        'addustour_3' => 'https://www.addustour.com/temp_rss.php?categoryid=3',
        'addustour_4' => 'https://www.addustour.com/temp_rss.php?categoryid=4',
        'addustour_5' => 'https://www.addustour.com/temp_rss.php?categoryid=5',
        'addustour_6' => 'https://www.addustour.com/temp_rss.php?categoryid=6',
        'addustour_7' => 'https://www.addustour.com/temp_rss.php?categoryid=7',
        'addustour_8' => 'https://www.addustour.com/temp_rss.php?categoryid=8',
        'addustour_9' => 'https://www.addustour.com/temp_rss.php?categoryid=9',
        'addustour_10' => 'https://www.addustour.com/temp_rss.php?categoryid=10',
        'addustour_11' => 'https://www.addustour.com/temp_rss.php?categoryid=11',
        'addustour_12' => 'https://www.addustour.com/temp_rss.php?categoryid=12',
        'addustour_13' => 'https://www.addustour.com/temp_rss.php?categoryid=13',
        'addustour_14' => 'https://www.addustour.com/temp_rss.php?categoryid=14',
        'addustour_15' => 'https://www.addustour.com/temp_rss.php?categoryid=15',
        'addustour_16' => 'https://www.addustour.com/temp_rss.php?categoryid=16',
        'addustour_17' => 'https://www.addustour.com/temp_rss.php?categoryid=17',
        'addustour_18' => 'https://www.addustour.com/temp_rss.php?categoryid=18',
        'addustour_19' => 'https://www.addustour.com/temp_rss.php?categoryid=19',
        'addustour_20' => 'https://www.addustour.com/temp_rss.php?categoryid=20',
        'addustour_21' => 'https://www.addustour.com/temp_rss.php?categoryid=21',
        'addustour_22' => 'https://www.addustour.com/temp_rss.php?categoryid=22',
        'addustour_23' => 'https://www.addustour.com/temp_rss.php?categoryid=23',
        'addustour_24' => 'https://www.addustour.com/temp_rss.php?categoryid=24',
        'addustour_25' => 'https://www.addustour.com/temp_rss.php?categoryid=25',
    );

    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $subUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $response = curl_exec($ch);
      curl_close($ch);
      $fileName = str_replace(' ', '_', $cat . '.xml');
      file_put_contents($fileName, $response);
    }
  }

}
