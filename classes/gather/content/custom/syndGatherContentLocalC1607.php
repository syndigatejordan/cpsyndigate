<?php

class syndGatherContentLocalC1607 extends syndGatherContentLocal {

  public $soapUrl = 'services.countrywatch.com';
  public $token = "CWPARTNER7GK0MSH5FUEP3X";
  public $countryISO = array('Afghanistan' => 'AFG', 'Albania' => 'ALB', 'Algeria' => 'DZA', 'Andorra' => 'AND', 'Angola' => 'AGO', 'Antigua' => 'ATG', 'Argentina' => 'ARG', 'Armenia' => 'ARM', 'Australia' => 'AUS', 'Austria' => 'AUT', 'Azerbaijan' => 'AZE', 'Bahamas' => 'BHS', 'Bahrain' => 'BHR', 'Bangladesh' => 'BGD', 'Barbados' => 'BDS', 'Belarus' => 'BLR'
      , 'Belgium' => 'BEL', 'Belize' => 'BLZ', 'Benin' => 'BEN', 'Bhutan' => 'BTN', 'Bolivia' => 'BOL', 'Bosnia-Herzegovina' => 'BIH', 'Botswana' => 'BWA'
      , 'Brazil' => 'BRA', 'Brunei' => 'BRN', 'Bulgaria' => 'BGR', 'Burkina-Faso' => 'BFA', 'Burma' => 'MMR', 'Burundi' => 'BDI', 'Cambodia' => 'KHM'
      , 'Cameroon' => 'CMR', 'Canada' => 'CAN', 'Cape-Verde' => 'CPV', 'Central-African-Republic' => 'CAF', 'Chad' => 'TCD', 'Chile' => 'CHL', 'China' => 'CHN'
      , 'China-Hong-Kong' => 'HKG', 'China-Taiwan' => 'TWN', 'Colombia' => 'COL', 'Comoros' => 'COM', 'Congo-DRC' => 'COD', 'Congo-RC' => 'COG', 'Costa-Rica' => 'CRI'
      , 'Cote-d-Ivoire' => 'CIV', 'Croatia' => 'HRV', 'Cuba' => 'CUB', 'Cyprus' => 'CYP', 'Czech-Republic' => 'CZE', 'Denmark' => 'DNK', 'Djibouti' => 'DJI'
      , 'Dominica' => 'DMA', 'Dominican-Republic' => 'DOM', 'East-Timor' => 'TLS', 'Ecuador' => 'ECU', 'Egypt' => 'EGY', 'El-Salvador' => 'SLV', 'Equatorial-Guinea' => 'GNQ'
      , 'Eritrea' => 'ERI', 'Estonia' => 'EST', 'Ethiopia' => 'ETH', 'Fiji' => 'FJI', 'Finland' => 'FIN', 'Former-Yugoslav-Rep-of-Macedonia' => 'MKD', 'France' => 'FRA'
      , 'Gabon' => 'GAB', 'Gambia' => 'GMB', 'Georgia' => 'GEO', 'Germany' => 'DEU', 'Ghana' => 'GHA', 'Greece' => 'GRC', 'Grenada' => 'GRD', 'Guatemala' => 'GTM', 'Guinea' => 'GIN'
      , 'Guinea-Bissau' => 'GNB', 'Guyana' => 'GUY', 'Haiti' => 'HTI', 'Holy-See' => 'VAT', 'Honduras' => 'HND', 'Hungary' => 'HUN', 'Iceland' => 'ISL', 'India' => 'IND'
      , 'Indonesia' => 'IDN', 'Iran' => 'IRN', 'Iraq' => 'IRQ', 'Ireland' => 'IRL', 'Israel' => 'ISR', 'Italy' => 'ITA', 'Jamaica' => 'JAM', 'Japan' => 'JPN', 'Jordan' => 'JOR'
      , 'Kazakhstan' => 'KAZ', 'Kenya' => 'KEN', 'Kiribati' => 'KIR', 'Korea-North' => 'PRK', 'Korea-South' => 'KOR', 'Kuwait' => 'KWT', 'Kyrgyzstan' => 'KGZ', 'Laos' => 'LAO', 'Latvia' => 'LVA'
      , 'Lebanon' => 'LBN', 'Lesotho' => 'LSO', 'Liberia' => 'LBR', 'Libya' => 'LBY', 'Liechtenstein' => 'LIE', 'Lithuania' => 'LTU', 'Luxembourg' => 'LUX', 'Madagascar' => 'MDG'
      , 'Malawi' => 'MWI', 'Malaysia' => 'MYS', 'Maldives' => 'MDV', 'Mali' => 'MLI', 'Malta' => 'MLT', 'Malta' => 'MLT', 'Marshall-Islands' => 'MHL', 'Mauritania' => 'MRT', 'Mauritius' => 'MUS'
      , 'Mexico' => 'MEX', 'Micronesia' => 'FSM', 'Moldova' => 'MDA', 'Monaco' => 'MCO', 'Mongolia' => 'MNG', 'Morocco' => 'MAR', 'Mozambique' => 'MOZ', 'Namibia' => 'NAM', 'Nauru' => 'NRU'
      , 'Nepal' => 'NPL', 'Netherlands' => 'NLD', 'New-Zealand' => 'NZL', 'Nicaragua' => 'NIC', 'Niger' => 'NER', 'Nigeria' => 'NGA', 'Norway' => 'NOR', 'Oman' => 'OMN'
      , 'Pakistan' => 'PAK', 'Palau' => 'PLW', 'Panama' => 'PAN', 'Papua-New-Guinea' => 'PNG', 'Paraguay' => 'PRY', 'Peru' => 'PER', 'Philippines' => 'PHL', 'Poland' => 'POL'
      , 'Portugal' => 'PRT', 'Qatar' => 'QAT', 'Romania' => 'ROU', 'Russia' => 'RUS', 'Rwanda' => 'RWA', 'Saint-Kitts-and-Nevis' => 'KNA', 'Saint-Lucia' => 'LCA'
      , 'Saint-Vincent-and-Grenadines' => 'VCT', 'Samoa' => 'WSM', 'San-Marino' => 'SMR', 'Sao-Tome-and-Principe' => 'STP', 'Saudi-Arabia' => 'SAU', 'Senegal' => 'SEN'
      , 'Serbia' => 'SRB', 'Seychelles' => 'SYC', 'Sierra-Leone' => 'SLE', 'Singapore' => 'SGP', 'Slovakia' => 'SVK', 'Slovenia' => 'SVN', 'Solomon-Islands' => 'SLB'
      , 'Somalia' => 'SOM', 'South-Africa' => 'ZAF', 'Spain' => 'ESP', 'Sri-Lanka' => 'LKA', 'Sudan' => 'SDN', 'Suriname' => 'SUR', 'Swaziland' => 'SWZ', 'Sweden' => 'SWE'
      , 'Switzerland' => 'CHE', 'Syria' => 'SYR', 'Tajikistan' => 'TJK', 'Tanzania' => 'TZA', 'Thailand' => 'THA', 'Togo' => 'TGO', 'Tonga' => 'TON', 'Trinidad-Tobago' => 'TTO'
      , 'Tunisia' => 'TUN', 'Turkey' => 'TUR', 'Turkmenistan' => 'TKM', 'Tuvalu' => 'TUV', 'Uganda' => 'UGA', 'Ukraine' => 'UKR', 'United-Arab-Emirates' => 'ARE'
      , 'United-Kingdom' => 'GBR', 'United-States' => 'USA', 'Uruguay' => 'URY', 'Uzbekistan' => 'UZB', 'Vanuatu' => 'VUT', 'Venezuela' => 'VEN', 'Vietnam' => 'VNM', 'Yemen' => 'YEM', 'Zambia' => 'ZMB', 'Zimbabwe' => 'ZWE');

  public function __construct($titleId, $reportId = '') {
    $model = new syndModel();
    $homeDirPath = $model->getHomeDir($titleId);

    foreach ($this->countryISO as $iso) {
      ////Get News/////
      $InternationalNews = '<?xml version="1.0" encoding="utf-8"?><CountryWatch>';
      $WireTitles = $this->getCountryWireTitles($iso);
      preg_match_all('/<articleID>[^>]+<\/articleID>/i', $WireTitles, $articleid);
      foreach ($articleid as &$id) {
        $id = str_replace("<articleID>", "", $id);
        $id = str_replace("</articleID>", "", $id);
      }
      $articleid = $articleid[0];
      foreach ($articleid as $id) {
        $InternationalNews .='<ResponseArticle>' . $this->getWireArticle($id) . '</ResponseArticle>';
        $InternationalNews = str_replace('<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body>', "", $InternationalNews);
        $InternationalNews = str_replace('</soap:Body></soap:Envelope>', "", $InternationalNews);
        $InternationalNews = str_replace('<getWireArticleResponse xmlns="http://tempuri.org/">', "<getWireArticleResponse>", $InternationalNews);
        $InternationalNews = str_replace('</getWireArticleResult></getWireArticleResponse><getWireArticleResponse><getWireArticleResult>', "", $InternationalNews);
        $InternationalNews = str_replace('<getWireArticleResponse><getWireArticleResult>', "", $InternationalNews);
        $InternationalNews = str_replace('</getWireArticleResult></getWireArticleResponse>', "", $InternationalNews);
      }
      $InternationalNews .='</CountryWatch>';
      $name = $iso . "_InternationalNews";
      $InternationalNews = preg_replace('!\s+!', ' ', $InternationalNews);
      $this->saveNewsFile($InternationalNews, $name, $homeDirPath);

      $Countryprofile = '<?xml version="1.0" encoding="utf-8"?><CountryWatch>';
      //////////Get Politics////////////
 
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Politics,National Security</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'PONAS');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";

      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Politics,Defense Forces</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'PODEF');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";
      //////////Get Government////////////
     /* $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Government,Government Effectiveness</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'POGEF');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";*/

      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Government,Government Functions</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'POGSY');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";

      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Government,Government Structure</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'POGST');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";

      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Government,Principal Government Officials</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'POPGO');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";
      

            //////////Get Geography////////////
  /*    $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Geography,Geography</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryTemplate($iso, 5);
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";*/
      //////////Get History////////////
     /* $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>History,History</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'POHIS');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";*/
      
      /*
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Politics,Human Rights</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'POHRT');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";

      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Politics,Leader Biography</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'POBIO');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";

      */
      
      //////////Get Foreign Policy////////////
     /* $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Foreign Policy,Foreign Policy</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'POFOR');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";
      //////////Get Culture////////////
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Culture,Culture and Arts</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'CLART');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";*/
/*
      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Culture,Cultural Etiquette</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'CLETQ');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";*/
      //////////Get Economy////////////
     /* $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Economy,Economic Conditions</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'MAOVR');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";

      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Economy,Macroeconomic Forecast</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getDatatable($iso, 10);
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";*/
      //////////Get Business////////////
   /*   $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Business,Investment Climate</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'INFIC');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";

      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Business,Taxation</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getCountryReviewSection($iso, 'INTAX');
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";*/
      //////////Get Market Research////////////
    /*  $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Market Research,Macroeconomic Data</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getDatatable($iso, 9);
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";

      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Market Research,Energy Data</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getDatatable($iso, 6);
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";

      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Market Research,Metals Data</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getDatatable($iso, 8);
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";

      $Countryprofile .="<subCountryWatch>";
      $Countryprofile .="<category>Market Research,Agriculture Data</category>";
      $Countryprofile .="<text>";
      $Countryprofile .= $this->getDatatable($iso, 7);
      $Countryprofile .="</text>";
      $Countryprofile .="</subCountryWatch>";*/

      $Countryprofile .="</CountryWatch>";

      $Countryprofile = str_replace('<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body>', "", $Countryprofile);
      $Countryprofile = str_replace('</soap:Body></soap:Envelope>', "", $Countryprofile);
      $Countryprofile = str_replace('xmlns="http://tempuri.org/"', "", $Countryprofile);
      $Countryprofile = preg_replace('!\s+!', ' ', $Countryprofile);
      $Countryprofile = str_replace('<getDatatableResponse >', "", $Countryprofile);
      $Countryprofile = str_replace('<getDatatableResult>', "", $Countryprofile);
      $Countryprofile = str_replace('</getDatatableResult>', "", $Countryprofile);
      $Countryprofile = str_replace('</getDatatableResponse>', "", $Countryprofile);
      $Countryprofile = str_replace('<getCountryReviewSectionResult>', "", $Countryprofile);
      $Countryprofile = str_replace('<getCountryReviewSectionResponse >', "", $Countryprofile);
      $Countryprofile = str_replace('</getCountryReviewSectionResult>', "", $Countryprofile);
      $Countryprofile = str_replace('</getCountryReviewSectionResponse>', "", $Countryprofile);
      $Countryprofile = str_replace('<getCountryTemplateResponse >', "", $Countryprofile);
      $Countryprofile = str_replace('<getCountryTemplateResult>', "", $Countryprofile);
      $Countryprofile = str_replace('</getCountryTemplateResult>', "", $Countryprofile);
      $Countryprofile = str_replace('</getCountryTemplateResponse>', "", $Countryprofile);


      $this->saveNewsFile($Countryprofile, $iso, $homeDirPath);
    }
 
    parent::__construct($titleId, $reportId);
  }

  public function getCountryReviewSection($countryISO, $sectionID) {
    $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <getCountryReviewSection xmlns="http://tempuri.org/">
      <CountryISO>' . $countryISO . '</CountryISO>
      <SectionID>' . $sectionID . '</SectionID>
    </getCountryReviewSection>
  </soap:Body>
</soap:Envelope>';
    // data from the form, e.g. some ID number
    $headers = array("Content-type: text/xml;charset=\"utf-8\"", 'SOAPAction: "http://tempuri.org/getCountryReviewSection"', "Content-length: " . strlen($xml_post_string),);
    // PHP cURL  for https connection with auth
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_URL, $this->soapUrl . "/ws/cwwebservice.asmx");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
    // the SOAP request
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    // converting
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }

  public function getCountryWireTitles($countryISO) {
    $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <getCountryWireTitles xmlns="http://tempuri.org/">
      <CountryISO>' . $countryISO . '</CountryISO>
    </getCountryWireTitles>
  </soap:Body>
</soap:Envelope>';
    // data from the form, e.g. some ID number
    $headers = array("Content-type: text/xml;charset=\"utf-8\"", 'SOAPAction: "http://tempuri.org/getCountryWireTitles"', "Content-length: " . strlen($xml_post_string),);
    // PHP cURL  for https connection with auth
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_URL, $this->soapUrl . "/ws/cwwebservice.asmx");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
    // the SOAP request
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    // converting
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }

  public function getWireArticle($articleid) {
    $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <getWireArticle xmlns="http://tempuri.org/">
      <articleID>' . $articleid . '</articleID>
    </getWireArticle>
  </soap:Body>
</soap:Envelope>';
    // data from the form, e.g. some ID number
    $headers = array("Content-type: text/xml;charset=\"utf-8\"", 'SOAPAction: "http://tempuri.org/getWireArticle"', "Content-length: " . strlen($xml_post_string),);
    // PHP cURL  for https connection with auth
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_URL, $this->soapUrl . "/ws/cwwebservice.asmx");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
    // the SOAP request
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    // converting
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }

  public function getCountryTemplate($countryISO, $Tid) {
    $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <getCountryTemplate xmlns="http://tempuri.org/">
      <CountryISO>' . $countryISO . '</CountryISO>
      <TemplateID>' . $Tid . '</TemplateID>
    </getCountryTemplate>
  </soap:Body>
</soap:Envelope>';
    // data from the form, e.g. some ID number
    $headers = array("Content-type: text/xml;charset=\"utf-8\"", 'SOAPAction: "http://tempuri.org/getCountryTemplate"', "Content-length: " . strlen($xml_post_string),);
    // PHP cURL  for https connection with auth
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_URL, $this->soapUrl . "/ws/cwwebservice.asmx");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
    // the SOAP request
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    // converting
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }

  public function getDatatable($countryISO, $Tid) {
    $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <getDatatable xmlns="http://tempuri.org/">
      <CountryISO>' . $countryISO . '</CountryISO>
      <TableCategoryID>' . $Tid . '</TableCategoryID>
    </getDatatable>
  </soap:Body>
</soap:Envelope>';
    // data from the form, e.g. some ID number
    $headers = array("Content-type: text/xml;charset=\"utf-8\"", 'SOAPAction: "http://tempuri.org/getDatatable"', "Content-length: " . strlen($xml_post_string),);
    // PHP cURL  for https connection with auth
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_URL, $this->soapUrl . "/ws/cwwebservice.asmx");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
    // the SOAP request
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    // converting
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }

  public function saveNewsFile($str, $ISO, $homeDirPath) {
    $file_name = $ISO . ".xml";
    $file_name = $homeDirPath . DIRECTORY_SEPARATOR . $file_name;
    $fp = fopen($file_name, 'w');
    fwrite($fp, $str);
    fclose($fp);
  }

  public function getSoapData($function, $params) {
    $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
                            <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
							  <soap12:Body>
							    <' . $function . ' xmlns="http://tempuri.org/">
							      <usr>' . $this->soapUser . '</usr>
							      <pwd>' . $this->soapPassword . '</pwd>
							      ' . $params . '
							    </' . $function . '>
							  </soap12:Body>
							</soap12:Envelope>';

    // data from the form, e.g. some ID number
    $headers = array("Content-type: text/xml;charset=\"utf-8\"", 'SOAPAction: "http://tempuri.org/' . $function . '"', "Content-length: " . strlen($xml_post_string),);
    // PHP cURL  for https connection with auth
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_URL, $this->soapUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERPWD, $this->soapUser . ":" . $this->soapPassword);
    // username and password - declared at the top of the doc
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
    // the SOAP request
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    // converting
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }

}
