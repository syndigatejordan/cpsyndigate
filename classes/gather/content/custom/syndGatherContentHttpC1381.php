<?php

/*
 * MediAvataarME.com
 */

class syndGatherContentHttpC1381 extends syndGatherContentHttp {

  protected $urls = array(
      'Advertising' => 'http://www.mediavataarme.com/index.php/industry-news/advertising?format=feed',
      'Marketing' => 'http://www.mediavataarme.com/index.php/industry-news/marketing?format=feed',
      'Print' => 'http://www.mediavataarme.com/index.php/industry-news/print?format=feed',
      'Television' => 'http://www.mediavataarme.com/index.php/industry-news/television?format=feed',
      'Radio' => 'http://www.mediavataarme.com/index.php/industry-news/radio?format=feed',
      'Digital' => 'http://www.mediavataarme.com/index.php/industry-news/digital?format=feed',
      'Mobile' => 'http://www.mediavataarme.com/index.php/industry-news/mobile?format=feed',
      'OOH' => 'http://www.mediavataarme.com/index.php/industry-news/ooh?format=feed',
      'EXPERIMENTAL_MARKETING' => 'http://www.mediavataarme.com/index.php/industry-news/experiential-marketing?format=feed',
      'UAE_News' => 'http://www.mediavataarme.com/index.php/region-news/uae?format=feed',
      'Saudi_News' => 'http://www.mediavataarme.com/index.php/region-news/saudi?format=feed',
      'Oman_News' => 'http://www.mediavataarme.com/index.php/region-news/oman1?format=feed',
      'Qatar_News' => 'http://www.mediavataarme.com/index.php/region-news/qatar?format=feed',
      'Kuwait_News' => 'http://www.mediavataarme.com/index.php/region-news/kuwait?format=feed',
      'Bahrain_News' => 'http://www.mediavataarme.com/index.php/region-news/bahrain?format=feed',
      'Lebanon_News' => 'http://www.mediavataarme.com/index.php/region-news/lebanon?format=feed',
      'World_Roundup' => 'http://www.mediavataarme.com/index.php/world-roundup?format=feed',
      'FACE_2_FACE' => 'http://www.mediavataarme.com/index.php/interviews?format=feed',
      'Blogs' => 'http://www.mediavataarme.com/index.php/blogs?format=feed',
  );

  public function setOpt() {
    parent::setOpt();
    curl_setopt($this->ch, CURLOPT_ENCODING, 'text');
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }

    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->urls as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $subUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $response = curl_exec($ch);
      curl_close($ch);
      $fileName = str_replace(' ', '_', $cat . '.xml');
      file_put_contents($fileName, $response);
    }
  }

}

?>
