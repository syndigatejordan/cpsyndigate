<?php
class syndGatherContentLocalC23 extends syndGatherContentLocal {    
	
    public function __construct ($titleId, $reportId = '') {    	
    	$dirName 		= '/syndigate/sources/home/19/23/'.date('Y/m').'/';
  		$dirs   		= scandir($dirName); 	
	
		foreach ($dirs as $dir) {
			if($dir!="." && $dir!="..") {
				$date  = explode("-", $dir);
				$day   = $date[0];
				
				if(!is_dir($dirName.$day)){
					if(!is_dir($dirName.$day)) {
						mkdir($dirName.$day, 0777, true);				
					}	
				}
				
				$files = $this->directoryToArray($dirName.$dir, true);
				foreach ($files as $file) {
					if(is_file($file)) {
						$tmp   = explode('/', $file);
						$file2 = $dirName.$day.'/'.$tmp[count($tmp)-1];
						rename($file, $file2);	
					}
				}
				if(is_dir($dirName.$dir)) {
    				rmdir($dirName.$dir);
    			}	
    		}
		}
		
		parent::__construct($titleId, $reportId);
    }
   
    function directoryToArray($directory, $recursive) {
		$array_items = array();
		if ($handle = opendir($directory)) {
			while (false !== ($file = readdir($handle))) {
				if ($file != "." && $file != "..") {
					if (is_dir($directory. "/" . $file)) {
						if($recursive) {
							$array_items = array_merge($array_items, $this->directoryToArray($directory. "/" . $file, $recursive));
						}
						$file = $directory . "/" . $file;
						$array_items[] = preg_replace("/\/\//si", "/", $file);
					} else {
						$file = $directory . "/" . $file;
						$array_items[] = preg_replace("/\/\//si", "/", $file);
					}
				}
			}
			closedir($handle);
		}
		return $array_items;
    }
    
}