<?php

require_once 'Net/FTP.php';

class syndGatherContentFtpC6403 extends syndGatherContentFtp {

  /**
   * Constructor for the FTP class
   *
   * @param int $titleId
   */
  public function __construct($titleId, $reportId) {
    parent::__construct($titleId, $reportId);
  }

  public function copyContent() {
    $this->GetFilesFromFTP();
    $copyHomeToWork = new syndGatherContentLocal($this->titleId, $this->reportId);
    $copyHomeToWork->copyContent();
  }

  /**
   * Get the Newest Directories to copy
   *
   * @return array
   */
  public function GetFilesFromFTP() {
    // Getting all files
    $files = $this->ftp->ls($pwd . '/' . $checkDir, NET_FTP_FILES_ONLY);
    $date_dir = date('Y/m/d');
    $c = new Criteria();
    $c->add(TitlePeer::ID, $this->titleId);
    $currentTitle = TitlePeer::doSelectOne($c);
    $HomeDir = $currentTitle->getHomeDir();
    $title_dir = $HomeDir . $date_dir . DIRECTORY_SEPARATOR;
    $this->checkDirs = $title_dir;
    if (!is_dir($title_dir)) {
      mkdir($title_dir, 0777, true);
    }

    foreach ($files as $file) {
      $new_file_new = $this->nameToSafe($file['name']);
      // Copy the file from FTP server to Title home DIR
      if ($this->ftp->get($file['name'], $title_dir . $new_file_new, TRUE)) {
        echo "Copy file Successfully....." . chr(10);
        $this->ftp->_rmFile($file['name']);
      } else {
        echo "Fail copy files....." . chr(10);
      }
    }

    $this->ftp->disconnect();
    return true;
  }
}
