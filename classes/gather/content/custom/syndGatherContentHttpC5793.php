<?php

class syndGatherContentHttpC5793 extends syndGatherContentHttp {

  public $articles = array();

  protected function getRssUrl() {
    $offset = 0;
    $urls = array();
    //277
    for ($i = 1; $i < 5; $i++) {
      $urls["NewsCred Insights $i"] = "https://rbmbtnx.rbcp.at/api/v2/content/communication/C-GLOBAL?q=&limit=50&offset=$offset&f%5BmediaType%5D=Story&include=references&sort=-firstActivationTimestamp&fields%5BmediaType%5D=Video%7CPhoto%7CStory%7CCompilation%7CCollection&-fields%5Bprivate%5D=Yes";
      $offset+=50;
    }
    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);

      $response = $this->getContant($subUrl);
      $fileName = $this->homeDir . str_replace(' ', '_', $cat . '.xml');
      $contents = json_decode($response, true);
      $contents = $contents["items"];
      $data = array();
      foreach ($contents as &$item) {
        var_dump($item["id"]);
        $images = $this->getImages($item["id"]);
        $item["images"] = $images;
        $videos = $this->getVideos($item["id"]);
        $item["videos"] = $videos;
        $data[]["fullArticle"] = $item;
      }

      // creating object of SimpleXMLElement
      $xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');
      // function call to convert array to xml
      $this->array_to_xml($data, $xml_data);
      //saving generated xml file; 
      $result = $xml_data->asXML($fileName);
    }
  }

  public function getContant($subUrl) {
    // sleep(1);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $subUrl);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
    curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
    // This is what solved the issue (Accepting gzip encoding)
    curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }

  public function getImages($id) {
    //sleep(1);
    $subUrl = "https://rbmbtnx.rbcp.at/api/v2/content/communication/C-GLOBAL/$id/children?sort=-mediaType%2C-firstActivationTimestamp&limit=500&offset=0&include=references&fields[mediaType]=Photo|Story|Compilation|Collection";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $subUrl);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
    curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
    // This is what solved the issue (Accepting gzip encoding)
    curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
    $response = curl_exec($ch);
    curl_close($ch);
    $contents = json_decode($response, true);
    return $contents;
  }

  public function getVideos($id) {
    //sleep(1);
    $subUrl = "https://rbmbtnx.rbcp.at/api/v2/content/communication/C-GLOBAL/$id/children?sort=-mediaType%2C-firstActivationTimestamp&limit=500&offset=0&include=references&fields[mediaType]=Video";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $subUrl);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
    curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
    // This is what solved the issue (Accepting gzip encoding)
    curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
    $response = curl_exec($ch);
    curl_close($ch);
    $contents = json_decode($response, true);
    return $contents;
  }

  public function array_to_xml($item, &$xml_data) {
    foreach ($item as $key => $value) {
      if (is_numeric($key)) {
        $key = 'item';
      }
      if (is_array($value)) {
        $subnode = $xml_data->addChild($key);
        $this->array_to_xml($value, $subnode);
      } else {
        $xml_data->addChild("$key", htmlspecialchars("$value"));
      }
    }
  }

}
