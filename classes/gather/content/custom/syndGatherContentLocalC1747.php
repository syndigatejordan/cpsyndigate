<?php

class syndGatherContentLocalC1747 extends syndGatherContentLocal {

  public function __construct($titleId, $reportId = '') {
    //parent::__construct($titleId, $reportId);
    $custom_dirName = '/syndigate/sources/home/606/FlightInternational/';
    $title_dir = '/syndigate/sources/home/606/1747/';
    chdir($custom_dirName);
    $files = scandir($custom_dirName);
    foreach ($files as $file) {
      if ($file != "." && $file != ".." && is_file($file)) {
        $file = str_replace(' ', "\ ", $file);
        $file = str_replace('&', "\&", $file);
         $file = str_replace('$', "\\$", $file);
        $file = str_replace('(', "\(", $file);
        $file = str_replace(")", "\)", $file);
        $file = str_replace("'", "\'", $file);
        $file = str_replace(";", "\;", $file);
        $file = $custom_dirName . $file;
        $cond = "mv $file $title_dir";
        echo $cond.PHP_EOL;
        exec($cond);
      }
    }
    chdir("/usr/local/syndigate/classes/gather/content/custom");
    $this->MoveFromRootToDateDir($titleId);
    parent::__construct($titleId, $reportId);
  }

}
