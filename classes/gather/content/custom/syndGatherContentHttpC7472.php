<?php

class syndGatherContentHttpC7472 extends syndGatherContentHttp {

  protected function getRssUrl() {
    $urls = array(
        'Ecofin Agency' => 'https://www.ecofinagency.com/obrss/ecofin-agency-syndigate',
    );
    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
    } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

    public function copyContent()
    {
        foreach ($this->getRssUrl() as $cat => $subUrl) {
            $this->checkContent($subUrl, $cat);
            $opts = array();
            $http_headers = array();
            $http_headers[] = 'Expect:';

            $opts[CURLOPT_URL] = $subUrl;
            $opts[CURLOPT_HTTPHEADER] = $http_headers;
            $opts[CURLOPT_CONNECTTIMEOUT] = 10;
            $opts[CURLOPT_TIMEOUT] = 60;
            $opts[CURLOPT_HEADER] = FALSE;
            $opts[CURLOPT_BINARYTRANSFER] = TRUE;
            $opts[CURLOPT_VERBOSE] = FALSE;
            $opts[CURLOPT_SSL_VERIFYPEER] = FALSE;
            $opts[CURLOPT_SSL_VERIFYHOST] = 2;
            $opts[CURLOPT_RETURNTRANSFER] = TRUE;
            $opts[CURLOPT_FOLLOWLOCATION] = TRUE;
            $opts[CURLOPT_MAXREDIRS] = 2;
            $opts[CURLOPT_IPRESOLVE] = CURL_IPRESOLVE_V4;

            # Initialize PHP/CURL handle
            $ch = curl_init();
            curl_setopt_array($ch, $opts);
            $response = curl_exec($ch);

            # Close PHP/CURL handle
            curl_close($ch);
            $fileName = str_replace(' ', '_', $cat . '.xml');
            file_put_contents($fileName, $response);
        }
    }
}
