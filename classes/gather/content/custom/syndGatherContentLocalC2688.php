<?php

class syndGatherContentLocalC2688 extends syndGatherContentLocal {

  public function __construct($titleId, $reportId = '') {

    $c = new Criteria();
    $c->add(TitlePeer::ID, $titleId);
    $currentTitle = TitlePeer::doSelectOne($c);
    $currentDirName = $currentTitle->getHomeDir();

    $directories = scandir($currentDirName);
    foreach ($directories as $TGZFile) {
      $path = pathinfo($TGZFile);
      if (empty($path['extension'])) {
        continue;
      }
      $path_file = $path['extension'];
      if (($TGZFile != '.') && ($TGZFile != '..') && (is_file("{$currentDirName}{$TGZFile}")) && strtolower($path_file) == "zip") {
        $path["basename"] = preg_replace("/[  ~&,:\-\)\(\']/", ' ', $path["basename"]);
        $path["basename"] = str_replace('..', '.', $path["basename"]);
        $path["basename"] = str_replace("'", "\'", $path["basename"]);
        $path["basename"] = str_replace("\'", "", $path["basename"]);
        $path["basename"] = str_replace("`", "\`", $path["basename"]);
        $path["basename"] = str_replace("\`", "", $path["basename"]);
        $path["basename"] = str_replace("\$", "\\$", $path["basename"]);
        $path["basename"] = str_replace("\$", "", $path["basename"]);
        $path["basename"] = str_replace('  ', ' ', $path["basename"]);
        $path["basename"] = preg_replace('!\s+!', '_', $path["basename"]);
        $path["basename"] = str_replace('._', '_', $path["basename"]);
        $path["basename"] = str_replace(' _', '_', $path["basename"]);
        $TGZFile = str_replace(' ', '\ ', $TGZFile);
        $TGZFile = str_replace('(', '\(', $TGZFile);
        $TGZFile = str_replace(')', '\)', $TGZFile);
        $TGZFile = str_replace("'", "\'", $TGZFile);
        $TGZFile = str_replace("`", "\`", $TGZFile);
        $TGZFile = str_replace("&", "\&", $TGZFile);
        $TGZFile = str_replace("\$", "\\$", $TGZFile);
        $move = "mv " . $currentDirName . $TGZFile . " " . $currentDirName . $path["basename"];
        if ($TGZFile != $path["basename"]) {
          echo $move . chr(10);
          shell_exec($move);
          $TGZFile = $path["basename"];
        }

        $command = 'unzip ' . $currentDirName . $TGZFile . " -d " . $currentDirName;
        $remove_command = 'rm ' . $currentDirName . $TGZFile;
        $move_command = 'mv ' . $currentDirName . "split_files/*.xml " . $currentDirName;
        echo $command . chr(10);
        echo $move_command . chr(10);
        echo $remove_command . chr(10);
        if (shell_exec($command)) {
          shell_exec($move_command);
          shell_exec($remove_command);
        }
      }
    }
    $this->MoveFromRootToDateDir($titleId);
    parent::__construct($titleId, $reportId);
  }

}

?>
