<?php

class syndGatherContentLocalC5662 extends syndGatherContentLocal {

  public $articles = array();
  public $YouTubeDataAPIKey = "AIzaSyCJbgjR4DnTb27DY4mjsS-HqUkLqzbCkvM";
  public $channelId = "UCK9x-9FlwYSoEizYjcCyH2A";

  public function __construct($titleId, $reportId = '') {
    $playlists = $this->getplaylists();
    foreach ($playlists->items as $list) {
      $this->articles = array();
      $resp = $this->getplaylistItems($list->id);
      $page = 1;
      if ($resp) {
        echo ("Found data.") . PHP_EOL;
        if ($resp) {
          echo ("JSON decoded") . PHP_EOL;
          if (isset($resp->nextPageToken)) {
            $nextPageToken = $resp->nextPageToken;
          } else {
            $nextPageToken = null;
          }
          $items = $resp->items;
        } else {
          exit("Error. could not parse JSON." . json_last_error_msg());
        }

        $items = $this->objectToArray($items);
        array_push($this->articles, $items);

        // second level search using nextpage token
        while ($nextPageToken != null) {
          echo ("Fetching page " . ( ++$page) . " using pagetoken=" . $nextPageToken) . PHP_EOL;
          $resp = $this->getplaylistItems($list->id, $nextPageToken);
          $nextPageToken = null; // clear current value;
          if ($resp) {
            if ($resp) {
              if (isset($resp->nextPageToken)) {
                $nextPageToken = $resp->nextPageToken;
              } else {
                $nextPageToken = null;
              }
              $items = $resp->items;
            }
            $items = $this->objectToArray($items);
            array_push($this->articles, $items);
          } // if $resp
          // sometimes pagetoken is filled but no items are fetched , in such a case stop the code
          if (count($items) == 0)
            $nextPageToken = null;
        } // while $nextPageToken
        echo("Finished") . PHP_EOL;
      }
      $ids = array();
      foreach ($this->articles[0] as $article) {
        array_push($ids, $article["contentDetails"]["videoId"]);
      }
      $ids = implode(",", $ids);
      $contant = $this->getVideosList($ids);
      $contant = $this->objectToArray($contant);
      // creating object of SimpleXMLElement
      $xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');

      // function call to convert array to xml
      $this->array_to_xml($contant, $xml_data);
      $directory = "/syndigate/sources/home/1254/5662/";
      if (!is_dir($directory)) {
        mkdir($directory, 0777, true);
      }
      echo "saving generated xml file $directory/$list->id.xml" . PHP_EOL;
      //saving generated xml file; 
      $result = $xml_data->asXML("$directory/$list->id.xml");
    }

    parent::__construct($titleId, $reportId);
  }

  public function getplaylists() {
    sleep(1);
    // make api request
    $url = "https://www.googleapis.com/youtube/v3/playlists?key=$this->YouTubeDataAPIKey&channelId=$this->channelId&part=snippet,contentDetails&maxResults=20&pageToken=";
    echo PHP_EOL . $url . PHP_EOL;
    $options = array(
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // do not return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_USERAGENT => "spider", // who am i
        CURLOPT_AUTOREFERER => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
        CURLOPT_TIMEOUT => 120, // timeout on response
        CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
    );

    $ch = curl_init($url);
    curl_setopt_array($ch, $options);
    $resp = curl_exec($ch);
    curl_close($ch);
    $resp = json_decode($resp);
    return $resp;
  }

  public function getplaylistItems($id, $pageToken = "") {
    sleep(1);
    // make api request
    $url = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet,contentDetails&maxResults=10&playlistId=$id&key=$this->YouTubeDataAPIKey&pageToken=$pageToken";
    echo PHP_EOL . $url . PHP_EOL;
    $options = array(
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // do not return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_USERAGENT => "spider", // who am i
        CURLOPT_AUTOREFERER => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
        CURLOPT_TIMEOUT => 120, // timeout on response
        CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
    );

    $ch = curl_init($url);
    curl_setopt_array($ch, $options);
    $resp = curl_exec($ch);
    curl_close($ch);
    $resp = json_decode($resp);
    return $resp;
  }

  public function getVideosList($ids) {
    sleep(1);
    // make api request
    $url = "https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails,statistics&id=$ids&key=$this->YouTubeDataAPIKey";
    echo PHP_EOL . $url . PHP_EOL;
    $options = array(
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // do not return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_USERAGENT => "spider", // who am i
        CURLOPT_AUTOREFERER => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
        CURLOPT_TIMEOUT => 120, // timeout on response
        CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
    );

    $ch = curl_init($url);
    curl_setopt_array($ch, $options);
    $resp = curl_exec($ch);
    curl_close($ch);
    $resp = json_decode($resp);
    return $resp;
  }

  public function prossesItems($items) {

    foreach ($items as $item) {
      $videoId = "";
      if (!isset($item["contentDetails"]["videoId"])) {
        continue;
      }
      $videoId = $item["contentDetails"]["videoId"];
      if (!is_null($videoId)) {
        echo "Start video id $videoId" . PHP_EOL;
        $glob = "/usr/local/youtube/upload/$videoId.*";
        if (glob($glob)) {
          echo "The file exists" . PHP_EOL;
          continue;
        }
        $command = 'youtube-dl -f best -o /usr/local/youtube/"' . $videoId . '.%(ext)s" https://www.youtube.com/watch?v=' . $videoId;
        echo $command . PHP_EOL;
        exec($command);
        $file = glob("/usr/local/youtube/$videoId.*");
        $file = basename($file[0]);
        $this->uploadToAmazon($file);
      }
      // process each item
    } // foreach
  }

  public function objectToArray($data) {
    if (is_array($data) || is_object($data)) {
      $result = array();
      foreach ($data as $key => $value) {
        $result[$key] = $this->objectToArray($value);
      }
      return $result;
    }
    return $data;
  }

  public function array_to_xml($item, &$xml_data) {
    foreach ($item as $key => $value) {
      if (is_numeric($key)) {
        $key = 'item'; //dealing with <0/>..<n/> issues
      }
      if (is_array($value)) {
        $subnode = $xml_data->addChild($key);
        $this->array_to_xml($value, $subnode);
      } else {
        $xml_data->addChild("$key", htmlspecialchars("$value"));
      }
    }
  }

}
