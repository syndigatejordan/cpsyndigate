<?php

class syndGatherContentHttpC1735 extends syndGatherContentHttp {

  protected function getRssUrl() {
    $urls = array();
    $options = array(
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // do not return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_USERAGENT => "spider", // who am i
        CURLOPT_AUTOREFERER => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
        CURLOPT_TIMEOUT => 120, // timeout on response
        CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
    );
    $ch = curl_init("https://ameinfo.com/rssfeed/");
    curl_setopt_array($ch, $options);
    $file = curl_exec($ch);
    curl_close($ch);

    $file = preg_replace("!\s+!", " ", $file);
    $matches = NULL;
    preg_match_all('/<a href=(.*?)>(.*?)<\/a>/is', $file, $matches);
    foreach ($matches[0] as $matche) {
      $find = NULL;
      preg_match('/<a href="(.*?)"(.*?)>(.*?)<\/a>/is', $matche, $find);
      $find[3] = strip_tags($find[3]);
      if (!empty($find[3])) {
        $find[3] = html_entity_decode($find[3]);
        $find[3] = str_replace("&", "", $find[3]);
        $urls[$find[3]] = $find[1] . "feed/";
      }
    }
    $urls["AMEInfo55"] = "https://ameinfo.com/feed/";
    return $urls;
  }

  public function checkContent($subUrl = '', $cat = '', $check = false) {
    $this->check = $check;
    if ($check) {
      if (!$this->ch = curl_init($subUrl)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
        return false;
      } else {
        $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
        return true;
      }
    }
    $fileName = str_replace(' ', '_', $cat . '.xml');
    if (!$this->fp = fopen($fileName, "w")) {
      //Log the proccess for debugging
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $fileName;
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
      if (file_exists('.xml')) {
        unlink('.xml');
      }
    }
    if (!$this->ch = curl_init($subUrl)) {
      $this->processState = ezcLog::FAILED_AUDIT;
      $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $subUrl;
      $this->reportParams['LatestStatus'] = 0;
      $this->report->updateReport($this->reportParams);
      $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      return false;
    } else {
      $this->errorMsg = 'cURL session for ' . $subUrl . ' initialized ';
      $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));
    }
  }

  public function copyContent() {
    foreach ($this->getRssUrl() as $cat => $subUrl) {
      $this->checkContent($subUrl, $cat);
      $this->setOpt();
      //grab URL and pass it to the browser
      if (!curl_exec($this->ch)) {
        $this->processState = ezcLog::FAILED_AUDIT;
        $this->reportParams['Error'] = $this->errorMsg = 'Could not Perform the cURL session of ' . $subUrl;
        $this->reportParams['LatestStatus'] = 0;
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->errorMsg, ezcLog::ERROR, array('report_id' => $this->reportId));
      } else {
        $this->errorMsg = 'Perform the cURL session';
        $this->log->log($this->errorMsg, ezcLog::DEBUG, array('report_id' => $this->reportId));

        curl_close($this->ch);
        fclose($this->fp);
      }
    }
  }

}
