<?php

class syndGatherContentLocalC2074 extends syndGatherContentLocal {

  public function __construct($titleId, $reportId = '') {

    for ($i = 0; $i < 4; $i++) {
      $dateDir = date('Y/m/d', strtotime('-' . $i . 'day'));
      $directory = '/syndigate/sources/home/185/2074/' . $dateDir;
      echo $directory . chr(10);
      if (!is_dir($directory)) {
        mkdir($directory, 0777, true);
      }
      $currentDirName = '/syndigate/sources/home/185/538/' . $dateDir . DIRECTORY_SEPARATOR;

      $directories = scandir($currentDirName);
      foreach ($directories as $File) {
        if (($File != '.') && ($File != '..') && (is_file("{$currentDirName}{$File}")) && strpos($File, "Syndigate") === FALSE) {
          $copy_command = "cp -r " . $currentDirName . $File . " " . $directory . DIRECTORY_SEPARATOR;
          echo $copy_command . chr(10);
          shell_exec($copy_command);
        }
      }
    }
    $this->MoveFromRootToDateDir($titleId);
    parent::__construct($titleId, $reportId);
  }

//  public function moveFileToDateCustem($i, $dateDir) {
//    $directory = '/syndigate/sources/home/185/2074/' . $dateDir;
//    echo $directory . chr(10);
//    if (!is_dir($directory)) {
//      mkdir($directory, 0777, true);
//      return $directory;
//    }
//  }
}

?>