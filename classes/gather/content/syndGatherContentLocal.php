<?php
class syndGatherContentLocal extends syndGatherContent
{
    /**
     * File list for the home directory
     *
     * @var array
     */
    protected $homeArray = array();
    /**
     * File list of the directories in the homeArray folders
     *
     * @var array
     */
    protected $homeArrayFiles = array();
    /**
     * File list for the work directory
     *
     * @var array
     */
    protected $workArray = array();
    /**
     * File list of the directories in the workArray folders
     *
     * @var array
     */
    protected $workArrayFiles = array();
    /**
     * The name of the folder to work on
     *
     * @var string
     */
    protected $folder;
    /**
     * The path of the working dir of the title
     *
     * @var string
     */
    protected $workDirPath;
    /**
     * The path of the home dir of the title
     *
     * @var string
     */
    protected $homeDirPath;
    /**
     * An array thet holder the added and updated folder  and files.
     *
     * @var array
     */
    protected $svnAdd;
    /**
     * the Title name.
     *
     * @var string
     */
    protected $titleName;
    /**
     * an Object of the Model class
     *
     * @var object
     */
    protected $model;
    /**
     * Get the one and only instance of the ezLog class
     *
     * @var object
     */
    protected $log;
    /**
     * Get the one and only instance of the ezLog class
     *
     * @var object
     */
    protected $svn;
    /**
     * Check if the Title is new or not
     *
     * @var boolean
     */
    protected $firstTime;
    /**
     * The new added content
     *
     * @var array
     */
    protected $addedContent = array();
    /**
     * The Deleted content
     *
     * @var array
     */
    protected $deletedContent = array();
    /**
     * The not changed content to check if updated or not
     *
     * @var array
     */
    protected $notDeletedContent = array();
    /**
     * The status of the proccess
     *
     * @var string
     */
    protected $processState = ezcLog::SUCCESS_AUDIT;
    /**
     * The error msg to log it or sent it to the report class
     *
     * @var string
     */
    protected $errorMsg;
    /**
     * Store the report parameter
     *
     * @var array
     */
    protected $reportParams;
    /**
     * Get instance of the ezLog class
     *
     * @var object
     */
    protected $report;
    /**
     * The report ID for the gather_report table
     *
     * @var int
     */
    protected $reportId;
    /**
     * The Title connection type
     *
     * @var string
     */
    protected $titleType;

    /**
     * The Title ID
     *
     * @var string
     */
    protected $titleId;
    
    /**
     * Log params
     * @var  array 
     */
    protected $logParams;
    
    
    /**
     * Title object
     *
     * @var Object
     */
    protected $title;
    
    /**
     * Uniqe slug for the currect process
     *
     * @var unknown_type
     */
    protected$binTmpDir;
    
    /**
     * SVN revision number
     *
     * @var unknown_type
     */
    protected $revNo;
    protected $ModelMongo;

    
    /**
     *Function to Initiate the Class variables
     * 
     * @param 	array 	the new or updated folders
     * 
     * @return	void
     */
    public function __construct ($titleId, $reportId = '')
    {

    	
    	
    	
        //set the values of the home and the working copy directories		
        $this->model 			= new syndModel();
        $this->log 				= ezcLog::getInstance();
        $this->ModelMongo		= syndModelMongo::getInstance();
        $this->log->source 		= 'gather';
        $this->log->category 	= 'LOCAL';
       
        $this->report 			= new syndReport('gather');
        $this->titleName 		= $this->model->getTitleName($titleId);
  	    $this->titleId 			= $titleId;
  	    $this->title			= TitlePeer::retrieveByPK($titleId); 	    	    
  	    $this->binTmpDir		= APP_PATH . 'tmp/bin_' . $this->titleId . '_' . time() . '/';
  	    //mkdir($this->binTmpDir, 0777, true);
  	     	      	    

  	    $this->titleType 	= $this->model->getConnectionType($titleId);  	    
        
        $this->workDirPath 	= $this->model->getWorkDir($titleId);
        $this->homeDirPath 	= $this->model->getHomeDir($titleId);
        
        
        //initiate the report parameters
        $this->reportParams['TitleId'] 		= $titleId;
        $this->reportParams['NumberFiles'] 	= 0;
        $this->reportParams['LatestStatus'] = 0;
        $this->reportParams['FileSize'] 	= 0;
        $this->reportParams['Error'] 		= 'Starting gather contnt local';
        
        $reportId = ($reportId == '') ? $this->report->addReport($this->reportParams) : $reportId;
        $this->reportParams['Id'] 	= $reportId;
        $this->reportId 			= $reportId;
        $this->logParams 			= array('report_gather_id' => $this->reportId, 'source' => 'gather');
        $this->log->log($this->reportParams['Error'], Ezclog::DEBUG, $this->logParams);
       
        $this->svn 			= new syndGatherSvn($titleId, $reportId);
        
        // If the content were on the root directory and the type is local it will be a mistake
  	    // so we will reomve the content into the date directory path before adding them to the svn
  	    
  	    if( 'date' == $this->model->dirLayout($titleId) ) {
  	    	$this->MoveToDateDir($titleId);
  	    	$homeArray = syndGatherCheck::getNewContent($this->homeDirPath, $titleId);
  	    	$this->checkDays($homeArray);
  	    } elseif ( 'issues' == $this->model->dirLayout($titleId) ) {
  	    	$this->prepareIssueDir($this->homeDirPath);
  	    	$homeArray = syndGatherCheck::getNewContent($this->homeDirPath, $titleId);  	    	  	    	
  	    	$this->checkIssues($homeArray);
  	    }
  	      	    
        $this->homeArray = syndGatherCheck::getNewContent($this->homeDirPath, $titleId);        
        $this->workArray = syndGatherCheck::getNewContent($this->workDirPath, $titleId);
        $this->firstTime = $this->model->isTitleFirstGather($titleId);
        $this->firstTime = 0;
        
        
        $this->mylog("After check content");
        $this->mylog("Home array :"); $this->mylog($this->homeArray, 'print');
		$this->mylog("Work array :"); $this->mylog($this->workArray, 'print');
		
		
        $this->reportParams['Error'] 		= 'Start Copying the Content from the Home Directory to the WC Directory';
        $this->report->updateReport($this->reportParams);
        $this->log->log($this->reportParams['Error'], ezcLog::DEBUG, $this->logParams);
    }

    /**
     *Function to get the deleted files
     *@return 	array		the deleted files
     */
    public function deletedFiles ()
    {
        return array_diff($this->workArray, $this->homeArray);
    }
    /**
     *Function to get the added files
     *@return 	array		new files added
     */
    public function addedFiles ()
    {
        return array_diff($this->homeArray, $this->workArray);
    }
    /**
     *Function to get the file that is not deleted may be those files are updated so we need to copy them
     *@return 	array		files not deleted. that doesnt mean they are updated we have to check if updated or not
     */
    public function notDeleted ()
    {
        return array_intersect($this->homeArray, $this->workArray);
    }

    /**
     *Function to get a list of the updated files to copy them 
     *@return 	array		updated files
     */
    public function findRecursive ($path)
    {
		$files = array();
		foreach ( $this->notDeleted () as $file ) {
						
			if (is_dir($file) || is_file($file)) {
				//absolute path
				if (is_dir($file)) {
					foreach (ezcBaseFile::findRecursive ( $file, array (), array ('@.svn@' ) ) as $dirFile ) {
						$files [] = substr($dirFile, strlen($path));
					}
				}
				
				if (is_file($file)) {
					$files [] = substr($file, strlen($path));
				}
			
			} else {
				if (is_dir($path . $file)) {
					foreach (ezcBaseFile::findRecursive ( $path . $file, array (), array ('@.svn@' ) ) as $dirFile) {
						$files[] = substr($dirFile, strlen($path));
					}
				}


				if (is_file ($path . $file)) {
					$files[] = $file;
				}				
			}
			

		
		}
//var_dump($files);exit;
		return $files;

    }
    /**
     *Function to get a list of the updated files to copy them 
     *@return 	array		updated files
     */
    public function checkContent ()
    {
        $addedFiles = array();
        $deletedFiles = array();
        $notDeleted = array();

        $this->homeArrayFiles = $this->findRecursive($this->homeDirPath);
        $this->workArrayFiles = $this->findRecursive($this->workDirPath);

        $addedFiles 	= is_array(array_diff($this->homeArrayFiles, $this->workArrayFiles)) ? array_diff($this->homeArrayFiles, $this->workArrayFiles) : array();
        $deletedFiles 	= is_array(array_diff($this->workArrayFiles, $this->homeArrayFiles)) ? array_diff($this->workArrayFiles, $this->homeArrayFiles) : array();
        $notDeleted 	= is_array(array_intersect($this->workArrayFiles, $this->homeArrayFiles)) ? array_intersect($this->workArrayFiles, $this->homeArrayFiles) : array();

        $this->addedContent 		= array_merge($this->addedFiles(), $addedFiles);
        $this->deletedContent 		= $deletedFiles;
        $this->notDeletedContent 	= $notDeleted;

        //loop thought the notDeleted array and check if it is updated or not
        $files = array();
        foreach ($this->notDeletedContent as $var) {
            //check if the name of the working direcitory file is the same file in the home directory to compare							
			if (filesize($this->workDirPath . $var) != filesize($this->homeDirPath . $var) || filemtime($this->homeDirPath . $var) > filemtime($this->workDirPath . $var)) {
                $files[] = $var;
            }
        }
		
        $newContent = array_merge($files, $this->addedContent);
        if (! empty($newContent)) {
			return array_merge($files, $this->addedContent);
        } else {
            return false;
        }
    }
    
    /**
     *Function to copy the new and updated file from the home dir to the working dir
     *@return 	array		
     */
    public function copyContent ()
    {

	$checkDirs = $this->checkContent();
		if (is_array($checkDirs) && !$this->firstTime) {
            unset($this->svnAdd);
            $this->log->log('Copying the content for ' . $this->titleName, ezcLog::DEBUG, $this->logParams);
            foreach ($checkDirs as $file) {
            	
                if (is_dir($this->homeDirPath . $file)) {                	
                    chdir($this->workDirPath);
                    $fl = explode('/', $file);
                    array_pop($fl);
                    $path = $this->workDirPath;
                    foreach ($fl as $folder) {
                        if (is_dir($path . $folder)) {
                            if (chdir($path . $folder)) {
                                $path = $path . $folder . '/';
                                $this->svnAdd[] = $path;
                            }
                        } else {
							if (mkdir($path . '/' . $folder)) {
                                $path = $path . $folder . '/';
                                $this->svnAdd[] = $path;
                            }
                        }
                    }
                    
                    if(is_dir($this->homeDirPath . $file)) {
                    	$file.= '/';
                    }                    
                    ezcBaseFile::copyRecursive($this->homeDirPath . $file , $this->workDirPath . $file );
                    $this->svnAdd[] = $this->workDirPath . $file;
                } else {
					$tmp = explode("/", $file);
					$newDir = $this->workDirPath;
					for($count=0; $count<count($tmp)-1; $count++){
						$newDir .=  $tmp[$count] . "/";
						if(!is_dir($newDir)) {
							mkdir($newDir);
							$this->svnAdd[] = $newDir;
						}
			
					}


				/*$finfo = finfo_open(FILEINFO_MIME_TYPE);
				foreach (glob("*") as $filename) {
					if( strstr( finfo_file($finfo, $filename), "video/") ) {
						echo PHP_EOL . $filename . '  ' . finfo_file($finfo, $filename);
						
					}
				}
				finfo_close($finfo);
				echo PHP_EOL;
				exit;*/

				//$finfo = new finfo(FILEINFO_MIME_TYPE);
				//$MIMEType = $finfo->file( $this->homeDirPath . $file );
				$MIMEType = ezcBaseFile::getMimeContentType( $this->homeDirPath . $file );
				
				if( strstr( $MIMEType, "video") || strstr( $MIMEType, "audio") ) {
					$command = touch($this->workDirPath . $file);
				} else {
					$command = copy($this->homeDirPath . $file, $this->workDirPath . $file);
				}

				$this->log->log('coping ' . $this->homeDirPath . $file . ' to ' . $this->workDirPath . $file, ezcLog::DEBUG, $this->logParams);

					//if (copy($this->homeDirPath . $file, $this->workDirPath . $file)) {
					if ( $command ) {
                        $this->svnAdd[] = $this->workDirPath . $file;
                        $this->log->log('File ' . $file . ' copied successfully for Title: ' . $this->titleName, ezcLog::DEBUG, $this->logParams);
                    } else {
                        $this->errorMsg = 'File ' . $file . ' could not copied for Title: ' . $this->titleName;
                        $this->reportParams['TitleId'] = $this->titleId;
                        $this->reportParams['LatestStatus'] = 0;
                        $this->reportParams['Error'] = $this->errorMsg;
                        $this->report->updateReport($this->reportParams);
                        $this->log->log($this->errorMsg, ezcLog::ERROR, $this->logParams);
                    }
                }
            }
        } elseif ($this->firstTime) {
            foreach ($this->homeArray as $folder) {
                if (is_dir($this->homeDirPath . $folder)) {
                    $folderName = substr($folder, strlen($this->homeDirPath));
                    ezcBaseFile::copyRecursive($this->homeDirPath . $folder, $this->workDirPath . $folder);
                    $this->svnAdd[] = $this->workDirPath . $folder;
                } else {
                    if (copy($this->homeDirPath . $folder, $this->workDirPath . $folder)) {
                        $this->svnAdd[] = $this->workDirPath . $folder;
                        $this->log->log('File ' . $folder . ' copied successfully for Title: ' . $this->titleName, ezcLog::DEBUG, $this->logParams);
                    } else {
                        $this->errorMsg = 'File ' . $folder . ' could not copied for Title: ' . $this->titleName;
                        $this->reportParams['TitleId'] = $this->titleId;
                        $this->reportParams['LatestStatus'] = 0;
                        $this->reportParams['Error'] = $this->errorMsg;
                        $this->report->updateReport($this->reportParams);
                        $this->log->log($this->errorMsg, ezcLog::ERROR, $this->logParams);
                    }
                }
            }
        }
        
        
        //
        //Removing binary files from working copy
        //
	/*        
        foreach ($this->svnAdd as $location) {
        	if(is_dir($location)) {
        		$files = ezcBaseFile::findRecursive($location); 
        		foreach ($files as $f) {
        			$f = str_replace('//', '/', $f);
        			if($this->isBinaryFile($f)) {
        				unlink($f);
        			}        			
        		}
        	} else{        		
        		$loc = str_replace('//', '/', $location);
        		if($this->isBinaryFile($loc)) {
        			unlink($loc);
        		}
        	}
        }
	*/
       
        // Commiting and return the revision number
        $rev 		= $this->svn->svnCommit($this->svnAdd, $this->deletedContent, $this->workDirPath);  
        
        $parsingStateCriteria = new Criteria();
        $parsingStateCriteria->add(ParsingStatePeer::TITLE_ID, $this->title->getId());
        $parsingStateCriteria->add(ParsingStatePeer::REVISION_NUM , $rev);
        //$parsingStateCriteria->add(ParsingStatePeer::PID, 0);
        $parsingStateCriteria->addDescendingOrderByColumn(ParsingStatePeer::ID);
        $parsingState = ParsingStatePeer::doSelectOne($parsingStateCriteria);
        
        if(is_object($parsingState)) {
        	$parsingState->setReportGatherId($this->logParams['report_gather_id']);
        	$parsingState->save();
        	
        	//Also add the revision number to the report
        	$c = new Criteria();
        	$c->add(ReportPeer::TITLE_ID, $this->titleId);
        	$c->add(ReportPeer::REPORT_GATHER_ID, $this->logParams['report_gather_id']);
        	$report = ReportPeer::doSelectOne($c);
        	$report->setRevisionNum($rev);
        	$report->save();
    	}
              
        $binPath 	= '/syndigate/sources/binary/' . $this->title->getPublisherId() . '/' . $this->titleId . '/' . ((abs(crc32($rev)) % 1000)+1) . "/$rev";
        
        
        if($rev && is_dir($this->binTmpDir)) {
			if(!is_dir(dirname($binPath))) {
				mkdir(dirname($binPath), 0777, true);
			}
        	rename($this->binTmpDir, $binPath);
        }
        
		
    }
    
    /**
     *The Desructor
     */
    public function __destruct ()
    {
    
        if ($this->processState != ezcLog::FAILED_AUDIT) {
            $this->errorMsg = 'Finished Copying the Content from the Home Directory to the Working Dirctory.';
            $this->reportParams['LatestStatus'] = 1;
            $this->reportParams['Error'] = $this->errorMsg;
            $this->report->updateReport($this->reportParams);
        } else {
            $this->reportParams['LatestStatus'] = 0;
            $this->reportParams['Error'] = $this->errorMsg;
            $this->report->updateReport($this->reportParams);
        }
        $this->log->log($this->errorMsg, ezcLog::$this->processState, $this->logParams);
    }
    

    
    
    protected function getDirectoryFiles($dir)
	{
		$files = array();
		
		if('/' != substr($dir, strlen($dir)-1)) {
			$dir .= '/';
		}
			
		if ($handle = opendir($dir)) {
			while (false !== ($file = readdir($handle))) {
				
				if($file != '.' && $file!='..') {
					 $path = $dir . $file; 
					 rename($path, $dir . $this->nameToSafe($file));
					 $path = $dir . $this->nameToSafe($file);
					 
					 if(is_dir($path . '/')) {
					 	$path .= '/';
					 }
					 $files[] = $path;
				}
			}
			closedir($handle);
			return $files;
		}
	}
	
	
	protected function MoveToDateDir($titleId)
    {   	    
    	$firstYear      = 2000;    	    	
    	$homeDir 		= $this->model->getHomeDir($titleId);    	    	    	
    	$yearPath		= $homeDir . date('Y/');	
    	$monthPaht		= $homeDir . date('Y/m/');
    	

    	//Checking year path   	
    	$localContent 	= $this->getDirectoryFiles($homeDir);
    	$this->mylog("local content : "); $this->mylog($localContent, 'print');
    	foreach ($localContent as $content) {
    		if(is_file($content)) {
    			$this->mylog("file $content ");
    			$this->moveFileToProperDir($content);
    		} else {
    			$this->mylog("not file $content ");
    			if(!is_numeric(basename($content))) {
    				$this->mylog("not numeric " . basename($content));
    				$this->MoveDirToDate($content);
    			} else {    				
    				if( (basename($content)<$firstYear) || basename($content) > (int)date('Y', (time() + 259200)) ) { // more than 3 days in the next year
    					$this->mylog("more than 3 days ");
    					$this->MoveDirToDate($content);
    				}
    			}
    		}
    	}
    	
    	
    	if(is_dir($yearPath)) {    		
    		$this->mylog("$yearPath is a dir ");
    		$localContent = $this->getDirectoryFiles($yearPath);
    		$this->mylog("year content "); $this->mylog($localContent, 'print');
    		foreach ($localContent as $content) {
    			if(is_file($content)) {
    				"is file $content" . PHP_EOL;
    				$this->moveFileToProperDir($content);
    			} else {
    				"not file $content " . PHP_EOL;
    				if(!is_numeric(basename($content))) {
    					$this->mylog("not numeric $content ");
    					$this->MoveDirToDate($content);
    				} else {
    					if (basename($content) > (int)date('m') ) { // more than 3 days in the next month
    						$this->mylog("more than 3 days ");
    						$this->MoveDirToDate($content);
    					}
    				}
    			}
    		}
    	}
    	
    	if(is_dir($monthPaht)) {
    		$this->mylog("$monthPaht is a dir ");    		
    		$localContent = $this->getDirectoryFiles($monthPaht);
    		$this->mylog("month content "); $this->mylog($localContent, 'print');
    		
    		foreach ($localContent as $content) {
    			if(is_file($content)) {
    				$this->mylog("is file $content ");    				
    				$this->moveFileToProperDir($content);
    			} else {
    				if(!is_numeric(basename($content))) {
    					$this->mylog("is numeric " . basename($content));    					
    					$this->MoveDirToDate($content);
    				} else {
    					if (basename($content) > (int)date('d')) { // more than 3 days in the next month
    						$this->mylog("more than 3 days ");    						
    						$this->MoveDirToDate($content);
    					}
    				}
    			}
    		}
    	}   	
    	
    }
    
    
    protected function MoveDirToDate($dir, $toSubPath = '') {    	
    	$this->mylog("function move dir to date");
       	$contents 	= $this->getDirectoryFiles($dir);       	
       	$this->mylog("files of $dir "); $this->mylog($contents, 'print');
       	
       	$date 		= !$toSubPath ? date('Y/m/d/') : $toSubPath;

    	foreach ($contents as $content) {    		
    		if(is_file($content)) {
    			$this->mylog("is file $content");
    			$this->moveFileToProperDir($content, $date);    			
    		} else {
    			$this->mylog("not file $content");
    			$this->MoveDirToDate($content, $date);
    		}
    	}
    	
    	if($dir != $this->homeDirPath . date('Y/m/d/')) {
    		$this->mylog($dir  . ' != ' . $this->homeDirPath . date('Y/m/d/') . " removing $dir ");
    		rmdir($dir);   	
    	}
    	
    }
    
    
    /**
     * Move file from a directory to a date path 
     *
     * @param String $filePath : full file path
     * @param string $date : date that the file will be moved to format Y/m/d/
     */
    protected function moveFileToProperDir($filePath, $subPath = '') {    	
    	
    	$path 		= '';
    	$date 		= !$subPath ? date('Y/m/d/') : $subPath;    	
    	
    	if(!$this->isBinaryFile($filePath) ) {
    		$this->mylog("not binary $filePath ");
    		$path = $this->homeDirPath . $date;
    		$this->mylog("check path $path");    		
    		if(!is_dir($path)) {
    			$this->mylog("creating dir $path ");    			
    			mkdir($path, 0770, true);
    			$this->givePermissionsForDatePath($path);
    		}    		
    		
    	} else {
    		$this->mylog("binary file $filePath ");
    		$this->mylog("check path $path ");
    		
    		$path = $this->binTmpDir;    		
    		if(!is_dir($path)) {
    			$this->mylog("creating dir $path ");    			
    			mkdir($path, 0777, true);
    		}
    	}
    	
    	if($filePath != $path . basename($filePath)) {
    		$this->mylog(".moving $filePath to " . $path . basename($filePath));    		
    		rename($filePath, $path . basename($filePath));
    	} else {
    		$this->mylog("file is the same ... not thing to change");    		
    		// the file is the same do not do any thing
    	}
    }
    
    
    protected function checkDays($daysArray) {
    	$this->mylog("function check days ");
    	$this->mylog("checking the days ");
    	$this->mylog($daysArray, 'print');
    	
    	$fullPath = $this->title->getHomeDir();
    	
    	foreach ($daysArray as $day) {
    		$this->mylog("checking $day ");
    		$contents = $this->getDirectoryFiles($fullPath . $day);
    		$this->mylog("contents for day "); $this->mylog($contents, 'print');
    		foreach ($contents as $content) {    			
    			if(is_dir($content)) {    				
    				$this->mylog("is dir $content ");
    				$this->mylog("..moving $content to $day/");
    				$this->MoveDirToDate($content, $day .'/');
    			} else {
    				$this->mylog("is file $content ");
    				$this->mylog("...moving $content to $day/");
    				$this->moveFileToProperDir($content, $day .'/');
    			}
    		}    		
    	}
    }
    
    protected  function MoveFromRootToDateDir($titleId) {
    	//nothing just the subclass continue to working
    }
    
    protected function givePermissionsForDatePath($fullDatePath) {
    	
    	$titleName 		= $this->title->getFtpUsername();
    	$publisher  	= PublisherPeer::retrieveByPK($this->title->getPublisherId());
    	$publisherName  = $publisher->getFtpUsername();
    	
    	//date
    	chown($fullDatePath, $titleName);
    	chgrp($fullDatePath, $publisherName);
    	chmod($fullDatePath, 0770);    					
    			
    	//month
    	chown(dirname($fullDatePath), $titleName);
    	chgrp(dirname($fullDatePath), $publisherName);
    	chmod(dirname($fullDatePath), 0770);    					
    			
    	//year path
    	chown(dirname(dirname($fullDatePath)), $titleName);
    	chgrp(dirname(dirname($fullDatePath)), $publisherName);
    	chmod(dirname(dirname($fullDatePath)), 0770);    					
    }
    
    protected function moveFilesToBinDir($binaryFiles, $svnRevNo) {
    	foreach ($files as $file) {
    		if(is_file($file)) {
    			$pathToMove = $this->title->getPublisherId() . '/' . $this->title->getId() . '/' . ((abs(crc32($svnRevNo)) % 100)+1) . '/' . $svnRevNo . '/';
    			if(!is_dir($pathToMove)) {
    				mkdir($pathToMove, 0777, true);
    			}
    			rename($file, $pathToMove . basename($file));
    		}
    	}
    }
    
    protected function isBinaryFile($file) {
    	if(is_file($file)) {
    		$mimeType = strtolower(mime_content_type($file));
 			$this->mylog("checking file mime type for $file : " . $mimeType);
    		if(!$mimeType) {
    			return false;
    		} else {
    			
    			$accepted = array('text', 'xml', 'html', 'empty', 'txt');
    			foreach ($accepted as $pattern) {
    				$rez = '';
    				preg_match("/$pattern/i", $mimeType, $rez);
    				
    				if($rez) {
    					return false;    					
    				}
    			}
    			
    			//Some errors with mime types (i.e you got Audio instad of txt), so we also check the extension
    			$ext = syndParseHelper::getFileExtension($file);
    			$this->mylog("file extension : $ext");
    			if(in_array($ext, $accepted)) {
    				$this->mylog("extension in accepted xtensions");
    				return false;
    			}
    			
    			if(!is_dir($this->binTmpDir)) {
    				mkdir($this->binTmpDir, 0777, true);
    			}
    			return true;
    	}
    	return false;
    	}
    }
    
    protected function checkIssues($homeArray) {
    	
    	foreach ($homeArray as $folder) {
    		$subFolders = ezcBaseFile::findRecursive($this->homeDirPath . $folder);    		
    		foreach ($subFolders as $folder) {
    			if(is_file($folder) && ($this->isBinaryFile($folder))) {
    				$this->moveFileToProperDir($folder);
    			}
    		}
    	}
    }
    
    protected function prepareIssueDir($dir) {
    	$sources = scandir($dir);
    	natsort($sources);
    	$sources = array_reverse($sources);
    	
    	foreach ($sources as $source) {
    		if( (is_dir($dir . $source))) {
    			if(($source != '.') && ($source != '..')) {    				
    				$dirs[] = $dir . $source;    				
    			}
    		} else {
    			$files[] = $dir . $source;
    		}
    	}
    	
    	
    	if($files) {
    		if($dirs) {
    			$toDir = $dirs[0] . '/';
    		} else {
    			$toDir = $this->homeDirPath . '1/';
    		}
    		
    		foreach ($files as $file) {
    			if($this->isBinaryFile($file)) {
    				$this->moveFileToProperDir($file);
    			} else {
    				rename($file, $toDir . basename($file));
    			}
    		}
    	}
    }
    
    public function mylog($msg, $type = "echo") {
    	$canLog = true;
    	if($canLog) {
    		if($type == 'print') {
    			$this->log->log(print_r($msg, true), ezcLog::DEBUG, $this->logParams);
    		} else {
    			$this->log->log($msg, ezcLog::DEBUG, $this->logParams);
    		}
    	}
    	/*
    	if($type == 'print') {
    		print_r($msg);
    	} else {
    		echo $msg;
    	}
	*/
    	echo PHP_EOL;
    }
    
}
