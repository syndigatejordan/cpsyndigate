<?php
//include_once dirname(__FILE__) . '/../../../ez_autoload.php';
class syndGatherContentHttp extends syndGatherContent
{
    /**
     * cURL Session 
     *
     * @var resource the cURL session
     */
    protected $ch;
    /**
     * The file we need to copy the HTTP content to.
     *
     * @var resource	The file we need to copy the HTTP content to.
     */
    protected $fp;
    /**
     * The URL of the page to copy
     *
     * @var string	
     */
    protected $url;
    /**
     * The Title name
     * 
     * @var string
     */
    protected $title;
    /**
     * The Title connection type
     *
     * @var string
     */
    protected $titleType;
    /**
     * The Title name
     * 
     * @var string
     */
    protected $titleId;
    /**
     * The Home directory of the title
     *
     * @var string
     */
    protected $homeDir;
    /**
     * an Object of the Model class
     *
     * @var object
     */
    protected $model;
    /**
     * Get the one and only instance of the ezLog class
     *
     * @var object
     */
    protected $log;
    /**
     * Get the one and only instance of the syndAlert class
     *
     * @var object
     */
    protected $alert;
    /**
     * The status of the proccess
     *
     * @var string
     */
    protected $processState = ezcLog::SUCCESS_AUDIT;
    /**
     * The error msg to log it or sent it to the report class
     *
     * @var string
     */
    protected $errorMsg;
    /**
     * Store the report parameter
     *
     * @var array
     */
    protected $reportParams;
    /**
     *creat an Object from the syndReport class
     *
     * @var object
     */
    protected $report;
    /**
     * The report ID for the gather_report table
     *
     * @var int
     */
    protected $reportId;
    protected $check;
    /**
     * Funcion to initiate the curl and open the HTTP file to write on the new content
     * 
     * @param 	$url	The HTTP url to copy	
     */
    public function __construct ($titleId, $reportId)
    {


        //Initialize the log writer
        $this->log = ezcLog::getInstance();
        $this->alert = syndAlert::getInstance();
        $this->log->source = 'gather';
        $this->log->category = 'HTTP';
        $this->model = new syndModel();
        $this->report = new syndReport('gather');
        $this->url = $this->model->getUrl($titleId);
        $this->title = $this->model->getTitleName($titleId);
        $this->homeDir = $this->model->getHomeDir($titleId);
        $this->titleType = $this->model->getConnectionType($titleId);
        $this->titleId = $titleId;

        //Report Parameters
        $this->reportParams['TitleId'] = $titleId;
        $this->reportParams['NumberFiles'] = 0;
        $this->reportParams['LatestStatus'] = 0;
        $this->reportParams['FileSize'] = 0;
        $this->reportParams['Error'] = 'Start Gathering the URL';
        $this->reportId  = $this->reportParams['Id'] = $reportId;
        $this->logParams = array('report_gather_id' => $this->reportId, 'source' => 'gather');
        
        if (! chdir($this->homeDir)) {
            //Log the proccess for debugging
            $this->processState = ezcLog::FAILED_AUDIT;
            $this->reportParams['LatestStatus'] = 0;
            $this->reportParams['Error'] = $this->errorMsg = 'Could not change directory of ' . $this->title . ' home Directory, to ' . $this->homeDir;
            $this->report->updateReport($this->reportParams);
            $this->log->log($this->errorMsg, ezcLog::ERROR, $this->logParams);
        } else {
            $this->errorMsg = 'Home Directory of ' . $this->title . ' changed to ' . $this->homeDir;
            $this->log->log($this->errorMsg, ezcLog::DEBUG, $this->logParams);
        }
    }
    /**
     * Initialize a cURL session
     *
     */
    public function checkContent ($subUrl = '', $cat = '', $check = false)
    {
        $this->check = $check;
        if ($check) {
            if (! $this->ch = curl_init($this->url)) {
                $this->processState = ezcLog::FAILED_AUDIT;
                $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $this->url;
                $this->reportParams['LatestStatus'] = 0;
                $this->report->updateReport($this->reportParams);
                $this->log->log($this->errorMsg, ezcLog::ERROR, $this->logParams);
                return false;
            } else {
                $this->errorMsg = 'cURL session for ' . $this->url . ' initialized ';
                $this->log->log($this->errorMsg, ezcLog::DEBUG, $this->logParams);
                return true;
            }
        }
        try {
        	$filename = $this->nameToSafe($this->title . '.xml');

            if (! $this->fp = fopen($filename, "w")) {
                $this->errorMsg = 'Could not open the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $filename;
                throw new Exception($this->errorMsg);
            } else {
                $this->errorMsg = 'Opened the file to write the HTTP new content. Title Name: ' . $this->title . ' Filename: ' . $filename;
                $this->log->log($this->errorMsg, ezcLog::DEBUG, $this->logParams);
            }
        } catch (Exception $e) {
            //Log the proccess for debugging
            $this->processState = ezcLog::FAILED_AUDIT;
            $this->reportParams['Error'] = $this->errorMsg = $e->getMessage();
            $this->reportParams['LatestStatus'] = 0;
            $this->report->updateReport($this->reportParams);
            $this->log->log($this->errorMsg, ezcLog::ERROR, $this->logParams);
            return false;
        }
        if (! $this->ch = curl_init($this->url)) {
            $this->processState = ezcLog::FAILED_AUDIT;
            $this->reportParams['Error'] = $this->errorMsg = 'Could not initialize the cURL session for ' . $this->url;
            $this->reportParams['LatestStatus'] = 0;
            $this->report->updateReport($this->reportParams);
            $this->log->log($this->errorMsg, ezcLog::ERROR, $this->logParams);
            return false;
        } else {
            $this->errorMsg = 'cURL session for ' . $this->url . ' initialized ';
            $this->log->log($this->errorMsg, ezcLog::DEBUG, $this->logParams);
        }
    }
    /**
     * Set the needed options for cURL
     */
    public function setOpt ()
    {
        curl_setopt($this->ch, CURLOPT_FILE, $this->fp);
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
    }
    /**
     * Execute the cURL session
     */
    public function copyContent ()
    {
        $this->checkContent();
        $this->setOpt();
        //grab URL and pass it to the browser
        if (! curl_exec($this->ch)) {
            $this->processState = ezcLog::FAILED_AUDIT;
            $this->reportParams['Error'] = $this->errorMsg = 'Could not Perform the cURL session of ' . $this->url;
            $this->reportParams['LatestStatus'] = 0;
            $this->report->updateReport($this->reportParams);
            $this->log->log($this->errorMsg, ezcLog::ERROR, $this->logParams);
            $this->alert->send("Server is down.", syndAlert::CRITICAL, 'SERVER', $this->titleId);
            return false;
        } else {
            /*$this->reportParams['Error']	    = $this->errorMsg = 'Perform the cURL session of ' . $this->url;
			$this->reportParams['LatestStatus'] = 1;
			$this->report->updateReport($this->reportParams);*/
            $this->errorMsg = 'Perform the cURL session of ' . $this->url;
            $this->log->log('Perform the cURL session of ' . $this->url, ezcLog::DEBUG, $this->logParams);
	    curl_close($this->ch);
	    fclose($this->fp);

            return true;
        }
    }
    /**
     * Funcion to close the cURL session and the HTTP file
     */
    public function __destruct ()
    {
        if (! $this->check) {
            curl_close($this->ch);
            fclose($this->fp);
            //Save a new record in the report table
            if ($this->processState != ezcLog::FAILED_AUDIT) {
                //$this->errorMsg = 'Finshed copying the Content Successfully.';			
                //$this->reportParams['LatestStatus'] = 1;
                //$this->reportParams['Error'] = $this->errorMsg;
                //copy the new content to the working dirctory
                $this->reportParams['Error'] = $this->errorMsg = 'Finshed copying the Content Successfully, Started copying the Content localy from home to wc directories.';
                $this->reportParams['LatestStatus'] = 1;
                $this->report->updateReport($this->reportParams);
                $this->log->log($this->errorMsg, ezcLog::$this->processState, $this->logParams);
                $copyHomeToWork = new syndGatherContentLocalRss($this->titleId, $this->reportId);
                $copyHomeToWork->copyContent();
            } else {
                $this->reportParams['LatestStatus'] = 0;
                $this->reportParams['Error'] = $this->errorMsg;
                $this->report->updateReport($this->reportParams);
                $this->log->log($this->errorMsg, ezcLog::$this->processState, $this->logParams);
            }
        }
    }
}
