<?php
require_once 'Net/FTP.php';
class syndGatherContentFtp extends syndGatherContent
{
    /**
     * Set the host URL
     *
     * @var string
     */
    protected $ftpServer;
    /**
     * The host username
     *
     * @var string
     */
    protected $ftpUsername;
    /**
     * The host password
     *
     * @var string
     */
    protected $ftpUserPass;
    /**
     * The home directory
     *
     * @var sting
     */
    protected $homeDir;
    
    /**
     * The ftp port default 21
     *
     * @var integer
     */
    protected $ftpPort;
    
    /**
     * creat an Object from the Model class
     *
     * @var object
     */
    protected $model;
    /**

     * Check if the Title is new or not
     *
     * @var bool
     */
    protected $new;
    /**
     * Get the one and only instance of the ezcLog class
     *
     * @var object
     */
    protected $log;
    /**
     * Get the one and only instance of the syndAlert class
     *
     * @var object
     */
    protected $alert;
    /**
     * Object of the NET_FTP pear package
     *
     * @var object
     */
    protected $ftp;
    /**
     * The Title ID
     *
     * @var int
     */
    protected $titleId;
    /**
     *creat an Object from the syndReport class
     *
     * @var object
     */
    protected $report;
    /**
     * The report ID for the gather_report table
     *
     * @var int
     */
    protected $reportId;
    /**
     * Store the report parameter
     *
     * @var array
     */
    protected $reportParams;
    protected $dirLayout;
    
    protected $logParams;
    
    /**
     * Constructor for the FTP class
     *
     * @param int $titleId
     */
    public function __construct ($titleId, $reportId)
    {
		$this->log = ezcLog::getInstance();
        $this->alert = syndAlert::getInstance();
        $this->log->source = 'gather';
        $this->log->category = 'FTP';
        $this->model = new syndModel();
        $this->ftp = new Net_FTP();
        $this->report = new syndReport('gather');
        $this->ftpServer = $this->model->getUrl($titleId);
        $this->ftpUserPass = $this->model->getPassword($titleId);
        $this->ftpUsername = $this->model->getUsername($titleId);
        $this->dirLayout = $this->model->dirLayout($titleId);
        $this->ftpPort  = $this->model->getFtpPort($titleId);
        $this->homeDir = $this->model->getHomeDir($titleId);
        $this->new = $this->model->isTitleFirstGather($titleId);
        $this->titleId = $titleId;
        $this->reportId = $reportId;
        $this->reportParams['Id'] = $reportId;
        $this->reportParams['TitleId'] = $titleId;
        
        $this->logParams = array('source' => 'gather', 'category' => 'FTP', 'report_gather_id' => $this->reportId);
        
        
        $this->login();
    }
    /**
     * Connect to the Title's FTP server
     *
     */
    public function connect ()
    {
        //Note:  FTP URL MOST NOT HAVE [ FTP:// ] prefix		
        $connect = $this->ftp->connect($this->ftpServer, $this->ftpPort);
        if (PEAR::isError($connect)) {
            $this->reportParams['LatestStatus'] = 0;
            $this->reportParams['Error'] = $connect->getMessage() . ' for: ' . $this->ftpServer;
            $this->report->updateReport($this->reportParams);
            $this->log->log($connect->getMessage() . ' for: ' . $this->ftpServer, ezcLog::FATAL, $this->logParams);
            $this->alert->send("Server is down.", syndAlert::CRITICAL, 'SERVER', $this->titleId);
        } else {
            $this->log->log('FTP connection to ' . $this->ftpServer . ' Succeeded', ezcLog::DEBUG, $this->logParams);
        }
    }
    /**
     * Login to the Title's FTP server
     *
     */
    public function login ()
    {
        $this->connect();
        $login = $this->ftp->login($this->ftpUsername, $this->ftpUserPass);
        if (PEAR::isError($login)) {
            $this->reportParams['LatestStatus'] = 0;
            $this->reportParams['Error'] = $login->getMessage() . ' for: ' . $this->ftpServer;
            $this->report->updateReport($this->reportParams);
            $this->log->log($login->getMessage() . ' for: ' . $this->ftpServer, ezcLog::FATAL, $this->logParams);
            $this->alert->send("Server is down.", syndAlert::CRITICAL, 'SERVER', $this->titleId);
        } else {
            $this->log->log('FTP login to ' . $this->ftpServer . ' Succeeded', ezcLog::DEBUG, $this->logParams);
        }
    }
    /**
     * List the given FTP directory
     *
     * @param string $dir
     * @return array
     */
    public function ls ($dir)
    {
        $folders = $this->ftp->ls($dir);
        if (PEAR::isError($folders)) {
            $this->reportParams['LatestStatus'] = 0;
            $this->reportParams['Error'] = $folders->getMessage() . ' for: ' . $this->ftpServer;
            $this->report->updateReport($this->reportParams);
            $this->log->log($folders->getMessage() . ' for: ' . $this->ftpUsername, ezcLog::FATAL, $this->logParams);
            return false;
        } else {
            $this->log->log('FTP Listing for ' . $this->ftpUsername . ' Succeeded.', ezcLog::DEBUG, $this->logParams);
            return $folders;
        }
    }
    /**
     * Get the Newest Directories to copy
     *
     * @return array
     */
    public function checkContent ()
    {
        $dirs = array();
        $folders = $this->ls('.');

        if ($folders) {
            if ($this->dirLayout == 'root') {
				foreach ($folders as $folder) {
			    	$dirs[] = $folder['name'];
				}
            } else {
                foreach ($folders as $folder) {
                    $dirs[$folder['stamp']] = $folder['name'] . '/';
                }

                if (!$this->new) {
                    krsort($dirs);
                    $dirs = array_slice($dirs, 0, 1);
                }
            }

            $this->log->log('Getting new directories for ' . $this->ftpServer . ' Succeeded', ezcLog::DEBUG, $this->logParams);
            return $dirs;
        } else {
            $this->reportParams['LatestStatus'] = 0;
            $this->reportParams['Error'] = 'Getting new directories for ' . $this->ftpServer . ' Failed';
            $this->report->updateReport($this->reportParams);
            $this->log->log('Getting new directories for ' . $this->ftpServer . ' Failed', ezcLog::FATAL, $this->logParams);
            return false;
        }
    }
    
    /**
     * Copy the new content to the home directory of the title
     *
     */
    public function copyContent ()
    {
    	$checkContentDirs = $this->checkContent();
		
        if ($checkContentDirs) {
            foreach ($checkContentDirs as $folder) {
                $this->log->log('Started copying Directory ' . $folder . ' from  ' . $this->ftpServer . ' title.', ezcLog::DEBUG, $this->logParams);
				if($this->dirLayout == 'root'){
					$currDir = getcwd();
					chdir($this->homeDir);
					foreach ($checkContentDirs as $folder) {
						$download = $this->ftp->get($folder, $folder, true, FTP_ASCII);
					}
				} else {
	                $download = $this->ftp->getRecursive($folder, $this->homeDir . $folder, true);
			
				}
                if (PEAR::isError($download)) {
                    $this->reportParams['LatestStatus'] = 0;
                    $this->reportParams['Error'] = $download->getMessage() . $folder . ' from ' . $this->ftpServer . ' title. Failed.';
                    $this->report->updateReport($this->reportParams);
                    $this->log->log($download->getMessage() . $folder . ' from ' . $this->ftpServer . ' title. Failed.', ezcLog::FATAL, $this->logParams);
                    return false;
                } else {
                    $this->log->log('Copying Directory ' . $folder . ' from ' . $this->ftpServer . ' title. Succeeded.', ezcLog::DEBUG, $this->logParams);
		    		//must go back to the original DIR so it won't confuse absolute and relative paths
		    		chdir($currDir);

                    //copy the new content to the working dirctory
                    $copyHomeToWork = new syndGatherContentLocal($this->titleId, $this->reportId);
                    $copyHomeToWork->copyContent();
                }
            }
        } else {
            $this->log->log('No content found from ' . $this->ftpServer . ' title.', ezcLog::WARNING, $this->logParams);
            return false;
        }
    }
    /**
     * Distroy the object after the proccess finished
     *
     */
    public function __destruct ()
    {
        $this->log->log('Closing FTP Connection for ' . $this->ftpServer, ezcLog::DEBUG, $this->logParams);
        $this->ftp->disconnect();
    }
}
