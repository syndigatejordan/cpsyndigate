<?php
class syndGatherContentLocalRss extends syndGatherContentLocal
{
    public function checkContent ()
    {
        $addedFiles = parent::addedFiles();
        $deletedFiles = parent::deletedFiles();
        $notDeleted = parent::notDeleted();
        $files = array();
        foreach ($notDeleted as $var) {
            //check if the name of the working directory file is the same file in the home directory to compare							
            if (filesize($this->workDirPath . $var) != filesize($this->homeDirPath . $var) || filemtime($this->homeDirPath . $var) > filemtime($this->workDirPath . $var)) {
                $files[] = $var;
            }
        }
		$newContent = array_merge($files, $addedFiles);
        if (! empty($newContent)) {
            return array_merge($files, $addedFiles);
        } else {
            return false;
        }
    }
}
?>
