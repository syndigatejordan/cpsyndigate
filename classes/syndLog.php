<?php

	/**
	 * This is the main logging class in the system. Mainly used for CLI
	 *
	 * It's just a customization for ezc EventLog. What it does is that it logs
	 * log messages in different DB tables & files based on the source.
	 * 
	 * Example:
	 * 
	 * <code>
	 *	// Use log
	 *	$log = ezcLog::getInstance();
	 *	$log->log('test', ezcLog::DEBUG, array('source' => 'gather', 'category' => 'DB', 'report_id' => 5) );
	 * </code>
	 * 
	 * For more info: http://ezcomponents.org/docs/tutorials/EventLog
	 * 
	 * @todo Refactor writers, there must be a better way to do this
	 */
	class syndLog extends ezcLogDatabaseWriter implements ezcBaseConfigurationInitializer {

		/**
		 * Log dir root
		 *
		 * @var string
		 */
		public static $logDir = '/var/log/syndigate/';
		
		/**
		 * The file that will log all messages 
		 * if a source is not configured to log to a table or file
		 *
		 * @var string
		 */
		public static $catchAllFile = 'catch_all.log'; 
		
		/**
		 * Mapping of sources to DB tables
		 * key: source
		 * val: table name
		 *
		 * @var array
		 */
		public static $dbSources = array(
			//'gather' => 'log_gather',
			//'parse' => 'log_parse',
		);
		
		/**
		 * Mapping of sources to log files
		 * key: source
		 * val: file name
		 *
		 * @var array
		 */		
		public static $fileSources = array(
			'vcrond' => 'vcrond.log',
			'send' => 'send.log',
			'gather' => 'gather.log',
			'parse' => 'parse.log',
			'generate' => 'generate.log',
			'shepherd' => 'shepherd.log'
		);
		
		
		public static function configureObject($log) {			
			
			//get DB conf from Propel
			$propelConf = Propel::getConfiguration();
			$dbConf = $propelConf['datasources']['propel']['connection'];
			$dsn = sprintf('%s://%s:%s@%s/%s', $dbConf['phptype'], $dbConf['username'], $dbConf['password'], $dbConf['hostspec'], $dbConf['database']);
			
			//connection to Database
			//$db = ezcDbFactory::create($dsn);
  			
			//DB loggers
			if(isset($db)) {
				foreach (self::$dbSources as $source => $table) {
	
					//create a writer
					$writer = new ezcLogDatabaseWriter($db, $table);
					$filter = new ezcLogFilter;
					
					$filter->source = array($source);
					
					$log->getMapper()->appendRule(new ezcLogFilterRule($filter, $writer, false));
				}
			}
			
			
			//File loggers
			foreach (self::$fileSources as $source => $file) {

				//create a writer
				$writer = new ezcLogUnixFileWriter(self::$logDir, $file);
				$filter = new ezcLogFilter;
				
				$filter->source = array($source);
				
				$log->getMapper()->appendRule(new ezcLogFilterRule($filter, $writer, false));
			}		
			
			//Catch all log file
			$writeAll = new ezcLogUnixFileWriter(self::$logDir, self::$catchAllFile);
			$log->getMapper()->appendRule( new ezcLogFilterRule( new ezcLogFilter, $writeAll, false ) );
		}
		
	}
  
