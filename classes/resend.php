<?php
    require_once dirname(__FILE__) . '/../ez_autoload.php';
    require_once APP_PATH . 'classes/syndReGenerate.php'; 
    $model 			= syndModel::getModelInstance();    
    
    $titlesIds  	= explode(',', trim($argv[1])); // ex. 1,2,2
    $clientsIds 	= explode(',', trim($argv[2])); // ex. 1,25,21
    $fromTimestamp      = !isset($argv[3]) ? strtotime(date('Y-m-d')) : strtotime($argv[3]); //ex. '2009-10-12'
    $toTimestamp        = !isset($argv[4]) ? strtotime(date('Y-m-d')) : strtotime($argv[4]); //ex. '2009-10-12' could be the same day
    if(!empty($argv[5])){
     $article_ids  = explode(',', trim($argv[5]));
    }else{
      $article_ids = null;
    }
    // Fixing timestamps to add the end of the day minutes and secounds)
    $fromDateTime = date('Y-m-d H:i:s', $fromTimestamp);
    $toDateTime   = date('Y-m-d H:i:s', strtotime(date('Y-m-d', $toTimestamp))+ 86399 );
   
    
    if(!$titlesIds || !$clientsIds) {
    	die('Invalid titles or clients ids group' . PHP_EOL);
    }
    
    if( substr($fromDateTime, 0, 10) == '1970-01-01' || substr($toDateTime, 0, 10) == '1970-01-01') {
    	die('Invalid date range' . PHP_EOL);
    }
        
    echo  "Generating articles from $fromDateTime to $toDateTime " . PHP_EOL;
    
    
    $generate = new syndReGenerate();
    $generate->generate($titlesIds, $clientsIds, $fromDateTime, $toDateTime,$article_ids);
  
    unset($generate);
    

    /*
    foreach ($pushClients as $pclient) {

        $push = null;
    	$customeClientSenderClass = 'syndSend_' . $pclient->getId();
    	$clientCustomeSenderFile  = dirname(__FILE__) . "/send/custom/{$customeClientSenderClass}.php";
    	
    	if(file_exists($clientCustomeSenderFile)) {    		
    		require_once $clientCustomeSenderFile;    		
    		$push = new  $customeClientSenderClass($pclient->getId(), NUM_OF_DAYS);
    	} else {    		
    		$push = new syndSend($pclient->getId(), NUM_OF_DAYS);
    	}
        $push->copyContent();
    }
    */
	
