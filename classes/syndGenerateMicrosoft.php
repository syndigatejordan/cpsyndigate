<?php

require_once dirname(__FILE__) . '/../ez_autoload.php';

$titleId = 0;
$revision = 0;
$force_resend = 0;
$reportId = 0;
$clientsIds = array();

foreach ($argv as $arg) {
  if (strstr($arg, '=')) {

    list($key, $value) = explode('=', $arg);

    switch ($key) {
      case 'title_id' : $titleId = (int) $value;
        break;
      case 'revision' : $revision = (int) $value;
        break;
      case 'report_id' : $reportId = (int) $value;
        break; // optional
      // Optional when called from the system it will join the report generate with report table, if manually called
      // 		then report id will not provided to not override the existing report record or calling unavilable one 
      case 'clients_ids': $cIds = explode(',', $value); // optional
        foreach ($cIds as $cid) {
          $clientsIds[] = (int) $cid;
        }
        break;
      case 'force_resend' : $force_resend = (int) $value;
        break; //optional               

      default:
        break;
    }
  }
}



$model = syndModel::getModelInstance();

if (!$titleId) {
  die('NO title Id entered in argv[1]' . PHP_EOL);
}

if (!$revision) {
  die('No revision spcefied in argv[2]' . PHP_EOL);
}

if (!$reportId) {
  $reportId = 0;
}


require_once dirname(__FILE__) . '/syndGenerateRevMicrosoft.php';
require_once dirname(__FILE__) . '/send/syndSendRev.php';

$generator = new syndGenerateRevMicrosoft(mktime(0, 0, 0, date('m'), date('d'), date('Y')), $reportId);
$files = $generator->generateAll($titleId, $revision);
