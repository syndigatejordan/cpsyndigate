<?php
    require_once dirname(__FILE__) . '/../ez_autoload.php';
    define('NUM_OF_DAYS', 3);

    $model = syndModel::getModelInstance();
    $pushClients = $model->getPushClients();
    //$pushClients[0]=ClientPeer::retrieveByPK(24);
   
    foreach ($pushClients as $pclient) {
    	
    	$push = null;
    	$customeClientSenderClass = 'syndSend_' . $pclient->getId();
    	$clientCustomeSenderFile  = dirname(__FILE__) . "/custome/{$customeClientSenderClass}.php";
    	
    	if(file_exists($clientCustomeSenderFile)) {
    		$push = new  $customeClientSenderClass($pclient->getId(), NUM_OF_DAYS);
    	} else {
    		$push = new syndSend($pclient->getId(), NUM_OF_DAYS);
    	}        
        $push->copyContent();
    }
