<?php

/**
 * About ...
 * 
 * argv parameters must be witten in thi format ex.  var_name=var_value
 * supported paramters 
 *  title_id       required  title id
 *  revision       required  revision number to process 
 *  report_id      optional  logging report_id 
 *  clients_ids    optional  clients ids to send the content, ids must be exploded by ,
 *  force_resend   optional  0 or 1 , if (1), the file will be resend to the client even if send before
 * 
 */
    require_once dirname(__FILE__) . '/../ez_autoload.php';
    
    $titleId      = 0;
    $revision     = 0;
    $force_resend = 0;
    $reportId     = 0;
    $clientsIds   = array();
    
    foreach ($argv as $arg) {
        if(strstr($arg, '=')) {
            
            list($key, $value) = explode('=', $arg);
            
            switch ($key) {
                case 'title_id'  : $titleId    = (int)$value; break;
                case 'revision'  : $revision   = (int)$value; break;
                case 'report_id' : $reportId   = (int)$value; break; // optional
                // Optional when called from the system it will join the report generate with report table, if manually called
                // 		then report id will not provided to not override the existing report record or calling unavilable one 
                case 'clients_ids': $cIds = explode(',', $value); // optional
                              foreach ($cIds as $cid) {
                                  $clientsIds[] = (int)$cid;
                              }
                              break;
                case 'force_resend' : $force_resend = (int)$value;  break; //optional               
                               
                default:
                    break;
            }
        }
    }
    
    
	
    $model 			= syndModel::getModelInstance();
        
    if(!$titleId) {
    	die('NO title Id entered in argv[1]' . PHP_EOL);
    }
    
    if(!$revision) {
    	die('No revision spcefied in argv[2]' . PHP_EOL);
    }
    
    if(!$reportId) {
        $reportId = 0;
    }
    
    
    require_once dirname( __FILE__ ) . '/syndGenerateRev.php';
    require_once dirname( __FILE__ ) . '/send/syndSendRev.php';
    
    $generator = new syndGenerateRev( mktime(0, 0, 0, date('m'), date('d'), date('Y')), $reportId, $argv );
    $generator->force_resend = $force_resend;    
    $generator->clientsIds   = $clientsIds; 
    $files     = $generator->generateByRev($titleId, $revision);
        
    $c = new Criteria();
    $c->add(ClientConnectionFilePeer::IS_SENT , 0);
    $c->add(ClientConnectionFilePeer::TITLE_ID, $titleId);
    $files = ClientConnectionPeer::doSelect($c);

    if( !empty($files) ) {
    	$sender = new syndSendRev();

	$sender->titleId  = $titleId;
	$sender->revision = $revision;

    	$sender->copyContent();
    	$sender->__destruct();
    } 
