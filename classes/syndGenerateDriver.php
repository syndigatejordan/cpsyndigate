<?php
	require_once dirname(__FILE__) . '/../ez_autoload.php';
    
    define('NUM_OF_DAYS', 3);
    
    $model 			= syndModel::getModelInstance();
    $clients 		= $model->getActiveClients();
    $pushClients 	= $model->getPushClients();
  
    
    $generate = new syndGenerate(mktime(0, 0, 0, date('m'), date('d'), date('Y')));
    $generate->generateByTitle($clients);
    
    $generate = new syndGenerate(mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')));
    $generate->generateByTitle($clients);

  
    unset($generate);


    
    foreach ($pushClients as $pclient) {

        $push = null;
    	$customeClientSenderClass = 'syndSend_' . $pclient->getId();
    	$clientCustomeSenderFile  = dirname(__FILE__) . "/send/custom/{$customeClientSenderClass}.php";
    	
    	if(file_exists($clientCustomeSenderFile)) {    		
    		require_once $clientCustomeSenderFile;    		
    		$push = new  $customeClientSenderClass($pclient->getId(), NUM_OF_DAYS);
    	} else {    		
    		$push = new syndSend($pclient->getId(), NUM_OF_DAYS);
    	}
        $push->copyContent();
    }
	
