<?php
    require_once dirname(__FILE__).'/../ez_autoload.php';

	////////////////////////////////////////////////////////////////////////////////////////
	// configuraion
	///////////////////////////////////////////////////////////////////////////////////////
//	define('COPYRIGHT', "Provided by <a href='http://syndigate.info'>Syndigate.info</a>, an <a href='http://albawaba.com'>Albawaba.com</a> company");
defined('COPYRIGHT')      OR define('COPYRIGHT', " Provided by SyndiGate Media Inc. (<a href='http://syndigate.info'>Syndigate.info</a>).");
	define('STRIPPING_TAGS', false);
	define('TMP_DIR', APP_PATH.'tmp/');
		

	class syndGenerate
	{
		static public $DB_NAME;
		static public $DB_USER;
		static public $DB_PASS;
		static public $DB_HOST;
		
		private $model;
		private $tmpFiles;
		private $timeStamp;
		private $scriptStartTimestamp;
		private $titlesHasNewContent = array();

		public function __construct($timeStamp='')
		{
			$this->model = syndModel::getModelInstance();
						
			$propelConf  = Propel::getConfiguration();
			$dbConf 	 = $propelConf['datasources']['propel']['connection'];
			
			self::$DB_USER = $dbConf['username'];
			self::$DB_PASS = $dbConf['password'];
			self::$DB_HOST = $dbConf['hostspec'];
			self::$DB_NAME = $dbConf['database'];			

			if($timeStamp) {
				$this->timeStamp  = $timeStamp;
			}
			
			$this->version = date('dhis');
			$this->scriptStartTimestamp = date('Y-m-d H:i:s');
		}
		
		public function getMaxGeneratedArticleId($titleId, $clientId)
		{				
			$c = new Criteria();
			$c->add(MaxPeer::TITLE_ID, $titleId);
			$c->add(MaxPeer::CLIENT_ID, $clientId);
			$result = MaxPeer::doSelectOne($c);
			return is_object($result) ? (int)$result->getArticleId() : 0;
		}
		
		public function setMaxGeneratedArticleId($titleId, $clientId, $articleId)
		{
			$c = new Criteria();
			$c->add(MaxPeer::TITLE_ID, $titleId);
			$c->add(MaxPeer::CLIENT_ID, $clientId);
			$maxObj = MaxPeer::doSelectOne($c);
			
			if(!is_object($maxObj)) {
				$maxObj = new Max();
				$maxObj->setTitleId($titleId);
				$maxObj->setClientId($clientId);
			}
			
			$maxObj->setArticleId($articleId);
			$maxObj->save();
			return;
		}
		
		public function generateByTitle(&$clients)
		{
			//Getting Titles that has new content and put them in array
			$c = new Criteria();
			$c->add(TitlePeer::IS_ACTIVE, 1);
			$c->add(TitlePeer::HAS_NEW_PARSED_CONTENT, 1);
			$resultTitles = TitlePeer::doSelect($c);
			
			foreach ($resultTitles as $t) {
				
				$this->titlesHasNewContent[] = $t->getId();
				
				if($t->getLastContentTimestamp() < $this->scriptStartTimestamp) {
					$t->setHasNewParsedContent(false);
					$t->save();	
				}
			}//End foreach
			
			
			$xml = array();
			if($clients) {
				
				foreach($clients as $client) {
					$filter    = $this->model->getClientRules($client->getId());
					ksort($filter);

					$filterKey = md5(serialize($filter) . $this->timeStamp);

					if(!isset($xml[$filterKey])) {
						$titles = $this->getArticlesTree($filter, $client->getId());
						$template = syndGenerateTemplatesManager::factory('NewsMl');
						foreach ($titles as $title) {
							$xml[$filterKey][$title['id']] = $template->generate(array($title));
						}
					}
					
					$ftpDir = $this->prepareDir($client->getHomeDir());
					foreach($xml[$filterKey] as $key => $value) {
						if($client->getGenerateType() == 'updates') {
							copy($value, $ftpDir.$key. '_' . $this->version .'.xml');
							echo "copying $value to " . $ftpDir.$key. '_' . $this->version .'.xml' . PHP_EOL; 
						} else {
							copy($value, $ftpDir.$key.'.xml');
							echo "copying $value to $key".'.xml' . PHP_EOL;
						}
						
						$this->tmpFiles[md5($value)] = $value;  
					}
					
					$afterGenerateFile = dirname(__FILE__) . '/generate/customClient/syndGenerate_' . $client->getId() . '.php';
					
					if(file_exists($afterGenerateFile)) {
						require_once $afterGenerateFile;
						$params = array("ftpDir"=> $ftpDir, "generateType"=>$client->getGenerateType());
						$className = "syndGenerate_" . $client->getId();
						$obj = new $className($params);
						$obj->afterGenerate();
					}
				}
				$this->clearTmp();
			}
		} // End function generateByTitle
		
		
		private function clearTmp()
		{
			if(is_array($this->tmpFiles)) {
				foreach($this->tmpFiles as $file) {
					unlink($file);
				}
			}
		}
	
		
		/**
		 * Prepares the dir to write files to, and returns the full path
		 *
		 * @param string $clientHomeDir
		 */
		private function prepareDir($clientHomeDir) {
			
			if($this->timeStamp) {
				$ftpDir = rtrim($clientHomeDir, '/').'/'.date('Y/m/d/', $this->timeStamp);
			} else {
				$ftpDir = rtrim($clientHomeDir, '/').'/'.date('Y/m/d/');
			}
			if(!is_dir($ftpDir)) {
				mkdir($ftpDir, 0755, true);
			}			
			return $ftpDir;
		}


		/**
		 * get titles sql where statement for client filters
		 *
		 * @param $filters array 
		 * @return string sql where statement but without "WHERE"
		 */
		public function getTitleFilters($filters=array())
		{
			$titlesRules = array();

			foreach($filters as $filter) {
				
				$titlesFilters   = array();
				if(!is_array($filter)) {
					$filter = array();
				}
				
				$filter = array_merge(array('title'=>'', 'lang'=>''), $filter);
				if($filter['title']) {
					$titlesFilters[]   = TitlePeer::ID." IN ({$filter['title']})";		// get client title filters
				}
				
				if($filter['lang']) {
					$titlesFilters[]   = TitlePeer::LANGUAGE_ID." IN ({$filter['lang']})";  // get client title language filters
				}
				
				if(count($titlesFilters)) {
					$titlesRules[]   = '('.implode(' AND ', $titlesFilters).')';		// mirge all client filter in one rule
				}
			} // End foreach
			
			if(count($titlesRules)) {
				return ' ('.implode(' OR ', $titlesRules).')';	
			}
			
			return '';
		} // End function getTitleFilters

		

		/**
		* get articles sql where statement for client filters
        *
        * @param $filters array
        * @return string sql where statement but without "WHERE"
        */
        private function getArticleFilters($filters=array())
		{
			if(!count($filters)) {
				$filters = array(array());
			}
			
			$articlesRules   = array();
			
			foreach($filters as $filter) {
				
				$articlesFilters = array();
				if(!is_array($filter)) {
					$filter = array();
				}
				
				if($this->timeStamp) {
					$filter['date'] = date('Y-m-d', $this->timeStamp);
				}
				
				$filter = array_merge(array('category'=>'', 'date'=>''), $filter);
				
				if($filter['category']) {
					$articlesFilters[] = ArticlePeer::IPTC_ID." IN ({$filter['category']})";	// get client article catigory filter
				}
				
				if($filter['date']) {
					
					$minDate = date('Y-m-d H:i', strtotime($filter['date']));
                    $maxDate = date('Y-m-d H:i', strtotime($filter['date'].' 23:59'));		// get client article date filter
                    $articlesFilters[] = ArticlePeer::PARSED_AT. " >= '$minDate'";
                    $articlesFilters[] = ArticlePeer::PARSED_AT. " <= '$maxDate'";
				}
				
				if(count($articlesFilters)) {
					$articlesRules[] = '('.implode(' AND ', $articlesFilters).')';			// mirge all client filters in one rule
				}
				
			} // End foreach

			
			if(count($articlesRules)) {
				return ' ('.implode(' OR ', $articlesRules).')';
			}
			return '';
			
		} // End function getArticleFilters
		
		
		
		/**
		 * get all title id from article table which parsed in specific date
		 * and put the results in tree
		 *
		 * @param string $date
		 * @return array
		 */
		private function &getArticlesTree($filters=array(), $clientId)
		{			
			$conn = mysql_connect(self::$DB_HOST, self::$DB_USER, self::$DB_PASS, true) or die("\n\nCannot connect to database\n\n");
			mysql_select_db (self::$DB_NAME) or die("\n\nDatabase doesn't exists\n\n");
	
			$titlesRules   = $this->getTitleFilters($filters);
			$articlesRules = $this->getArticleFilters($filters);

			$sql =  'SELECT * FROM '.TitlePeer::TABLE_NAME;
			if($titlesRules) {
				$sql .= ' WHERE '.$titlesRules;
			}

			$titlesResult = mysql_query($sql, $conn);
			while ($titleRow = mysql_fetch_assoc($titlesResult)) {
								
				$id = $titleRow['id'];
				$titles[$id]['id']           = $id;
				$titles[$id]['providerName'] = $titleRow['name'];
				$titles[$id]['copyright']    = $titleRow['copyright'];
				$titles[$id]['webSite']      = $titleRow['website']; //echo $articlesRules; die();
					
				
				if(!in_array($id, $this->titlesHasNewContent)) {
					unset($titles[$id]);
					continue;
				} 				

				// get title's articles
				//--- old sql before adding the original category ---// $sql = 'SELECT * FROM '.ArticlePeer::TABLE_NAME .' WHERE '.ArticlePeer::TITLE_ID.' = '.$id;
				$sql = 'SELECT article.*, original_article_category.cat_name FROM article left join article_original_data on article_original_data.id = article.article_original_data_id left join original_article_category on article_original_data.original_cat_id = original_article_category.id WHERE ' . ArticlePeer::TITLE_ID.' = '.$id; ;
				if($articlesRules) {
					$sql .= ' AND '. $articlesRules;
				}
				
				
								
				
								
				$client = ClientPeer::retrieveByPK($clientId);
				if($client->getGenerateType() == 'updates') {
					$maxArticleId = $this->getMaxGeneratedArticleId($id, $clientId);
					$sql .= ' AND ' . ArticlePeer::ID . ' > ' . $maxArticleId;
				}
				
				$articlesResult = mysql_query($sql, $conn) ;
				$titles[$id]['articles']  = new syndGenerateMySqlIterator($articlesResult, new ArticleArrayFormater($this->model));
				
				if(!mysql_num_rows($articlesResult)) {
					unset($titles[$id]);
				} else {
					if($client->getGenerateType() == 'updates') {
						$sql 	= 'SELECT MAX(id) AS the_max FROM '.ArticlePeer::TABLE_NAME .' WHERE '.ArticlePeer::TITLE_ID.' = '.$id;
						$result = mysql_query($sql, $conn);
						$result = mysql_fetch_assoc($result);
						$this->setMaxGeneratedArticleId($id, $clientId, $result['the_max']);
					}
				}

			}
			
			return $titles;
		}
		
		
		/*
		public function generateByAll(&$clients)
		{
			$xml = array();

			if($clients) {
				
				foreach($clients as $client) {
					$filter   = $this->model->getClientRules($client->getId());
					ksort($filter);

					$filterKey = md5(serialize($filter));

					if(!isset($xml[$filterKey])) {
						$titles = $this->getArticlesTree($filter);
						$template = syndGenerateTemplatesManager::factory('NewsMl');
						$xml[$filterKey] = $template->generate($titles);
					}
					
					$ftpDir = $this->prepareDir($client->getHomeDir());
					copy($xml[$filterKey], $ftpDir."contents.xml");
					$this->tmpFiles[md5($xml[$filterKey])] = $xml[$filterKey];
				}
				$this->clearTmp();
			}
		}
		*/

		/*
		public function generateByLanguage(&$clients)
		{
			$xml = array();

			if($clients) {
				
				foreach($clients as $client) {
					
					$filter    = $this->model->getClientRules($client->getId());
					ksort($filter);

					$filterKey = md5(serialize($filter));

					if(!isset($xml[$filterKey])) {
						$langs = LanguagePeer::doSelect(new Criteria());
						$languages = array();
						if($langs) {
							foreach($langs as $lang) {
								$languages[$lang->getId()] = $lang->getCode();
							}
						}


						$template = syndGenerateTemplatesManager::factory('NewsMl');
						//$titles = $this->getArticlesTree($filter);


						foreach ($languages as $id => $code) {
							foreach($filter as &$filt) {
								if(!isset($filt['lang'])) {
									$filt['lang'] = $id;
								}
							}
							$titles = $this->getArticlesTree($filter);
							if(count($titles)) {
								$xml[$filterKey][$code] = $template->generate($titles);
							}
						}
					}
					$ftpDir = $this->prepareDir($client->getHomeDir());
					foreach($xml[$filterKey] as $key => $value) {
						copy( $value, $ftpDir.$key.'.xml');
						$this->tmpFiles[md5($value)] = $value;  
					}
				}
				$this->clearTmp();
			}
		}
		*/

	} // End class syndGenerate

	
	class ArticleArrayFormater implements syndGenerateArrayFormater
	{
		private $model;
		public function __construct($model)
		{
			$this->model = $model;
		}

		private function getLanguage($langId)
		{
			static $languages = array();
			if(isset($languages[$langId])) {
				return $languages[$langId];
			}
			$lang = $this->model->getLanguage($langId);
			if($lang) {
				$languages[$lang->getId()] = $lang->getName();
			}
			return ($lang ? $lang->getName() : '');
                }
		
		public function format($articleRow)
		{
			$artId = $articleRow['id'];
			
			$returnVal['id'] = $artId;
			$returnVal['headline'] 		= $articleRow['headline'];
			$returnVal['byLine']   		= $articleRow['author'];
			$returnVal['language'] 		= $this->getLanguage($articleRow['language_id']);
			$returnVal['category'] 		= $articleRow['iptc_id'];
			$returnVal['abstract'] 		= $articleRow['summary'];
			$returnVal['text'] 			=& $articleRow['body'];
			$returnVal['date'] 			= trim($articleRow['date']) ? $articleRow['date'] : $articleRow['parsed_at'];
			$returnVal['originalCategory'] = trim($articleRow['cat_name']);
			
			$headlineImage    	  		= $this->model->getArticleHeadlineImage($artId);
			$returnVal['headlineImageCaption'] = $headlineImage ? $headlineImage->getImageCaption() : '';
			$returnVal['headlineImage'] = $headlineImage ? $headlineImage->getImgName() : '';
			$returnVal['videos'] 		= $this->model->getArticleVideos($artId);
			$returnVal['images'] 		= $this->model->getArticleImages($artId);
			return $returnVal;
		}
		
		
		
	} // End class 
