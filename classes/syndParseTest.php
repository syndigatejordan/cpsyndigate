<?php

	define('OUTPUT_CHARSET', 'UTF-8');
	require_once dirname(__FILE__).'/../'.'ez_autoload.php';
	require_once APP_PATH . 'conf/pheanstalk.conf.php';
	
	$x = new syndParse(array('titleId'=> $argv[1],'rowId'=>$argv[2]));
    $x->process();



	class syndParse
	{
		private $revision ;
		private $titleId = 193  ;
		private $rowId =  1497694;
		private $log;
		private $memcache, $memcacheConnected = false;
		private $generalReport;
		
		public function __construct($params)
		{
			$this->log = ezcLog::getInstance();
			$this->rowId = $params['rowId'];
			$this->titleId = $params['titleId'];
			echo "parser for title " . $this->titleId . " with row num " . $this->rowId . PHP_EOL ;
			//$this->log->log("Parser called. RowID [{$this->rowId}]", ezcLog::DEBUG, array('source' => 'parse') );
		}

		public function process()
		{
			try {
				$files   = array();                
				$model   = syndModel::getModelInstance();

				if(!$this->titleId) die('no title id' . PHP_EOL );
				//if(!$this->rowId) die('no parsing state row id entered' . PHP_EOL);

               		        $t       = TitlePeer::retrieveByPK($this->titleId);
				//$files = $this->getFilesByParsingState($this->rowId);


   				//$files = $this->getDirectoryFiles('/home/admin/ruba/svn/revisions/');
			        //$files = array('/home/syndigate/Desktop/newsml1.xml');
		                //$files = $this->getFilesByParsingState(1);
                		//$files = $this->getDirectoryFiles('/syndigate/sources/pwc/74/217/');
				$files = $this->getDirectoryFiles('/syndigate/sources/wc/714/2257/');
                                //$files = $this->getDirectoryFiles('/syndigate/sources/home/1/1/');

		                foreach ($files as $file) {
                			if(!is_file($file)) {
                				die("file not exist $file \n");
		                	} else {
                				echo "file exist $file \n";
		                	}
		                }
				echo PHP_EOL . PHP_EOL . PHP_EOL;
                
                		
				$parserClass = "syndParser_{$this->titleId}";
				require_once dirname(__FILE__)."/parse/custom/$parserClass.php";				
				$model->parseStartUpdateState($this->rowId);
				$parser      = new $parserClass($files, $model->getTitleById($this->titleId), $this->revision);
				$parser->beforeParse();
				$articles    = $parser->parse();
				echo "count is " . count($articles) . PHP_EOL;
				$parser->afterParse(array('articles' => &$articles));
				echo "count is " . count($articles) . PHP_EOL;
				$parser->addLog("After parse function finished");
				
			
				
				foreach($articles as $a) {
					echo PHP_EOL;
					echo "\theadline : " .  $a['headline'] . PHP_EOL;
					echo "\tdate     : " . $a['date'] . PHP_EOL;
					echo "\toriginal_article_id     : " . $a['original_data']['original_article_id'] . PHP_EOL;
					echo "\told_original_article_id : " . $a['original_data']['old_original_article_id'] . PHP_EOL;
					echo PHP_EOL;
				}
				
				//die();
//				print_r($articles);
//				die();
				
		

				$articlesIds 	= array();
				$contentChanged = false;
				$headlines 		= array();
				
				// Trying to connect to memcach if avilable
				$this->memcache 	= new Memcache;	
				try {	
					$this->memcacheConnected = $this->memcache->connect(MEMCACHE_HOST, MEMCACHE_PORT);
				} catch (Exception $e) {
					$parser->addLog("Could not connect to memcachd server", ezcLog::ERROR);
				}
				
				foreach($articles as $article) {
					echo "\n\n\ntrying to process article -------------------------\n";
					print_r($article);
					$headlines[$article['headline'] ]= 	 $articles['date'];
					
					// Set the revision of the article
					$article['original_data']['revision_num']   = $this->revision;
					
					//check if not the category exist create new one and assing the iptc to nothing
					if(!$model->isOriginalArticleCategory($this->titleId, $article['original_data']['original_category'])) {
						$model->addOriginalArticleCategory($this->titleId, $article['original_data']['original_category']);
					}
					//set the original category id
					$article['original_data']['original_cat_id'] = $model->getOriginalCategoryId($article['original_data']['original_category'], $this->titleId);

					
					// add original articles
					$originalArticleData    = $model->getArticleByOriginalId($article['original_data']['original_article_id']); // get original article
					$oldOriginalArticleData = $model->getArticleByOriginalId($article['original_data']['old_original_article_id']);				

					$articleObj = null;
					if($originalArticleData) {
						$originalId  = $originalArticleData->getId();
						$articleObj  = $model->getArticleByOriginalDataId($originalId);
						if($articleObj) {
							$parser->addLog("article original key found by new key with article id " . $articleObj->getId(), ezcLog::DEBUG, array('source' => 'parse') );
                       } else {
                       }

					} else {
						if($oldOriginalArticleData) {
							$originalId  = $oldOriginalArticleData->getId();
							$articleObj  = $model->getArticleByOriginalDataId($originalId);
							if($articleObj) {
								$parser->addLog("Found by old key with article id " . $articleObj->getId());
							}	
						} else {
							echo "no found " . $article['headline'];
						}

					} 
					
					
					if((!$originalArticleData && !$oldOriginalArticleData) || !$articleObj) {
						$parser->addLog("Add article with original id [".$article['original_data']['original_article_id']."]");
						if(!$originalArticleData) {
							$originalId = $model->addOriginalArticle($article['original_data']);
						} else {
							$model->updateOriginalArticle($originalId, $article['original_data']);
						}
						
						// add article
						$article['original_data_id'] = $originalId;
						$articleId     = $model->addArticle($article);
						$articlesIds[] = $articleId;												
						$parser->addLog("article added ...  ($articleId) with orginal " . $article['original_data']['original_article_id']);					
						if($this->memcacheConnected) {
							$this->memcache->set($memcachd_check_sum, 1, false, 864000);
						}
						$contentChanged = true;
					} else {
						$parser->addLog("Update article with original id [".$article['original_data']['original_article_id']."]");
						$model->updateOriginalArticle($originalId, $article['original_data']);

						// update article
						$article['original_data_id'] = $originalId;
						$articleId  	= $articleObj->getId();
						$updated    	= $model->updateArticle($articleId, $article);
						$contentChanged	= !$updated	? $contentChanged : true;
						
						if($updated) {
							echo "Updated ... ($articleId) with orginal " . $article['original_data']['original_article_id'] . PHP_EOL;	
							$articlesIds[] = $articleId;
							if($this->memcacheConnected) {
								$this->memcache->set($memcachd_check_sum, 1, false, 864000);
							}
						} else {
							echo "Not updated ... ($articleId) with orginal " . $article['original_data']['original_article_id'] . PHP_EOL;	
						}
						
					}
				
					//update article videos 
					foreach ($article['videos'] as $video) {
						$model->addOrUpdateArticleVideo($articleId, $video);
					}
					
					//update articel images
					foreach ($article['images'] as $image) {
						$model->addOrUpdateArticleImage($articleId, $image);
					}
					//$articlesIds[] = $articleId;
				}
				
				echo "count of headlines " . count($headlines) . PHP_EOL;
				//print_r($headlines);
				
				//Update title and set it to has new content if available								
				if($contentChanged) {
					echo "content chaned ............ \r\n";
					$parser->title->setHasNewParsedContent(1);
					$parser->title->setLastContentTimestamp(time());

					// Updating cahced articles to improve performance to the frontend list titles page
					$parser->title->setCachedArticlesToday((int)$parser->title->getArticlesToday());
					$parser->title->setCachedCurrentSpan((int)$parser->title->getArticlesSpanCount(0));
					$parser->title->setCachedPrevSpan((int)$parser->title->getArticlesSpanCount(-1, -1));

					$parser->title->save();
				}
				//updating parser report
				if(count($articlesIds) > 0) {
					$parser->parseReport['ArticlesIds'] = implode(',', $articlesIds);
					$parser->parseReport['ArticleNum']  = count($articlesIds);
					$model->parseUpdateReport($parser->parseReport);
					$parser->addLog("starting generation");

					$r = '';
					if($this->generalReport) {
						//$r = $this->generalReport->getId();
                                                $r = ' report_id=' . $this->generalReport->getId();
					}
					
					//$cmd = 'nohup /usr/bin/php /usr/local/syndigate/classes/syndGenerateRevDriver.php ' . $this->titleId . ' ' . $this->revision . ' ' . $r . '  > /dev/null &';
                                        $cmd = 'nohup /usr/bin/php /usr/local/syndigate/classes/syndGenerateRevDriver.php title_id=' . $this->titleId . ' revision=' . $this->revision . ' ' . $r . '  > /dev/null &';
					$parser->addLog($cmd);
					exec($cmd);
					$parser->addLog("finsihed generation");
				}
				
				$model->parseFinishUpdateState($this->rowId);
				
				
				

			} catch (Exception $e) {
				die($e->getMessage());
				$parser->addLog($e->getMessage(), ezcLog::ERROR);
				//$this->log->log($e->getMessage(), ezcLog::ERROR, array('source' => 'parse'));
			}
		}
		
		
		
		
		public function getDirectoryFiles($dir)
		{
			$files = array();

			if ($handle = opendir($dir)) {
			 
				while (false !== ($file = readdir($handle))) {
			           		
					if(is_file($dir . $file)) {
						$files[] = $dir . $file; 
					}
				
			}
				closedir($handle);
				return $files;
			} else {
				die("Invalid directory : $dir ......................... \n");
			}
		}

		public function getFilesByParsingState($rowId) {
			$files = array();		
			$model = syndModel::getModelInstance();
				
				$parsingState = $model->getParsingStateById($rowId);
				
				if($parsingState) {
					$this->titleId  = $parsingState->getTitleId();
					$this->revision = $parsingState->getRevisionNum();
					
					//get the general report 
					$c = new Criteria();
					$c->add(ReportPeer::TITLE_ID , $parsingState->getTitleId());
					$c->add(ReportPeer::REVISION_NUM, $parsingState->getRevisionNum());
					$this->generalReport = ReportPeer::doSelectOne($c);
					
				} else {
					//$this->log->log("No parsing state record with title id : " . $this->titleId . " & revision : " . $this->revision, ezcLog::ERROR , array('source' => 'parse') );
					throw new Exception('Porcess state does not exists.');
				}
				
				$svn   = new syndSvnDataGathering($this->titleId);
				$files = $svn->update($this->revision);
				
				echo "files from svn up ";  print_r($files);

				if(empty($files)) {
					// Re processing revision
					$files = $svn->getRevLog($this->revision);
					echo "files from re porcess" ; print_r($files);
				}
			return $files ;
		}

	}

