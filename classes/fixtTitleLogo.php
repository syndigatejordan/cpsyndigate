<?php
require_once '/usr/local/syndigate/scripts/propel_include_config.php';

class FixtTitleLogoCustom {

	public function __construct($ClientID) {
		$this -> ClientID = $ClientID;
		if (!$this -> ClientID) {echo "please provide the Client ID.";
			exit ;
		}
		$this -> processJob();
	}

	public function processJob() {

		$clientTitles = TitlesClientsRulesPeer::getClientTitles($this -> ClientID);
		foreach ($clientTitles as $t) {

			$title = TitlePeer::retrieveByPK($t->getId());
			$logoPath = $title -> getLogoPath();
			if (!$logoPath) {
				$this -> log("No logo path defind");
				return true;
			}

			if ($logoPath) {

				$logoPath = explode('/', $logoPath);
				$logo = $logoPath[count($logoPath) - 1];
				$logoPath = sfConfig::get('sf_upload_dir') . '/' . $logo;
				echo $title -> getId().chr(10);
				$newLogoPath = '/syndigate/pub/logos/' . $this -> ClientID . '/' . $title -> getId() . '.gif';

				if (is_file($logoPath)) {
					echo "coppying $logoPath to $newLogoPath";
					copy($logoPath, $newLogoPath);
					$this -> log("Trying to rezise image $newLogoPath");
					$this -> saveResizedImage($newLogoPath, $newLogoPath, 400, 40);
					chmod($newLogoPath, 0777);
				}

				return true;
			} else {
				return true;
			}
		}
	}

	public function saveResizedImage($srcfile, $dstfile, $maxw = '', $maxh = '') {

		$settings = new ezcImageConverterSettings( array(new ezcImageHandlerSettings('GD', 'ezcImageGdHandler'),
		//new ezcImageHandlerSettings( 'ImageMagick', 'ezcImageImagemagickHandler' ),
		));
		$converter = new ezcImageConverter($settings);

		if ($maxh || $maxw) {
			$scaleFilters = array(new ezcImageFilter('scale', array('width' => $maxw, 'height' => $maxh, 'direction' => ezcImageGeometryFilters::SCALE_DOWN)));
		} else {
			$scaleFilters = array();
		}

		// Which MIME types the conversion may output
		$mimeTypes = array('image/gif');

		// Create the transformation inside the manager, this can be re-used
		$converter -> createTransformation('gif', $scaleFilters, $mimeTypes);

		try {
			$converter -> transform('gif', $srcfile, $dstfile);
		} catch ( ezcImageTransformationException $e) {
			$this -> log($e -> getMessage());
			trigger_error("Exception: {$e->getMessage()} File:'$srcfile' ", E_USER_WARNING);
			return false;
		}

		return true;
	}

}// End class

$ClientID = $argv[1];
$obj = new FixtTitleLogoCustom($ClientID);
