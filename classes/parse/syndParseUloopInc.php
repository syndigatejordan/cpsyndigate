<?php

/**
 * Description of syndParseUloopInc
 *
 * @author eyad
 */
class syndParseUloopInc extends syndParseXmlContent
{

    public $languageTitle;

    public function customInit()
    {
        parent::customInit();
        $languageId = $this->title->getLanguageId();
        switch ($languageId) {
            case 1:
                $this->languageTitle = "Arabic";
                $this->defaultLang = $this->model->getLanguageId('ar');
                break;
            case 2:
                $this->languageTitle = "English";
                $this->defaultLang = $this->model->getLanguageId('en');
                break;
            case 4:
                $this->languageTitle = "French";
                $this->defaultLang = $this->model->getLanguageId('fr');
                break;
        }
        $this->extensionFilter = 'xml';
    }


    public function getRawArticles(&$fileContents)
    {
        //get raw articles
        $this->addLog("getting raw articles text");
        $rawArticles = $this->getElementsByName('item', $fileContents);
        return $rawArticles;
    }

    public function getArticleReference(&$text)
    {
        $this->addLog("getting article reference");
        return trim($this->textFixation($this->getCData($this->getElementByName('LINK', $text))));
    }

    public function getArticleOriginalId($params = array())
    {
        $transmissionID = trim($this->getCData($this->getElementByName('ID', $params['text'])));
        if ($transmissionID) {
            return $this->title->getId() . '_' . sha1($transmissionID);
        } else {
            return parent::getArticleOriginalId($params);
        }
    }

    protected function getHeadline(&$text)
    {
        $this->addLog("getting article headline");
        return trim($this->textFixation($this->getCData($this->getElementByName('TITLE', $text))));
    }

    protected function getStory(&$text)
    {
        $this->addLog("getting article body");
        return $this->textFixation($this->getCData($this->getElementByName('TEXT', $text)));
    }

    protected function getArticleDate(&$text)
    {
        $this->addLog("getting article date");
        $date = trim($this->getCData($this->getElementByName('DATE', $text)));
        $date = date('Y-m-d', strtotime($date));
        return $date;
    }

    protected function getAuthor(&$text)
    {
        $this->addLog("getting article author");
        return trim($this->textFixation($this->getCData($this->getElementByName('BY', $text))));
    }

    protected function getImages(&$text)
    {
        $imagesArray = array();
        return $imagesArray;
    }

}