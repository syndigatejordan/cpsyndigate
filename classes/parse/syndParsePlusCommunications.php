<?php
///////////////////////////////////////////////////////////////////////////
// Publisher : Plus Communications 
///////////////////////////////////////////////////////////////////////////

class syndParsePlusCommunications extends syndParseContent {
	
	protected $article;
	
	public function customInit() {
		parent::customInit();
		$this->defaultLang 		= $this->model->getLanguageId('en');
		$this->extensionFilter 	= 'txt';
	}
	
	public function getRawArticles(&$fileContents) {

		unset($this->article);
		$fileParts = explode("\r\n", $fileContents);
		
		if(is_array($fileParts)) {
			
			foreach ($fileParts as $part) {
				if($part) {
					
					if(!$this->article['headline']) {
						$this->article['headline'] = $part;
					} else {
						if(!$this->article['author']) {
							$this->article['author'] = $part;
						} else {
							if(!$this->article['body']) {
								$this->article['body'] = $this->configureArticle($part);
							} else {
								$this->article['body'] .= "\r\n". $part;
							}
						}
					}
					
				} else {
					continue;
				}
			}
			
		} else {
			return array();
		}
		
		// we will not use the return value, only returning for abstraction 
		// Just use this->article
		return array($fileContents);
	}
	

	protected function getStory(&$text) {	
		$this->addLog("getting article text");
		$story = $this->textFixation($this->article['body']);
		return  '<p>' . str_replace("\r\n", "</p>\r\n<p>", $story) . '</p>';
	}
		
	protected function getHeadline(&$text) {
		$this->addLog("getting article headline");
		return $this->textFixation($this->article['headline']);
	}
	
	public function getAuthor(&$text) {
		$this->addLog("getting article Author");
		return $this->textFixation($this->article['author']);
	}
	
	public function getArticleDate(&$text) {
		$this->addLog('Getting article date');				
		$date = explode('.', $this->article['date']);
		$date = date('Y-') . date('m', strtotime($date[0])) . '-' . (int)$date[1];
		return $this->dateFormater($date);
	}
	
	
	protected function configureArticle($text) {
		$text = explode('--', $text);
				
		$part1 = explode(',', $text[0]);
		if($part1[0]) {
			$this->article['byline'] = $part1[0];
		}
		if($part1[1]) {
			$this->article['date'] = $part1[1];
		}
		
		return $text[1];			
	}

	public function getArticleOriginalId($params = array()) {		
		$parentFunc = parent::getArticleOriginalId($params);		
		return $this->title->getId() . '_' . $parentFunc;
	}	
}
