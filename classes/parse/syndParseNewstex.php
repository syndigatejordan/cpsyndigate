<?php


class syndParseNewstex extends syndParseXmlContent
{

    public $languageTitle;
    public $story;

    public function customInit()
    {
        parent::customInit();
        $languageId = $this->title->getLanguageId();//
        $language = new Criteria();
        $language->add(LanguagePeer::ID, $languageId);
        $currentLanguage = LanguagePeer::doSelectOne($language);

        $this->languageTitle = $currentLanguage->getName();
        $this->defaultLang = $this->model->getLanguageId($currentLanguage->getCode());
        $this->extensionFilter = 'xml';
    }

    public function getRawArticles(&$fileContents)
    { //get raw articles
        $this->addLog("getting raw articles text");
        $matches = null;
        preg_match_all("/<Story(.*?)<\/Story>/is", $fileContents, $matches);
        return $matches[0];
    }

    public function getArticleOriginalId($params = array())
    {
        $title_id = $this->title->getId();
	$defaultArray = array('text' => '', 'headline' => '', 'articleDate' => '', 'issueNum' => 0);
        $params = array_merge($defaultArray, $params);
        if (!$articleOriginalId = trim($this->getElementByName('id', $params['text']))) {
            return parent::getArticleOriginalId($params);
        }
        return "{$title_id}_".sha1($articleOriginalId . trim($params['headline']));
    }

    public function getOriginalCategory(&$text)
    {
        $this->addLog("getting original category");
        $category = trim($this->getElementByName('categories', $text));
        $matches = null;
        if (preg_match_all('/<name>(.*?)<\/name>/is', $category, $matches)) {
            return implode(', ', $matches[1]);
        }
        return ""; 
   }

    public function getArticleReference(&$text)
    {
        $this->addLog('Getting article Link');
        $link = $this->getElementByName('permalink', $text);
        if (empty($link)) {
            return '';
        }
        return $link;
    }

    protected function getArticleDate(&$text)
    {
        //get article date
        $this->addLog("getting article date");
        $date = $this->getElementByName('date', $text);
        if ($date) {
            return $this->dateFormater($date);
        }
        return parent::getArticleDate($text);
    }

    protected function getHeadline(&$text)
    {
        $this->addLog("getting article headline");
        return $this->textFixation($this->getElementByName('headline', $text));
    }

    public function textFixation($text)
    {
        $text = parent::textFixation($text);
        return html_entity_decode($text);
    }

    protected function getAbstract(&$text)
    {
        //get article summary
        $this->addLog("getting article summary");
        $matche = null;
        if (preg_match("/<subheadline>(.*?)<\/subheadline>/is", $text, $matche)) {
            return $matche[1];
        } else {
            return "";
        }
    }

    protected function getStory(&$text)
    {
        $this->addLog("getting article body");
        $this->story = $this->getCData($this->getElementByName('html_content', $text));
        $this->story = str_replace("&amp;", "&", $this->story);
        $this->story = $this->textFixation($this->story);
        $this->story = preg_replace('!\s+!', ' ', $this->story);
        $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
            return $this->story;
    }

    protected function getAuthor(&$text)
    {
        $this->addLog("getting article author");
        $matche = null;
        if (preg_match("/<Author>(.*?)<\/Author>/is", $text, $matche)) {
            return $matche[1];
        } else {
            return "";
        }
    }

    protected function getImages(&$text)
    {
        $imagesArray = array();
        return $imagesArray;
    }
}
