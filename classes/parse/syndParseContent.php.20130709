<?php
	abstract class syndParseContent extends syndParseParser
	{

		public $parseReport;

		protected $files;
		public $title;
		protected $revisionId;
		protected $model;
		protected $log;
		protected $report;
		protected $errorMessage = '';
		protected $currentlyParsedFile;
		protected $extensionFilter;

		protected $charEncoding;

		protected $defaultLang = 0;
		protected $defaultDate = '0000-00-00';
		protected $defaultIptcId = 0;
		protected $defaultHasTime = false;
		protected $defaultProcessState = ezcLog::SUCCESS_AUDIT;
		protected $dafaultHost = '';
		protected $svnImages = array();
		
		protected $parseTmpDir;
		protected $imgCacheDir;
		protected $hasNewParsedContent;


		/**
		 * initiating class instances
		 *
		 * @param $files array() incomming svn files
		 * @param $titleId int title id
		 * @param $revisionId int svn revision number which was used in svn update process
		 */
		public function __construct($files, $title, $revisionId)
		{
			$this->files       = $files;
			$this->title       = $title;
			$this->revisionId  = $revisionId;
			$this->defaultDate = $this->dateFormater(date('Y-m-d'));
			
			$this->log         = ezcLog::getInstance();
			$this->log->source = 'parse';
			$this->report      = new syndReport('parse');
			$this->model       = syndModel::getModelInstance();

			$this->parseReport['ArticlesIds']  = '';
			$this->parseReport['Date']         = time();
			$this->parseReport['ParserError']  = '';
			$this->parseReport['ParserStatus'] = 'start';
			$this->parseReport['TitleId']      = $this->title->getId();
			$this->parseReport['ArticleNum']   = 0;
			$this->parseTmpDir = APP_PATH.'tmp/';
			$this->imgCacheDir = APP_PATH.'tmp/parser_' . $this->title->getId() . '_cache/images/';
			$this->hasNewParsedContent = (int)$this->title->getHasNewParsedContent();
		}

		protected function customInit()
		{
		}

		public function beforeParse($params = array())
		{
			$this->customInit();
			$this->parseReport['Id'] = $this->report->addReport($this->parseReport);
			$this->addLog('start parsing (title:'.$this->title->getName().')');
			
			//Adding report gather id to parsing state
			$c = new Criteria();
			$c->add(ParsingStatePeer::REVISION_NUM , $this->revisionId);
			$c->add(ParsingStatePeer::TITLE_ID, $this->title->getId());
			$parsingState = ParsingStatePeer::doSelectOne($c);
			
			if(is_object($parsingState)) {
				$parsingState->setReportParseId($this->parseReport['Id']);
				$parsingState->save();
				
				//Also add the report parse id to the table "report" because the parsing state may be deleted
				$c = new Criteria();
				$c->add( ReportPeer::TITLE_ID, $this->title->getId());
				$c->add( ReportPeer::REVISION_NUM , $this->revisionId);
				$report = ReportPeer::doSelectOne($c);
				if(is_object($report)) {
					$report->setReportParseId($this->parseReport['Id']);
					$report->save();
				}
			}
		}

		public function afterParse($params = array()) 
		{
			if($this->defaultProcessState == ezcLog::FAILED_AUDIT) {
				$this->parseReport['ParserError']  = $this->errorMessage;
				$this->parseReport['ParserStatus'] = 'failed';
				$this->report->updateReport($this->parseReport);
			} else {
								
				$this->parseReport['ParserStatus'] = 'completed';
				$this->report->updateReport($this->parseReport);
			}
			$this->addLog('close parsing (title:'.$this->title->getName().')', $this->defaultProcessState);
		}

		public function parse()
		{
			$this->getSvnImages();
						
			$articles = array();
			foreach ($this->files as $file) {
				if($this->extensionFilter) {
					if(!syndParseHelper::isFileExtension($file, $this->extensionFilter)) {
						$this->addLog("Wrong extension for file : $file)");
						continue;
					}
				}
				if(!file_exists($file)) {
					$this->addLog("file dose not exist: $file)");
					continue;
				}

				$this->addLog("get file contents (file:$file)");
				$fileContents = $this->getFileContents($file);

				if(!$fileContents) {
					continue;
				}

				$this->currentlyParsedFile = $file;
				$this->loadCurrentDirectory();
				$this->loadUpperDir();
				$rawArticles = $this->getRawArticles($fileContents);
				foreach($rawArticles as $rawArticle) {
					$article = $this->getArticle($rawArticle);
					
					if(trim(strip_tags($article['story']))) {
						$articles[] = $article;
					}else {
						$this->addLog("The file ($file) have article with empty body", ezcLog::WARNING);
					}
				}
			}
			return $articles;
		}

		protected function getArticle(&$text)
		{
			try {
				$allowedTags = '<h1><h2><h3><h4><h5><h6><b><i><u><p><hr><br><br/><br /><big><em><small><strong><sub><sup><ins><del><s><strike><code><kbd><q><img><BLOCKQUOTE><cite><dd><div><span><ul><li><ol><style><table><td><th><tr><tbody><thead><a>';
												
				//$article['iptc_id']     = $this->getIptcId($text);
				$article['title_id']      = $this->title->getId();
				$article['lang_id']       = $this->getLangId($text);
				$article['headline']      = strip_tags($this->getHeadline($text));
				$article['abstract']      = strip_tags($this->getAbstract($text));
				$article['author']        = strip_tags($this->getAuthor($text));
				$article['date']          = $this->getArticleDate($text);
				$article['has_time']      = $this->getHasTime();
				$article['parsed_at']     = time();
				$article['original_data'] = $this->getOriginalData($text);
				$article['images']        = $this->getImages($text);
				$article['videos']	      = $this->getVideos($text);
				
				$issueNum = isset($article['original_data']['issue_number']) ? $article['original_data']['issue_number'] : 0;
				$article['original_data']['original_article_id'] = $this->getArticleOriginalId(array('text' => &$text, 'headline' =>$article['headline'], 'articleDate' => $article['date'], 'issueNum' => $issueNum));
				$article['original_data']['old_original_article_id'] = syndParseContent::getArticleOriginalId(array('text' => &$text, 'headline' =>$article['headline'], 'articleDate' => $article['date'], 'issueNum' => $issueNum));
				$article['original_data']['original_category']   = strip_tags($this->getOriginalCategory($text));
				
				/*			
				Because getIptcId remove the category -in some parsers like AbText- the text and we do use this in 
				other function we put it at last 
				*/
				$article['iptc_id']       = $this->getIptcId($text);
				
				$this->processExtraTags($text);
				$article['story']         = strip_tags($this->getStory($text), $allowedTags);
							
			} catch (Exception $e) {
				$this->addLog($e->getMessage(), ezcLog::ERROR);
				$this->errorMessage = $e->getMessage();
				$this->defaultProcessState = ezcLog::FAILED_AUDIT;
			}
			
			return $article;
		}

		protected function getImages(&$text)
		{
			return array();
		}
		
		protected function getArticleImage(&$text)
		{
			return false;
		}
		
		protected function getIptcId(&$text)
		{
			return $this->defaultIptcId;
		}

		protected function getLangId(&$text)
		{
			return $this->defaultLang;
		}

		protected function getAbstract(&$text)
		{
			return '';
		}

		protected function getAuthor(&$text)
		{
			return '';
		}

		protected function getHasTime()
		{
			return $this->defaultHasTime;
		}

		protected function getOriginalData(&$text)
		{
			return array();
		}

		public function getArticleOriginalId($params = array())
		{
			$defaultArray = array('text' => '', 'headline' =>'', 'articleDate' => '', 'issueNum' => 0);
			$params =  array_merge($defaultArray, $params);
			if($params['issueNum']) {
				return sha1(trim($params['headline']).$params['issueNum']);
			} else {
				return sha1(trim($params['headline']).$params['articleDate']);
			}
		}

		protected function getFileContents($file)
		{
			if($this->charEncoding) {
				return iconv($this->charEncoding, OUTPUT_CHARSET, file_get_contents($file));
			} else {
				return file_get_contents($file);
			}
		}

		public function textFixation($text)
		{
			return $text;
		}

		public function addLog($message, $type = ezcLog::DEBUG, $source = 'parse') {
			$this->log->log($message, $type, array( 'report_parse_id' => $this->parseReport['Id'], 'source' => $source ));
		}

		private function getSvnImages()
		{
			//get all images from svn
			foreach($this->files as $file) {
				if(syndParseHelper::isFileExtension($file, array('jpg', 'gif', 'png', 'bmp'))) {
					$this->svnImages[basename($file)] = $file;
				}
			}
		}

		public function dateFormater($date)
		{			
			if( date('Y-m-d', strtotime($date)) == '1970-01-01') {
				return date('Y-m-d', time());
			} else {
				return date('Y-m-d', strtotime($date));
			}
		}

		protected function getArticleDate(&$text)
		{
			return $this->defaultDate;
		}
		
		protected function getOriginalCategory(&$text)
		{
			return '';
		}
		
		protected function getVideos(&$text)
		{
			return array();
		}
				
		protected function addVideo($articleId, $params=array()) 
		{
			if(!$params['video_id']) {
				return false;
			} else {
				$params['article_id'] = $articleId;
				return $this->model->addVideo($params);
			}
		}
		
		protected function addImage($articleId, $params=array()) 
		{
			if(!$params['image_id']) {
				return false;
			} else {
				$params['article_id'] = $articleId;
				return $this->model->addImage($params);
			}
		}
			
		
		protected function loadCurrentDirectory()
		{
			$currentDir = explode('/', $this->currentlyParsedFile);
			unset($currentDir[count($currentDir)-1]);
			$this->currentDir = implode($currentDir, '/') . '/';
		}

		protected function loadUpperDir()
		{
			$paths = explode('/', $this->currentDir);

			$this->upperDir = '';
			for ($i=0; $i<count($paths)-2;$i++) {
				$this->upperDir .= $paths[$i] . '/';
			}
		}	
		
		
		/**
		 * Some articles comes with extra tags as their system generate it
		 * 	These tags must be removed it some parsers such as Albawaba text format or it 
		 * 	will appear in the story body
		 *
		 * @param text $text
		 */
		protected function processExtraTags(&$text)
		{
		}
		
		
		
		/**
		 * Copying Images to images path and return the images array 
		 *
		 * @param array $images array of images full path
		 * @return array of images formated as article images array
		 */
		protected final function getAndCopyImagesFromArray($images = array())
		{
			if(!is_array($images)) {
				return array();
			}
			
			$returnedImages = array();
			
			foreach ($images as $imageName) {
				
				if(is_file($imageName)) {
					
					$image = basename($imageName);
					
					$name =  $this->model->getImgReplacement($image, dirname($imageName) . '/', $this->title->getId());
					
					$img['img_name']		= str_replace(IMGS_PATH, IMGS_HOST, $name);
					$img['original_name']	= $imageName;
					$img['image_caption']	= $image;
					$img['is_headline']     = true;
					$returnedImages[] = $img;
				} 
			} //End foreach
			
			return $returnedImages;
			
		} //End function
		
		/**
		 * Copying videos to videos path and return the video name 
		 * @author Mohammad Mousa
		 * @param string $videoName 
		 * @return string $videoName
		 */
		protected final function getAndCopyVideosFromArray($videoName = '')
		{
			if ( ! $videoName  ) {
				return array();
			}
			
			$returnedVideos = array();
			if(is_file($videoName)) {
				$video = basename($videoName);
				//$this->addLog("Mousa0 :  $videoName");
				//$videoName = str_replace($this->title->getParseDir(),  $this->title->getHomeDir(), $videoName);
				$this->addLog("Note: Replacing the current directory to home directory");
				$this->addLog("Replace this:  " . $videoName);
				$videoName = str_replace($this->title->getParseDir(),  $this->title->getHomeDir(), $videoName);
				$this->addLog("By this:  " . $videoName);
				$name =  $this->model->getVidReplacement($video, dirname($videoName) . '/', $this->title->getId());
				$returnedVideoName		= str_replace(VIDEOS_PATH, VIDEOS_HOST, $name);
			}
			return $returnedVideoName;
		} //End function
		
		
		public function copyUrlImgIfNotCached($imageUrl)
		{
			$baseName 	 = basename($imageUrl);
			$copiedImage = $this->imgCacheDir . $baseName;			
					
															
			if(!is_dir($this->imgCacheDir)) {
				mkdir($this->imgCacheDir, 0755,true);
			}
			
			if(!file_exists($copiedImage)) {
				$imgHeaders  = get_headers($imageUrl);
				if(('HTTP/1.1 200 OK' == $imgHeaders[0]) || ('HTTP/1.0 200 OK' == $imgHeaders[0])) {					
					$copyResult = copy($imageUrl, $copiedImage);
					return !$copyResult? false : $copiedImage;
				} else {
					return false;
				}
			} else {								
				return $copiedImage;
			}
		}// End function copyUrlImgIfNotCached
		
		
		public function clearImgsCache($imgsDir)
		{
			$dirFiles = syndParseHelper::getDirectoryFiles($imgsDir);
			
			if(!is_array($dirFiles)) {
				return false;
			} else {
				foreach ($dirFiles as $file) {
					if(is_file($file)) {
						$delResult = @unlink($file);
						if(!$delResult) {
							$this->addLog("Error : unable to delete $file");
						}
					}
				}
			}
		}//End function clearImgsCache

		protected function getCurrentParsedFileDateFromFolder() {
			
			$dir   = dirname($this->currentlyParsedFile);
			$dir   = explode('/' . $this->title->getPublisherId() . '/' . $this->title->getId() . '/', $dir);
			$date  = str_replace('/', '-', $dir[1]);
			
			return $this->dateFormater($date);			
		}
						
	}
