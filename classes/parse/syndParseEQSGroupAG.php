<?php

class syndParseEQSGroupAG extends syndParseXmlContent
{

  public $languageTitle;
  public $story;

  public function customInit()
  {
    parent::customInit();
    $languageId = $this->title->getLanguageId();//
    $language = new Criteria();
    $language->add(LanguagePeer::ID, $languageId);
    $currentLanguage = LanguagePeer::doSelectOne($language);

    $this->languageTitle = $currentLanguage->getName();
    $this->defaultLang = $this->model->getLanguageId($currentLanguage->getCode());
    $this->extensionFilter = 'xml';
  }

  public function getRawArticles(&$fileContents)
  {
    //get raw articles
    $this->addLog("getting raw articles text");
    $rawArticles = $this->getElementsByName('EquityStory-News', $fileContents);
    return $rawArticles;
  }

  public function getArticleOriginalId($params = array())
  {
    $defaultArray = array('text' => '', 'headline' => '', 'articleDate' => '', 'issueNum' => 0);
    $params = array_merge($defaultArray, $params);
    if (!$articleOriginalId = trim($this->getElementByName('news_id', $params['text']))) {
      return parent::getArticleOriginalId($params);
    }
    return sha1($articleOriginalId . trim($params['headline']));
  }

  public function getOriginalCategory(&$text)
  {
    $this->addLog("getting original category");
    $category = trim($this->getElementByName('news_type', $text));
    $cats = $this->getElementsByName('value', $category);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = $this->textFixation($this->getCData($cat));
      }
    }
    return implode(', ', $originalCats);
  }

  public function textFixation($text)
  {
    $text = parent::textFixation($text);
    return html_entity_decode($text);
  }

  public function getArticleReference(&$text)
  {
    $this->addLog('Getting article Link');
    return '';
  }

  protected function getArticleDate(&$text)
  {
    //get article date
    $this->addLog("getting article date");
    $date = $this->getElementByName('deliveryDate', $text);
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  protected function getHeadline(&$text)
  {
    $this->addLog("getting article headline");
    return $this->textFixation($this->getElementByName('Headline', $text));
  }

  protected function getAbstract(&$text)
  {
    //get article summary
    $this->addLog("getting article summary");
    return "";

  }

  protected function getStory(&$text)
  {
    $this->addLog("getting article body");
    $this->story = $this->getCData($this->getElementByName('Story', $text));
    $this->story = str_replace("&amp;", "&", $this->story);
    $this->story = $this->textFixation($this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    return $this->story;
  }

  protected function getAuthor(&$text)
  {
    $this->addLog("getting article author");
    return "";

  }

  protected function getImages(&$text)
  {
    $imagesArray = array();
    return $imagesArray;
  }
}
