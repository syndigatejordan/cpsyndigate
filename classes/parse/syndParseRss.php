<?php

	class syndParseRss extends syndParseXmlContent
	{
		public function customInit()
		{
			parent::customInit();
			$this->extensionFilter = 'xml';
		}

		public function getRawArticles(&$fileContents)
		{
			//get language
			if(!$this->defaultLang) {
				$this->addLog("getting articles language id");
				$lang = $this->getElementByName('Language', $fileContents);
				$lang = isset($lang) ? substr($lang, 0, 2) : '';
				$this->defaultLang = $this->model->getLanguageId($lang);
			}
			$this->defaultHasTime = true;


			//get raw articles
			$this->addLog("getting raw articles text");
			$rawArticles = $this->getElementsByName('item', $fileContents);
			return $rawArticles;
		}

		protected function getIptcId(&$text)
		{
			//get category name from file name
			$this->addLog("getting category name");
			$originalCategoryName = $this->getElementByName('category', $text);
			//if there is no category tag, read it from the file name
			if(!$originalCategoryName) {
				$originalCategoryName = syndParseHelper::getCategoryNameFromFileName($this->currentlyParsedFile);
			}
			$iptcId = $this->model->getIptcCategoryId($originalCategoryName);
			return ($iptcId ? $iptcId : parent::getIptcId($text));
		}

		protected function getOriginalCategory(&$text)
		{
			//get article text
			$this->addLog("getting article original category");
			return $this->textFixation($this->getCData($this->getElementByName('category', $text)));
		}

		protected function getStory(&$text)
		{
			$this->addLog("getting article text ..");
			 $this->addLog("getting element .." );
			$text = $this->getElementByName('description', $text) ;
			$this->addLog("getting cdata .." );
			$text = $this->getCData($text);
			$this->addLog("getting text fixation .." );
			$text =  $this->textFixation($text);
			$this->addLog("returnning text ..");
			return $text;
		}

		protected function getHeadline(&$text)
		{
			$this->addLog("getting article headline");
			return $this->textFixation($this->getCData($this->getElementByName('title', $text)));
		}

		/*
		public function getArticleOriginalId($params = array())
		{
			$defaultArray = array('text' => '', 'headline' =>'', 'articleDate' => '', 'issueNum' => 0);
			$params =  array_merge($defaultArray, $params);
			if(!$articleOriginalId = trim($this->getElementByName('guid', $params['text']))) {
				return parent::getArticleOriginalId($params);
			}
			return sha1($articleOriginalId);
		}
		*/
		public function getArticleOriginalId($params = array())
		{
			$defaultArray = array('text' => '', 'headline' =>'', 'articleDate' => '', 'issueNum' => 0);
			$params =  array_merge($defaultArray, $params);
			
			$articleOriginalId = trim($this->getElementByName('guid', $params['text']));
			if(!$articleOriginalId) {
				$articleOriginalId = trim($this->getElementByName('link', $params['text']));
			}
			
			$articleOriginalId = trim($this->getCData($articleOriginalId));
			
			if(!$articleOriginalId) {
				return parent::getArticleOriginalId($params);				
			} else {				
				return $this->title->getId() . "_" . sha1($articleOriginalId);
			}
		}		

		protected function getArticleDate(&$text)
		{
			$this->addLog("getting article date");
			$date = $this->getElementByName('pubDate', $text);
			if($date) {
				return $this->dateFormater($date);
			}
			return parent::getArticleDate($text);
		}

		protected function getAuthor(&$text)
		{
			$this->addLog("getting article author");
			return $this->textFixation($this->getCData($this->getElementByName('author', $text)));
		}
                public function getArticleReference(&$text)
		{
			$this->addLog("getting article reference");
			return $this->textFixation($this->getCData($this->getElementByName('link', $text)));
		}


	}

