<?php
////////////////////////////////////////////////////////////////////////
// Publisher : HT Media Ltd
////////////////////////////////////////////////////////////////////////

class syndParseHtMedia extends syndParseXmlContent
{
	
	private $transmissionID;

	public function customInit() {
		parent::customInit();
		$this->defaultLang 		= $this->model->getLanguageId('en');
		$this->extensionFilter 	= 'xml';
	}

	protected function getRawArticles(&$fileContents) {			
		$this->addLog("getting articles raw text");
		
		$this->transmissionID 	= null;		
		$transmissionID 		= syndParseHelper::getStringsBetween('TransmissionID="', '">', $fileContents);
		$this->transmissionID 	= $transmissionID[0];
		
		return $this->getElementsByName('Document', $fileContents);
	}

	protected function getStory(&$text) {	
		$this->addLog("getting article text");
		/*	
		$bodyWithDate = $this->textFixation($this->getElementByName('body', $text));
		$body = explode('</Dateline>', $bodyWithDate);
		return $this->textFixation($body[1]);*/
		$body = $this -> textFixation($this -> getElementByName('BodyContent', $text));
		$body = strip_tags($body, '<p>');
		return $body;
	}

	protected function getHeadline(&$text) {
		$this->addLog("getting article headline");
		/*	
		$headline = $this->getElementByName('Headline', $text);
		return $this->textFixation($this->getElementByName('PrimaryHeadline', $headline));*/
		$headline = $this -> getElementByName('Headline1', $text);
		return $this -> textFixation($headline);
	}

	protected function getArticleDate(&$text) {			
		$this->addLog("getting article date");
		/*	
		$bodyWithDate 	= $this->textFixation($this->getElementByName('body', $text));
		$date 		  	= $this->getElementByName('Dateline', $bodyWithDate);
		return  $this->textFixation($this->getElementByName('ContentDate', $date));*/
		$date = $this -> getElementByName('ContentDate', $text);
		$date = date('Y-m-d',strtotime($date));
		return $date;
	}
	
	public function getArticleOriginalId($params = array()) {			
		if($this->transmissionID) {
			return $this->title->getId() . '_' . sha1($this->transmissionID);
		} else {
			return parent::getArticleOriginalId($params);
		}
	}

}