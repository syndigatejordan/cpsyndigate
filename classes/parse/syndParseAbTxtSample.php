<?php

	class syndParseAbTxtSample extends syndParseContent
	{

		public function customInit()
		{
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('en');
			$this->extensionFilter = 'txt';
		}


		protected function getRawArticles(&$fileContents)
		{
			$fileContents = str_replace("\n\r", "\n", $fileContents);
			$fileContents = str_replace("\r", "\n", $fileContents);
			
			return array($fileContents);
		}

		protected function getIptcId(&$text, $originalCategoryTagName = 'Category')
		{
			$this->addLog("getting article category");
			$category =  $this->textFixation($this->getItemValue($originalCategoryTagName, $text));
			$this->removeItem($originalCategoryTagName, $text);
			$iptcId = $this->model->getIptcCategoryId($category);
			return ($iptcId ? $iptcId : parent::getIptcId($text));
		}
		
		protected function getOriginalCategory(&$text, $originalCategoryTagName = 'Category')
		{
			//get article text
			$this->addLog("getting article original category");
			return $this->textFixation($this->getItemValue($originalCategoryTagName, $text));
		}

		protected function getStory(&$text)
		{
			//get article text
			$this->addLog("getting article text");
			$story = trim($text);
			return $this->textFixation($story);
		}

		protected function getHeadline(&$text, $headlineTagName='Headline')
		{
			//get article title
			$this->addLog("getting article headline");
			$headline =  $this->textFixation($this->getItemValue($headlineTagName, $text));
			$this->removeItem($headlineTagName, $text);

			return $headline;
		}

		protected function getArticleDate(&$text, $dateTagName = 'Date')
		{
			//get article date
			$this->addLog("getting article date");
			$date = $this->getItemValue($dateTagName, $text);
			$this->removeItem($dateTagName, $text);
			if($date) {
				return $this->dateFormater($date);
			}
			return parent::getArticleDate($text);
		}

		protected function getAuthor(&$text, $authorTagName = 'Author')
		{
			//get author
			$this->addLog("getting article author");
			$author = $this->textFixation($this->getItemValue($authorTagName, $text));
			$this->removeItem($authorTagName, $text);

			return $author;
		}

		protected function getAbstract(&$text, $AbstractTagName = 'Summary') 
		{
			$this->addLog('Getting article summary');
			$summary = $this->textFixation($this->getItemValue($AbstractTagName, $text));
			$this->removeItem($AbstractTagName, $text);
			return $summary;
		}	

		
		/**
		 * Enter description here...
		 *
		 * @param String $text The Article text
		 * @param String $imageTagName The image tag name in the albawaba text format 
		 * @param array $extras
		 * 				array of extras 
		 * 					image_extension => If the full image name (with extention) is not provided with the albawba text format you can spesify the extention in this key
		 * 					image_folder    => The folder containing the image (default is the same text file), ex. Images ex. ../Images
		 * 					path_type  		=> 'absolute' but default= '' which is relative
		 * 
		 * @return unknown
		 */
		protected function getImages(&$text, $imageTagName = 'Image', $extras=array())
        {        	
        	$this->addLog("getting image in ab");
			$image = ltrim( rtrim($this->textFixation($this->getItemValue($imageTagName, $text))));
			$this->removeItem($imageTagName, $text);
						
			$imageName='';
			if($image) {
				
				//processing image extention
				if(array_key_exists('image_extension', $extras)) {
					if(!strstr($image, $extras['image_extension'])) {
						$image.= '.'.$extras['image_extension'];
					}
				}
				
				//processing image folder
				if(array_key_exists('image_folder', $extras) && !is_file($this->currentDir . $image)) {
					if(array_key_exists('path_type', $extras) && $extras['path_type'] = 'absolute') {
						$imageName = $extras['image_folder'] . $image;
					} else {
						$imageName = $this->currentDir . $extras['image_folder'] . $image;
					}
					
				} else {
					$imageName 	= $this->currentDir . $image;
				}
			}// End if image
			
			
			if(is_file($imageName)) {
							
				$name =  $this->model->getImgReplacement($image, dirname($imageName) . '/', $this->title->getId());
				$img['img_name']		= str_replace(IMGS_PATH, IMGS_HOST, $name);
				$img['original_name']	= $imageName;
				$img['image_caption']	= $image;
				$img['is_headline']     = true;
				$images[] = $img;
				return $images;
			} else {
				return array();
			}
        }
        
        

		protected function getItemValue($name, $text)
		{
			$regExp = '\([\s]*'.$name."[\\s]*:([^\n]*)[\\s]*\\)";
			if(preg_match("/$regExp/i", $text, $matches)) {
				return $matches[1];
			}
			return null;
		}

		protected function removeItem($name, &$text)
		{
			$regExp = '\([\s]*'.$name."[\\s]*:([^\n]*)[\\s]*\\)";
			$text = preg_replace("/$regExp/i", '', $text);
		}
		
	}
?>
