<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Israel-America Chamber of Commerce  [# publisher id = 806]
//Title      : Israel-America Chamber of Commerce News [ English ] 
//Created on : Feb 02, 2016, 12:09:54 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2464 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}