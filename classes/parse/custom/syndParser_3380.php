<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : EMDP - Egypt Media Development Program  [# publisher id = 1164]
//Title      : Egyptian Editors Forum (EEF) [ Arabic ] 
//Created on : Aug 03, 2021, 12:45:35 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3380 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}