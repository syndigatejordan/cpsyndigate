<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IRIB World Service  [# publisher id = 799]
//Title      : ParsToday [ Swahili ] 
//Created on : Oct 22, 2020, 12:35:53 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6581 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('sw'); 
	} 
}