<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : MONTSAME News Agency  [# publisher id = 1423]
//Title      : MONTSAME News Agency [ English ] 
//Created on : Oct 17, 2020, 2:36:57 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6638 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}