<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ministry of Foreign Affairs of the Republic of Uzbekistan  [# publisher id = 896]
//Title      : Ministry of Foreign Affairs of the Republic of Uzbekistan - News [ English ] 
//Created on : Apr 07, 2016, 10:57:30 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2664 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}