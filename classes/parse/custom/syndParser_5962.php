<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bloomberg L.P.  [# publisher id = 1202]
//Title      : Bloomberg News Service (Asia Edition) [ English ] 
//Created on : Aug 04, 2021, 10:29:29 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5962 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}