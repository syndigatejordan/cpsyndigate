<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Swazi Observer (Pty) Ltd  [# publisher id = 965]
//Title      : The Swazi Observer [ English ] 
//Created on : May 19, 2016, 11:36:44 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2766 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}