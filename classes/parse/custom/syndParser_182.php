<?php

//////////////////////////////////////////////////////////////////////////////
// Publisher: Hilal Publishing and Marketing Group
// Titles   : Gulf Industry
//////////////////////////////////////////////////////////////////////////////

class syndParser_182 extends syndParseAbTxtSample {

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'WINDOWS-1256';
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getHeadline(&$text, $headlineTagName = 'Headline') {
    //get article title
    $this->addLog("getting article headline");
    $headline = $this->textFixation($this->getItemValue($headlineTagName, $text));
    $this->removeItem($headlineTagName, $text);
    $headline = str_replace("â€ک", "‘", $headline);
    $headline = str_replace("â€™", "’", $headline);
    return $headline;
  }

  protected function getAuthor(&$text) {
    //get author
    $this->addLog("getting article author");
    $author = $this->textFixation($this->getItemValue('Dateline', $text));
    $this->removeItem('Dateline', $text);
    return $author;
  }

  protected function getStory(&$text) {
    $story = strip_tags(parent::getStory($text), '<br><BR><p><P><div><DIV>');
    $story = preg_replace("/\r\n|\r|\n/", "</p><p>", $story);
    return trim($story);
  }

  protected function getArticleDate(&$text, $dateTagName = 'Date') {
    //get article date
    $this->addLog("getting article date");
    $date = $this->getItemValue($dateTagName, $text);
    $this->removeItem($dateTagName, $text);
    $date = $this->getCurrentParsedFileDateFromFolder(); // TO Be removed wehen the Title fix the date issue in his XML files.
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  protected function getOriginalCategory(&$text) {
    $this->addLog("getting original article category");
    preg_match('/\(Category:(.*?)\)/is', $text, $cat);
    $text = str_replace($cat[0], "", $text);
    $cat = trim($cat[1]);
    return $cat;
  }

  protected function getImages(&$text) {
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      return array();
      // this contain just one image [ video image ]
      $this->addLog("getting video image");
      $image_Name = null;
      preg_match('/\(Image:(.*?)\)/is', $text, $image_Name);
      $text = str_replace($image_Name[0], "", $text);
      $image_Name = trim($image_Name[1]);
      $image_Name = explode("/", $image_Name);
      $image_Name = $this->nameToSafeFile(end($image_Name));
      $image_full_path = $this->currentDir . $image_Name;
      $original_name = explode('.', $image_Name);
    if (file_exists($image_full_path)) {
      //the video name is same as the image name here  		
      $name = $this->model->getImgReplacement($image_Name, $this->currentDir, 182);
      $img['img_name'] = str_replace(IMGS_PATH, IMGS_HOST, $name);
      $img['original_name'] = $original_name[0];
      $img['image_caption'] = $image_Name;
      $img['is_headline'] = true;
      $images[] = $img;
      //var_dump($images);exit;
      return $images;
    } else {

      return false;
    }
  }

  private function nameToSafeFile($name, $maxlen = 70) {
    $noalpha = 'ÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÀÈÌÒÙàèìòùÄËÏÖÜäëïöüÿÃãÕõÅåÑñÇç@°ºª';
    $alpha = 'AEIOUYaeiouyAEIOUaeiouAEIOUaeiouAEIOUaeiouyAaOoAaNnCcaooa';


    $name = substr($name, 0, $maxlen);
    $name = strtr($name, $noalpha, $alpha);

    // not permitted chars are replaced with "_"
    return preg_replace('/[^a-zA-Z0-9\._]/', '_', $name);
  }

}
