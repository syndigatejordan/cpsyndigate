<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : A Diario CR  [# publisher id = 1785]
//Title      : A Diario CR [ Spanish ] 
//Created on : Mar 08, 2022, 12:56:08 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12515 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}