<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Timor-Leste News Agency (ANTIL)  [# publisher id = 1429]
//Title      : Timor-Leste News Agency (ANTIL) [ Tetum ] 
//Created on : Oct 13, 2020, 10:18:39 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6662 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('tet'); 
	} 
}