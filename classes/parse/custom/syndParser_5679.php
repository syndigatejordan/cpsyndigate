<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BBC World Service  [# publisher id = 1192]
//Title      : BBC Arabic - Arts [ Arabic ] 
//Created on : Sep 14, 2021, 1:27:16 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5679 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}