<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Haymarket Media Ltd.  [# publisher id = 1604]
//Title      : FinanceAsia [ English ] 
//Created on : Oct 06, 2021, 7:19:32 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11949 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}