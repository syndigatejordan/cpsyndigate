<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Alakhbar  [# publisher id = 1369]
//Title      : Alakhbar [ French ] 
//Created on : Oct 05, 2020, 8:06:30 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6458 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}