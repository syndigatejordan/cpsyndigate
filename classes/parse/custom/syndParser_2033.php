<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Asianet-Pakistan (Pvt) Ltd.  [# publisher id =83 ] 
// Titles    : Daily Mirror
///////////////////////////////////////////////////////////////////////////
class syndParser_2033 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->charEncoding = 'UTF-8';
    //$this->extensionFilter = 'xml';
  }

  public function getRawArticles(&$fileContents) {
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('DigestName', $fileContents);
  }

  public function getStory(&$text) {

    $this->addLog("getting article text");
    $story = trim($this->getElementByName('Text', $text));
    $story = '<p>' . str_replace(".\r\n", ".\r\n" . "</p>\r\n<p>", $story) . '</p>';
    return $story;
  }

  public function getHeadline(&$text) {
    $this->addLog("getting article headline");

    return trim($this->getElementByName('Headline', $text));
  }

  public function getArticleDate(&$text) {
    $this->addLog("getting article date");

    $date = trim($this->getElementByName('Date', $text));
    return date('Y-m-d', strtotime($date));
  }

  public function getAuthor(&$text) {
    $this->addLog("getting article Source");

    return trim($this->getElementByName('Source', $text));
  }

}