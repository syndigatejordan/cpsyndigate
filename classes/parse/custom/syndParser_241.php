<?php 
///////////////////////////////////////////////////////////////////////////
// Publisher : Corporate Publishing International / IDG
// Titles    : Computer News Middle East [ Englsih ]
///////////////////////////////////////////////////////////////////////////

class syndParser_241 extends syndParseRss 
{
	
	public function customInit() {
		parent::customInit();
		$this->defaultLang  = $this->model->getLanguageId('en');		
	}
	
	public function getRawArticles(&$fileContents) {
		$this->addLog("Getting raw articles");
		return $this->getElementsByName('item', $fileContents);
	}
	
	protected function getArticleDate(&$text) {
		
		$this->addLog("Getting article date");
		$date = substr($this->getElementByName('pubDate', $text), 5, 11);
		return $this->dateFormater(date('Y-m-d', strtotime($date)));
	}
	
	protected function getStory(&$text) {
		$this->addLog("getting article body");
		
		$body = strip_tags($text, '<p><br /><a><br>');
		$body = str_replace('<p> </p>', '', $body);
		return $body;
	}

	protected function getAuthor(&$text) {
                $this->addLog("getting article author");
                return $this->textFixation($this->getCData($this->getElementByName('dc:creator', $text)));
        }

	
	protected function getImages(&$text) {	
		$body      = $this->textFixation($this->getCData($this->getElementByName('content:encoded', $text)));
		$bodyParts = explode('<div class="feedflare">', $body);
		$text      = $bodyParts[0];
					
		$images  = $this->getElementsByName('a',$text);
		$imagesArray = array();
		foreach ($images as $img) {
			$this->addLog("getting article images");
			
			$imagePath = syndParseHelper::getImgElements($img, 'img');
			if(is_array($imagePath)&& $imagePath) {
				$imagePath = $imagePath[0];
			}
			
			if(!$imagePath) {					
				continue;
			}
			if($this->checkImageifCached($imagePath)){
        // Image already parsed..
        continue;
      }	
			$copiedImage = $this->copyUrlImgIfNotCached($imagePath);
			if(!$copiedImage) {
				continue;
			}		
			
			$images 	= $this->getAndCopyImagesFromArray(array($copiedImage));
			array_push($imagesArray, $images[0]);
			
			$text = str_replace($img, '', $text);
		}
		
		return $imagesArray;
	}
	
} //End Class
