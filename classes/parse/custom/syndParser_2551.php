<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : SANEC  [# publisher id = 829]
//Title      : Southern African Netherlands Chamber of Commerce (SANEC) - News [ English ] 
//Created on : Feb 02, 2016, 12:12:01 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2551 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}