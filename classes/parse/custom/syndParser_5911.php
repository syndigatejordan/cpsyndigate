<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ROBOTS Association  [# publisher id = 1296]
//Title      : Robohub [ English ] 
//Created on : Sep 19, 2021, 8:52:59 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5911 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}