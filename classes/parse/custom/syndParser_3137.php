<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Times Group  [# publisher id = 1144]
//Title      : Times Online [ English ] 
//Created on : Nov 14, 2017, 2:11:40 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3137 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}