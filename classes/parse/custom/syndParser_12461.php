<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : MM ACTIV  [# publisher id = 1766]
//Title      : BioSpectrum Asia [ English ] 
//Created on : Feb 20, 2022, 11:50:19 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12461 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}