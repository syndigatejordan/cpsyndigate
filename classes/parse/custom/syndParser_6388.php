<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Oil and Gas Republic Publication  [# publisher id = 1351]
//Title      : Oil and Gas Republic [ English ] 
//Created on : Aug 24, 2021, 8:46:00 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6388 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}