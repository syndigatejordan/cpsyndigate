<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Business News for Press and Distribution  [# publisher id = 9]
//Title      : Alborsanews.com [ Arabic ] 
//Created on : Nov 28, 2021, 7:48:59 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1222 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}