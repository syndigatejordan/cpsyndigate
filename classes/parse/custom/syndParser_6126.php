<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Namibia Press Agency  [# publisher id = 1346]
//Title      : Namibia Press Agency (NAMPA) - Text Service [ English ] 
//Created on : Aug 15, 2021, 10:15:22 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6126 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}