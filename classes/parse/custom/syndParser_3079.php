<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : African Media Agency (AMA)  [# publisher id = 1111]
//Title      : African Media Agency (AMA) [ French ] 
//Created on : Sep 19, 2021, 7:05:47 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3079 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}