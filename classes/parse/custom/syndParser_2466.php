<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Federation of Palestinian Chambers of Commerce, Industry and Agriculture  [# publisher id = 786]
//Title      : Federation of Palestinian Chambers of Commerce, Industry and Agriculture (FPCCIA) News [ Arabic ] 
//Created on : Feb 01, 2016, 9:36:08 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2466 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}