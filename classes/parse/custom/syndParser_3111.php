<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence de Presse Sénégalaise  [# publisher id = 1119]
//Title      : Agence de Presse Senegalaise [ French ] 
//Created on : Aug 28, 2017, 7:44:34 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3111 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}