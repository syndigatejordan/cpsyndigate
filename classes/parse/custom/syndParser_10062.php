<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : FAMA Publishing  [# publisher id = 1527]
//Title      : FAMA MAGAZINE [ Spanish ] 
//Created on : Aug 04, 2021, 7:47:44 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10062 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}