<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Jadwa Investment  [# publisher id =696 ]  
//Title      : Jadwa Investment [ Arabic ] 
//Created on : Jan 04, 2016, 3:51:28 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_2164 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

    //Handle empty body
    public function parse() {
        $articles = array();
        foreach ($this->files as $file) {
            if ($this->extensionFilter) {
                if (!syndParseHelper::isFileExtension($file, $this->extensionFilter)) {
                    $this->addLog("Wrong extension for file : $file)");
                    continue;
                }
            }
            if (!file_exists($file)) {
                $this->addLog("file dose not exist: $file)");
                continue;
            }

            $this->addLog("get file contents (file:$file)");
            $fileContents = $this->getFileContents($file);

            if (!$fileContents) {
                continue;
            }

            $this->currentlyParsedFile = $file;
            $this->loadCurrentDirectory();
            $this->loadUpperDir();
            $rawArticles = $this->getRawArticles($fileContents);
            foreach ($rawArticles as $rawArticle) {
                $article = $this->getArticle($rawArticle);
                $articles[] = $article;
            }
        }
        return $articles;
    }
}
