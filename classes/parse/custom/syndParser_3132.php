<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : 24.com  [# publisher id = 1131]
//Title      : The Witness [ English ] 
//Created on : Sep 04, 2017, 7:14:57 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3132 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}