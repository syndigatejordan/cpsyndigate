<?php
///////////////////////////////////////////////////////////////////////////
// Publisher : Deutsche Welle
// Titles    : Deutsche Welle [Arabic]
///////////////////////////////////////////////////////////////////////////

class syndParser_596 extends syndParseRss {
	
	public function customInit() {
		parent::customInit();
		$this->defaultLang 	= $this->model->getLanguageId('ar');
		$this->extensionFilter = 'xml';		
	}

	public function getStory(&$text) {
		$this->addLog('Getting article story');
		$story = parent::getStory($text);
		
		//Invalid image at last 
		$imgPosition = strpos($story, '<img class="img"');
		if($imgPosition) {
			$story = substr($story, 0, $imgPosition);
		}
		return $this->replaceAmp($story);
	}
	
	public function getHeadline(&$text) {
		$headline = parent::getHeadline($text);		
		return $this->getCData($this->replaceAmp($headline));
	}
	
}
