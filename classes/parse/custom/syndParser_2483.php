<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Abu Dhabi Chamber of Commerce and Industry  [# publisher id = 789]
//Title      : Abu Dhabi Chamber of Commerce and Industry Annual Reports [ English ] 
//Created on : Sep 04, 2017, 8:15:30 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2483 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}