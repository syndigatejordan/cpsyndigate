<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agência de Información Paraguaya (IP)  [# publisher id = 1384]
//Title      : Agência de Información Paraguaya (IP) [ Spanish ] 
//Created on : Aug 15, 2021, 11:46:47 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6492 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}