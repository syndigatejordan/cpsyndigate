<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Jagran Prakashan Ltd  [# publisher id =564 ]
// Titles    : Jagran Post [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1627 extends syndParseXmlContent {
	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('en');
		$this -> extensionFilter = 'xml';
	}

	protected function getRawArticles(&$fileContents) {
		//get articles
		$this -> addLog("getting articles raw text");
		$d = $this -> getElementsByName('Articles', $fileContents);
		return $d;
	}

	public function getStory(&$text) {
		$this -> addLog('Getting article story');
		$body = trim($this -> getCData($this -> getElementByName('news_details', $text)));
		$body -> story = strip_tags($body, '<p><br><strong><b><u><i>');
		if (empty($body)) {
			return '';
		}
		return $body;
	}

	public function getHeadline(&$text) {
		$head = trim($this -> getCData($this -> getElementByName('news_title', $text)));
		return $head;
	}

	public function getArticleDate(&$text) {
		$date = trim($this -> getCData($this -> getElementByName('published_date', $text)));
		return date('Y-m-d', strtotime($date));
	}

	protected function getAuthor(&$text) {
		$this -> addLog("getting article author");
		$author = trim($this -> getCData($this -> getElementByName('author', $text)));
		return $author;
	}

}
