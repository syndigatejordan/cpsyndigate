<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CBRE Group  [# publisher id = 1233]
//Title      : CBRE Research and Reports [ English ] 
//Created on : Apr 22, 2019, 10:18:24 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5547 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}