<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IPS - Inter Press Service  [# publisher id = 1436]
//Title      : IPS - Inter Press Service [ German ] 
//Created on : Sep 12, 2021, 7:27:18 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6711 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('de'); 
	} 
}