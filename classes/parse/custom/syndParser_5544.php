<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Hilal Publishing and Marketing Group  [# publisher id = 4]
//Title      : Trade Arabia - Analysis, Interviews, Opinions [ English ] 
//Created on : Apr 17, 2019, 12:39:06 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5544 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
