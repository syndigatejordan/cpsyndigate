<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Elaph Publishing House Ltd.  [# publisher id =453 ]
// Titles    : Elaph [Arabic]
///////////////////////////////////////////////////////////////////////////

class syndParser_1549 extends syndParseXmlContent {
	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('es');
		$this -> extensionFilter = 'xml';
	}

	protected function getRawArticles(&$fileContents) {
		//get articles
		$this -> addLog("getting articles raw text");
		return $this -> getElementsByName('item', $fileContents);
	}

	public function getStory(&$text) {
		$this -> addLog('Getting article story');
		$this -> story = $this -> textFixation($this -> getCData($this -> getElementByName('content:encoded', $text)));
		$this -> story = strip_tags($this -> story, '<p><br><strong><b><u><i>');
		if (empty($this -> story)) {
			return '';
		}
		return $this -> story;
	}

	public function getHeadline(&$text) {
		return trim($this -> getElementByName('title', $text));
	}

	public function getArticleDate(&$text) {
		$date = trim($this -> getElementByName('pubDate', $text));
		return date('Y-m-d', strtotime($date));
	}

	protected function getAuthor(&$text) {
		$this -> addLog("getting article author");

		return trim($this -> textFixation($this -> getElementByName('dc:creator', $text)));
	}

	protected function getImages(&$text) {
		$story = trim($this -> textFixation($this -> getCData($this -> getElementByName('content:encoded', $text))));

		$imagesArray = array();
		preg_match_all("/<img[^>]+\>/i", $story, $imgs);

		foreach ($imgs[0] as $img) {
			$this -> addLog("getting article images");

			$imageInfo = syndParseHelper::getImgElements($img, 'img');
			$imagePath = $imageInfo[0];

			if (!$imagePath) {
				continue;
			}
			$copiedImage = $this -> copyUrlImgIfNotCached($imagePath);

			if (!$copiedImage) {
				echo "no pahr";
				continue;
			}
			$images = $this -> getAndCopyImagesFromArray(array($copiedImage));
			$images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
			$images[0]['is_headline'] = false;
			array_push($imagesArray, $images[0]);
		}
		return $imagesArray;
	}

}
