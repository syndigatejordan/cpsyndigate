<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Myanmar President Office News  [# publisher id = 1424]
//Title      : Myanmar President Office News [ English ] 
//Created on : Oct 13, 2020, 10:22:16 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6643 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}