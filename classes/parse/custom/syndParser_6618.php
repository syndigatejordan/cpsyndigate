<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : KHOVAR (National information agency of Tajikistan)  [# publisher id = 1418]
//Title      : KHOVAR (National information agency of Tajikistan) [ English ] 
//Created on : Sep 19, 2021, 8:56:19 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6618 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}