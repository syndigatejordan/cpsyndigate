<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Refinitiv / London Stock Exchange  [# publisher id = 1700]
//Title      : International Financing Review (IFR) by Refinitiv (Deals) [ English ] 
//Created on : Jan 13, 2022, 12:03:45 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12329 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}