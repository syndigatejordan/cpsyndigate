<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Jimmy Lloyd Media LLC  [# publisher id = 1182]
//Title      : Jimmy Lloyd TV [ English ] 
//Created on : Aug 03, 2021, 1:47:26 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3516 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}