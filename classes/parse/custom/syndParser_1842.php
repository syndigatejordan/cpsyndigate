<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Armoks Interactive Labs  [# publisher id = 653]
//Title      : Trak.in [ English ] 
//Created on : Jan 26, 2020, 12:08:36 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1842 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}