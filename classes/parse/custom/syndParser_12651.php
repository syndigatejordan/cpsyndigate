<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Boston Globe Media Partners, LLC  [# publisher id = 1817]
//Title      : Boston.com [ English ] 
//Created on : Jun 20, 2022, 9:40:44 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12651 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}