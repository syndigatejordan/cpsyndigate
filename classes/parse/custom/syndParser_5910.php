<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Institute for Policy Studies  [# publisher id = 1295]
//Title      : OtherWords [ English ] 
//Created on : Feb 10, 2020, 7:44:57 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5910 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
