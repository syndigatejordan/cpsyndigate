<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Islamic Republic of Afghanistan - Government Media & Information Center (GMIC)  [# publisher id = 1413]
//Title      : Islamic Republic of Afghanistan - Government Media & Information Center (GMIC) [ Pashto ] 
//Created on : Oct 20, 2020, 5:59:46 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6598 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ps'); 
	} 
}