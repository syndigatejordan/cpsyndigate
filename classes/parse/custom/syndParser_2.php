<?php

////////////////////////////////////////////////////////////////////////
// Publisher : Jordan Press & Publishing Company
// Titles    : Ad-Dustour
////////////////////////////////////////////////////////////////////////

class syndParser_2 extends syndParseRss {

  protected $cat_translation;
  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $cat = $this->getElementByName('title', $fileContents);
    $articles = $this->getElementsByName('item', $fileContents);
    foreach ($articles as &$article) {
      $article.="<cat>$cat</cat>";
    }
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = null;
    if (preg_match("/<title>(.*?)<\/title>/is", $text, $headline)) {
      return $this->textFixation($this->getCData($headline[1]));
    } else {
      return "";
    }
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");

    $date = $this->getElementByName('pubDate', $text);
    $date = date('Y-m-d', strtotime($date));

    return $date;
  }

  protected function getStory(&$text) {
      $this->addLog("getting article text");
      $checkDays = 3;
      $this->story = preg_replace('/<style(.*?)<\/style>/is', '', $this->story);
      $this->story = preg_replace('/<figure(.*?)<\/figure>/is', '', $this->story);
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
      //$this->story = str_replace("<p> </p>", "", $this->story);
      $this->story = trim(preg_replace('!\s+!', ' ', $this->story));

      $date = $this->getElementByName('pubDate', $text);
      $difference = strtotime(date("Y-m-d")) - strtotime($date);
      $difference = $difference / 24 / 60 / 60;
      if ($difference < $checkDays) {
          return $this->story;
      } else {
          $this->addLog("The article is older than 3 days");
          return "";
      }

  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getCData($this->getElementByName('dc:creator', $text)));
  }

  public function getOriginalCategory(&$text) {

    $this->addLog('Getting article category');


    return $this->textFixation($this->getCData($this->getElementByName('cat', $text)));
  }

  protected function getImages(&$text) {
      $imagesArray = array();
      $homepage = $this->getElementByName('homepage', $text);
      $this->story = $this->getCData($this->getElementByName('description', $text));
      $this->story = str_replace("&nbsp;", " ", $this->story);
      $this->story = $this->textFixation($this->story);

      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return $imagesArray;


      $image_caption = "";
      $imagePath = $this->getCData($this->getElementByName('fullImage', $text));
      if ($imagePath) {
          if (!$this->checkImageifCached($imagePath)) {
              $imagePath = str_replace(' ', '%20', $imagePath);
              $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
              if ($copiedImage) {
                  $images = $this->getAndCopyImagesFromArray(array($copiedImage));
                  if (!empty($image_caption)) {
                      $images[0]['image_caption'] = $image_caption;
                  }
          $name_image = explode('/images/', $copiedImage);
          if ($images[0]['image_caption'] == $name_image[1]) {
            $images[0]['image_caption'] = '';
          }
          $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
          $images[0]['is_headline'] = TRUE;
          array_push($imagesArray, $images[0]);
        }
      }
    }



    preg_match_all("/<img(.*?)>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
      if (!empty($image_caption[0])) {
        $image_caption = $image_caption[0];
      } else {
        $image_caption = "";
      }
      if (!$this->is_arabic($image_caption)) {
        $image_caption = iconv("UTF-8", "ISO-8859-1", $image_caption);
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      $imagePath = preg_replace("/\?(.*)/is", "", $imagePath);
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = sha1($imageUrl) . ".jpeg";

    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $imageUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

  private function uniord($u) {
    // i just copied this function fron the php.net comments, but it should work fine!
    $k = mb_convert_encoding($u, 'UCS-2LE', 'UTF-8');
    $k1 = ord(substr($k, 0, 1));
    $k2 = ord(substr($k, 1, 1));
    return $k2 * 256 + $k1;
  }

  private function is_arabic($str) {
    if (mb_detect_encoding($str) !== 'UTF-8') {
      $str = mb_convert_encoding($str, mb_detect_encoding($str), 'UTF-8');
    }

    /*
      $str = str_split($str); <- this function is not mb safe, it splits by bytes, not characters. we cannot use it
      $str = preg_split('//u',$str); <- this function woulrd probably work fine but there was a bug reported in some php version so it pslits by bytes and not chars as well
     */
    preg_match_all('/.|\n/u', $str, $matches);
    $chars = $matches[0];
    $arabic_count = 0;
    $latin_count = 0;
    $total_count = 0;
    foreach ($chars as $char) {
      //$pos = ord($char); we cant use that, its not binary safe
      $pos = $this->uniord($char);
      //echo $char . " --> " . $pos . PHP_EOL;

      if ($pos >= 1536 && $pos <= 1791) {
        $arabic_count++;
      } else if ($pos > 123 && $pos < 123) {
        $latin_count++;
      }
      $total_count++;
    }
    if (($arabic_count / $total_count) > 0.6) {
      // 60% arabic chars, its probably arabic
      return true;
    }
    return false;
  }

}
