<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : MONTSAME News Agency  [# publisher id = 1423]
//Title      : MONTSAME News Agency [ Chinese ] 
//Created on : Oct 17, 2020, 2:37:30 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6640 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh'); 
	} 
}