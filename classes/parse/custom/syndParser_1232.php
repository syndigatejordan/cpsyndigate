<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : C.G. Arabia W.L.L. (Arabian Magazines)  [# publisher id = 366]
//Title      : Areej [ Arabic ] 
//Created on : Feb 02, 2016, 11:51:17 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1232 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}