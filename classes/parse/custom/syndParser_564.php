<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ French ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_564 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority'		=>	'Pouvoir adjudicateur',
										'Nature of Contract'		=>	'Nature du contrat', 
										'Regulation of Procurement' => 	'Règlement des marchés publics', 
										'Type of bid required'		=> 	'Type de soumission requise', 
										'Award Criteria'			=>	'Critères d\'attribution',
										'Original CPV'				=>	'Original CPV', 
										'Country'					=>	'Pays',
										'Document Id' 				=>	'Document Id', 
										'Type of Document'			=>	'Type de Document',
										'Procedure'					=>	'Procédure', 
										'Original Language'			=>	'Langue d\'origine', 
										'Current Language'			=> 'Langue courante');	
		
		
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('fr');
		}
	}