<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Bawaba Middle East Limited  [# publisher id = 62]
//Title      : Albawaba.com - Arabic Entertainment [ Arabic ] 
//Created on : Aug 30, 2020, 7:49:35 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_6406 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

}
