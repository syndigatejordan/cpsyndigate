<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : G. Eric and Edith Matson Photograph Collection  [# publisher id = 1316]
//Title      : G. Eric and Edith Matson Photograph Collection [ English ] 
//Created on : Mar 03, 2020, 8:37:19 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5950 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
