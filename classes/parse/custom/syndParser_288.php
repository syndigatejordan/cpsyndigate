<?php

////////////////////////////////////////////////////////////////////////
// Publisher : Independent Television News Limited
// Titles    : ITN- Independent Video feeds
////////////////////////////////////////////////////////////////////////

class syndParser_288 extends syndParseXmlContent {

	private $articleDate 		= '';
	private $filesWillBeParsed 	= array( 'world.xml', 'headlines.xml', 'entertainment.xml', 'britain.xml' );
	private $filesShouldBeParsed= array();
	
	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('en');
		$this -> extensionFilter = 'xml';
		
		require_once('/usr/local/syndigate/lib/getid3/getid3/getid3.php');
	}

	public function beforeParse($params = array()) {

		parent::beforeParse($params = array());

		$titleHomeDir = $this -> title -> getHomeDir();

		foreach ( $this->files as $willParsedFile ) {
			$fileName = basename($willParsedFile);

			if ( substr($fileName, -7) !== 'New.xml' ) {
				if ( substr($fileName, -4) === '.xml' ) {
					if ( in_array($fileName, $this -> filesWillBeParsed ) ) {
						$this -> filesShouldBeParsed[] = $willParsedFile;
					}
				} else {
					$this -> filesShouldBeParsed[] = $willParsedFile;
				}
			}
		}

		$this -> files = $this -> filesShouldBeParsed;
	}

	protected function getRawArticles(&$fileContents)
	{
		$this->addLog("getting articles raw text");
		$articles 	= $this->getElementsByName('STORY', $fileContents);

		$xmlContent = simplexml_load_string($fileContents);
		
		//print_r($articles);
		//print_r($xmlContent);
		//exit;
		
		foreach ($articles as $id => &$article) {
			//$articleDate = $xmlContent -> STORY[$id] -> attributes() -> MOD_TIME;
			$date    =  '<DATE>' . $xmlContent -> STORY[$id] -> attributes() -> MOD_TIME . '</DATE>';
			$article =  $article . $date;
		}
		//print_r($articles);
		//exit;
		return $articles;
	}

	protected function getStory(&$text) {
		$this -> addLog("getting article text");
		
		$story = $this -> replaceAmp($this -> textFixation($this -> getElementByName('FULLTEXT', $text)));
		return str_replace(array('<PARA>', '</PARA>'), array('<p>', '</p>'), $story);
	}

	protected function getHeadline(&$text) {
		$this -> addLog("getting article headline");
		return $this -> textFixation($this -> getElementByName('TITLE', $text));
	}

	protected function getAbstract(&$text) {
		$this -> addLog("getting article summary");
		return $this -> textFixation($this -> getElementByName('TEASER', $text));
	}

	protected function getArticleDate(&$text) {

		$this -> articleDate = trim( $this -> textFixation($this -> getElementByName('DATE', $text)) );
		$this -> articleDate = explode('T', $this -> articleDate);
		$this -> articleDate = trim( $this -> articleDate[0] );

		$this->addLog("getting article date");
		$date = syndParseHelper::getDateFromPath($this->currentlyParsedFile);
		if($date) {
			return $this->dateFormater($date);
		}
		return parent::getArticleDate(&$text);
	}

	protected function getImages(&$text) {
		$this->addLog("getting article image");
		
		$image = syndParseHelper::getImgElements($text, 'IMAGE');

		//echo PHP_EOL . $this->articleDate . PHP_EOL;
		//echo PHP_EOL . $this->currentlyParsedFile . PHP_EOL;
		//echo PHP_EOL . $this->currentDir  . PHP_EOL;	

		if ( $image ) {
			if (file_exists($this -> currentDir . $image[0])) {
				return $this -> getAndCopyImagesFromArray(array($this -> currentDir . $image[0]));
			} else {
				$alternativePath = $this -> title -> getHomeDir() . str_replace('-', '/', $this->articleDate);
				$alternativePath = $alternativePath . DIRECTORY_SEPARATOR . $image[0];

				$this->addLog("article image does not exist in " . $this -> currentDir . $image[0]);
				$this->addLog("will try to get it from " . $alternativePath);

				if ( file_exists($alternativePath) ) {
					$this->addLog("article image founded in " . $alternativePath);
					return $this -> getAndCopyImagesFromArray(array( $alternativePath ));
				}

				$this->addLog("article image can not be found");
				return array();
			}
		}

		return array();
	}

	protected function getVideos(&$text) {
		$this->addLog("getting article Videos");

		$videos = syndParseHelper::getElements($text, 'VIDEO');
		/* print_r( $videos )
		 * Array (
		 *	[0] =>  BASENAME="itnp_af_swimming_1815_23" CATEGORY="broadband" SRC="itnp_af_swimming_1815_23_broadband_flv400.flv" ID="f9fdd0dc0d92405a9a1f77ec1984dc92" SIZE="3131844" FORMAT="flv400" BITRATE="464kbps" DURATION="50s" MIME_TYPE="video/x-flv
		 *	[1] =>  BASENAME="itnp_af_swimming_1815_23" CATEGORY="broadband" SRC="itnp_af_swimming_1815_23_broadband_android.mp4" ID="f9fdd0dc0d92405a9a1f77ec1984dc92" SIZE="2275337" FORMAT="android" BITRATE="700kbps" DURATION="50s" MIME_TYPE="application/unknown
		 * )
		 */

		$videoCaption = $this -> textFixation($this -> getElementByName('VIDEO', $text));

		$vids = null;
		foreach ($videos as &$video) {

			$video = preg_replace("/ +/", " ", $video);
			$video = str_replace(array("\r", "\n"), '', $video);
			$video = explode(' ', $video);

			$atts = array();
			foreach ($video as &$videoAtt) {
				if (trim($videoAtt)) {
					$videoAtt = explode('=', $videoAtt);
					$atts[$videoAtt[0]] = str_replace('"', '', $videoAtt[1]);
				}
			}
			$vids[] = $atts;
		}

		/* print_r( $vids )
		 * Array
		 *	(
		 *	    [0] => Array
		 *	        (
		 *	            [BASENAME] => itnp_af_swimming_1815_23
		 *	            [CATEGORY] => broadband
		 *	            [SRC] => itnp_af_swimming_1815_23_broadband_flv400.flv
		 *	            [ID] => f9fdd0dc0d92405a9a1f77ec1984dc92
		 *	            [SIZE] => 3131844
		 *	            [FORMAT] => flv400
		 *	            [BITRATE] => 464kbps
		 *	            [DURATION] => 50s
		 *	            [MIME_TYPE] => video/x-flv
		 *	        )
		 *
		 *	    [1] => Array
		 *	        (
		 *	            [BASENAME] => itnp_af_swimming_1815_23
		 *	            [CATEGORY] => broadband
		 *	            [SRC] => itnp_af_swimming_1815_23_broadband_android.mp4
		 *	            [ID] => f9fdd0dc0d92405a9a1f77ec1984dc92
		 *	            [SIZE] => 2275337
		 *              [FORMAT] => android
		 *	            [BITRATE] => 700kbps
		 *	            [DURATION] => 50s
		 *	            [MIME_TYPE] => application/unknown
		 *	        )
		 *
		 *	)
		 */

		$videos = array();
		foreach ($vids as $vid) {
			
			//$this->currentDir = str_replace($this->title->getWorkDir(),  $this->title->getHomeDir(), $this->currentDir);
			
			$videoName = $this -> currentDir . $vid['SRC'];  //$this->title->getHomeDir()
				//echo PHP_EOL . PHP_EOL . '$videoName: ' . $videoName . PHP_EOL; exit;
				
				
			
			// Begin block to check the file that exist in current directory
			// if not get the article date, then search in the date directory for the article
			if (file_exists($videoName)) {
				$videoName = $videoName;
			} else {
				$alternativePath = $this -> title -> getHomeDir() . str_replace('-', '/', $this->articleDate);
				$alternativePath = $alternativePath . DIRECTORY_SEPARATOR . $vid['SRC'];

				$this->addLog("article video does not exist in " . $this -> currentDir . $vid['SRC']);
				$this->addLog("will try to get it from " . $alternativePath);
				
				if ( file_exists($alternativePath) ) {
					$this->addLog("article video founded in " . $alternativePath);
					$videoName = $alternativePath;
				} else {
					$this->addLog("article video can not be found");
					return array();
				}
			}
			// End block

			if ( ! file_exists($videoName)) {
				return array();
				continue;
			}


			if (file_exists($videoName)) {

				$videoInformation = array();
				$neededInfo 	  = array('filesize', 'fileformat', 'encoding', 'mime_type', 'playtime_seconds', 'bitrate', 'playtime_string', 'video' );

				$getID3 		= new getID3;
				$realVideoPath 	= str_replace($this->title->getParseDir(),  $this->title->getHomeDir(), $videoName);
				$videoInfo 		= $getID3->analyze($realVideoPath);	


				foreach ($neededInfo as $key => $value) {

					if ( in_array($value, $videoInfo) ) {
						switch ( $value ) {
							case 'video':
								$videoInformation['width']  = $videoInfo[$value]['resolution_x'];
								$videoInformation['height'] = $videoInfo[$value]['resolution_y'];
								break;

							case 'bitrate':
								$videoInformation[$value] = floor ( $videoInfo[$value] / 1024 );
								break;

							default:
								$videoInformation[$value] = $videoInfo[$value];
								break;
						}
					}

				}
				
				$finalVideoInformation = '';
				if ( empty( $videoInformation ) ) {
					$bitrate = substr($vid['BITRATE'], 0, strpos($vid['BITRATE'], 'kbps') );
					$videoInformation['bitrate'] = $bitrate;
				}
				
				$finalVideoInformation = json_encode($videoInformation);

				//$xx = $this->getAndCopyVideosFromArray( $videoName );
				//$name  = $this->model->getVidReplacement(basename($videoName), $this->currentDir, 288) ;

				$video = array();
				//$video['video_name']    	= str_replace(VIDEOS_PATH, VIDEOS_HOST, $name);
				$video['video_name'] = $this -> getAndCopyVideosFromArray($videoName);
				//$vid['BASENAME'];  //$vid['SRC'];
				$video['original_name'] = $videoName;
				$video['video_caption'] = $vid['SRC'];
				//$vid['BASENAME'];
				$video['bit_rate'] = $finalVideoInformation;   // json_encode($videoInformation);  //$vid['BITRATE'];
				//(int) $vid['BITRATE'] . 'k';
				$video['added_time'] = date('Y-m-d h:i:s', filemtime($video['original_name']));

				$video['mime_type'] = $vid['MIME_TYPE'];
				$video['video_type'] = $vid['FORMAT'];
				$videos[] = $video;

				//copy($videoName, $videoName . '.old');
			} else {
				echo "video dose not exist $videoName \n";
			}
		}
		return $videos;
	}

}
?>
