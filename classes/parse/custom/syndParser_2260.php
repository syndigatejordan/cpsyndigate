<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence France Presse  [# publisher id = 716]
//Title      : Agence France-Presse (AFP) [ English ] 
//Created on : Mar 31, 2019, 8:04:46 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2260 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
