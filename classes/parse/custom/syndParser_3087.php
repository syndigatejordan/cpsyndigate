<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Motivate publishing  [# publisher id = 116]
//Title      : What's On Dubai [ English ] 
//Created on : Sep 05, 2017, 12:20:38 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3087 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}