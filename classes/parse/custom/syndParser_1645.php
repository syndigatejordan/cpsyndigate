<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Cyber Media (India) Ltd  [# publisher id = 570]
//Title      : DQ Channels  [ English ] 
//Created on : Aug 02, 2021, 8:11:24 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1645 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}