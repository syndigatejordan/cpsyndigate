<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Republic of Iraq Ministry of Foreign Affairs  [# publisher id = 839]
//Title      : Republic of Iraq Ministry of Foreign Affairs - Minister News [ English ] 
//Created on : Jan 30, 2016, 8:30:45 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2589 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}