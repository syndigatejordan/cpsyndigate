<?php

	////////////////////////////////////////////////////////////////////////
	// Publisher : Dar Al Hayat
	// Titles    : Dar Al Hayat English
	////////////////////////////////////////////////////////////////////////


	//class syndParser_20 extends syndParseContent
	class syndParser_20 extends syndParseAbTxtSample
	{

		public function customInit()
		{
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('en');
			$this->charEncoding = 'UTF-16';
			$this->extensionFilter = 'txt';
		}
		
		
		protected function getRawArticles(&$fileContents)
		{
			return array($fileContents);
		}

		protected function getStory(&$text)
		{
			//get article text
			$this->addLog("getting article text");
			$regExp = '\([\s]*Page[\s]*:([^\)]*)[\s]*\)';
			$story = preg_split("/$regExp/i", $text);
			$story = isset($story[1]) ? $story[1] : '';
			return $this->textFixation($story);
		}

		protected function getHeadline(&$text)
		{
			//get article title
			$this->addLog("getting article headline");
			return $this->textFixation($this->getItemValue('Title', $text));
		}

		protected function getArticleDate(&$text)
		{
			//get article date
			$this->addLog("getting article date");
			$date = $this->getItemValue('Date', $text);
			$dateAr = explode('-', $date);
			if(count($dateAr) == 3) {
				$date = trim($dateAr[2]).'-'.trim($dateAr[1]).'-'.trim($dateAr[0]);
			}
			if($date) {
				return $this->dateFormater($date);
			}
		}

		protected function getOriginalData(&$text)
		{
			$originalData['extras']['source'] = $this->getItemValue('Source', $text);
			$originalData['hijri_date']       = $this->getItemValue('Hijri date', $text);
			$originalData['original_source']  = $this->getItemValue('Original source', $text);
			$originalData['issue_number']     = $this->getItemValue('Issue', $text);
			$page = $this->getItemValue('Page', $text);
			$page = explode('-', $page);
			$page = isset($page[0]) ? trim($page[0]) : 0;
			$originalData['page_number'] = $page;
			return $originalData;
		}


		protected function getAuthor(&$text)
		{
			//get author
			$this->addLog("getting article author");
			return $this->textFixation($this->getItemValue('Author', $text));
		}

		protected function getItemValue($name, $text)
		{

			$regExp = '\([\s]*'.$name."[\\s]*:([^\n]*)[\\s]*\\)";
			if(preg_match("/$regExp/i", $text, $matches)) {
				return $matches[1];
			}
			return null;
		}
		
		
		

		/*

		protected function getRawArticles(&$fileContents)
		{
			return array($fileContents);
		}

		protected function getStory(&$text)
		{
			//get article text
			$this->addLog("getting article text");
			$regExp = '\([\s]*Page[\s]*:([^\)]*)[\s]*\)';
			$story = preg_split("/$regExp/i", $text);
			$story = isset($story[1]) ? $story[1] : '';
			return $this->textFixation($story);
		}

		protected function getHeadline(&$text)
		{
			//get article title
			$this->addLog("getting article headline");
			return $this->textFixation($this->getItemValue('Title', $text));
		}

		protected function getArticleDate(&$text)
		{
			//get article date
			$this->addLog("getting article date");
			$date = $this->getItemValue('Date', $text);
			$dateAr = explode('-', $date);
			if(count($dateAr) == 3) {
				$date = trim($dateAr[2]).'-'.trim($dateAr[1]).'-'.trim($dateAr[0]);
			}
			if($date) {
				return $this->dateFormater($date);
			}
		}

		protected function getOriginalData(&$text)
		{
			$originalData['extras']['source'] = $this->getItemValue('Source', $text);
			$originalData['hijri_date']       = $this->getItemValue('Hijri date', $text);
			$originalData['original_source']  = $this->getItemValue('Original source', $text);
			$originalData['issue_number']     = $this->getItemValue('Issue', $text);
			$page = $this->getItemValue('Page', $text);
			$page = explode('-', $page);
			$page = isset($page[0]) ? trim($page[0]) : 0;
			$originalData['page_number'] = $page;
			return $originalData;
		}


		protected function getAuthor(&$text)
		{
			//get author
			$this->addLog("getting article author");
			return $this->textFixation($this->getItemValue('Author', $text));
		}

		protected function getItemValue($name, $text)
		{

			$regExp = '\([\s]*'.$name."[\\s]*:([^\n]*)[\\s]*\\)";
			if(preg_match("/$regExp/i", $text, $matches)) {
				return $matches[1];
			}
			return null;
		}
		*/
	}