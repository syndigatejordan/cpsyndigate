<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Organization of Islamic Cooperation (OIC)  [# publisher id = 1209]
//Title      : The Union of News Agencies (UNA) [ English ] 
//Created on : Feb 14, 2019, 8:05:23 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5358 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}