<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Damascus Chamber of Industry  [# publisher id = 802]
//Title      : Damascus Chamber of Industry News [ Arabic ] 
//Created on : Jan 31, 2016, 1:43:31 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2460 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}