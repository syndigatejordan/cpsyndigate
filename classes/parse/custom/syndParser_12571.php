<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Euronews  [# publisher id = 604]
//Title      : Euronews [ German ] 
//Created on : Apr 10, 2022, 8:42:56 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12571 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('de'); 
	} 
}