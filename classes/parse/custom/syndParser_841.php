<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: China Economic Review Publishing
	// Titles   : CHINAinsight [English]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_841 extends syndParseContent
	{	
		
	       public function customInit() {
		  parent::customInit();
		  $this->defaultLang = $this->model->getLanguageId('en');
	       }
	
		protected function getRawArticles(&$fileContents) {			
			$this->addLog('Get raw articles');	
			$fileContents = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $fileContents);
			return array($fileContents);
		}

		protected function getStory(&$text) {
			$this->addLog("getting article body");

			$body = SyndParseHelper::getStringAfter('[[QQ]]',$text);
			$body = str_replace('[over]','', $body);
			$body = str_replace('[OVER]','', $body);
			$body = nl2br($body);
			return $body;
		}
		protected function getHeadline(&$text) {	
			$this->addLog("getting article headline");

			$headline = SyndParseHelper::getStringsBetween('[[HH]]',"\n",$text);
			if($headline) {
			    return $headline[0];	
			}
			return '';
		}

		
		
		protected function getOriginalCategory(&$text) {
			//get article category
			$this->addLog("getting article original category");
			$category = SyndParseHelper::getStringsBetween('[[CT]]',"\n",$text);
			
			if($category) {
				return $category[0];
			}
			return '';
			
		}
}
		
