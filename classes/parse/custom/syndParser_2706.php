<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dubai Media Incorporated  [# publisher id = 913]
//Title      : Al Bayan [ Arabic ] 
//Created on : May 19, 2016, 11:09:04 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2706 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = strip_tags($this->story, "<p><span><ul><li>");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
