<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Yeni Gün Haber Ajansi Basin Ve Yayincilik A.Ş.   [# publisher id = 1312]
//Title      : Cumhuriyet Video [ Turkish ] 
//Created on : Sep 27, 2021, 10:54:13 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5945 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('tr'); 
	}

    protected function getVideos(&$text) {
        $this->addLog("Getting videos");
        $videos = array();
        $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));

        $date = $this->getElementByName('date', $text);
        $date = date('Y-m-d h:i:s', strtotime($date));
        $story = $this->getCData($this->getElementByName('fulltext', $text));
        $story = str_replace("&nbsp;", " ", $story);
        $story = $this->textFixation($story);
        $story = trim(preg_replace('!\s+!', ' ', $story));
        $matches = null;
        preg_match_all('/<iframe(.*?)<\/iframe>/is', $story, $matches);
        foreach ($matches[0] as $match) {
            $video = array();
            $videoname = syndParseHelper::getImgElements($match, 'iframe', 'src');
            $videoname = $videoname[0];
            if (preg_match("/youtube.com/", $videoname)) {
                continue;
            }
            $mimeType = "";
            $video['video_name'] = $videoname;
            $video['original_name'] = $videoName;
            $video['video_caption'] = $videoName;
            $video['mime_type'] = $mimeType;
            $video['added_time'] = $date;
            $videos[] = $video;
        }
        return $videos;
    }

    //Handle empty body
  public function parse() {
    $articles = array();
    foreach ($this->files as $file) {
      if ($this->extensionFilter) {
        if (!syndParseHelper::isFileExtension($file, $this->extensionFilter)) {
          $this->addLog("Wrong extension for file : $file)");
          continue;
        }
      }
      if (!file_exists($file)) {
        $this->addLog("file dose not exist: $file)");
        continue;
      }

      $this->addLog("get file contents (file:$file)");
      $fileContents = $this->getFileContents($file);

      if (!$fileContents) {
        continue;
      }

      $this->currentlyParsedFile = $file;
      $this->loadCurrentDirectory();
      $this->loadUpperDir();
      $rawArticles = $this->getRawArticles($fileContents);
      foreach ($rawArticles as $rawArticle) {
        $article = $this->getArticle($rawArticle);
        $articles[] = $article;
      }
    }
    return $articles;
  }
 
}
