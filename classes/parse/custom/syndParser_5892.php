<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Chalkbeat Inc.  [# publisher id = 1276]
//Title      : Chalkbeat [ English ] 
//Created on : Jan 02, 2020, 9:51:39 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5892 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}