<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Afghanistan Group of Newspaper  [# publisher id = 1660]
//Title      : Daily Outlook Afghanistan [ English ] 
//Created on : Oct 06, 2021, 6:16:38 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12215 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}