<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : The Arab-Irish Chamber of Commerce  [# publisher id =776 ]   
//Title     : The Arab-Irish Chamber of Commerce news [ English ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2415 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

}
