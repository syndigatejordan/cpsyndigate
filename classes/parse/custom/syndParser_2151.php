<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Kalem Content Publishing  [# publisher id = 684]
//Title      : Abu Nawaf Network [ Arabic ] 
//Created on : Oct 07, 2021, 12:03:27 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2151 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}