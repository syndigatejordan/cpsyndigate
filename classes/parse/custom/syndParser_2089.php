<?php

/////////////////////////////////////////////////////////////////////////////////////
////Publisher  : Al Bawaba (Middle East) Ltd.  [# publisher id = 62]
////Title      :Albawaba.com Slideshows [ Arabic ]
////Created on : Jun 28, 2021, 8:11:46 AM
////Author     : eyad
///////////////////////////////////////////////////////////////////////////////////////

class syndParser_2089 extends syndParseCms
{

    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('ar');
    }

    public function getStory(&$text)
    {
        $this->story = preg_replace('!\s+!', ' ', $this->story);
        $this->story = preg_replace('/<slideshow_images>(.*?)<\/slideshow_images>/is', '', $this->story);
        $this->story = preg_replace('/<image_original_alt>(.*?)<\/image_original_alt>/is', '', $this->story);
        $this->story = preg_replace('/<image_thumb>(.*?)<\/image_thumb>/is', '', $this->story);
        $this->story = preg_replace('/<image_original>(.*?)<\/image_original>/is', '', $this->story);
        $this->story = preg_replace('!\s+!', ' ', $this->story);
        return $this->story;
    }


    protected function getImages(&$text)
    {
        $this->story = $this->getCData($this->getElementByName('fulltext', $text));
        $this->story = str_replace("&amp;", "&", $this->story);
        $this->story = $this->textFixation($this->story);
        $imagesArray = array();
        $matches = null;
        preg_match_all('/<image>(.*?)<\/image>/s', $this->story, $matches);
        foreach ($matches[1] as $matche) {
            $this->addLog("getting article images");
            ///////////////////Get image original//////////////////////
            preg_match("/<image_url>(.*?)<\/image_url>/i", $matche, $imagePath);
            $imagePath = $this->getCData($imagePath[1]);
            preg_match("/<image_alt>(.*?)<\/image_alt>/i", $matche, $image_caption);
            $image_caption = $this->getCData($image_caption[1]);
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption))
        $images[0]['image_caption'] = $image_caption;
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);

      ///////////////////Get thumb image //////////////////////
      $this->addLog("getting article thumb images");
      $imagePath = "";
      $copiedImage = "";
      preg_match("/<image_thumb_url>(.*?)<\/image_thumb_url>/i", $matche, $imagePath);
      $imagePath = $this->getCData($imagePath[1]);
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgSlideShowIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption))
        $images[0]['image_caption'] = $image_caption;
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    ///////////////////Get image original//////////////////////
    $this->addLog("getting article images");
    $imagePath = "";
        $copiedImage = "";
        preg_match("/<image_original>(.*?)<\/image_original>/i", $this->story, $imagePath);
        $imagePath = $this->getCData($imagePath[1]);
        preg_match("/<image_original_alt>(.*?)<\/image_original_alt>/i", $this->story, $image_caption);
        $image_caption = $this->getCData($image_caption[1]);
    if (!$imagePath) {
      return $imagesArray;
    }
    if ($image = $this->checkImageifCached($imagePath)) {
      // Image already parsed..
      return $imagesArray;
    }
    $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

    if (!$copiedImage) {
      echo "no pahr";
      return $imagesArray;
    }
    $images = $this->getAndCopyImagesFromArray(array($copiedImage));
    if (!empty($image_caption))
      $images[0]['image_caption'] = $image_caption;
    $images[0]['is_headline'] = false;
    array_push($imagesArray, $images[0]);
///////////////////Get thumb image original//////////////////////
    $this->addLog("getting article thumb images");
    $imagePath = "";
        $copiedImage = "";
        preg_match("/<image_thumb>(.*?)<\/image_thumb>/i", $this->story, $imagePath);
        $imagePath = $this->getCData($imagePath[1]);
    $imagePath = str_replace(' ', '%20', $imagePath);
    if (!$imagePath) {
      return $imagesArray;
    }
    if ($image = $this->checkImageifCached($imagePath)) {
      // Image already parsed..
      return $imagesArray;
    }
    $copiedImage = $this->copyUrlImgSlideShowIfNotCached($imagePath);
    if (!$copiedImage) {
      echo "no pahr";
      return $imagesArray;
    }
    $images = $this->getAndCopyImagesFromArray(array($copiedImage));
    if (!empty($image_caption))
      $images[0]['image_caption'] = $image_caption;
    $images[0]['is_headline'] = false;
    array_push($imagesArray, $images[0]);


    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $imageUrl = preg_replace("/\?(.*)/is", "", $imageUrl);
    $baseName = basename($imageUrl);
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $imageUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

  public function copyUrlImgSlideShowIfNotCached($imageUrl) {
     $imageUrl = preg_replace("/\?(.*)/is", "", $imageUrl);
    $baseName = 'thumb_' . basename($imageUrl);
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $imageUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

// End function copyUrlImgIfNotCached
}
