<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Middle East Monitor  [# publisher id = 1266]
//Title      : The Middle East Monitor - Destinations [ English ] 
//Created on : Oct 13, 2019, 10:39:12 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5840 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}