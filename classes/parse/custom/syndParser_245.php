<?php
	////////////////////////////////////////////////////////////////////////
	// Publisher : Pakistan Press International (PPI) 
	// Titles    : Pakistan Press International (PPI) [English]
	////////////////////////////////////////////////////////////////////////

	class syndParser_245 extends syndParseXmlContent 
	{

		public function customInit()
		{
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('en');
			$this->charEncoding = 'ISO-8859-1';
			$this->extensionFilter = 'txt';
		}


		protected function getRawArticles(&$fileContents)
		{				
			
			$fileContents = str_replace("â", "“", $fileContents);
			$fileContents = str_replace("â", "”", $fileContents);
			$fileContents = str_replace("â", "’", $fileContents);
			$fileContents = str_replace("â", "‘", $fileContents);
			$fileContents = str_replace("â¬", "€", $fileContents);
			
			$this->addLog("getting articles raw text");
			
			$articles = explode("ZCZC", $fileContents);
			unset($articles[count($articles)-1]);
			unset($articles[0]);
			
			return $articles;
		}
		
		protected function getHeadline(&$text) {
			$this->addLog("Getting article headline");
			return trim($this->textFixation($this->getElementByName('headline', $text)));
		}
		
		protected function getStory(&$text) {
			$this->addLog("Getting article body");
			
			$story	 =	"<p>";
			$story  .=	trim($this->textFixation($this->getElementByName('Text', $text)));
			$story	 =	preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "</p>\n<p>", $story);
			$story	.=	"</p>";
			$story	 = str_replace("<p></p>", "", $story);		
			
			return $story;
		}		
		
		protected function getArticleDate(&$text){
			$this->addLog("Getting article date");
			
			$date = trim($this->textFixation($this->getElementByName('Date', $text)));
			$date = explode(' ', $date);
			return $this->dateFormater(date('Y-m-d', strtotime($date[1] . " " . $date[0] . " " . date('Y'))));
		}
		
	}
	
