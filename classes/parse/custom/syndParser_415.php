<?php

/**
 * Publisher : IRIN (United Nations Integrated Regional Information Networks)
 * Title     : PlusNews French service [ French ]
 * 
 * -----------------------------------------------------------------------------------------------------
 */
 
 /*
  * The content will be moved to 405 so it will always be zero count
  */

class syndParser_415 extends syndParseRss  {
	
	public function customInit() {
		parent::customInit();
		$this->defaultLang = $this->model->getLanguageId('fr');
		$this->charEncoding = 'ISO-8859-1';
		$this->extensionFilter = 'xml';
	}
	
	public function beforeParse($params = array()) {
		
		parent::beforeParse($params = array());
		
		$c = new Criteria();
		$c->add(TitlePeer::ID , 405);
		$targetTitle = TitlePeer::doSelectOne($c);
		
		if($targetTitle) {
			$files = scandir($this->title->getHomeDir());
			
			foreach ($files as $file) {

				if($file!= '.' && $file !='..') {
					if(!file_exists($targetTitle->getHomeDir() . $file)) {
						$this->addLog("moving " . $this->title->getHomeDir() . $file . " to " . $targetTitle->getHomeDir());
						copy($this->title->getHomeDir() . $file, $targetTitle->getHomeDir() . $file);
					}
				}

			}
		}
		
	}

	public function getRawArticles(&$fileContents) {
		return array();
		/*
		$fileContents	= str_replace("", "'", $fileContents);
		$fileContents	= str_replace("", "\"", $fileContents);
		$fileContents	= str_replace("", "\"", $fileContents);
		$fileContents	= str_replace("", "", $fileContents);
		$fileContents	= str_replace("", "'", $fileContents);
 		$fileContents   = str_replace("", "œ", $fileContents);
		$fileContents   = str_replace("", "", $fileContents);

		return parent::getRawArticles($fileContents);
		*/
	}



	protected function getStory(&$text) {	
		$this->addLog("getting article text");
		return $this->textFixation($this->getElementByName('body', $text));
	}

	public function getAbstract(&$text) {
		$this->addLog('Getting article summary');
		return $this->textFixation($this->getElementByName('description', $text));
	}
	
	public function getArticleDate(&$text) {
		$this->addLog("getting article date");
		$date = $this->getElementByName('date', $text);
		$date = substr( $date, strpos($date, ',') +2);
		$date = date('Y-m-d', strtotime($date));
		return $this->dateFormater($date);
	}
}
