<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Turan Information Agency  [# publisher id = 424]
//Title      : Turan Information Agency - Economical Review [ Russian ] 
//Created on : Aug 02, 2021, 11:51:13 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1758 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}