<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sundiata Post Media Ltd.  [# publisher id = 1815]
//Title      : Sundiata Post [ English ] 
//Created on : Jun 19, 2022, 12:52:30 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12647 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}