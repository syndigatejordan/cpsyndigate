<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Infomedia / Opoint Technology  [# publisher id = 1534]
//Title      : Global News Firehose (Japanese) [ Japanese ] 
//Created on : Sep 23, 2021, 12:59:22 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10230 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	} 
}