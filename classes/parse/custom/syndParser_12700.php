<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : linfodrome.ci  [# publisher id = 1840]
//Title      : linfodrome.ci [ French ] 
//Created on : Aug 31, 2022, 1:19:39 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12700 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}