<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Nation Media Group Limited  [# publisher id = 338]
//Title      : NTV Kenya [ English ] 
//Created on : Sep 26, 2021, 11:50:33 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3480 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}