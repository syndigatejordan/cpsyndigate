<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: TBREAK MEDIA
	// Titles   : Middle East Gamers[ Arabic]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_816 extends syndParseRss
	{
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('ar');
		}				
		
		public function getRawArticles(&$fileContents) { 
			$articles = parent::getRawArticles($fileContents);
			$match = array();
			$sourceArticles = array();

			foreach ($articles as $article) {
			
			  preg_match("/Source: (.*)/", $article, $match);
				
			  if(count($match)==0) {
				$sourceArticles [] = $article;
		          }
			} 
			return $sourceArticles;
		}
		protected function getHeadline(&$text) {
			$this->addLog("getting article headline");	
			return $this->textFixation($this->getCData($this->getElementByName('headline', $text)));
		}

		protected function getStory(&$text) {			
			$this->addLog("getting article text");			
			$body = str_replace('<![CDATA[', '', $this->getCData($this->getElementByName('description', $text)));	
			$body = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "<br>", $body);
			
			return $this->textFixation($body);
		}

	}
