<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Islamic Republic of Afghanistan - Government Media & Information Center (GMIC)  [# publisher id = 1413]
//Title      : Islamic Republic of Afghanistan - Government Media & Information Center (GMIC) [ Dari ] 
//Created on : Oct 20, 2020, 6:00:07 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6599 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('prs'); 
	} 
}