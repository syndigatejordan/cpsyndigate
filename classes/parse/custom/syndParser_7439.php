<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : London Stock Exchange.  [# publisher id = 1489]
//Title      : London Stock Exchange [ English ] 
//Created on : Dec 17, 2020, 12:35:32 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7439 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}