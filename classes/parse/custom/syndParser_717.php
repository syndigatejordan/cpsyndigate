<?php
require_once dirname(__FILE__) . '/syndParserPublisher_204.php';

class syndParser_717 extends syndParserPublisher_204 {
	
	
	public function getRawArticles(&$fileContents) {				
		$this->addLog("getting raw articles text");		
		$xml = simplexml_load_string($fileContents);
		
		
		$articles = array();
		foreach ($xml->Projects as $t) {			
			unset($article);

			$article['original_id'] 		= (int)$t->attributes()->Project_ID;
			$article['headline'] 			= (string)$t->attributes()->Subject;
			$article['date']	 			= (string)$t->attributes()->Date_Published;
			$article['body']	 			= (string)$t->attributes()->Description;
			
			$article['extra']['Country']   = (string)$t->attributes()->Country_Name;			
			$article['extra']['Expiry_Date']   = (string)$t->attributes()->Expiry_Date;			
			$article['extra']['Location']   = (string)$t->Location->attributes()->Location_Name;
			
			
			foreach ($t->Projects_Sub_Sectors_View as $attribute) {								
				$article['extra']['sectors'][] = array( 'sector'=> $this->sectors[(int)$attribute->attributes()->Sector_ID]  , 
														'sub_sector'=> $this->subSectors[ (int)$attribute->attributes()->Sub_Sector_ID ]['Sub_Sector_Name']); 
			}
			$articles[] = $article;					
		}
		return $articles;
	}
	
	
	protected function getStory(&$text) {
		$this->addLog('Getting article story');				
		
		$story = $this->textFixation($text['body']);		
		$story .= "<br /><br />\n";
				
		$this->addToBody('Country', $text['extra']['Country'], $story);
		$this->addToBody('Location', $text['extra']['Location'], $story);
		$this->addToBody('Expiry Date', $text['extra']['Expiry_Date'], $story);				
		
		if($text['extra']['sectors']) {
			$story .= "<br /><b>Sectors : </b><br /><br />\n";
			foreach ($text['extra']['sectors'] as $sector) {
				$story.= $sector['sector'] . '/' . $sector['sub_sector'] . "<br />\n";
			}			
		}
		
		return $story;
	}
	
}
