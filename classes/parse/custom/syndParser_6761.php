<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Pacific Islands News Association (PINA)  [# publisher id = 1442]
//Title      : Pacific Islands News Association (PINA) [ English ] 
//Created on : Oct 15, 2020, 1:35:25 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6761 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}