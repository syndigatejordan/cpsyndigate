<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Hong Kong General Chamber of Commerce  [# publisher id = 852]
//Title      : Hong Kong General Chamber of Commerce (HKGCC) - News [ English ] 
//Created on : Feb 08, 2016, 1:11:46 PM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2609 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}