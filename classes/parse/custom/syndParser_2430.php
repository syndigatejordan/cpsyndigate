<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : SPA El Khabar  [# publisher id = 784]
//Title      : El Khabar [ Arabic ] 
//Created on : Sep 23, 2021, 7:54:00 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2430 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}