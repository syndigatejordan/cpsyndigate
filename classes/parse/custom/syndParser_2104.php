<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Shohob for Web Development  [# publisher id =670 ] 
//Title      : Maqar [ Arabic ] 
//Created on : Oct 17, 2019, 07:02:01 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2104 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getStory(&$text) {
      $this->addLog("getting article text");
      $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
      $this->story = trim(preg_replace('!\s+!', ' ', $this->story));
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);

      return $this->story;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    sleep(1);
    $baseName = sha1($imageUrl) . ".jpeg";
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );

      $ch = curl_init($imageUrl);
      curl_setopt_array($ch, $options);
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $fulltext = $this->textFixation($this->getElementByName('fulltext', $text));
    $videos = array();
    $matches = null;
    preg_match_all('/<iframe(.*?)<\/iframe>/is', $fulltext, $matches);
    foreach ($matches[0] as $matche) {
      $video = array();
      $videoname = syndParseHelper::getImgElements($matche, 'iframe', 'src');

      $video['video_name'] = $videoname[0];
      $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
      $video['original_name'] = $videoName;
      $video['video_caption'] = $videoName;
      $date = $this->getElementByName('date', $text);
      $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
      $video['mime_type'] = "";
      $videos[] = $video;
    }
    return $videos;
  }

}
