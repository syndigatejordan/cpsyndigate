<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Emirates News Agency (WAM)  [# publisher id = 32]
//Title      : Emirates News Agency (WAM) [ Pashto ] 
//Created on : Oct 13, 2020, 11:24:49 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6552 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ps'); 
	} 
}