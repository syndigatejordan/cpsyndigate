<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence de Presse Africaine  [# publisher id = 356]
//Title      : Agence de Presse Africaine (APAnews) [ Arabic ] 
//Created on : Nov 30, 2020, 10:53:27 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6448 extends syndParseRss {

    public $story;

    public function customInit() {
        parent::customInit();
        $this->extensionFilter = '';
        $this->defaultLang = $this->model->getLanguageId('ar');
    }

    Public function getRawArticles(&$fileContents) {
        //get articles
        $this->addLog("getting articles raw text");
        $articles = explode('<span class="Style1"><span class="Style4">Article', $fileContents);
        unset($articles[0]);
        return $articles;
    }

    protected function getHeadline(&$text) {
        $this->addLog("getting article headline");
        $headline = null;
        preg_match('/<a(.*?)>(.*?)<\/a>/is', $text, $headline);
        return trim($this->textFixation($headline[2]));
    }

    protected function getArticleDate(&$text) {
        $this->addLog("getting article date");
        $date = null;
        preg_match('/<span class="champs">\((.*?)\)<\/span>/is', $text, $date);
        $date = trim($this->textFixation($date[1]));
        return date('Y-m-d', strtotime($date));
    }

    protected function getStory(&$text) {
        $this->addLog("getting article text");
        $this->story = strip_tags($this->story, '<div><p><strong><span>');
        $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) width=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) height=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) align=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('!\s+!', ' ', $this->story);
        return $this->textFixation($this->story);
    }

    public function getOriginalCategory(&$text) {
        $this->addLog('getting article category');
        $category = null;
        preg_match('/<td width="796" class="normtitle"> <span class="champs">(.*?)<\/span> <\/td>/is', $text, $category);
        return trim($this->textFixation($category[1]));
    }

    public function getArticleReference(&$text) {
        $this->addLog("getting article reference");
        $link = null;
        preg_match('/<a(.*?)>(.*?)<\/a>/is', $text, $link);
        $link = syndParseHelper::getImgElements($link[0], 'a', 'href');
        $link = trim($link[0]);
        return trim($link);
    }

    protected function getImages(&$text) {
        $this->story = NULL;
        preg_match('/ <td><div align="justify">(.*)/is', $text, $this->story);
        $this->story = $this->story[0];
        $imagesArray = array();
        preg_match_all("/<img[^>]+\>/i", $this->story, $imgs);
        foreach ($imgs[0] as $img) {
            $this->addLog("getting article images");
            $image_caption = '';
            $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
            $image_caption = trim($image_caption[0]);
            $imageInfo = syndParseHelper::getImgElements($img, 'img');
            $imagePath = $imageInfo[0];
            $imagePath = $imageInfo[0];
            if (strpos($imagePath, "https://") === FALSE) {
                $imagePath = "https://apanews.net" . $imagePath;
            }
            if (!$imagePath) {
                continue;
            }
            if ($this->checkImageifCached($imagePath)) {
                // Image already parsed..
                continue;
            }
            $imagePath = str_replace(' ', '%20', $imagePath);
            $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

            if (!$copiedImage) {
                echo "no pahr";
                continue;
            }
            $images = $this->getAndCopyImagesFromArray(array($copiedImage));
            if (!empty($image_caption)) {
                $images[0]['image_caption'] = $image_caption;
            }
            $name_image = explode('/images/', $copiedImage);
            if ($images[0]['image_caption'] == $name_image[1]) {
                $images[0]['image_caption'] = '';
            }
            $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
            $images[0]['is_headline'] = false;
            array_push($imagesArray, $images[0]);
        }
        return $imagesArray;
    }

    public function copyUrlImgIfNotCached($imageUrl) {
        $baseName = basename($imageUrl);
        $copiedImage = $this->imgCacheDir . $baseName;

        if (!is_dir($this->imgCacheDir)) {
            mkdir($this->imgCacheDir, 0755, true);
        }

        if (!file_exists($copiedImage)) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $imageUrl);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            $imgContent = curl_exec($ch);
            curl_close($ch);
            $myfile = fopen($copiedImage, "w");
            fwrite($myfile, $imgContent);
            fclose($myfile);
            if (!empty($imgContent)) {
                return $copiedImage;
            } else {
                return false;
            }
        } else {
            return $copiedImage;
        }
    }

}
