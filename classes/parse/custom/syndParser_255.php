<?php

////////////////////////////////////////////////////////////////////////
// Publisher : Al Rafidayn Newspaper  
// Titles    : Al Rafidayn
////////////////////////////////////////////////////////////////////////

class syndParser_255 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = str_replace(".", ".</p> <p>", $this->story);
    if (!empty($this->story)) {
      $this->story = "<p>" . trim($this->story) . "</p>";
    }
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story)) {
      return $this->textFixation($this->getElementByName('fulltext', $text));
    } else {
      return $this->story;
    }
  }

}
