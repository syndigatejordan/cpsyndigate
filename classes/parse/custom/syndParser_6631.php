<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Malaysia National News Agency (BERNAMA)  [# publisher id = 1421]
//Title      : Malaysia National News Agency (BERNAMA) [ English ] 
//Created on : Oct 26, 2020, 7:26:43 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6631 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}