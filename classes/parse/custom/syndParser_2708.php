<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : United Jordan Press Company  [# publisher id = 915]
//Title      : Al Ghad [ Arabic ] 
//Created on : May 17, 2016, 12:35:02 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2708 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    return $this->story;
  }

  protected function getImages(&$text) {
      $this->story = $this->textFixation($this->getCData($this->getElementByName('fulltext', $text)));
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();

      $imagesArray = array();
      preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
      foreach ($imgs[0] as $img) {
          $this->addLog("getting article images");
          $image_caption = '';
          $image_caption = explode('alt="', $img);
          $image_caption = explode('"', $image_caption[1]);
          $image_caption = $image_caption[0];
          $imageInfo = syndParseHelper::getImgElements($img, 'img');
          $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $headline = $this->textFixation($this->getElementByName('title', $text));
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath, $headline);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl, $headline) {
    preg_match('/fileid=(.*?)&width/', $imageUrl, $name);
    $name = $name[1];
    $baseName = sha1($headline) . '_' . $name . '.jpeg';
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $curl_handle = curl_init();
      curl_setopt($curl_handle, CURLOPT_URL, $imageUrl);
      curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl_handle, CURLOPT_USERAGENT, 'php');
      $imgContent = curl_exec($curl_handle);
      curl_close($curl_handle);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
