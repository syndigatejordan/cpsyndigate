<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Stichting Global Voices  [# publisher id = 1235]
//Title      : Global Voices [ Amharic ] 
//Created on : Feb 22, 2021, 8:00:56 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5594 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('am'); 
	} 
}