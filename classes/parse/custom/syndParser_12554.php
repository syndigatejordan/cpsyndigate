<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IDG Communications, Inc.  [# publisher id = 1790]
//Title      : MACWORLD SWEDEN [ Swedish ] 
//Created on : Apr 05, 2022, 11:44:17 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12554 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('sv'); 
	} 
}