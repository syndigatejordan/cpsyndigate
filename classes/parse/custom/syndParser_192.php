<?php

////////////////////////////////////////////////////////////////////////
// Publisher : Al Bawaba (Middle East) Ltd.
// Titles    : Mena Report [ Arabic ]
////////////////////////////////////////////////////////////////////////

class syndParser_192 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getImages(&$text) {
    $image = $this->textFixation($this->getElementByName('imageurl', $text));

    if (!$image) {
      return array();
    }
    if ($this->checkImageifCached($image)) {
      // Image already parsed..
      return;
    }
    $copiedImg = $this->copyUrlImgIfNotCached($image);
    if (!$copiedImg) {
      return array();
    }

    $images = $this->getAndCopyImagesFromArray(array($copiedImg));
    $images[0]['image_caption'] = $this->textFixation($this->getElementByName('imagetitle', $text));
    return $images;
  }

}

