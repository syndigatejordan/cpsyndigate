<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Abacity Blog Media  [# publisher id = 1455]
//Title      : AbaCityBlog [ English ] 
//Created on : Aug 24, 2021, 12:35:56 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7345 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}