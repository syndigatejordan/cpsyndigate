<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Khaama Press News Agency  [# publisher id = 1667]
//Title      : The Khaama Press News Agency [ English ] 
//Created on : Oct 10, 2021, 10:14:38 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12228 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}