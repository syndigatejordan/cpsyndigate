<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Syrian Arab News Agency (SANA)  [# publisher id = 134]
//Title      : Syrian Arab News Agency (SANA) [ Hebrew ] 
//Created on : Oct 15, 2020, 12:18:37 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6658 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('he'); 
	} 
}