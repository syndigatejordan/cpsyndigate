<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Fresh Angle Nigeria Limited  [# publisher id = 1523]
//Title      : Fresh Angle International Newspaper [ English ] 
//Created on : Sep 28, 2021, 1:49:24 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_9507 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}