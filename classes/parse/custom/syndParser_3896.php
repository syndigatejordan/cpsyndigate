<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BBC World Service  [# publisher id = 1192]
//Title      : BBC - What we eat [ English ] 
//Created on : Dec 09, 2018, 8:25:15 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3896 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}