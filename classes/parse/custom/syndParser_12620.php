<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Morningstar Japan KK  [# publisher id = 1801]
//Title      : MORNINGSTAR Japan [ Japanese ] 
//Created on : May 15, 2022, 9:48:18 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12620 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	} 
}