<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Pajhwok Afghan News   [# publisher id = 39]
//Title      : Pajhwok Afghan News [ Pashto ] 
//Created on : Dec 02, 2020, 2:54:29 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_68 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ps'); 
	} 
}