<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : National News Agency Lebanon (NNA)  [# publisher id = 132]
//Title      : National News Agency Lebanon (NNA) [ French ] 
//Created on : Oct 14, 2020, 11:06:03 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6723 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}