<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Hayat Al-Jadeda Company  [# publisher id = 916]
//Title      : Al Hayat Al-Jadedah [ Arabic ] 
//Created on : May 18, 2016, 10:53:15 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2710 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}