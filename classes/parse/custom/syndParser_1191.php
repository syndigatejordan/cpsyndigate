<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ArmInfo News Agency  [# publisher id = 358]
//Title      : ArmInfo - Newswire [ English ] 
//Created on : Apr 16, 2019, 12:44:06 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1191 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}