<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : Euronews SA
//Title     : euronews [ English ]
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_1731 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('item', $fileContents);
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
    return $this->textFixation($headline);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->textFixation($this->getCData($this->getElementByName('pubDate', $text)));
    return date('Y-m-d', strtotime($date));
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = $this->getCData($this->getElementByName('content:encoded', $text));
    $story = preg_replace("/\r\n|\r|\n/", "</p><p>", $story);
    $story = "<p>" . $story . "<p>";
    preg_match('#<media:content url="[^>]+/>#i', $text, $video);
    $video = $video[0];
    $video = explode('url="', $video);
    $video = $video[1];
    $video = explode('"', $video);
    $video = $video[0];
    $video = '</br><a href="' . $video . '">' . $video . '</a>';
    $story = $story . $video;
    return $this->textFixation($story);
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $filename = basename($this->currentlyParsedFile);
    $filename = str_replace('.xml', '', $filename);
    return $filename;
  }

  protected function getImages(&$text) {
    $story = $text;
    $this->imagesArray = array();
    preg_match_all('#<media:thumbnail url="[^>]+/>#i', $story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $img = explode('url="', $img);
      $img = $img[1];
      $img = explode('"', $img);
      $img = $img[0];
      $imagePath = $img;
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

}
