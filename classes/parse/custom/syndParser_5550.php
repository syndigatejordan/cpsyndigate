<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Muscat Media Group  [# publisher id = 42]
//Title      : Muscat Media Group [ English ] 
//Created on : Apr 23, 2019, 11:48:35 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5550 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
