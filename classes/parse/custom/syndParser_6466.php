<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ethiopian News Agency (ENA)  [# publisher id = 437]
//Title      : Ethiopian News Agency (ENA) [ Oromo ] 
//Created on : Oct 08, 2020, 7:24:01 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6466 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('om'); 
	} 
}