<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Maliactu  [# publisher id = 1830]
//Title      : maliactu.net  [ French ] 
//Created on : Jul 25, 2022, 8:46:55 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12675 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}