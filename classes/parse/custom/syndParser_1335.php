<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Arab Press Agency (APA)  [# publisher id = 405]
//Title      : Arab Press Agency (APA)  [ Arabic ] 
//Created on : Sep 17, 2019, 11:18:04 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1335 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}