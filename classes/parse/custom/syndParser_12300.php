<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Panama 24 Horas  [# publisher id = 1699]
//Title      : Panama 24 Horas [ Spanish ] 
//Created on : Jan 04, 2022, 2:11:57 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12300 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}