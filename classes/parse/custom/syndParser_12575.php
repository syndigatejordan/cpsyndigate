<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Euronews  [# publisher id = 604]
//Title      : Euronews [ Arabic ] 
//Created on : Apr 10, 2022, 8:51:55 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12575 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}