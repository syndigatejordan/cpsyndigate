<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The National Chamber of Commerce of Sri Lanka.  [# publisher id = 849]
//Title      : National Chamber of Commerce of Sri Lanka (NCCSL) - News [ English ] 
//Created on : Feb 08, 2016, 1:08:56 PM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2606 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}