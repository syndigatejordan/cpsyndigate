<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Bawaba Middle East Limited  [# publisher id = 62]
//Title      : 7elwa.com [ Arabic ] 
//Created on : Mar 10, 2020, 12:06:07 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5934 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

}
