<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CPI Financial  [# publisher id = 259]
//Title      : Islamic Business & Finance [ English ] 
//Created on : Dec 03, 2018, 12:04:04 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_4248 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}