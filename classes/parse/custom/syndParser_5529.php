<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Business News for Press and Distribution  [# publisher id = 9]
//Title      : Daily News Egypt - Sports [ English ] 
//Created on : Aug 18, 2021, 6:25:55 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5529 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}