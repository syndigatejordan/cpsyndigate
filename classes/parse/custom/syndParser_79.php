<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Entreprise Maghrebine de Médias  
//Title     : Al Ahdath Al Maghribia [ Arabic ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_79 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  public $story;

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('/<\/li> <li><div><div data-action="share"(.*)/is', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    $this->story = trim(str_replace('<div> </div>', '', $this->story));
    if (empty($this->story)) {
      return '';
    }
    //var_dump($this->story);
    //$this->story = utf8_encode($this->story);
    return $this->story;
  }

  protected function getImages(&$text) {
      $this->story = $this->textFixation($this->getCData($this->getElementByName('fulltext', $text)));
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();

      $this->imagesArray = array();
      preg_match_all("/<img[^>]+\>/i", $this->story, $imgs);

      foreach ($imgs[0] as $img) {
          $this->addLog("getting article images");
          $image_caption = '';
          $image_caption = explode('alt="', $img);
          if (!empty($image_caption[1])) {
              $image_caption = explode('"', $image_caption[1]);
              $image_caption = $image_caption[0];
      } else {
        $image_caption = "";
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      $imagePath = explode('?', $imagePath);
      if (count($imagePath) > 1) {
        $imagePath = $imagePath[0];
      } else {
        $imagePath = $imageInfo[0];
      }
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = str_replace('%20', ' ', $image_caption);
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['is_headline'] = false;
      $this->story = str_replace($image_caption, "", $this->story);
      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

  public function getArticleOriginalId($params = array()) {
    $defaultArray = array('text' => '', 'headline' => '', 'articleDate' => '', 'issueNum' => 0);
    $params = array_merge($defaultArray, $params);
    if (!$articleOriginalId = trim($this->getElementByName('id', $params['text']))) {
      return parent::getArticleOriginalId($params);
    }
    return $this->title->getId() . '_' . sha1($articleOriginalId . trim($params['headline'])) . '_' . sha1($params['text']);
  }

}
