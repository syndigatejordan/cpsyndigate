<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : A15   [# publisher id = 330]
//Title      : Masrawy - Arts & Culture [ Arabic ] 
//Created on : Aug 12, 2021, 12:17:48 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6407 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}