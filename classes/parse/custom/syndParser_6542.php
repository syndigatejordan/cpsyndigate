<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ariana News Agency (ANA)  [# publisher id = 1404]
//Title      : Ariana News Agency (ANA) [ Persian ] 
//Created on : Sep 19, 2021, 8:56:12 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6542 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fa'); 
	} 
}