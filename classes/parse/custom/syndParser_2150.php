<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sultan AlQahtani  [# publisher id = 683]
//Title      : Unlimit Tech [ Arabic ] 
//Created on : Sep 19, 2021, 7:02:48 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2150 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}