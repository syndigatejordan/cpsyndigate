<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : NEWSNOW / The News Agency of the Slovak Republic (TASR)  [# publisher id = 1439]
//Title      : The News Agency of the Slovak Republic (TASR) [ Slovak ] 
//Created on : Oct 19, 2020, 11:43:49 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6725 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('sk'); 
	} 
}