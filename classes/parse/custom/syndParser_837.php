<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: Sabq Group 
	// Titles   : Sabq [English]
	//////////////////////////////////////////////////////////////////////////////
	
	class syndParser_837 extends syndParseRss {

		public function customInit() {
			parent::customInit();
			$this->defaultLang  = $this->model->getLanguageId('en');
		}
		
		protected function getArticleDate(&$text) {
			$this->addLog("getting article date");
			$date = $this->getElementByName('pubDate', $text);
			if($date) {
				$date = explode(" ",$date);
				return $this->dateFormater($date[1]);
			}
			return parent::getArticleDate($text);
		}
	}
	
