<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Armenpress News Agency  [# publisher id = 354]
//Title      : Armenpress News Agency [ Armenian ] 
//Created on : Aug 09, 2021, 12:14:31 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1178 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ARM'); 
	} 
}