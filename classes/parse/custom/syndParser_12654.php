<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : MEI Global, LLC  [# publisher id = 1818]
//Title      : National Journal [ English ] 
//Created on : Jul 03, 2022, 12:31:57 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_12654 extends syndParseXmlContent
{

    public $story;

    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('en');
        $this->extensionFilter = 'xml';
    }

    public function getHeadline(&$text)
    {
        $this->addLog("getting article headline");
        return trim($this->textFixation($this->getElementByName('HeadLine', $text)));
    }

    public function getArticleDate(&$text)
    {
        $this->addLog("getting article date");
        $date = trim(preg_replace("/T(.*)/is", "", $this->getElementByName('FirstCreated', $text)));
        if ($date) {
            return $this->dateFormater($date);
        }
        return parent::getArticleDate($text);
    }

    public function getOriginalCategory(&$text)
    {
        $this->addLog('getting article category');
        $category = syndParseHelper::getImgElements($text, 'NewsItemType', 'FormalName');
        return $category[0];
    }

    public function getStory(&$text)
    {
        $this->addLog('Getting article story');
        $this->story = $this->textFixation($this->getCData($this->getElementByName('DataContent', $text)));
        $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) width=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) height=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) align=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
        $this->story = trim(preg_replace('!\s+!', ' ', $this->story));
        if (empty($this->story)) {
            return '';
        }
        return $this->story;
    }

    protected function getRawArticles(&$fileContents)
    {
        //get articles
        $this->addLog("getting articles raw text");
        $matchs = null;
        preg_match_all("/<NewsItem(.*?)<\/NewsItem>/is", $fileContents, $matchs);
        $matchs = $matchs[0];
        return $matchs;
    }

    protected function getAuthor(&$text)
    {
        $this->addLog("getting article author");
        return $this->getElementByName('ByLine', $text);
    }

    protected function getImages(&$text)
    {
        return array();
    }
}
	
