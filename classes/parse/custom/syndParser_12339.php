<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ESG Today  [# publisher id = 1736]
//Title      : ESGToday [ English ] 
//Created on : Jan 16, 2022, 12:52:23 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12339 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}