<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Breaking Media, Inc.  [# publisher id = 531]
//Title      : Fashionista [ English ] 
//Created on : Aug 10, 2021, 9:32:58 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1577 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}