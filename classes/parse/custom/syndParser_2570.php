<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Foreign Ministry Algeria  [# publisher id = 837]
//Title      : Algerian Ministry of Foreign Affairs - Press conferences & statements [ French ] 
//Created on : Jan 31, 2016, 11:37:06 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2570 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}