<?php

//////////////////////////////////////////////////////////////////////////////
// Publisher: The Zimbabwean  [# publisher id =390 ] 
// Titles   : The Zimbabwean [English]
//////////////////////////////////////////////////////////////////////////////

class syndParser_1286 extends syndParseCms {
  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
