<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BIA VOICE INTERNATIONAL LTD  [# publisher id = 1516]
//Title      : BVI Channel 1 [ English ] 
//Created on : Sep 19, 2021, 10:21:51 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_8868 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}