<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : National Aeronautics and Space Administration (NASA)  [# publisher id = 1292]
//Title      : NASA on The Commons [ English ] 
//Created on : Oct 04, 2021, 7:21:04 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5906 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}