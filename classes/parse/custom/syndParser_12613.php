<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : China Economic Net  [# publisher id = 1797]
//Title      : Economic Daily News [ Russian ] 
//Created on : May 10, 2022, 11:17:10 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12613 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}