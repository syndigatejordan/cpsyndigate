<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Cameroon News Today-CNT  [# publisher id = 1169]
//Title      : Cameroon News Today-CNT [ French ] 
//Created on : Oct 11, 2018, 9:36:07 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3428 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}