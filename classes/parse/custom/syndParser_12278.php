<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Merkle LLC  [# publisher id = 1686]
//Title      : The VR Soldier [ English ] 
//Created on : Nov 23, 2021, 11:00:03 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12278 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}