<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : AIC PERSPECTIV  [# publisher id = 1796]
//Title      : Togo Web [ French ] 
//Created on : May 09, 2022, 11:30:24 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12592 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}