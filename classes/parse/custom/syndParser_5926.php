<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Pressenza  [# publisher id = 1305]
//Title      : Pressenza [ English ] 
//Created on : Feb 15, 2020, 6:32:52 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5926 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
