<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Evening Standard Limited  [# publisher id =535 ]
// Titles    : London Evening Standard [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1584 extends syndParseXmlContent {
	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('en');
		//$this -> extensionFilter = 'XML';
	}

	protected function getRawArticles(&$fileContents) {
		//get articles
		$this -> addLog("getting articles raw text");
		$art = $this -> getElementsByName('NLAML', $fileContents);
		return $art;
	}

	protected function getAbstract(&$text) {
		$this -> addLog('getting article summary');
		$summary = $this -> textFixation($this -> getElementByName('SubHeadLine', $text));
		return $summary;
	}

	public function getStory(&$text) {
		$this -> addLog('Getting article story');
		$DataContent = trim($this -> getElementByName('SourceMetaData', $text));
		$cat = syndParseHelper::getImgElements($DataContent, 'Property FormalName="Page_Section"', 'Value');
		
		$cat = str_replace(';', ',', $cat);
              $cat=preg_replace('!\s+!', '', $cat);
		$cat = explode(',', $cat[0]);
		if(array_search('Teasers', $cat)){
			return '';
		}
		$DataContent = trim($this -> getElementByName('Article', $text));
		$body = $this -> getElementByName('body', $DataContent);
		$body = utf8_decode($body);
		$body = str_replace('&#x3C;', '<', $body);
		$body = str_replace('&#x3E;', '> ', $body);
		$body = str_replace('<P>', '<p> ', $body);
		$body = str_replace('</P>', '</p> ', $body);
		//$body = html_entity_decode($body);
		//var_dump($body);
		$body = strip_tags($body, '<p><br><strong><b><u><i>');
		if (empty($body)) {
			return '';
		}
		return $body;
	}

	public function getHeadline(&$text) {
		$DataContent = trim($this -> getElementByName('Article', $text));
		$headline = trim($this -> getElementByName('HeadLine', $DataContent));
              $headline = str_replace('&#x3C;', '<', $headline);
		$headline = str_replace('&#x3E;', '> ', $headline);
		return strip_tags($headline);
	}

	public function getAuthor(&$text) {
		$DataContent = trim($this -> getElementByName('Article', $text));
		$ByLine = trim($this -> getElementByName('Author', $DataContent));
		return $ByLine;
	}

	public function getOriginalCategory(&$text) {
		$DataContent = trim($this -> getElementByName('SourceMetaData', $text));
		$cat = syndParseHelper::getImgElements($DataContent, 'Property FormalName="Page_Section"', 'Value');
		$cat = $cat[0];
		$cat = str_replace(';', ',', $cat);
		return $cat;
	}

	public function getArticleDate(&$text) {
		$DataContent = trim($this -> getElementByName('SourceMetaData', $text));
		$date = syndParseHelper::getImgElements($DataContent, 'Property FormalName="Publication_Date"', 'Value');
		$date = $date[0];
		$year = substr($date, 0, 4);
		$month = substr($date, 4, 2);
		$day = substr($date, 6, 2);
		$date = $year . '-' . $month . '-' . $day;
		return $date;
	}

}
