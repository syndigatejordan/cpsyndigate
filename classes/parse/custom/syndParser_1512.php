<?php

///////////////////////////////////////////////////////////////////////////
// Publisher: SWOL LLC  [# publisher id =482 ]
// Title    : SWOL [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1512 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('item', $fileContents);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    $date = date('Y-m-d', strtotime($date));
    return $date;
  }

  protected function getHeadline(&$text) {

    $this->addLog("getting article headline");
    $headline = $this->getElementByName('title', $text);
    return $headline;
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $date = strtotime($this->getElementByName('pubDate', $text));
    $last3day=strtotime('-9 day');
    if($date<$last3day)
      return"";
    $this->story = html_entity_decode($this->story);
    $this->story = strip_tags($this->story, '<p><br><strong><b><u><i>');
    $this->story=  str_replace("&nbsp;"," ", $this->story);
    return $this->textFixation($this->story);
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return trim($this->textFixation($this->getElementByName('dc:creator', $text)));
  }

  protected function getImages(&$text) {    
    $story = trim($this->textFixation($this->getCData($this->getElementByName('content:encoded', $text))));
    $this->story = $story;
    $imagesArray = array();
     $date = strtotime($this->getElementByName('pubDate', $text));
    $last3day=strtotime('-9 day');
    if($date<$last3day)
      return $imagesArray;
    preg_match_all("/<img[^>]+\>/i", $story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $imageInfo = syndParseHelper::getImgElements($img, 'img', 'src');
      $imagePath = $imageInfo[0];
      $imagePath = explode('?', $imagePath);
      $imagePath = $imagePath[0];
      if (!$imagePath) {
        continue;
      }
      if ($image = $this->checkImageifCached($imagePath)) {
        if(!empty($image[2]))
        $this->story = str_replace($imageInfo[0], $image[2] , $this->story);
        // Image already parsed..
        continue;
      }
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['image_caption'] = $images[0]['image_caption'];
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
      $this->story = str_replace($imageInfo[0], $images[0]['img_name'] , $this->story);
    }
    return $imagesArray;
  }

}
