<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Asianet-Pakistan (Pvt) Ltd.  [# publisher id =83 ] 
// Titles    : The Bangkok Post
///////////////////////////////////////////////////////////////////////////
class syndParser_2026 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->charEncoding = 'UTF-8';
    //$this->extensionFilter = 'xml';
  }

  public function getRawArticles(&$fileContents) {
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('DigestName', $fileContents);
  }

  public function getStory(&$text) {

      $this->addLog("getting article text");
      $story = trim($this->getElementByName('Text', $text));
      $story = '<p>' . str_replace(".\r\n", ".\r\n" . "</p>\r\n<p>", $story) . '</p>';
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $story = preg_replace('/<img[^>]+\>/i', '', $story);
      return $story;
  }

  public function getHeadline(&$text) {
    $this->addLog("getting article headline");

    return trim($this->getElementByName('Headline', $text));
  }

  public function getArticleDate(&$text) {
    $this->addLog("getting article date");

    $date = trim($this->getElementByName('Date', $text));
    return date('Y-m-d', strtotime($date));
  }

  public function getAuthor(&$text) {
    $this->addLog("getting article Source");

    return trim($this->getElementByName('Source', $text));
  }

  protected function getImages(&$text)
  {
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      return array();
      // this contain just one image [ video image ]
      $imagesArray = array();
      preg_match_all("/<image>(.*?)<\/image>/si", $text, $images);
      foreach ($images[1] as $image) {
          $this->addLog("getting article images");
          preg_match('/<img href="(.*?)"\/>/i', $image, $imagePath);
          $imagePath = $imagePath[1];
          $imagePath = preg_replace("/[  ~&,:\-\)\(\']/", ' ', $imagePath);
          $imagePath = str_replace('..', '.', $imagePath);
          $imagePath = str_replace("'", "\'", $imagePath);
      $imagePath = str_replace("\'", "", $imagePath);
      $imagePath = str_replace("`", "\`", $imagePath);
      $imagePath = str_replace("\`", "", $imagePath);
      $imagePath = str_replace("\$", "\\$", $imagePath);
      $imagePath = str_replace("\$", "", $imagePath);
      $imagePath = str_replace('  ', ' ', $imagePath);
      $imagePath = preg_replace('!\s+!', '_', $imagePath);
      $imagePath = str_replace('._', '_', $imagePath);
      $imagePath = str_replace(' _', '_', $imagePath);
      if (!$imagePath) {
        continue;
      }
      if (!empty($imagePath)) {
        $image_full_path = $this->currentDir . $imagePath;
        $original_name = explode('.', $imagePath);
        if (file_exists($image_full_path)) {
          //the video name is same as the image name here  		
          $name = $this->model->getImgReplacement($imagePath, $this->currentDir, 2026);
          $img['img_name'] = str_replace(IMGS_PATH, IMGS_HOST, $name);
          $img['original_name'] = $imagePath;
          $original_name = str_replace('_', ' ', $original_name[0]);
          $img['image_caption'] = $original_name;
          $img['is_headline'] = true;
          array_push($imagesArray, $img);
        }
      }
    }
    return $imagesArray;
  }

}
