<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Mongabay  [# publisher id = 1294]
//Title      : Mongabay [ English ] 
//Created on : Feb 09, 2020, 2:53:05 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5908 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
