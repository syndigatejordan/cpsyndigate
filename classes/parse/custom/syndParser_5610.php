<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Conversation Canada  [# publisher id = 1238]
//Title      : The Conversation (Canada Edition) [ English ] 
//Created on : Sep 27, 2021, 7:44:14 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5610 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}