<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Fly  [# publisher id = 1749]
//Title      : The Fly on the wall [ English ] 
//Created on : Feb 01, 2022, 11:18:28 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12378 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}