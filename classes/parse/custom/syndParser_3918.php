<?php

class syndParser_3918 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

//Handle empty body
  public function parse() {
    $articles = array();
    foreach ($this->files as $file) {
      if ($this->extensionFilter) {
        if (!syndParseHelper::isFileExtension($file, $this->extensionFilter)) {
          $this->addLog("Wrong extension for file : $file)");
          continue;
        }
      }
      if (!file_exists($file)) {
        $this->addLog("file dose not exist: $file)");
        continue;
      }

      $this->addLog("get file contents (file:$file)");
      $fileContents = $this->getFileContents($file);

      if (!$fileContents) {
        continue;
      }

      $this->currentlyParsedFile = $file;
      $this->loadCurrentDirectory();
      $this->loadUpperDir();
      $rawArticles = $this->getRawArticles($fileContents);
      foreach ($rawArticles as $rawArticle) {
        $article = $this->getArticle($rawArticle);
        $articles[] = $article;
      }
    }
    return $articles;
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    return "";
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getCData($this->getElementByName('dc:creator', $text)));
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    $date = explode("/", $date);
    $date = "{$date[2]}-{$date[1]}-{$date[0]}";
    return $date;
  }

  protected function getImages(&$text) {
    $images = array();
    $media = null;
    preg_match_all("/<media:content(.*?)\/>/is", $text, $media);
    $media = $media[0];
    foreach ($media as $item) {
      $this->addLog("getting article image");
      $image_caption = $this->textFixation($this->getCData($this->getElementByName('title', $item)));
      $image_Name = syndParseHelper::getImgElements($item, 'media:content', 'url');
      $image_Name = $image_Name[0];
      $mimetype = syndParseHelper::getImgElements($item, 'media:content', 'type');
      $mimetype = $mimetype[0];
      $image_full_path = $this->currentDir . $image_Name;
      list($width, $height) = getimagesize($image_full_path);
      $width = round($width, -2);
      $height = round($height, -2);
      $original_name = explode('.', $image_Name);
      if (file_exists($image_full_path)) {
        //the video name is same as the image name here  		
        $name = $this->model->getImgReplacement($image_Name, $this->currentDir, 3918);
        $img['img_name'] = str_replace(IMGS_PATH, IMGS_HOST, $name) . "?mimetype={$mimetype}&height={$height}&width={$width}";
        $img['original_name'] = $original_name[0];
        $img['image_caption'] = $image_caption;
        $img['is_headline'] = FALSE;
        $img['image_original_key'] = sha1($image_Name);
        $images[] = $img;
      }
    }
    return $images;
  }

}

?>
