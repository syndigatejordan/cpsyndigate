<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : PetriDish Media Private Limited  [# publisher id = 1804]
//Title      : BioVoice News [ English ] 
//Created on : May 24, 2022, 11:49:36 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12623 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}