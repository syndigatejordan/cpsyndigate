<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Human Rights Watch  [# publisher id = 1302]
//Title      : Human Rights Watch [ Russian ] 
//Created on : Feb 28, 2022, 1:39:18 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12506 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}