<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ethiopian News Agency (ENA)  [# publisher id = 437]
//Title      : Ethiopian News Agency (ENA) [ Amharic ] 
//Created on : Oct 08, 2020, 7:23:07 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6464 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('am'); 
	} 
}