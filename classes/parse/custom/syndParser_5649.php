<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Insider Inc.  [# publisher id = 1220]
//Title      : Business Insider [ English ] 
//Created on : Jun 10, 2019, 1:52:15 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5649 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}