<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sarl Interface rp  [# publisher id = 509]
//Title      : Radio-M.net [ Arabic ] 
//Created on : Jul 18, 2022, 12:24:08 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12670 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}