<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Singapore International Chamber of Commerce  [# publisher id = 853]
//Title      : Singapore International Chamber of Commerce (SICC) - Annual Reports [ English ] 
//Created on : Feb 17, 2016, 8:55:40 AM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2611 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}