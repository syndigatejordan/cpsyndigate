<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Agenzia ANSA Società Cooperativa  [# publisher id =518 ] 
// Titles    : ANSA [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1602 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('nitf', $fileContents);
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->getElementByName('title', $text);
    $headline = trim(strip_tags($headline));
    return $headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");

    $date = $this->getElementByName('docdata', $text);
    $date = $this->getElementByName('date.issue', $date);
    $date = syndParseHelper::getImgElements($date, 'date.release', 'norm');
    $date = date('Y-m-d', strtotime($date[0]));
    return $date;
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = $this->textFixation($this->getElementByName('block', $text));
    $this->story = strip_tags($this->story, '<p><br><strong><b><u><i>');
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  public function getOriginalCategory(&$text) {
    $DataContent = trim($this->getElementByName('head', $text));
    $cat = syndParseHelper::getImgElements($DataContent, 'meta name ="subcategory"', 'content');
    $cat = $cat[0];
    return $cat;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $DataContent = trim($this->getElementByName('head', $text));
    $auther = syndParseHelper::getImgElements($DataContent, 'meta name ="author"', 'content');
    return $auther[0];
  }

  public function getAbstract(&$text) {
    $this->addLog('Getting article summary');
    $this->abs = trim(strip_tags($this->getElementByName('byline', $text)));
    return $this->abs;
  }

}
