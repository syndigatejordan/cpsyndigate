<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Noticia.co.mz  [# publisher id = 1508]
//Title      : Noticia.co.mz [ Portuguese ] 
//Created on : Sep 19, 2021, 10:21:35 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7469 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}