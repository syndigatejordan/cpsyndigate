<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Saudi Voters Center  [# publisher id =329 ] 
//Title      : Saudi in Focus [ Arabic ] 
//Created on : Oct 11, 2016, 10:25:57 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1073 extends syndParseRss {

  public $story;
  public $Third_patty = array(
      "وكالات",
      "واس",
      "أ ف ب",
      "أفب",
      "الوسط البحرينية",
      "الوطن الكويتية",
      "القدس العربي",
      "اللواء اللبنانية",
      "النهار اللبنانية",
      "المستقبل اللبنانية",
      "الأناضول", "الاقتصادية",
      "الاقتصادية الإلكترونية",
      "الاناضول",
      "الاتحاد الاماراتية",
      "الجريدة الكويتية",
      "الجزيرة",
      "الحياة",
      "الرياض",
      "الراية القطرية",
      "السفير اللبنانية",
      "الشرق الاوسط",
      "العربي الجديد",
      "بي بي سي",
      "رويترز",
      "سبأنت",
      "عربي21"
  );

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'WINDOWS-1256';
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    foreach ($this->Third_patty as $v) {
      $v = trim(strtolower($v));
      $searchin = strip_tags(strtolower($this->story));
      if (preg_match("/\b$v\b/", $searchin)) {
        $this->addLog("The source was found  $v .");
        return "";
      }
    }
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    $this->story = html_entity_decode($this->story, ENT_QUOTES, "UTF-8");
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = ($this->getCData($this->getElementByName('title', $text)));
    $headline = html_entity_decode($headline, ENT_QUOTES, "UTF-8");
    $headline = html_entity_decode($headline, ENT_QUOTES, "UTF-8");
    return $headline;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getCData($this->getElementByName('dc:creator', $text)));
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    $date = date("Y-m-d", strtotime($date));
    return $date;
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getElementsByName('category', $text);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = $this->textFixation($this->getCData($cat));
      }
    }
    return implode(', ', $originalCats);
  }

  protected function getImages(&$text) {

    $this->story = trim($this->textFixation($this->getCData($this->getElementByName('content:encoded', $text))));
    $imagesArray = array();
    preg_match_all("/<img[^>]+\>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");

      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];

      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
