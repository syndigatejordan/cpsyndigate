<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Oman News Agency (ONA)  [# publisher id = 139]
//Title      : Oman News Agency (ONA) [ English ] 
//Created on : Jun 02, 2016, 4:08:09 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_389 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}