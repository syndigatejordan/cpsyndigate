<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Siam News Network  [# publisher id = 1049]
//Title      : Thailand Business News [ English ] 
//Created on : Sep 19, 2021, 7:05:15 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2913 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}