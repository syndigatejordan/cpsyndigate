<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ClickPress  [# publisher id = 225]
//Title      : theGazette [ English ] 
//Created on : Aug 30, 2021, 7:33:20 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7336 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}