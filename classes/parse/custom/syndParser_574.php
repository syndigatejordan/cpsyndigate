<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Bulgarian ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_574 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority' 		=> 	'Възлагащия орган', 
										'Nature of Contract'		=> 	'Същност на договора', 
										'Regulation of Procurement' => 	'Регламент на обществените поръчки', 
										'Type of bid required'		=> 	'Тип на офертата се изисква', 
										'Award Criteria'			=>	'Критерии за възлагане', 
										'Original CPV'				=>	'Оригинален CPV', 
										'Country'					=>	'Страна', 
										'Document Id' 				=>	'Документ  самоличност',
										'Type of Document'			=>	'Вид на документа',
										'Procedure'					=>	'Процедура',
										'Original Language'			=>	'Oригиналния език',
										'Current Language'			=> 	'Текущ език');	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('bg');
		}
	}