<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Uzbekistan National News Agency (UzA)  [# publisher id = 74]
//Title      : Uzbekistan National News Agency (UzA) [ French ] 
//Created on : Aug 04, 2021, 1:55:30 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_220 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}