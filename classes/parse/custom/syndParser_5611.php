<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Conversation France  [# publisher id = 1239]
//Title      : The Conversation (France Edition) [ French ] 
//Created on : Sep 27, 2021, 7:45:21 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5611 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}