<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Hard Beat Communications  [# publisher id = 1394]
//Title      : CaribPR Wire [ English ] 
//Created on : Sep 19, 2021, 8:55:37 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6513 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}