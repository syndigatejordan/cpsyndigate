<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Media Sud Publishing  [# publisher id = 1676]
//Title      : Curierul National [ Romanian ] 
//Created on : Nov 02, 2021, 1:44:42 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12243 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ro'); 
	} 
}