<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ministry of Foreign Affairs - Arab Republic of Egypt  [# publisher id = 836]
//Title      : Ministry of Foreign Affairs of the Arab Republic of Egypt - News [ French ] 
//Created on : Jan 30, 2016, 8:27:28 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2561 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}