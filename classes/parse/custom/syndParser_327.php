<?php
//////////////////////////////////////////////////////////////////////////////////////
//  Publisher : Motivate publishing
//  Title     : Middle East MICE & Events [English]
//////////////////////////////////////////////////////////////////////////////////////
	
	class syndParser_327 extends syndParseCms {
		
		protected function getStory(&$text) {
			$story = parent::getStory($text);
			$story = str_replace(". ", ".</p>\n<p>", $story);
			return $story;
		}
		
	}