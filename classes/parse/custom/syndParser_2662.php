<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ministry of External Affairs, Government of India.  [# publisher id = 895]
//Title      : Government of India Ministry of External Affairs - Speeches & Statements [ English ] 
//Created on : Apr 07, 2016, 10:05:28 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2662 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}