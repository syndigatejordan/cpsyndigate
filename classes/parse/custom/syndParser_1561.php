<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Streamline RBR, Inc.  [# publisher id = 522]
//Title      : Radio and Television Business Report (RBR-TVBR) [ English ] 
//Created on : Sep 19, 2021, 7:06:24 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1561 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}