<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Galadari Printing & Publishing LLC  [# publisher id = 28]
//Title      : Khaleej Times [ English ] 
//Created on : Sep 14, 2017, 12:16:50 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_44 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
