<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Buzz Future LLC  [# publisher id =519 ]
// Titles    : Eurasia Review [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1557 extends syndParseRss {

	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('en');
	}

	public function getStory(&$text) {
		$this -> addLog('Getting article story');
		$body = $this -> textFixation($this -> getCData($this -> getElementByName('content:encoded', $text)));
		$this -> story = strip_tags($body, '<p><br><strong><b><u><i>');
		if (empty($this -> story)) {
			return '';
		}
		return $this -> story;
	}

	public function getHeadline(&$text) {
		$headline = trim($this -> getElementByName('title', $text));
		return $headline;
	}

	public function getArticleDate(&$text) {
		$date = trim($this -> getElementByName('pubDate', $text));
		return date('Y-m-d', strtotime($date));
	}

	public function getOriginalCategory(&$text) {
		$this -> addLog('getting article category');
		$cats = $this -> getCData($this -> getElementsByName('category', $text));
		$originalCats = array();

		if (!empty($cats)) {
			foreach ($cats as $cat) {
				if ($this -> textFixation($this -> getCData($cat)) != '1') {
					$originalCats[] = $this -> textFixation($this -> getCData($cat));
				}
			}
		}
		return implode(', ', $originalCats);
	}

	protected function getAuthor(&$text) {
		$this -> addLog("getting article author");
		$creator = trim($this -> getElementByName('dc:creator', $text));
		return $creator;
	}

}
