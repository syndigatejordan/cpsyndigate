<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Web Middle East (Web Me) s.a.l.  [# publisher id = 19]
//Title      : The Daily Star - Business [ English ] 
//Created on : Aug 19, 2019, 11:35:45 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5685 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('nitf', $fileContents);
  }

  protected function getStory(&$text) {
    //get body text
    $this->addLog("getting article text");
    $blocks = $this->getElementsByName('body\.content', $text);
    $blocks = $this->getElementsByName('block', $text);
    $block = (isset($blocks[1]) ? $blocks[1] : $blocks[0]);
      $story = $this->textFixation($block);

      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $story = preg_replace('/<img[^>]+\>/i', '', $story);

      return $story;
  }

  protected function getHeadline(&$text) {
    //get headline
    $this->addLog("getting article headline");
    $headline = $this->getElementByName('hedline', $text);
    return $this->textFixation($this->getElementByName('hl1', $text));
  }

  protected function getArticleDate(&$text) {
    //get article date
    $this->addLog("getting article date");
    $date = $this->getElementByName('dateline', $text);
    $date = $this->getElementByName('story\.date', $date);
    return $this->dateFormater($date);
  }

  protected function getOriginalCategory(&$text) {
    $this->addLog("getting article Category");
    $cat = $this->getElementByName('pubdata', $text);
    $cat = $this->getElementByName('position\.section', $cat);
    return $cat;
  }

  protected function getIptcId(&$text) {
    $this->addLog("getting category name");
    $originalCategoryName = $this->getElementByName('position\.section', $text);
    $iptcId = $this->model->getIptcCategoryId($originalCategoryName);
    if (!$iptcId) {
      return parent::getIptcId(&$text);
    }
    return $iptcId;
  }

  protected function getOriginalData(&$text) {
    $originalData['extras']['distributor'] = $this->getElementByName('distributor', $text);
    $originalData['extras']['location'] = $this->getElementByName('location', $text);
    return $originalData;
  }

  protected function getAbstract(&$text) {
    //get abstract
    $this->addLog("getting article abstract");
    $bodyHead = $this->getElementByName('body\.head', $text);
    return $this->textFixation($this->getElementByName('abstract', $bodyHead));
  }

  protected function getAuthor(&$text) {
    //get author
    $this->addLog("getting article author");
    $author = $this->textFixation($this->getElementByName('byline', $text));
    if (!$author) {
      return parent::getAuthor($text);
    }
    return $author;
  }

  public function getArticleReference(&$text) {
    preg_match('/<media-reference source="(.*?)"/i', $text, $link);
    return $link[1];
  }

}

?>