<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : CPI Financial  
//Title     : Banker Middle East [ English ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_337 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function beforeParse($params = array()) {

    parent::beforeParse($params = array());

    foreach ($this->files as $file) {
      $filename = strtolower(basename($file));

      // dublicating directories for mena
      //if(strpos($filename, 'news')) {

      $c = new Criteria();
      $c->add(TitlePeer::ID, 5483);
      $targetTitle = TitlePeer::doSelectOne($c);

      if ($file != '.' && $file != '..') {
        $this->addLog("checking file " . $targetTitle->getHomeDir() . $filename);
        if (!file_exists($targetTitle->getHomeDir() . $filename)) {
          $this->addLog("copying $file to " . $targetTitle->getHomeDir());
          copy($file, $targetTitle->getHomeDir() . $filename);
        }
      }
      //}
    } // end foreach
  }

}
