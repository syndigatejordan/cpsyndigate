<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Are We Europe Foundation  [# publisher id = 1520]
//Title      : Are We Europe [ English ] 
//Created on : Sep 19, 2021, 10:22:00 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_9502 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}