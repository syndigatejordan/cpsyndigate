<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Algeria Press Service  [# publisher id = 136]
//Title      : Algeria Press Service [ Tifinagh ] 
//Created on : Oct 05, 2020, 10:39:04 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6461 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('tfng'); 
	} 
}