<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Bayyraq.com  [# publisher id =665 ] 
// Title     : Bayyraq.com Slideshows
///////////////////////////////////////////////////////////////////////////

class syndParser_2094 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('article', $fileContents);
    return $articles;
  }

  public function getStory(&$text) {
    $story = trim($this->textFixation($this->getCData($this->getElementByName('body', $text))));
    if (empty($story)) {
      $story = trim($this->textFixation($this->getCData($this->getElementByName('abstract', $text))));
    }
    $this->addLog('Getting article story');
    if (empty($story)) {
      return '';
    }
    return $story;
  }

  public function getHeadline(&$text) {
    $headline = trim($this->textFixation($this->getCData($this->getElementByName('title', $text))));
    return $headline;
  }

  public function getArticleDate(&$text) {
    $date = trim($this->getElementByName('date', $text));
    return date('Y-m-d', strtotime($date));
  }

  public function getAbstract(&$text) {
    $this->addLog('Getting article summary');
    $summary = trim($this->textFixation($this->getCData($this->getElementByName('abstract', $text))));
    return $summary;
  }

  protected function getImages(&$text) {

    $imagesArray = array();
    $matches = null;
    preg_match_all('/<image>(.*?)<\/image>/s', $text, $matches);
    foreach ($matches[1] as $matche) {
      $this->addLog("getting article images");
      ///////////////////Get image original//////////////////////
      /* preg_match("/<image_url>(.*?)<\/image_url>/i", $matche, $imagePath);
        $imagePath = $this->getCData($imagePath[1]); */
      $imagePath = trim($this->textFixation($this->getCData($this->getElementByName('image_url', $matche))));
      /* $imagePath = rawurlencode($imagePath);
        $imagePath = str_replace('%3A', ':', $imagePath);
        $imagePath = str_replace('%2F', '/', $imagePath); */
      preg_match("/<image_alt>(.*?)<\/image_alt>/i", $matche, $image_caption);
      $image_caption = $this->getCData($image_caption[1]);
      $image_caption = strip_tags($image_caption);
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption))
        $images[0]['image_caption'] = $image_caption;
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);

      ///////////////////Get thumb image //////////////////////
      $this->addLog("getting article thumb images");
      $imagePath = "";
      $copiedImage = "";
      /* preg_match("/<image_thumb_url>(.*?)<\/image_thumb_url>/i", $matche, $imagePath);
        $imagePath = $this->getCData($imagePath[1]); */
      $imagePath = trim($this->textFixation($this->getCData($this->getElementByName('image_thumb_url', $matche))));
      /* $imagePath = rawurlencode($imagePath);
        $imagePath = str_replace('%3A', ':', $imagePath);
        $imagePath = str_replace('%2F', '/', $imagePath); */
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgSlideShowIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption))
        $images[0]['image_caption'] = $image_caption;
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    ///////////////////Get image original//////////////////////
    $this->addLog("getting article images");
    $imagePath = "";
    $copiedImage = "";
    /* preg_match("/<image_original>(.*?)<\/image_original>/i", $text, $imagePath);
      $imagePath = $this->getCData($imagePath[1]); */
    $imagePath = trim($this->textFixation($this->getCData($this->getElementByName('image_original', $text))));
    /* $imagePath = rawurlencode($imagePath);
      $imagePath = str_replace('%3A', ':', $imagePath);
      $imagePath = str_replace('%2F', '/', $imagePath); */
    preg_match("/<image_original_alt>(.*?)<\/image_original_alt>/i", $text, $image_caption);
    $image_caption = $this->getCData($image_caption[1]);
    $image_caption = strip_tags($image_caption);
    if (!$imagePath) {
      // return;
    }
    if ($image = $this->checkImageifCached($imagePath)) {
      // Image already parsed..
      return $imagesArray;
    }
    $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

    if (!$copiedImage) {
      echo "no pahr";
      return $imagesArray;
    }
    $images = $this->getAndCopyImagesFromArray(array($copiedImage));
    if (!empty($image_caption))
      $images[0]['image_caption'] = $image_caption;
    $images[0]['is_headline'] = false;
    array_push($imagesArray, $images[0]);
///////////////////Get thumb image original//////////////////////
    $this->addLog("getting article thumb images");
    $imagePath = "";
    $copiedImage = "";
    /* preg_match("/<image_thumb>(.*?)<\/image_thumb>/i", $text, $imagePath);
      $imagePath = $this->getCData($imagePath[1]); */
    $imagePath = trim($this->textFixation($this->getCData($this->getElementByName('image_thumb', $text))));
    /* $imagePath = rawurlencode($imagePath);
      $imagePath = str_replace('%3A', ':', $imagePath);
      $imagePath = str_replace('%2F', '/', $imagePath); */
    $imagePath = str_replace(' ', '%20', $imagePath);
    if (!$imagePath) {
      return $imagesArray;
    }
    if ($image = $this->checkImageifCached($imagePath)) {
      // Image already parsed..
     return $imagesArray;
    }
    $copiedImage = $this->copyUrlImgSlideShowIfNotCached($imagePath);
    if (!$copiedImage) {
      echo "no pahr";
      return $imagesArray;
    }
    $images = $this->getAndCopyImagesFromArray(array($copiedImage));
    if (!empty($image_caption))
      $images[0]['image_caption'] = $image_caption;
    $images[0]['is_headline'] = false;
    array_push($imagesArray, $images[0]);


    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {

    $imageUrl = urlencode($imageUrl);
    $imageUrl = str_replace('%3A', ':', $imageUrl);
    $imageUrl = str_replace('%2F', '/', $imageUrl);
    $baseName = urldecode(basename($imageUrl));
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $curl_handle = curl_init();
      curl_setopt($curl_handle, CURLOPT_URL, $imageUrl);
      curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl_handle, CURLOPT_USERAGENT, 'php');
      $imgContent = curl_exec($curl_handle);
      curl_close($curl_handle);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

// End function copyUrlImgIfNotCached

  public function copyUrlImgSlideShowIfNotCached($imageUrl) {

    $imageUrl = urlencode($imageUrl);
    $imageUrl = str_replace('%3A', ':', $imageUrl);
    $imageUrl = str_replace('%2F', '/', $imageUrl);
    $baseName = urldecode('thumb_' . basename($imageUrl));
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $curl_handle = curl_init();
      curl_setopt($curl_handle, CURLOPT_URL, $imageUrl);
      curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl_handle, CURLOPT_USERAGENT, 'php');
      $imgContent = curl_exec($curl_handle);
      curl_close($curl_handle);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

// End function copyUrlImgIfNotCached
}