<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence Burundaise de Presse (ABP)  [# publisher id = 1361]
//Title      : Agence Burundaise de Presse (ABP) [ French ] 
//Created on : Sep 19, 2021, 8:54:36 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6446 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}