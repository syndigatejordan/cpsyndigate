<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Cameroon News Today-CNT  [# publisher id = 1169]
//Title      : Cameroon News Today-CNT [ English ] 
//Created on : Feb 14, 2018, 11:24:47 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3427 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $videos = array();
    $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));

    $date = $this->getElementByName('date', $text);
    $date = date('Y-m-d h:i:s', strtotime($date));
    $story = $this->getCData($this->getElementByName('fulltext', $text));
    $story = str_replace("&nbsp;", " ", $story);
    $story = $this->textFixation($story);
    $story = trim(preg_replace('!\s+!', ' ', $story));
    $matches = null;
    preg_match_all('/<video(.*?)<\/video>/is', $story, $matches);
    foreach ($matches[0] as $match) {

      $sources = null;
      if (preg_match_all('/<source(.*?)>/is', $match, $sources)) {
        foreach ($sources[0] as $source) {
          $video = array();
          $videoname = syndParseHelper::getImgElements($source, 'source', 'src');
          if (isset($videoname[0])) {
            $videoname = $videoname[0];
          } else {
            $videoname = "";
          }
          if (empty($videoname)) {
            continue;
          }
          $mimeType = syndParseHelper::getImgElements($source, 'source', 'type');
          if (isset($mimeType[0])) {
            $mimeType = $mimeType[0];
          } else {
            $mimeType = "";
          }
          if (preg_match("/youtube.com/", $videoname)) {
            continue;
          }

          $video['video_name'] = $videoname;
          $video['original_name'] = $videoname;
          $video['video_caption'] = $videoname;
          $video['mime_type'] = $mimeType;
          $video['added_time'] = $date;
          $videos[] = $video;
        }
      }
    }
    return $videos;
  }

}
