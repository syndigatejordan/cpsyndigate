<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bourse de Tunis  [# publisher id = 596]
//Title      : Bourse de Tunis [ English ] 
//Created on : Feb 02, 2016, 11:54:36 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1687 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}