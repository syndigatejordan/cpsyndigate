<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Groupe de presse Iwacu  [# publisher id = 1138]
//Title      : Iwacu [ English ] 
//Created on : Mar 15, 2018, 11:35:07 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3125 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}