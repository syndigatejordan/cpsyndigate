<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : MPI Media Ltd  [# publisher id = 1792]
//Title      : Irish Medical Times [ English ] 
//Created on : Apr 12, 2022, 12:07:17 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12582 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}