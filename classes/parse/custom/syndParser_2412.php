<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Asharqia Chamber  [# publisher id =774 ]   
//Title     : Asharqia Chamber of Commerce News  [ English ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2412 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = trim($this->textFixation($this->getCData($this->getElementByName('fulltext', $text))));
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    $this->story = html_entity_decode($this->story, ENT_QUOTES, "UTF-8");
    if (empty($this->story)) {
      return "";
    } else {
      return $this->story;
    }
  }

}
