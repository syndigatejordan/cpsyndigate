<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Bawaba Middle East Limited  [# publisher id = 62]
//Title      : Albawaba.com Videos [ English ] 
//Created on : Aug 18, 2021, 8:52:22 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3519 extends syndParseCms
{
    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('en');
    }

    // Bug report #113 Remove the body (Al Bawaba videos) titles
    public function parse()
    {
        $articles = array();
        foreach ($this->files as $file) {
            if ($this->extensionFilter) {
                if (!syndParseHelper::isFileExtension($file, $this->extensionFilter)) {
                    $this->addLog("Wrong extension for file : $file)");
                    continue;
                }
            }
            if (!file_exists($file)) {
                $this->addLog("file dose not exist: $file)");
                continue;
            }

            $this->addLog("get file contents (file:$file)");
            $fileContents = $this->getFileContents($file);

            if (!$fileContents) {
                continue;
            }

            $this->currentlyParsedFile = $file;
            $this->loadCurrentDirectory();
            $this->loadUpperDir();
            $rawArticles = $this->getRawArticles($fileContents);
            foreach ($rawArticles as $rawArticle) {
                $article = $this->getArticle($rawArticle);
                if (trim(strtolower($article["original_data"]["original_category"])) == "entertainment") {
                    $articles[] = $article;
                }
            }
        }
        return $articles;
    }

    public function getOriginalCategory(&$text)
    {
        $this->addLog('getting article category');
        return "entertainment";
    }

    protected function getStory(&$text)
    {
        $this->addLog("getting article text");
        return "";
    }

    protected function getVideos(&$text)
    {
        $this->addLog("Getting videos");
        $matches = null;
        $videos = array();
        $videoName = "";
        $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
        $fulltext = $this->textFixation($this->getElementByName('fulltext', $text));
        $videoname = null;
        if (preg_match('/<iframe src="(.*?)"><\/iframe>/is', $fulltext, $videoname)) {
            $videoname = $videoname[1];
            if (!empty($videoname)) {
                $video = array();
                $video['video_name'] = $videoname;
                $video['original_name'] = $videoName;
                $video['video_caption'] = $videoName;
                $video['added_time'] = date('Y-m-d h:i:s', $this->getElementByName('date', $text));

                $videos[] = $video;
            }
        }
        return $videos;
    }
}
