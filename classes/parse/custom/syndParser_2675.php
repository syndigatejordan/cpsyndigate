<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Republic of Korea Ministry of Foreign Affairs  [# publisher id = 900]
//Title      : Republic of Korea Ministry of Foreign Affairs - Press Releases [ English ] 
//Created on : Apr 07, 2016, 10:54:12 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2675 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}