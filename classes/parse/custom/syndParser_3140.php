<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Caxton Ltd  [# publisher id = 1148]
//Title      : Randburg Sun [ English ] 
//Created on : Aug 30, 2017, 10:33:51 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3140 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}