<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Lebanon Debate  [# publisher id = 1336]
//Title      : Lebanon Debate [ Arabic ] 
//Created on : Sep 23, 2021, 8:00:17 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6033 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}