<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Saudi Research & Publishing Co.  
//Title     : Asharq Alawsat [ Arabic ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_424 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  public function getStory(&$text) {
      $this->addLog('Getting article story');
      $this->story = str_replace('" ">', '">', $this->story);
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      $this->story = preg_replace('/<div class="node_new_photo_text">(.*?)<\/div>/i', '', $this->story);
      $this->story = preg_replace('!\s+!', ' ', $this->story);
      $this->story = str_replace("div", "p", $this->story);
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);

      if (empty($this->story)) {
          return '';
      }
      return $this->story;
  }

}
