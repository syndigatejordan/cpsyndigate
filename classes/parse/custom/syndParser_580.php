<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Italian ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_580 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority' 		=> 	'Ente concedente', 
										'Nature of Contract'		=> 	'Natura dell\'appalto',
										'Regulation of Procurement' => 	'Normativa degli appalti', 
										'Type of bid required'		=> 	'Tipo d\'offerta richiesta', 
										'Award Criteria'			=>	'Criteri di aggiudicazione', 
										'Original CPV'				=> 	'Originale CPV', 
										'Country'					=>	'Paese', 
										'Document Id' 				=>	'Documento di identità', 
										'Type of Document'			=>	'Tipo di documento', 
										'Procedure'					=>	'Procedura', 
										'Original Language'			=>	'Lingua originale', 
										'Current Language'			=> 	'Lingua corrente');	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('it');
		}
	}