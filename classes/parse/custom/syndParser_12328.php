<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Vogel Communications Group  [# publisher id = 1723]
//Title      : Next Mobility [ German ] 
//Created on : Jan 11, 2022, 2:34:52 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12328 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('de'); 
	} 
}