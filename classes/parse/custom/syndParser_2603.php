<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dhaka Chamber of Commerce & Industry   [# publisher id = 846]
//Title      : Dhaka Chamber of Commerce & Industry (DCCI) - News [ English ] 
//Created on : Feb 08, 2016, 1:03:54 PM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2603 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}