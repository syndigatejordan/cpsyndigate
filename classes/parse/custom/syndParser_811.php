<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Euclid Infotech Pvt. Ltd
// Titles    : TendersInfo - Project Notices
///////////////////////////////////////////////////////////////////////////

require_once '/usr/local/syndigate/classes/parse/custom/syndParser_81.php';

class syndParser_811 extends syndParser_81
{

    public function beforeParse($params = array()) {
        $this->customInit();
        $this->parseReport['Id'] = $this->report->addReport($this->parseReport);
        $this->addLog('start parsing (title:' . $this->title->getName() . ')');

        //Adding report gather id to parsing state
        $c = new Criteria();
        $c->add(ParsingStatePeer::REVISION_NUM, $this->revisionId);
        $c->add(ParsingStatePeer::TITLE_ID, $this->title->getId());
        $c->addDescendingOrderByColumn(ParsingStatePeer::ID);
        $parsingState = ParsingStatePeer::doSelectOne($c);

        if (is_object($parsingState)) {
            $parsingState->setReportParseId($this->parseReport['Id']);
            $parsingState->save();

            //Also add the report parse id to the table "report" because the parsing state may be deleted
            $c = new Criteria();
            $c->add(ReportPeer::TITLE_ID, $this->title->getId());
            $c->add(ReportPeer::REVISION_NUM, $this->revisionId);
            $c->addDescendingOrderByColumn(ReportPeer::ID);
            $report = ReportPeer::doSelectOne($c);
            if (is_object($report)) {
                $report->setReportParseId($this->parseReport['Id']);
                $report->save();
            }
        }
    }
}
