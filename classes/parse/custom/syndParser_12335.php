<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bonhill Group plc  [# publisher id = 1733]
//Title      : ESG Clarity Europe [ English ] 
//Created on : Jan 16, 2022, 12:12:11 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12335 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}