<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Naya Group for Excellence FZ LLC  [# publisher id = 988]
//Title      : AbuDhabiEnv.ae [ Arabic ] 
//Created on : Aug 15, 2016, 12:56:37 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2810 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}