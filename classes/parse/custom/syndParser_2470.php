<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Amman Chamber Of Commerce  [# publisher id = 810]
//Title      : Amman Chamber Of Commerce (ACC) News [ Arabic ] 
//Created on : Feb 01, 2016, 9:36:19 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2470 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}