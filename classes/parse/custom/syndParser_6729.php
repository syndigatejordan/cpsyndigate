<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Rossiya Segodnya  [# publisher id = 1441]
//Title      : RIA Novosti (РИА Новости) [ Russian ] 
//Created on : Oct 19, 2020, 9:48:21 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6729 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}