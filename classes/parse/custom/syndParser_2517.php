<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Asianet-Pakistan (Pvt) Ltd.  [# publisher id = 83]
//Title      : The Star [ English ] 
//Created on : Mar 04, 2020, 1:27:04 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2517 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}