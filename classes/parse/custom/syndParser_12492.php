<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IFC - International Finance Corporation  [# publisher id = 1773]
//Title      : IFC (World Bank Group) [ Portuguese ] 
//Created on : Feb 28, 2022, 12:28:14 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12492 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}