<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Iranian Students News Agency (ISNA)  [# publisher id = 147]
//Title      : Iranian Students News Agency (ISNA) [ French ] 
//Created on : Jan 30, 2016, 6:32:10 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2444 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}