<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Myles Mellor Themed Crosswords   [# publisher id = 1267]
//Title      : Myles Mellor - Medium Sudokus [ English ] 
//Created on : Oct 30, 2019, 06:28:44 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5854 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getRawArticles(&$fileContents) {
    $this->addLog("getting raw articles text");

    if (!preg_match("/[0-9]Puzzle/", $this->currentlyParsedFile)) {
      return array();
    }
    $rawArticles = array("file" => $this->currentlyParsedFile);
    return $rawArticles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = basename($this->currentlyParsedFile);
    $headline = explode(".", $headline);
    $headline = trim(preg_replace('/[^0-9]/', ' ', $headline[0]));
    $headline = explode(" ", $headline);
    $headline = "Sudoku (Medium) - " . $headline[1];
    return $headline;
  }

  //Return empty body for the article by Mariam request
  protected function getStory(&$text) {
    $this->addLog("getting article body");
    return "";
  }

  protected function getImages(&$text) {
    // this contain just one image [ video image ]
    $this->addLog("getting video image");
    $image_Name = basename($this->currentlyParsedFile);
    $image_full_path = $this->currentDir . $image_Name;
    $original_name = explode('.', $image_Name);
    if (file_exists($image_full_path)) {
      //the video name is same as the image name here  		
      $name = $this->model->getImgReplacement($image_Name, $this->currentDir, 5854);
      $img['img_name'] = str_replace(IMGS_PATH, IMGS_HOST, $name);
      $img['original_name'] = $original_name[0];
      $img['image_caption'] = "";
      $img['is_headline'] = false;
      $img['image_original_key'] = sha1($image_Name);
      $images[] = $img;
      $original_name = explode('.', $image_Name);
      $original_name[0] = str_replace("Puzzle", "Sol", $original_name[0]);
      $image_Name = implode(".", $original_name);
      $image_full_path = $this->currentDir . $image_Name;

      if (file_exists($image_full_path)) {
        //the video name is same as the image name here  		
        $name = $this->model->getImgReplacement($image_Name, $this->currentDir, 5854);
        $img['img_name'] = str_replace(IMGS_PATH, IMGS_HOST, $name);
        $img['original_name'] = $original_name[0];
        $img['image_caption'] = "";
        $img['is_headline'] = false;
        $img['image_original_key'] = sha1($image_Name);
        $images[] = $img;
      }
      return $images;
    } else {

      return false;
    }
  }

  //Handle empty body
  public function parse() {
    $articles = array();
    foreach ($this->files as $file) {
      if ($this->extensionFilter) {
        if (!syndParseHelper::isFileExtension($file, $this->extensionFilter)) {
          $this->addLog("Wrong extension for file : $file)");
          continue;
        }
      }
      if (!file_exists($file)) {
        $this->addLog("file dose not exist: $file)");
        continue;
      }

      $this->addLog("get file contents (file:$file)");
      $fileContents = $this->getFileContents($file);

      if (!$fileContents) {
        continue;
      }

      $this->currentlyParsedFile = $file;
      $this->loadCurrentDirectory();
      $this->loadUpperDir();
      $rawArticles = $this->getRawArticles($fileContents);
      foreach ($rawArticles as $rawArticle) {
        $article = $this->getArticle($rawArticle);
        $articles[] = $article;
      }
    }
    return $articles;
  }

}
