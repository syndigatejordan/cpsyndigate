<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: Palestine News Network (PNN) 
	// Titles   : Palestine News Network (PNN) [Hebrew]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_166 extends syndParseRss
	{
		public function customInit()
		{
			parent::customInit();
			$this->charEncoding = 'WINDOWS-1255';
			$this->defaultLang = $this->model->getLanguageId('he');
		}
		

		/**
		 * main parsing processing method
		 *
		 * @return array of articles
		 */
		public function parse()
		{
			$articles = parent::parse();

			$artCount = count($articles);
			for($i=0; $i<$artCount; $i++) {
				$articles[$i]['story'] = $this->textFixation($articles[$i]['story']);
			}

			return $articles;
		}
	}
?>
