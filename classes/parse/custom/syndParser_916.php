<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : Al-Ahram Publishing House
//Title     : Al Ahram al Duwali [ Arabic ]
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_916 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $story = $this->textFixation($this->getElementByName('fulldescription', $text));
    $story = preg_replace('!\s+!', ' ', $story);
    $story = strip_tags($story, '<p><br><strong><b><u><i><img>');
    return $story;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    $date = explode('CMT', $date);
    if (is_array($date)) {
      $date = trim($date[0]);
    }
    return $this->dateMapper($date);
  }

  public function getOriginalCategory(&$text) {
    $category = $this->getElementByName('category', $text);
    return $category;
  }

  protected function getImages(&$text) {
    $this->story = $this->textFixation($this->getCData($this->getElementByName('description', $text)));
    $imagesArray = array();
    preg_match_all("/<img[^>]+\>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  private function dateMapper($date) {

    $date = trim($date);
    $monthsEn = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',);
    $monthsAr = array('يناير', 'فبراير', 'مارس', 'أبريل', 'مايو', 'يونيو', 'يوليو', 'أغسطس', 'سبتمبر', 'أكتوبر', 'نوفمبر', 'ديسمبر',);

    $daysEn = array('Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri',);
    $daysAr = array('السبت', 'الأحد', 'الإثنين', 'الثلاثاء', 'الأربعاء', 'الخميس', 'الجمعة',);

    $date = str_replace($daysAr, $daysEn, $date);
    $date = str_replace($monthsAr, $monthsEn, $date);
    return $this->dateFormater($date);
  }

}
