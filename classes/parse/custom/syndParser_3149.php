<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Libya Herald  [# publisher id = 1153]
//Title      : The Libya Herald [ English ] 
//Created on : May 09, 2018, 8:49:03 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3149 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}