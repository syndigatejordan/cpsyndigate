<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Germán Alvarez Comunicación Corporativa  [# publisher id = 1492]
//Title      : Region Mar del Plata [ Spanish ] 
//Created on : Sep 23, 2021, 8:12:20 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7442 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
    protected function getVideos(&$text) {
        $this->addLog("Getting videos");
        $videos = array();
        $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
        $date = $this->getElementByName('date', $text);
        $date = date('Y-m-d h:i:s', strtotime($date));
        $story = $this->getCData($this->getElementByName('fulltext', $text));
        $story = str_replace("&nbsp;", " ", $story);
        $story = $this->textFixation($story);
        $story = trim(preg_replace('!\s+!', ' ', $story));
        $matches = null;
        preg_match_all('/<audio(.*?)<\/audio>/is', $story, $matches);
        foreach ($matches[0] as $match) {
            $video = array();
            $videoname = syndParseHelper::getImgElements($match, 'audio', 'src');
            $videoname = $videoname[0];
            $mimeType = syndParseHelper::getImgElements($match, 'audio', 'type');
            $mimeType = $mimeType[0];

            $mimeType = "";
            $video['video_name'] = $videoname;
            $video['original_name'] = $videoName;
            $video['video_caption'] = $videoName;
            $video['mime_type'] = $mimeType;
            $video['added_time'] = $date;
            $videos[] = $video;
        }
        return $videos;
    }


}
