<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : PriMetrica Inc./TeleGeography  [# publisher id = 539]
//Title      : CommsUpdate [ English ] 
//Created on : Dec 12, 2019, 12:48:37 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1601 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}