<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Xinhua News Agency  [# publisher id = 1555]
//Title      : Shanghai Securities News [ simplified Chinese ] 
//Created on : Mar 28, 2021, 7:55:47 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10592 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh-Hans'); 
	} 
}