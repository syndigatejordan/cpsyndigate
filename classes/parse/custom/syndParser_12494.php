<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IFC - International Finance Corporation  [# publisher id = 1773]
//Title      : IFC (World Bank Group) [ Japanese ] 
//Created on : Feb 28, 2022, 12:31:38 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12494 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	} 
}