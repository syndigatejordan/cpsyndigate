<?php 
		///////////////////////////////////////////////////////////////////////////////////// 
		//Publisher : Searchlight Communications Incorporated  
		//Title     : The New Dawn [ English ] 
		/////////////////////////////////////////////////////////////////////////////////////
		
		class syndParser_1188 extends syndParseCms { 
			 public function customInit() { 
				 parent::customInit(); 
				 $this->defaultLang = $this->model->getLanguageId('en'); 
			} 
		}
