<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Conversation Trust (UK) Limited   [# publisher id = 1243]
//Title      : The Conversation (UK Edition) [ English ] 
//Created on : Sep 27, 2021, 7:48:44 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5616 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}