<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Hilal Publishing and Marketing Group  [# publisher id = 4]
//Title      : Akhbar Al Khaleej [ Arabic ] 
//Created on : Feb 28, 2019, 7:55:16 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_395 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}