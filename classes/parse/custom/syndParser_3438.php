<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : NewsCred  [# publisher id = 1173]
//Title      : NewsCred Insights [ English ] 
//Created on : Sep 26, 2021, 2:08:17 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3438 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}