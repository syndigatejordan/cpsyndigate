<?php


///////////////////////////////////////////////////////////////////////////
// Publisher : Independent Print Limited  [# publisher id =534 ] 
// Titles    : Independent on Sunday [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1582 extends syndParseXmlContent {
	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('en');
		$this -> extensionFilter = 'xml';
	}

	protected function getRawArticles(&$fileContents) {
		//get articles
		$this -> addLog("getting articles raw text");
		$fileContents = str_replace('<ByLine/>', '<ByLine></ByLine>', $fileContents);
		return $this -> getElementsByName('NewsComponent', $fileContents);
	}

	public function getStory(&$text) {
		$this -> addLog('Getting article story');
		$body = trim($this -> getElementByName('body.content', $text));
		$body = strip_tags($body, '<p><br><strong><b><u><i>');
		if (empty($body)) {
			return '';
		}
		return $body;
	}

	public function getHeadline(&$text) {
		$headline = trim($this -> getElementByName('HeadLine', $text));
		return $headline;
	}

	public function getAuthor(&$text) {
		$ByLine = trim($this -> getElementByName('ByLine', $text));
		return $ByLine;
	}

	public function getArticleDate(&$text) {
		$date = $this -> getElementByName('DateLine', $text);
		$date = date('Y-m-d', strtotime($date));
		//var_dump($date);exit;
		return $date;
	}

}