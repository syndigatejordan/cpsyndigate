<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Youssef Rakha  [# publisher id = 436]
//Title      : The Sultan’s Seal [ English ] 
//Created on : Jul 29, 2021, 10:36:02 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1385 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}