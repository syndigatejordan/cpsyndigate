<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ansar Press  [# publisher id = 1643]
//Title      : Ansar Press [ Arabic ] 
//Created on : Oct 04, 2021, 7:53:28 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12196 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}