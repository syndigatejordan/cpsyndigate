<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Daily Independent  [# publisher id = 633]
//Title      : Daily Independent [ English ] 
//Created on : Jun 03, 2021, 11:06:14 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10873 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	}
    protected function getStory(&$text) {
        $this->addLog("getting article body");
        $this->story = preg_replace('!\s+!', ' ', $this->story);
        $this->story = preg_replace('/<div(.*?) itemtype="https:\/\/schema.org\/WPAdBlock" (.*?)<\/div>/is', '', $this->story);
        $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
         $this->story = preg_replace('/<h3>Comments(.*)/is', '</div>',  $this->story);
         $this->story = preg_replace('#<div class="code-block code-block-11"><strong>(.*?)</strong></div>#is', '',  $this->story);
         $this->story = preg_replace('#<div class="td-a-rec td-a-rec-id-content(.*?)</div>#is', '',  $this->story);
         $this->story = preg_replace('#<div class="td-all-devices">(.*?)</div>#is', '',  $this->story);
         $this->story = preg_replace('#REVEALED:(.*?)</a></strong>#is', '</a></strong>',  $this->story);
         $this->story = preg_replace('#<div class="code-block code-block-11"(.*?)</p></div>#is', '',  $this->story);
         $this->story = preg_replace('#<p>READ ALSO:<a(.*?)</a></p>#is', '',  $this->story);
         $this->story = preg_replace('#<p>ALSO READ: <a (.*?)</a></p>#is', '',  $this->story);
         $this->story = preg_replace('#<div class="tdwgcsleh tdwgcsleh(.*?)</div></div></div>#is', '',  $this->story);
         $this->story = preg_replace('#<h2>READ ALSO: <a(.*?)</a></h2>#is', '',  $this->story);
         $this->story = preg_replace('#<div class="jkue jkue-clearfix jkue-post-top(.*?)</table></div></div>#is', '',  $this->story);
         $this->story = preg_replace('#<div class="jkue jkue-clearfix jkue-post-top(.*?)</table></div></div>#is', '',  $this->story);
         $this->story = preg_replace('#<div class="mldqv mldqv-clearfix(.*?)</table></div></div>#is', '',  $this->story);
         $this->story = preg_replace('#<div class="bs-pagination-wrapper(.*?)</span></div></div></div></div>#is', '',  $this->story);
         $this->story = preg_replace('#<div class="bs-irp center bs-irp(.*?)</span></div></div>#is', '',  $this->story);
         $this->story = preg_replace('#<div class="kixfcy kixfcy-clearfix kixfcy-post(.*?)</a></div></div>#is', '',  $this->story);
         $this->story = preg_replace('#<div class="lbnxkc lbnxkc-post-inline(.*?)</div> </div></div>#is', '',  $this->story);
         $this->story = preg_replace('#<a href="http://fruitionoaksolutions(.*?)</a>#is', '',  $this->story);
         $this->story = preg_replace('#<a href="https://chat.whatsapp(.*?)</a>#is', '',  $this->story);
         $this->story = preg_replace('/<div id="odkra-(.*?)<\/div>/is', '',  $this->story);
         $this->story = preg_replace('/<div class="odkra odkra-post(.*?)<\/div>/is', '',  $this->story);
         $this->story = strip_tags( $this->story, "<em><a><p><iframe><span><strong>");
        $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
        $this->story = preg_replace('!\s+!', ' ', $this->story);
        $this->story = preg_replace('#<a href="http://fruitionoaksolutions(.*?)</a>#is', '',  $this->story);
        $this->story = preg_replace('#<a href="http://greatlivingn.com/b/(.*?)</a>#is', '',  $this->story);
        if (empty($this->story))
            return $this->textFixation($this->getElementByName('fulltext', $text));
        else {
            return $this->story;
        }
    }
}