<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : HT Media Ltd.  [# publisher id = 89]
//Title      : Josh Talks [ English ] 
//Created on : Apr 02, 2020, 7:55:58 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5977 extends syndParseCms
{

    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('en');
    }

    protected function getStory(&$text)
    {
        $this->addLog("getting article body");
        $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
        $this->story = preg_replace('/<iframe(.*?)<\/iframe>/is', '', $this->story);
        $this->story = preg_replace('!\s+!', ' ', $this->story);
        if (empty($this->story))
            return $this->textFixation($this->getElementByName('fulltext', $text));
        else {
            return $this->story;
        }
    }


    protected function getVideos(&$text)
    {
        $this->addLog("Getting videos");
        $videos = array();
        $matches = null;
        $path = "/usr/local/syndigate/tmp/parser_5977_cache/images";
        $date = $this->textFixation($this->getCData($this->getElementByName('date', $text)));
        $fulltext = $this->textFixation($this->getElementByName('fulltext', $text));
        $videoname = null;
        if (preg_match('/<iframe src="(.*?)"><\/iframe>/is', $fulltext, $videoname)) {
            $link = trim($videoname[1]);
            $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
            $array = explode('.', $link);
            $extension = end($array);
            $file = sha1($link) . ".$extension";
            $difference = strtotime(date("Y-m-d")) - strtotime($date);
            $difference = $difference / 24 / 60 / 60;
            if ($difference > 9) {
                return array();
            }
            if (!file_exists("$path/$file") && $difference <= 9) {
                sleep(10);
                $comma = "curl -u syndigate:xJA#8103KSIS-_9HE  $link > $path/$file";
                echo "$comma" . chr(10);
                shell_exec($comma);
            }
            $video_name = $this->model->getVidReplacement($file, $path, 5977);
            $video_name = str_replace(VIDEOS_PATH, VIDEOS_HOST, $video_name);

            $video = array();
            $video['video_name'] = $video_name;
            $video['original_name'] = $link;
            $video['video_caption'] = $videoName;
            $video['mime_type'] = $extension;
            $video['video_type'] = $extension;
            $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
            if (!isset($video['video_name']) && empty($video['video_name'])) {
                $video = array();
            }
            $videos[] = $video;
        }
    return $videos;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    sleep(1);
    $baseName = basename($imageUrl);
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $imageUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

  public function copyUrlVideoIfNotCached($imageUrl) {
    $baseName = basename($imageUrl);
    $username = 'syndigate';
    $password = 'xJA#8103KSIS-_9HE';
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {

      $myfile = fopen($copiedImage, "w+");
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $imageUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_TIMEOUT, 60);
      curl_setopt($ch, CURLOPT_FILE, $myfile);
      curl_exec($ch);
      curl_close($ch);
      fclose($myfile);
      // $myfile = fopen($copiedImage, "w+");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
