<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Great Lakes Publications Ltdذ  [# publisher id = 1846]
//Title      : Sunrise [ English ] 
//Created on : Sep 12, 2022, 9:05:17 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12709 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}