<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : InSight Crime  [# publisher id = 1355]
//Title      : InSight Crime - Investigations (Full PDF Version) [ Spanish ] 
//Created on : Aug 25, 2020, 12:46:47 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6415 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}