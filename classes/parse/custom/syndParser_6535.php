<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence Kampuchea Presse (AKP)  [# publisher id = 1402]
//Title      : Agence Kampuchea Presse (AKP) [ Khmer ] 
//Created on : Oct 08, 2020, 11:59:51 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6535 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('km'); 
	} 
}