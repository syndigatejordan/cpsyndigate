<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Conversation Global  [# publisher id = 1240]
//Title      : The Conversation (Global Perspective) [ English ] 
//Created on : Oct 14, 2020, 9:03:04 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5612 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}