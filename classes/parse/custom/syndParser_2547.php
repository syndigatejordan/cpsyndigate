<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Cape Chamber  [# publisher id = 825]
//Title      : Cape Chamber of Commerce and Industry (CCCI) - News [ English ] 
//Created on : Feb 01, 2016, 8:11:20 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2547 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}