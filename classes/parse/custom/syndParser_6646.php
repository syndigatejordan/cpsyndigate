<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Palestine News Network (PNN)  [# publisher id = 58]
//Title      : Palestine News Network (PNN) [ German ] 
//Created on : Oct 13, 2020, 10:45:52 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6646 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('de'); 
	} 
}