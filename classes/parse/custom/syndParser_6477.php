<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sahara Press Service  [# publisher id = 1378]
//Title      : Sahara Press Service [ Spanish ] 
//Created on : Oct 07, 2020, 12:47:57 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6477 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}