<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Crisp Network  [# publisher id = 1331]
//Title      : Crisp Network: Travel and Tourism [ Arabic ] 
//Created on : Jul 09, 2020, 8:17:32 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6302 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}