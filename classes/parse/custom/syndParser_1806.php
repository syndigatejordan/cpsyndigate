<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Multimedia Congo s.p.r.l.  [# publisher id = 635]
//Title      : Digital Congo [ English ] 
//Created on : Feb 02, 2016, 11:53:29 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1806 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}