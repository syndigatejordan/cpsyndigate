<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : Customized Feeds for Yahoo
//Title     : 3eesho.com - Custom[ Arabic ]
/////////////////////////////////////////////////////////////////////////////////////

require_once '/usr/local/syndigate/classes/parse/custom/syndParser_1699.php';

class syndParser_1835 extends syndParser_1699 {
    public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('ar');
  }
}