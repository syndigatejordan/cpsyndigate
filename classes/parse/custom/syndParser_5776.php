<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sudan Tribune   [# publisher id = 53]
//Title      : Sudan Tribune - Saudi Arabia [ English ] 
//Created on : Sep 23, 2019, 11:10:22 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5776 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}