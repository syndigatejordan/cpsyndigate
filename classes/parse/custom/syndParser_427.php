<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Saudi Research & Publishing Co.  
//Title     : Arriyadiyah [ Arabic ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_427 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

}
