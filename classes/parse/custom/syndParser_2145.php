<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Talal Abu-Ghazaleh Organization  [# publisher id = 681]
//Title      : TAG IT News Agency [ Arabic ] 
//Created on : Sep 14, 2021, 7:41:43 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2145 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}