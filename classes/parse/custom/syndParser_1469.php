<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Massif Society  [# publisher id = 488]
//Title      : Yerepouni Daily News [ Armenian ] 
//Created on : Aug 15, 2021, 7:21:15 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1469 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ARM'); 
	} 
}