<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Prathibha Sastry  [# publisher id = 1196]
//Title      : The PS Show [ English ] 
//Created on : Sep 19, 2021, 8:52:22 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3900 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}