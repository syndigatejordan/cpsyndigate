<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : NBK Capital  [# publisher id = 695]
//Title      : NBK Capital - MENA Weekly [ English ] 
//Created on : Feb 02, 2016, 11:59:43 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2172 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}