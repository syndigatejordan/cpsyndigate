<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Yemen News Agency (SABA)  [# publisher id = 143]
//Title      : Yemen News Agency (SABA) [ Dutch ] 
//Created on : Nov 26, 2019, 6:55:41 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5873 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('nl'); 
	} 
}