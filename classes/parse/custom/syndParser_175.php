<?php

/////////////////////////////////////////////////////////////////////////////////////
////Publisher  : Al Bawaba (Middle East) Ltd.  [# publisher id = 62]
////Title      : Albawaba.com [ Arabic ]
////Created on : Jun 28, 2021, 8:11:46 AM
////Author     : eyad
///////////////////////////////////////////////////////////////////////////////////////


class syndParser_175 extends syndParseCms
{
    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('ar');
    }
}