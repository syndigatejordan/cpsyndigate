<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Mediamania Sàrl  [# publisher id = 1510]
//Title      : Agence Ecofin [ French ] 
//Created on : Sep 23, 2021, 8:18:41 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7471 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}