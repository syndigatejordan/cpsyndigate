<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Kingdom of Bahrain Ministry of Foreign Affairs  [# publisher id = 838]
//Title      : Kingdom of Bahrain Ministry of Foreign Affairs - News [ English ] 
//Created on : Jan 31, 2016, 3:05:12 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2584 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}