<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : AGERPRES (Romanian National News Agency)  [# publisher id = 1433]
//Title      : AGERPRES (Romanian National News Agency) [ English ] 
//Created on : Dec 14, 2021, 12:45:41 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6696 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}