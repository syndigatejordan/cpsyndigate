<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Tripoli and North Lebanon, Chamber of Commerce, Industry and Agriculture  [# publisher id = 792]
//Title      : Tripoli and North Lebanon, Chamber of Commerce, Industry and Agriculture News [ English ] 
//Created on : Feb 02, 2016, 12:08:17 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2442 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}