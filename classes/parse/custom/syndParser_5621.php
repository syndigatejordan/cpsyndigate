<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Tasnim News Agency  [# publisher id = 1246]
//Title      : Tasnim News Agency [ Arabic ] 
//Created on : May 22, 2019, 6:48:13 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5621 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}