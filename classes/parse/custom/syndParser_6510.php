<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Government of Belize  [# publisher id = 1391]
//Title      : Government of Belize Press Office [ English ] 
//Created on : Sep 19, 2021, 8:55:30 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6510 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}