<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bangladesh Ministry of Foreign Affairs  [# publisher id = 893]
//Title      : Bangladesh Ministry of Foreign Affairs - Statements [ English ] 
//Created on : Apr 07, 2016, 10:12:04 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2658 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}