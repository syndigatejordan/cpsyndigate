<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : CountryWatch Inc.  [# publisher id =549 ] 
//Title     : CountryWatch Reviews [ English ]
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_1652 extends syndParseXmlContent {

  public $countryISO = array('Afghanistan' => 'AFG', 'Albania' => 'ALB', 'Algeria' => 'DZA', 'Andorra' => 'AND', 'Angola' => 'AGO', 'Antigua' => 'ATG', 'Argentina' => 'ARG', 'Armenia' => 'ARM', 'Australia' => 'AUS', 'Austria' => 'AUT', 'Azerbaijan' => 'AZE', 'Bahamas' => 'BHS', 'Bahrain' => 'BHR', 'Bangladesh' => 'BGD', 'Barbados' => 'BDS', 'Belarus' => 'BLR'
      , 'Belgium' => 'BEL', 'Belize' => 'BLZ', 'Benin' => 'BEN', 'Bhutan' => 'BTN', 'Bolivia' => 'BOL', 'Bosnia-Herzegovina' => 'BIH', 'Botswana' => 'BWA'
      , 'Brazil' => 'BRA', 'Brunei' => 'BRN', 'Bulgaria' => 'BGR', 'Burkina-Faso' => 'BFA', 'Burma' => 'MMR', 'Burundi' => 'BDI', 'Cambodia' => 'KHM'
      , 'Cameroon' => 'CMR', 'Canada' => 'CAN', 'Cape-Verde' => 'CPV', 'Central-African-Republic' => 'CAF', 'Chad' => 'TCD', 'Chile' => 'CHL', 'China' => 'CHN'
      , 'China-Hong-Kong' => 'HKG', 'China-Taiwan' => 'TWN', 'Colombia' => 'COL', 'Comoros' => 'COM', 'Congo-DRC' => 'COD', 'Congo-RC' => 'COG', 'Costa-Rica' => 'CRI'
      , 'Cote-d-Ivoire' => 'CIV', 'Croatia' => 'HRV', 'Cuba' => 'CUB', 'Cyprus' => 'CYP', 'Czech-Republic' => 'CZE', 'Denmark' => 'DNK', 'Djibouti' => 'DJI'
      , 'Dominica' => 'DMA', 'Dominican-Republic' => 'DOM', 'East-Timor' => 'TLS', 'Ecuador' => 'ECU', 'Egypt' => 'EGY', 'El-Salvador' => 'SLV', 'Equatorial-Guinea' => 'GNQ'
      , 'Eritrea' => 'ERI', 'Estonia' => 'EST', 'Ethiopia' => 'ETH', 'Fiji' => 'FJI', 'Finland' => 'FIN', 'Former-Yugoslav-Rep-of-Macedonia' => 'MKD', 'France' => 'FRA'
      , 'Gabon' => 'GAB', 'Gambia' => 'GMB', 'Georgia' => 'GEO', 'Germany' => 'DEU', 'Ghana' => 'GHA', 'Greece' => 'GRC', 'Grenada' => 'GRD', 'Guatemala' => 'GTM', 'Guinea' => 'GIN'
      , 'Guinea-Bissau' => 'GNB', 'Guyana' => 'GUY', 'Haiti' => 'HTI', 'Holy-See' => 'VAT', 'Honduras' => 'HND', 'Hungary' => 'HUN', 'Iceland' => 'ISL', 'India' => 'IND'
      , 'Indonesia' => 'IDN', 'Iran' => 'IRN', 'Iraq' => 'IRQ', 'Ireland' => 'IRL', 'Israel' => 'ISR', 'Italy' => 'ITA', 'Jamaica' => 'JAM', 'Japan' => 'JPN', 'Jordan' => 'JOR'
      , 'Kazakhstan' => 'KAZ', 'Kenya' => 'KEN', 'Kiribati' => 'KIR', 'Korea-North' => 'PRK', 'Korea-South' => 'KOR', 'Kuwait' => 'KWT', 'Kyrgyzstan' => 'KGZ', 'Laos' => 'LAO', 'Latvia' => 'LVA'
      , 'Lebanon' => 'LBN', 'Lesotho' => 'LSO', 'Liberia' => 'LBR', 'Libya' => 'LBY', 'Liechtenstein' => 'LIE', 'Lithuania' => 'LTU', 'Luxembourg' => 'LUX', 'Madagascar' => 'MDG'
      , 'Malawi' => 'MWI', 'Malaysia' => 'MYS', 'Maldives' => 'MDV', 'Mali' => 'MLI', 'Malta' => 'MLT', 'Malta' => 'MLT', 'Marshall-Islands' => 'MHL', 'Mauritania' => 'MRT', 'Mauritius' => 'MUS'
      , 'Mexico' => 'MEX', 'Micronesia' => 'FSM', 'Moldova' => 'MDA', 'Monaco' => 'MCO', 'Mongolia' => 'MNG', 'Morocco' => 'MAR', 'Mozambique' => 'MOZ', 'Namibia' => 'NAM', 'Nauru' => 'NRU'
      , 'Nepal' => 'NPL', 'Netherlands' => 'NLD', 'New-Zealand' => 'NZL', 'Nicaragua' => 'NIC', 'Niger' => 'NER', 'Nigeria' => 'NGA', 'Norway' => 'NOR', 'Oman' => 'OMN'
      , 'Pakistan' => 'PAK', 'Palau' => 'PLW', 'Panama' => 'PAN', 'Papua-New-Guinea' => 'PNG', 'Paraguay' => 'PRY', 'Peru' => 'PER', 'Philippines' => 'PHL', 'Poland' => 'POL'
      , 'Portugal' => 'PRT', 'Qatar' => 'QAT', 'Romania' => 'ROU', 'Russia' => 'RUS', 'Rwanda' => 'RWA', 'Saint-Kitts-and-Nevis' => 'KNA', 'Saint-Lucia' => 'LCA'
      , 'Saint-Vincent-and-Grenadines' => 'VCT', 'Samoa' => 'WSM', 'San-Marino' => 'SMR', 'Sao-Tome-and-Principe' => 'STP', 'Saudi-Arabia' => 'SAU', 'Senegal' => 'SEN'
      , 'Serbia' => 'SRB', 'Seychelles' => 'SYC', 'Sierra-Leone' => 'SLE', 'Singapore' => 'SGP', 'Slovakia' => 'SVK', 'Slovenia' => 'SVN', 'Solomon-Islands' => 'SLB'
      , 'Somalia' => 'SOM', 'South-Africa' => 'ZAF', 'Spain' => 'ESP', 'Sri-Lanka' => 'LKA', 'Sudan' => 'SDN', 'Suriname' => 'SUR', 'Swaziland' => 'SWZ', 'Sweden' => 'SWE'
      , 'Switzerland' => 'CHE', 'Syria' => 'SYR', 'Tajikistan' => 'TJK', 'Tanzania' => 'TZA', 'Thailand' => 'THA', 'Togo' => 'TGO', 'Tonga' => 'TON', 'Trinidad-Tobago' => 'TTO'
      , 'Tunisia' => 'TUN', 'Turkey' => 'TUR', 'Turkmenistan' => 'TKM', 'Tuvalu' => 'TUV', 'Uganda' => 'UGA', 'Ukraine' => 'UKR', 'United-Arab-Emirates' => 'ARE'
      , 'United-Kingdom' => 'GBR', 'United-States' => 'USA', 'Uruguay' => 'URY', 'Uzbekistan' => 'UZB', 'Vanuatu' => 'VUT', 'Venezuela' => 'VEN', 'Vietnam' => 'VNM', 'Yemen' => 'YEM', 'Zambia' => 'ZMB', 'Zimbabwe' => 'ZWE');
  public $contant;
  public $tag;
  public $imagesArray = array();

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    return $this->getElementsByName('subCountryWatch', $fileContents);
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = trim($this->getCData($this->getElementByName('category', $text)));
    $headline = explode(",", $headline);
    $headline = $headline[1];
    $filename = basename($this->currentlyParsedFile);
    $filename = str_replace('.xml', '', $filename);
    $filename = array_search($filename, $this->countryISO);
    $headline = $headline . " " . $filename;
    return $headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = date('Y-m-d');
    return $date;
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    //$this->story = $this->getCData($this->getElementByName('text', $text));
    $this->story = html_entity_decode($this->story, ENT_COMPAT, 'UTF-8');
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', " ", $this->story);
    return $this->story;
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    if ($this->contant == "News")
      $cats = "News";
    else {
      $cats = trim($this->getCData($this->getElementByName('category', $text)));
      $cats = explode(",", $cats);
      $cats = $cats[0];
    }
    return $cats;
  }

  protected function getImages(&$text) {
    $this->story = trim($this->textFixation($this->getCData($this->getElementByName('text', $text))));

      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();

    $imagesArray = array();
    preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
      if (!empty($image_caption[0])) {
        $image_caption = $image_caption[0];
      } else {
        $image_caption = "";
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      $imagePath = preg_replace("/\?(.*)/is", "", $imagePath);
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
