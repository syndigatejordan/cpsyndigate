<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ministry of Foreign Affairs of the Palestinain State  [# publisher id = 880]
//Title      : Ministry of Foreign Affairs of the Palestinain State - Ministry Statement  [ Arabic ] 
//Created on : Feb 16, 2016, 9:32:50 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2632 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}