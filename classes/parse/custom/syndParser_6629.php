<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Kuwait News Agency (KUNA)   [# publisher id = 131]
//Title      : Kuwait News Agency (KUNA) [ French ] 
//Created on : Oct 13, 2020, 1:43:54 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6629 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}