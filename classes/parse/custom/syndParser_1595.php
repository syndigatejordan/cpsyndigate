<?php 
		///////////////////////////////////////////////////////////////////////////////////// 
		//Publisher : Standard Publications Ltd.  
		//Title     : The Malta Business Weekly [ English ] 
		/////////////////////////////////////////////////////////////////////////////////////
		
		class syndParser_1595 extends syndParseCms { 
			 public function customInit() { 
				 parent::customInit(); 
				 $this->defaultLang = $this->model->getLanguageId('en'); 
			} 
		}