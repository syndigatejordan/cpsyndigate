<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Middle East Monitor  [# publisher id = 1266]
//Title      : The Middle East Monitor - Explained [ English ] 
//Created on : Oct 13, 2019, 10:38:05 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5839 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}