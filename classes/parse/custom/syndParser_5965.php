<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bloomberg L.P.  [# publisher id = 1202]
//Title      : Bloomberg News Service (U.S. Edition) [ English ] 
//Created on : Aug 04, 2021, 10:53:24 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5965 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}