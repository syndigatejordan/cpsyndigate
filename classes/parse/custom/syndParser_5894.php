<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Addy.media LLC  [# publisher id = 1270]
//Title      : Addy.media - Images [ English ] 
//Created on : Jan 07, 2020, 8:02:52 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5894 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}