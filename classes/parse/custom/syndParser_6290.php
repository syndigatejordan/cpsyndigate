<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Crisp Network  [# publisher id = 1331]
//Title      : Crisp Network: Chef Recipes [ Arabic ] 
//Created on : Jul 09, 2020, 8:16:07 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6290 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}