<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Gaye Média  [# publisher id = 1454]
//Title      : Equonet.net [ French ] 
//Created on : Sep 23, 2021, 8:10:53 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7330 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}