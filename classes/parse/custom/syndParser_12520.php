<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BBC World Service  [# publisher id = 1192]
//Title      : BBC Spanish - News [ Spanish ] 
//Created on : Mar 15, 2022, 12:37:24 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12520 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}