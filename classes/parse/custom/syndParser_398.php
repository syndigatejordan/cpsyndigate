<?php
	class syndParser_398 extends syndParseCms{
         
           public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getStory(&$text) {
      $this->addLog('Getting article story');
      $story = $this->textFixation($this->getCData($this->getElementByName('fulltext', $text)));
      $story = preg_replace("#class(.*?)>#si", ">", $story);
      if (empty($story)) {
          return '';
      }
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);

      return $story;
  }
       }
