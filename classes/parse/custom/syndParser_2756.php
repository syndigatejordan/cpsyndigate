<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : New Times Corporation  [# publisher id = 956]
//Title      : The Ghanaian Times [ English ] 
//Created on : May 22, 2016, 6:38:36 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2756 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}