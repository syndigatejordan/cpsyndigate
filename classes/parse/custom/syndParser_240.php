<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CPI Financial  [# publisher id = 259]
//Title      : CPI Financial [ English ] 
//Created on : Jun 13, 2018, 8:08:10 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_240 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}