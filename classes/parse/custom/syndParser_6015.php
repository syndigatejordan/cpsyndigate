<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Exchange Data International (EDI)  [# publisher id = 174]
//Title      : African Financial & Economic Data (AFED) - Country Profiles [ English ] 
//Created on : Apr 09, 2020, 7:32:09 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6015 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}