<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : SetFron Ltd  [# publisher id = 1564]
//Title      : Opinion Nigeria [ English ] 
//Created on : Apr 21, 2021, 7:47:17 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_10806 extends syndParseCms
{
    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('en');
    }
}
