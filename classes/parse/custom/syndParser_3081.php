<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : TFS Media (Pty) Ltd  [# publisher id = 1112]
//Title      : TALK IoT [ English ] 
//Created on : Sep 19, 2021, 7:05:53 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3081 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}