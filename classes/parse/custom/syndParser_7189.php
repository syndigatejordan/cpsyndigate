<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Mareeg Media  [# publisher id = 1452]
//Title      : Mareeg Media [ English ] 
//Created on : Sep 19, 2021, 10:17:54 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7189 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}