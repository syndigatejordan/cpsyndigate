<?php
class syndParser_223 extends syndParseRss {
	
	public function customInit() {
		parent::customInit();
		$this->defaultLang = $this->model->getLanguageId('en');
	}
	
	
	protected  function getImages(&$text) {
		
		$imagesArray = array();
		$imgs	     = array();
		
		preg_match_all('/<media:content type="image" url="[^>]+jpg">/i', $text, $imgs);		
		$imgs = $imgs[0];
		
		$counter =0;
		foreach ($imgs as $img) {
			
			
			$this->addLog("getting article images");
			$start_from = strpos($img, 'url')+5;
			$imagePath = substr($img, $start_from, strpos($img, '.jpg') + 4 - $start_from);
					
			if(!$imagePath) {					
			   continue;
			}
			if($this->checkImageifCached($imagePath)){
        // Image already parsed..
        continue;
      }	
			$copiedImage = $this->copyUrlImgIfNotCached($imagePath);
			
			if(!$copiedImage) {
			   continue;
			}		
			$images = $this->getAndCopyImagesFromArray(array($copiedImage));
			if($counter>0) {
				$images[0]['is_headline'] = 0;
			}
			
			
			array_push($imagesArray, $images[0]);
			$counter++;
		}
		
		return $imagesArray;			
	}
	
} // End class

