<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : RASANAH - International Institute for Iranian Studies  [# publisher id = 1264]
//Title      : RASANAH - Daily Newswire [ English ] 
//Created on : May 04, 2020, 8:44:13 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5823 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}