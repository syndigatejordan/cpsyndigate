<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Public Domain Review  [# publisher id = 1298]
//Title      : The Public Domain Review - Essays [ English ] 
//Created on : Feb 10, 2020, 8:57:23 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5913 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
