<?php
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : TT Nyhetsbyrån Ab  [# publisher id = 1493]
//Title      : TT Nyhetsbyrån Ab [ English ] 
//Created on : Feb 07, 2021, 9:59:01 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_7443 extends syndParseXmlContent
{

    public $story;

    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('en');
        $this->extensionFilter = 'xml';
    }

    public function getHeadline(&$text)
    {
        $this->addLog("getting article headline");
        return trim($this->textFixation($this->getElementByName('HeadLine', $text)));
    }

    public function getArticleDate(&$text)
    {
        $this->addLog("getting article date");
        $date = trim(preg_replace("/CET(.*)/is", "", $this->getElementByName('DateLine', $text)));
        if ($date) {
            return $this->dateFormater($date);
        }
        return parent::getArticleDate($text);
    }

    public function getOriginalCategory(&$text)
    {
        $this->addLog('getting article category');
        $category = syndParseHelper::getImgElements($text, 'NewsItemType', 'FormalName');
        return $category[0];
    }

    public function getStory(&$text)
    {
        $this->addLog('Getting article story');
        $this->story = $this->textFixation($this->getCData($this->getElementByName('DataContent', $text)));
        $this->story = preg_replace('/<hl1(.*?)<\/hl1>/is', '', $this->story);
        $this->story = preg_replace('/<html(.*?)">/is', '', $this->story);
        $this->story = preg_replace('/<!--(.*?)-->/is', '', $this->story);
        $this->story = preg_replace('/<head>(.*?)<\/head>/is', '', $this->story);
        $this->story = preg_replace('/<h1(.*?)<\/h1>/is', '', $this->story);
        $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) width=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) height=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) align=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
        $this->story = preg_replace("/<media media-type=\"image\">(.*?)<\/media>/is", "", $this->story);
        $this->story = preg_replace('!\s+!', ' ', $this->story);
        $this->story = str_replace('" "', '"', $this->story);
        $this->story = str_replace('<body>', '', $this->story);
        $this->story = str_replace('</body>', '', $this->story);
        $this->story = trim(str_replace('</html>', '', $this->story));
        $this->story = trim(preg_replace('!\s+!', ' ', $this->story));
        if (empty($this->story)) {
            return '';
        }
        return $this->story;
    }

    protected function getRawArticles(&$fileContents)
    {
        //get articles
        $this->addLog("getting articles raw text");
        $matchs = null;
        preg_match_all("/<NewsItem(.*?)<\/NewsItem>/is", $fileContents, $matchs);
        $matchs = $matchs[0];
        return $matchs;
    }

    protected function getAuthor(&$text)
    {
        $this->addLog("getting article author");
        return $this->getElementByName('ByLine', $text);
    }

    protected function getImages(&$text)
    {
        return array();
    }
}
