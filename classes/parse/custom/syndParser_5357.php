<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Organization of Islamic Cooperation (OIC)  [# publisher id = 1209]
//Title      : The Union of News Agencies (UNA) [ Arabic ] 
//Created on : Feb 14, 2019, 8:05:19 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5357 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}