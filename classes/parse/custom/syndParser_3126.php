<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Groupe de presse Iwacu  [# publisher id = 1138]
//Title      : Iwacu [ French ] 
//Created on : Mar 15, 2018, 11:35:32 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3126 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('fr');
  }
}
