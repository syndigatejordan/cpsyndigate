<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Media Times Online  [# publisher id = 1024]
//Title      : Media Times Online [ Arabic ] 
//Created on : Aug 21, 2016, 2:14:40 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2854 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}