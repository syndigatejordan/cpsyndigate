<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Venezuela Analysis, Inc  [# publisher id = 1501]
//Title      : Venezuelanalysis.com [ English ] 
//Created on : Jan 17, 2021, 11:18:32 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7461 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}