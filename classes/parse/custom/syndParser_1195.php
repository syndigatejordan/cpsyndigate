<?php 

///////////////////////////////////////////////////////////////////////////
// Publisher : ArmInfo News Agency  [# publisher id =358 ] 
// Titles    : ArmInfo - Mining Bulletin [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1195 extends syndParseXmlContent {
	
    public function customInit() {
        parent::customInit();
		$this->defaultLang = $this->model->getLanguageId('en');
        $this->extensionFilter = array( 'txt', 'TXT' );
    }
    
    /*public function getRawArticles(&$fileContents) {
		return array($fileContents);
    }*/

    public function getRawArticles(&$fileContents) {
	$this->addLog('Getting article story');

        return $this->getElementsByName('item', $fileContents);
    }

    
    public function getStory(&$text) {
    	$this->addLog('Getting article story');

        $story = trim($this->getElementByName('body', $text));
        $story = '<p>' . str_replace(".\r\n"    , ".\r\n" . "</p>\r\n<p>", $story) .'</p>';               
        return $story;
    }
    
    public function getHeadline(&$text) {
    	$this->addLog('Getting article headline');

        return trim($this->getElementByName('headline', $text));
    }
    
    public function getArticleDate(&$text) {
    	$this->addLog('Getting article date');

        $date = trim($this->getElementByName('pubdate', $text)); 
		list($d, $m, $y) = preg_split('/\//', $date);
		$y = '20' . $y;
		$date = "$y-$m-$d";
		$date =  date('Y-m-d', strtotime($date));
		if ( $date == '1970-01-01' ) {
			$date = date('Y-m-d');
		}
		return $date;
    }
}
