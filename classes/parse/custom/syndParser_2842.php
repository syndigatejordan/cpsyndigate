<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Erem Media FZ LLC  [# publisher id = 1015]
//Title      : Erem News [ Arabic ] 
//Created on : Aug 18, 2016, 11:27:08 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2842 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}