<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Alpha Comunicaciones Integrales  [# publisher id = 1485]
//Title      : San Francisco [ Spanish ] 
//Created on : Mar 27, 2022, 9:25:55 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7423 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}