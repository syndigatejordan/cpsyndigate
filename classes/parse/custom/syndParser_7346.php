<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : La Hora Online  [# publisher id = 1456]
//Title      : La Hora Online [ Spanish ] 
//Created on : Sep 19, 2021, 10:18:01 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7346 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}