<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Democracy Now!  [# publisher id = 1245]
//Title      : Democracy Now! [ English ] 
//Created on : May 21, 2019, 9:30:28 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5618 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace("/<script(.*?)<\/script>/s", "", $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);

      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);

      if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
