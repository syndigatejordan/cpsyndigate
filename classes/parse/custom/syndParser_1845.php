<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Global Data Point Ltd.  [# publisher id = 378]
//Title      : Automotive Monitor Worldwide [ English ] 
//Created on : Aug 10, 2021, 1:52:46 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1845 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}