<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : HYPEBEAST  [# publisher id = 1253]
//Title      : HYPEBEAST - Interviews [ English ] 
//Created on : Jun 11, 2019, 9:10:52 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5652 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}