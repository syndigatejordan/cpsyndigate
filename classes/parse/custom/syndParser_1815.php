<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Baaboonej Company Management Electronic Sites  [# publisher id = 642]
//Title      : El Gawee [ Arabic ] 
//Created on : Dec 13, 2015, 4:03:45 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1815 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
    $this->extensionFilter = 'xml';
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('/<source[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('/<video(.*?)<\/video>/si', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story)) {
      $story = $this->textFixation($this->getElementByName('fulltext', $text));
      $story = preg_replace('/<img[^>]+\>/i', '', $story);
      $story = preg_replace('/<source[^>]+\>/i', '', $story);
      $story = preg_replace('/<video(.*?)<\/video>/si', '', $story);
      $story = preg_replace('!\s+!', ' ', $story);
      return $story;
    } else {
      return $this->story;
    }
  }

  protected function getImages(&$text) {
    $this->story = $this->textFixation($this->getCData($this->getElementByName('fulltext', $text)));
    $imagesArray = array();
    preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = explode('alt="', $img);
      $image_caption = explode('"', $image_caption[1]);
      $image_caption = $image_caption[0];
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = str_replace('%20', ' ', $image_caption);
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");

    $vid = $this->textFixation($text);
    preg_match_all('/<source[^>]+\/>/si', $vid, $iframes);
    $videos = array();
    foreach ($iframes[0] as $iframe) {
      $iframename = explode('src="', $iframe);
      $iframename = $iframename[1];
      $iframename = explode('"', $iframename);
      $link = $iframename[0];
      $videoName = $this->textFixation($this->getElementByName('title', $text));
      $video['video_name'] = $link;
      $video['original_name'] = $videoName;
      $video['video_caption'] = $videoName;

      /* $mimeType = $videoInfo['mime_type'];
        $video['mime_type'] = $mimeType; */
      $video['added_time'] = date('Y-m-d h:i:s', strtotime(date('Y-m-d h:i:s')));
      $videos[] = $video;
    }
    return $videos;
  }

}
