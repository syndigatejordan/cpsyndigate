<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Yeni Gün Haber Ajansi Basin Ve Yayincilik A.Ş.   [# publisher id = 1312]
//Title      : Cumhuriyet [ Turkish ] 
//Created on : Mar 01, 2020, 10:17:35 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5941 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('tr');
  }
}
