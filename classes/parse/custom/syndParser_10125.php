<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : FreeMedia Group  [# publisher id = 1531]
//Title      : Politico SL Newspaper [ English ] 
//Created on : Sep 23, 2021, 8:14:49 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10125 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}