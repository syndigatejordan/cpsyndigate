<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al-Ahram Publishing House  [# publisher id =265 ] 
//Title      : Al-Ahram Messai [ Arabic ] 
//Created on : Feb 18, 2020, 11:46:50 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////	
class syndParser_911 extends syndParseRss {

  public $source;
  public $Third_patty = array("وكالات", "وكالات الأنباء", "وكالات الأنباء‏", "أ .ف.ب", "أ ف ب", "أ ش أ", "أ ش أ‏", "أ‏.‏ ف‏.‏ ب", "أ‏.‏ ش‏.‏ أ‏", "أ‏.‏ش‏.‏ أ", "أ‏.‏ش‏.‏أ‏", "ا ش ا", "د ب أ", "د. ب. أ", "رويترز");

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('item', $fileContents);
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->getElementByName('title', $text);
    $headline = preg_replace('/[^\x{0600}-\x{06FF}A-Za-z0-9 !@#$%^&*().\'"]/u', '', $headline);
    return $this->textFixation($headline);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubdate', $text);
    $date = explode('CMT', $date);
    $date = $date[0];
    return date('Y-m-d', strtotime($date));
  }

  protected function getStory(&$text) {
    if (in_array($this->source, $this->Third_patty)) {
      return"";
    }
    $this->addLog("getting article text");
    $this->story = $this->getCData($this->getElementByName('body', $text));
    $this->story = preg_replace('/[\n\r]/', "</p><p>", $this->story);
    if (!empty($this->story)) {
      $this->story = "<p>$this->story</p>";
    }
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    $this->story = str_replace('<p></p>', "", $this->story);
    return $this->textFixation($this->story);
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getElementsByName('category', $text);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = $this->textFixation($this->getCData($cat));
      }
    }
    return implode(', ', $originalCats);
  }

  public function getAbstract(&$text) {
    $this->addLog('Getting article summary');
    $this->abs = strip_tags($this->textFixation($this->getCData($this->getElementByName('abstract', $text))));
    return $this->abs;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $this->source = trim($this->textFixation($this->getElementByName('source', $text)));
    return $this->source;
  }

  protected function getImages(&$text) {

    $imagesArray = array();
    if (in_array($this->source, $this->Third_patty)) {
      return $imagesArray;
    }
    $images = $this->getElementsByName('image', $text);
    foreach ($images as $image) {
      $this->addLog("getting article images");
      $imagePath = str_replace(' ', '%20', $image);
      if (!$imagePath) {
        return;
      }
      if ($image = $this->checkImageifCached($imagePath)) {
        // Image already parsed..
        return;
      }
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        return;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    $bigimage = $this->getElementsByName('bigimage', $text);
    foreach ($bigimage as $image) {
      $this->addLog("getting article images");
      $imagePath = str_replace(' ', '%20', $image);
      if (!$imagePath) {
        return;
      }
      if ($image = $this->checkImageifCached($imagePath)) {
        // Image already parsed..
        return;
      }
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        return;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = sha1($imageUrl) . ".jpeg";
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $curl_handle = curl_init();
      curl_setopt($curl_handle, CURLOPT_URL, $imageUrl);
      curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl_handle, CURLOPT_USERAGENT, 'php');
      $imgContent = curl_exec($curl_handle);
      curl_close($curl_handle);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
