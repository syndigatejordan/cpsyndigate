<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : United Arab Emirates Ministry of Foreign Affairs  [# publisher id = 884]
//Title      : United Arab Emirates Ministry of Foreign Affairs - News [ English ] 
//Created on : Feb 18, 2016, 12:59:55 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2642 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}