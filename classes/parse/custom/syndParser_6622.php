<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : KHOVAR (National information agency of Tajikistan)  [# publisher id = 1418]
//Title      : KHOVAR (National information agency of Tajikistan) [ Persian ] 
//Created on : Sep 19, 2021, 8:56:49 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6622 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fa'); 
	} 
}