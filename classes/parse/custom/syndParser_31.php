<?php

	////////////////////////////////////////////////////////////////////////
	// Publisher : Dar Al Tahrir Publishing and Printing House 
	// Titles    : Le Progrès Egyptien [ French ]
	////////////////////////////////////////////////////////////////////////

	class syndParser_31 extends syndParseCms
	{

		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('fr');
		}
		
		public function getStory(&$text) {
			$story = parent::getStory($text);
			$story = str_replace('��', '', $story);			
			return $story;
		}

		public function getHeadline(&$text) {
			$headline = parent::getHeadline($text);
			$headline = str_replace('��', '', $headline);			
			return $headline;
		}	
	}