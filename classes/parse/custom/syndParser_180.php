<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : Cihan Haber Ajansı Reklâmcılık A.Ş.  [# publisher id =65 ] 
//Title     : Cihan News Agency (CNA) (English)
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_180 extends syndParseRss {

  // Available video types
  public $filename;

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('item', $fileContents);
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->getElementByName('title', $text);
    return $this->textFixation($headline);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    $date = trim(preg_replace('/GMT(.*)/is', '', $date));
    return date('Y-m-d', strtotime($date));
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = $this->getElementByName('description', $text);
    return $this->textFixation($story);
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getCData($this->getElementByName('creator', $text)));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getElementsByName('category', $text);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = $this->textFixation($this->getCData($cat));
      }
    }
    return implode(', ', $originalCats);
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    preg_match_all('#<media:content type="image"(.*?)/media:content>#si', $text, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = $this->getElementByName('media:title', $img);
      $imageInfo = explode('url="', $img);
      $imageInfo = $imageInfo[1];
      $imageInfo = explode('"', $imageInfo);
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = str_replace('%20', ' ', $image_caption);
      }
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }

    preg_match('#<media:thumbnail url="(.*?)" />#si', $text, $thumbnail);
    $this->addLog("getting video images thumbnail");
    $imagePath = $thumbnail[1];
    if (!$imagePath) {
      return $imagesArray;
    }
    if ($this->checkImageifCached($imagePath)) {
      // Image already parsed..
      return $imagesArray;
    }
    $imagePath = str_replace(' ', '%20', $imagePath);
    $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

    if (!$copiedImage) {
      echo "no pahr";
      return $imagesArray;
    }
    $images = $this->getAndCopyImagesFromArray(array($copiedImage));
    $images[0]['image_caption'] = "";
    $images[0]['is_headline'] = TRUE;
    array_push($imagesArray, $images[0]);
    return $imagesArray;
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    preg_match_all('#<media:content height(.*)/media:content>#si', $text, $cvideo);
    $cvideo = explode('</media:content>', $cvideo[0][0]);
    $videoname = $cvideo[1];
    $videoname = explode('url="', $videoname);
    $videoname = $videoname[1];
    $videoname = explode('"', $videoname);
    $link = $videoname[0];

    $videoName = $story = $this->getElementByName('media:title', $cvideo[1]);
    $videos = array();

    $video['video_name'] = $link;
    $video['original_name'] = $videoName;
    $video['video_caption'] = $videoName;

    /* $mimeType = $videoInfo['mime_type'];
      $video['mime_type'] = $mimeType; */
    $date = $this->getElementByName('pubDate', $text);
    $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
    $videos[] = $video;
    return $videos;
  }

}
