<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : United Press  LLC   
//Title     : The Moscow Times [ English ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1396 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

}
