<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : China Chamber of Commerce of Metals Minerals & Chemicals Importers & Exporters  [# publisher id = 843]
//Title      : China Chamber of Commerce of Metals Minerals & Chemicals Importers & Exporters (CCCMC) - News [ English ] 
//Created on : Feb 17, 2016, 8:58:48 AM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2600 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}