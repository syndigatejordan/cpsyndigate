<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Le Pays  [# publisher id = 946]
//Title      : Le Pays [ French ] 
//Created on : May 18, 2016, 12:46:43 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2742 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}