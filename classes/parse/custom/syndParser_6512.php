<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Government of the Republic of Trinidad and Tobago  [# publisher id = 1393]
//Title      : Trinidad & Tobago Government News [ English ] 
//Created on : Sep 23, 2021, 8:05:51 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6512 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}