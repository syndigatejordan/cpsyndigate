<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The LionWork Group LLC  [# publisher id = 1687]
//Title      : E-Crypto News [ English ] 
//Created on : Nov 23, 2021, 11:52:47 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12280 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}