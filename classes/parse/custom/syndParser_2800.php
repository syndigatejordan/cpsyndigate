<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ALP Content Services operating as AL Publishing  [# publisher id = 979]
//Title      : Al-medicine.net [ Arabic ] 
//Created on : Jul 13, 2016, 1:04:24 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2800 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}