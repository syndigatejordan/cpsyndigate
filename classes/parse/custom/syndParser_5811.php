<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : RASANAH - International Institute for Iranian Studies  [# publisher id = 1264]
//Title      : RASANAH - Iranian Issues Series [ Arabic ] 
//Created on : Sep 01, 2020, 9:11:39 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5811 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}