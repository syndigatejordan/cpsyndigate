<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BEHAK Multimedia Private Limited Company (PLC)  [# publisher id = 728]
//Title      : New Business Ethiopia  [ English ] 
//Created on : Mar 06, 2022, 10:19:52 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2285 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}