<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dockaysworld Media  [# publisher id = 1471]
//Title      : Dockaysworld [ English ] 
//Created on : Aug 15, 2021, 12:12:10 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7366 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}