<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Thomson Reuters Foundation  [# publisher id = 309]
//Title      : Aswat Masriya [ English ] 
//Created on : Apr 18, 2016, 13:00:59 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1026 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}