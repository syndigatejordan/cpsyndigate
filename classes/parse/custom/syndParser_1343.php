<?php

//////////////////////////////////////////////////////////////////////////////
// Publisher: Mobinet SARL  [# publisher id =410 ] 
// Titles   : L'Éconews [French]
//////////////////////////////////////////////////////////////////////////////

class syndParser_1343 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('fr');
  }

  public function getImages(&$text) {
    $this->addLog('Getting article images.');

    $regExp = '<enclosure[\s]+.*url[\s]*=[\s]*([\"]([^>\"]*)[\"]|' . "[\\']([^>\\']*)[\\']|([^>\\s]*))([\\s]|[^>])*[\/]?>";
    preg_match_all("/$regExp/i", $text, $matches);

    $img = '';
    $img = $matches[2][0];
    if ($this->checkImageifCached($img)) {
      // Image already parsed..
      return;
    }
    if ($img) {
      $localImg = $this->copyUrlImgIfNotCached($img);
      if ($localImg) {
        return $this->getAndCopyImagesFromArray(array($localImg));
      }
    } else {
      return array();
    }
  }

}
