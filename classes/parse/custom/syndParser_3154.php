<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Hypenica  [# publisher id = 1158]
//Title      : Cape Business News [ English ] 
//Created on : Aug 28, 2017, 10:35:52 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3154 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}