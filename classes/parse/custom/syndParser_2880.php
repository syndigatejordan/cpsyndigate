<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Zimbabwe Newspapers (1980) Limited  [# publisher id = 958]
//Title      : Chronicle [ English ] 
//Created on : Jan 11, 2021, 1:12:31 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2880 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}