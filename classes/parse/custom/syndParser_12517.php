<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Australian Associated Press (AAP)  [# publisher id = 527]
//Title      : Australian Associated Press (AAP) [ English ] 
//Created on : Mar 13, 2022, 9:52:14 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12517 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}