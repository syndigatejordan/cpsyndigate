<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Korolov Media  [# publisher id = 1839]
//Title      : Hypergrid Business [ English ] 
//Created on : Aug 17, 2022, 10:12:33 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12699 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}