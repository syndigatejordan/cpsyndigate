<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Post (Pty) Ltd  [# publisher id = 1475]
//Title      : The Post [ English ] 
//Created on : Sep 19, 2021, 10:19:49 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7371 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}