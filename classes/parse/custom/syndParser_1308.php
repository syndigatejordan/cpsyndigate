<?php
///////////////////////////////////////////////////////////////////////////
// Publisher : Bina Aisha Shah  [# publisher id =397 ]
// Titles    : Bina Shah [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1308 extends syndParseXmlContent {
	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('en');
	}

	protected function getRawArticles(&$fileContents) {
		//get articles
		$this -> addLog("getting articles raw text");

		$articles = $this -> getElementsByName('entry', $fileContents);
		return $articles;
	}

	protected function getHeadline(&$text) {
		$this -> addLog("getting article headline");

		$headline = $this -> getElementByName('title', $text);
		return trim($this -> textFixation($headline));
	}

	protected function getAuthor(&$text) {
		$this -> addLog("getting article author");

		$author = $this -> getElementByName('author', $text);
		$author = $this -> getElementByName('name', $author);
		return $this -> textFixation($author);
	}

	protected function getArticleDate(&$text) {
		$this -> addLog("getting article date");

		$date = $this -> getElementByName('published', $text);
		return $this -> dateFormater($date);
	}

	protected function getStory(&$text) {
		$this -> addLog("getting article text");
		$story = $this -> getCData($this -> getElementByName('content', $text));
		$story = html_entity_decode($story);
		$story = strip_tags($story, '<p><b><strong><i><a><ul><li><ol><div><br><blockquote><span><s>');
		return $story;
	}

	protected function getImages(&$text) {
		$body = $this -> textFixation($this -> getCData($this -> getElementByName('content', $text)));
		$images = $this -> getElementsByName('a', $body);

		$imagesArray = array();
		foreach ($images as $img) {
			$this -> addLog("getting article images");

			$imagePath = syndParseHelper::getImgElements($img, 'img');

			if (is_array($imagePath) && $imagePath) {
				$imagePath = $imagePath[0];
			}

			if (!$imagePath) {
				continue;
			}

			$copiedImage = $this -> copyUrlImgIfNotCached($imagePath);
			if (!$copiedImage) {
				continue;
			}

			$images = $this -> getAndCopyImagesFromArray(array($copiedImage));
			array_push($imagesArray, $images[0]);
			$text = str_replace($img, '', $text);
		}
		return $imagesArray;
	}

}