<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Irish ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_579 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority' 		=>	'Dámhachtana Údarás', 
										'Nature of Contract'		=>	'Cineál Conartha', 
										'Regulation of Procurement' =>	'Soláthar a Rialáil', 
										'Type of bid required'		=> 	'Cineál ar tairiscint ag teastáil', 
										'Award Criteria'			=>	'Critéir dámhachtana', 
										'Original CPV'				=> 	'Bunaidh CPV', 
										'Country'					=>	'Tír', 
										'Document Id' 				=>	'Doiciméad Id', 
										'Type of Document'			=>	'Cineál Document', 
										'Procedure'					=>	'Nós imeachta', 
										'Original Language'			=>	'Bunaidh Teanga', 
										'Current Language'			=> 	'Reatha Teanga');	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('ga');
		}
	}