<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Alpha Comunicaciones Integrales  [# publisher id = 1485]
//Title      : TuComunidad [ Spanish ] 
//Created on : Mar 27, 2022, 9:26:21 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7428 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}