<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Marshall Islands Journal  [# publisher id = 1086]
//Title      : The Marshall Islands Journal [ English ] 
//Created on : Aug 06, 2017, 6:12:26 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3073 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}