<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : MBC Group  
//Title     : MBC TV Station [ English ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_191 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");

    $this->story = $this->textFixation($this->getElementByName('fulltext', $text));
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    return $this->story;
  }

}
