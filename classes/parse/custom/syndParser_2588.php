<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Republic of Iraq Ministry of Foreign Affairs  [# publisher id = 839]
//Title      : Republic of Iraq Ministry of Foreign Affairs - News [ Arabic ] 
//Created on : Jan 30, 2016, 8:30:18 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2588 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}