<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Infos Plus Gabon  [# publisher id = 1160]
//Title      : Infos Plus Gabon [ French ] 
//Created on : Sep 23, 2021, 7:55:17 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3173 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}