<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Stichting Global Voices  [# publisher id = 1235]
//Title      : Global Voices [ Aymara ] 
//Created on : Feb 22, 2021, 7:59:56 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5585 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ay'); 
	} 
}