<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CG Arabia SPC.  [# publisher id = 366]
//Title      : GulfInsider [ English ] 
//Created on : Jan 27, 2021, 9:14:56 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7473 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}