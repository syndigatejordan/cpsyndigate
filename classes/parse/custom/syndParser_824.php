<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Impression Presse Edition  
//Title     : La Nouvelle Tribune [ French ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_824 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('fr');
  }
}
