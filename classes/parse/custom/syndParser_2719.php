<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Namibia Media Holdings  [# publisher id =924 ] 
//Title      : Allgemeine Zeitung [ German ] 
//Created on : May 20, 2016, 9:59:01 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2719 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('de');
  }

}
