<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sarmady Communications  [# publisher id = 763]
//Title      : FilFan [ Arabic ] 
//Created on : Sep 09, 2020, 10:44:44 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2373 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
