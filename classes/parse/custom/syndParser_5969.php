<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Aylien Ltd.  [# publisher id =1323 ] 
//Title      : AYLIEN - Adverse Media Screening Service [ English ] 
//Created on : Mar 19, 2020, 10:05:58 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5969 extends syndParseXmlContent {

  public $story;
  public $imagesArray = array();
  public $category;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('fullArticle', $fileContents);
    return $articles;
  }

  public function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return trim($this->textFixation($this->getElementByName('title', $text)));
  }

  public function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = trim($this->getElementByName('published_at', $text));
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  public function getArticleOriginalId($params = array()) {
    $articleOriginalId = $this->getElementByName("id", $params['text']);
    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }

    return $this->title->getId() . '_' . sha1($articleOriginalId);
  }

  protected function getAbstract(&$text) {
    $this->addLog("getting article summary");
    return trim($this->getElementByName('jsondata', $text));
  }

  public function getArticleReference(&$text) {
    $this->addLog('Getting article Link');
    $matches = null;
    if (preg_match("/<links><permalink>(.*?)<\/permalink>/is", $text, $matches)) {
      return $matches[1];
    } else {
      return "";
    }
  }

  public function getStory(&$text) {
      $this->addLog('Getting article story');
      $story = trim($this->textFixation($this->getElementByName('body', $text)));
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $story = preg_replace('/<img[^>]+\>/i', '', $story);
      return $story;
  }

  protected function getImages(&$text)
  {
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      return array();

      $this->imagesArray = array();
      $media = $this->getElementByName("media", $text);

      $url = $this->getElementByName("url", $media);
      $imgs = array();
      $imgs[] = $url;
      foreach ($imgs as $imagePath) {
          $this->addLog("getting article images");
          //echo $imagePath.PHP_EOL;
          if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no path";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    sleep(1);
    $baseName = sha1($imageUrl) . ".jpeg";
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );

      $ch = curl_init($imageUrl);
      curl_setopt_array($ch, $options);
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
