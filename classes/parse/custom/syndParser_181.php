<?php

//////////////////////////////////////////////////////////////////////////////
// Publisher: Hilal Publishing and Marketing Group
// Titles   : Gulf Construction
//////////////////////////////////////////////////////////////////////////////

class syndParser_181 extends syndParseAbTxtSample {

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'WINDOWS-1256';
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getHeadline(&$text, $headlineTagName = 'Headline') {
    //get article title
    $this->addLog("getting article headline");
    $headline = $this->textFixation($this->getItemValue($headlineTagName, $text));
    $this->removeItem($headlineTagName, $text);
    $headline = str_replace("â€ک", "‘", $headline);
    $headline = str_replace("â€™", "’", $headline);
    return $headline;
  }

  protected function getAuthor(&$text) {
    //get author
    $this->addLog("getting article author");
    $author = $this->textFixation($this->getItemValue('Dateline', $text));
    $this->removeItem('Dateline', $text);
    return $author;
  }

  protected function getStory(&$text) {
    $story = strip_tags(parent::getStory($text), '<br><BR><p><P><div><DIV>');
    $story = explode('© Copyright', $story);
    $story = preg_replace("/\r\n|\r|\n/", "</p><p>", $story[0]);
    return trim($story);
  }

  protected function getArticleDate(&$text, $dateTagName = 'Date') {
    //get article date
    $this->addLog("getting article date");
    $date = $this->getItemValue($dateTagName, $text);
    $this->removeItem($dateTagName, $text);
    $date = $this->getCurrentParsedFileDateFromFolder(); // TO Be removed wehen the Title fix the date issue in his XML files.
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  protected function getOriginalCategory(&$text) {
    $this->addLog("getting original article category");
    preg_match('/\(Category:(.*?)\)/is', $text, $cat);
    $text = str_replace($cat[0], "", $text);
    $cat = trim($cat[1]);
    return $cat;
  }

  protected function getImages(&$text) {
      $imagesArray = array();
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      return $imagesArray;
      $this->addLog("getting article images");
      $image_Name = null;
      preg_match('/\(Image:(.*?)\)/is', $text, $image_Name);
      $text = str_replace($image_Name[0], "", $text);
      $imagePath = trim($image_Name[1]);
      if (!empty($imagePath)) {
          $imagePath = "http://www.gulfconstructionworldwide.com/" . $imagePath;
          if (!$imagePath) {
              return array();
          }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        return array();
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no path ";
        return array();
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
      return $imagesArray;
    } else {

      return array();
    }
  }

}
