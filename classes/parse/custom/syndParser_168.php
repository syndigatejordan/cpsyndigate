<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ITP Digital Ltd.  [# publisher id = 55]
//Title      : ArabianBusiness.com [ Arabic ] 
//Created on : Jun 28, 2020, 6:47:30 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_168 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}