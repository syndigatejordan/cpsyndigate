<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BSE India  [# publisher id = 1746]
//Title      : Corporate Announcements [ English ] 
//Created on : Jan 25, 2022, 12:41:52 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12351 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}