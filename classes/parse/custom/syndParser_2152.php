<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Hatem AlKhathlan  [# publisher id = 685]
//Title      : Saudi Shift [ Arabic ] 
//Created on : Sep 19, 2021, 7:06:32 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2152 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}