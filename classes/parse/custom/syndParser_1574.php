<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Breaking Media, Inc.  [# publisher id = 531]
//Title      : Breaking Energy [ English ] 
//Created on : Sep 26, 2021, 8:44:05 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1574 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}