<?php

////////////////////////////////////////////////////////////////////////
// Publisher : Media Options Ltd.
// Titles    : EMBIN (Emerging Markets Business Information News) [ English ]
////////////////////////////////////////////////////////////////////////

class syndParser_2074 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
    $this->charEncoding = 'ISO-8859-1';
  }

  protected function getRawArticles(&$fileContents) {
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('articles', $fileContents);
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    return trim($this->textFixation($this->getElementByName('body', $text)));
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return trim($this->textFixation($this->getElementByName('HL1', $text)));
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->textFixation($this->getElementByName('date', $text));
    return $this->dateFormater(date('Y-m-d', strtotime($date)));
  }

  protected function getAbstract(&$text) {
    $this->addLog("getting article abstract");
    return trim($this->textFixation($this->getElementByName('summary', $text)));
  }

  protected function getOriginalCategory(&$text) {
    $this->addLog("getting article original category");
    return trim($this->textFixation($this->getElementByName('category', $text)));
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article Author");
    return trim($this->textFixation($this->getElementByName('author', $text)));
  }

}