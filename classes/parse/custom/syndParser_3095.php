<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Motivate publishing  [# publisher id = 116]
//Title      : The Black Hat Guide [ English ] 
//Created on : Sep 10, 2017, 11:21:22 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3095 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}