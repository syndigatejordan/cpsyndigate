<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sarl Interface rp  [# publisher id = 509]
//Title      : Radio-M.net [ French ] 
//Created on : Jul 18, 2022, 12:19:42 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12669 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}