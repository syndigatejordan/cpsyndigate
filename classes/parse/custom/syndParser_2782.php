<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Bilad Establishment for Journalism & Publishing  [# publisher id = 977]
//Title      : Al Bilad [ Arabic ] 
//Created on : Sep 06, 2016, 8:56:43 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2782 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}