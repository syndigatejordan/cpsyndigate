<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Smithsonian Institution  [# publisher id = 1315]
//Title      : The Smithsonian Institution [ English ] 
//Created on : Mar 03, 2020, 8:32:24 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5949 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}