<?php

//////////////////////////////////////////////////////////////////////////////
// Publisher: African Press Organization (APO)  [# publisher id =448 ] 
// Titles   : African Press Organization (APO) [ Arabic ]
//////////////////////////////////////////////////////////////////////////////

class syndParser_2378 extends syndParseRss {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  public function getStory(&$text) {
    $this->addLog('getting article story');
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('/<div(.*?)>/is', '', $this->story);
    $this->story = str_replace('</div>', '', $this->story);
    $this->story = preg_replace('!\s+!', " ", $this->story);
    return $this->story;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return $this->textFixation($this->getCData($this->getElementByName('title', $text)));
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getCData($this->getElementByName('source', $text)));
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    $date = date("Y-m-d", strtotime($date));
    return $date;
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getElementsByName('category', $text);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = $this->textFixation($this->getCData($cat));
      }
    }
    return implode(', ', $originalCats);
  }

  protected function getImages(&$text) {

    $this->story = trim($this->textFixation($this->getCData($this->getElementByName('description', $text))));
    $imagesArray = array();
    preg_match_all("/<img[^>]+\>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");

      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];

      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {

    $baseName = str_replace('/200/70', '', $imageUrl);
    $baseName = basename($baseName);
    $baseName = sha1($imageUrl) . ".jpeg";
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $curl_handle = curl_init();
      curl_setopt($curl_handle, CURLOPT_URL, $imageUrl);
      curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl_handle, CURLOPT_USERAGENT, 'php');
      $imgContent = curl_exec($curl_handle);
      curl_close($curl_handle);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

  public function getArticleOriginalId($params = array()) {
    $articleHeadline = trim($params['headline']);
    if (!$articleHeadline) {
      return parent::getArticleOriginalId($params);
    } else {
      return $this->title->getId() . "_" . sha1($articleHeadline);
    }
  }

}
