<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : Chamber of Commerce, Industry and Agriculture of Beirut and Mount Lebanon  [# publisher id =785 ] 
//Title     : Chamber of Commerce, Industry and Agriculture of Beirut and Mount Lebanon news [ arabic ]
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2432 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = trim($this->textFixation($this->getCData($this->getElementByName('fulltext', $text))));
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    $this->story = html_entity_decode($this->story, ENT_QUOTES, "UTF-8");
    if (empty($this->story)) {
      return "";
    } else {
      return $this->story;
    }
  }

}
