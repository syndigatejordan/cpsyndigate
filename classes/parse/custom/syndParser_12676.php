<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : African Eye Report Ltd  [# publisher id = 1831]
//Title      : African Eye Report [ English ] 
//Created on : Jul 28, 2022, 1:04:14 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12676 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}