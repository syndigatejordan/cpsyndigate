<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Financial News  [# publisher id = 1556]
//Title      : Financial News [ simplified Chinese ] 
//Created on : Mar 28, 2021, 8:13:12 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10594 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh-Hans'); 
	} 
}