<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Pulse Media   [# publisher id = 1291]
//Title      : Le Desk [ French ] 
//Created on : Aug 15, 2021, 9:57:57 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5904 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}