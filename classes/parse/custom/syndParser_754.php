<?php
//////////////////////////////////////////////////////////////////////////////
// Publisher:  AmmanNet
// Titles   : Parliament Monitor [ Arabic ]
//////////////////////////////////////////////////////////////////////////////

class syndParser_754 extends syndParseRss {
	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('ar');
	}

	protected function getStory(&$text) {
		$this -> addLog("getting article text");
		$text = $this -> getCData($this -> getElementByName('content:encoded', $text));
		$story = $this -> textFixation($text);
		$story = strip_tags($story, '<p> <strong> <br>');
		$story = preg_replace("/<p[^>]+\>/i", "<p>", $story);
		return $story;
	}

	public function getAuthor(&$text) {
		$ByLine = trim($this -> getElementByName('dc:creator', $DataContent));
		return $ByLine;
	}

}
