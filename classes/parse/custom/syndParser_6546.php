<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Central News Agency (CNA)  [# publisher id = 1406]
//Title      : Central News Agency (CNA) [ Chinese ] 
//Created on : Oct 19, 2020, 11:49:43 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6546 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh'); 
	} 
}