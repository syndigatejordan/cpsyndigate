<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Pakistan Ministry of Foreign Affairs  [# publisher id = 899]
//Title      : Pakistan Ministry of Foreign Affairs - Press Releases [ English ] 
//Created on : Apr 07, 2016, 10:10:37 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2671 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}