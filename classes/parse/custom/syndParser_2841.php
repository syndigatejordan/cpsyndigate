<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Zarks Media  [# publisher id = 1014]
//Title      : Channel Post Arabia [ Arabic ] 
//Created on : Aug 21, 2016, 1:56:45 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2841 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}