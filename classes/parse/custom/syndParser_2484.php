<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Iran Chamber of Commerce, Industries, Mines & Agriculture  [# publisher id = 814]
//Title      : Iran Chamber of Commerce, Industries, Mines & Agriculture (ICCIMA) News [ English ] 
//Created on : Feb 01, 2016, 8:59:03 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2484 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}