<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Jamaica Information Service (JIS)  [# publisher id = 1395]
//Title      : Jamaica Information Service (JIS) [ English ] 
//Created on : Oct 20, 2020, 9:48:20 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6514 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}