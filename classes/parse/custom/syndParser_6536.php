<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : AKIpress News Agency  [# publisher id = 40]
//Title      : AKIpress News Agency [ Russian ] 
//Created on : Oct 22, 2020, 9:47:23 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6536 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}