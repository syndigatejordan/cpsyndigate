<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Arcturus Publishing Limited  [# publisher id = 1334]
//Title      : Our Beautiful World: Colour by Numbers [ English ] 
//Created on : Feb 08, 2021, 2:31:05 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_8929 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}