<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Middle East Monitor  [# publisher id = 1266]
//Title      : The Middle East Monitor - The Week in Pictures [ English ] 
//Created on : Oct 13, 2019, 10:42:39 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5844 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}