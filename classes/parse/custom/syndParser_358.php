<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : United Press International (UPI)  [# publisher id = 126]
//Title      : UPI  [ English ] 
//Created on : Jun 14, 2016, 10:38:42 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_358 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}