<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Mediaquest Corp  [# publisher id =84 ]  
//Title      : Communicate [ English ] 
//Created on : May 22, 2017, 10:44:25 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_780 extends syndParseCms {

    public function customInit() {
        parent::customInit();
        $this->defaultLang 	= $this->model->getLanguageId('en');
    }


}