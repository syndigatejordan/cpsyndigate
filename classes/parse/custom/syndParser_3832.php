<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Asia News Network (ANN)  [# publisher id = 659]
//Title      :  Asia News Network (ANN) - Analysis [ English ] 
//Created on : Sep 06, 2018, 5:53:27 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3832 extends syndParseCms {

  public $story;

  protected function getArticle(&$text) {
    try {
      $allowedTags = '<h1><h2><h3><h4><h5><h6><b><i><u><p><hr><br><br/><br /><big><em><small><strong><sub><sup><ins><del><s><strike><code><kbd><q><img><BLOCKQUOTE><cite><dd><div><span><ul><li><ol><style><table><td><th><tr><tbody><thead><a><iframe><embed><video><source><object><param><figcaption><figure>';

      //$article['iptc_id']     = $this->getIptcId($text);
      $article['title_id'] = $this->title->getId();
      $article['lang_id'] = $this->getLangId($text);
      $article['headline'] = strip_tags($this->getHeadline($text));
      $article['abstract'] = strip_tags($this->getAbstract($text));
      $article['author'] = strip_tags($this->getAuthor($text));
      $article['date'] = $this->getArticleDate($text);
      $article['has_time'] = $this->getHasTime();
      $article['parsed_at'] = time();
      $article['original_data'] = $this->getOriginalData($text);
      $article['images'] = $this->getImages($text);
      $article['videos'] = $this->getVideos($text);

      $issueNum = isset($article['original_data']['issue_number']) ? $article['original_data']['issue_number'] : 0;
      $article['original_data']['original_article_id'] = $this->getArticleOriginalId(array('text' => &$text, 'headline' => $article['headline'], 'articleDate' => $article['date'], 'issueNum' => $issueNum));
      $article['original_data']['old_original_article_id'] = syndParseContent::getArticleOriginalId(array('text' => &$text, 'headline' => $article['headline'], 'articleDate' => $article['date'], 'issueNum' => $issueNum));
      $article['original_data']['original_category'] = strip_tags($this->getOriginalCategory($text));
      $article['original_data']['reference'] = strip_tags($this->getArticleReference($text));

      /* 			
        Because getIptcId remove the category -in some parsers like AbText- the text and we do use this in
        other function we put it at last
       */
      $article['iptc_id'] = $this->getIptcId($text);

      $this->processExtraTags($text);
      $article['story'] = strip_tags($this->getStory($text), $allowedTags);
    } catch (Exception $e) {
      $this->addLog($e->getMessage(), ezcLog::ERROR);
      $this->errorMessage = $e->getMessage();
      $this->defaultProcessState = ezcLog::FAILED_AUDIT;
    }

    return $article;
  }

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $this->story = preg_replace('/<style(.*?)<\/style>/is', '', $this->story);
    $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) srcset=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) sizes=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) width=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) height=".*?"/i', '$1', $this->story);
    $this->story = trim(preg_replace('!\s+!', ' ', $this->story));
    return $this->story;
  }

  protected function getImages(&$text) {
      $imagesArray = array();
      $this->story = $this->getCData($this->getElementByName('fulltext', $text));
      $this->story = str_replace("&nbsp;", " ", $this->story);
      $this->story = $this->textFixation($this->story);
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();

      preg_match_all("/<img(.*?)>/i", $this->story, $imgs);
      foreach ($imgs[0] as $img) {
          $this->addLog("getting article images");
          $image_caption = '';
          $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
          if (!empty($image_caption[0])) {
              $image_caption = $image_caption[0];
          } else {
              $image_caption = "";
          }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      $imagePath = preg_replace("/\?(.*)/is", "", $imagePath);
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = basename($imageUrl);
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $imageUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
