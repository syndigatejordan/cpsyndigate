<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Iranian Labour News Agency (ILNA)  [# publisher id = 1411]
//Title      : Iranian Labour News Agency (ILNA) [ Arabic ] 
//Created on : Oct 20, 2020, 11:58:06 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6569 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}