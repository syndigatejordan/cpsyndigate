<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Televisión Pública Argentina - News  [# publisher id = 1399]
//Title      : Televisión Pública Argentina - Noticias [ Spanish ] 
//Created on : Oct 15, 2020, 9:25:49 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6520 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}