<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ukrainian National News Agency "Ukrinform"  [# publisher id = 1337]
//Title      : Ukrinform [ Spanish ] 
//Created on : Sep 23, 2021, 8:01:33 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6038 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}