<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Gulf Tech Arabia  [# publisher id = 984]
//Title      : Gulf Tech Arabia [ Arabic ] 
//Created on : Aug 10, 2016, 6:51:32 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2804 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}