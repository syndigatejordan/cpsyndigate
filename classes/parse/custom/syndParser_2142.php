<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Talal Abu-Ghazaleh Organization  [# publisher id = 681]
//Title      : Talal Abu-Ghazaleh IP News [ English ] 
//Created on : Sep 14, 2021, 7:41:39 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2142 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}