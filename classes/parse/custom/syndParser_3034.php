<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Wiser Hotel project  [# publisher id = 1096]
//Title      : Hotel Jen [ English ] 
//Created on : Jan 11, 2017, 2:01:59 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3034 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = $this->textFixation($this->getElementByName('fulltext', $text));
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    return $this->story;
  }

  protected function getImages(&$text) {
    
  }

}
