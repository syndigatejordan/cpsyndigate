<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Times Media Group  [# publisher id =767 ] 
//Title      : The Sunday Times (South Africa) [ English ] 
//Created on : Jan 24, 2016, 4:59:12 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2388 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('Article', $fileContents);
    return $articles;
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $body = null;
    preg_match('/<Element Type="Body">(.*?)<\/Element>/is', $text, $body);
    $body = $body[0];
    $story = $this->textFixation($this->getCData($this->getElementByName('Content', $body)));
    if (!empty($story)) {
      $story = preg_replace("/\r\n|\r|\n/", "</p><p>", $story);
      $story = '<p>' . $story . '</p>';
    }
    $story = str_replace('<p></p>', '', $story);
    if (empty($story)) {
      return "";
    } else {
      return $story;
    }
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = null;
    preg_match('/<Element Type="Headline">(.*?)<\/Element>/is', $text, $headline);
    $headline = trim($headline[0]);
    $headline = trim($this->getElementByName('Content', $headline));
    return $this->textFixation($headline);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('PublicationDate', $text);
    $date = date('Y-m-d', strtotime($date));
    return $date;
  }

  public function getOriginalCategory(&$text) {
    $category = trim($this->getCData($this->getElementByName('Categories', $text), ','));
    return $category;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return trim($this->textFixation($this->getElementByName('Byline', $text)));
  }

}
