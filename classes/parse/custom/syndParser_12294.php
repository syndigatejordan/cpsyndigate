<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Jawad Al-Raed for Media, Communication and Technology  [# publisher id = 1695]
//Title      : Algerie Maintenant [ Arabic ] 
//Created on : Jan 03, 2022, 1:01:53 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12294 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}