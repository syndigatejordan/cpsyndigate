<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Searchlight Communications Incorporated  [# publisher id = 474]
//Title      : The New Dawn [ English ] 
//Created on : Sep 19, 2021, 7:06:40 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1442 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}