<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Philippine Information Agency  [# publisher id = 1427]
//Title      : Philippine Information Agency [ English ] 
//Created on : Oct 13, 2020, 11:42:43 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6648 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}