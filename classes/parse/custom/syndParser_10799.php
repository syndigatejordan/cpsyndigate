<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Paulo Sérgio Pereira Santos -ME  [# publisher id = 1562]
//Title      : Paracatu News [ Portuguese ] 
//Created on : Sep 23, 2021, 8:16:45 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10799 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}