<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Notre Voie  [# publisher id = 1851]
//Title      : Notre Voie [ French ] 
//Created on : Sep 25, 2022, 9:13:40 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12714 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}