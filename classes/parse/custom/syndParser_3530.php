<?php

///////////////////////////////////////////////////////////////////////////
// Publisher  : Cision  [# publisher id =1183 ] 
// Titles     : PR Newswire Malaysia [English]
// Created on : Jul 31, 2018, 10:48:41 AM
// Author     : eyad
///////////////////////////////////////////////////////////////////////////

class syndParser_3530 extends syndParseXmlContent {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('NewsML', $fileContents);
  }

  public function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return trim($this->textFixation($this->getElementByName('HeadLine', $text)));
  }

  public function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = trim($this->getElementByName('DateId', $text));
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  protected function getAbstract(&$text) {
    $this->addLog('getting article summary');
    return trim($this->textFixation($this->getElementByName('SubHeadLine', $text)));
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return trim($this->textFixation($this->getElementByName('SlugLine', $text)));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $category = syndParseHelper::getImgElements($text, 'NewsItemType', 'FormalName');
    return $category[0];
  }

  public function getArticleOriginalId($params = array()) {
    $articleOriginalId = $this->getElementByName('TransmissionId', $params['text']);

    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }

    return $this->title->getId() . '_' . sha1($articleOriginalId);
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = preg_replace('/<html(.*?)">/is', '', $this->story);
    $this->story = preg_replace('/<!--(.*?)-->/is', '', $this->story);
    $this->story = preg_replace('/<head>(.*?)<\/head>/is', '', $this->story);
    $this->story = preg_replace('/<h1(.*?)<\/h1>/is', '', $this->story);
    $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) width=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) height=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) align=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    $this->story = str_replace('" "', '"', $this->story);
    $this->story = str_replace('<body>', '', $this->story);
    $this->story = str_replace('</body>', '', $this->story);
    $this->story = trim(str_replace('</html>', '', $this->story));
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getImages(&$text) {
    $this->imagesArray = array();
    $this->story = $this->textFixation($this->getCData($this->getElementByName('DataContent', $text)));
    $imgs = array();
    preg_match_all("/<img[^>]+\>/i", $this->story, $imgs);

    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = explode('alt="', $img);
      $image_caption = explode('"', $image_caption[1]);
      $image_caption = $image_caption[0];
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      $oldimagePath = $imagePath;
      $imagePath = explode('?', $imagePath);
      if (!empty($imagePath[0])) {
        $imagePath = $imagePath[0];
      } else {

        $imagePath = $imageInfo[0];
      }
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no path" . PHP_EOL;
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($oldimagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = basename($imageUrl);
    $baseName = explode(".", $baseName);
    $baseNameUn = sha1($imageUrl) . "." . $baseName[1];
    $copiedImage = $this->imgCacheDir . $baseNameUn;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $imageUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        unlink($copiedImage);
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
