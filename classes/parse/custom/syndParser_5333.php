<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Asianet-Pakistan (Pvt) Ltd.  [# publisher id = 83]
//Title      : The News International [ English ] 
//Created on : Jan 14, 2020, 9:03:09 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5333 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}