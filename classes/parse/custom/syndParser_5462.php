<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : 7 Dimensions Media FZE  [# publisher id = 1216]
//Title      : Aviation Guide [ English ] 
//Created on : Jul 15, 2019, 12:03:37 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5462 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}