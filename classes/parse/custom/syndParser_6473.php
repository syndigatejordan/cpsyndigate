<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Maghreb Press Agency  [# publisher id = 1377]
//Title      : Maghreb Press Agency [ French ] 
//Created on : Oct 06, 2020, 1:19:27 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6473 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}