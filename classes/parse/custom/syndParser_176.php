<?php

/////////////////////////////////////////////////////////////////////////////////////
////Publisher  : Al Bawaba (Middle East) Ltd.  [# publisher id = 62]
////Title      : Albawaba.com [ English ]
////Created on : Jun 21, 2021, 8:11:46 AM
////Author     : eyad
///////////////////////////////////////////////////////////////////////////////////////

class syndParser_176 extends syndParseCms
{
    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('en');
    }
}