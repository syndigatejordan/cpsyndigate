<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The United Nations Department of Global Communications  [# publisher id = 1400]
//Title      : UN News [ Spanish ] 
//Created on : Oct 20, 2020, 1:42:17 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6526 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}