<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Kabulpress.org  [# publisher id = 75]
//Title      : Kabulpress.org [ English ] 
//Created on : Aug 09, 2021, 9:39:13 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_230 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}