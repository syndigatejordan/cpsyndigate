<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Algeria Press Service  [# publisher id = 136]
//Title      : Algeria Press Service [ Tamazight ] 
//Created on : Oct 05, 2020, 10:36:33 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6459 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zgh'); 
	} 
}