<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Asianet-Pakistan (Pvt) Ltd.  [# publisher id = 83]
//Title      : PM News [ English ] 
//Created on : Feb 16, 2020, 1:39:33 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1934 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}