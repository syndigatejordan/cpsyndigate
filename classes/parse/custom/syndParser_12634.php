<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : L'economiste du Bénin  [# publisher id = 1809]
//Title      : L'economiste du Bénin [ French ] 
//Created on : Jun 05, 2022, 10:04:48 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12634 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}