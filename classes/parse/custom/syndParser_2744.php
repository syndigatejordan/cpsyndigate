<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Le Sahel Dimanche  [# publisher id = 947]
//Title      : Le Sahel [ French ] 
//Created on : Sep 19, 2021, 7:04:31 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2744 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}