<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dar Assayad S.A.L  [# publisher id = 908]
//Title      : Al Anwar [ Arabic ] 
//Created on : May 16, 2016, 10:46:36 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2701 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}