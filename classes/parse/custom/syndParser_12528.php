<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Mentor Marketing&Distribution SRL  [# publisher id = 1789]
//Title      : Romania Journal [ English ] 
//Created on : Mar 30, 2022, 12:57:52 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12528 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}