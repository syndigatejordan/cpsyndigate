<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bahrain News Agency (BNA)  [# publisher id = 137]
//Title      : Bahrain News Agency (BNA) [ English ] 
//Created on : Oct 02, 2019, 8:42:15 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5795 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}