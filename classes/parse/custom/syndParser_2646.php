<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Arabian Post  [# publisher id = 886]
//Title      : The Arabian Post [ English ] 
//Created on : Nov 10, 2021, 11:00:01 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2646 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}