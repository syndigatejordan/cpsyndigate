<?php

//////////////////////////////////////////////////////////////////////////////
// Publisher: Sudan Tribune
// Titles   : Sudan Tribune [ Arabic ]
//////////////////////////////////////////////////////////////////////////////

class syndParser_1004 extends syndParseCms{

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }
}
