<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : EP Executive Press, Inc.  [# publisher id = 1738]
//Title      : Practical ESG (The Corporate Counsel) [ English ] 
//Created on : Jan 16, 2022, 12:58:00 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12341 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}