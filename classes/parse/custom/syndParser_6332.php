<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BANG Media International  [# publisher id = 306]
//Title      : BANG Showbiz - Music [ English ] 
//Created on : Sep 13, 2021, 1:38:46 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6332 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}