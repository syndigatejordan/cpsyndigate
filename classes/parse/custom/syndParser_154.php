<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IC Publications   [# publisher id =51 ]  
//Title      : The Middle East  [ English ] 
//Created on : May 9, 2017, 6:03:17 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_154 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
