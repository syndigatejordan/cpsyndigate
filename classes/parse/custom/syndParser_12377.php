<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Industry Dive  [# publisher id = 1740]
//Title      : Waste Dive [ English ] 
//Created on : Jan 27, 2022, 10:10:01 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12377 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}