<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Addy.media LLC  [# publisher id = 1270]
//Title      : Addy.media - OTT Video [ English ] 
//Created on : Nov 11, 2019, 9:16:02 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5861 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  //Handle Empty body
  public function parse() {
    $articles = array();
    foreach ($this->files as $file) {
      if ($this->extensionFilter) {
        if (!syndParseHelper::isFileExtension($file, $this->extensionFilter)) {
          $this->addLog("Wrong extension for file : $file)");
          continue;
        }
      }
      if (!file_exists($file)) {
        $this->addLog("file dose not exist: $file)");
        continue;
      }

      $this->addLog("get file contents (file:$file)");
      $fileContents = $this->getFileContents($file);

      if (!$fileContents) {
        continue;
      }

      $this->currentlyParsedFile = $file;
      $this->loadCurrentDirectory();
      $this->loadUpperDir();
      $rawArticles = $this->getRawArticles($fileContents);
      foreach ($rawArticles as $rawArticle) {
        $article = $this->getArticle($rawArticle);
        $articles[] = $article;
      }
    }
    return $articles;
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $videosItem = array();
    $videos = array();
    $videosArray = array();
    $videos = $this->textFixation($this->getElementByName('images', $text));
    $videos = str_replace('<img>', '<image>', $videos);
    $videos = str_replace('</img>', '</image>', $videos);
    $videos = $this->getElementsByName('image', $videos);
    $date = $this->getElementByName('date', $text);
    $date = date('Y-m-d h:i:s', strtotime($date));
    foreach ($videos as $imgName) {
      $video = array();
      $this->addLog("getting video");
      $videoDescription = $this->textFixation($this->getElementByName('img_description', $imgName));
      $video_Name = trim($this->textFixation($this->getElementByName('img_path', $imgName)));
      $mimeType = "";
      $path = pathinfo($video_Name);
      $extension = $path["extension"];
      $videoExtension = array("mp4", "mov", "flv");
      if (!in_array($extension, $videoExtension)) {
        continue;
      }
      if (preg_match("/google.com\/maps/", $video_Name)) {
        continue;
      }
      $video['video_name'] = $video_Name;
      $video['original_name'] = $video_Name;
      $video['video_caption'] = $videoDescription;
      $video['mime_type'] = $mimeType;
      $video['added_time'] = $date;
      $videosItem[] = $video;
    }
    $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));

    $date = $this->getElementByName('date', $text);
    $date = date('Y-m-d h:i:s', strtotime($date));
    $story = $this->getCData($this->getElementByName('fulltext', $text));
    $story = str_replace("&nbsp;", " ", $story);
    $story = $this->textFixation($story);
    $story = trim(preg_replace('!\s+!', ' ', $story));
    $matches = null;
    preg_match_all('/<video(.*?)<\/video>/is', $story, $matches);
    foreach ($matches[0] as $match) {
      $video = array();
      $videoname = syndParseHelper::getImgElements($match, 'source', 'src');
      $videoname = $videoname[0];
      if (preg_match("/youtube.com/", $videoname)) {
        continue;
      }
      $mimeType = syndParseHelper::getImgElements($match, 'source', 'type');
      $mimeType = $mimeType[0];
      $video['video_name'] = $videoname;
      $video['original_name'] = $videoName;
      $video['video_caption'] = $videoName;
      $video['mime_type'] = $mimeType;
      $video['added_time'] = $date;
      $videosItem[] = $video;
    }

    $trailer = array();
    foreach ($videosItem as $key => $item) {
      if (preg_match("/trailer/", strtolower($item["video_caption"]))) {
        $trailer = $item;
        unset($videosItem[$key]);
        array_unshift($videosItem, $trailer);
      }
    }
    return $videosItem;
  }

}
