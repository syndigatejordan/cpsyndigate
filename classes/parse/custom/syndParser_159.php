<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sudan Tribune   [# publisher id = 53]
//Title      : Sudan Tribune [ English ] 
//Created on : Aug 04, 2021, 1:36:35 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_159 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}