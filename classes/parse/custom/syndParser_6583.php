<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IRIB World Service  [# publisher id = 799]
//Title      : ParsToday [ Russian ] 
//Created on : Oct 22, 2020, 12:37:34 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6583 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}