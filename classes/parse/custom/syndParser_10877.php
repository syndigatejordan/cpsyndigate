<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Corporación Medios Digitales del Sur Ltda  [# publisher id = 1590]
//Title      : Noticias de la Tierra [ Spanish ] 
//Created on : Jun 09, 2021, 1:54:11 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10877 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}