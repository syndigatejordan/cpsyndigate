<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Pajhwok Afghan News   [# publisher id = 39]
//Title      : Pajhwok Afghan News [ English ] 
//Created on : Dec 02, 2020, 2:54:56 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_66 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}