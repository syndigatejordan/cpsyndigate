<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Acclaim Communications Ltd  [# publisher id = 1688]
//Title      : The Niche [ English ] 
//Created on : Nov 24, 2021, 8:26:23 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12283 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}