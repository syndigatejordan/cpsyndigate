<?php

//////////////////////////////////////////////////////////////////////////////
// Publisher: Global Times  [# publisher id =478 ]
// Titles   : New Straits Times [ English ]
//////////////////////////////////////////////////////////////////////////////

class syndParser_1448 extends syndParseRss {// syndParseXmlContent

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    //$this -> extensionFilter = 'xml';
  }

  protected function getStory(&$text) {
    $this->story = preg_replace("/<!--(.*?)-->/is", "", $this->story);
    $this->story = preg_replace("#<script(.*?)</script>#is", "", $this->story);
    $this->story = preg_replace("/<style\b[^>]*>(.*?)<\/style>/s", "", $this->story);
    $this->story = preg_replace("/<!--(.|\s)*?-->/", "", $this->story);
    $this->story = preg_replace('/style="(.*?)"/', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    return $this->story;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getElementByName('author', $text));
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    $date = str_replace('GMT', '', $date);
    $date = date('Y-m-d', strtotime($date));
    return $date;
  }

  protected function getImages(&$text) {
      $imagesArray = array();
      $this->story = $this->getElementByName('description', $text);
      $this->story = str_replace("&amp;", "&", $this->story);
      $this->story = $this->textFixation($this->story);
      $this->story = html_entity_decode($this->story, ENT_QUOTES, "UTF-8");
      $this->story = $this->textFixation($this->getCData($this->story));
      $this->story = html_entity_decode($this->story, ENT_QUOTES, "UTF-8");
      $this->story = str_replace("&amp;", "&", $this->story);
      $this->story = $this->textFixation($this->story);

      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();

      $imgs = NULL;
      preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
      foreach ($imgs[0] as $img) {
          $this->addLog("getting article images");
          $image_caption = '';
          $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
          if (!empty($image_caption[0])) {
              $image_caption = $image_caption[0];
          } else {
              $image_caption = syndParseHelper::getImgElements($img, 'img', 'title');
        if (!empty($image_caption[0])) {
          $image_caption = $image_caption[0];
        } else {
          $image_caption = "";
        }
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = sha1($imageUrl) . ".jpeg";
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );

      $ch = curl_init($imageUrl);
      curl_setopt_array($ch, $options);
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

  public function getArticleReference(&$text) {
    $this->addLog('Getting article Link');
    preg_match('/<source url="(.*?)"/is', $text, $link);
    $link = $link[1];
    if (empty($link)) {
      return '';
    }
    return $link;
  }

}
