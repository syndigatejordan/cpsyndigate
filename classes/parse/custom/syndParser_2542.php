<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Galadari Printing & Publishing LLC  [# publisher id = 28]
//Title      : City Times [ English ] 
//Created on : Jan 31, 2016, 3:04:57 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2542 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story)) {
      return $this->textFixation($this->getElementByName('fulltext', $text));
    } else {
      return $this->story;
    }
  }

  protected function getImages(&$text) {
      $this->story = $this->textFixation($this->getCData($this->getElementByName('fulltext', $text)));
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();

      $imagesArray = array();
      preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
      foreach ($imgs[0] as $img) {
          $this->addLog("getting article images");
          $image_caption = '';
          $image_caption = explode('alt="', $img);
          if (!empty($image_caption[1])) {
              $image_caption = explode('"', $image_caption[1]);
              $image_caption = $image_caption[0];
          }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePathSmall = explode('jpg&', $imageInfo[0]);
      if (!empty($imagePathSmall[0])) {
        $imagePath = $imagePathSmall[0] . 'jpg';
      } else {
        $imagePath = $imageInfo[0];
      }
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        $this->addLog("No path");
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imageInfo[0], $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
