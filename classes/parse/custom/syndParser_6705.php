<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bulgarian News Agency (BTA)  [# publisher id = 1435]
//Title      : Bulgarian News Agency (BTA) [ English ] 
//Created on : Sep 23, 2021, 8:08:42 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6705 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}