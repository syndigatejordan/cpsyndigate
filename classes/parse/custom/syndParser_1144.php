<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Nation Media Group Limited  [# publisher id = 338]
//Title      : Daily Nation [ English ] 
//Created on : Aug 09, 2021, 11:36:00 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1144 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}