<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Independent Newspapers (Pty) Limited  [# publisher id =392 ] 
//Title      : DFA  [ English ] 
//Created on : Sep 05, 2017, 06:16:03 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1303 extends syndParseCms {
  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
