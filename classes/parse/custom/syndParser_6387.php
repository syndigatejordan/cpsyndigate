<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : QUOI Media Group Inc.  [# publisher id = 1341]
//Title      : QUOI Media - Seniors [ English ] 
//Created on : Aug 04, 2021, 7:33:03 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6387 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}