<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : TNA Media (Pty) Ltd  [# publisher id = 885]
//Title      : The New Age [ English ] 
//Created on : Feb 28, 2016, 9:50:26 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2645 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}