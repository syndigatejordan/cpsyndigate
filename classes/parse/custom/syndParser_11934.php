<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Grupo Estado  [# publisher id = 1599]
//Title      : Agência Estado [ Portuguese ] 
//Created on : Jun 30, 2021, 9:18:41 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11934  extends syndParseXmlContent
{

    public $languageTitle;
    public $story;

    public function customInit()
    {
        parent::customInit();
        $languageId = $this->title->getLanguageId();//
        $language = new Criteria();
        $language->add(LanguagePeer::ID, $languageId);
        $currentLanguage = LanguagePeer::doSelectOne($language);

        $this->languageTitle = $currentLanguage->getName();
        $this->defaultLang = $this->model->getLanguageId($currentLanguage->getCode());
        $this->extensionFilter = 'xml';
    }

    public function getRawArticles(&$fileContents)
    {
        //get raw articles
        $this->addLog("getting raw articles text");
        $rawArticles = $this->getElementsByName('MATERIA', $fileContents);
        return $rawArticles;
    }

    public function getArticleOriginalId($params = array())
    {
        $defaultArray = array('text' => '', 'headline' => '', 'articleDate' => '', 'issueNum' => 0);
        $params = array_merge($defaultArray, $params);
        if (!$articleOriginalId = trim($this->getElementByName('ID', $params['text']))) {
            return parent::getArticleOriginalId($params);
        }
        return sha1($articleOriginalId . trim($params['headline']));
    }

    public function getOriginalCategory(&$text)
    {
        $this->addLog("getting original category");
        $cats = $this->getElementsByName('KEYWORDS', $text);
        $originalCats = array();

        if (!empty($cats)) {
            foreach ($cats as $cat) {
                $originalCats[] = $this->textFixation($this->getCData($cat));
            }
        }
        return implode(', ', $originalCats);
    }

    public function textFixation($text)
    {
        $text = parent::textFixation($text);
        return html_entity_decode($text);
    }

    public function getArticleReference(&$text)
    {
        $this->addLog('Getting article Link');
        return $this->textFixation($this->getElementByName('URL', $text));
    }

    protected function getArticleDate(&$text)
    {
        //get article date
        $this->addLog("getting article date");
        $date = explode("/", $this->getElementByName('DATA', $text));
        $date = "{$date[2]}-{$date[1]}-{$date[0]}";
        if ($date) {
            return $this->dateFormater($date);
        }
        return parent::getArticleDate($text);
    }

    protected function getHeadline(&$text)
    {
        $this->addLog("getting article headline");
        return $this->textFixation($this->getElementByName('TITULO', $text));
    }

    protected function getAbstract(&$text)
    {
        //get article summary
        $this->addLog("getting article summary");
        return "";

    }

    protected function getStory(&$text)
    {
        $this->addLog("getting article body");
        $this->story = $this->getCData($this->getElementByName('INTEGRA', $text));
        $this->story = str_replace("&amp;", "&", $this->story);
        $this->story = $this->textFixation($this->story);
        $this->story = preg_replace('!\s+!', ' ', $this->story);
        $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
        return $this->story;
    }

    protected function getAuthor(&$text)
    {
        $this->addLog("getting article author");
        return $this->textFixation($this->getElementByName('EDITORIA', $text));

    }

    protected function getImages(&$text)
    {
        $imagesArray = array();
        return $imagesArray;
    }
}
