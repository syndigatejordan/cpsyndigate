<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : NEO CYMED PUBLISHING LTD  [# publisher id = 41]
//Title      : Cyprus Mail  [ English ] 
//Created on : Aug 04, 2021, 1:08:08 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_70 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}