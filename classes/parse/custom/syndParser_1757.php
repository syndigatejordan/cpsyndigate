<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Turan Information Agency  [# publisher id = 424]
//Title      : Turan Information Agency - Political Monitoring [ English ] 
//Created on : Aug 02, 2021, 11:51:07 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1757 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}