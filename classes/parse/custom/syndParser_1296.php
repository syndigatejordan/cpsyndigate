<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Independent Newspapers (Pty) Limited  [# publisher id = 392]
//Title      : Post [ English ] 
//Created on : Jul 28, 2021, 1:57:59 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1296 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}