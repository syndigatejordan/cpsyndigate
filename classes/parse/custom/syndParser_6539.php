<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ariana News Agency (ANA)  [# publisher id = 1404]
//Title      : Ariana News Agency (ANA) [ English ] 
//Created on : Sep 19, 2021, 8:55:43 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6539 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}