<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Yeni Gün Haber Ajansi Basin Ve Yayincilik A.Ş.   [# publisher id = 1312]
//Title      : Cumhuriyet eNewspaper (PDF) [ Turkish ] 
//Created on : Mar 01, 2020, 10:17:41 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5943 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('tr');
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = array();
    $articles[] = $this->currentlyParsedFile;
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $date = preg_replace("/(.*?)_/", "", preg_replace("/\.(.*)/", "", $this->currentlyParsedFile));
    $date = $this->dateFormater($date);
    $date = date('d M, Y', strtotime($date));
    $headline = "Cumhuriyet eNewspaper $date";
    return $this->textFixation($headline);
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    return "";
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = preg_replace("/(.*?)_/", "", preg_replace("/\.(.*)/", "", $this->currentlyParsedFile));
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  protected function getImages(&$text) {
    $this->addLog("getting article image");
    $image_Name = basename($this->currentlyParsedFile);
    $image_full_path = $this->currentDir . $image_Name;
    $original_name = explode('.', $image_Name);
    if (file_exists($image_full_path)) {
      //the video name is same as the image name here  		
      $name = $this->model->getImgReplacement($image_Name, $this->currentDir, 5943);
      $img['img_name'] = str_replace(IMGS_PATH, IMGS_HOST, $name);
      $img['original_name'] = $original_name[0];
      $img['image_caption'] = $image_Name;
      $img['is_headline'] = true;
      $images[] = $img;
      return $images;
    } else {

      return false;
    }
  }

//Handle empty body
  public function parse() {
    $articles = array();
    foreach ($this->files as $file) {
      if ($this->extensionFilter) {
        if (!syndParseHelper::isFileExtension($file, $this->extensionFilter)) {
          $this->addLog("Wrong extension for file : $file)");
          continue;
        }
      }
      if (!file_exists($file)) {
        $this->addLog("file dose not exist: $file)");
        continue;
      }

      $this->addLog("get file contents (file:$file)");
      $fileContents = $this->getFileContents($file);

      if (!$fileContents) {
        continue;
      }

      $this->currentlyParsedFile = $file;
      $this->loadCurrentDirectory();
      $this->loadUpperDir();
      $rawArticles = $this->getRawArticles($fileContents);
      foreach ($rawArticles as $rawArticle) {
        $article = $this->getArticle($rawArticle);
        $articles[] = $article;
      }
    }
    return $articles;
  }

}
