<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Cointelegraph  [# publisher id = 1767]
//Title      : Cointelegraph [ Japanese ] 
//Created on : Feb 21, 2022, 11:29:51 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12471 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	} 
}