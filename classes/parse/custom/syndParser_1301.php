<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Independent Newspapers (Pty) Limited  [# publisher id = 392]
//Title      : Weekend Argus [ English ] 
//Created on : Jul 29, 2021, 9:34:05 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1301 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}