<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : 7 Dimensions Media FZE  [# publisher id = 1216]
//Title      : Aviation Guide [ English ] 
//Created on : Jul 15, 2019, 6:52:53 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5671 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}