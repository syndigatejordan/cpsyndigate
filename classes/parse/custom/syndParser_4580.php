<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Manhal FZ LLC (Arabic)  [# publisher id = 1184]
//Title      : Majallat al-Makhbar: Abḥāth fī al-Lughah wa-al-Adab al-Jazāʼirī [ Arabic ] 
//Created on : Feb 07, 2019, 1:11:52 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

require_once '/usr/local/syndigate/classes/parse/syndParseAlManhal.php';
		
class syndParser_4580 extends syndParseAlManhal {}