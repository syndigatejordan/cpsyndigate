<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Crypto Daily Pte Ltd  [# publisher id = 1683]
//Title      : CryptoDaily [ Spanish ] 
//Created on : Nov 23, 2021, 8:58:05 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12258 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}