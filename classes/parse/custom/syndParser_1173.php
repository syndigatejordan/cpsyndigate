<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher :  GHN News Agency  [# publisher id =353 ] 
//Title     :  GHN News Agency  [ English ] 
//Created on : Aug 20, 2019, 11:34:22 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1173 extends syndParseCms {


  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
