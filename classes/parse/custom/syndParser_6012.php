<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Exchange Data International (EDI)  [# publisher id = 174]
//Title      : EDI - Global Economic Data [ English ] 
//Created on : Apr 09, 2020, 7:14:00 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6012 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}