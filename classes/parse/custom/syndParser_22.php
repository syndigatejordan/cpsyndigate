<?php

////////////////////////////////////////////////////////////////////////
// Publisher :  Ayam Publishing Group
// Title     :  Al Ayam [Arabic]
////////////////////////////////////////////////////////////////////////
class syndParser_22 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = str_replace('onerror="this.src = \'/logo.png\';"', '', $this->story);
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('/<font class="caption">(.*?)<\/font>/is', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  public function getOriginalCategory(&$text) {
    $this->addLog("getting original category");
    $cat = $this->textFixation($this->getElementByName('category', $text));
    $cat = str_replace(' - ', ',', $cat);
    if (!$cat) {
      return parent::getOriginalCategory($text);
    }
    return $cat;
  }

  protected function getImages(&$text) {
      $this->story = $this->textFixation($this->getCData($this->getElementByName('fulltext', $text)));
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();
      $this->imagesArray = array();
      preg_match_all("/<img[^>]+\>/i", $this->story, $imgs);

      foreach ($imgs[0] as $img) {
          $this->addLog("getting article images");
          $image_caption = '';
          $image_caption = explode('alt="', $img);
          $image_caption = explode('"', $image_caption[1]);
          $image_caption = $image_caption[0];
          $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $oldimagePath = $imageInfo[0];
      $imagePath = explode('?', $oldimagePath);
      $imagePath = $imagePath[0];
      if (!@getimagesize($imagePath)) {
        $this->story = str_replace($img, '', $this->story);
        $this->addLog("Incorrect image path");
        continue;
      }
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($oldimagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);

      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

}
