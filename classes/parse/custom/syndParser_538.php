<?php

////////////////////////////////////////////////////////////////////////
// Publisher : Media Options Ltd.
// Titles    : EMBIN (Emerging Markets Business Information News) [ English ]
////////////////////////////////////////////////////////////////////////

class syndParser_538 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
    $this->charEncoding = 'ISO-8859-1';
  }

  protected function getRawArticles(&$fileContents) {
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('articles', $fileContents);
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    return trim($this->textFixation($this->getElementByName('body', $text)));
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return trim($this->textFixation($this->getElementByName('HL1', $text)));
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->textFixation($this->getElementByName('date', $text));
    return $this->dateFormater(date('Y-m-d', strtotime($date)));
  }

  protected function getAbstract(&$text) {
    $this->addLog("getting article abstract");
    return trim($this->textFixation($this->getElementByName('summary', $text)));
  }

  protected function getOriginalCategory(&$text) {
    $this->addLog("getting article original category");
    return trim($this->textFixation($this->getElementByName('category', $text)));
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article Author");
    return trim($this->textFixation($this->getElementByName('author', $text)));
  }

  public function getArticleOriginalId($params = array()) {
    $HL1 = $this->getElementByName('HL1', $params['text']);
    $this->articleOriginalId = $HL1;
    if (!$HL1) {
      return parent::getArticleOriginalId($params);
    }
    $body = $this->getElementByName('body', $params['text']);
    $this->articleOriginalId = $body;
    if (!$body) {
      return parent::getArticleOriginalId($params);
    }
    return $this->title->getId() . "_" . sha1($HL1) . "_" . sha1($body);
  }

  public function afterParse($params = array()) {
    for($i=0;$i<4;$i++)
    {
    $dateDir = date('Y/m/d', strtotime('-'.$i.'day'));
    $remove_command = 'rm /syndigate/sources/home/185/538/' . $dateDir . DIRECTORY_SEPARATOR . 'Syndigate*.xml';
    echo $remove_command . chr(10);
    shell_exec($remove_command);
    }
    parent::afterParse();
  }

}