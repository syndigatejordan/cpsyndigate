<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Alt Presse  [# publisher id = 1814]
//Title      : Alt Presse [ Arabic ] 
//Created on : Jun 19, 2022, 12:25:49 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12646 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}