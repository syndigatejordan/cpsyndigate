<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Hearst Newspapers  [# publisher id = 1600]
//Title      : mrt (Midland Reporter-Telegram) [ English ] 
//Created on : Jul 04, 2021, 12:42:36 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11937 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}