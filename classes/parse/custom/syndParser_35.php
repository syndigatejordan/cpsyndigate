<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : PINAOnline, Inc.  [# publisher id = 23]
//Title      : The Palestine Chronicle [ English ] 
//Created on : Aug 04, 2021, 1:02:40 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_35 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}