<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Ayam Publishing and Printing  [# publisher id = 18]
//Title      : The Daily Tribune [ English ] 
//Created on : Apr 01, 2019, 11:00:28 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5508 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}