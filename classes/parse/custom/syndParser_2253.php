<?php
/////////////////////////////////////////////////////////////////////////////////////
//Publisher  : Sounds Funny And The Not So Funny  [# publisher id = 712]
//Title      : Sounds Funny And The Not So Funny [ English ]
//Created on : Aug 02, 2021, 12:46:02 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2253 extends syndParseCms {
	 public function customInit() {
		 parent::customInit();
		 $this->defaultLang = $this->model->getLanguageId('en');
	}
  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $fulltext = $this->textFixation($this->getElementByName('fulltext', $text));
    $videos = array();
    $matches = null;
    preg_match_all('/<iframe(.*?)<\/iframe>/is', $fulltext, $matches);
    foreach ($matches[0] as $matche) {
      $video = array();
      $videoname = syndParseHelper::getImgElements($matche, 'iframe', 'src');
      if (preg_match("/youtube.com/", $videoname[0])) {
        $id = null;
        preg_match("/embed\/(.*)/", $videoname[0], $id);
        $id = preg_replace("/\?(.*)/is", "", $id[1]);
        $videoname[0] = "https://www.youtube.com/watch?v=$id";
      }
      if (preg_match("/google.com\/maps/", $videoname[0]) || preg_match("/youtube.com/", $videoname[0])) {
        continue;
      }
      $video['video_name'] = $videoname[0];
      $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
      $video['original_name'] = $videoname[0];
      $video['video_caption'] = $videoName;
      $date = $this->getElementByName('date', $text);
      $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
      $video['mime_type'] = "";
      $videos[] = $video;
    }
    return $videos;
  }
}
