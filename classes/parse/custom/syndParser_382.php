<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Perform Media Channels Ltd.  [# publisher id = 135]
//Title      : Goal.com [ Japanese ] 
//Created on : Mar 26, 2020, 2:01:24 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_382 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	}
  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  } 
}
