<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Maghreb Press Agency  [# publisher id = 1377]
//Title      : Maghreb Press Agency [ Arabic ] 
//Created on : Oct 06, 2020, 1:00:02 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6470 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}