<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : United Arab Emirates Ministry of Foreign Affairs  [# publisher id = 884]
//Title      : United Arab Emirates Ministry of Foreign Affairs - News [ Arabic ] 
//Created on : Feb 18, 2016, 1:00:16 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2643 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}