<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Euronews  [# publisher id = 604]
//Title      : Euronews [ Portuguese ] 
//Created on : Apr 10, 2022, 8:58:34 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12579 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}