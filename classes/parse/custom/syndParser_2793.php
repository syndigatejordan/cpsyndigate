<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Islamic Republic News Agency  [# publisher id = 799]
//Title      : ParsToday [ Portuguese ] 
//Created on : Jul 21, 2016, 12:52:29 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2793 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}