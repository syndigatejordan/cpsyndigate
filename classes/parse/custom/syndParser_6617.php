<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Khaosan Pathet Lao (KPL)  [# publisher id = 1417]
//Title      : Lao News Agency (KPL) [ Lao ] 
//Created on : Oct 13, 2020, 9:00:42 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6617 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('lo'); 
	} 
}