<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Fusion Media Limited  [# publisher id = 304]
//Title      : Investing.com Suomi (Finland Edition) [ Finnish ] 
//Created on : May 12, 2022, 7:59:17 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1544 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fi'); 
	} 
}