<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Federation of the Gulf Cooperation Council (GCC) Chambers  [# publisher id = 779]
//Title      : Federation of the Gulf Cooperation Council (GCC) Chambers News [ Arabic ] 
//Created on : Aug 02, 2021, 1:50:45 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2420 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}