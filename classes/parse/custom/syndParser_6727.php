<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Polish Press Agency (PAP)  [# publisher id = 1440]
//Title      : Polish Press Agency (PAP) [ English ] 
//Created on : Oct 18, 2020, 8:46:28 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6727 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}