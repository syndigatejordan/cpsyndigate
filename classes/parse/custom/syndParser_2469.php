<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Egyptian Ministry of Trade & Industry  [# publisher id = 809]
//Title      : Egyptian International Trade Point - EITP News [ Arabic ] 
//Created on : Jan 30, 2016, 8:24:23 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2469 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}