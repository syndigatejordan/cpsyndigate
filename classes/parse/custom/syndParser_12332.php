<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Incisive Business Media (IP) Limited  [# publisher id = 1731]
//Title      : BusinessGreen [ English ] 
//Created on : Jan 16, 2022, 11:52:12 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12332 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}