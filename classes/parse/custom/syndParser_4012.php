<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Bawaba (Middle East) Ltd.  [# publisher id =62 ] 
//Title      : Albawaba.com [ English ] 
//Created on : Nov 01, 2018, 11:51:46 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_4012 extends syndParseCms
{

    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('en');
    }

    protected function getVideos(&$text)
    {
        $this->addLog("Getting videos");
        $videos = array();
        $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));

        $date = $this->getElementByName('date', $text);
        $date = date('Y-m-d h:i:s', strtotime($date));
        $story = $this->getCData($this->getElementByName('fulltext', $text));
        $story = str_replace("&nbsp;", " ", $story);
        $story = $this->textFixation($story);
        $story = trim(preg_replace('!\s+!', ' ', $story));
        $matches = null;
        preg_match_all('/<iframe(.*?)<\/iframe>/is', $story, $matches);
        foreach ($matches[0] as $match) {
            $video = array();
            $videoname = syndParseHelper::getImgElements($match, 'iframe', 'src');
            $videoname = str_replace("//ljsp", "http://ljsp", $videoname[0]);
            if (preg_match("/ljsp.lwcdn.com/", $videoname) || preg_match("/soundcloud.com/", $videoname)) {
                $foundid = null;
        if (preg_match("/ljsp.lwcdn.com(.*?)\?id=(.*?)\&amp;(.*?)/", $videoname, $foundid)) {
          if (isset($foundid[2])) {
            $videoname = "https://cdn.flowplayer.com/6684a05f-6468-4ecd-87d5-a748773282a3/v-{$foundid[2]}_original.mp4";
          } else {
            continue;
          }
        }
        $video['video_name'] = $videoname;
        $video['original_name'] = $videoName;
        $video['video_caption'] = $videoName;
        $video['added_time'] = $date;
        $videos[] = $video;
      }
    }
    return $videos;
  }

}
