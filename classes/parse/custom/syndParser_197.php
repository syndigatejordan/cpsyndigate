<?php

//////////////////////////////////////////////////////////////////////////////
// Publisher: Hilal Publishing and Marketing Group
// Titles   : Oil & Gas News
//////////////////////////////////////////////////////////////////////////////

class syndParser_197 extends syndParseAbTxtSample {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'WINDOWS-1256';
    $this->defaultLang = $this->model->getLanguageId('en');
  }

//  protected function getAuthor(&$text) {
//    //get author
//    $this->addLog("getting article author");
//    $author = $this->textFixation($this->getItemValue('Dateline', $text));
//    $this->removeItem('Dateline', $text);
//    return trim($author);
//  }

  protected function getArticleDate(&$text, $dateTagName = 'Date') {
    //get article date
    $this->addLog("getting article date");
    $date = $this->getItemValue($dateTagName, $text);
    $this->removeItem($dateTagName, $text);
    $this->removeItem('Dateline', $text);
    $date = $this->getCurrentParsedFileDateFromFolder(); // TO Be removed wehen the Title fix the date issue in his XML files.
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  public function getStory(&$text) {
    $story = parent::getStory($text);
    $story = preg_replace('/<img[^>]+\>/i', '', $story);
    $story = preg_replace("/\r\n|\r|\n/", "</p><p>", $story);
    if (!empty($story)) {
      $story = "<p>" . $story . "</p>";
    }
    return $story;
  }

  protected function getOriginalCategory(&$text) {
    $this->addLog("getting original article category");
    preg_match('/\(Category:(.*?)\)/is', $text, $cat);
    $text = str_replace($cat[0], "", $text);
    $cat = trim($cat[1]);
    return $cat;
  }

  protected function getImages(&$text) {
      $this->imagesArray = array();
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      return $this->imagesArray;
      $this->addLog("getting article images");
      $image_Name = trim($this->textFixation($this->getItemValue('Image', $text)));
      $this->removeItem('Image', $text);
      if (!empty($image_Name)) {
          $image_Name = str_replace('-', '_', $image_Name);
          $image_Name = str_replace(')', '_', $image_Name);
          $image_Name = str_replace('(', '_', $image_Name);
          $image_Name = str_replace('/images/', '', $image_Name);
          $image_full_path = $this->currentDir . $image_Name;
          $original_name = explode('.', $image_Name);
      if (file_exists($image_full_path)) {
        //the video name is same as the image name here  		
        $name = $this->model->getImgReplacement($image_Name, $this->currentDir, 5);
        $img['img_name'] = str_replace(IMGS_PATH, IMGS_HOST, $name);
        $img['original_name'] = $original_name[0];
        $img['image_caption'] = $image_Name;
        $img['is_headline'] = true;
        array_push($this->imagesArray, $img);
      } else {
        
      }
    }
    preg_match_all("/<img[^>]+>/i", $text, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

}
