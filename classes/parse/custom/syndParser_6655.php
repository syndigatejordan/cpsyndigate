<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Syrian Arab News Agency (SANA)  [# publisher id = 134]
//Title      : Syrian Arab News Agency (SANA) [ Turkish ] 
//Created on : Oct 15, 2020, 12:18:33 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6655 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('tr'); 
	} 
}