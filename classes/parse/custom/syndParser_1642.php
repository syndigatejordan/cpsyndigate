<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Cyber Media (India) Ltd  [# publisher id = 570]
//Title      : CIOL.com [ English ] 
//Created on : Oct 13, 2021, 2:05:22 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1642 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}