<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Cointelegraph  [# publisher id = 1767]
//Title      : Cointelegraph [ Arabic ] 
//Created on : Feb 21, 2022, 10:59:39 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12466 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}