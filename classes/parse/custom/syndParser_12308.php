<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bobit Business Media  [# publisher id = 1704]
//Title      : Green Fleet [ English ] 
//Created on : Jan 11, 2022, 10:41:37 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12308 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}