<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agencia de Noticias y Publicidad por Internet Querétaro 360 SAS  [# publisher id = 1697]
//Title      : ANTON News [ Spanish ] 
//Created on : Jan 03, 2022, 1:21:08 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12296 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}