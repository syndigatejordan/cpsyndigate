<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : EMDP - Egypt Media Development Program  [# publisher id = 1164]
//Title      : Mantiqti [ Arabic ] 
//Created on : Sep 19, 2021, 7:06:16 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3378 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}