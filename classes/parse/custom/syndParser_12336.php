<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bonhill Group plc  [# publisher id = 1733]
//Title      : ESG Clarity US [ English ] 
//Created on : Jan 16, 2022, 12:14:58 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12336 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}