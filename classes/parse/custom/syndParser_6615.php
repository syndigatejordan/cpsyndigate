<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Khaosan Pathet Lao (KPL)  [# publisher id = 1417]
//Title      : Lao News Agency (KPL) [ English ] 
//Created on : Oct 13, 2020, 8:59:36 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6615 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}