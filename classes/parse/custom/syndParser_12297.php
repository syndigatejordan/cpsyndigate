<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : InfoBrisas  [# publisher id = 1698]
//Title      : InfoBrisas [ Spanish ] 
//Created on : Jan 03, 2022, 2:13:41 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12297 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}