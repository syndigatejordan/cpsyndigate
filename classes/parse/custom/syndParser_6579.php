<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IRIB World Service  [# publisher id = 799]
//Title      : ParsToday [ Japanese ] 
//Created on : Oct 22, 2020, 12:35:03 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6579 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	} 
}