<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Photos Public Domain  [# publisher id = 1308]
//Title      : Photos Public Domain [ English ] 
//Created on : Feb 15, 2020, 6:56:27 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5929 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
