<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Cynomedia Africa SARL  [# publisher id = 1481]
//Title      : Journal du Faso [ French ] 
//Created on : Aug 15, 2021, 12:22:10 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7379 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}