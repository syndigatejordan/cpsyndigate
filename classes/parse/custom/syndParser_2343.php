<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : D.P. Intelligence Experts  [# publisher id =748 ]  
// Titles    : Islamic Terrorist Financing Monitor (ITFM)
///////////////////////////////////////////////////////////////////////////

class syndParser_2343 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    $this->loadCurrentDirectory();
    $this->addLog("getting raw articles text");
    $Item[] = $fileContents;
    return $Item;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return $this->textFixation($this->getElementByName('HEADLINE', $text));
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('DATE', $text);
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = $this->textFixation($this->getElementByName('TEXT', $text));
    $story = str_replace('.', '.</p><p>', $story);
    if (!empty($story)) {
      $story = '<p>' . $story . '</p>';
    }
    $story = preg_replace('!\s+!', ' ', $story);
    $story = str_replace('<p> </p>', '', $story);
    return $story;
  }

}
