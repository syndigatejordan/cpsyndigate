<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : La SNIPE  [# publisher id = 943]
//Title      : La Presse de Tunisie [ French ] 
//Created on : Oct 19, 2016, 12:47:50 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2739 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}