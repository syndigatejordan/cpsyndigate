<?php

//////////////////////////////////////////////////////////////////////////////
// Publisher: Hilal Publishing and Marketing Group
// Titles   : The Gulf
//////////////////////////////////////////////////////////////////////////////

class syndParser_333 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'txt';
  }

  protected function getRawArticles(&$fileContents) {
    $this->addLog("getting raw articles text");
    $article[0] = $fileContents;
    return $article;
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $y = date('Y');
    $text = trim(preg_replace("/© Copyright $y(.*)/is", "", $text));
    return $text;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    preg_match('/\(Headline:(.*?)\)/is', $text, $headline);
    $text = str_replace($headline[0], "", $text);
    return trim($headline[1]);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    preg_match('/\(Date:(.*?)\)/is', $text, $date);
    $text = str_replace($date[0], "", $text);
    preg_match('/\(Dateline:(.*?)\)/is', $text, $date);
    $text = str_replace($date[0], "", $text);
    $dir = dirname($this->currentlyParsedFile);
    $dir = explode('/' . $this->title->getPublisherId() . '/' . $this->title->getId() . '/', $dir);
    $date = str_replace('/', '-', $dir[1]);

    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  /*
    protected function getIptcId(&$text) {
    $this->addLog("getting category name");
    $originalCategoryName = $this->getElementByName('section', $text);
    $originalCategoryName = $this->getElementByName('s1', $originalCategoryName);
    $iptcId = $this->model->getIptcCategoryId($originalCategoryName);
    if (!$iptcId) {
    return parent::getIptcId($text);
    }
    return $iptcId;
    } */

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getElementByName('byline', $text));
  }

  protected function getOriginalCategory(&$text) {
    $this->addLog("getting original article category");
    preg_match('/\(Category:(.*?)\)/is', $text, $cat);
    $text = str_replace($cat[0], "", $text);
    $cat = trim($cat[1]);
    return $cat;
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $this->addLog("getting article images");
    $image_Name = null;
    preg_match('/\(Image:(.*?)\)/is', $text, $image_Name);
    $text = str_replace($image_Name[0], "", $text);
    $imagePath = trim($image_Name[1]);
    if (!empty($imagePath)) {
      $imagePath = "http://www.thegulfonline.com/" . $imagePath;
      $imagePath = str_replace('source/source', 'source', $imagePath);
      if (!$imagePath) {
        return array();
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        return array();
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no path ";
        return array();
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
      return $imagesArray;
    } else {

      return array();
    }
  }

}
