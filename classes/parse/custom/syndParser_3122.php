<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ech-Chorouk Infed SARL  [# publisher id = 1135]
//Title      : Ech-Chorouk El Yaoumi [ Arabic ] 
//Created on : Aug 15, 2021, 9:11:51 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3122 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}