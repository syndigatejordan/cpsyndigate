<?php
/////////////////////////////////////////////////////////////////////////////////////
//Publisher  : Bakhtar News Agency  [# publisher id =222 ]
//Title      : Bakhtar News Agency [ English ]
//Created on : Sep 23, 2020, 10:38:58 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_775 extends syndParseCms
{

    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('en');
    }
}
