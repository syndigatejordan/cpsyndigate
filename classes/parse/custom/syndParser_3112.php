<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Watwan  [# publisher id = 1121]
//Title      : Al Watwan [ Arabic ] 
//Created on : Aug 28, 2017, 9:07:00 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3112 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}