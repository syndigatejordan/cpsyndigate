<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : China Medical News Co., Ltd.  [# publisher id = 1558]
//Title      : China Pharmaceutical Newspapers [ simplified Chinese ] 
//Created on : Apr 09, 2021, 4:30:06 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10793 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh-Hans'); 
	} 
}