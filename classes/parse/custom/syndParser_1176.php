<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Armenpress News Agency  [# publisher id = 354]
//Title      : Armenpress News Agency [ English ] 
//Created on : Aug 09, 2021, 12:06:37 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1176 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}