<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Finews.Asia Pte. Ltd  [# publisher id = 1626]
//Title      : finews.asia [ English ] 
//Created on : Oct 07, 2021, 6:07:32 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12174 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}