<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Maghreb Arabe Presse (MAP)  [# publisher id = 101]
//Title      : Maghreb Arabe Presse [ English ] 
//Created on : Oct 02, 2019, 11:19:53 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5803 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}