<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : Customized Feeds for Yahoo
//Title     : Cape Argus - Custom [ English ]
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_1824 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = $this->textFixation($this->getCData($this->getElementByName('fulltext', $text)));
    $this->story = str_replace("<strong><br />", "<strong>", $this->story);
    $this->story = str_replace("<br /><strong>", "<strong>", $this->story);
    $this->story = str_replace("</strong><br />", "</strong>", $this->story);
    $this->story = str_replace("<br /></strong>", "</strong>", $this->story);
    $this->story = str_replace("<strong><br /></strong>", "", $this->story);
    $this->story = str_replace("<br /><br />", "<br />", $this->story);
    $this->story = str_replace("<br />", "</p><p>", $this->story);
    $this->story = str_replace("<p>\n</p>", "", $this->story);
    $this->story = str_replace("<p>\n", "<p>", $this->story);
    $this->story = str_replace("\n<p>", "<p>", $this->story);
    $this->story = str_replace("\n</p>", "</p>", $this->story);
    $this->story = str_replace("</p>\n", "</p>", $this->story);
    $this->story = str_replace("\n&nbsp;", " ", $this->story);
    $this->story = str_replace(" ", " ", $this->story);
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getImages(&$text) {
    $this->story = $this->textFixation($this->getCData($this->getElementByName('fulltext', $text)));
    $this->imagesArray = array();
    preg_match_all("/<img[^>]+\>/i", $this->story, $imgs);

    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = explode('alt="', $img);
      $image_caption = explode('"', $image_caption[1]);
      $image_caption = $image_caption[0];
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['image_caption'] = str_replace('%20', ' ', $image_caption);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);

      $img = htmlspecialchars($img);
      $img = str_replace("&quot;", '"', $img);
      $new_img = htmlspecialchars($new_img);
      $new_img = str_replace("&quot;", '"', $new_img);

      $text = str_replace($img, $new_img, $text);

      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

  public function getArticleOriginalId($params = array()) {
    $title = $this->getElementByName('title', $params['text']);
    $fulltext = $this->getElementByName('fulltext', $params['text']);
    $articleOriginalId = $title . "_" . $fulltext;
    $this->articleOriginalId = $articleOriginalId;
    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }
    return $this->title->getId() . "_" . sha1($articleOriginalId);
  }

}