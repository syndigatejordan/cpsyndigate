<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Timor-Leste News Agency (ANTIL)  [# publisher id = 1429]
//Title      : Timor-Leste News Agency (ANTIL) [ Portuguese ] 
//Created on : Oct 13, 2020, 10:18:06 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6661 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}