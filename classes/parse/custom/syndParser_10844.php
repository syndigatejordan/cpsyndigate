<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : East Money Information Co., Ltd.  [# publisher id = 1583]
//Title      : Eastmoney.com [ simplified Chinese ] 
//Created on : May 19, 2021, 1:46:40 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10844 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh-Hans'); 
	} 
}