<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : dpa GmbH  [# publisher id =530 ] 
// Titles    : dpa German Press Agency [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1570 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('nitf', $fileContents);
    return $articles;
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = $this->textFixation($this->getElementByName('body.content', $text));
    $this->story = str_replace('<pre>', '<p>', $this->story);
    $this->story = str_replace('</pre>', '</p>', $this->story);
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  public function getHeadline(&$text) {
    $headline = trim($this->getElementByName('hl1', $text));
    return $headline;
  }

  public function getArticleDate(&$text) {
    $head = trim($this->getElementByName('head', $text));
    $dateline = trim($this->getElementByName('docdata', $head));
    $dateInfo = syndParseHelper::getImgElements($dateline, 'date.issue', 'norm');
    $dateInfo = preg_replace('/\+(.*)/is', '', $dateInfo[0]);
    return date('Y-m-d', strtotime($dateInfo));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getElementByName('key-list', $text);
    $cats = trim(preg_replace('?\s+?', " ", $cats));
    $cats = explode('<keyword', $cats);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        if (!empty($cat)) {
          $cat = explode('key="', $cat);
          $cat = explode('"', $cat[1]);
          $cat = trim(preg_replace('?\s+?', " ", $cat[0]));
          if (!empty($cat))
            $originalCats[] = $cat;
        }
      }
    }
    return implode(', ', $originalCats);
  }

}
