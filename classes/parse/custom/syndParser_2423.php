<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Jeddah chamber of commerce and industry  [# publisher id =780 ]  
//Title     : Jeddah chamber of commerce and industry News [ arabic ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2423 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

}
