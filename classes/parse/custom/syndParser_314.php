<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The New Nation (via HT Media Ltd.)  [# publisher id = 108]
//Title      : The New Nation [ English ] 
//Created on : Jan 22, 2020, 11:27:41 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_314 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}