<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Lebanon Files  [# publisher id = 1285]
//Title      : Lebanon Files [ Arabic ] 
//Created on : Dec 31, 2019, 9:19:24 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5890 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }
}
