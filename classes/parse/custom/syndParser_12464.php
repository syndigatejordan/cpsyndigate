<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : MM ACTIV  [# publisher id = 1766]
//Title      : AgroSpectrum India [ English ] 
//Created on : Feb 20, 2022, 12:29:39 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12464 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}