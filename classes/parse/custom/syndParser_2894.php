<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dar Anwar Press, Publishing & Distribution  [# publisher id = 914]
//Title      : Le Quotidien [ French ] 
//Created on : Oct 17, 2016, 1:55:15 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2894 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}