<?php
/**
 * Publisher : Andy McTiernan  
 * Title     : Andy McTiernan Property & Economy Bulletin [ English ]
 *
 */
class syndParser_432 extends syndParseAbTxtSample {

	public function customInit() {
		parent::customInit();
		$this->defaultLang     = $this->model->getLanguageId('en');
		$this->charEncoding    = 'ISO-8859-1';		
	}
	
}

