<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Le Reporter Sarl  [# publisher id = 1843]
//Title      : Le Reporter [ English ] 
//Created on : Sep 01, 2022, 12:15:59 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12704 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}