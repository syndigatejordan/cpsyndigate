<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Eurabia Media Corporation FZ LLC  [# publisher id = 1000]
//Title      : Al Watan Al Arabi [ Arabic ] 
//Created on : Aug 18, 2016, 10:25:57 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2822 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}