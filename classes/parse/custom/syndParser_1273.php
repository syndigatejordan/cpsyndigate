<?php
//////////////////////////////////////////////////////////////////////////////
// Publisher: Radio France Internationale / AEF  [# publisher id =384 ]
// Titles   : RFI [French]
//////////////////////////////////////////////////////////////////////////////

class syndParser_1273 extends syndParseRss {

	private $titleInfoURL;
	private $titleInfoName;
	private $titleInfoLink;

	public function customInit() {
		parent::customInit();
		$this -> charEncoding = 'UTF-8';
		$this -> defaultLang = $this -> model -> getLanguageId('fr');
	}

	public function getRawArticles(&$fileContents) {
		$this -> addLog("getting articles info Logo & URL to add them to body in RawArticles function");

		$titleInfo = $this -> getElementByName('image', $fileContents);
		$this -> titleInfoURL = trim($this -> textFixation($this -> getElementByName('url', $titleInfo)));
		$this -> titleInfoName = trim($this -> textFixation($this -> getElementByName('title', $titleInfo)));
		$this -> titleInfoLink = trim($this -> textFixation($this -> getElementByName('link', $titleInfo)));

		return parent::getRawArticles($fileContents);
	}

	public function getStory(&$text) {
		$body = parent::getStory($text);

		$this -> addLog('Adding Logo And links to the end of article body');

		$logo = "<img src='{$this->titleInfoURL}' />";
		$source = "<b>More videos available on <a href='$this->titleInfoLink' target='_blank'>$this->titleInfoLink</a></b>";
		$story = $body . ' <br /><p>' . $logo . '  ' . $source . '</p>';
		return $story;
	}

}