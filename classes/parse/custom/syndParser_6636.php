<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Malaysia National News Agency (BERNAMA)  [# publisher id = 1421]
//Title      : Malaysia National News Agency (BERNAMA) [ Tamil ] 
//Created on : Oct 26, 2020, 7:26:52 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6636 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ta'); 
	} 
}