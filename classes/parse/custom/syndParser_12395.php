<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : iciLome  [# publisher id = 1761]
//Title      : iciLome [ French ] 
//Created on : Feb 13, 2022, 12:40:35 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12395 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}