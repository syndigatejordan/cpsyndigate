<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Schafer Media CC  [# publisher id = 1581]
//Title      : Franschhoek Tatler [ English ] 
//Created on : Aug 04, 2021, 7:52:10 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10840 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}