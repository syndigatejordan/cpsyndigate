<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : ArmInfo News Agency  [# publisher id =358 ]
// Titles    : Enterprise Innovation [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1472 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = $this->textFixation($this->getCData($this->getElementByName('description', $text)));
    $this->story = strip_tags($this->story, '<p><br><strong><b><u><i><img>');

    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");

    return trim($this->textFixation($this->getElementByName('dc:creator', $text)));
  }

  protected function getImages(&$text) {
    $story = trim($this->textFixation($this->getCData($this->getElementByName('description', $text))));

    $imagesArray = array();
    preg_match_all("/<img[^>]+\>/i", $story, $imgs);

    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");

      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      $old_imagePath = $imagePath;
      $imagePath = 'http://enterpriseinnovation.net' . $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      $imagePath = explode('?', $imagePath);
      $imagePath = $imagePath[0];
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($old_imagePath, $images[0]['img_name'], $img);
      $text = str_replace($img, $new_img, html_entity_decode($text));
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
