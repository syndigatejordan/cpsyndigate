<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Shehab News Agency  [# publisher id = 1206]
//Title      : Shehab News Agency [ English ] 
//Created on : Feb 14, 2019, 7:55:10 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5356 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}