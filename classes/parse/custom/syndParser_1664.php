<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Israel National News  [# publisher id = 583]
//Title      : Arutz Sheva [ English ] 
//Created on : Sep 23, 2021, 7:49:04 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1664 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}