<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Watani Printing and Publishing Group  [# publisher id = 333]
//Title      : Watani [ French ] 
//Created on : Jul 28, 2021, 12:45:44 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1085 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}