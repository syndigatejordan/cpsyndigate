<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : New Media Publishing  [# publisher id = 496]
//Title      : South African Food Review [ English ] 
//Created on : Aug 06, 2019, 6:30:31 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1491 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	}
}
