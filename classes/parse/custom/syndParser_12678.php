<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Boston Globe Media Partners, LLC  [# publisher id = 1817]
//Title      : The Boston Globe [ English ] 
//Created on : Aug 01, 2022, 1:10:47 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12678 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}