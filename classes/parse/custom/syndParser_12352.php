<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BSE India  [# publisher id = 1746]
//Title      : Corporate Actions [ English ] 
//Created on : Jan 25, 2022, 3:44:50 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12352 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}