<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : A Music Company Ltd.  [# publisher id = 1483]
//Title      : UAS VISION [ English ] 
//Created on : Dec 08, 2020, 7:10:11 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7397 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	}
    protected function getStory(&$text) {
        $this->addLog("getting article body");
        $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) srcset=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) sizes=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) width=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) height=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) link_thumbnail=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('!\s+!', ' ', $this->story);
        if (empty($this->story))
            return $this->textFixation($this->getElementByName('fulltext', $text));
        else {
            return $this->story;
        }
    }
}