<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Nigeria German Business Association  [# publisher id = 826]
//Title      : Nigerian German Business Association (NGBA) - News [ English ] 
//Created on : Aug 19, 2018, 6:42:50 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2548 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}