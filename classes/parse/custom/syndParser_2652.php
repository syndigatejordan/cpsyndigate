<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Pride Magazine Nigeria  [# publisher id = 889]
//Title      : Pride Magazine Nigeria [ English ] 
//Created on : Feb 22, 2018, 6:30:19 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2652 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
