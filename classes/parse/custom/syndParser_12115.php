<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : One Green Planet LLC  [# publisher id = 1612]
//Title      : OneGreenPlanet.Org [ English ] 
//Created on : Aug 09, 2021, 9:07:41 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12115 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}