<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : EMDP - Egypt Media Development Program  [# publisher id = 1164]
//Title      : Zahma [ Arabic ] 
//Created on : Jun 09, 2021, 1:47:44 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3377 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}