<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : London Fields Publishing Limited  [# publisher id = 1775]
//Title      : Pioneers Post [ English ] 
//Created on : Feb 28, 2022, 12:47:18 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12497 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}