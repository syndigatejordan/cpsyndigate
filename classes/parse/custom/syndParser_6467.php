<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Imaz Press Réunion (IMAZ PRESS)  [# publisher id = 1372]
//Title      : Imaz Press Réunion (IMAZ PRESS) [ French ] 
//Created on : Sep 23, 2021, 8:05:14 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6467 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}