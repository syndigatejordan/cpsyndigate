<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : iciLome  [# publisher id = 1761]
//Title      : iciLome [ French ] 
//Created on : Feb 13, 2022, 12:35:03 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12394 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}