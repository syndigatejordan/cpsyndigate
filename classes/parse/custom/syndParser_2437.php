<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Abu Dhabi Chamber of Commerce and Industry  [# publisher id = 789]
//Title      : Abu Dhabi Chamber News [ English ] 
//Created on : Feb 02, 2016, 12:07:42 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2437 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}