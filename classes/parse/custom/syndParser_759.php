<?php

//////////////////////////////////////////////////////////////////////////////
// Publisher: Peter John Cooper
// Titles   : ArabianMoney.net [ English ]
//////////////////////////////////////////////////////////////////////////////

class syndParser_759 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    return $this->story;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getCData($this->getElementByName('dc:creator', $text)));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getElementsByName('category', $text);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = $this->textFixation($this->getCData($cat));
      }
    }
    return implode(', ', $originalCats);
  }

  protected function getImages(&$text) {
    $this->story = trim($this->textFixation($this->getCData($this->getElementByName('content:encoded', $text))));
    $imagesArray = array();
    preg_match_all("/<img[^>]+\>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $imageInfo = syndParseHelper::getImgElements($img, 'img', 'src');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($image = $this->checkImageifCached($imagePath)) {
        if (!empty($image[2]))
          $this->story = str_replace($imageInfo[0], $image[2], $this->story);
        // Image already parsed..
        continue;
      }
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
      $this->story = str_replace($imageInfo[0], $images[0]['img_name'], $this->story);
    }
    return $imagesArray;
  }

}
