<?php

//////////////////////////////////////////////////////////////////////////////
// Publisher: CPI Industry  [# publisher id =318 ] 
// Titles   : Power & Water Middle East [ English ]
//////////////////////////////////////////////////////////////////////////////

class syndParser_1057 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getRawArticles(&$fileContents) {
    $this->addLog('getting row articles');
    $rowArticles = parent::getRawArticles($fileContents);
    return $rowArticles;
  }

  protected function getAbstract(&$text) {
    $this->addLog('getting article summary');
    $summary = $this->textFixation($this->getCData($this->getElementByName('description', $text)));
    return $summary;
  }

  public function getStory(&$text) {
    $this->addLog('getting article story');
    $story = $this->textFixation($this->getCData($this->getElementByName('content:encoded', $text)));
    $story = str_replace("style=\"text-align: justify;\"", '', $story);
    $story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $story);
    return $story;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");

    return trim($this->textFixation($this->getElementByName('dc:creator', $text)));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getElementsByName('category', $text);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = $this->textFixation($this->getCData($cat));
      }
    }
    return implode(', ', $originalCats);
  }

  protected function getImages(&$text) {
    $body = $this->textFixation($this->getCData($this->getElementByName('content:encoded', $text)));
    $images = $this->getElementsByName('a', $body);

    $imagesArray = array();
    foreach ($images as $img) {
      $this->addLog("getting article images");

      $imagePath = syndParseHelper::getImgElements($img, 'img');

      if (is_array($imagePath) && $imagePath) {
        $imagePath = $imagePath[0];
      }

      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        continue;
      }

      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      array_push($imagesArray, $images[0]);

      $text = str_replace($img, '', $text);
    }

    return $imagesArray;
  }

}