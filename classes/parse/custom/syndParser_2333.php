<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Almesryoon Publishing  [# publisher id = 744]
//Title      : Almesryoon Newspaper [ Arabic ] 
//Created on : Nov 25, 2020, 10:55:23 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2333 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}