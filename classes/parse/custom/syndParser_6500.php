<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agencia Mexicana de Noticias (AMN)  [# publisher id = 1386]
//Title      : Agencia Mexicana de Noticias (AMN) [ Spanish ] 
//Created on : Oct 20, 2020, 7:54:40 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6500 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}