<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Angola Press News Agency (ANGOP)  [# publisher id = 1613]
//Title      : Angola Press News Agency (ANGOP) [ Spanish ] 
//Created on : Aug 10, 2021, 9:33:27 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12119 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}