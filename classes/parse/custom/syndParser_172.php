<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher :   Al-Arab Weekly Newspaper
//Title     : Al-Arab [ English ]
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_172 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getStory(&$text) {
      $this->addLog('Getting article story');

      $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      $this->story = preg_replace('/<td id="photocap">(.*?)<\/td>/si', '', $this->story);
      $this->story = preg_replace('!\s+!', ' ', $this->story);

      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      if (empty($this->story)) {
          return '';
      }
      return $this->story;
  }

}
