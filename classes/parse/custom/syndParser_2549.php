<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Nigeria South Africa Chamber of Commerce  [# publisher id = 827]
//Title      : Nigeria South Africa Chamber of Commerce (NSACC) - News [ English ] 
//Created on : Feb 02, 2016, 12:11:30 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2549 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}