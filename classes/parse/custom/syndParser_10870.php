<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Chijos Media  [# publisher id = 1587]
//Title      : Herald Nigeria [ English ] 
//Created on : Jun 03, 2021, 10:02:35 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10870 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}