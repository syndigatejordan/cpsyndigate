<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : China Economic Net  [# publisher id = 1797]
//Title      : Economic Daily News [ Arabic ] 
//Created on : May 10, 2022, 11:18:38 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12614 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}