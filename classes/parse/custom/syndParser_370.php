<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Perform Media Channels Ltd.  [# publisher id = 135]
//Title      : Goal.com [ English ] 
//Created on : Jul 23, 2017, 9:47:15 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_370 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    //$this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
