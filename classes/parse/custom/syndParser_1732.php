<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Euronews SA  [# publisher id = 604]
//Title      : euronews [ Arabic ] 
//Created on : May 21, 2019, 11:14:57 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1732 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}