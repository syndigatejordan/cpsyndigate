<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Hoqook News Network  [# publisher id =540 ] 
// Titles    : Hoqook News Network [Arabic]
///////////////////////////////////////////////////////////////////////////

class syndParser_1599 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = $this->getElementByName('description', $text);
    $this->story = strip_tags($this->story, '<p><br><strong><b><u><i><img>');
    $this->story = str_replace('�', '', $this->story);
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  public function getHeadline(&$text) {
    $headline = $this->getElementByName('title', $text);
    return $headline;
  }

  public function getAuthor(&$text) {
    $byline = $this->getCData($this->getElementByName('dc:creator', $text));
    return $byline;
  }

  public function getOriginalCategory(&$text) {
    $cat = $this->getCData($this->getElementByName('category', $text));
    return $cat;
  }

  public function getArticleDate(&$text) {
    $date = $this->getElementByName('pubDate', $text);
    $date = date('Y-m-d', strtotime($date));
    return $date;
  }

  protected function getImages(&$text) {
    $text = html_entity_decode($text);
    $story = $this->getElementByName('description', $text);
    $imagesArray = array();
    preg_match_all("/<img[^>]+\>/i", $story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");

      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath_orginal = $imageInfo[0];
      $imagePath = explode('?', $imagePath_orginal);
      $imagePath = $imagePath[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath_orginal, $images[0]['img_name'], $img);
      $text = str_replace($img, $new_img, $text);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}