<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Arab Press Agency (APA)  [# publisher id = 405]
//Title      : Sawt Al Balad [ Arabic ] 
//Created on : Aug 09, 2021, 1:00:38 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1336 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}