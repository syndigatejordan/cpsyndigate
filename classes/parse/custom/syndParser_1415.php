<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Services in Scientific Work in Africa (SSA)  [# publisher id = 454]
//Title      : The Africa Science News Service (ASNS) [ English ] 
//Created on : Sep 19, 2021, 7:01:45 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1415 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}