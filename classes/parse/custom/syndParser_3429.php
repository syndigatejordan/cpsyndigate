<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : TA telecom  [# publisher id = 1170]
//Title      : Digital Boom [ English ] 
//Created on : Dec 12, 2019, 1:26:59 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3429 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
      $this->addLog("getting article body");
      $this->story = preg_replace('!\s+!', ' ', $this->story);
      $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) srcset=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) sizes=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) width=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) height=".*?"/i', '$1', $this->story);
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);

      if (empty($this->story))
          return $this->textFixation($this->getElementByName('fulltext', $text));
      else {
          return $this->story;
      }
  }

}
