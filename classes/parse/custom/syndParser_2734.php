<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : L'Express de Madagascar S.A  [# publisher id = 938]
//Title      : L' Express de Madagascar [ French ] 
//Created on : May 19, 2016, 10:28:20 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2734 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('fr');
  }
}
