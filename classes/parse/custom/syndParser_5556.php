<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ech-Chorouk Infed SARL  [# publisher id = 1135]
//Title      : Ech-Chorouk El Yaoumi [ Arabic ] 
//Created on : Jul 06, 2021, 9:40:56 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5556 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}