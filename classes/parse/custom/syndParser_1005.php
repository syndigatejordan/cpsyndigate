<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Universal Solutions S.A.E.  [# publisher id = 224]
//Title      : Middle East Business News [ English ] 
//Created on : Aug 30, 2021, 7:06:13 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1005 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}