<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Stock Snips, Inc.  [# publisher id = 1694]
//Title      : StockSnips - ESG Sentiment (Metrics) [ English ] 
//Created on : Jan 06, 2022, 10:33:07 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12292 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}