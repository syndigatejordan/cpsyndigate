<?php
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Ayam Publishing Group  
//Title     : Daily Tribune [ English ] 
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_21 extends syndParseCms {
  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}