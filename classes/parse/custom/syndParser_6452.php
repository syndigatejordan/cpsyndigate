<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence Mauritanienne d'Information (AMI)  [# publisher id = 431]
//Title      : Agence Mauritanienne d'Information (AMI) [ English ] 
//Created on : Oct 13, 2020, 11:53:35 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6452 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}