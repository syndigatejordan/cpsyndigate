<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : La Voce d'Italia CA  [# publisher id = 1521]
//Title      : La Voce d'Italia [ Italian ] 
//Created on : Sep 19, 2021, 10:22:20 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_9505 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('it'); 
	} 
}