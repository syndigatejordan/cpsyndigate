<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Caxton Ltd  [# publisher id = 1148]
//Title      : Rekord Moot [ English ] 
//Created on : Aug 30, 2017, 10:55:15 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3143 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}