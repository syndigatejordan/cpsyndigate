<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BBC World Service  [# publisher id = 1192]
//Title      : BBC Turkish - News [ Turkish ] 
//Created on : Mar 14, 2021, 9:39:14 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10338 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('tr'); 
	} 
}