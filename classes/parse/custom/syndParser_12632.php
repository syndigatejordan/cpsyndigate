<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : RIM INTELLIGENCE Co  [# publisher id = 1807]
//Title      : RIM Intelligence - Market News [ English ] 
//Created on : Jun 02, 2022, 1:49:58 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12632 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}