<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Saudi Arabian Monetary Agency (SAMA)  [# publisher id = 177]
//Title      : Saudi Arabian Monetary Agency (SAMA) [ Arabic ] 
//Created on : Oct 16, 2016, 7:46:05 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2780 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}