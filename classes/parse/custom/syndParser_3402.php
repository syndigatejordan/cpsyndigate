<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Moçambique Media Online (MMO)  [# publisher id = 1166]
//Title      : Moçambique Media Online (MMO) [ Portuguese ] 
//Created on : Sep 19, 2021, 8:51:00 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3402 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}