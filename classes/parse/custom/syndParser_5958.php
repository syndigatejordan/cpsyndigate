<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Nature Picture Library Limited  [# publisher id =1321 ] 
//Title      : Nature Picture Library - Images (Rights Managed) [ English ] 
//Created on : Sep 24, 2020, 8:55:58 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5958 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

   public function getStory(&$text) {

    $this->addLog("getting article body");
    $copyright = $this->textFixation($this->getCData($this->getElementByName('copyright', $text)));
    $body = "Copyright: $copyright";
    return $body;
  }

  public function getArticleOriginalId($params = array()) {
    $id = $this->getElementByName('id', $params['text']);
    $guid = $this->getElementByName('guid', $params['text']);
    $articleOriginalId = "{$id}_{$guid}";
     $this->articleOriginalId = $articleOriginalId;
    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }

    return $this->title->getId() . '_' . sha1($articleOriginalId);
  }

  public function getHeadline(&$text) {
    $this->headline = trim($this->textFixation($this->getCData($this->getElementByName('caption', $text))));
    return $this->headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->textFixation($this->getCData($this->getElementByName('datetimetext', $text)));
    $date = trim(str_replace('/\+(.*)/is', '', $date));
    return date('Y-m-d', strtotime($date));
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getCData($this->getElementByName('copyright', $text)));
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $image_caption = $this->textFixation($this->getCData($this->getElementByName('caption', $text)));
    $imagePath = $this->textFixation($this->getCData($this->getElementByName('path', $text)));
    if ($imagePath) {
      $this->addLog("getting article images");
      if (!$this->checkImageifCached($imagePath)) {
        $imagePath = str_replace(' ', '%20', $imagePath);
        $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
        if ($copiedImage) {
          $images = $this->getAndCopyImagesFromArray(array($copiedImage));
          if (!empty($image_caption)) {
            $images[0]['image_caption'] = $image_caption;
          }
          $name_image = explode('/images/', $copiedImage);
          if ($images[0]['image_caption'] == $name_image[1]) {
            $images[0]['image_caption'] = '';
          }
          $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
          $images[0]['is_headline'] = TRUE;
          array_push($imagesArray, $images[0]);
        }
      }
    }
    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl, $extension = "jpeg") {
    sleep(1);
    $baseName = sha1($imageUrl) . ".$extension";
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $imageUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }


  //Handle Empty body
  public function parse() {
    $articles = array();
    foreach ($this->files as $file) {
      if ($this->extensionFilter) {
        if (!syndParseHelper::isFileExtension($file, $this->extensionFilter)) {
          $this->addLog("Wrong extension for file : $file)");
          continue;
        }
      }
      if (!file_exists($file)) {
        $this->addLog("file dose not exist: $file)");
        continue;
      }

      $this->addLog("get file contents (file:$file)");
      $fileContents = $this->getFileContents($file);

      if (!$fileContents) {
        continue;
      }

      $this->currentlyParsedFile = $file;
      $this->loadCurrentDirectory();
      $this->loadUpperDir();
      $rawArticles = $this->getRawArticles($fileContents);
      foreach ($rawArticles as $rawArticle) {
        $article = $this->getArticle($rawArticle);
        $articles[] = $article;
      }
    }
    return $articles;
  }
}
