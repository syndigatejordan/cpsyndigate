<?php

///////////////////////////////////////////////////////////////////////////
// Publisher :   Fadi Abu Sada 
// Titles    :   Fadi Abu Sa'da [Arabic]
///////////////////////////////////////////////////////////////////////////


class syndParser_925 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

}
