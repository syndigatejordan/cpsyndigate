<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Cyber Media (India) Ltd  [# publisher id = 570]
//Title      : Voice&Data [ English ] 
//Created on : Oct 13, 2021, 1:58:49 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1649 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}