<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Saudi Society News Agency  [# publisher id = 1029]
//Title      : WAMS [ Arabic ] 
//Created on : Aug 22, 2016, 8:44:23 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2860 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}