<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Adweek, LLC  [# publisher id = 1353]
//Title      : Adweek [ English ] 
//Created on : Sep 27, 2020, 7:34:02 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6403 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}