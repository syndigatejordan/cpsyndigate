<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The New York Times Company / nytLicensing  [# publisher id = 1197]
//Title      : Harvard Business Review [ English ] 
//Created on : Apr 01, 2019, 10:12:34 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5505 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}