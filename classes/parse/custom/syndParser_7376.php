<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Mideas Limited  [# publisher id = 1480]
//Title      : News Ghana [ English ] 
//Created on : Sep 19, 2021, 10:20:04 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7376 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}