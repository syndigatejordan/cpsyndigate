<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Talal Abu-Ghazaleh Organization  [# publisher id = 681]
//Title      : TAG-Org News [ Arabic ] 
//Created on : Sep 14, 2021, 7:41:41 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2147 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}