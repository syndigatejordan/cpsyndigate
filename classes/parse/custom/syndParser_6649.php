<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Philippine News Agency  [# publisher id = 1428]
//Title      : Philippine News Agency [ English ] 
//Created on : Oct 13, 2020, 10:26:20 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6649 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}