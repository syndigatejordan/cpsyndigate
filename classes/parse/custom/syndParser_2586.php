<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Asianet-Pakistan (Pvt) Ltd.  [# publisher id = 83]
//Title      : Le Renouveau [ French ] 
//Created on : Apr 07, 2019, 7:13:55 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
require_once '/usr/local/syndigate/classes/parse/custom/syndParser_1854.php';

class syndParser_2586 extends syndParser_1854 {
  
    public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('fr');
    $this->charEncoding = 'UTF-8';
    //$this->extensionFilter = 'xml';
  }
}