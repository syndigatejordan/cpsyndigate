<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ANDINA - Agencia Peruana de Noticias  [# publisher id = 1388]
//Title      : ANDINA - Peru News Agency [ Spanish ] 
//Created on : Oct 15, 2020, 11:48:39 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6506 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}