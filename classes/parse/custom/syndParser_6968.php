<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : KAMA VISION SARL  [# publisher id = 1449]
//Title      : 237online.com [ French ] 
//Created on : Aug 15, 2021, 11:59:27 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6968 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}