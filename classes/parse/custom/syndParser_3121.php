<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ech-Chorouk Infed SARL  [# publisher id = 1135]
//Title      : Ech-Chorouk El Yaoumi [ French ] 
//Created on : Aug 15, 2021, 9:13:45 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3121 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}