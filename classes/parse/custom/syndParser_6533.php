<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence Kampuchea Presse (AKP)  [# publisher id = 1402]
//Title      : Agence Kampuchea Presse (AKP) [ English ] 
//Created on : Oct 08, 2020, 11:59:15 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6533 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}