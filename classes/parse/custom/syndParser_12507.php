<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Fadaat Media Ltd  [# publisher id = 904]
//Title      : Diffah Taltah [ Arabic ] 
//Created on : Mar 06, 2022, 9:12:46 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12507 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}