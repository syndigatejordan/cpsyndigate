<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Nova Editora, SARL  [# publisher id = 907]
//Title      : A Semana [ Portuguese ] 
//Created on : Aug 15, 2021, 9:03:17 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2700 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}