<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Republic of Iraq Ministry of Foreign Affairs  [# publisher id = 839]
//Title      : Republic of Iraq Ministry of Foreign Affairs - Missions News [ Arabic ] 
//Created on : Feb 02, 2016, 12:14:30 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2592 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}