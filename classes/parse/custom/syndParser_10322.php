<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : iCrowdNewswire, LLC  [# publisher id = 1535]
//Title      : El Comercio [ Spanish ] 
//Created on : Mar 08, 2021, 9:09:26 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10322 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}