<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ​The Egyptian Center for Economic Studies (ECES)  [# publisher id = 1203]
//Title      : ECES Books [ English ] 
//Created on : Jan 29, 2019, 9:13:37 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5306 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}