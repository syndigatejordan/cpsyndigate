<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Palmyra Media Limited   [# publisher id = 711]
//Title      : Arageek.com [ Arabic ] 
//Created on : May 08, 2017, 1:13:04 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2251 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}