<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Gulf Sports Publishing  [# publisher id = 229]
//Title      : Sport360°- Slideshow [ Arabic ] 
//Created on : Aug 02, 2017, 7:28:06 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3107 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('/<div class="wp-caption alignnone">(.*?)<\/div>/s', '', $this->story);
    $this->story = preg_replace('/<p class="wp-caption-text onlyDesktops">(.*?)<\/p>/i', '', $this->story);
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

  protected function getImages(&$text) {
    $this->addLog("getting images");
    $this->imagesArray = array();
    $SlideshowImages = null;
    $this->story = $this->textFixation($this->getCData($this->getElementByName('fulltext', $text)));
    $id = $this->textFixation($this->getCData($this->getElementByName('id', $text)));
    preg_match_all('/<div class="wp-caption alignnone">(.*?)<\/div>/s', $this->story, $SlideshowImages);
    $SlideshowImages = $SlideshowImages[0];
    $this->getSlideshowImages($SlideshowImages, $id);
    preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = explode('alt="', $img);
      $image_caption = explode('"', $image_caption[1]);
      $image_caption = $image_caption[0];
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = TRUE;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

  public function getSlideshowImages($SlideshowImages, $id) {
    foreach ($SlideshowImages as $matche) {
      //$matche = preg_replace('!\s+!', ' ', $matche);
      $this->addLog("getting Slideshow images");
      ///////////////////Get image original//////////////////////
      $imageInfo = syndParseHelper::getImgElements($matche, 'img');
      $imagePath = $imageInfo[0];

      preg_match('/<p class="wp-caption-text onlyDesktops">(.*?)<\/p>/i', $matche, $image_caption);
      $image_caption = trim($this->getCData($image_caption[1]));
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath, $id);
      if (!$copiedImage) {
        $this->addLog("no path");
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['is_headline'] = false;
      array_push($this->imagesArray, $images[0]);
      $this->addLog("getting Slideshow thumb images");
      ///////////////////Get thumb image //////////////////////
      $copiedImage = $this->copyUrlImgSlideShowIfNotCached($imagePath, $id);
      if (!$copiedImage) {
        $this->addLog("no path");
        continue;
      }

      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }

      $images[0]['is_headline'] = false;
      array_push($this->imagesArray, $images[0]);
      $this->story = str_replace($matche, "", $this->story);
    }
  }

  public function copyUrlImgIfNotCached($imageUrl, $id) {
    $baseName = basename($imageUrl);
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $curl_handle = curl_init();
      curl_setopt($curl_handle, CURLOPT_URL, $imageUrl);
      curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl_handle, CURLOPT_USERAGENT, 'php');
      $imgContent = curl_exec($curl_handle);
      curl_close($curl_handle);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

  public function copyUrlImgSlideShowIfNotCached($imageUrl, $id) {
    if (!empty($imageUrl)) {
      $baseName = 'thumb_' . basename($imageUrl);
      $copiedImage = $this->imgCacheDir . $baseName;

      if (!is_dir($this->imgCacheDir)) {
        mkdir($this->imgCacheDir, 0755, true);
      }

      if (!file_exists($copiedImage)) {
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $imageUrl);
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_USERAGENT, 'php');
        $imgContent = curl_exec($curl_handle);
        curl_close($curl_handle);
        $myfile = fopen($copiedImage, "w");
        fwrite($myfile, $imgContent);
        fclose($myfile);
        if (!empty($imgContent)) {
          return $copiedImage;
        } else {
          return false;
        }
      } else {
        return $copiedImage;
      }
    }
  }

}
