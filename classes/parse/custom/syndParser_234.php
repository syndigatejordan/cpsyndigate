<?php

	////////////////////////////////////////////////////////////////////////
	// Publisher : All Data Processing Ltd.
	// Titles    : ADP News Middle East & Africa
	////////////////////////////////////////////////////////////////////////

	class syndParser_234 extends syndParseContent
	{

		public function customInit()
		{
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('en');
			$this->extensionFilter = 'txt';
			$this->charEncoding = 'ISO-8859-1';
		}

		// the file contains just one article
		protected function getRawArticles(&$fileContents)
		{
			$this->addLog("getting raw articles text");
			
			// fixing the text format [some times just \r available]
			$fileContents = str_replace("\r", "", $fileContents);
			$fileContents = str_replace("\n", "\r\n", $fileContents);
			
			$conf = array();
			$conf['[HH]'] = 'headline';
			$conf['[DD]'] = 'date';
			$conf['[QQ]'] = 'body';  		 // new paragraph
			$conf['[PL]'] = 'place';         // will not be used 
			$conf['[SO]'] = 'source'; 		 // will not be used because some will be removed later, some are for the title internal use
			$conf['[TC]'] = 'topic code';    // will not be used because some will be removed later, some are for the title internal use   
			$conf['[IC]'] = 'industry code'; // will not be used because some will be removed later, some are for the title internal use  
			$conf['[CC]'] = 'company code';  // will not be used because some will be removed later, some are for the title internal use
			
			$this->customArticle = array();
			$articlToArray = explode("\r\n", $fileContents);
						
			foreach ($articlToArray as $line) {
				
				$tag 		= substr($line, 0, 4);
				$lineValue 	= substr($line, 4);
				
				if(in_array($tag, array_keys($conf))) {
					
					if($conf[$tag] == 'body') {
						$this->customArticle['body'] = $this->customArticle['body'] . "<p>" . $lineValue . "</p>" ;
					} else {
						$this->customArticle[$conf[$tag]] = $lineValue;
					}
				} else {
					// just ignor
				}
			}
			
			return array(1);
		}

		protected function getStory(&$text)
		{			
			$this->addLog("getting article text");
			return $this->textFixation($this->customArticle['body']);
		}

		protected function getHeadline(&$text)
		{
			$this->addLog("getting article headline");
			return $this->textFixation($this->customArticle['headline']);
		}

		protected function getArticleDate(&$text)
		{
			$this->addLog("getting articles date");
			
			$date = $this->customArticle['date']; // ex. 01:10:08
			$date = explode(':', $date);
			$date = '20' . $date[2] . '-' . $date[1] . '-' . $date[0]; 
			return $this->dateFormater($date); 
		}
	}
?>