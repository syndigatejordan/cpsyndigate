<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Founder Apabi Technology Limited  [# publisher id = 1538]
//Title      : China Securities Journal [ simplified Chinese ] 
//Created on : Mar 14, 2021, 2:00:01 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10340 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh-Hans'); 
	} 
}