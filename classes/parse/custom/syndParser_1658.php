<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : 7awi Digital Publishing and Marketing  [# publisher id = 579]
//Title      : Layalina [ Arabic ] 
//Created on : May 21, 2019, 9:27:50 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1658 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}