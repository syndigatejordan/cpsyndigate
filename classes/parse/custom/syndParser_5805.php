<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Maghreb Arabe Presse (MAP)  [# publisher id = 101]
//Title      : Agence Marocaine De Presse (MAP) [ English ] 
//Created on : Oct 02, 2019, 11:24:35 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5805 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}