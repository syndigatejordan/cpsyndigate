<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Asia News Network (ANN)  [# publisher id = 659]
//Title      : Asia News Network (ANN) [ English ] 
//Created on : Apr 02, 2019, 6:59:17 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2075 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
