<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : WorldPRNetwork.com  [# publisher id = 1390]
//Title      : DubaiPRNetwork.com [ English ] 
//Created on : Sep 23, 2020, 6:46:24 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_6549 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
