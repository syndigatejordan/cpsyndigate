<?php

/////////////////////////////////////////////////////////////////////////////////////////
// Title     : ABN Newswire [English]
// Publisher : Asia Business News Pty Ltd (ABN Newswire) 
/////////////////////////////////////////////////////////////////////////////////////////

class syndParser_353 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

}
