<?php 
//////////////////////////////////////////////////////////////////////////////
// Publisher:  I Media LLC 
// Titles   :  Alrroya.com[ English ]
//////////////////////////////////////////////////////////////////////////////
class syndParser_734 extends syndParseRss {
	
	public function getArticleDate(&$text) {
		$date = $this->textFixation(trim($this->getElementByName('publicationdate', $text)));
		return $this->dateFormater(substr($date, 0, 10));
	}
	
	public function afterParse($params = array()) {
		
		if(isset($params['articles'])) {
			foreach ($params['articles'] as $index => $article) {
				if( strtotime($article['date']) < strtotime("-10 days") ) {
					$this->addLog("Unset article ... too old date :" . $article['date'] . ", headline :" . $article['headline']);
					unset($params['articles'][$index]);
				}
			}
		}
		
		$this->addLog("Number of articles after excluding old articles is :" . count($params['articles']));
		parent::afterParse($params);
	}
}
