<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Neotrope  [# publisher id = 841]
//Title      : California Newswire [ English ] 
//Created on : Sep 19, 2021, 10:18:32 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7350 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}