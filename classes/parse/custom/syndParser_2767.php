<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Leaders & Company Limited  [# publisher id = 966]
//Title      : This Day [ English ] 
//Created on : May 22, 2016, 6:49:45 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2767 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}