<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Yonhap News Agency  [# publisher id = 1053]
//Title      : Yonhap News Agency [ Chinese ] 
//Created on : Dec 28, 2016, 2:26:35 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2924 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh'); 
	} 
}