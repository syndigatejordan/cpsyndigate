<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sarl Interface rp  [# publisher id = 509]
//Title      : Radio-M.net [ French ] 
//Created on : Jul 18, 2022, 12:35:40 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12671 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}