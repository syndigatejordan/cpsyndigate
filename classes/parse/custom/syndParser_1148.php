<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Nation Media Group Limited.  [# publisher id = 339]
//Title      : Daily Monitor [ English ] 
//Created on : Aug 09, 2021, 11:46:49 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1148 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}