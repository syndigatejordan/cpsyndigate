<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Bawaba Middle East Limited  [# publisher id = 62]
//Title      : Projects Monitor Africa-Asia [ English ] 
//Created on : Aug 18, 2021, 11:44:54 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2102 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}