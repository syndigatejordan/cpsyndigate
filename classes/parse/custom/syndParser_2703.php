<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Ayyam Printing & Publishing  [# publisher id = 910]
//Title      : Al Ayyam [ Arabic ] 
//Created on : May 16, 2016, 10:48:53 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2703 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}