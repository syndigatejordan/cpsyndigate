<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Neotrope  [# publisher id = 841]
//Title      : Send2Press [ English ] 
//Created on : Sep 19, 2021, 10:18:24 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7349 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}