<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : News Corp  [# publisher id = 1221]
//Title      : The Wall Street Journal [ English ] 
//Created on : Apr 07, 2019, 12:12:07 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5515 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}