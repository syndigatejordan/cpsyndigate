<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Ethiopian News Agency - ENA  
//Title     : Ethiopian News Agency - ENA [ English ] 
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_1387 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

}
