<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Human Rights Watch  [# publisher id = 1302]
//Title      : Human Rights Watch - News [ Hindi ] 
//Created on : Feb 29, 2020, 1:59:10 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5939 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('hi');
  }
}
