<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Castle Hall Diligence  [# publisher id = 1772]
//Title      : Castle Hall ESG Newsletter [ English ] 
//Created on : Feb 28, 2022, 10:57:03 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12487 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}