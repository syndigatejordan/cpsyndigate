<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CPI Construction  [# publisher id =307 ]    
//Title      : Construction Machinery Middle East [ Arabic ] 
//Created on : Jul 20, 2016, 15:30:16 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1054 extends syndParseCms {
  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
