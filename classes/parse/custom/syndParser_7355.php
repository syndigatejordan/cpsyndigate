<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence Digitale d'Informations Africaines (ADIA)  [# publisher id = 1466]
//Title      : Les Scoops D'Afrique [ French ] 
//Created on : Sep 19, 2021, 10:19:16 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7355 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}