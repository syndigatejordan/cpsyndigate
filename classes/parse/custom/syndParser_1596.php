<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Fuseworks Limited  [# publisher id =517 ]
// Titles    : Fuseworks [Chinese]
///////////////////////////////////////////////////////////////////////////

class syndParser_1596 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('zh-Hant');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('News', $fileContents);
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $text = mb_convert_encoding($text, "UTF-8", "BIG5");
    $headline = $this->getCData($this->getElementByName('Headline', $text));
    return $headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");

    $date = $this->getElementByName('DateTime', $text);
    $date = date('Y-m-d', strtotime($date));
    return $date;
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = $this->textFixation($this->getCData($this->getElementByName('Content', $text)));
    $this->story = str_replace('<Para>', '<p>', $this->story);
    $this->story = str_replace('</Para>', '</p>', $this->story);
    $this->story = strip_tags($this->story, '<p><br><strong><b><u><i>');
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    preg_match_all('/<Category(.*?)>/is', $text, $cats);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats[0] as $cat) {
      $zh=syndParseHelper::getImgElements($cat, 'Category', 'zh');
        $originalCats[] = $zh[0];
      }
    }
    return implode(', ', $originalCats);
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getCData($this->getElementByName('AuthorName', $text)));
  }

  protected function getAbstract(&$text) {
    $this->addLog('getting article summary');
    $summary = $this->textFixation($this->getCData($this->getElementByName('description', $text)));
    return $summary;
  }

}
