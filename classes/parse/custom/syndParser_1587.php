<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Singapore Press Holdings Ltd.  [# publisher id =538 ]
// Titles    : The Straits Times [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1587 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'windows-1256';
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $art = $this->getElementsByName('article', $fileContents);
    return $art;
  }

  protected function getAbstract(&$text) {
    $this->addLog('getting article summary');
    $summary = $this->getCData($this->getElementByName('headline2', $text));
    return $summary;
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $body = $this->getCData($this->getElementByName('story', $text));
    $body = strip_tags($body, '<p><br><strong><b><u><i>');
    if (empty($body)) {
      return '';
    }
    return $body;
  }

  public function getHeadline(&$text) {
    $DataContent = trim($this->getElementByName('Article', $text));
    $headline = $this->getCData($this->getElementByName('headline1', $text));
    return $headline;
  }

  public function getAuthor(&$text) {
    $byline = $this->getCData($this->getElementByName('byline', $text));
    return $byline;
  }

  public function getOriginalCategory(&$text) {
    $cat1 = $this->getCData($this->getElementByName('category1', $text));
    $cat2 = $this->getCData($this->getElementByName('category2', $text));
    if (!empty($cat1)) {
      if (!empty($cat2)) {
        $cat = $cat1 . ',' . $cat2;
      } else {
        $cat = $cat1;
      }
    }
    if (!empty($cat)) {
      return $cat;
    } else {
      return '';
    }
  }

  public function getArticleDate(&$text) {
    $date = $this->getCData($this->getElementByName('pubdate', $text));
    // Sun
    return $date;
  }

  public function parse() {
    $this->articles = parent::parse();
    $articles = array();
    foreach ($this->articles as $article) {
      // Check if the Article Day is Sunday move it to TitlID:1587 
      $dayName = date('D', strtotime($article['date']));
      if ($dayName != 'Sun') {
        $articles[] = $article;
      }
    }
    $this->articles = $articles;
    return $this->articles;
  }

}
