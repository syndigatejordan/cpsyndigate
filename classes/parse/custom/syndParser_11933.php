<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Copesa SA  [# publisher id = 1598]
//Title      : LT Pulso [ Spanish ] 
//Created on : Jun 30, 2021, 8:55:26 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11933 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}