<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Greek ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_578 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority' 			=> 	'Αναθέτουσα Αρχή',
										'Nature of Contract'			=> 	'Φύση της σύμβασης', 
										'Regulation of Procurement' 	=>	'Κανονισμοί Προμήθειας', 
										'Type of bid required'			=>	'Του είδους της προσφοράς', 
										'Award Criteria'				=>	'Κριτήρια ανάθεσης', 
										'Original CPV'					=>	'Αρχικό CPV', 
										'Country'						=>	'Χώρα', 
										'Document Id' 					=>	'Έγγραφο Id', 
										'Type of Document'				=>	'Είδος Εγγράφου', 
										'Procedure'						=>	'Διαδικασία', 
										'Original Language'				=>	'Πρωτότυπη γλώσσα', 
										'Current Language'				=> 	'Τρέχουσα Γλώσσα');	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('el');
		}
	}