<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Newstex, LLC  [# publisher id = 1519]
//Title      : CoinJournal [ Swedish ] 
//Created on : Nov 22, 2021, 2:06:20 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12253 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('sv'); 
	} 
}