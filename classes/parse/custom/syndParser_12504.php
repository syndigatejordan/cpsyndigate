<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Human Rights Watch  [# publisher id = 1302]
//Title      : Human Rights Watch [ Portuguese ] 
//Created on : Feb 28, 2022, 1:34:51 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12504 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}