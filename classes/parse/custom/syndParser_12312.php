<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CleanTechnica  [# publisher id = 1709]
//Title      : CleanTechnica [ English ] 
//Created on : Jan 11, 2022, 11:06:13 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12312 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}