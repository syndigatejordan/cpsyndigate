<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CryptoNews  [# publisher id = 1685]
//Title      : CryptoNews [ Dutch ] 
//Created on : Nov 23, 2021, 10:24:21 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12270 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('nl'); 
	} 
}