<?php

////////////////////////////////////////////////////////////////////////
// Publisher :  Trade Publishing and Distribution Ltd 
// Title     :  The Tripoli Post [English]
////////////////////////////////////////////////////////////////////////

class syndParser_4 extends syndParseCms {

	public function customInit() {
		parent::customInit();
		$this->defaultLang = $this->model->getLanguageId('en');	
	}

	public function getRawArticles(&$fileContents) {
		$fileContents = str_replace('�', '\'', $fileContents);
		return parent::getRawArticles($fileContents);
	}
}
