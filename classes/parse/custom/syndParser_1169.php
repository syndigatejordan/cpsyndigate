<?php 

///////////////////////////////////////////////////////////////////////////
// Publisher : Sarke Information Agency
// Titles    : 
///////////////////////////////////////////////////////////////////////////


class syndParser_1169 extends syndParseXmlContent {
	
    public function customInit() {
        parent::customInit();
	$this->defaultLang = $this->model->getLanguageId('ru');
        $this->charEncoding = 'UTF-16';
        $this->extensionFilter = 'txt';
    }
    
    public function getRawArticles(&$fileContents) {
        
        $exploded = explode('<HEADLINE>', $fileContents);
        $contents = array();
        
        foreach ($exploded as $record) {
            if($record) {
                $contents[] = '<HEADLINE>' . $record;
            }
        }
        
        return $contents;
    }
    
    public function getStory(&$text) {
        $story = trim($this->getElementByName('TEXT', $text));
        $story = '<p>' . str_replace(".\r\n"    , ".\r\n" . "</p>\r\n<p>", $story) .'</p>';                
        return $story;
    }
    
    public function getHeadline(&$text) {
        return trim($this->getElementByName('HEADLINE', $text));
    }
    
    public function getArticleDate(&$text) {
        $date = trim($this->getElementByName('DATE', $text));        
        return date('Y-m-d', strtotime($date));
    }
    
    public function getAuthor(&$text){
        return trim($this->getElementByName('PUBLICATION', $text));        
    }
    
}

