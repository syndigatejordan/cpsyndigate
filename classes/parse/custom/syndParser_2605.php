<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Korea Chamber of Commerce and Industry  [# publisher id = 848]
//Title      : Korea Chamber of Commerce and Industry (KCCI) - News [ English ] 
//Created on : Feb 09, 2016, 12:05:50 PM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2605 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}