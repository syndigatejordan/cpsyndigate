<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Orient Magazine Newspaper and Communication Ltd  [# publisher id = 1696]
//Title      : Orient Energy Review Online [ English ] 
//Created on : Jan 03, 2022, 1:00:47 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12295 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}