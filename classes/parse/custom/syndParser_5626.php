<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : openDemocracy Limited  [# publisher id = 1247]
//Title      : openDemocracy [ Spanish ] 
//Created on : May 21, 2019, 8:01:54 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5626 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}