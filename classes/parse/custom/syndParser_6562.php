<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Government Information Bureau of the Macau SAR  [# publisher id = 1409]
//Title      : NEWS.GOV.MO [ English ] 
//Created on : Sep 23, 2021, 8:06:40 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6562 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}