<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Insurer  [# publisher id = 1778]
//Title      : The ESG Insurer [ English ] 
//Created on : Feb 28, 2022, 1:08:42 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12500 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}