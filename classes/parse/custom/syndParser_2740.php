<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Groupe Eco-Media  [# publisher id = 941]
//Title      : Le Matin [ French ] 
//Created on : May 19, 2016, 7:57:18 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2740 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}