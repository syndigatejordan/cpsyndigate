<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Mzone FZ-LLC  
//Title     : cashy [ Arabic ] 
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_821 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getImages(&$text) {
    $this->story = $this->textFixation($this->getCData($this->getElementByName('fulltext', $text)));
    $this->imagesArray = array();
    preg_match_all("/<img[^>]+>/i", $this->story, $imgs);

    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = explode('alt="', $img);
      $image_caption = explode('"', $image_caption[1]);
      $image_caption = $image_caption[0];
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption))
        $images[0]['image_caption'] = str_replace('%20', ' ', $image_caption);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

}