<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Shehab News Agency  [# publisher id = 1206]
//Title      : Shehab News Agency [ Arabic ] 
//Created on : May 07, 2019, 11:02:54 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5552 extends syndParseRss {

  // Available video types
  private $videoTypes = array('mp4' => '.mp4');

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
    $this->extensionFilter = 'xml';

    require_once('/usr/local/syndigate/lib/getid3/getid3/getid3.php');
  }

  //Handle empty body
  public function parse() {
    $articles = array();
    foreach ($this->files as $file) {
      if ($this->extensionFilter) {
        if (!syndParseHelper::isFileExtension($file, $this->extensionFilter)) {
          $this->addLog("Wrong extension for file : $file)");
          continue;
        }
      }
      if (!file_exists($file)) {
        $this->addLog("file dose not exist: $file)");
        continue;
      }

      $this->addLog("get file contents (file:$file)");
      $fileContents = $this->getFileContents($file);

      if (!$fileContents) {
        continue;
      }

      $this->currentlyParsedFile = $file;
      $this->loadCurrentDirectory();
      $this->loadUpperDir();
      $rawArticles = $this->getRawArticles($fileContents);
      foreach ($rawArticles as $rawArticle) {
        $article = $this->getArticle($rawArticle);
        $articles[] = $article;
      }
    }
    return $articles;
  }

  //Return empty body for the article by Mariam request
  protected function getStory(&$text) {
    $this->addLog("getting article body");
    return "";
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('item', $fileContents);
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->getCData($this->getElementByName('title', $text));
    return $this->textFixation($headline);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = trim(preg_replace('/T(.*)/is', '', $this->getElementByName('pubDate', $text)));
    return date('Y-m-d', strtotime($date));
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $video_Name = $this->textFixation($this->getElementByName('file', $text));
    $videoName = $this->currentDir . $video_Name;
    $video_Name_arr = explode('.', $video_Name);
    $videos = array();
    $videoInformation = array();
    $neededInfo = array('filesize', 'fileformat', 'encoding', 'mime_type', 'playtime_seconds', 'bitrate', 'playtime_string', 'video');

    //foreach ($this->videoTypes as $videoLable => $videoType) {
    if (in_array('.' . $video_Name_arr[1], $this->videoTypes)) {
      if (file_exists($videoName)) {

        $getID3 = new getID3;
        $realVideoPath = str_replace($this->title->getParseDir(), $this->title->getHomeDir(), $videoName);
        $videoInfo = $getID3->analyze($realVideoPath);

        foreach ($neededInfo as $key => $value) {

          if (in_array($value, $videoInfo)) {
            switch ($value) {
              case 'video':
                $videoInformation['width'] = $videoInfo[$value]['resolution_x'];
                $videoInformation['height'] = $videoInfo[$value]['resolution_y'];
                break;
              case 'bitrate':
                $videoInformation[$value] = floor($videoInfo[$value] / 1024);
                break;

              default:
                $videoInformation[$value] = $videoInfo[$value];
                break;
            }
          }
        }


        //$name = $this->model->getVidReplacement($this->videoName . $videoType, $this->currentDir, 238) ;
        //$video['video_name']    	= str_replace(VIDEOS_PATH, VIDEOS_HOST, $name);

        $video['video_name'] = $this->getAndCopyVideosFromArray($videoName);

        //$bitRate 					= $videoInfo['bitrate'] / 1000;
        $mimeType = $videoInfo['mime_type'];
        $video['original_name'] = $videoName;
        $video['video_caption'] = $video_Name_arr[0];
        //$video['bit_rate']       	= $this->getBitRateFromName($videoLable);
        $video['bit_rate'] = json_encode($videoInformation);    //$bitRate . ' kbps';
        $video['mime_type'] = $mimeType;
        $video['added_time'] = date('Y-m-d h:i:s', filemtime($video['original_name']));
        $videos[] = $video;
      }
    }
    return $videos;
  }

  private function getBitRateFromName($name) {
    $bit = explode("(", $name);

    if (is_array($bit) & count($bit) > 1) {
      $bit = $bit[1];
      $bit = explode(")", $bit);
      return $bit[0];
    } else {
      return '';
    }
  }

}
