<?php

class syndParser_1053 extends syndParseRss {

  public function getRawArticles(&$fileContents) {
    $this->addLog('getting row articles');
    $rowArticles = parent::getRawArticles($fileContents);
    return $rowArticles;
  }

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog('getting article story');
    $story = $this->textFixation($this->getCData($this->getElementByName('content:encoded', $text)));
    $story = preg_replace("/<img[^>]+\>/i", '', $story);
    $story = str_replace('<p style="text-align: center;"></p>', '', $story);

    return $story;
  }

  protected function getAbstract(&$text) {
    $this->addLog('getting article summary');
    $summary = $this->textFixation($this->getCData($this->getElementByName('description', $text)));
    $summary = strip_tags($summary);
    return $summary;
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article original data');
    $cats = $this->getElementsByName('category', $text);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = $this->textFixation($this->getCData($cat));
      }
    }
    return implode(', ', $originalCats);
  }

  protected function getImages(&$text) {
    $this->story = $this->textFixation($this->getCData($this->getElementByName('content:encoded', $text)));

    $imagesArray = array();
    $imgs = array();

    preg_match("/<img[^>]+\>/i", $this->story, $imgs);

    foreach ($imgs as $img) {



      $this->addLog("getting article images");
      $imagePath = syndParseHelper::getImgElements($img);

      $imagePath = $imagePath[0];

      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));

      array_push($imagesArray, $images[0]);
    }

    return $imagesArray;
  }

}