<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : WENN Media Group  [# publisher id =1189 ] 
//Title      : WENN Newswire [ English ] 
//Created on : Oct 09, 2018, 12:48:44 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3741 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    $this->loadCurrentDirectory();

    $this->addLog("getting raw articles text");
    return $this->getElementsByName('entry', $fileContents);
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = $this->textFixation($this->getCData($this->getElementByName('wenn_cm:body', $text)));
    $story = preg_replace('/[\n\r]/', "</p><p>", $story);
    if (!empty($story)) {
      $story = "<p>$story</p>";
    }

    $story = trim(preg_replace('!\s+!', ' ', $story));
    //This action depends on "Task #1064 Remove all the images from our clients feeds"
    $story = preg_replace('/<img[^>]+\>/i', '', $story);

    return $story;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return $this->textFixation($this->getCData($this->getElementByName('wenn_cm:title', $text)));
  }

  protected function getAbstract(&$text) {
    $this->addLog("getting abstract");
    $summary = $this->textFixation($this->getCData($this->getElementByName('wenn_cm:summary', $text)));
    if (empty($summary)) {

      $summary = $this->textFixation($this->getCData($this->getElementByName('wenn_cm:first_line', $text)));
    }
    return $summary;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('wenn_nm:published', $text);
    $date = trim(str_replace('/\+(.*)/is', '', $date));
    return date('Y-m-d', strtotime($date));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    return $this->textFixation($this->getCData($this->getElementByName('wenn_nm:feedarticle_section', $text)));
  }

  protected function getImages(&$text) {
    //This action depends on "Task #1064 Remove all the images from our clients feeds"
    return array();

    $images = array();
    $this->addLog("getting article image");
    $media = null;
    preg_match_all("/<wenn_mm:media(.*?)<\/wenn_mm:media>/is", $text, $media);
    $media = $media[0];
    foreach ($media as $item) {
      $image_caption = "";
      $caption = $this->textFixation($this->getCData($this->getElementByName('caption', $item)));
      $credit = $this->textFixation($this->getCData($this->getElementByName('credit', $item)));
      if (!empty($credit)) {
        $credit = " ($credit)";
      }
      $image_caption = trim("$caption$credit");

      $id = syndParseHelper::getImgElements($item, 'wenn_mm:media', 'id');
      $version = syndParseHelper::getImgElements($item, 'wenn_mm:media', 'version');
      $mimetype = syndParseHelper::getImgElements($item, 'wenn_mm:media', 'mimetype');


      $image_Name = syndParseHelper::getImgElements($item, 'wenn_mm:media', 'value');
      $image_Name = str_replace("file://", "", $image_Name[0]);
      $image_full_path = $this->currentDir . $image_Name;
      list($width, $height) = getimagesize($image_full_path);
      $width = round($width, -2);
      $height = round($height, -2);
      $original_name = explode('.', $image_Name);
      if (file_exists($image_full_path)) {
        //the video name is same as the image name here  		
        $name = $this->model->getImgReplacement($image_Name, $this->currentDir, 3741);
        $img['img_name'] = str_replace(IMGS_PATH, "https://syndigateimages.s3.amazonaws.com/syndigate/imgs/", $name) . "?id={$id[0]}&version={$version[0]}&mimetype={$mimetype[0]}&height={$height}&width={$width}";
        $img['original_name'] = $original_name[0];
        $img['image_caption'] = $image_caption;
        $img['is_headline'] = FALSE;
        $img['image_original_key'] = sha1($image_Name);
        $images[] = $img;
      }
    }
    return $images;
  }

  public function getArticleOriginalId($params = array()) {
    $defaultArray = array('text' => '', 'headline' => '', 'articleDate' => '', 'issueNum' => 0);
    $params = array_merge($defaultArray, $params);

    if (!$articleOriginalId = (int) trim($this->getElementByName('wenn_nm:original_article_id', $params['text']))) {
      return parent::getArticleOriginalId($params);
    }
    return $this->title->getId() . '_' . sha1($articleOriginalId) . '_' . $articleOriginalId;
  }

}

?>
