<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sultanate of Oman Ministry of Foreign Affairs  [# publisher id = 879]
//Title      : Sultanate of Oman Ministry of Foreign Affairs - News [ Arabic ] 
//Created on : Mar 01, 2016, 10:41:38 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2628 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}
