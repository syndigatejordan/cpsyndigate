<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CountryWatch Inc.  [# publisher id = 549]
//Title      : CountryWatch Political Intelligence Briefing [ English ] 
//Created on : Sep 24, 2017, 6:16:59 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1651 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}