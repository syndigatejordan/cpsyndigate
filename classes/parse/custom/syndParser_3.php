<?php

	////////////////////////////////////////////////////////////////////////
	// Publisher : Jordan Press & Publishing Company
	// Titles    : The Star
	////////////////////////////////////////////////////////////////////////

	class syndParser_3 extends syndParseCms
	{

		public function customInit()
		{
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('en');
		}

		public function getRawArticles(&$fileContents) {	//get raw articles
		    	$fileContents = str_replace("", " ", $fileContents);
			$fileContents = str_replace("", "\"", $fileContents);
			$fileContents = str_replace("", "\"", $fileContents);
			$fileContents = str_replace("", "'", $fileContents);
			$fileContents = str_replace("", "'", $fileContents);

			return parent::getRawArticles($fileContents);

		}

		protected function getArticleDate(&$text)
		{
			//get article date
			$this->addLog("getting articles date");
			$date = syndParseHelper::getDateFromPath($this->currentlyParsedFile);
			if($date) {
				return $this->dateFormater($date);
			}
			return parent::getArticleDate(&$text);
		}

		protected function getIptcId(&$text)
		{
			$this->addLog("getting category name");
			$originalCategoryName = syndParseHelper::getCategoryNameFromFileName($this->currentlyParsedFile);
			$iptcId = $this->model->getIptcCategoryId($originalCategoryName);
			return ($iptcId ? $iptcId : parent::getIptcId($text));
		}
	
	
		public function getArticleOriginalId($params = array()) {
				
			$originalId = (int)trim($this->getElementByName('id', $params['text']));
			$originalId = $this->title->getId() . '_' . sha1($originalId) . '_' . $originalId;
			
			if($originalId) {
				return $originalId;
			} else {
				return parent::getArticleOriginalId($params);
			}			
		}
		
		protected function getStory(&$text)
		{
			$this->addLog("getting article body");
			$story = $this->textFixation($this->getElementByName('fulltext', $text));
			$story = preg_replace("/<img[^>]+\>/i", "", $story);
			
			$story = "<p>".$story;
			$story = str_replace(". ",".</p>\n<p>",$story);
			$story .= "</p>"; 
			return $story;
		}
	}
