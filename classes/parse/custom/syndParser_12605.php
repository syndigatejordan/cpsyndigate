<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Fusion Media Limited  [# publisher id = 304]
//Title      : Investing.com ไทย (Thailand Edition) [ Thai ] 
//Created on : May 12, 2022, 7:55:59 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12605 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('th'); 
	} 
}