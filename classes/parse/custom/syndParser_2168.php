<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : Euronews SA
//Title     : euronews - Video Feed (English)
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_2168 extends syndParseRss {

  // Available video types
  private $videoTypes = array('mp4' => '.mp4', 'MP4' => '.MP4', 'flv' => '.flv', 'mpg' => '.mpg', 'wmv' => '.wmv', 'rm' => '.rm', 'rm(56k)' => '-56k.rm', 'wmv(56)' => '-56k.wmv', 'rm(256k)' => '-256k.rm', 'wmv(256k)' => '-256k.wmv');
  public $filename;

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('item', $fileContents);
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
    return $this->textFixation($headline);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->textFixation($this->getCData($this->getElementByName('pubDate', $text)));
    return date('Y-m-d', strtotime($date));
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
    return $this->textFixation($story);
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $filename = basename($this->currentlyParsedFile);
    $filename = str_replace('.xml', '', $filename);
    return $filename;
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    preg_match('#<media:content url="[^>]+/>#i', $text, $videoname);
    $videoname = $videoname[0];
    $videoname = explode('url="', $videoname);
    $videoname = $videoname[1];
    $videoname = explode('"', $videoname);
    $video_Name = $videoname[0];
    
    $videoName = explode('/en/', $video_Name);
    var_dump($videoName);
    $videoName = $videoName[1];

    $video_Name_arr = explode('.', $videoName);

    $videos = array();

    $video['video_name'] = $video_Name;
    $video['original_name'] = $videoName;
    $video['video_caption'] = $video_Name_arr[0];
    
   /* $mimeType = $videoInfo['mime_type'];
    $video['mime_type'] = $mimeType;*/
     $date = $this->getElementByName('pubDate', $text);
    $video['added_time'] = date('Y-m-d h:i:s',strtotime($date));
    $videos[] = $video;
    /*var_dump($videos);
    exit;*/
    return $videos;
  }

}
