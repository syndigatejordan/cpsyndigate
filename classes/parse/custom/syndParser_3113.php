<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Watwan  [# publisher id = 1121]
//Title      : Al Watwan [ French ] 
//Created on : Aug 28, 2017, 9:07:12 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3113 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}