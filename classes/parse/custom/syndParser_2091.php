<?php

//////////////////////////////////////////////////////////////////////////////
// Publisher: Global Data Point Ltd.  [# publisher id =378 ] 
// Titles   : Governance, Risk & Compliance Monitor Worldwide (PDF Reports)
//////////////////////////////////////////////////////////////////////////////

class syndParser_2091 extends syndParseAbTxtSample {

  public function customInit() {
    parent::customInit();
    //$this->charEncoding = 'WINDOWS-1256';
    $this->extensionFilter = 'txt';
    $this->defaultLang = $this->model->getLanguageId('en');
  }
protected function getRawArticles(&$fileContents)
		{
  var_dump($fileContents);
			return array($fileContents);
		}
  protected function getHeadline(&$text, $headlineTagName = 'report title') {
    //get article title
    $this->addLog("getting article headline");
    $headline = $this->textFixation($this->getItemValue($headlineTagName, $text));
    $this->removeItem($headlineTagName, $text);
    return $headline;
  }

  protected function getAuthor(&$text) {
    //get author
    $this->addLog("getting article author");
    $author = $this->textFixation($this->getItemValue('analyst', $text));
    $this->removeItem('Dateline', $text);
    return $author;
  }

  protected function getArticleDate(&$text, $dateTagName = 'reportDate') {
    //get article date
    $this->addLog("getting article date");
    $date = $this->getItemValue($dateTagName, $text);
    $this->removeItem($dateTagName, $text);
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  protected function getStory(&$text) {
    //return strip_tags(parent::getStory($text), '<br><BR>');
  }

  protected function getImages(&$text) {
    $this->addLog("getting article images");
    $image_Name = trim($this->textFixation($this->getItemValue('link', $text)));
    $this->removeItem('link', $text);
    if (!empty($image_Name)) {
      $image_full_path = $this->currentDir . $image_Name;
      $original_name = explode('.', $image_Name);
      if (file_exists($image_full_path)) {
        //the video name is same as the image name here  		
        $name = $this->model->getImgReplacement($image_Name, $this->currentDir, 5);
        $img['img_name'] = str_replace(IMGS_PATH, IMGS_HOST, $name);
        $img['original_name'] = $original_name[0];
        $img['image_caption'] = $image_Name;
        $img['is_headline'] = true;
        $images[] = $img;
        return $images;
      } else {

        return false;
      }
    } else {

      return false;
    }
  }

}