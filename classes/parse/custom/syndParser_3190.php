<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : MDS   [# publisher id = 1163]
//Title      : QallwDall [ Arabic ] 
//Created on : Sep 19, 2021, 7:06:01 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3190 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}