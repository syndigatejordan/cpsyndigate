<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : NBK Capital  [# publisher id = 695]
//Title      : NBK Capital - MENA Monthly [ English ] 
//Created on : Feb 02, 2016, 12:02:04 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2182 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}