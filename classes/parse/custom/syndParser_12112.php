<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Diario El Sol del Cusco  [# publisher id = 1609]
//Title      : El Sol del Cusco [ Spanish ] 
//Created on : Aug 09, 2021, 8:27:04 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12112 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}