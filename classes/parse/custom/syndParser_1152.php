<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Gender Links  
//Title     : Gender Links Opinion and Commentary Service [ English ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1152 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
