<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bloomberg  [# publisher id = 1202]
//Title      : Bloomberg [ English ] 
//Created on : Dec 12, 2018, 9:23:39 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_4810 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    $date = str_replace('/GMT(.*)/is', '', $date);
    return date('Y-m-d', strtotime($date));
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $this->story = $this->getCData($this->getElementByName('description', $text));
    $this->story = str_replace("&nbsp;", " ", $this->story);
    $this->story = $this->textFixation($this->story);
    $this->story = trim(preg_replace('!\s+!', ' ', $this->story));
    return $this->story;
  }

  protected function getVideos(&$text) {
    $enclosure = null;
    $videos = array();
    preg_match_all("/<media:content medium=\"video\"(.*?)\/>/is", $text, $enclosure);
    foreach ($enclosure[0] as $medium) {
      $this->addLog("getting article videos");
      $videoname = syndParseHelper::getImgElements($medium, 'media:content', 'url');
      $link = $videoname[0];
      $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
      $video['video_name'] = $link;
      $video['original_name'] = $videoName;
      $video['video_caption'] = $videoName;
      $mimeType = syndParseHelper::getImgElements($medium, 'media:content', 'type');
      $mimeType = $mimeType[0];
      $video['mime_type'] = $mimeType;
      $date = $this->getElementByName('pubDate', $text);
      $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
      $videos[] = $video;
    }

    return $videos;
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $image_caption = "";
    $enclosure = null;
    preg_match_all("/<media:content medium=\"image\"(.*?)<\/media:content>/is", $text, $enclosure);
    foreach ($enclosure[0] as $imgs) {
      $this->addLog("getting article images");
      $image_caption = '';
      $mediaTitle = $this->textFixation($this->getCData($this->getElementByName('media:title', $imgs)));
      $mediaText = $this->textFixation($this->getCData($this->getElementByName('media:text', $imgs)));
      $mediaCredit = $this->textFixation($this->getCData($this->getElementByName('media:credit', $imgs)));
      $image_caption = "$mediaTitle $mediaText $mediaCredit";
      $image_caption = trim(preg_replace('!\s+!', ' ', $image_caption));

      $imageInfo = syndParseHelper::getImgElements($imgs, 'media:content', 'url');
      $imagePath = $imageInfo[0];
      $imagePath = preg_replace("/\?(.*)/is", "", $imagePath);
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = sha1($imageUrl) . ".jpeg";
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $opts = array();
      $http_headers = array();
      $http_headers[] = 'Expect:';

      $opts[CURLOPT_URL] = $imageUrl;
      $opts[CURLOPT_HTTPHEADER] = $http_headers;
      $opts[CURLOPT_CONNECTTIMEOUT] = 10;
      $opts[CURLOPT_TIMEOUT] = 60;
      $opts[CURLOPT_HEADER] = FALSE;
      $opts[CURLOPT_BINARYTRANSFER] = TRUE;
      $opts[CURLOPT_VERBOSE] = FALSE;
      $opts[CURLOPT_SSL_VERIFYPEER] = FALSE;
      $opts[CURLOPT_SSL_VERIFYHOST] = 2;
      $opts[CURLOPT_RETURNTRANSFER] = TRUE;
      $opts[CURLOPT_FOLLOWLOCATION] = TRUE;
      $opts[CURLOPT_MAXREDIRS] = 2;
      $opts[CURLOPT_IPRESOLVE] = CURL_IPRESOLVE_V4;

      # Initialize PHP/CURL handle
      $ch = curl_init();
      curl_setopt_array($ch, $opts);
      $imgContent = curl_exec($ch);

      # Close PHP/CURL handle
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        //unlink($copiedImage);
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

  public function getArticleOriginalId($params = array()) {
    $articleOriginalId = $this->getElementByName('asset_id', $params['text']);
    $this->articleOriginalId = $articleOriginalId;
    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }
    return $this->title->getId() . "_" . sha1($articleOriginalId);
  }

}
