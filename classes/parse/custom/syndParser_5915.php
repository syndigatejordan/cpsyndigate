<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : 3E Médias  [# publisher id = 1300]
//Title      : Sustainability Times [ English ] 
//Created on : Sep 19, 2021, 8:53:13 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5915 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}