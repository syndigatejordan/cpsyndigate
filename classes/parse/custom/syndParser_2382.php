<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dar Al Khaleej for Press, Printing & Publishing  [# publisher id = 766]
//Title      : Al Khaleej [ Arabic ] 
//Created on : Aug 15, 2021, 8:02:38 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2382 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}