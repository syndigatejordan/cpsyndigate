<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Kapitalis SUARL  [# publisher id = 462]
//Title      : Kapitalis [ French ] 
//Created on : Mar 18, 2022, 1:49:47 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1424 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}