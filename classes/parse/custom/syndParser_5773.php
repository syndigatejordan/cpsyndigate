<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CG Arabia SPC.  [# publisher id = 366]
//Title      : GulfInsider - Saudi Arabia News [ English ] 
//Created on : Sep 23, 2019, 8:34:21 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5773 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}