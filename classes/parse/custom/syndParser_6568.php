<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Iranian Labour News Agency (ILNA)  [# publisher id = 1411]
//Title      : Iranian Labour News Agency (ILNA) [ Persian ] 
//Created on : Oct 20, 2020, 11:58:51 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6568 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fa'); 
	} 
}