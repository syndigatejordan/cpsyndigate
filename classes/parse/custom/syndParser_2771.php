<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Yedioth Tikshoret  [# publisher id = 970]
//Title      : Yedioth Ahronoth [ English ] 
//Created on : May 19, 2016, 11:06:47 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2771 extends syndParseRss {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('data', $fileContents);
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $article = $this->getElementsByName('article', $text);
    $headline = trim($this->textFixation($this->getCData($this->getElementByName('title', $article[1]))));
    return $headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $article = $this->getElementsByName('article', $text);
    $matches = NULL;
    preg_match("/<pubDate>(.*?)<\/pubDate>/is", $article[1], $matches);
    $date = explode(" ", $matches[1]);
    $date = explode("/", $date[0]);
    $date = $date[2] . "-" . $date[1] . "-" . $date[0];
    return $date;
  }

  protected function getStory(&$text) {
      $this->addLog("getting article text");
      $article = $this->getElementsByName('article', $text);
      $this->story = trim($this->textFixation($this->getCData($this->getElementByName('description', $article[1]))));
      $this->story = preg_replace('!\s+!', ' ', $this->story);

      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);

      return $this->story;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $article = $this->getElementsByName('article', $text);
    $author = trim($this->textFixation($this->getCData($this->getElementByName('author', $article[1]))));
    return $author;
  }

  protected function getAbstract(&$text) {
    //get article summary
    $this->addLog("getting article summary");
    $article = $this->getElementsByName('article', $text);
    $author = trim($this->textFixation($this->getCData($this->getElementByName('subTitle', $article[1]))));
    return $author;
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $article = $this->getElementsByName('article', $text);
    $category = trim($this->textFixation($this->getCData($this->getElementByName('category', $article[1]))));
    return $category;
  }

  public function getArticleReference(&$text) {
    $this->addLog('getting article Link');
    $link = $this->getElementsByName('href', $text);
    return $link[0];
  }

  protected function getImages(&$text)
  {
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      return array();

      $imagesArray = array();
      $article = $this->getElementsByName('article', $text);
      $image_caption = "";

      $imagePath = trim($this->textFixation($this->getCData($this->getElementByName('top_story_image', $article[1]))));
      if ($imagePath) {
          $this->addLog("getting article images");
          $image_caption = trim($this->textFixation($this->getCData($this->getElementByName('topStoryImageDetailsTitle', $article[1]))));
          if (!$this->checkImageifCached($imagePath)) {
              $imagePath = str_replace(' ', '%20', $imagePath);
        $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
        if ($copiedImage) {
          $images = $this->getAndCopyImagesFromArray(array($copiedImage));
          if (!empty($image_caption)) {
            $images[0]['image_caption'] = $image_caption;
          }
          $name_image = explode('/images/', $copiedImage);
          if ($images[0]['image_caption'] == $name_image[1]) {
            $images[0]['image_caption'] = '';
          }
          $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
          $images[0]['is_headline'] = TRUE;
          array_push($imagesArray, $images[0]);
        }
      }
    }
    $imagePath = trim($this->textFixation($this->getCData($this->getElementByName('link', $article[1]))));
    if ($imagePath) {
      $this->addLog("getting article images");
      $image_caption = trim($this->textFixation($this->getCData($this->getElementByName('linkCredits', $article[1]))));
      if (!$this->checkImageifCached($imagePath)) {
        $imagePath = str_replace(' ', '%20', $imagePath);
        $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
        if ($copiedImage) {
          $images = $this->getAndCopyImagesFromArray(array($copiedImage));
          if (!empty($image_caption)) {
            $images[0]['image_caption'] = $image_caption;
          }
          $name_image = explode('/images/', $copiedImage);
          if ($images[0]['image_caption'] == $name_image[1]) {
            $images[0]['image_caption'] = '';
          }
          $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
          $images[0]['is_headline'] = TRUE;
          array_push($imagesArray, $images[0]);
        }
      }
    }
    return $imagesArray;
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $article = $this->getElementByName('items_article_videos', $text);
    if (!empty($article)) {
      $link = trim($this->textFixation($this->getCData($this->getElementByName('url', $article))));
      $videoName = $this->getElementByName('title', $text);
      $videos = array();
      $video['video_name'] = $link;
      $video['original_name'] = $videoName;
      $video['video_caption'] = $videoName;
      $video['mime_type'] = "mp4";
      $date = $this->getElementByName('pubDate', $text);
      $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
      $videos[] = $video;
      return $videos;
    } else {
      return array();
    }
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = sha1($imageUrl) . ".jpeg";
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );

      $ch = curl_init($imageUrl);
      curl_setopt_array($ch, $options);
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
