<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bonhill Group plc  [# publisher id = 1733]
//Title      : ESG Clarity Asia [ English ] 
//Created on : Jan 16, 2022, 12:07:49 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12334 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}