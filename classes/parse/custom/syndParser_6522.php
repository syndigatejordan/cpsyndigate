<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The United Nations Department of Global Communications  [# publisher id = 1400]
//Title      : UN News [ Arabic ] 
//Created on : Oct 20, 2020, 1:42:11 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6522 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}