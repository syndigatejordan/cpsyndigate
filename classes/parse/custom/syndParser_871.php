<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Alpha Beta Publishers and Media Consultants  [# publisher id = 253]
//Title      : Al Ain Times [ Arabic ] 
//Created on : Feb 02, 2016, 11:49:37 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_871 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}