<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : NewsBites Pty Ltd.  [# publisher id = 341]
//Title      : BuySellSignals - Stock Reports (Intra-Day Reports) [ English ] 
//Created on : Jul 14, 2022, 11:46:19 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12664 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}