<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Corporación Medios Digitales del Sur Ltda  [# publisher id = 1590]
//Title      : Mundo Agropecuario BET [ Spanish ] 
//Created on : Jun 09, 2021, 8:11:46 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10876 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}