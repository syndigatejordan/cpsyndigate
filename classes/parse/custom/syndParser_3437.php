<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Nile Radio Productions  [# publisher id =1165 ] 
//Title      : NileFM  [ English ] 
//Created on : Feb 22, 2018, 08:16:03 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3437 extends syndParseRss {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('data', $fileContents);
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->textFixation($this->getCData($this->getElementByName('Title', $text)));
    return $headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");

    $date = trim(preg_replace('/\+(.*)/is', '', $this->getElementByName('PublishDate', $text)));
    return date('Y-m-d', strtotime($date));
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $this->story = str_replace('&nbsp;', '', $this->getElementByName('Description', $text));
    $this->story = $this->textFixation($this->getCData($this->story));
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story)) {
      $this->story = $this->textFixation($this->getCData($this->getElementByName('Title', $text)));
    }
    return $this->story;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getCData($this->getElementByName('FirstName', $text)));
  }

  public function getOriginalCategory(&$text) {

    $this->addLog('Getting article category');
    return $this->textFixation($this->getCData($this->getElementByName('cat', $text)));
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $image_caption = "";
    $imagePath = $this->textFixation($this->getCData($this->getElementByName('Thumbnail', $text)));
    if ($imagePath) {
      if (!$this->checkImageifCached($imagePath)) {
        $imagePath = str_replace(' ', '%20', $imagePath);
        $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
        if ($copiedImage) {
          $images = $this->getAndCopyImagesFromArray(array($copiedImage));
          if (!empty($image_caption)) {
            $images[0]['image_caption'] = $image_caption;
          }
          $name_image = explode('/images/', $copiedImage);
          if ($images[0]['image_caption'] == $name_image[1]) {
            $images[0]['image_caption'] = '';
          }
          $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
          $images[0]['is_headline'] = TRUE;
          array_push($imagesArray, $images[0]);
        }
      }
    }

    return $imagesArray;
  }

  protected function getVideos(&$text) {
    $link = $this->textFixation($this->getCData($this->getElementByName('EmbedURL', $text)));
    $type = "youtube";
    $videoName = $this->getElementByName('Title', $text);
    $videos = array();
    $video['video_name'] = $link;
    $video['original_name'] = $videoName;
    $video['video_caption'] = $videoName;

    $video['mime_type'] = $type;
    $date = $this->getElementByName('PublishDate', $text);
    $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
    $videos[] = $video;
    return $videos;
  }

}
