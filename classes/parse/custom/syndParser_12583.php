<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Jornal O Democrata  [# publisher id = 1793]
//Title      : Jornal O Democrata [ Portuguese ] 
//Created on : May 09, 2022, 2:34:25 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12583 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}