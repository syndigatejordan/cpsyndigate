<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : InsideEVs  [# publisher id = 1717]
//Title      : InsideEVs [ English ] 
//Created on : Jan 11, 2022, 1:33:43 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12321 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}