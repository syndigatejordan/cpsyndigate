<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Foreign Ministry Algeria  [# publisher id = 837]
//Title      : Algerian Ministry of Foreign Affairs - Press conferences & statements [ English ] 
//Created on : Jan 31, 2016, 1:42:55 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2569 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}