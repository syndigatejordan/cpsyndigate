<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Islamic Republic News Agency (IRNA)  [# publisher id = 1412]
//Title      : Islamic Republic News Agency (IRNA) [ Chinese ] 
//Created on : Oct 22, 2020, 6:20:34 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6596 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh'); 
	} 
}