<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: Motivate publishing
	// Titles   : Business traveller Middle East [ English ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_323 extends syndParseCms {
		
		protected function getStory(&$text) {
			
			$story = parent::getStory($text);
			$story = str_replace(". ", ".</p>\n<p>", $story);
			return $story;
		}
	}