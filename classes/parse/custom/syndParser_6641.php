<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : MONTSAME News Agency  [# publisher id = 1423]
//Title      : MONTSAME News Agency [ Japanese ] 
//Created on : Oct 17, 2020, 2:37:34 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6641 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	} 
}