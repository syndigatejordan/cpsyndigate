<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Tanzania Chamber of Commerce, Industry and Agriculture  [# publisher id = 835]
//Title      : Tanzania Chamber of Commerce, Industry and Agriculture (TCCIA) - News [ English ] 
//Created on : Feb 02, 2016, 12:13:09 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2559 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}