<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Infonicks Information & Internet Services GmbH  [# publisher id =905 ]  
//Title      : PortSEurope [ English ] 
//Created on : Jul 19, 2016, 11:23:16 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2688 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getRawArticles(&$fileContents) {
    $this->addLog("Getting raw articles text");
    $rawArticles = $this->getElementsByName('Article', $fileContents);
    return $rawArticles;
  }

  protected function getHeadline(&$text) {
    $this->addLog('Getting aticle Headline');
    $headline = trim($this->textFixation($this->getCData($this->getElementByName('Headline', $text))));
    $headline = trim(preg_replace('/\[http(.*)html\]/is', '', $headline));
    return $headline;
  }

  protected function getStory(&$text) {
    $this->addLog('Getting aticle Headline');
    $Country = trim($this->textFixation($this->getCData($this->getElementByName('Tags-Country', $text))));
    $Country = trim(preg_replace('/Add(.*?)here/is', '', $Country));
    if (!empty($Country)) {
      $Country = $Country . "\n";
    }
    $Subject = trim($this->textFixation($this->getCData($this->getElementByName('Tags-Subject', $text))));
    $Subject = trim(preg_replace('/Add(.*?)here/is', '', $Subject));
    if (!empty($Subject)) {
      $Subject = "<br />" . $Subject . "\n";
    }
    $Companies = trim($this->textFixation($this->getCData($this->getElementByName('Tags-Companies', $text))));
    $Companies = trim(preg_replace('/Add(.*?)here/is', '', $Companies));
    if (!empty($Companies)) {
      $Companies = "<br />" . $Companies . "\n";
    }
    $Ports = trim($this->textFixation($this->getCData($this->getElementByName('Tags-Ports', $text))));
    $Ports = trim(preg_replace('/Add(.*?)here/is', '', $Ports));
    if (!empty($Ports)) {
      $Ports = "<br />" . $Ports . "\n";
    }
    $story = "<br />" . trim($this->textFixation($this->getCData($this->getElementByName('Abstract', $text))));
    $story = $Country . $Subject . $Companies . $Ports . $story;
    $story = trim(preg_replace('/[\n\r]Source:(.*)/is', '', $story));
    return $story;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("Getting article date");
    $date = $this->textFixation($this->getCData($this->getElementByName('Date', $text)));
    return date('Y-m-d', strtotime($date));
  }

  public function getArticleReference(&$text) {
    $this->addLog("Getting article reference");
    $link = $this->textFixation($this->getCData($this->getElementByName('link', $text)));
    $link = trim(preg_replace('/\[http(.*)html\]/is', '', $link));
    return $link;
  }

  protected function getOriginalCategory(&$text) {
    $this->addLog("getting article original category");
    $cat = $this->textFixation($this->getCData($this->getElementByName('Tags-Industry', $text)));
    $cat = str_replace('Industry -', "", $cat);
    $cat = trim(preg_replace('!\s+!is', ',', $cat), ',');
    return $cat;
  }

  protected function getAuthor(&$text) {
    $this->addLog("Getting article author");
    $author = trim($this->getCData($this->getElementByName('Source', $text)));
    return $this->textFixation($author);
  }

  public function getArticleOriginalId($params = array()) {
    $this->addLog("Getting article OriginalId");
    $articleOriginalId = trim($this->getElementByName('Id', $params['text']));
    $this->articleOriginalId = $articleOriginalId;
    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }
    return $this->title->getId() . "_" . sha1($articleOriginalId) . "_" . $articleOriginalId;
  }

}
