<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CPI Construction  [# publisher id = 307]
//Title      : Big Project Middle East [ English ] 
//Created on : Jul 28, 2021, 12:35:46 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1024 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}