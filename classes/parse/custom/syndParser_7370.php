<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CLYS Comunicaciones 3.0. S. L.  [# publisher id = 1474]
//Title      : 14ymedio [ Spanish ] 
//Created on : Sep 23, 2021, 8:11:44 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7370 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}