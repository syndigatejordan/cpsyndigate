<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : RIM INTELLIGENCE Co  [# publisher id = 1807]
//Title      : RIM Intelligence - Market News [ Japanese ] 
//Created on : Jun 02, 2022, 1:51:43 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12633 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	} 
}