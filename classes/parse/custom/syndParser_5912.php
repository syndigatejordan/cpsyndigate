<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Teachers College at Columbia University  [# publisher id = 1297]
//Title      : The Hechinger Report [ English ] 
//Created on : Sep 19, 2021, 8:53:05 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5912 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}