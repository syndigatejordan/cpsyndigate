<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : InSight Crime  [# publisher id = 1355]
//Title      : InSight Crime [ English ] 
//Created on : Sep 19, 2021, 8:54:15 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6410 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}