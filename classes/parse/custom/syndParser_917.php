<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : PR Factory  
//Title     : Leaders [ French ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_917 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('fr');
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = preg_replace('!\s+!', ' ', $this->story);
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

}
