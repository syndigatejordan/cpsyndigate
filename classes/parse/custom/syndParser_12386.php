<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Yerwa Express News  [# publisher id = 1755]
//Title      : Yerwa Express News [ English ] 
//Created on : Feb 14, 2022, 7:16:05 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12386 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}