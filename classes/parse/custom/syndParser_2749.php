<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Nation Publishing Services  [# publisher id = 951]
//Title      : Seychelles Nation [ English ] 
//Created on : May 18, 2016, 9:23:19 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2749 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}