<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sarke Information Agency  [# publisher id =352 ] 
//Title      : Sarke Special Report [ Russian ] 
//Created on : Jul 31, 2016, 7:02:16 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2797 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ru');
    $this->charEncoding = 'UTF-16';
    $this->extensionFilter = 'txt';
  }

  public function getRawArticles(&$fileContents) {

    $exploded = explode('<HEADLINE>', $fileContents);
    $contents = array();

    foreach ($exploded as $record) {
      if ($record) {
        $contents[] = '<HEADLINE>' . $record;
      }
    }

    return $contents;
  }

  public function getStory(&$text) {
      $story = trim($this->getElementByName('TEXT', $text));
      $story = '<p>' . str_replace(".\r\n", ".\r\n" . "</p>\r\n<p>", $story) . '</p>';
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $story = preg_replace('/<img[^>]+\>/i', '', $story);
      return $story;
  }

  public function getHeadline(&$text) {
    return trim($this->getElementByName('HEADLINE', $text));
  }

  public function getArticleDate(&$text) {
    $date = trim($this->getElementByName('DATE', $text));
    return date('Y-m-d', strtotime($date));
  }

  public function getAuthor(&$text) {
    return trim($this->getElementByName('SOURCE', $text));
  }

}
