<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Core Communications  [# publisher id = 1494]
//Title      : Curaçao Chronicle [ English ] 
//Created on : Jan 24, 2021, 8:20:06 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7446 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}