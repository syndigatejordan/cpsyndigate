<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Searchlight Communications Incorporated  [# publisher id = 474]
//Title      : The New Dawn [ English ] 
//Created on : Oct 25, 2020, 7:46:34 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7018 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}