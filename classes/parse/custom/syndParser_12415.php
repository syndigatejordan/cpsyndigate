<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Gannett Co., Inc.  [# publisher id = 1763]
//Title      : Gannett [ English ] 
//Created on : Feb 15, 2022, 11:08:06 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12415 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}