<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Uzbekistan National News Agency (UzA)  [# publisher id = 74]
//Title      : Uzbekistan National News Agency (UzA) [ English ] 
//Created on : Aug 04, 2021, 1:55:22 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_217 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}