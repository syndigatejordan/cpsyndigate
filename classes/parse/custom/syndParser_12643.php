<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : KPICOM for Advertising, Media and Communication  [# publisher id = 1811]
//Title      : Chercell News [ Arabic ] 
//Created on : Jun 16, 2022, 12:22:04 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12643 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}