<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ANI Media Pvt Ltd  [# publisher id = 79]
//Title      : Asian News International (ANI) - Photo Service [ English ] 
//Created on : Apr 10, 2019, 12:09:03 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////


class syndParser_5478 extends syndParseXmlContent {

  public $author = "";

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    $this->loadCurrentDirectory();

    $this->addLog("getting raw articles text");
    return $this->getElementsByName('articles', $fileContents);
  }

  protected function getStory(&$text) {
    $this->author = $this->textFixation($this->getElementByName('author', $text));
    if (strtolower($this->author) == "reuters") {
      return "";
    }
    $this->addLog("getting article text");
    $story = $this->textFixation($this->getElementByName('Caption', $text));
    return $story;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return $this->textFixation($this->getElementByName('headline', $text));
  }

  protected function getAbstract(&$text) {
    $this->addLog("getting abstract");
    return $this->textFixation($this->getElementByName('summary', $text));
  }

  protected function getArticleDate(&$text) {

    $this->addLog("getting articles date");
    $date = $this->getElementByName('date', $text);
    $date = explode("/", $date);
    $date = "{$date[2]}-{$date[1]}-{$date[0]}";
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->author;
  }

  protected function getImages(&$text) {
    // this contain just one image [ video image ]
    $this->addLog("getting video image");
    $caption = $this->textFixation($this->getElementByName('Caption', $text));
    //$imageName = $this->currentDir . $this->videoName . '.jpg';
    $image_Name = $this->textFixation($this->getElementByName('imagename', $text));
    $image_full_path = $this->currentDir . $image_Name;
    $original_name = explode('.', $image_Name);
    if (file_exists($image_full_path)) {
      //the video name is same as the image name here  		
      $name = $this->model->getImgReplacement($image_Name, $this->currentDir, 5478);
      $img['img_name'] = str_replace(IMGS_PATH, "https://syndigateimages.s3.amazonaws.com/syndigate/imgs/", $name);
      $img['original_name'] = $original_name[0];
      $img['image_caption'] = $caption;
      $img['is_headline'] = true;
      $images[] = $img;
      //var_dump($images);exit;
      return $images;
    } else {

      return false;
    }
  }

  protected function getOriginalCategory(&$text) {
    return $this->textFixation($this->getElementByName('subcats', $text));
  }

}

?>
