<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Breaking Media, Inc.  [# publisher id = 531]
//Title      : Above The Law [ English ] 
//Created on : Sep 26, 2021, 10:01:07 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1576 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}