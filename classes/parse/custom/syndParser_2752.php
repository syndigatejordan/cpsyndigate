<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Nouvelles Imprimeries du Niger  [# publisher id = 953]
//Title      : Tamtaminfo [ French ] 
//Created on : Aug 03, 2021, 12:03:00 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2752 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}