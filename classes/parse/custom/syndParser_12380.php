<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Togo Times  [# publisher id = 1751]
//Title      : Togo Times [ French ] 
//Created on : Jan 30, 2022, 1:52:05 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12380 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}