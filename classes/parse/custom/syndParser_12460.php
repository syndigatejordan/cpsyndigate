<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The BioVoice  [# publisher id = 1765]
//Title      : BioVoice News [ English ] 
//Created on : Feb 20, 2022, 11:41:10 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12460 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}