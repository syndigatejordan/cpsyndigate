<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : NextDayNews S.A
//Title     : Le Soir Echos [ French ]
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1156 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('fr');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    return $this->textFixation($this->getCData($this->getElementByName('content:encoded', $text)));
  }

  protected function getImages(&$text) {
    $body = $this->textFixation($this->getCData($this->getElementByName('content:encoded', $text)));
    $images = $this->getElementsByName('a', $body);

    $imagesArray = array();
    foreach ($images as $img) {
      $this->addLog("getting article images");

      $imagePath = syndParseHelper::getImgElements($img, 'img');
      if (is_array($imagePath) && $imagePath) {
        $imagePath = $imagePath[0];
      }

      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        continue;
      }

      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      array_push($imagesArray, $images[0]);

      $text = str_replace($img, '', $text);
    }
    return $imagesArray;
  }

}