<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Alpha Comunicaciones Integrales  [# publisher id = 1485]
//Title      : Ancon News [ Spanish ] 
//Created on : Mar 27, 2022, 9:26:08 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7426 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}