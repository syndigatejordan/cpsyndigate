<?php

////////////////////////////////////////////////////////////////////////
// Publisher :  HT Media Ltd #89
// Title     :  Sri Lankan Government News
////////////////////////////////////////////////////////////////////////
	
class syndParser_676 extends syndParseHtMedia {
        private $date = '';

	protected function getRawArticles(&$fileContents) {			
		$releaseTime = syndParseHelper::getStringsBetween( 'ReleaseTime="', '" ', $fileContents);
		$date        = explode(' ', $releaseTime[0]);
		$this->date  = $date[0];		
	
		return parent::getRawArticles($fileContents);
	}

	protected function getArticleDate(&$text){
		$this->addLog("getting article date");	
              return $this->textFixation($this->date);
	}



}
