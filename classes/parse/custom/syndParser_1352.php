<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Tamimi & Company  [# publisher id = 415]
//Title      : Law Update [ English ] 
//Created on : Apr 29, 2018, 8:31:59 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1352 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}