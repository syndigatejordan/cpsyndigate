<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Somali National News Agency (SONA)  [# publisher id = 1380]
//Title      : Somali National News Agency (SONA) [ Somali ] 
//Created on : Sep 19, 2021, 8:55:24 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6486 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('so'); 
	} 
}