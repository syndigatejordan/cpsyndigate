<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The New York Times Company / nytLicensing  [# publisher id = 1197]
//Title      : T (The New York Times Style Magazine) [ English ] 
//Created on : Oct 19, 2018, 4:51:50 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3908 extends syndParseXmlContent {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $matchs = null;
    preg_match_all("/<NewsItem(.*?)<\/NewsItem>/is", $fileContents, $matchs);
    $matchs = $matchs[0];
    return $matchs;
  }

  public function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return trim($this->textFixation($this->getElementByName('HeadLine', $text)));
  }

  public function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = trim(preg_replace("/@(.*)/is", "", $this->getElementByName('DateLine', $text)));
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  protected function getAbstract(&$text) {
    $this->addLog('getting article summary');
    return trim($this->textFixation($this->getElementByName('SlugLine', $text)));
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $author = syndParseHelper::getImgElements($text, 'SubjectMatter', 'FormalName');
    if (isset($author[0])) {
      return trim($author[0]);
    } else {
      return "";
    }
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $category = syndParseHelper::getImgElements($text, 'NewsItemType', 'FormalName');
    return $category[0];
  }

  public function getArticleReference(&$text) {
    $this->addLog('getting article Link');
    $link = $this->getElementByName('MoreLink', $text);
    if (empty($link)) {
      return '';
    }
    return $link;
  }

  public function getArticleOriginalId($params = array()) {
    $articleOriginalId = $this->getElementByName('MoreLink', $params['text']);

    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }

    return $this->title->getId() . '_' . sha1($articleOriginalId);
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = preg_replace('/<hl1(.*?)<\/hl1>/is', '', $this->story);
    $this->story = preg_replace('/<html(.*?)">/is', '', $this->story);
    $this->story = preg_replace('/<!--(.*?)-->/is', '', $this->story);
    $this->story = preg_replace('/<head>(.*?)<\/head>/is', '', $this->story);
    $this->story = preg_replace('/<h1(.*?)<\/h1>/is', '', $this->story);
    $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) width=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) height=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) align=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
    $this->story = preg_replace("/<media media-type=\"image\">(.*?)<\/media>/is", "", $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    $this->story = str_replace('" "', '"', $this->story);
    $this->story = str_replace('<body>', '', $this->story);
    $this->story = str_replace('</body>', '', $this->story);
    $this->story = trim(str_replace('</html>', '', $this->story));
    $this->story = trim(preg_replace('!\s+!', ' ', $this->story));
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $matches = null;
    preg_match_all("/<media media-type=\"image\">(.*?)<\/media>/is", $text, $matches);
    foreach ($matches[0] as $match) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = $this->textFixation($this->getCData($this->getElementByName('media-caption', $match)));
      $enclosure = null;
      preg_match("/<media-reference[^>]+\>/is", $match, $enclosure);

      $imageInfo = syndParseHelper::getImgElements($enclosure[0], 'media-reference', 'source');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no path" . PHP_EOL;
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    $this->story = $this->textFixation($this->getCData($this->getElementByName('DataContent', $text)));
    $imgs = array();
    preg_match_all("/<img[^>]+\>/i", $this->story, $imgs);

    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
      $image_caption = $image_caption[0];
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      $oldimagePath = $imagePath;
      $imagePath = explode('?', $imagePath);
      if (!empty($imagePath[0])) {
        $imagePath = $imagePath[0];
      } else {

        $imagePath = $imageInfo[0];
      }
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no path" . PHP_EOL;
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($oldimagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  protected function getVideos(&$text) {
    $videos = array();
    $matches = null;
    preg_match_all("/<media media-type=\"video\">(.*?)<\/media>/is", $text, $matches);
    foreach ($matches[1] as $match) {
      $this->addLog("Getting videos");
      $video = array();
      $videoname = syndParseHelper::getImgElements($match, 'media-reference', 'source');
      $videoname = $videoname[0];
      $mimeType = syndParseHelper::getImgElements($match, 'media-reference', 'mime-type');
      $mimeType = $mimeType[0];
      $video_caption = $this->textFixation($this->getCData($this->getElementByName('media-caption', $match)));
      $date = trim(preg_replace("/@(.*)/is", "", $this->getElementByName('DateLine', $text)));
      $date = date('Y-m-d h:i:s', strtotime($date));

      $video['video_name'] = $videoname;
      $video['original_name'] = $video_caption;
      $video['video_caption'] = $video_caption;
      $video['mime_type'] = $mimeType;
      $video['added_time'] = $date;
      $videos[] = $video;
    }
    return $videos;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = basename($imageUrl);
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $opts = array();
      $http_headers = array();
      $http_headers[] = 'Expect:';

      $opts[CURLOPT_URL] = $imageUrl;
      $opts[CURLOPT_HTTPHEADER] = $http_headers;
      $opts[CURLOPT_CONNECTTIMEOUT] = 10;
      $opts[CURLOPT_TIMEOUT] = 60;
      $opts[CURLOPT_HEADER] = FALSE;
      $opts[CURLOPT_BINARYTRANSFER] = TRUE;
      $opts[CURLOPT_VERBOSE] = FALSE;
      $opts[CURLOPT_SSL_VERIFYPEER] = FALSE;
      $opts[CURLOPT_SSL_VERIFYHOST] = 2;
      $opts[CURLOPT_RETURNTRANSFER] = TRUE;
      $opts[CURLOPT_FOLLOWLOCATION] = TRUE;
      $opts[CURLOPT_MAXREDIRS] = 2;
      $opts[CURLOPT_IPRESOLVE] = CURL_IPRESOLVE_V4;

      # Initialize PHP/CURL handle
      $ch = curl_init();
      curl_setopt_array($ch, $opts);
      $imgContent = curl_exec($ch);

      # Close PHP/CURL handle
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        unlink($copiedImage);
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
