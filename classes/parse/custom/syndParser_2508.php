<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Assafir  [# publisher id = 29]
//Title      : As-Safir [ French ] 
//Created on : Feb 15, 2016, 14:30:47 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2508 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('fr');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $d = $this->getElementsByName('item', $fileContents);
    return $d;
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $body = html_entity_decode(trim($this->getElementByName('description', $text)));
    $body = strip_tags($body, '<p><br><strong><b><u><i>');
    if (empty($body)) {
      return '';
    }
    return $body;
  }

  public function getHeadline(&$text) {
    $head = trim($this->getElementByName('title', $text));
    return $head;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $author = trim($this->getElementByName('a10:author', $text));
    $author = trim($this->getElementByName('a10:name', $author));
    return $author;
  }

  public function getArticleDate(&$text) {
    $date = trim($this->getElementByName('pubDate', $text));
    $date = preg_replace('/ \+(.*)/is', '', $date);
    return date('Y-m-d', strtotime($date));
  }

  public function getArticleReference(&$text) {
    preg_match('/link href="(.*?)\/RssFeed/si', $text, $link);
    $link = $link[1];
    return $link;
  }

  protected function getImages(&$text) {
    $this->imagesArray = array();
    preg_match_all('/<enclosure url="(.*?)"/si', $text, $imgs);

    foreach ($imgs[1] as $img) {
      $this->addLog("getting article images");
      $imagePath = '';
      $imagePath = $img;
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $name_image = explode('/images/', $copiedImage);
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if ($images[0]['image_caption'] == $name_image[1])
        $images[0]['image_caption'] = '';
      $images[0]['is_headline'] = false;
      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

}
