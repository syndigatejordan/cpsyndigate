<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IRIB World Service  [# publisher id = 799]
//Title      : ParsToday [ Armenian ] 
//Created on : Oct 22, 2020, 12:31:51 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6572 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ARM'); 
	} 
}