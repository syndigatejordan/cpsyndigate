<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Industry Dive  [# publisher id = 1740]
//Title      : Healthcare [ English ] 
//Created on : Jan 27, 2022, 8:55:28 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12365 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}