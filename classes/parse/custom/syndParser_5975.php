<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : HT Media Ltd.  [# publisher id = 89]
//Title      : Livehindustan [ Hindi ] 
//Created on : Apr 02, 2020, 7:45:33 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5975 extends syndParseHtMedia {
   public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('hi');
  }
}