<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : News Agency of Nigeria  [# publisher id = 1046]
//Title      : News Agency of Nigeria [ English ] 
//Created on : Sep 19, 2021, 7:05:06 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2895 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}