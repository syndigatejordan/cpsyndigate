<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : SARL Réflexion  [# publisher id = 1530]
//Title      : Reflexion [ French ] 
//Created on : Mar 02, 2021, 1:43:13 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10124 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}