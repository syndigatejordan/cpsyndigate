<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Africa News Agency (ANA)  [# publisher id = 1432]
//Title      : Africa News Agency (ANA) [ Arabic ] 
//Created on : Oct 05, 2020, 11:21:10 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6694 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}