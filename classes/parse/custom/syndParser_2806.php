<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Okaz Digital  [# publisher id = 986]
//Title      : 3alyoum.com [ Arabic ] 
//Created on : Aug 10, 2016, 7:00:09 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2806 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $this->story = str_replace(']]>', '', $this->story);
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace("/(<[^>]+) style='.*?'/i", '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) class=(.*?)>/i', '$1>', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    return $this->story;
  }

  protected function getImages(&$text) {
    $this->story = $this->getCData($this->getElementByName('fulltext', $text));
    $this->story = str_replace('&nbsp;', ' ', $this->story);
    $this->story = str_replace('onerror="this.src=\'images/no-image.png\'"', '', $this->story);
    $this->story = $this->textFixation($this->story);
    $imagesArray = array();
    preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
      if (!empty($image_caption[0])) {
        $image_caption = $image_caption[0];
      } else {
        $image_caption = "";
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      $imagePath = preg_replace("/\?(.*)/is", "", $imagePath);
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
