<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Buzz Publishing  [# publisher id = 1677]
//Title      : Money Buzz [ Romanian ] 
//Created on : Feb 24, 2022, 3:15:47 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12485 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ro'); 
	} 
}