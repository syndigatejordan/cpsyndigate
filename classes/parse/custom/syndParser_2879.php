<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Zimbabwe Newspapers (1980) Limited  [# publisher id = 958]
//Title      : The Sunday Mail [ English ] 
//Created on : Oct 19, 2021, 6:43:49 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2879 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}