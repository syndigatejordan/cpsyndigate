<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Eurasianet  [# publisher id = 1281]
//Title      : Eurasianet [ English ] 
//Created on : Dec 11, 2019, 1:41:37 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5885 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
