<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Southern African - German Chamber of Commerce and Industry  [# publisher id = 828]
//Title      : Southern African-German Chamber of Commerce and Industry (SAGCCI) - News [ English ] 
//Created on : Feb 02, 2016, 12:11:53 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2550 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}