<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : SourceMedia, LLC d/b/a Arizent  [# publisher id = 1338]
//Title      : Money Management Executive [ English ] 
//Created on : Jun 18, 2020, 7:06:27 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6059 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}