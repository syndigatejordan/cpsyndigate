<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : CountryWatch Inc.  [# publisher id =549 ] 
//Title     : CountryWatch [ English ]
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_1607 extends syndParseXmlContent {

  public $countryISO = array('Afghanistan' => 'AFG', 'Albania' => 'ALB', 'Algeria' => 'DZA', 'Andorra' => 'AND', 'Angola' => 'AGO', 'Antigua' => 'ATG', 'Argentina' => 'ARG', 'Armenia' => 'ARM', 'Australia' => 'AUS', 'Austria' => 'AUT', 'Azerbaijan' => 'AZE', 'Bahamas' => 'BHS', 'Bahrain' => 'BHR', 'Bangladesh' => 'BGD', 'Barbados' => 'BDS', 'Belarus' => 'BLR'
      , 'Belgium' => 'BEL', 'Belize' => 'BLZ', 'Benin' => 'BEN', 'Bhutan' => 'BTN', 'Bolivia' => 'BOL', 'Bosnia-Herzegovina' => 'BIH', 'Botswana' => 'BWA'
      , 'Brazil' => 'BRA', 'Brunei' => 'BRN', 'Bulgaria' => 'BGR', 'Burkina-Faso' => 'BFA', 'Burma' => 'MMR', 'Burundi' => 'BDI', 'Cambodia' => 'KHM'
      , 'Cameroon' => 'CMR', 'Canada' => 'CAN', 'Cape-Verde' => 'CPV', 'Central-African-Republic' => 'CAF', 'Chad' => 'TCD', 'Chile' => 'CHL', 'China' => 'CHN'
      , 'China-Hong-Kong' => 'HKG', 'China-Taiwan' => 'TWN', 'Colombia' => 'COL', 'Comoros' => 'COM', 'Congo-DRC' => 'COD', 'Congo-RC' => 'COG', 'Costa-Rica' => 'CRI'
      , 'Cote-d-Ivoire' => 'CIV', 'Croatia' => 'HRV', 'Cuba' => 'CUB', 'Cyprus' => 'CYP', 'Czech-Republic' => 'CZE', 'Denmark' => 'DNK', 'Djibouti' => 'DJI'
      , 'Dominica' => 'DMA', 'Dominican-Republic' => 'DOM', 'East-Timor' => 'TLS', 'Ecuador' => 'ECU', 'Egypt' => 'EGY', 'El-Salvador' => 'SLV', 'Equatorial-Guinea' => 'GNQ'
      , 'Eritrea' => 'ERI', 'Estonia' => 'EST', 'Ethiopia' => 'ETH', 'Fiji' => 'FJI', 'Finland' => 'FIN', 'Former-Yugoslav-Rep-of-Macedonia' => 'MKD', 'France' => 'FRA'
      , 'Gabon' => 'GAB', 'Gambia' => 'GMB', 'Georgia' => 'GEO', 'Germany' => 'DEU', 'Ghana' => 'GHA', 'Greece' => 'GRC', 'Grenada' => 'GRD', 'Guatemala' => 'GTM', 'Guinea' => 'GIN'
      , 'Guinea-Bissau' => 'GNB', 'Guyana' => 'GUY', 'Haiti' => 'HTI', 'Holy-See' => 'VAT', 'Honduras' => 'HND', 'Hungary' => 'HUN', 'Iceland' => 'ISL', 'India' => 'IND'
      , 'Indonesia' => 'IDN', 'Iran' => 'IRN', 'Iraq' => 'IRQ', 'Ireland' => 'IRL', 'Israel' => 'ISR', 'Italy' => 'ITA', 'Jamaica' => 'JAM', 'Japan' => 'JPN', 'Jordan' => 'JOR'
      , 'Kazakhstan' => 'KAZ', 'Kenya' => 'KEN', 'Kiribati' => 'KIR', 'Korea-North' => 'PRK', 'Korea-South' => 'KOR', 'Kuwait' => 'KWT', 'Kyrgyzstan' => 'KGZ', 'Laos' => 'LAO', 'Latvia' => 'LVA'
      , 'Lebanon' => 'LBN', 'Lesotho' => 'LSO', 'Liberia' => 'LBR', 'Libya' => 'LBY', 'Liechtenstein' => 'LIE', 'Lithuania' => 'LTU', 'Luxembourg' => 'LUX', 'Madagascar' => 'MDG'
      , 'Malawi' => 'MWI', 'Malaysia' => 'MYS', 'Maldives' => 'MDV', 'Mali' => 'MLI', 'Malta' => 'MLT', 'Malta' => 'MLT', 'Marshall-Islands' => 'MHL', 'Mauritania' => 'MRT', 'Mauritius' => 'MUS'
      , 'Mexico' => 'MEX', 'Micronesia' => 'FSM', 'Moldova' => 'MDA', 'Monaco' => 'MCO', 'Mongolia' => 'MNG', 'Morocco' => 'MAR', 'Mozambique' => 'MOZ', 'Namibia' => 'NAM', 'Nauru' => 'NRU'
      , 'Nepal' => 'NPL', 'Netherlands' => 'NLD', 'New-Zealand' => 'NZL', 'Nicaragua' => 'NIC', 'Niger' => 'NER', 'Nigeria' => 'NGA', 'Norway' => 'NOR', 'Oman' => 'OMN'
      , 'Pakistan' => 'PAK', 'Palau' => 'PLW', 'Panama' => 'PAN', 'Papua-New-Guinea' => 'PNG', 'Paraguay' => 'PRY', 'Peru' => 'PER', 'Philippines' => 'PHL', 'Poland' => 'POL'
      , 'Portugal' => 'PRT', 'Qatar' => 'QAT', 'Romania' => 'ROU', 'Russia' => 'RUS', 'Rwanda' => 'RWA', 'Saint-Kitts-and-Nevis' => 'KNA', 'Saint-Lucia' => 'LCA'
      , 'Saint-Vincent-and-Grenadines' => 'VCT', 'Samoa' => 'WSM', 'San-Marino' => 'SMR', 'Sao-Tome-and-Principe' => 'STP', 'Saudi-Arabia' => 'SAU', 'Senegal' => 'SEN'
      , 'Serbia' => 'SRB', 'Seychelles' => 'SYC', 'Sierra-Leone' => 'SLE', 'Singapore' => 'SGP', 'Slovakia' => 'SVK', 'Slovenia' => 'SVN', 'Solomon-Islands' => 'SLB'
      , 'Somalia' => 'SOM', 'South-Africa' => 'ZAF', 'Spain' => 'ESP', 'Sri-Lanka' => 'LKA', 'Sudan' => 'SDN', 'Suriname' => 'SUR', 'Swaziland' => 'SWZ', 'Sweden' => 'SWE'
      , 'Switzerland' => 'CHE', 'Syria' => 'SYR', 'Tajikistan' => 'TJK', 'Tanzania' => 'TZA', 'Thailand' => 'THA', 'Togo' => 'TGO', 'Tonga' => 'TON', 'Trinidad-Tobago' => 'TTO'
      , 'Tunisia' => 'TUN', 'Turkey' => 'TUR', 'Turkmenistan' => 'TKM', 'Tuvalu' => 'TUV', 'Uganda' => 'UGA', 'Ukraine' => 'UKR', 'United-Arab-Emirates' => 'ARE'
      , 'United-Kingdom' => 'GBR', 'United-States' => 'USA', 'Uruguay' => 'URY', 'Uzbekistan' => 'UZB', 'Vanuatu' => 'VUT', 'Venezuela' => 'VEN', 'Vietnam' => 'VNM', 'Yemen' => 'YEM', 'Zambia' => 'ZMB', 'Zimbabwe' => 'ZWE');
  public $contant;
  public $tag;
  public $imagesArray = array();

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $filename = basename($this->currentlyParsedFile);
    if (strpos($filename, 'InternationalNews') !== false) {
      $this->contant = "News";
    } else {
      $this->contant = "Country";
    }
    $this->addLog("getting articles raw text");
    if ($this->contant == "News") {
      return $this->getElementsByName('ResponseArticle', $fileContents);
    } else {
      return $this->getElementsByName('subCountryWatch', $fileContents);
    }
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    if ($this->contant == "News")
      $headline = trim($this->getCData($this->getElementByName('articleTitle', $text)));
    else {
      $headline = trim($this->getCData($this->getElementByName('category', $text)));
      $headline = explode(",", $headline);
      $headline = $headline[1];
      $filename = basename($this->currentlyParsedFile);
      $filename = str_replace('.xml', '', $filename);
      $filename = array_search($filename, $this->countryISO);
      $headline = $headline . " " . $filename;
    }
    var_dump($headline);
    return $headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    if ($this->contant == "News")
      $date = date('Y-m-d', strtotime($this->getElementByName('articleDate', $text)));
    else
      $date = date('Y-m-d');
    return $date;
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    if ($this->contant == "News") {
      $this->story = $this->getCData($this->getElementByName('articleText', $text));
    } else {
      $this->story = $this->getCData($this->getElementByName('text', $text));
//      if ($this->tag == "Geography") {
//        $this->story = "Geography : </br>";
//        $this->story .=$this->getElementByName('Geography', $text);
//      } else if ($this->tag == "History") {
//        $this->story = "History : </br>";
//        $this->story .=$this->getElementByName('History', $text);
//      } else if ($this->tag == "Politics") {
//        $Politics = $this->getElementByName('Politics', $text);
//        $this->story ="Politics : </br>";
//        $this->story .="   Political Conditions : </br>";
//        $this->story .=$this->getElementByName('PoliticalConditions', $Politics);
//        $this->story .="   Human Rights : </br>";
//        $this->story .=$this->getElementByName('HumanRights', $Politics);
//        $this->story .="   Leader Biography : </br>";
//        $this->story .=$this->getElementByName('LeaderBiography', $Politics);
//        $this->story .="   National Security : </br>";
//        $this->story .=$this->getElementByName('NationalSecurity', $Politics);
//        $this->story .="   Defense Forces : </br>";
//        $this->story .=$this->getElementByName('DefenseForces', $Politics);
//      } else if ($this->tag == "Government") {
//        $Government = $this->getElementByName('Government', $text);
//        $this->story ="Government : </br>";
//        $this->story .="   Government Effectiveness : </br>";
//        $this->story .=$this->getElementByName('GovernmentEffectiveness', $Government);
//        $this->story .="   Government Functions : </br>";
//        $this->story .=$this->getElementByName('GovtFunctions', $Government);
//        $this->story .="   Government Structure : </br>";
//        $this->story .=$this->getElementByName('GovtStructure', $Government);
//        $this->story .="   Principal Government Officials : </br>";
//        $this->story .=$this->getElementByName('PrincipalGovtOfficials', $Government);
//      } else if ($this->tag == "ForeignPolicy") {
//        $this->story = "Foreign Relations : </br>";
//        $this->story .=$this->getElementByName('ForeignPolicy', $text);
//      } else if ($this->tag == "Culture") {
//        $Culture = $this->getElementByName('Culture', $text);
//        $this->story = "Culture : </br>";
//        $this->story .= "  Culture and Arts : </br>";
//        $this->story .=$this->getElementByName('CultureandArts', $Culture);
//        $this->story .= "  Cultural Etiquette : </br>";
//        $this->story .=$this->getElementByName('CulturalEtiquette', $Culture);
//      } else if ($this->tag == "Economy") {
//        $Economy = $this->getElementByName('Economy', $text);
//        $this->story = "Economy : </br>";
//        $this->story .= "  Economic Conditions : </br>";
//        $this->story .=$this->getElementByName('EconomicConditions', $Economy);
//        $this->story .= "  Economic Conditions : </br>";
//        $this->story .=$this->getElementByName('MacroeconomicForecast', $Economy);
//      } else if ($this->tag == "Business") {
//        $Business = $this->getElementByName('Business', $text);
//        $this->story = "Business : </br>";
//        $this->story .= "  Investment Climate : </br>";
//        $this->story .=$this->getElementByName('InvestmentClimate', $Business);
//        $this->story .= "  Taxation : </br>";
//        $this->story .=$this->getElementByName('Taxation', $Business);
//      } else if ($this->tag == "MarketResearch") {
//        $MarketResearch = $this->getElementByName('MarketResearch', $text);
//        $this->story ="Market Research : </br>";
//        $this->story .="   Macroeconomic Data : </br>";
//        $this->story .=$this->getElementByName('MacroeconomicData', $MarketResearch);
//        $this->story .="   Energy Data : </br>";
//        $this->story .=$this->getElementByName('EnergyData', $MarketResearch);
//        $this->story .="   Metals Data : </br>";
//        $this->story .=$this->getElementByName('MetalsData', $MarketResearch);
//        $this->story .="   Agriculture Data : </br>";
//        $this->story .=$this->getElementByName('AgricultureData', $MarketResearch);
//      }
    }
    $this->story = html_entity_decode($this->story, ENT_COMPAT, 'UTF-8');
    return $this->story;
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    if ($this->contant == "News")
      $cats = "News";
    else {
      $cats = trim($this->getCData($this->getElementByName('category', $text)));
      $cats = explode(",", $cats);
      $cats = $cats[0];
    }
    return $cats;
  }

}