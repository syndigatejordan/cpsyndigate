<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Newstex, LLC  [# publisher id = 1519]
//Title      : ALJAZEERA [ Arabic ] 
//Created on : Feb 23, 2022, 8:29:22 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12476 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}