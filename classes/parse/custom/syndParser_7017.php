<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Searchlight Communications Incorporated  [# publisher id = 474]
//Title      : The New Dawn [ English ] 
//Created on : Sep 19, 2021, 8:57:58 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7017 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}