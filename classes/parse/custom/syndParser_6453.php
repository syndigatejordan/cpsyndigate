<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence Tawary pour l'Informations  [# publisher id = 1366]
//Title      : Agence Tawary pour l'Informations [ Arabic ] 
//Created on : Sep 17, 2020, 11:54:48 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6453 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}