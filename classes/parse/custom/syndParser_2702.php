<?php
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dar Al Arab Est.  [# publisher id = 909]
//Title      : Al Arab [ Arabic ] 
//Created on : May 16, 2016, 10:47:51 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2702 extends syndParseCms
{
    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('ar');
    }
}
