<?php 
	///////////////////////////////////////////////////////////////////////////////////// 
	//Publisher : I Media LLC   
	//Title     : Alrroya.com [ Arabic ] 
	/////////////////////////////////////////////////////////////////////////////////////
		
	class syndParser_736 extends syndParseRss {
	
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('ar');
		}	
		
		protected function getArticleDate(&$text) {
			$this->addLog("getting article date");
			$date = $this->getElementByName('publicationdate', $text);
			if($date) {
				return $this->dateFormater($date);
			}
			return parent::getArticleDate($text);
		}
		
	} // End class

	