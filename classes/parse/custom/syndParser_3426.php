<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Nile Radio Productions  [# publisher id = 1165]
//Title      : Nogoum FM [ Arabic ] 
//Created on : Sep 19, 2021, 8:51:17 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3426 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}