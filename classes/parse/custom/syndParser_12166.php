<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ndjamena actu.com  [# publisher id = 1622]
//Title      : N'Djaména Actu [ French ] 
//Created on : Sep 05, 2021, 12:13:43 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12166 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}