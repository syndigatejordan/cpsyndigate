<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Khaama Press News Agency  [# publisher id = 1667]
//Title      : The Khaama Press News Agency [ Pashto ] 
//Created on : Oct 10, 2021, 10:19:12 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12230 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ps'); 
	} 
}