<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Are We Europe Foundation  [# publisher id = 1520]
//Title      : Are We Europe [ English ] 
//Created on : Feb 24, 2021, 7:13:58 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_9503 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}