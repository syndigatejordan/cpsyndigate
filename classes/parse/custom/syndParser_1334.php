<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Contify  [# publisher id =394 ]
// Titles    : India Trademark News [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1334 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");

    $author = $this->getElementByName('dc:creator', $text);
    return $this->textFixation($author);
  }

  protected function getOriginalCategory(&$text) {
    $this->addLog("getting article original category");

    return 'India Trademark';
  }

  public function getStory(&$text) {
    //$story = parent::getStory($text);
    $story = preg_replace("/<img[^>]+\>/i", "", $text);
    $story = str_replace('<p></p>', '', $story);
    return $story;
  }

  protected function getImages(&$text) {
    $story = parent::getStory($text);

    $imagesArray = array();
    preg_match_all("/<img[^>]+\>/i", $story, $imgs);

    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");

      $imageInfo = syndParseHelper::getImgElements($img);
      $imagePath = $imageInfo[0];

      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        continue;
      }

      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}