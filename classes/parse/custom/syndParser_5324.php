<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ​The Egyptian Center for Economic Studies (ECES)  [# publisher id = 1203]
//Title      : ECES Reform Bulletin [ Arabic ] 
//Created on : Jan 03, 2019, 9:12:47 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5324 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}