<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BANG Media International  [# publisher id = 306]
//Title      : BANG Showbiz - Tech [ English ] 
//Created on : Sep 13, 2021, 1:38:50 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6337 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}