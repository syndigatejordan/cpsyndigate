<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Exchange Data International (EDI)  [# publisher id = 174]
//Title      : African Financial & Economic Data (AFED) - Data Hub [ English ] 
//Created on : Apr 09, 2020, 7:29:50 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6014 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}