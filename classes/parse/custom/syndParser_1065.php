<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Emerging Markets Direct Media Holdings LLC
// Titles    : IntelliNews - Weekly Reports [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1065 extends syndParseXmlContent {
	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('en');
		$this -> extensionFilter = 'xml';
	}

	protected function getRawArticles(&$fileContents) {
		$this -> addLog("getting articles raw text");

		return $this -> getElementsByName('article', $fileContents);
	}

	protected function getStory(&$text) {
		$this -> addLog("getting article text");

		$body = $this -> getCData($this -> getElementByName('body', $text));
		//$body = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $body);
		//$body = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $body);
		//$body = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $body);
		//$body = preg_replace('/<IMG SRC=".*?">/i', '', $body);
		return $body;
	}

	protected function getHeadline(&$text) {
		$this -> addLog("getting article headline");

		$headline = $this -> getCData($this -> getElementByName('title', $text));
		return $headline;
	}

	protected function getArticleDate(&$text) {
		$this -> addLog("getting article date");

		$date = $this -> getElementByName('published', $text);
		$date = date('Y-m-d', strtotime($date));
		return $date;
	}

	public function getOriginalCategory(&$text) {
		$this -> addLog('getting article category');

		$cats = $this -> getElementsByName('subject', $text);
		$originalCats = array();

		if (!empty($cats)) {
			foreach ($cats as $cat) {
				if (empty($cat)) {
					continue;
				}
				$originalCats[] = $this -> textFixation($cat);
			}
		}
		return implode(', ', $originalCats);
	}

	public function getArticleOriginalId($params = array()) {
		$defaultArray = array('text' => '', 'headline' => '', 'articleDate' => '', 'issueNum' => 0);
		$params = array_merge($defaultArray, $params);

		$articleOriginalId = trim($this -> getElementByName('doc_id', $params['text']));
		$articleOriginalId = trim($this -> getCData($articleOriginalId));

		if (!$articleOriginalId) {
			return parent::getArticleOriginalId($params);
		} else {
			return $this -> title -> getId() . "_" . sha1($articleOriginalId) . "_" . $articleOriginalId;
		}
	}

	protected function getImages(&$text) {
		$this -> addLog("getting article images");

		//echo PHP_EOL . $this -> currentlyParsedFile . PHP_EOL;
		//echo PHP_EOL . PHP_EOL ; print_r($text); echo PHP_EOL . PHP_EOL;
		//exit;
		$images = $this -> getElementsByName('images', $text);
		if (empty($images)) {
			return array();
		}

		$finalImages = array();
		foreach ($images as $image) {
			$image = $this -> getElementByName('image', $image);

			$image = trim($image);
			if (empty($image)) {
				return array();
			}

			if (file_exists($this -> currentDir . $image)) {
				return $this -> getAndCopyImagesFromArray(array($this -> currentDir . $image));
			}
			return array();
		}
	}

}
