<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Response Global Media Limited  [# publisher id = 1739]
//Title      : Responsible Investor (sections) [ English ] 
//Created on : Jan 16, 2022, 1:05:30 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12342 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}