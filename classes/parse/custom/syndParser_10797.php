<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : SolMedios  [# publisher id = 1561]
//Title      : Sol Noticias [ Spanish ] 
//Created on : Sep 19, 2021, 10:25:00 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10797 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}