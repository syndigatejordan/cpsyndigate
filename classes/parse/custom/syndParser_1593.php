<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : MediaToday Co. Ltd.  [# publisher id = 541]
//Title      : Business Today [ English ] 
//Created on : Mar 18, 2022, 1:26:04 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1593 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}