<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Cointelegraph  [# publisher id = 1767]
//Title      : Cointelegraph [ simplified Chinese ] 
//Created on : Feb 21, 2022, 11:15:34 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12467 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh-Hans'); 
	} 
}