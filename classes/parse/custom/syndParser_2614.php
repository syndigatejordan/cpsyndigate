<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Mongolian National Chamber of Commerce and Industry  [# publisher id = 854]
//Title      : Mongolian National Chamber of Commerce and Industry (MNCCI) - Newsletter [ English ] 
//Created on : Feb 17, 2016, 8:25:01 AM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2614 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}