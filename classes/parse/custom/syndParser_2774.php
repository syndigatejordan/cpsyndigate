<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Jazirah Corporation Press, Printing & Publishing  [# publisher id = 972]
//Title      : Al Jazirah [ Arabic ] 
//Created on : May 24, 2016, 7:26:39 AM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2774 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }
}
