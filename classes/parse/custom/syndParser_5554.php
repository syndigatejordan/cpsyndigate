<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ech-Chorouk Infed SARL  [# publisher id = 1135]
//Title      : Ech-Chorouk Al Riyadi [ Arabic ] 
//Created on : Jul 06, 2021, 9:40:33 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5554 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}