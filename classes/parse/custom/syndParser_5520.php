<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Luxuria Lifestyle International Ltd.  [# publisher id = 1225]
//Title      : Luxuria Lifestyle [ English ] 
//Created on : Apr 08, 2019, 11:46:49 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5520 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
