<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Swedish Southern African Chamber of Commerce (SSACC)  [# publisher id = 832]
//Title      : Swedish Southern African Chamber of Commerce (SSACC) - News [ English ] 
//Created on : Aug 03, 2021, 10:46:55 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2556 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}