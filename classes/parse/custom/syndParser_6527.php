<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The United Nations Department of Global Communications  [# publisher id = 1400]
//Title      : UN News [ Portuguese ] 
//Created on : Oct 20, 2020, 1:42:19 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6527 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}