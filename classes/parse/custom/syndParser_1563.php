<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : FairWarning Inc.  [# publisher id = 524]
//Title      : FairWarning [ English ] 
//Created on : Sep 19, 2021, 7:01:52 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1563 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}