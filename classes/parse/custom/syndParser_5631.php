<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Seychelles News Agency  [# publisher id = 1248]
//Title      : Seychelles News Agency [ French ] 
//Created on : Sep 23, 2021, 7:59:40 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5631 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}