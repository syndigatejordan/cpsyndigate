<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Abu Dhabi Securities Exchange (ADX)  [# publisher id = 594]
//Title      : Abu Dhabi Securities Exchange (ADX) [ Arabic ] 
//Created on : Feb 02, 2016, 11:54:25 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1681 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}