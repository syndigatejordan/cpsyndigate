<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Observateur-Guinee   [# publisher id = 628]
//Title      : L'Observateur [ French ] 
//Created on : Feb 02, 2016, 11:53:06 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1798 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}