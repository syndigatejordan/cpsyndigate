<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ukrinform  [# publisher id = 1337]
//Title      : Ukrinform [ Russian ] 
//Created on : May 19, 2020, 8:09:48 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_6035 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ru');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('nitf', $fileContents);
    return $articles;
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $body = trim($this->getElementByName('body.content', $text));
    $this->story = $this->textFixation($this->getElementByName('body.content', $text));
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  public function getHeadline(&$text) {
    $head = trim($this->getElementByName('body.head', $text));
    $headline = trim(strip_tags($this->getElementByName('hedline', $head)));
    return $headline;
  }

  public function getArticleDate(&$text) {
    $head = trim($this->getElementByName('head', $text));
    $dateline = trim($this->getElementByName('docdata', $head));
    $dateInfo = syndParseHelper::getImgElements($dateline, 'date.issue', 'norm');
    $date = preg_replace('/\+(.*)/is', '', $dateInfo[0]);
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  protected function getAbstract(&$text) {
    $this->addLog('getting article summary');
    $head = trim($this->getElementByName('body.head', $text));
    $summary = trim($this->textFixation($this->getElementByName('abstract', $head)));
    return $summary;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $head = trim($this->getElementByName('body.head', $text));
    return trim($this->textFixation($this->getElementByName('byline', $head)));
  }

  protected function getImages(&$text) {
    $story = trim($this->textFixation($this->getCData($this->getElementByName('body.content', $text))));
    $imagesArray = array();
    preg_match_all("/<media-reference[^>]+\>/i", $story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");

      $imageInfo = syndParseHelper::getImgElements($img, 'media-reference', 'source');
      $imagePath = $imageInfo[0];

      if (!$imagePath) {
        continue;
      }
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['image_caption'] = $images[0]['image_caption'];
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
