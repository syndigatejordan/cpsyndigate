<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Carol M. Highsmith  [# publisher id = 1307]
//Title      : Carol M. Highsmith [ English ] 
//Created on : Feb 20, 2020, 2:14:47 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5928 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
