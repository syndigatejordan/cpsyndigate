<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Islamic Republic News Agency  [# publisher id = 799]
//Title      : ParsToday [ German ] 
//Created on : Jul 21, 2016, 12:52:02 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2790 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('de'); 
	} 
}