<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Motivate publishing  [# publisher id = 116]
//Title      : Golf Digest Middle East [ English ] 
//Created on : Jan 04, 2018, 9:47:04 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3101 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}