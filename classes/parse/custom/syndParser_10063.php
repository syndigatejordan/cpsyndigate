<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Corporacion Day Group S.A.  [# publisher id = 1528]
//Title      : The Costa Rica News [ English ] 
//Created on : Sep 19, 2021, 10:22:27 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10063 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}