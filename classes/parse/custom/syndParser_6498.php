<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agencia Informativa Latinoamericana Prensa Latina  [# publisher id = 1385]
//Title      : Prensa Latina [ Italian ] 
//Created on : Oct 19, 2020, 12:27:59 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6498 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('it'); 
	} 
}