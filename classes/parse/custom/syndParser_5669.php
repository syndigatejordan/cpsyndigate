<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : RE Media   [# publisher id =1254 ] 
//Title      : Kidzy [ Arabic ] 
//Created on : Jul 15, 2019, 7:21:13 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5669 extends syndParseRss {

  public $category;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $matches = null;
    preg_match_all("/<item><kind>(.*?)<\/commentCount><\/statistics><\/item>/is", $fileContents, $matches);
    return $matches[0];
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return trim($this->textFixation($this->getCData($this->getElementByName('title', $text))));
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");

    $date = trim(preg_replace('/T(.*)/is', '', $this->getCData($this->getElementByName('publishedAt', $text))));
    return date('Y-m-d', strtotime($date));
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $this->story = preg_replace('!\s+!', ' ', $this->textFixation($this->getCData($this->getElementByName('description', $text))));
    if (!empty($this->category)) {
      $this->story .="<p><strong>keywords </strong> $this->category</p>";
    }
    $videoUrl = trim($this->textFixation($this->getCData($this->getElementByName('id', $text))));
    $this->story .="<p><iframe width=\"600\" height=\"400\" src=\"https://www.youtube.com/embed/$videoUrl\"></iframe></p>";
    return $this->story;
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $this->category = "";
    $tag = null;
    if (preg_match("/<tags>(.*?)<\/tags>/is", $text, $tag)) {
     
      $cats = $this->getElementsByName('item', $tag[1]);
    
      $originalCats = array();

      if (!empty($cats)) {
        foreach ($cats as $cat) {
          $originalCats[] = $this->textFixation($this->getCData($cat));
        }
      }
      $this->category = implode(', ', $originalCats);
      return $this->category;
    } else {
      return "";
    }
  }


  protected function getImages(&$text) {
    $imagesArray = array();
    $this->addLog("getting article main images");
    $imagePath = trim($this->textFixation($this->getCData($this->getElementByName('maxres', $text))));
    $imagePath = trim($this->textFixation($this->getCData($this->getElementByName('url', $imagePath))));
    if (!$imagePath) {
      return $imagesArray;
    }
    if ($this->checkImageifCached($imagePath)) {
      // Image already parsed..
      return $imagesArray;
    }
    $imagePath = str_replace(' ', '%20', $imagePath);
    $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
    if (!$copiedImage) {
      echo "no path" . PHP_EOL;
      return $imagesArray;
    }
    $images = $this->getAndCopyImagesFromArray(array($copiedImage));
    if (!empty($image_caption)) {
      $images[0]['image_caption'] = $image_caption;
    }
    $name_image = explode('/images/', $copiedImage);
    if ($images[0]['image_caption'] == $name_image[1]) {
      $images[0]['image_caption'] = '';
    }
    $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
    $images[0]['is_headline'] = TRUE;
    array_push($imagesArray, $images[0]);

    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = sha1($imageUrl) . ".jpeg";
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $curl_handle = curl_init();
      curl_setopt($curl_handle, CURLOPT_URL, $imageUrl);
      curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl_handle, CURLOPT_USERAGENT, 'php');
      $imgContent = curl_exec($curl_handle);
      curl_close($curl_handle);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $videos = array();
    $name = trim($this->textFixation($this->getCData($this->getElementByName('title', $text))));
    $videoUrl = trim($this->textFixation($this->getCData($this->getElementByName('id', $text))));
    $date = $this->getElementByName('publishedAt', $text);
    $date = date('Y-m-d h:i:s', strtotime($date));


    $videoUrl = "https://www.youtube.com/watch?v=" . $videoUrl;
    $video['video_name'] = $videoUrl;
    $video['original_name'] = $name;
    $video['video_caption'] = $name;
    $video['video_type'] = "youtube";
    $video['added_time'] = $date;
    $videos[] = $video;
    return $videos;
  }

  public function getArticleOriginalId($params = array()) {
    $id = $this->getElementByName('id', $params['text']);
    $channelId = $this->getElementByName('channelId', $params['text']);
    $articleOriginalId = $id . "_" . $channelId;
    $this->articleOriginalId = $articleOriginalId;
    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }
    return $this->title->getId() . "_" . sha1($articleOriginalId);
  }

}
