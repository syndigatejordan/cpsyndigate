<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Oman News Agency (ONA)  [# publisher id = 139]
//Title      : Oman News Agency (ONA) [ Arabic ] 
//Created on : Feb 14, 2016, 11:09:41 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_866 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}