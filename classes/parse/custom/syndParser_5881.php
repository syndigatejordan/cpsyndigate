<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Annual Reviews  [# publisher id = 1279]
//Title      : Knowable Magazine [ English ] 
//Created on : Dec 11, 2019, 1:31:16 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5881 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
  public function copyUrlImgIfNotCached($imageUrl) {
    if (preg_match("/.pdf/", strtolower($imageUrl))) {
      $baseName = sha1($imageUrl) . ".pdf";
    } else {
      $path = pathinfo($imageUrl);
      $path_file = strtolower($path['extension']);
      $baseName = sha1($imageUrl) . ".$path_file";
    }
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );

      $ch = curl_init($imageUrl);
      curl_setopt_array($ch, $options);
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
