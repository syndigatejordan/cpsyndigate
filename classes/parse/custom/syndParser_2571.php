<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Foreign Ministry Algeria  [# publisher id = 837]
//Title      : Algerian Ministry of Foreign Affairs - Press conferences & statements [ Arabic ] 
//Created on : Jan 31, 2016, 11:36:52 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2571 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}