<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Independent Newspapers (Pty) Limited  [# publisher id =392 ] 
//Title      : Sentinel News  [ English ] 
//Created on : Sep 07, 2017, 06:16:03 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3165 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
    protected function getStory(&$text)
    {
        $this->addLog("getting article text");
        $this->story = preg_replace("/<!--(.*?)-->/is", "", $this->story);
        $this->story = preg_replace("#<script(.*?)>(.*?)</script>#is", "", $this->story);
        $this->story = preg_replace("/<style\b[^>]*>(.*?)<\/style>/s", "", $this->story);
        $this->story = preg_replace("/<!--(.|\s)*?-->/", "", $this->story);
        $this->story = preg_replace('/<img[^>]+>/i', '', $this->story);
        $this->story = preg_replace("/<video(.*?)<\/video>/is", "", $this->story);
        $this->story = preg_replace('!\s+!', ' ', $this->story);
        return $this->story;
    }


    protected function getVideos(&$text)
    {
        $this->addLog("Getting videos");
        $videos = array();
        $video_Name = $this->textFixation($this->getCData($this->getElementByName('title', $text)));

        $date = $this->getElementByName('date', $text);
        $date = date('Y-m-d h:i:s', strtotime($date));
        $story = $this->getCData($this->getElementByName('fulltext', $text));
        $story = str_replace("&nbsp;", " ", $story);
        $story = $this->textFixation($story);
        $story = trim(preg_replace('!\s+!', ' ', $story));
        $matches = null;
        preg_match_all('/<video(.*?)<\/video>/is', $story, $matches);
        foreach ($matches[0] as $match) {

            $sources = null;
            if (preg_match_all('/<source(.*?)>/is', $match, $sources)) {
                foreach ($sources[0] as $source) {
                    $video = array();
                    $videoname = syndParseHelper::getImgElements($source, 'source', 'src');
                    if (isset($videoname[0])) {
                        $videoname = $videoname[0];
                    } else {
                        $videoname = "";
                    }
                    if (empty($videoname)) {
                        continue;
                    }
                    $mimeType = syndParseHelper::getImgElements($source, 'source', 'type');
                    if (isset($mimeType[0])) {
                        $mimeType = $mimeType[0];
                    } else {
                        $mimeType = "";
                    }
                    if (preg_match("/youtube.com/", $videoname)) {
                        continue;
                    }

                    $video['video_name'] = $videoname;
                    $video['original_name'] = $videoname;
                    $video['video_caption'] = $video_Name;
                    $video['mime_type'] = $mimeType;
                    $video['added_time'] = $date;
                    $videos[] = $video;
                }
            }
        }
        return $videos;
    }

}
