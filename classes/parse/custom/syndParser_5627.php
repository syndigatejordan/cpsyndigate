<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : openDemocracy Limited  [# publisher id = 1247]
//Title      : openDemocracy [ Russian ] 
//Created on : May 21, 2019, 8:02:18 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5627 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}