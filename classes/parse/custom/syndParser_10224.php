<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Infomedia / Opoint Technology  [# publisher id = 1534]
//Title      : Global News Firehose (Russian) [ Russian ] 
//Created on : Sep 23, 2021, 1:08:25 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10224 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}