<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Media 24: Newspapers  [# publisher id = 1134]
//Title      : Daily Sun [ English ] 
//Created on : Aug 28, 2017, 11:30:36 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3119 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}