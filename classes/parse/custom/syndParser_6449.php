<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence d'Information d'Afrique Centrale  [# publisher id = 1363]
//Title      : Agence d'Information d'Afrique Centrale [ French ] 
//Created on : Oct 06, 2020, 11:15:43 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6449 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}