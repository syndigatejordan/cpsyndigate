<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Iran Press News Agency  [# publisher id = 1410]
//Title      : Iran Press News Agency [ Persian ] 
//Created on : Oct 26, 2020, 9:49:08 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6564 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fa'); 
	} 
}