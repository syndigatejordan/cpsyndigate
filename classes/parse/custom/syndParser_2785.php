<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ALP Content Services operating as AL Publishing  [# publisher id = 979]
//Title      : Al-Press.com [ English ] 
//Created on : Jul 13, 2016, 12:57:32 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2785 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}