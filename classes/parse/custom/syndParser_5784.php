<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Fundación Efecto Cocuyo  [# publisher id = 1260]
//Title      : Efecto Cocuyo [ Spanish ] 
//Created on : Mar 27, 2022, 9:42:09 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5784 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}