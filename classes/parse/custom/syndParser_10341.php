<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Founder Apabi Technology Limited  [# publisher id = 1538]
//Title      : Securities Times [ simplified Chinese ] 
//Created on : Mar 14, 2021, 2:07:23 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10341 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh-Hans'); 
	} 
}