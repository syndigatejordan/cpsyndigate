<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Palestine News Network (PNN)  [# publisher id = 58]
//Title      : Palestine News Network (PNN) [ French ] 
//Created on : Aug 12, 2021, 10:08:04 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_235 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}