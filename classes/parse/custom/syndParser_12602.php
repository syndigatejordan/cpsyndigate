<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Fusion Media Limited  [# publisher id = 304]
//Title      : Investing.com Português (Brasil Edition) [ Portuguese ] 
//Created on : May 12, 2022, 7:52:42 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12602 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}