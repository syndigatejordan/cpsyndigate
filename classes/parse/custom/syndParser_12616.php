<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : China Economic Net  [# publisher id = 1797]
//Title      : Economic Daily News [ French ] 
//Created on : May 10, 2022, 11:23:05 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12616 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}