<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : GROUPE AFRIQUE54 SARL  [# publisher id = 1505]
//Title      : A5 NEWS [ French ] 
//Created on : Aug 15, 2021, 12:39:20 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7465 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}