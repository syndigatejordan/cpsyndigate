<?php

///////////////////////////////////////////////////////////////////////////
// Publisher: Bashdar Pusho Ismaeel  [# publisher id =380 ] 
// Title    : Bashdar Pusho Ismaeel [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1261 extends syndParseRss {

	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('en');
	}

	protected function getStory(&$text) {
		$this -> addLog("getting article text");
		echo $story = $this -> textFixation($this -> getCData($this -> getElementByName('content:encoded', $text)));
		return $story;
		//return strip_tags($story, '<p><b><strong><i><a><br>');
	}

	public function getOriginalCategory(&$text) {
		$this -> addLog('getting article category');
		$cats = $this -> getElementsByName('category', $text);
		$originalCats = array();

		if (!empty($cats)) {
			foreach ($cats as $cat) {
				$originalCats[] = $this -> textFixation($this -> getCData($cat));
			}
		}
		return implode(', ', $originalCats);
	}

}