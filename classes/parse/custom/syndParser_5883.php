<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The New Inquiry  [# publisher id = 1280]
//Title      : The New Inquiry [ English ] 
//Created on : Dec 11, 2019, 1:36:20 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5883 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
