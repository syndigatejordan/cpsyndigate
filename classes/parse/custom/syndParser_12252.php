<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Coin Rivet Limited  [# publisher id = 1682]
//Title      : Coin Rivet [ English ] 
//Created on : Nov 22, 2021, 1:37:11 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12252 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}