<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Pressenza  [# publisher id = 1305]
//Title      : Pressenza [ German ] 
//Created on : Feb 15, 2020, 6:32:10 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5923 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('de');
  }
}
