<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Zimbabwe Newspapers (1980) Limited  [# publisher id = 958]
//Title      : Sunday News [ English ] 
//Created on : Oct 19, 2021, 6:45:00 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2881 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}