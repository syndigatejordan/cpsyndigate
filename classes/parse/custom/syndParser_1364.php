<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Turan Information Agency  [# publisher id = 424]
//Title      : Turan Information Agency [ Azerbaijan ] 
//Created on : Aug 02, 2021, 11:50:50 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1364 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('az'); 
	} 
}