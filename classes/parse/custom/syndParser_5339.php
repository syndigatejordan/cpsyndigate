<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Emirates News Agency (WAM)  [# publisher id = 32]
//Title      : Emirates News Agency (WAM) [ Italian ] 
//Created on : Feb 10, 2019, 9:06:06 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5339 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('it');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $allowedTags = '<h1><h2><h3><h4><h5><h6><b><i><u><p><hr><br><br/><br /><big><em><small><strong><sub><sup><ins><del><s><strike><code><kbd><q><img><BLOCKQUOTE><cite><dd><div><span><table><td><th><tr><tbody><thead><a><iframe><embed><video><source><figure><figcaption><audio>';
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) srcset=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) sizes=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) src-gif=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) data-lazy-sizes=".*?"/i', '$1', $this->story);
    $this->story = strip_tags($this->story, $allowedTags);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
