<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Diario Bahia  [# publisher id = 1517]
//Title      : Diario Bahia [ Portuguese ] 
//Created on : Sep 23, 2021, 8:14:12 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_8869 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}