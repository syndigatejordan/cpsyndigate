<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Shephard Media  [# publisher id = 1352]
//Title      : Shephard Media Sample Handbooks [ English ] 
//Created on : Nov 08, 2020, 12:23:47 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7354 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}