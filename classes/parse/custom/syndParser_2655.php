<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agenzia ANSA Società Cooperativa  [# publisher id =518 ]    
//Title      : ANSA Salute & Benessere [ Italy ] 
//Created on : Mar 23, 2016, 5:01:28 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2655 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('it');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('nitf', $fileContents);
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = trim($this->getElementByName('body.content', $text));
    $this->story = strip_tags($this->story, '<p><br><strong><b><u><i>');
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  public function getAbstract(&$text) {
    $this->addLog('Getting article summary');
    $this->abs = trim(strip_tags($this->getElementByName('byline', $text)));
    return $this->abs;
  }

  public function getHeadline(&$text) {
    $this->addLog('Getting article Headline');
    $head = trim($this->getElementByName('title', $text));
    $headline = trim(strip_tags($head));
    return $headline;
  }

  public function getArticleDate(&$text) {
    $head = trim($this->getElementByName('docdata', $text));
    $dateInfo = syndParseHelper::getImgElements($head, 'date.issue', 'norm');
    return date('Y-m-d', strtotime($dateInfo[0]));
  }

  public function getOriginalCategory(&$text) {
    $DataContent = trim($this->getElementByName('head', $text));
    $cat = syndParseHelper::getImgElements($DataContent, 'meta name ="subcategory"', 'content');
    $cat = $cat[0];
    return $cat;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $DataContent = trim($this->getElementByName('head', $text));
    $auther = syndParseHelper::getImgElements($DataContent, 'meta name ="author"', 'content');
    return $auther[0];
  }

}
