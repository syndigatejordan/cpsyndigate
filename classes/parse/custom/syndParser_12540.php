<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IDG Communications, Inc.  [# publisher id = 1790]
//Title      : COMPUTERWORLD [ English ] 
//Created on : Apr 19, 2022, 11:13:28 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12540 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}