<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Kuwait News Agency (KUNA)   [# publisher id = 131]
//Title      : Kuwait News Agency (KUNA)  [ English ] 
//Created on : Oct 02, 2019, 10:11:29 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5800 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}