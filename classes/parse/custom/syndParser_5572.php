<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Stichting Global Voices  [# publisher id = 1235]
//Title      : Global Voices [ Indonesian ] 
//Created on : Feb 22, 2021, 7:58:46 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5572 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('id'); 
	} 
}