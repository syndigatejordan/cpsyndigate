<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Trend News Agency   [# publisher id =355 ] 
// Titles    : Trend News Agency [Persian]
///////////////////////////////////////////////////////////////////////////

class syndParser_1182 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('fa');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = $this->textFixation($this->getCData($this->getElementByName('description', $text)));
    $story = strip_tags($story, '<p>');
    $story = preg_replace('/<p>={2,}<\/p>/i', '', $story);

    return preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i", '<$1$2>', $story);
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('Getting article original category');
    return $this->textFixation($this->getElementByName('category', $text));
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $this->addLog("getting article image");

    $regExp = '<enclosure[\s]+.*url[\s]*=[\s]*([\"]([^>\"]*)[\"]|' . "[\\']([^>\\']*)[\\']|([^>\\s]*))([\\s]|[^>])*[\/]?>";
    preg_match_all("/$regExp/i", $text, $imgs);
    $imagePath = $imgs[2][0];
    if (!$imagePath) {
      return;
    }
    if ($this->checkImageifCached($imagePath)) {
      // Image already parsed..
      return;
    }
    $imagePath = str_replace(' ', '%20', $imagePath);
    $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
    if (!$copiedImage) {
      echo "no pahr";
      return;
    }
    $images = $this->getAndCopyImagesFromArray(array($copiedImage));
    $name_image = explode('/images/', $copiedImage);
    if ($images[0]['image_caption'] == $name_image[1]) {
      $images[0]['image_caption'] = '';
    }
    $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
    $images[0]['is_headline'] = TRUE;
    array_push($imagesArray, $images[0]);
    return $imagesArray;
  }

}
