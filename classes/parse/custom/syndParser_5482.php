<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CPI Financial  [# publisher id = 259]
//Title      : Wealth Arabia [ English ] 
//Created on : Mar 26, 2019, 12:03:42 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5482 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getArticleOriginalId($params = array()) {
    $defaultArray = array('text' => '', 'headline' => '', 'articleDate' => '', 'issueNum' => 0);
    $params = array_merge($defaultArray, $params);
    if (!$articleOriginalId = trim($this->getElementByName('id', $params['text']))) {
      return parent::getArticleOriginalId($params);
    }
    return $this->title->getId() . "_" . sha1($articleOriginalId . trim($params['headline']));
  }

}
