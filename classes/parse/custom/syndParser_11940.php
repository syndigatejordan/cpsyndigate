<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Hearst Newspapers  [# publisher id = 1600]
//Title      : Times Union [ English ] 
//Created on : Jul 04, 2021, 12:52:09 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11940 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}