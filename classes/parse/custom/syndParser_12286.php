<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ODS SRL  [# publisher id = 1691]
//Title      : Ora de Sibiu [ Romanian ] 
//Created on : Dec 07, 2021, 2:42:39 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12286 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ro'); 
	} 
}