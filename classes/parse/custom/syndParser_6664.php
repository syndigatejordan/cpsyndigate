<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Turkish Government  [# publisher id = 148]
//Title      : Anadolu Agency (AA) [ Bosnian ] 
//Created on : Oct 13, 2020, 9:37:29 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6664 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('bs'); 
	} 
}