<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Africa News Agency (ANA)  [# publisher id = 1432]
//Title      : Africa News Agency (ANA) [ French ] 
//Created on : Oct 05, 2020, 11:22:05 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6695 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}