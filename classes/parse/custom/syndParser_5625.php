<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : openDemocracy Limited  [# publisher id = 1247]
//Title      : openDemocracy [ English ] 
//Created on : May 21, 2019, 8:01:46 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5625 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
