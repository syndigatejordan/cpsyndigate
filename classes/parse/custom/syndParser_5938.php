<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Human Rights Watch  [# publisher id = 1302]
//Title      : Human Rights Watch - News [ French ] 
//Created on : Feb 29, 2020, 1:57:52 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5938 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('fr');
  }
}
