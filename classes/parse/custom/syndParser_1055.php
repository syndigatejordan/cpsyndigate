<?php


	class syndParser_1055 extends syndParseRss
	{
		public function getRawArticles(&$fileContents) {		
			$this->addLog('getting row articles');	
			$rowArticles = parent::getRawArticles($fileContents);			
			return $rowArticles;
		}
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang  = $this->model->getLanguageId('en');
		}
		
		
	    protected function getAbstract (&$text) {
	    	$this->addLog('getting article summary');
			$summary = $this->textFixation($this->getCData($this->getElementByName('description', $text)));			
			return $summary;
		}
		
		public function getStory(&$text) {
			$this->addLog('getting article story');
			$strory = $this->textFixation($this->getCData($this->getElementByName('content:encoded', $text)));
			$strory = str_replace("style=\"text-align: justify;\"", '', $strory);
			$story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $story);
			return $strory;
		}

		public function getOriginalCategory(&$text) {
			$this->addLog('getting article original data');
			return $this->textFixation($this->getElementByName('category', $text));
		}
	}