<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: Dar Al Hayat
	// Titles   : Dar Al Hayat [Arabic]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_19 extends syndParseAbTxtSample {
		
		
		public function getRawArticles(&$fileContents) {			
			$this->addLog('Get raw articles');
 			$fileContents =	str_replace(">", "\"", $fileContents);
 			$fileContents =	str_replace("<", "\"", $fileContents);
			//echo $fileContents . PHP_EOL . PHP_EOL . PHP_EOL ;
	        return array($fileContents);
		}
		
		public function customInit() {
			parent::customInit();
			$this->charEncoding = 'windows-1256';
			$this->defaultLang = $this->model->getLanguageId('ar');
		}

		
		public function getStory(&$text) {
			
			$this->removeItem('المصدر', $text);
			$this->removeItem('الكاتب', $text);
			$this->removeItem('ت·هـ', $text);
			$this->removeItem('ت.ه', $text);
			$this->removeItem('جهة المصدر', $text);
			$this->removeItem('العدد', $text);
			$this->removeItem('الصفحة', $text);
			$this->removeItem('الواصفات', $text);
			
			$story	=	"<p>";
			$story	.=	parent::getStory(&$text);
			$story  = 	preg_replace("/\r\n|\r|\n/", "</p><p>", $story);
			$story .= 	"</p>"; 

			return $story;			
		}
		

		public function getArticleDate(&$text,  $dateTagName = 'ت.م') {
			
			$date = trim($this->getItemValue($dateTagName, $text));
		    $this->removeItem($dateTagName, $text);
		    return $this->dateFormater($date);		    
		}
		
		protected function getHeadline(&$text, $headlineTagName='Headline') {
			return parent::getHeadline($text, 'العنوان');
		}
		
		protected function getAuthor(&$text) {			
			$this->addLog("getting article author");
			return $this->textFixation($this->getItemValue('الكاتب', $text));
		}





	} // End class 
	
