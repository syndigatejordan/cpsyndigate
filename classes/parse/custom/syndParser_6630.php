<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Macau News Agency (MNA) / DeFicção Multimedia Projects  [# publisher id = 1420]
//Title      : Macau Business [ English ] 
//Created on : Sep 19, 2021, 8:56:57 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6630 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}