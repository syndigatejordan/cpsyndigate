<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Cointelegraph  [# publisher id = 1767]
//Title      : Cointelegraph [ Italian ] 
//Created on : Feb 21, 2022, 11:27:37 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12470 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('it'); 
	} 
}