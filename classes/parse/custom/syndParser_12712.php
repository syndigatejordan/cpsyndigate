<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Fankoosh Studios LTD  [# publisher id = 1849]
//Title      : 7azarfazar [ Arabic ] 
//Created on : Sep 19, 2022, 7:11:34 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12712 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}