<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher  : Thomson Reuters  [# publisher id =537 ]   
//Title      : Reuters Newswire [English]
//Created on : Mar 26, 2020, 10:36:27 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_5972 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('itemSet', $fileContents);
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->getElementByName('headline', $text);
    return $this->textFixation($headline);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('versionCreated', $text);
    $date = preg_replace('/T(.*)/is', '', $date);
    return date('Y-m-d', strtotime($date));
  }

  protected function getAbstract(&$text) {
    $this->addLog("getting article summary");
    return trim($this->textFixation($this->getElementByName('description', $text)));
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $body = $this->textFixation($this->getElementByName('body', $text));
    $body = preg_replace('!\s+!', ' ', $body);
    return $body;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return trim($this->textFixation($this->getElementByName('by', $text)));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getElementsByName('memberOf', $text);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = trim($this->textFixation($this->getElementByName('name', $cat)));
      }
    }
    return implode(', ', $originalCats);
  }

  public function getArticleOriginalId($params = array()) {
    $articleOriginalId = syndParseHelper::getImgElements($params['text'], 'rtr:versionedId', 'guid');
    $articleOriginalId = $articleOriginalId[0];
    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }

    return $this->title->getId() . '_' . sha1($articleOriginalId);
  }

}
