<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Electric Cars Report  [# publisher id = 1710]
//Title      : Electric Cars Report [ English ] 
//Created on : Jan 11, 2022, 11:09:33 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12313 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}