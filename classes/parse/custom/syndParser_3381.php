<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : EMDP - Egypt Media Development Program  [# publisher id = 1164]
//Title      : Egyptian Editors Forum (EEF) [ Arabic ] 
//Created on : Jan 04, 2018, 1:42:30 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3381 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}