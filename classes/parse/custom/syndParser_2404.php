<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Alexandria Chamber of Commerce  [# publisher id =769 ] 
//Title     : Alexandria Chamber of Commerce News [ arabic ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2404 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

}
