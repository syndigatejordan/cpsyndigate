  <?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : HT Media Ltd.  [# publisher id = 89]
//Title      : Light Castle [ English ] 
//Created on : Apr 02, 2020, 9:43:33 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5979 extends syndParseHtMedia {

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    /* 	
      $headline = $this->getElementByName('Headline', $text);
      return $this->textFixation($this->getElementByName('PrimaryHeadline', $headline)); */
    $headline = $this->getElementByName('Headline1', $text);
    return $this->textFixation($headline);
  }

  protected function getStory(&$text) {

    $this->addLog("getting article text");
    /* 	
      $bodyWithDate = $this->textFixation($this->getElementByName('body', $text));
      $body = explode('</Dateline>', $bodyWithDate);
      return $this->textFixation($body[1]); */
    $body = $this->textFixation($this->getElementByName('Body', $text));
    $body = strip_tags($body, '<p>');
    return $body;
  }

}
