<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : A15   [# publisher id = 330]
//Title      : Masrawy [ Arabic ] 
//Created on : Aug 12, 2021, 12:09:22 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2167 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}