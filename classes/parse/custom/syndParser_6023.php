<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Highlights for Children, Inc.  [# publisher id = 1333]
//Title      : News-O-Matic [ English ] 
//Created on : Sep 27, 2021, 1:36:33 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6023 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}