<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Breaking Belize News  [# publisher id = 1821]
//Title      : Breaking Belize News [ English ] 
//Created on : Jul 04, 2022, 11:26:13 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12663 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}