<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Namibian  [# publisher id = 959]
//Title      : The Namibian [ English ] 
//Created on : May 19, 2016, 7:16:43 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2760 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}