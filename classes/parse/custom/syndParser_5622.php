<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Tasnim News Agency  [# publisher id = 1246]
//Title      : Tasnim News Agency [ Urdu ] 
//Created on : May 22, 2019, 6:48:20 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5622 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ur'); 
	} 
}