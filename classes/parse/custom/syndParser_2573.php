<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Foreign Ministry Algeria  [# publisher id = 837]
//Title      : Algerian Ministry of Foreign Affairs - Spokesman Statements [ French ] 
//Created on : Jan 31, 2016, 11:36:38 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2573 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}