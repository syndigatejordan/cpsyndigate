<?php
///////////////////////////////////////////////////////////////////////////
// Publisher : Palestine News & Information Agency (WAFA)
// Titles    : Palestine News & Information Agency (WAFA)
///////////////////////////////////////////////////////////////////////////

 class syndParser_200 extends syndParseCms {

	public function customInit() {
		parent::customInit();
		$this->defaultLang = $this->model->getLanguageId('en');
	}

	public function getRawArticles(&$fileContents) {
	    	$fileContents = str_replace("�", "'", $fileContents);
		return parent::getRawArticles($fileContents);

	}
}
