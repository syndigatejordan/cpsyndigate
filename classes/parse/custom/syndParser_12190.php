<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Afghanistan Analysts Network  [# publisher id = 1639]
//Title      : Afghanistan Analysts Network [ English ] 
//Created on : Oct 03, 2021, 10:42:34 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12190 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}