<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Flightdeck Communications Enterprises  [# publisher id = 1770]
//Title      : Nigerian Flight Deck [ English ] 
//Created on : Feb 24, 2022, 10:09:56 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12484 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}