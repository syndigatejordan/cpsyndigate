<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Motivate publishing  [# publisher id = 116]
//Title      : Gulf Business [ English ] 
//Created on : Mar 10, 2021, 10:49:14 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10333 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}