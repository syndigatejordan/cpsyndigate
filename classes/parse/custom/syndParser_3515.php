<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Libya News Agency (LANA)  [# publisher id = 150]
//Title      : Libya News Agency (LANA) [ French ] 
//Created on : Jul 03, 2018, 7:17:51 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3515 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}