<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: United Press International (UPI)
	// Titles   : UPI Security Industry
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_356 extends syndParseXmlContent 
	{
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('en');
		}
		
		public function getRawArticles(&$fileContents) {	
			
			$this->addLog("Getting raw articles");
			$this->xml = simplexml_load_string($fileContents);
			return $this->getElementsByName('NewsML', $fileContents);
		}
						
		protected function getStory(&$text) {
			
			$this->addLog("getting article text");
			$story = @$this->xml->NewsItem->NewsComponent->NewsComponent->ContentItem->Encoding->DataContent->asXml();
			return $this->textFixation($this->getElementByName('DataContent', $story));
		}
		
		protected function getHeadline(&$text) {
			
			$this->addLog('Getting headline');
			$headline = $this->xml->NewsItem->NewsComponent->NewsLines->HeadLine;
			return $this->textFixation($headline);
		}
		
		protected function getArticleDate(&$text) {
			
			$this->addLog("getting article date");
			$dateId = $this->xml->NewsItem->Identification->NewsIdentifier->DateId;
			$date	= substr($dateId, 0, 4) . '-' . substr($dateId, 4, 2) . '-' . substr($dateId, 6, 2);
			
			if($date) {
				return $this->dateFormater($date);
			}
			return parent::getArticleDate($text);
		}
		
		public function getArticleOriginalId($params = array()) {						 
			$articleOriginalId = trim($this->xml->NewsItem->Identification->NewsIdentifier->NewsItemId);			
			
			if(!$articleOriginalId ) {
				return parent::getArticleOriginalId($params);
			}			
		
			return $this->title->getId() . '_' . sha1($articleOriginalId) . '_' . $articleOriginalId;
		}

		
	} // End class 
