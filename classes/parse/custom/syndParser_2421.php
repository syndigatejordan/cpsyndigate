<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Federation of the Gulf Cooperation Council (GCC) Chambers  [# publisher id = 779]
//Title      : Federation of the Gulf Cooperation Council (GCC) Chambers News [ English ] 
//Created on : Aug 03, 2021, 10:32:18 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2421 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}