<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Saud Alhawawi  [# publisher id = 682]
//Title      : Tech World [ Arabic ] 
//Created on : Sep 19, 2021, 7:02:38 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2149 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}