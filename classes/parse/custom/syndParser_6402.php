<?php
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Shephard Media  [# publisher id = 1352]
//Title      : Shephard Media - Premium News [ English ] 
//Created on : Jan 27, 2021, 7:51:43 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_6402 extends syndParseCms {
    public function customInit() {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('en');
    }
}