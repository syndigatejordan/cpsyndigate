<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Foreign Ministry Algeria  [# publisher id = 837]
//Title      : Algerian Ministry of Foreign Affairs - News [ French ] 
//Created on : Jan 30, 2016, 8:28:53 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2567 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}