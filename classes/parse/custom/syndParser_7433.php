<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : MEDIOS DE COMUNICACIÓN LA QILQA Ltda.  [# publisher id = 1488]
//Title      : La CiUDAD [ Spanish ] 
//Created on : Sep 19, 2021, 10:20:28 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7433 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}