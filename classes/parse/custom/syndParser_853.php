<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Eaglemont Media  [# publisher id = 246]
//Title      : The Islamic Globe [ English ] 
//Created on : Nov 16, 2016, 8:01:56 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_853 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    return $this->story;
  }

  protected function getImages(&$text) {
    $this->story = $this->textFixation($this->getCData($this->getElementByName('fulltext', $text)));
    $this->imagesArray = array();
    preg_match_all("/<img[^>]+\>/i", $this->story, $imgs);

    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = explode('alt="', $img);
      $image_caption = explode('"', $image_caption[1]);
      $image_caption = $image_caption[0];
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      $imagePath = explode("?", $imagePath);
      $imagePath = $imagePath[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['image_caption'] = str_replace('%20', ' ', $image_caption);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);

      $img = htmlspecialchars($img);
      $img = str_replace("&quot;", '"', $img);
      $new_img = htmlspecialchars($new_img);
      $new_img = str_replace("&quot;", '"', $new_img);

      $text = str_replace($img, $new_img, $text);

      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

}
