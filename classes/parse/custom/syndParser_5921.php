<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Mark Allen Group (MAG)  [# publisher id = 1304]
//Title      : Farmers Weekly [ English ] 
//Created on : Feb 22, 2021, 11:30:35 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5921 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}