<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Emirates News Agency (WAM)  [# publisher id = 32]
//Title      : Emirates News Agency (WAM) [ Urdu ] 
//Created on : Feb 10, 2019, 9:07:24 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5345 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ur'); 
	} 
}