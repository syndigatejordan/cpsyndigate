<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Harris & Ewing Photographs  [# publisher id = 1314]
//Title      : Harris & Ewing Photographs [ English ] 
//Created on : Mar 03, 2020, 8:03:59 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5948 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
