<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ACN Newswire Co Ltd  [# publisher id = 1174]
//Title      : ACN Newswire [ traditional Chinese ] 
//Created on : Sep 23, 2021, 7:58:25 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3443 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh-Hant'); 
	} 
}