<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Central News Agency (CNA)  [# publisher id = 1406]
//Title      : Focus Taiwan (CNA English News) [ English ] 
//Created on : Oct 19, 2020, 11:42:21 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6545 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}