<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Gulf Publishing and Printing Company  
//Title     : Al Rayah [ Arabic ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_170 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

}
