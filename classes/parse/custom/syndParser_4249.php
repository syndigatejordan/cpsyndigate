<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dubai Chamber of Commerce and Industry  [# publisher id = 790]
//Title      : Dubai Chamber TV [ English ] 
//Created on : Nov 19, 2018, 7:40:50 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_4249 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}