<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : China Daily Information Co.  [# publisher id = 498]
//Title      : China Daily [ English ] 
//Created on : Dec 11, 2019, 11:58:31 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1493 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

  protected function getVideos(&$text) {
      $this->addLog("Getting videos");
      $fulltext = $this->textFixation($this->getElementByName('fulltext', $text));
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();

      $videos = array();
      $matches = null;
      preg_match_all('/<iframe(.*?)<\/iframe>/is', $fulltext, $matches);
      foreach ($matches[0] as $matche) {
          $video = array();
          $videoname = null;
          if (preg_match('/ src="(.*?)" /is', $videoname)) {
              if (preg_match("/youtube.com/", $videoname[1])) {
                  continue;
              }
        if (strpos($videoname[1], "http:") === FALSE) {
          $videoname[1] = "http:" . $videoname[1];
        }
        $video['video_name'] = $videoname[1];
        $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
          $video['original_name'] = $videoName;
          $video['video_caption'] = $videoName;
          $date = $this->getElementByName('date', $text);
          $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
          $video['mime_type'] = "";
          $videos[] = $video;
      }
    }
      return $videos;
  }

    protected function getImages(&$text)
    {
        $imagesArray = array();
        $this->story = $this->getCData($this->getElementByName('fulltext', $text));
        $this->story = str_replace("&amp;", "&", $this->story);
        $this->story = $this->textFixation($this->story);
        preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
        foreach ($imgs[0] as $img) {
            $this->addLog("getting article images");
            $image_caption = '';
            $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
            if (!empty($image_caption[0])) {
                $image_caption = $image_caption[0];
            } else {
                $image_caption = syndParseHelper::getImgElements($img, 'img', 'title');
                if (!empty($image_caption[0])) {
                    $image_caption = $image_caption[0];
                } else {
                    $image_caption = "";
                }
            }
            $imageInfo = syndParseHelper::getImgElements($img, 'img');
            $imagePathold = $imageInfo[0];
            $imagePath = preg_replace("/\?(.*)/is", "", $imagePathold);
            if (!$imagePath) {
                continue;
            }
            $imagePath = str_replace(' ', '%20', $imagePath);
            $new_img = array();
            $new_img['img_name'] = $imagePath;
            $new_img['original_name'] = $imagePath;
            $new_img['image_caption'] = $image_caption;
            $new_img['is_headline'] = FALSE;
            $new_img['image_original_key'] = sha1($imagePath);
            array_push($imagesArray, $new_img);
        }
        return $imagesArray;
    }

}
