<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : EUobserver  [# publisher id = 1509]
//Title      : EUobserver [ English ] 
//Created on : Dec 14, 2021, 2:18:20 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7470 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}