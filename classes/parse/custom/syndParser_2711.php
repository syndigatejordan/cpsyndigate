<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Wahda Foundation For Press, Printing and Publishing  [# publisher id = 917]
//Title      : Al Jamahir [ Arabic ] 
//Created on : May 16, 2016, 9:30:55 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2711 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}