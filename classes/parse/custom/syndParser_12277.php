<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Merkle LLC  [# publisher id = 1686]
//Title      : NullTX [ English ] 
//Created on : Nov 23, 2021, 10:55:14 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12277 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}