<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Egyptian Streets (ES Media)  [# publisher id = 1495]
//Title      : Egyptian Streets [ English ] 
//Created on : Sep 19, 2021, 10:20:48 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7448 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}