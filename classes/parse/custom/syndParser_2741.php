<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Le Mauricien Ltd  [# publisher id = 945]
//Title      : Le Mauricien [ French ] 
//Created on : May 18, 2016, 9:22:07 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2741 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}