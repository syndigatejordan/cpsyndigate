<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Crisp Network  [# publisher id = 1331]
//Title      : Crisp Network: Cars [ Arabic ] 
//Created on : Jul 09, 2020, 8:15:54 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6288 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}