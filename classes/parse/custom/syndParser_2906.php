<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : U.S. Department of State  [# publisher id = 1048]
//Title      : Share America [ simplified Chinese ] 
//Created on : Sep 13, 2021, 2:13:16 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2906 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh-Hans'); 
	} 
}