<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ministry of Foreign Affairs, the People's Republic of China  [# publisher id = 898]
//Title      : Ministry of Foreign Affairs of the People's Republic of China - News [ French ] 
//Created on : Apr 07, 2016, 9:38:46 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2669 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}