<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Conversation Media Group Ltd  [# publisher id = 1237]
//Title      : The Conversation (New Zealand Edition) [ English ] 
//Created on : Oct 14, 2020, 9:03:13 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5614 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}