<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Palestine Exchange (PEX)  [# publisher id = 600]
//Title      : Palestine Exchange (PEX) [ English ] 
//Created on : Jul 21, 2019, 5:27:24 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1696 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}