<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IFC - International Finance Corporation  [# publisher id = 1773]
//Title      : IFC (World Bank Group) [ simplified Chinese ] 
//Created on : Feb 28, 2022, 11:14:43 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12489 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh-Hans'); 
	} 
}