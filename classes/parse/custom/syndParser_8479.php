<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : FPS Chancellery of the Prime Minister and the FPS Policy and Support (BOSA)  [# publisher id = 1513]
//Title      : Belgium.be [ German ] 
//Created on : Feb 01, 2021, 2:22:33 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_8479 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('de'); 
	} 
}