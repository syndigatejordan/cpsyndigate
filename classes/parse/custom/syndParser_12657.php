<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BLACK SEA PRESS LLC  [# publisher id = 1819]
//Title      : NewsGeorgia [ Russian ] 
//Created on : Jun 26, 2022, 12:27:46 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12657 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}