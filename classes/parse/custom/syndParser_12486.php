<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Matininfos.net  [# publisher id = 1771]
//Title      : Matininfos.net [ French ] 
//Created on : Feb 28, 2022, 10:28:13 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12486 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}