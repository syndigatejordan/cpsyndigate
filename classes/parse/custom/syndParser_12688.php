<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Global Crypto Press Association  [# publisher id = 1834]
//Title      : The Global Crypto Press Association [ simplified Chinese ] 
//Created on : Aug 07, 2022, 11:24:09 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12688 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh-Hans'); 
	} 
}