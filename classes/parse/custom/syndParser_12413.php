<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Standard Group  [# publisher id = 1155]
//Title      : FarmKenya [ English ] 
//Created on : May 19, 2022, 7:58:05 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12413 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}