<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ministry of Press of the Russian Federation  [# publisher id = 1640]
//Title      : Afghanistan.ru [ Russian ] 
//Created on : Oct 03, 2021, 11:45:21 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12192 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}