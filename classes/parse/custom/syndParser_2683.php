<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Vietnam Ministry of Foreign Affairs  [# publisher id = 903]
//Title      : Vietnam Ministry of Foreign Affairs - News [ English ] 
//Created on : Apr 07, 2016, 10:44:42 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2683 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}