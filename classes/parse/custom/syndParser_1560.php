<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Mondaq Limited  [# publisher id =521 ]
// Titles    : Mondaq [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1560 extends syndParseXmlContent {
	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('en');
		$this -> extensionFilter = 'xml';
	}

	protected function getRawArticles(&$fileContents) {
		//get articles
		$this -> addLog("getting articles raw text");
		$art = $this -> getElementsByName('article', $fileContents);
		return $art;
	}

	public function getStory(&$text) {
		$this -> addLog('Getting article story');
		$body = $this -> getCData($this -> getElementByName('body', $text));
		$body = str_replace("<p align=center>", '', $body);
		$body = str_replace("(c) Mondaq Ltd, 2013 - Tel. +44 (0)20 8544 8300 -", '', $body);
		$body = str_replace("http://www.mondaq.com</a></p>", '', $body);

		$body = utf8_encode($body);
		$body = strip_tags($body, '<p><br><strong><b><u><i>');
		if (empty($body)) {
			return '';
		}
		return $body;
	}

	public function getHeadline(&$text) {
		$headline = $this -> getCData($this -> getElementByName('abstract', $text));
		$headline = utf8_encode($headline);
		return $headline;
	}

	public function getAuthor(&$text) {
		$byline = $this -> getCData($this -> getElementByName('author', $text));
		return $byline;
	}

	public function getOriginalCategory(&$text) {
		$cat = $this -> getCData($this -> getElementByName('category', $text));
		return $cat;

	}

	public function getArticleDate(&$text) {
		$date = $this -> getCData($this -> getElementByName('date', $text));
		$date = date('Y-m-d', strtotime($date));
		return $date;
	}

}
