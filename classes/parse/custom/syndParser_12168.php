<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Infomedia / Opoint Technology  [# publisher id = 1534]
//Title      : Global News Firehose (Test Feed) [ English ] 
//Created on : Sep 23, 2021, 12:38:11 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12168 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}