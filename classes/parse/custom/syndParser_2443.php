<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ramallah Chamber of Commerce and Industry  [# publisher id = 793]
//Title      : Ramallah Chamber of Commerce and Industry Business News [ Arabic ] 
//Created on : Feb 02, 2016, 12:08:25 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2443 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}