<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Domain Integrators L.L.C  [# publisher id = 673]
//Title      : Fitnessyard [ Arabic ] 
//Created on : Aug 15, 2021, 7:50:18 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2255 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}