<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : KABAR News Agency  [# publisher id = 1415]
//Title      : KABAR News Agency [ Arabic ] 
//Created on : Oct 22, 2020, 10:32:41 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6607 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}