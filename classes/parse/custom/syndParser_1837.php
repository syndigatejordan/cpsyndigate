<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Assa-Irada News Agency  [# publisher id = 650]
//Title      : Azeri Observer [ English ] 
//Created on : Aug 02, 2021, 12:10:13 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1837 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}