<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Corporación Medios Digitales del Sur Ltda  [# publisher id = 1590]
//Title      : Mundo Agropecuario [ Spanish ] 
//Created on : Jun 08, 2021, 6:27:42 AM
//Author     : rand
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10875 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}