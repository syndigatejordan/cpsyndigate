<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : NewsBites Pty Ltd.  [# publisher id =341 ] 
// Titles    : NewsBites Finance - China
///////////////////////////////////////////////////////////////////////////

class syndParser_2338 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    $this->loadCurrentDirectory();
    $this->addLog("getting raw articles text");
    $Item = $this->getElementsByName('DigestName', $fileContents);
    return $Item;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return $this->textFixation($this->getElementByName('Headline', $text));
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('Date', $text);
    return date('Y-m-d', strtotime($date));
  }

  protected function getOriginalCategory(&$text) {
    $this->addLog("getting article category");
    $category = trim(trim($this->textFixation($this->getElementByName('Category', $text))), ',');
    $Keyword = trim($this->textFixation($this->getElementByName('Keyword', $text)));
    $Keyword = str_replace(';', ',', $Keyword);
    if (!empty($Keyword)) {
      $category = $category . ', ' . $Keyword;
    }
    return $category;
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    return $this->textFixation($this->getElementByName('Abstract', $text));
  }

  public function getAbstract(&$text) {
    $this->addLog('Getting article summary');
    return strip_tags($this->getElementByName('IndustrySubject', $text));
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getCData($this->getElementByName('Source', $text)));
  }

}
