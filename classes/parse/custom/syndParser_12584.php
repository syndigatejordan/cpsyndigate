<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Shingetsu News Agency  [# publisher id = 1794]
//Title      : Akihabara news [ English ] 
//Created on : Apr 21, 2022, 8:54:04 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12584 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}