<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BBC World Service  [# publisher id = 1192]
//Title      : BBC Vietnamese - News [ Vietnamese ] 
//Created on : Mar 15, 2022, 1:20:08 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12523 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('vi'); 
	} 
}