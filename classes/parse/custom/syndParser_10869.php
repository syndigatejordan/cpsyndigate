<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Marcos Aurélio Marques da Cunha  [# publisher id = 1586]
//Title      : Itamaraju Noticias [ Portuguese ] 
//Created on : Jun 01, 2021, 11:49:17 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10869 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}