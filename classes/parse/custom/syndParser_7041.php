<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : TND News (GLCSMS LLC)  [# publisher id = 1450]
//Title      : TND News [ English ] 
//Created on : Sep 28, 2021, 1:16:40 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7041 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}