<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : NOTIMEX - Agencia del Estado Mexicano  [# publisher id = 1396]
//Title      : NOTIMEX [ Spanish ] 
//Created on : Oct 20, 2020, 8:23:52 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6516 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}