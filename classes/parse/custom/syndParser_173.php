<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : MBC Group  
//Title     : AlArabiya.net [ Arabic ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_173 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  public function getStory(&$text) {
      $this->addLog('Getting article story');
      $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
      $this->story = preg_replace("/<video(.*?)<\/video>/is", "", $this->story);
      $this->story = preg_replace('/<div[^>]+>/i', '', $this->story);
      $this->story = str_replace('<div>', '', $this->story);
      $this->story = str_replace('</div>', '', $this->story);
      $this->story = preg_replace('!\s+!', ' ', $this->story);

      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      if (empty($this->story)) {
          return '';
      }
      return $this->story;
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $videos = array();
    $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));

    $date = $this->getElementByName('date', $text);
    $date = date('Y-m-d h:i:s', strtotime($date));
    $story = $this->getCData($this->getElementByName('fulltext', $text));
    $story = str_replace("&nbsp;", " ", $story);
    $story = $this->textFixation($story);
    $story = trim(preg_replace('!\s+!', ' ', $story));
    $matches = null;
    preg_match_all('/<video(.*?)<\/video>/is', $story, $matches);
    foreach ($matches[0] as $match) {

      $sources = null;
      if (preg_match_all('/<source(.*?)>/is', $match, $sources)) {
        foreach ($sources[0] as $source) {
          $video = array();
          $videoname = syndParseHelper::getImgElements($source, 'source', 'src');
          if (isset($videoname[0])) {
            $videoname = $videoname[0];
          } else {
            $videoname = "";
          }
          if (empty($videoname)) {
            continue;
          }
          $mimeType = syndParseHelper::getImgElements($source, 'source', 'type');
          if (isset($mimeType[0])) {
            $mimeType = $mimeType[0];
          } else {
            $mimeType = "";
          }
          if (preg_match("webm", $mimeType)) {
            continue;
          }
          if (preg_match("/youtube.com/", $videoname)) {
            continue;
          }

          $video['video_name'] = $videoname;
          $video['original_name'] = $videoname;
          $video['video_caption'] = $videoname;
          $video['mime_type'] = $mimeType;
          $video['added_time'] = $date;
          $videos[] = $video;
        }
      }
    }
    return $videos;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = sha1($imageUrl) . ".jpeg";
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );

      $ch = curl_init($imageUrl);
      curl_setopt_array($ch, $options);
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
