<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Hürriyet Gazetecilik ve Matbaacılık A.Ş.-Turkish   [# publisher id = 1478]
//Title      : Hürriyet [ Turkish ] 
//Created on : Nov 30, 2020, 1:29:26 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7373 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('tr'); 
	} 
}