<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : L'Observateur Paalga  [# publisher id =939 ] 
//Title      : L' Observateur Paalga [ French ] 
//Created on : May 25, 2016, 11:15:38 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2735 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('fr');
  }
}
