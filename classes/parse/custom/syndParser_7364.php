<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Complete Communications Limited  [# publisher id = 1469]
//Title      : Complete Sports [ English ] 
//Created on : Oct 28, 2021, 8:15:35 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7364 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}