<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : 24.com  [# publisher id = 1131]
//Title      : People's Post [ English ] 
//Created on : Aug 30, 2017, 8:09:40 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3130 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}