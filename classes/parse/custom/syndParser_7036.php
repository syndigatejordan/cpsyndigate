<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Teacher Created Materials  [# publisher id = 1358]
//Title      : Learn-at-Home: Reading Bundle Grade K: 4-Book Set [ English ] 
//Created on : Nov 01, 2020, 8:22:26 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_7036 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getRawArticles(&$fileContents) {
    $this->addLog("getting raw articles text");
    $rawArticles = array();
    $articles = $this->getElementsByName('item', $fileContents);
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->textFixation($this->getCData($this->getElementByName('Title', $text)));
    return $headline;
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $body = "";
    $GradeLevel = trim($this->textFixation($this->getCData($this->getElementByName('Grade_Level', $text))));
    if (!empty($GradeLevel)) {
      $body .= "<p>Grade: $GradeLevel</p>";
    }
    $Lexile = $this->textFixation($this->getCData($this->getElementByName('Lexile', $text)));
    if (!empty($Lexile)) {
      $body .= "<p>Lexile: $Lexile</p>";
    }
    $Interest_Level = $this->textFixation($this->getCData($this->getElementByName('Interest_Level', $text)));
    if (!empty($Interest_Level)) {
      $body .= "<p>Interest Level: $Interest_Level</p>";
    }
    $GuidedReadingLevel = $this->textFixation($this->getCData($this->getElementByName('Guided_Reading_Level', $text)));
    if (!empty($GuidedReadingLevel)) {
      $body .= "<p>Guided Reading Level: $GuidedReadingLevel</p>";
    }
    $ebookISBN = $this->textFixation($this->getCData($this->getElementByName('ebook_ISBN', $text)));
    if (!empty($ebookISBN)) {
      $body .= "<p>ISBN: $ebookISBN</p>";
    }
    $Language = $this->textFixation($this->getCData($this->getElementByName('Language', $text)));
    if (!empty($Language)) {
      $body .= "<p>Language: $Language</p>";
    }

    $Description = $this->textFixation($this->getCData($this->getElementByName('Description', $text)));
    if (!empty($Description)) {
      $body .= "<p>Description: $Description</p>";
    }
    return $body;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    return date('Y-m-d');
  }

  protected function getImages(&$text) {
    $this->addLog("getting pdf file");
    $imagesArray = array();
    $files =null;
    preg_match_all("/<file>(.*?)<\/file>/is", $text, $files);
      foreach ($files[1] as $file) {
        $img = array();
        $image_Name = trim($this->textFixation($this->getCData($file)));
        $image_full_path = $this->currentDir . $image_Name;
        $original_name = explode('.', $image_Name);
        $image_caption = str_replace("_", " ", $original_name[0]);
        if (file_exists($image_full_path)) {
          //the video name is same as the image name here  		
          $name = $this->model->getImgReplacement($image_Name, $this->currentDir, 7036);
          $img['img_name'] = str_replace(IMGS_PATH, IMGS_HOST, $name);
          $img['original_name'] = $original_name[0];
          $img['image_caption'] = $image_caption;
          $img['is_headline'] = false;
          $img['image_original_key'] = sha1($image_Name);
          array_push($imagesArray, $img);
        }
      }
    return $imagesArray;
  }


}
