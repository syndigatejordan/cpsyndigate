<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Coconuts Media Limited  [# publisher id = 1468]
//Title      : Coconuts Media [ English ] 
//Created on : Sep 19, 2021, 10:19:24 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7361 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}