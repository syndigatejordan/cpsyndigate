<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Alpha Comunicaciones Integrales  [# publisher id = 1485]
//Title      : Costa del Este, Panama [ Spanish ] 
//Created on : Mar 27, 2022, 9:26:01 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7424 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}