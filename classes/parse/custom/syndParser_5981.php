<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : HT Media Ltd.  [# publisher id = 89]
//Title      : BD News 24 [ Bengali ] 
//Created on : Apr 02, 2020, 8:09:30 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_5981 extends syndParseHtMedia {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('bn');
  }

}
