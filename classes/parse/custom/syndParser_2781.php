<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Eastern Press, Printing & Media  [# publisher id = 976]
//Title      : Al Sharq [ Arabic ] 
//Created on : May 28, 2016, 8:39:00 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2781 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}