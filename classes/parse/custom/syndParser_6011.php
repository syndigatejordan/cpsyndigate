<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Exchange Data International (EDI)  [# publisher id = 174]
//Title      : EDI - Economic Indicator Service (EIS) [ English ] 
//Created on : Apr 09, 2020, 7:09:12 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6011 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}