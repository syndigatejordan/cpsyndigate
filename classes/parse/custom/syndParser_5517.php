<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Motley Fool Holdings Inc.  [# publisher id = 1223]
//Title      : The Motley Fool [ English ] 
//Created on : Apr 07, 2019, 1:07:08 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5517 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}