<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Islamic Republic News Agency (IRNA)  [# publisher id = 1412]
//Title      : Islamic Republic News Agency (IRNA) [ French ] 
//Created on : Oct 22, 2020, 6:21:09 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6595 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}