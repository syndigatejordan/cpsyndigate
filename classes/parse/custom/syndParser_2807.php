<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Press TV  [# publisher id = 198]
//Title      : Press TV [ French ] 
//Created on : Jul 14, 2016, 9:00:43 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2807 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}