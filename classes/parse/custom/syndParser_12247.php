<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Three Men on a Boat  [# publisher id = 1678]
//Title      : 3-Mob.com [ English ] 
//Created on : Jan 04, 2022, 6:52:54 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12247 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}