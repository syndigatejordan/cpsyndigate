<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : PIMA Ventures UK Ltd  [# publisher id = 1332]
//Title      : BOLT+ Extra Time [ English ] 
//Created on : Apr 29, 2020, 10:00:49 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6019 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  Public function getRawArticles(&$fileContents) {
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('channel', $fileContents);
    return $articles;
  }

  public function getStory(&$text) {
    $story = trim($this->textFixation($this->getCData($this->getElementByName('media:description', $text))));
    if (!empty($story)) {
      $story = "<p>$story</p>";
    }
    $keywords = trim($this->textFixation($this->getCData($this->getElementByName('media:tags', $text))));
    if (!empty($keywords)) {
      $keywords = "<p>Tags: $keywords</p>";
    }
    $story = $story . $keywords;
    return $story;
  }

  public function getHeadline(&$text) {
    $this->headline = $this->textFixation($this->getCData($this->getElementByName('media:title', $text)));
    return $this->headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    $date = trim(str_replace('/BST/is', '', $date));
    return date('Y-m-d', strtotime($date));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = trim($this->getElementByName('media:category', $text));
    return $cats;
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $textImages = $this->getElementsByName("item", $text);
    $image_caption = $this->textFixation($this->getCData($this->getElementByName('title', $textImages[1])));
    $matches = null;
    preg_match_all('/<media:content(.*?)medium="image"\/>/is', $textImages[1], $matches);
    foreach ($matches[0] as $match) {
      $this->addLog("getting article images");
      $image_caption = trim($this->getElementByName('media:title', $match));
      $imageInfo = syndParseHelper::getImgElements($match, 'media:content', 'url');
      $image_full_path = $this->currentDir . $imageInfo[1];
      $original_name = explode('.', $imageInfo[1]);
      if (file_exists($image_full_path)) {
        //the video name is same as the image name here  		
        $name = $this->model->getImgReplacement($imageInfo[1], $this->currentDir, 6019);
        $img['img_name'] = str_replace(IMGS_PATH, IMGS_HOST, $name);
        $img['original_name'] = $imageInfo[1];
        $img['image_caption'] = $image_caption;
        $img['is_headline'] = true;
        array_push($imagesArray, $img);
      }
    }
    return $imagesArray;
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $matches = null;
    $videos = array();
    preg_match('/<media:content(.*?)medium="video"(.*?)>/is', $text, $matches);
    $matches = preg_replace("!\s+!", " ", $matches[0]);
    $videoname = syndParseHelper::getImgElements($matches, 'media:content', 'url');
    $link = str_replace(' ', '%20', basename($videoname[0]));
    if (!empty($link)) {
      $link = "https://pima-ventures-uk-ltd.s3.amazonaws.com/BOLT+Extra+Time+%236019/" . $link;
      $videoName = $this->textFixation($this->getCData($this->getElementByName('media:title', $text)));
      $video['video_name'] = $link;
      $video['original_name'] = $videoName;
      $video['video_caption'] = $videoName;
      $mimeType = syndParseHelper::getImgElements($matches, 'media:content', 'type');
      $mimeType = $mimeType[0];
      $video['mime_type'] = $mimeType;
      $date = $this->getElementByName('pubDate', $text);
      $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
      $videos[] = $video;
    }
    return $videos;
  }
  public function getArticleOriginalId($params = array()) {
    $articleOriginalId = strtolower($this->currentlyParsedFile) . $this->getElementByName('pubDate', $params['text']);
    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }
    return $this->title->getId() . '_' . sha1($articleOriginalId);
  }

}
