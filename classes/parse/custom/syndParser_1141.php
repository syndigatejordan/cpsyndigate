<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Project Pen LLC  [# publisher id =337 ]
// Title     : Project Pen Creativity Blog
///////////////////////////////////////////////////////////////////////////

class syndParser_1141 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('item', $fileContents);
    return $articles;
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $story = "<p>";
    $story .= trim($this->textFixation($this->getCData($this->getElementByName('description', $text))));
    $story = preg_replace("/\r\n|\r|\n/", "</p><p>", $story);
    $story .= "</p>";
    if (empty($story)) {
      return '';
    }
    return $story;
  }

  public function getHeadline(&$text) {
    $headline = trim(strip_tags($this->textFixation($this->getCData($this->getElementByName('title', $text)))));
    return $headline;
  }

  public function getArticleDate(&$text) {
    $date = trim($this->getElementByName('pubDate', $text));
    return date('Y-m-d', strtotime($date));
  }

//  public function getOriginalCategory(&$text) {
//    $this->addLog('getting article category');
//    $cats = $this->getCData($this->getElementsByName('category', $text));
//    $originalCats = array();
//
//    if (!empty($cats)) {
//      foreach ($cats as $cat) {
//        $originalCats[] = $this->textFixation($this->getCData($cat));
//      }
//    }
//    return implode(', ', $originalCats);
//  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $creator = trim($this->getElementByName('dc:creator', $text));
    return $creator;
  }

  protected function getImages(&$text) {

    $this->addLog("getting article images");
    $imagesArray = array();
    preg_match_all("/<enclosure[^>]+\>/i", $text, $img);
    $img = str_replace('<enclosure url="', '', $img[0][0]);
    $img = explode('"', $img);
    $imagePath = $img[0];
    if (!$imagePath) {
      return;
    }
    if ($image = $this->checkImageifCached($imagePath)) {
      // Image already parsed..
      return;
    }
    $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

    if (!$copiedImage) {
      echo "no pahr";
      return;
    }
    $images = $this->getAndCopyImagesFromArray(array($copiedImage));
    $images[0]['image_caption'] = $images[0]['image_caption'];
    $images[0]['is_headline'] = false;
    array_push($imagesArray, $images[0]);
    return $imagesArray;
  }

}
