<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Latvian ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_582 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority'		=>	'Piešķīrēja iestāde', 
										'Nature of Contract'		=>	'Līguma veids', 
										'Regulation of Procurement' =>	'Regulējumu iepirkuma', 
										'Type of bid required'		=>	'Veida cenu, kas nepieciešama', 
										'Award Criteria'			=>	'Piešķiršanas kritēriji', 
										'Original CPV'				=> 	'Sākotnējā CPV', 
										'Country'					=>	'Valsts', 
										'Document Id' 				=>	'Dokumentu Id', 
										'Type of Document'			=>	'Dokumenta veids', 
										'Procedure'					=>	'Procedūra', 
										'Original Language'			=>	'Oriģinālvalodā', 
										'Current Language'			=>	'Pašreizējā valoda');	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('lv');
		}
	}