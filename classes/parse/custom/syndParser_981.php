<?php

////////////////////////////////////////////////////////////////////////
// Publisher :
// Title     : RTT News (via HT Media Ltd.)
////////////////////////////////////////////////////////////////////////

class syndParser_981 extends syndParseHtMedia {
	protected function getStory(&$text) {
		$this -> addLog("getting article text");

		$body = $this -> textFixation($this -> getElementByName('BodyContent', $text));
		$body = strip_tags($body, '<p>');
		return $body;
	}

	protected function getHeadline(&$text) {
		$this -> addLog("getting article headline");

		$headline = $this -> getElementByName('Headline1', $text);
		return $this -> textFixation($headline);
	}

	protected function getArticleDate(&$text) {
		$this -> addLog("getting article date");
		$date = $this -> getElementByName('ContentDate', $text);
		$date = date('Y-m-d',strtotime($date));
		return $date;
	}

}
