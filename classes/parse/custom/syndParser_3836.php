<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BBC World Service  [# publisher id = 1192]
//Title      : BBC Arabic - Features [ Arabic ] 
//Created on : Sep 14, 2021, 1:27:06 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3836 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}