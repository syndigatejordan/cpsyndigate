<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Business Wire, Inc. / Berkshire Hathaway  [# publisher id = 1251]
//Title      : Business Wire [ Arabic ] 
//Created on : Aug 08, 2019, 6:03:58 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5645 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}