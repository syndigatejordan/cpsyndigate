<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Caxton Ltd  [# publisher id = 1148]
//Title      : Rekord Centurion [ English ] 
//Created on : Aug 30, 2017, 10:55:24 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3144 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}