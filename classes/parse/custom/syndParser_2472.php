<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Amman Chamber Of Commerce  [# publisher id = 810]
//Title      : Amman Chamber Of Commerce (ACC) E-Magazine [ Arabic ] 
//Created on : Feb 02, 2016, 12:10:30 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2472 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}