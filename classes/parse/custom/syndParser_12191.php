<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Afghanistan Analysts Network  [# publisher id = 1639]
//Title      : Afghanistan Analysts Network [ Pashto ] 
//Created on : Oct 03, 2021, 10:42:43 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12191 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ps'); 
	} 
}