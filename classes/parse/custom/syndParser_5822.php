<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : RASANAH - International Institute for Iranian Studies  [# publisher id = 1264]
//Title      : RASANAH - Iranian Daily Report [ Arabic ] 
//Created on : May 11, 2020, 9:20:00 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5822 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $fileContents = preg_replace('!\s+!', ' ', $fileContents);

    $data = null;
    if (preg_match("/<DATE>(.*?)<\/DATE>/is", $fileContents, $data)) {
      $data = $this->dateMapper($data[1]);
    } else {
      $data = date("Y-m-d");
    }
    $fileContents = preg_replace('/(.*?)<\/DATE>/', '', $fileContents);
    $fileContents = str_replace('<HEADLINE> الأخبار: </HEADLINE>', '', $fileContents);
    $matches = null;
    preg_match_all("/<HEADLINE>(.*?)<\/TEXT>/is", $fileContents, $matches);
    $matches = $matches[0];
    foreach ($matches as &$match) {
      $match.="<DATE>$data</DATE>";
    }
    return $matches;
  }

  public function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return trim($this->textFixation($this->getElementByName('HEADLINE', $text))) . " " . trim($this->getElementByName('DATE', $text));
  }

  public function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = trim($this->getElementByName('DATE', $text));
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  public function getStory(&$text) {
      $this->addLog("getting article text");
      $story = trim($this->textFixation($this->getElementByName('TEXT', $text)));

      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $story = preg_replace('/<img[^>]+\>/i', '', $story);
      return $story;

  }

  private function dateMapper($date) {
    $date = str_replace("2020م", "2020", $date);
    $date = trim($date);
    $monthsEn = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',);
    $monthsAr = array('يناير', 'فبراير', 'مارس', 'أبريل', 'مايو', 'يونيو', 'يوليو', 'أغسطس', 'سبتمبر', 'أكتوبر', 'نوفمبر', 'ديسمبر',);

    $daysEn = array('Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri',);
    $daysAr = array('السبت', 'الأحد', 'الإثنين', 'الثلاثاء', 'الأربعاء', 'الخميس', 'الجمعة',);

    $date = str_replace($daysAr, $daysEn, $date);
    $date = str_replace($monthsAr, $monthsEn, $date);
    return $this->dateFormater($date);
  }

}
