<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Publicdomainvectors.org  [# publisher id = 1319]
//Title      : Publicdomainvectors.org [ English ] 
//Created on : Mar 03, 2020, 9:53:32 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5953 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
