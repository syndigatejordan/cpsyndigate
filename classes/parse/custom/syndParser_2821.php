<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Assir Est. for Press & Publishing  [# publisher id = 999]
//Title      : Al Watan Online [ Arabic ] 
//Created on : Aug 18, 2016, 10:23:39 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2821 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->textFixation($this->getElementByName('title', $text));
    $headline = preg_replace('/[^\x{0600}-\x{06FF}A-Za-z0-9 !@#$%^&*().\'"]/u', '', $headline);
    return $headline;
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $images = $this->textFixation($this->getElementByName('images', $text));
    $images = str_replace('<img>', '<image>', $images);
    $images = str_replace('</img>', '</image>', $images);
    $images = $this->getElementsByName('image', $images);
    foreach ($images as $imgName) {
      $img = array();
      $this->addLog("getting image");
      $imageDescription = $this->textFixation($this->getElementByName('img_description', $imgName));
      $image_Name = trim($this->textFixation($this->getElementByName('img_path', $imgName)));
      $path = pathinfo($image_Name);
      $extension = strtolower($path["extension"]);
      $imageExtension = array( "pdf", "zip");
      if (!in_array($extension, $imageExtension)) {
        continue;
      }
        $img['img_name'] = $image_Name;
        $img['original_name'] = $image_Name;
        $img['image_caption'] = $imageDescription;
        $img['is_headline'] = FALSE;
        $img['image_original_key'] = sha1($image_Name);
        array_push($imagesArray, $img);
    }
      $this->story = $this->getCData($this->getElementByName('fulltext', $text));
      $this->story = str_replace("&amp;", "&", $this->story);
      $this->story = $this->textFixation($this->story);
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return $imagesArray;

      $this->story = preg_replace('/(<[^>]+) onerror=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) data-was-processed=".*?"/i', '$1', $this->story);

      preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
      foreach ($imgs[0] as $img) {
          $this->addLog("getting article images");
          $image_caption = '';
          $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
          if (!empty($image_caption[0])) {
              $image_caption = $image_caption[0];
      } else {
        $image_caption = syndParseHelper::getImgElements($img, 'img', 'title');
        if (!empty($image_caption[0])) {
          $image_caption = $image_caption[0];
        } else {
          $image_caption = "";
        }
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePathold = $imageInfo[0];
      $imagePath = preg_replace("/\?(.*)/is", "", $imagePathold);
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        $this->addLog("No Path");
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePathold, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = sha1($imageUrl) . ".jpeg";
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );

      $ch = curl_init($imageUrl);
      curl_setopt_array($ch, $options);
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
