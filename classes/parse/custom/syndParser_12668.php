<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : 256 Business and Corporate News Limited  [# publisher id = 1827]
//Title      : 256 Business News [ English ] 
//Created on : Jul 17, 2022, 12:07:00 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12668 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}