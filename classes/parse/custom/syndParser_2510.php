<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Asianet-Pakistan (Pvt) Ltd.  [# publisher id = 83]
//Title      : Daily Guide Network [ English ] 
//Created on : Mar 04, 2020, 1:27:06 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2510 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}