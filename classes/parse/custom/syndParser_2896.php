<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bleed This Music (BMI) / Tony K Music  [# publisher id =1047 ] 
//Title      : Tony K [ English ] 
//Created on : Nov 15, 2016, 10:44:23 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2896 extends syndParseXmlContent {

  // Available video types
  private $videoTypes = array('mp4' => '.mp4', 'flv' => '.flv', 'mpg' => '.mpg', 'wmv' => '.wmv', 'rm' => '.rm', 'rm(56k)' => '-56k.rm', 'wmv(56)' => '-56k.wmv', 'rm(256k)' => '-256k.rm', 'wmv(256k)' => '-256k.wmv');

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';

    require_once('/usr/local/syndigate/lib/getid3/getid3/getid3.php');
  }

  protected function getRawArticles(&$fileContents) {
    $this->loadCurrentDirectory();

    $this->addLog("getting raw articles text");
    return $this->getElementsByName('article', $fileContents);
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = $this->textFixation($this->getElementByName('content', $text));
    return $story;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return $this->textFixation($this->getElementByName('headline', $text));
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('date', $text);
    $date = preg_replace('/T(.*)/is', '', $date);
    return date('Y-m-d', strtotime($date));
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return trim($this->textFixation($this->getElementByName('author', $text)));
  }

//  private function loadVideoName($text) {
//    $videoName = $this->textFixation($this->getElementByName('video', $text));
//    $videoName = explode(".", $videoName);
//    $this->videoName = $videoName[0];
//  }
  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $video_Name = $this->textFixation($this->getElementByName('video', $text));
    $videoName = $this->currentDir . $video_Name;
    $video_Name_arr = explode('.', $video_Name);
    $videos = array();
    $videoInformation = array();
    $neededInfo = array('filesize', 'fileformat', 'encoding', 'mime_type', 'playtime_seconds', 'bitrate', 'playtime_string', 'video');

    //foreach ($this->videoTypes as $videoLable => $videoType) {
    if (in_array('.' . $video_Name_arr[1], $this->videoTypes)) {
      if (file_exists($videoName)) {

        $getID3 = new getID3;
        $realVideoPath = str_replace($this->title->getParseDir(), $this->title->getHomeDir(), $videoName);
        $videoInfo = $getID3->analyze($realVideoPath);

        foreach ($neededInfo as $key => $value) {

          if (in_array($value, $videoInfo)) {
            switch ($value) {
              case 'video':
                $videoInformation['width'] = $videoInfo[$value]['resolution_x'];
                $videoInformation['height'] = $videoInfo[$value]['resolution_y'];
                break;
              case 'bitrate':
                $videoInformation[$value] = floor($videoInfo[$value] / 1024);
                break;

              default:
                $videoInformation[$value] = $videoInfo[$value];
                break;
            }
          }
        }


        //$name = $this->model->getVidReplacement($this->videoName . $videoType, $this->currentDir, 238) ;
        //$video['video_name']    	= str_replace(VIDEOS_PATH, VIDEOS_HOST, $name);

        $video['video_name'] = $this->getAndCopyVideosFromArray($videoName);

        //$bitRate 					= $videoInfo['bitrate'] / 1000;
        $mimeType = $videoInfo['mime_type'];
        $video['original_name'] = $videoName;
        $video['video_caption'] = $video_Name_arr[0];
        //$video['bit_rate']       	= $this->getBitRateFromName($videoLable);
        $video['bit_rate'] = json_encode($videoInformation);    //$bitRate . ' kbps';
        $video['mime_type'] = $mimeType;
        $video['added_time'] = date('Y-m-d h:i:s', filemtime($video['original_name']));
        $videos[] = $video;
      }
    }
    return $videos;
  }

//  private function getBitRateFromName($name) {
//    $bit = explode("(", $name);
//
//    if (is_array($bit) & count($bit) > 1) {
//      $bit = $bit[1];
//      $bit = explode(")", $bit);
//      return $bit[0];
//    } else {
//      return '';
//    }
//  }
}

?>
