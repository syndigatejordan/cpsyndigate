<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Okaz Organization for Press & Publication  [# publisher id =511 ]
//Title      : The Saudi Gazette [ English ] 
//Created on : Jul 06, 2017, 7:27:16 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1526 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
