<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Central News Agency (CNA)  [# publisher id = 1406]
//Title      : Central News Agency (CNA) [ Spanish ] 
//Created on : Oct 21, 2020, 11:27:18 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6548 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}