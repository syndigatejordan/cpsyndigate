<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Cavendish Maxwell  [# publisher id = 1167]
//Title      : Cavendish Maxwell Blog [ English ] 
//Created on : Apr 17, 2019, 8:22:20 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5543 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}