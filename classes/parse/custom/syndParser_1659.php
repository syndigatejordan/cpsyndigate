<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : 7awi.com 
//Title     : ArabsTurbo [ Arabic ]
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_1659 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('item', $fileContents);
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->getCData($this->getElementByName('title', $text));
    return $this->textFixation($headline);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('time', $text);
    return date('Y-m-d', strtotime($date));
  }

//  protected function getAbstract(&$text) {
//
//    $this->addLog('Getting article summary');
//    $summary = $this->getCData($this->getElementByName('shortDescription', $text));
//    return $summary;
//  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = $this->getCData($this->getElementByName('fullDescription', $text));
    $story = str_replace("//www.youtube.com", "http://www.youtube.com", $story);
    preg_match_all('#<iframe[^>]+>#i', $story, $videos);
    $videolink = "";
    foreach ($videos[0] as $video) {
      $video = explode('src="', $video);
      $video = $video[1];
      $video = explode('"', $video);
      $video = $video[0];
      $videolink .= '<p><a href="' . $video . '">' . $video . '</a></p>';
    }
    $story = $story . $videolink;
    return $this->textFixation($story);
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $Category = array('New_news' => 'أخبار-جديدة', 'Test_drive' => 'تجارب-قيادة', 'Compare_and_reports' => 'مقارنة-و-تقارير', 'Hybrid_cars' => 'سيارات-هجينة', 'Latest_videos' => 'أحدث-الفيديوهات', 'Car_accidents' => 'حوادث-سيارات', 'Moto_G_B' => 'موتو-جي-بي', 'Formula' => 'فورمولا-1', 'Racing_speed_rally' => 'سباقات-السرعة-و-الراليات', 'Drvat' => 'درفت', 'Videos' => 'فيديوهات');
    $filename = basename($this->currentlyParsedFile);
    $filename = str_replace('.xml', '', $filename);
    $cat = $Category[$filename];
    return $cat;
  }

//  protected function getAuthor(&$text) {
//    $this->addLog("getting article author");
//    return $this->textFixation($this->getCData($this->getElementByName('dc:creator', $text)));
//  }

  protected function getImages(&$text) {
    $this->addLog("getting article images");
    $this->imagesArray = array();
    $imagePath = $this->getCData($this->getElementByName('bigImage', $text));
    if ($this->checkImageifCached($imagePath)) {
      // Image already parsed..
      return;
    }
    $imagePath = str_replace(' ', '%20', $imagePath);
    $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
    $images = $this->getAndCopyImagesFromArray(array($copiedImage));
    $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
    $images[0]['is_headline'] = false;
    array_push($this->imagesArray, $images[0]);

    $story = $this->textFixation($this->getCData($this->getElementByName('fullDescription', $text)));
    preg_match_all(
            "/<img[^>]+\>/i", $story, $imgs);
    foreach ($imgs[0] as $img) {

      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $text = str_replace($img, $new_img, $text);

      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

}