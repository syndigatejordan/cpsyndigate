<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ansar Press  [# publisher id = 1643]
//Title      : Ansar Press [ English ] 
//Created on : Oct 04, 2021, 7:59:43 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12198 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}