<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Jeddah Chamber of Commerce and Industry (JCCI)  [# publisher id = 780]
//Title      : Jeddah Chamber [ English ] 
//Created on : Aug 03, 2021, 10:40:55 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2422 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}