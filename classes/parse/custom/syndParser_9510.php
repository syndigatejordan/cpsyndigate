<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Glean.info  [# publisher id = 1525]
//Title      : Omnibus News Feed [ English ] 
//Created on : Feb 18, 2021, 9:57:49 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_9510 extends   syndParseXmlContent {


    public function customInit() {
        parent::customInit();
        $this->charEncoding = 'UTF-8';
        $this->defaultLang = $this->model->getLanguageId('en');
        $this->extensionFilter = 'xml';
    }

    protected function getRawArticles(&$fileContents) {
        $this->loadCurrentDirectory();

        $this->addLog("getting raw articles text");
        $Article = null;
        preg_match_all('/<article>(.*?)<\/article>/is', $fileContents, $Article);
        return $Article[1];
    }

    protected function getStory(&$text) {
        $this->addLog("getting article text");
        $story = $this->textFixation($this->getElementByName('content', $text));
        return $story;
    }

    protected function getHeadline(&$text) {
        $this->addLog("getting article headline");
        return trim($this->textFixation($this->getElementByName('articletitle', $text)));
    }

    protected function getArticleDate(&$text) {
        $this->addLog("getting article date");
        $date =trim($this->getElementByName('published', $text));
        return date('Y-m-d', strtotime($date));
    }

    protected function getAuthor(&$text) {
        $this->addLog("getting article author");
        return trim($this->textFixation($this->getElementByName('author', $text)));
    }
    public function getArticleReference(&$text)
    {
        $this->addLog("getting article reference");
        return $this->textFixation($this->getCData($this->getElementByName('url', $text)));
    }
    public function getOriginalCategory(&$text) {
        $this->addLog('getting article category');
        return $this->textFixation($this->getCData($this->getElementByName('mediatype', $text)));
    }
}
