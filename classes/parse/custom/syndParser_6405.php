<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : World Stage Limited  [# publisher id = 1354]
//Title      : World Stage [ English ] 
//Created on : Jul 25, 2021, 7:53:40 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_6405 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
