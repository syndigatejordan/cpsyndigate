<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : 7 Dimensions Media FZE  [# publisher id = 1216]
//Title      : Air Cargo Update [ English ] 
//Created on : Jul 15, 2019, 6:52:35 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5460 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}