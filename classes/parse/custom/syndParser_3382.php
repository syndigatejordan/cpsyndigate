<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Nile Radio Productions  [# publisher id = 1165]
//Title      : Nogoum FM [ Arabic ] 
//Created on : Aug 15, 2021, 9:32:56 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3382 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}