<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Standard Publications Ltd.  
//Title     : The Malta Independent [ English ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1594 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getImages(&$text) {
      $this->story = $this->getCData($this->getElementByName('fulltext', $text));
      $id = $this->getCData($this->getElementByName('id', $text));
      $this->story = html_entity_decode($this->story, ENT_NOQUOTES, 'UTF-8');
      $this->story = $this->textFixation($this->story);
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();

      $this->imagesArray = array();
      preg_match_all("/<img[^>]+\>/i", $this->story, $imgs);

      foreach ($imgs[0] as $img) {
          $this->addLog("getting article images");
          $image_caption = '';
          $image_caption = explode('title="', $img);
          if (!empty($image_caption[1])) {
              $image_caption = explode('"', $image_caption[1]);
              $image_caption = $image_caption[0];
      } else {
        $image_caption = "";
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath, $id);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = str_replace('%20', ' ', $image_caption);
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['is_headline'] = false;
      $this->story = str_replace($image_caption, "", $this->story);
      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl, $id) {
    $baseName = basename($imageUrl);
    preg_match('/\?f=(.*?)&/is', $baseName, $fid);
    $fid = $fid[1];
    $baseName = $id . '_' . $fid . '_' . rand(1, 100000) . '.jpeg';
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $curl_handle = curl_init();
      curl_setopt($curl_handle, CURLOPT_URL, $imageUrl);
      curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl_handle, CURLOPT_USERAGENT, 'php');
      $imgContent = curl_exec($curl_handle);
      curl_close($curl_handle);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
