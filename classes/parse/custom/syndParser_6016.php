<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Exchange Data International (EDI)  [# publisher id = 174]
//Title      : African Financial & Economic Data (AFED) - Sector Focus [ English ] 
//Created on : Apr 09, 2020, 7:34:04 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6016 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}