<?php

////////////////////////////////////////////////////////////////////////
// Publisher : India Today Group  
// Titles    : Cosmopolitan
////////////////////////////////////////////////////////////////////////

class syndParser_1098 extends syndParseRss {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getRawArticles(&$fileContents) {
    $this->addLog("Getting raw articles text");
    $rawArticles = explode('<section>', $fileContents);
    foreach ($rawArticles as &$raw) {
      $raw = '<section>' . $raw;
    }
    return $rawArticles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");

    $headline = $this->getCData($this->getElementByName('headline', $text));
    return $headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('edition', $text);
    $date = preg_replace('/ \-(.*)/is', '', $date);
    $date = date('Y-m-d', strtotime($date));
    return $date;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return trim($this->getCData($this->getElementByName('byline', $text)));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getElementsByName('section', $text);
    $originalCats = array();
    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = $this->textFixation($this->getCData($cat));
      }
    }
    return implode(', ', $originalCats);
  }

  protected function getAbstract(&$text) {
    $this->addLog('getting article summary');
    $summary = strip_tags($this->textFixation($this->getCData($this->getElementByName('introtext', $text))));
    return $summary;
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    return $this->story;
  }

  protected function getImages(&$text) {
    $this->story = trim($this->textFixation($this->getCData($this->getElementByName('bodytext', $text))));
    $imagesArray = array();
    preg_match_all("/<img[^>]+\>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");

      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];

      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
