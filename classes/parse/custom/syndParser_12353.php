<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BSE India  [# publisher id = 1746]
//Title      : Financial Results (quarterly, half-yearly, yearly) [ English ] 
//Created on : Jan 25, 2022, 2:30:31 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12353 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}