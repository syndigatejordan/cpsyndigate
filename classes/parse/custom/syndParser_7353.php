<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Neotrope  [# publisher id = 841]
//Title      : eNewsChannels [ English ] 
//Created on : Sep 19, 2021, 10:19:06 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7353 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}