<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Buzz Future LLC  [# publisher id = 519]
//Title      : Eurasia Review [ English ] 
//Created on : Jul 13, 2021, 7:08:30 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1556 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}