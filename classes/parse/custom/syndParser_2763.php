<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Post Newspapers Ltd  [# publisher id = 962]
//Title      : The Post [ English ] 
//Created on : May 22, 2016, 12:59:16 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2763 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}