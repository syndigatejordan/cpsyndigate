<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : openDemocracy Limited  [# publisher id = 1247]
//Title      : openDemocracy [ Portuguese ] 
//Created on : May 21, 2019, 8:02:30 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5628 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}