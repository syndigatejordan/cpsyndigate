<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dar Al-Bilad Printing and Media  [# publisher id = 928]
//Title      : Attamaddon [ Arabic ] 
//Created on : May 16, 2016, 10:51:44 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2723 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}