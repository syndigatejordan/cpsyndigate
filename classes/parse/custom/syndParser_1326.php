<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Islamic Ideology Dissemination Organization (IIDO)  [# publisher id = 149]
//Title      : Mehr News Agency (MNA) [ Persian ] 
//Created on : Oct 14, 2020, 11:30:17 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1326 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fa'); 
	} 
}