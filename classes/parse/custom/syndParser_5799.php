<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : FARS News Agency  [# publisher id = 129]
//Title      : FARS News Agency [ English ] 
//Created on : Oct 02, 2019, 10:00:25 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5799 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}