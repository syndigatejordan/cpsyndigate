<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Watani Printing and Publishing Group  
//Title     : Watani [ Arabic ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1083 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }
}
