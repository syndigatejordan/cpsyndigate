<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : L'Express DZ  [# publisher id = 1620]
//Title      : L'Express DZ [ French ] 
//Created on : Sep 01, 2021, 11:49:02 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12164 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}