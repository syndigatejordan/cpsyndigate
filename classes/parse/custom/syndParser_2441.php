<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Tripoli and North Lebanon, Chamber of Commerce, Industry and Agriculture  [# publisher id = 792]
//Title      : Tripoli and North Lebanon, Chamber of Commerce, Industry and Agriculture News [ Arabic ] 
//Created on : Feb 01, 2016, 8:48:52 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2441 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}