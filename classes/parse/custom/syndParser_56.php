<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sot Al-Iraq  [# publisher id =33 ] 
//Title      : Sot Al-Iraq [ Arabic ] 
//Created on : Mar 26, 2017, 11:25:18 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_56 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $this->story = preg_replace('/<style(.*?)<\/style>/is', '', $this->story);
    $this->story = preg_replace('/<figure(.*?)<\/figure>/is', '', $this->story);
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
    $this->story = strip_tags($this->story, '<p><br><strong><b><u><i><img></iframe>');
    $this->story = trim(preg_replace('!\s+!', ' ', $this->story));
    $this->story = str_replace('(adsbygoogle = window.adsbygoogle || []).push({});', '', $this->story);
    return $this->story;
  }

  protected function getImages(&$text) {
      $imagesArray = array();

      $this->story = $this->getCData($this->getElementByName('fulltext', $text));
      $this->story = str_replace("&nbsp;", " ", $this->story);
      $this->story = $this->textFixation($this->story);
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();
      
      preg_match_all("/<img(.*?)>/i", $this->story, $imgs);
      foreach ($imgs[0] as $img) {
          $this->addLog("getting article images");
          $image_caption = '';
          $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
          if (!empty($image_caption[0])) {
              $image_caption = $image_caption[0];
          } else {
              $image_caption = "";
          }
      if (!$this->is_arabic($image_caption)) {
        $image_caption = iconv("UTF-8", "ISO-8859-1", $image_caption);
      }


      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      $imagePath = preg_replace("/\?(.*)/is", "", $imagePath);
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    sleep(1);
    $baseName = sha1($imageUrl) . ".jpeg";
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );

      $ch = curl_init($imageUrl);
      curl_setopt_array($ch, $options);
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

  private function is_arabic($str) {
    if (mb_detect_encoding($str) !== 'UTF-8') {
      $str = mb_convert_encoding($str, mb_detect_encoding($str), 'UTF-8');
    }
    preg_match_all('/.|\n/u', $str, $matches);
    $chars = $matches[0];
    $arabic_count = 0;
    $latin_count = 0;
    $total_count = 0;
    foreach ($chars as $char) {
      //$pos = ord($char); we cant use that, its not binary safe 
      $pos = $this->uniord($char);
      //echo $char . " --> " . $pos . PHP_EOL;

      if ($pos >= 1536 && $pos <= 1791) {
        $arabic_count++;
      } else if ($pos > 123 && $pos < 123) {
        $latin_count++;
      }
      $total_count++;
    }
    if (($arabic_count / $total_count) > 0.6) {
      // 60% arabic chars, its probably arabic
      return true;
    }
    return false;
  }

  private function uniord($u) {
    // i just copied this function fron the php.net comments, but it should work fine!
    $k = mb_convert_encoding($u, 'UCS-2LE', 'UTF-8');
    $k1 = ord(substr($k, 0, 1));
    $k2 = ord(substr($k, 1, 1));
    return $k2 * 256 + $k1;
  }

}
