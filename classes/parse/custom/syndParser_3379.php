<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : EMDP - Egypt Media Development Program  [# publisher id = 1164]
//Title      : Mantiqti [ Arabic ] 
//Created on : Jan 04, 2018, 1:41:49 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3379 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}