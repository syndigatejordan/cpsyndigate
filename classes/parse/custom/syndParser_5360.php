<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Palestinian Media and Communications Company  [# publisher id = 1210]
//Title      : Palestinian Media and Communications Company [ Arabic ] 
//Created on : Feb 14, 2019, 8:14:40 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5360 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}