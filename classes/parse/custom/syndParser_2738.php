<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ONIP  [# publisher id = 942]
//Title      : La Nation [ French ] 
//Created on : May 17, 2016, 12:02:38 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2738 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}