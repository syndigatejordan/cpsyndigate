<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Infomedia / Opoint Technology  [# publisher id = 1534]
//Title      : Global News Firehose (Spanish) [ Spanish ] 
//Created on : Sep 23, 2021, 12:48:04 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10978 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}