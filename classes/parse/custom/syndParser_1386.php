<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al-Masry Al-Youm For Press, Publishing, Distribution & Advertising  [# publisher id = 419]
//Title      : Egypt Independent [ English ] 
//Created on : Aug 10, 2021, 7:36:18 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1386 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}