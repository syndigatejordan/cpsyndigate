<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Liberian Observer Corporation (LOC)  [# publisher id = 1133]
//Title      : Liberian Observer Online [ English ] 
//Created on : Jan 21, 2019, 2:07:38 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3118 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}