<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Human Rights Watch  [# publisher id = 1302]
//Title      : Human Rights Watch - News [ Turkish ] 
//Created on : Feb 29, 2020, 2:00:25 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5940 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('tr');
  }
}
