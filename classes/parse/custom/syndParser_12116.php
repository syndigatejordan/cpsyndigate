<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Angola Press News Agency (ANGOP)  [# publisher id = 1613]
//Title      : Angola Press News Agency (ANGOP) [ English ] 
//Created on : Aug 10, 2021, 9:32:13 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12116 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}