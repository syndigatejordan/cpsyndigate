<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Media OutReach Newswire  [# publisher id = 1172]
//Title      : Media OutReach Newswire [ Arabic ] 
//Created on : Aug 03, 2021, 1:07:01 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3434 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}