<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : JSC “Teleradiocompany LUX”  [# publisher id = 1603]
//Title      : 24 News Channel [ Ukrainian ] 
//Created on : Aug 09, 2021, 12:26:42 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11947 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('uk'); 
	} 
}