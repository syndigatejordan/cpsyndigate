<?php

////////////////////////////////////////////////////////////////////////
// Publisher : Big News Network FZ LLC  [# publisher id =408 ] 
// Titles    : Big News Network.com [English]
////////////////////////////////////////////////////////////////////////
class syndParser_1341 extends syndParseCms {
  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
