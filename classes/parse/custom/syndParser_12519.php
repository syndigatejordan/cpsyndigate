<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BBC World Service  [# publisher id = 1192]
//Title      : BBC Serbian - News [ Serbian ] 
//Created on : Mar 15, 2022, 12:33:16 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12519 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('sr'); 
	} 
}