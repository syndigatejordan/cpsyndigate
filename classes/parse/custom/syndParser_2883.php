<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : SJ Digital LLP  [# publisher id = 1043]
//Title      : News Crab [ English ] 
//Created on : Aug 15, 2021, 9:17:41 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2883 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}