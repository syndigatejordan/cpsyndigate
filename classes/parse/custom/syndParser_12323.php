<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : MH Sub I, LLC.  [# publisher id = 1718]
//Title      : Green Car Reports [ English ] 
//Created on : Jan 11, 2022, 1:50:36 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12323 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}