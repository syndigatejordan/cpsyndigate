<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Galadari Printing and Publishing LLC.  [# publisher id = 28]
//Title      : KT Blogs [ English ] 
//Created on : Aug 30, 2022, 1:41:29 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7404 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}