<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ​The Egyptian Center for Economic Studies (ECES)  [# publisher id = 1203]
//Title      : Our Economy and the World [ English ] 
//Created on : Jan 03, 2019, 9:13:44 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5329 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}