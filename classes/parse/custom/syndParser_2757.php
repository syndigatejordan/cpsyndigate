<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Guardian Newspapers Limited  [# publisher id = 957]
//Title      : The Guardian [ English ] 
//Created on : May 18, 2016, 10:43:23 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2757 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}