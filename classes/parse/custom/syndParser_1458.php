<?php

///////////////////////////////////////////////////////////////////////////
//Publisher  : Filing Services Canada Inc.  [# publisher id =486 ]
//Title      : FSCwire [ English ]
//Created on : Oct 16, 2018, 13:33:12 PM
//Author     : eyad
///////////////////////////////////////////////////////////////////////////

class syndParser_1458 extends syndParseXmlContent {

  public $story = array();

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('newsItem', $fileContents);
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");

    $headline = trim($this->getCData($this->getElementByName('headline', $text)));
    return $headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('contentCreated', $text);
    $date = preg_replace('/T(.*)/is', '', $date);
    $date = date('Y-m-d', strtotime($date));
    return $date;
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = preg_replace('/<style\\b[^>]*>(.*?)<\\/style>/s', '', $this->story);
    $this->story = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    $this->story = strip_tags($this->story, '<table><tr><td><span><p><br><strong><b><u><i><a><ifram>');
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return trim($this->textFixation($this->getCData($this->getElementByName('creator', $text))));
  }

  protected function getImages(&$text) {
      $imagesArray = array();
      $this->story = str_replace('&nbsp;', '', $this->getElementByName('body', $text));
      $this->story = $this->textFixation($this->getCData($this->story));

      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();


      $imgs = NULL;
      preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
      foreach ($imgs[0] as $img) {
          $this->addLog("getting article images");
          $image_caption = '';
          $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
          if (!empty($image_caption[0])) {
              $image_caption = trim($image_caption[0]);
          } else {
              $image_caption = "";
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (preg_match("/img.ashx\?id\=/is", $imagePath)) {
        continue;
      }
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no path";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
