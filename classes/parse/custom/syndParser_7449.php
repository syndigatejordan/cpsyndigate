<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ouestaf  [# publisher id = 1496]
//Title      : Ouestaf [ French ] 
//Created on : Sep 19, 2021, 10:20:54 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7449 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}