<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Standard Group  [# publisher id = 1155]
//Title      : Enterprise [ English ] 
//Created on : May 19, 2022, 7:57:20 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12407 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}