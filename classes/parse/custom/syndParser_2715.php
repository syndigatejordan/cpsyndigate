<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dar Al Qabas Press, Printing and Publishing  [# publisher id = 921]
//Title      : Al Qabas [ Arabic ] 
//Created on : Jun 27, 2019, 5:54:40 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2715 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}