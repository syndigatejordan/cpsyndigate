<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Polish Press Agency (PAP)  [# publisher id = 1440]
//Title      : Polish Press Agency (PAP) [ Polish ] 
//Created on : Sep 23, 2021, 8:10:08 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6728 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pl'); 
	} 
}