<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : News Media  [# publisher id = 1533]
//Title      : News Direct [ English ] 
//Created on : Sep 23, 2021, 8:15:26 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10151 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}