<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Saudi Press Agency (SPA)  [# publisher id = 133]
//Title      : Saudi Press Agency (SPA) [ Chinese ] 
//Created on : Oct 12, 2020, 12:11:38 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6650 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh'); 
	} 
}