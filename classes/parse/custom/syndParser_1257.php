<?php 
		///////////////////////////////////////////////////////////////////////////////////// 
		//Publisher : Omani Establishment for Press, Printing, Publishing & Distribution LLC  
		//Title     : Oman Tribune [ English ] 
		/////////////////////////////////////////////////////////////////////////////////////
		
		class syndParser_1257 extends syndParseCms { 
			 public function customInit() { 
				 parent::customInit(); 
				 $this->defaultLang = $this->model->getLanguageId('en'); 
			} 
		}