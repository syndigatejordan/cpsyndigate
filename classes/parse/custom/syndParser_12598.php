<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Fusion Media Limited  [# publisher id = 304]
//Title      : Investing.com India Edition [ English ] 
//Created on : May 12, 2022, 7:48:20 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12598 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}