<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Techhive Limited  [# publisher id = 1465]
//Title      : Information Nigeria [ English ] 
//Created on : Sep 19, 2021, 10:18:17 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7348 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}