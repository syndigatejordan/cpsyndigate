<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Gender Links  
//Title     : Gender Links Opinion and Commentary Service [ French ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1153 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('fr');
  }

}
