<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ITWeb Limited  [# publisher id = 503]
//Title      : ITWeb [ English ] 
//Created on : Aug 10, 2021, 8:07:04 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1504 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}