<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : Yemen Observer Publishing House
//Title     : Yemen Observer  [ English ]
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1049 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $date = strtotime($this->getElementByName('pubDate', $text));
    if ($date < 1430431200) {
      return "";
    }
    return preg_replace("/<img[^>]+\>/i", '', $this->story);
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $date = strtotime($this->getElementByName('pubDate', $text));
    if ($date < 1430431200) {
      return $imagesArray;
    }
    $this->story = $this->textFixation($this->getCData($this->getElementByName('description', $text)));

    preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = explode('alt="', $img);
      $image_caption = explode('"', $image_caption[1]);
      $image_caption = $image_caption[0];
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
