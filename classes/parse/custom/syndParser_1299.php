<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Independent Newspapers (Pty) Limited  [# publisher id = 392]
//Title      : The Independent on Saturday [ English ] 
//Created on : Jul 29, 2021, 8:39:57 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1299 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}