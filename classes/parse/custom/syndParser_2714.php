<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : International Media Company  [# publisher id = 920]
//Title      : Al Manar [ Arabic ] 
//Created on : May 16, 2016, 9:25:41 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2714 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}