<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : NEW STATESMAN MEDIA GROUP LTD  [# publisher id = 1774]
//Title      : Capital Monitor [ English ] 
//Created on : Feb 28, 2022, 12:39:30 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12496 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}