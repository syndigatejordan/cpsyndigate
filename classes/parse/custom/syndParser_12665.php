<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Brown Cow Books (Pty) Ltd  [# publisher id = 1822]
//Title      : Brown Cow Books [ English ] 
//Created on : Jul 14, 2022, 12:00:48 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12665 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}