<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Maltese ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_584 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority' 		=> 	'Għoti Awtorità', 
										'Nature of Contract'		=> 	'Natura tal-kuntratt', 
										'Regulation of Procurement' => 	'Regolament tal-Akkwist', 
										'Type of bid required'		=> 	'Tip ta \'offerta meħtieġa', 
										'Award Criteria'			=>	'Kriterji ta \'attribuzzjoni', 
										'Original CPV'				=> 	'Oriġinali CPV', 
										'Country'					=>	'Pajjiż', 
										'Document Id' 				=>	'Id-dokument',
										'Type of Document'			=>	'Tip ta \'Dokument', 
										'Procedure'					=>	'Proċedura', 
										'Original Language'			=>	'Original Lingwa', 
										'Current Language'			=> 	'Kurrenti Lingwa');	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('mt');
		}
	}