<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : AgênciaBrasil (ABR)  [# publisher id = 1387]
//Title      : AgênciaBrasil (ABR) [ Spanish ] 
//Created on : Oct 01, 2020, 10:35:54 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6503 extends syndParseCms
{
    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('es');
    }

    protected function getImages(&$text)
    {
        $imagesArray = array();
        $this->story = $this->getCData($this->getElementByName('fulltext', $text));
        $this->story = str_replace("&amp;", "&", $this->story);
        $this->story = $this->textFixation($this->story);
        preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
        foreach ($imgs[0] as $img) {
            $this->addLog("getting article images");
            $image_caption = '';
            $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
            if (!empty($image_caption[0])) {
                $image_caption = $image_caption[0];
            } else {
                $image_caption = syndParseHelper::getImgElements($img, 'img', 'title');
                if (!empty($image_caption[0])) {
                    $image_caption = $image_caption[0];
                } else {
                    $image_caption = "";
                }
            }
            $imageInfo = syndParseHelper::getImgElements($img, 'img');
            $imagePathold = $imageInfo[0];
            $imagePath = preg_replace("/\?(.*)/is", "", $imagePathold);
            if (!$imagePath) {
                continue;
            }
            $imagePath = str_replace(' ', '%20', $imagePath);
            $new_img = array();
            $new_img['img_name'] = $imagePath;
            $new_img['original_name'] = $imagePath;
            $new_img['image_caption'] = $image_caption;
            $new_img['is_headline'] = FALSE;
            $new_img['image_original_key'] = sha1($imagePath);
            array_push($imagesArray, $new_img);
        }
        return $imagesArray;
    }
}