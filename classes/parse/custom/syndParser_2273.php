<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : AB Digital, Inc.  [# publisher id = 720]
//Title      : ABNewswire [ English ] 
//Created on : Sep 19, 2021, 7:02:59 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2273 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}