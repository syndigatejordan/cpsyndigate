<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IPS - Inter Press Service  [# publisher id = 1436]
//Title      : IPS - Inter Press Service [ Portuguese ] 
//Created on : Sep 12, 2021, 7:40:42 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6714 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}