<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Uzbekistan National News Agency (UzA)  [# publisher id = 74]
//Title      : Uzbekistan National News Agency (UzA) [ Uzbek ] 
//Created on : Aug 18, 2021, 11:58:18 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_218 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('uz'); 
	} 
}