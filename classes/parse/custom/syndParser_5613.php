<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Conversation  [# publisher id = 1241]
//Title      : The Conversation (Indonesia Edition) [ Indonesian ] 
//Created on : Sep 27, 2021, 7:46:29 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5613 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('id'); 
	} 
}