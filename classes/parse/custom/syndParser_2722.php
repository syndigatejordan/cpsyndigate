<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dar Assabah  [# publisher id = 927]
//Title      : Assabah Al Ousboui [ Arabic ] 
//Created on : May 16, 2016, 9:28:23 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2722 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->textFixation($this->getElementByName('title', $text));
    $headline = preg_replace('/[^\x{0600}-\x{06FF}A-Za-z0-9 !@#$%^&*().\'"]/u', '', $headline);
    return $headline;
  }

}
