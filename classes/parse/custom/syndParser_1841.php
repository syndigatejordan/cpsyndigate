<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : HT Media Ltd  [# publisher id =89 ] 
// Titles    : Hindustan Times (with Photos) (via HT Media Ltd.)
///////////////////////////////////////////////////////////////////////////
class syndParser_1841 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('Document', $fileContents);
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = $this->getElementByName('Body', $text);
    $story = explode("</Dateline>", $story);
    return $story[1];
  }

  protected function getHeadline(&$text) {
    //get headline
    $this->addLog("getting article headline");
    $headline = $this->getElementByName('PrimaryHeadline', $text);
    return $headline;
  }

  protected function getArticleDate(&$text) {
    //get article date
    $this->addLog("getting article date");
    $date = $this->getElementByName('ContentDate', $text);
    return date("Y-m-d", strtotime($date));
  }

  protected function getAuthor(&$text) {
    //get author
    $this->addLog("getting article author");
    $author = $this->getElementByName('Byline', $text);
    return $author;
  }

  protected function getImages(&$text) {
    // this contain just one image [ video image ]
    $this->addLog("getting article images");

    //$imageName = $this->currentDir . $this->videoName . '.jpg';
    $image_Name = $this->textFixation($this->getElementByName('name', $text));
    $image_full_path = $this->currentDir . $image_Name;
    $original_name = explode('.', $image_Name);
    if (file_exists($image_full_path)) {
      //the video name is same as the image name here  		
      $name = $this->model->getImgReplacement($image_Name, $this->currentDir, 1841);
      $img['img_name'] = str_replace(IMGS_PATH, IMGS_HOST, $name);
      $img['original_name'] = $original_name[0];
      $img['image_caption'] = $image_Name;
      $img['is_headline'] = true;
      $images[] = $img;
      return $images;
    } else {

      return false;
    }
  }

}
