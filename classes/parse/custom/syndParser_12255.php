<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Newstex, LLC  [# publisher id = 1519]
//Title      : CoinJournal [ Polish ] 
//Created on : Nov 22, 2021, 2:18:00 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12255 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pl'); 
	} 
}