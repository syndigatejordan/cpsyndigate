<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Daily Independent  [# publisher id = 633]
//Title      : Daily Independent [ English ] 
//Created on : Oct 15, 2019, 1:04:01 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1803 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}