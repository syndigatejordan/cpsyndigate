<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : AGERPRES (Romanian National News Agency)  [# publisher id = 1433]
//Title      : AGERPRES (Romanian National News Agency) [ Romanian ] 
//Created on : Dec 14, 2021, 12:44:03 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6697 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ro'); 
	} 
}