<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Confidente Newspaper  [# publisher id = 1448]
//Title      : Confidente Newspaper [ English ] 
//Created on : Sep 19, 2021, 8:57:15 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6965 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}