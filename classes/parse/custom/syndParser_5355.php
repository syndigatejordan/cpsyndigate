<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Shehab News Agency  [# publisher id = 1206]
//Title      : Shehab News Agency [ Arabic ] 
//Created on : Feb 14, 2019, 7:55:30 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5355 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}