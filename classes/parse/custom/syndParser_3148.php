<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Caxton & CTP publishers & printers  [# publisher id = 1152]
//Title      : The Citizen [ English ] 
//Created on : Aug 30, 2017, 10:07:15 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3148 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}