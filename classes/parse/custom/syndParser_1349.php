<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : South Sudan News Agency (SSNA)  [# publisher id = 413]
//Title      : South Sudan News Agency (SSNA) [ English ] 
//Created on : May 24, 2016, 11:12:48 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1349 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}