<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Maghreb Press Agency  [# publisher id = 1377]
//Title      : Maghreb Press Agency [ Italian ] 
//Created on : Oct 06, 2020, 1:16:50 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6472 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('it'); 
	} 
}