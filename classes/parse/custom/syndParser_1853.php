<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : AsiaNet – Pakistan  [# publisher id =211 ] 
// Titles    : Roznama Express [Arabic]
///////////////////////////////////////////////////////////////////////////

class syndParser_1853 extends syndParseXmlContent {
	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('ur');
		$this -> charEncoding = 'UTF-8';
		$this -> extensionFilter = 'txt';
	}

	public function getRawArticles(&$fileContents) {
		$exploded = explode('<Headline>', $fileContents);
		$contents = array();

		foreach ($exploded as $record) {
			if ($record) {
				$contents[] = '<Headline>' . $record;
			}
		}
		return $contents;
	}

	public function getStory(&$text) {
		//var_dump($text);
		$story = trim($this -> getElementByName('Text', $text));
		$story = '<p>' . str_replace(".\r\n", ".\r\n" . "</p>\r\n<p>", $story) . '</p>';
		return $story;
	}

	public function getHeadline(&$text) {
		return trim($this -> getElementByName('Headline', $text));
	}

	public function getArticleDate(&$text) {
		$date = trim($this -> getElementByName('Date', $text));
		//$date = explode('/', $date);
		//$date = $date[1] . '/' . $date[0] . '/' . $date[2];
		return date('Y-m-d', strtotime($date));
	}

}
