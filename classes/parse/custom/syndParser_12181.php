<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Adewale Ademowo  [# publisher id = 1632]
//Title      : InfoStride News [ English ] 
//Created on : Sep 30, 2021, 9:42:27 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12181 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}