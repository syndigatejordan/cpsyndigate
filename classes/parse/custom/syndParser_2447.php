<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sahar Universal Network  [# publisher id = 794]
//Title      : Sahar Universal Network [ French ] 
//Created on : Feb 02, 2016, 11:44:27 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2447 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}