<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ministry of Foreign Affairs of the Palestinain State  [# publisher id = 880]
//Title      : Ministry of Foreign Affairs of the Palestinain State - Ministry News  [ English ] 
//Created on : Feb 16, 2016, 9:31:36 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2629 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}