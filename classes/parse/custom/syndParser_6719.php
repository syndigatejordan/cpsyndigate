<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dezan Shira & Associates  [# publisher id = 1437]
//Title      : Silk Road Briefing [ English ] 
//Created on : Oct 01, 2020, 7:57:32 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6719 extends syndParseCms
{
    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('en');
    }

    protected function getImages(&$text)
    {
        $imagesArray = array();
        $this->story = $this->getCData($this->getElementByName('fulltext', $text));
        $this->story = str_replace("&amp;", "&", $this->story);
        $this->story = $this->textFixation($this->story);


        //This action depends on "Task #1064 Remove all the images from our clients feeds"
        $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
        return array();

        preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
        foreach ($imgs[0] as $img) {
            $this->addLog("getting article images");
            $image_caption = '';
            $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
            if (!empty($image_caption[0])) {
                $image_caption = $image_caption[0];
            } else {
                $image_caption = syndParseHelper::getImgElements($img, 'img', 'title');
                if (!empty($image_caption[0])) {
                    $image_caption = $image_caption[0];
                } else {
                    $image_caption = "";
                }
            }
            $imageInfo = syndParseHelper::getImgElements($img, 'img');
            $imagePathold = $imageInfo[0];
            $imagePath = preg_replace("/\?(.*)/is", "", $imagePathold);
            if (!$imagePath) {
                continue;
            }
            $imagePath = str_replace(' ', '%20', $imagePath);
            $new_img = array();
            $new_img['img_name'] = $imagePath;
            $new_img['original_name'] = $imagePath;
            $new_img['image_caption'] = $image_caption;
            $new_img['is_headline'] = FALSE;
            $new_img['image_original_key'] = sha1($imagePath);
            array_push($imagesArray, $new_img);
        }
        return $imagesArray;
    }
}