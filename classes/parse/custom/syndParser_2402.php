<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Bahrain Chamber of Commerce & Industry  [# publisher id =768 ]  
//Title     : BCCI News [ arabic ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2402 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

}
