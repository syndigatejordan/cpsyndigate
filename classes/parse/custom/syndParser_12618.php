<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sankei Shimbun Group  [# publisher id = 1799]
//Title      : SankeiBiz [ Japanese ] 
//Created on : May 15, 2022, 6:56:51 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12618 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	} 
}