<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Watani Printing and Publishing Group  [# publisher id = 333]
//Title      : Watani [ English ] 
//Created on : Sep 15, 2021, 11:06:22 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1084 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}