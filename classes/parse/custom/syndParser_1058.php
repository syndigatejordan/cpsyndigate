<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Mubasher  
//Title     : Mubasher [ English ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1058 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) srcset=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) fit-vids=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) itemprop=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) onerror=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) href=".*?"/i', '$1', $this->story);
    $this->story = str_replace("<a>", "<span>", $this->story);
    $this->story = str_replace("</a>", "</span>", $this->story);
    $this->story = str_replace("<em>", "<span>", $this->story);
    $this->story = str_replace("</em>", "</span>", $this->story);
    $this->story = str_replace("<strong>", "<span>", $this->story);
    $this->story = str_replace("</strong>", "</span>", $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getImages(&$text) {
      $this->story = $this->getCData($this->getElementByName('fulltext', $text));
      $this->story = str_replace("&amp;", "&", $this->story);
      $this->story = $this->textFixation($this->story);
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();

      $this->imagesArray = array();
      preg_match_all("/<img[^>]+>/i", $this->story, $imgs);

      foreach ($imgs[0] as $img) {
          $this->addLog("getting article images");
          $image_caption = '';
          $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
          if (!empty($image_caption[0])) {
              $image_caption = $image_caption[0];
          } else {
        $image_caption = "";
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = sha1($imageUrl) . ".jpg";
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $opts = array();
      $http_headers = array();
      $http_headers[] = 'Expect:';

      $opts[CURLOPT_URL] = $imageUrl;
      $opts[CURLOPT_HTTPHEADER] = $http_headers;
      $opts[CURLOPT_CONNECTTIMEOUT] = 10;
      $opts[CURLOPT_TIMEOUT] = 60;
      $opts[CURLOPT_HEADER] = FALSE;
      $opts[CURLOPT_BINARYTRANSFER] = TRUE;
      $opts[CURLOPT_VERBOSE] = FALSE;
      $opts[CURLOPT_SSL_VERIFYPEER] = FALSE;
      $opts[CURLOPT_SSL_VERIFYHOST] = 2;
      $opts[CURLOPT_RETURNTRANSFER] = TRUE;
      $opts[CURLOPT_FOLLOWLOCATION] = TRUE;
      $opts[CURLOPT_MAXREDIRS] = 2;
      $opts[CURLOPT_IPRESOLVE] = CURL_IPRESOLVE_V4;

      # Initialize PHP/CURL handle
      $ch = curl_init();
      curl_setopt_array($ch, $opts);
      $imgContent = curl_exec($ch);

      # Close PHP/CURL handle
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        //unlink($copiedImage);
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
