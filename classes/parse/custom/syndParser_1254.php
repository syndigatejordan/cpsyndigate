<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : International Business Associates Group
//Title     : Egypt Today [ English ] 
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_1254 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

}
