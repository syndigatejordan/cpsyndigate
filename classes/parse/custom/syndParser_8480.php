<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : FPS Chancellery of the Prime Minister and the FPS Policy and Support (BOSA)  [# publisher id = 1513]
//Title      : Belgium.be [ English ] 
//Created on : Feb 01, 2021, 2:24:19 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_8480 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}