<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Infomedia / Opoint Technology  [# publisher id = 1534]
//Title      : Global News Firehose (Italian) [ Italian ] 
//Created on : Sep 23, 2021, 12:39:58 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10956 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('it'); 
	} 
}