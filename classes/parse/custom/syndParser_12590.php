<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Industry Dive  [# publisher id = 1740]
//Title      : Proformative [ English ] 
//Created on : Apr 27, 2022, 1:51:46 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12590 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}