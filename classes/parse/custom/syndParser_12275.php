<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CryptoNews  [# publisher id = 1685]
//Title      : CryptoNews [ Spanish ] 
//Created on : Nov 23, 2021, 10:46:11 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12275 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}