<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence Djiboutienne d'Information (ADI)  [# publisher id = 1364]
//Title      : Agence Djiboutienne d'Information (ADI) [ French ] 
//Created on : Oct 07, 2020, 8:06:41 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6450 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}