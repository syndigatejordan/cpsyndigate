<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Saudi Arabian Monetary Agency (SAMA)  [# publisher id = 177]
//Title      : Saudi Arabian Monetary Agency (SAMA) [ English ] 
//Created on : Oct 16, 2016, 7:47:59 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2779 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}