<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence Tawary pour l'Informations  [# publisher id = 1366]
//Title      : Agence Tawary pour l'Informations [ French ] 
//Created on : Oct 05, 2020, 8:09:02 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6454 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}