<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Mediagene Inc.  [# publisher id = 1800]
//Title      : Business Insider Japan [ Japanese ] 
//Created on : May 15, 2022, 9:44:20 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12619 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	} 
}