<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Cynomedia Africa SARL  [# publisher id = 1481]
//Title      : Journal du Mali [ French ] 
//Created on : Aug 15, 2021, 12:22:31 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7393 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}