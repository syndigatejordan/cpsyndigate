<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BOAT International Media  [# publisher id = 1232]
//Title      : BOAT [ English ] 
//Created on : Apr 17, 2019, 8:16:38 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5542 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) srcset=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $this->story = $this->getCData($this->getElementByName('fulltext', $text));
    $this->story = str_replace("&amp;", "&", $this->story);
    $this->story = $this->textFixation($this->story);

      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();

      $imgs = $this->getElementsByName('figure', $this->story);
    foreach ($imgs as $img) {
      $this->addLog("getting article images");
      $image_caption = strip_tags($this->textFixation($this->getElementByName('figcaption', $img)));

      $imageInfo = syndParseHelper::getImgElements($img, 'source', 'srcset');
      $imageInfo = explode(",", $imageInfo[0]);
      $imageInfo = end($imageInfo);
      $imageInfo = preg_replace("/\ (.*)/", "", trim($imageInfo));
      if (!preg_match("/(https|http)/", $imageInfo)) {
        $imageInfo = "http:" . $imageInfo;
      }
      $imagePath = $imageInfo;
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        $this->addLog("No Path");
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = "<img src=\"{$images[0]['img_name']}\" alt=\"$image_caption\" />";
      $body = preg_replace("/<picture>(.*?)<\/picture>/", $new_img, $img);
      $this->story = str_replace($img, $body, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
