<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CPI Financial  [# publisher id = 259]
//Title      : Banker Middle East [ English ] 
//Created on : Dec 03, 2018, 12:03:37 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_4246 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

  public function beforeParse($params = array()) {

    parent::beforeParse($params = array());

    foreach ($this->files as $file) {
      $filename = strtolower(basename($file));

      // dublicating directories for mena
      //if(strpos($filename, 'news')) {

      $c = new Criteria();
      $c->add(TitlePeer::ID, 5483);
      $targetTitle = TitlePeer::doSelectOne($c);

      if ($file != '.' && $file != '..') {
        $this->addLog("checking file " . $targetTitle->getHomeDir() . $filename);
        if (!file_exists($targetTitle->getHomeDir() . $filename)) {
          $this->addLog("copying $file to " . $targetTitle->getHomeDir());
          copy($file, $targetTitle->getHomeDir() . $filename);
        }
      }
      //}
    } // end foreach
  }

}
