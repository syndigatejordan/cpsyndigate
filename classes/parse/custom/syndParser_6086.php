<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : QUOI Media Group Inc.  [# publisher id = 1341]
//Title      : QUOI Media - Francais [ French ] 
//Created on : Aug 04, 2021, 7:32:44 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6086 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}