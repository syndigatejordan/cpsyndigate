<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Kuwait Stock Exchange (KSE)  [# publisher id = 598]
//Title      : Kuwait Stock Exchange (KSE) [ English ] 
//Created on : Feb 02, 2016, 11:54:11 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1692 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}