<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Czech ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_575 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority' 			=> 	'Zadavatele',
										'Nature of Contract'			=> 	'Povaha smlouvy', 
										'Regulation of Procurement' 	=> 	'Nařízení o zadávání veřejných zakázek', 
										'Type of bid required'			=> 	'Typ nabídky požadovaných', 
										'Award Criteria'				=>	'Kritéria pro přidělování grantů', 
										'Original CPV'					=> 	'Původní CPV', 
										'Country'						=>	'Země', 
										'Document Id' 					=>	'Dokument Id', 
										'Type of Document'				=>	'Druh dokumentu', 
										'Procedure'						=>	'Postup',
										'Original Language'				=>	'Původní jazyk', 
										'Current Language'				=> 	'Aktuální jazyk');	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('cs');
		}
	}