<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Finch Computing  [# publisher id = 1511]
//Title      : Crypto Sentiment Events Feed (Broadcast) [ English ] 
//Created on : Jan 10, 2022, 12:10:33 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12303 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}