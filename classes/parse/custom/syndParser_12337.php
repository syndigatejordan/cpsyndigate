<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Global Markets Media Ltd  [# publisher id = 1734]
//Title      : ESG Investing [ English ] 
//Created on : Jan 16, 2022, 12:45:55 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12337 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}