<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : Health-e
//Title     : Health-e [ English ]
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1086 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getRawArticles(&$fileContents) {
    $this->addLog('getting row articles');
    $rowArticles = parent::getRawArticles($fileContents);
    return $rowArticles;
  }

  public function getStory(&$text) {
    $this->addLog('getting article story');
    $story = $this->textFixation($this->getCData($this->getElementByName('content:encoded', $text)));
    $story = strip_tags($story, "<p><ul><li><a><span><div><table><tr><td><b><i><strong>");
    $story = html_entity_decode($story, ENT_QUOTES, "UTF-8");
    return $story;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $creator = $this->textFixation($this->getCData($this->getElementByName('dc:creator', $text)));
    return $creator;
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getElementsByName('category', $text);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = $this->textFixation($this->getCData($cat));
      }
    }
    return implode(', ', $originalCats);
  }

  protected function getImages(&$text) {
    $story = trim($this->textFixation($this->getCData($this->getElementByName('content:encoded', $text))));

    $imagesArray = array();
    preg_match_all("/<img[^>]+\>/i", $story, $imgs);

    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");

      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = str_replace('150x150', 'SyndiGate', $imageInfo[0]);

      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no path ";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $text = str_replace($img, $new_img, $text);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
