<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher  : Blue Ocean Network  [# publisher id =822 ]  
//Title      : Blue Ocean Network [English]
//Created on : Jan 13, 2016, 4:36:27 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_2544 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('newsItem', $fileContents);
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->getElementByName('headline', $text);
    return $this->textFixation($headline);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('contentCreated', $text);
    $date = preg_replace('/T(.*)/is', '', $date);
    return date('Y-m-d', strtotime($date));
  }

  protected function getAbstract(&$text) {
    $this->addLog("getting article summary");
    return $this->textFixation($this->getElementByName('slugline', $text));
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = "";
    $descriptions = $this->getElementsByName('description', $text);
    foreach ($descriptions as $description) {
      $story.=$description;
    }
    return $this->textFixation($story);
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $videoDetail = $this->getElementByName('contentSet', $text);
    $videoName = $this->getElementByName('headline', $text);
    preg_match('/residref="(.*?)"/is', $videoDetail, $url);
    $link = "https://s3.amazonaws.com/syndblueocean/" . $url[1];
    $videos = array();
    $video['video_name'] = $link;
    $video['original_name'] = $url[1];
    $video['video_caption'] = $videoName;
    preg_match('/videocodec="(.*?)"/is', $videoDetail, $videoType);
    $video['video_type'] = $videoType[1];
    preg_match('/videodefinition="(.*?)"/is', $videoDetail, $mimeType);
    $video['mime_type'] = $mimeType[1];
    preg_match('/size="(.*?)"/is', $videoDetail, $bitRate);
    $bitRate = round(((intval($bitRate[1])) / 1000000), 2);
    $video['bit_rate'] = $bitRate;
    $date = $this->getElementByName('contentCreated', $text);
    $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
    $videos[] = $video;
    return $videos;
  }

}
