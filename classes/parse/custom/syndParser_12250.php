<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CryptoSlate  [# publisher id = 1680]
//Title      : CryptoSlate [ English ] 
//Created on : Nov 22, 2021, 11:05:39 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12250 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}