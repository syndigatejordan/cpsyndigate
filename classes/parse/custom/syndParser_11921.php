<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : iCrowdNewswire, LLC  [# publisher id = 1535]
//Title      : El Pais [ Spanish ] 
//Created on : Jul 01, 2021, 1:30:52 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11921 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}