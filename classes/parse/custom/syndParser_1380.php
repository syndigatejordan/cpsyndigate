<?php
//////////////////////////////////////////////////////////////////////////////
// Publisher: Fustany Digital Media  [# publisher id =433 ]
// Titles   : Fustany [English]
//////////////////////////////////////////////////////////////////////////////

class syndParser_1380 extends syndParseXmlContent {

	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('en');
		$this -> extensionFilter = 'txt';
	}

	public function getRawArticles(&$fileContents) {
		$this -> addLog('Getting article story');

		return $this -> getElementsByName('ITEM', $fileContents);
	}

    public function getArticleDate(&$text) {
    	$this->addLog('Getting article date');

        $date 		= trim($this->getElementByName('DATE', $text));
		$date 		= explode('/', $date);
		$finalDate	= $date[2] . '-' . $date[1] . '-' . $date[0];
        return $finalDate;
    }

	public function getHeadline(&$text) {
		$this -> addLog('Getting article headline');

		return trim($this -> getElementByName('HEADLINE', $text));
	}

	public function getStory(&$text) {
		$this -> addLog('Getting article body');

		$story = trim($this -> getElementByName('TEXT', $text));
		return '<p>' . $story . '</p>';
	}

	protected function getImages(&$text) {
		$images = $this -> getElementsByName('IMAGE', $text);

		$imgs = array();
		$count = count($images);
		for ($i = 0; $i < $count; $i++) {

			$imagePath = $this -> currentDir . trim($images[$i]);
			if (file_exists($imagePath)) {
				$this -> addLog('Getting article image');
				$imgs[$i] = $imagePath;
			}
		}

		if (!empty($imgs)) {
			return $this -> getAndCopyImagesFromArray($imgs);
		}
		return array();

	}

}