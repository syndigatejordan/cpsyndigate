<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Conakry Infos  [# publisher id = 1762]
//Title      : Conakry Infos [ French ] 
//Created on : Feb 13, 2022, 2:18:28 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12396 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}