<?php

////////////////////////////////////////////////////////////////////////
// Publisher : ASAPP Media Pvt Ltd
// Titles    : Construction World - Indian Edition
////////////////////////////////////////////////////////////////////////

class syndParser_248 extends syndParseRss  
{
	public function customInit() {
		$this->addLog('Custom init');
		
		parent::customInit();
		$this->defaultLang 		= $this->model->getLanguageId('en');
		$this->extensionFilter 	= 'xml';
	}
		
	public function getRawArticles(&$fileContents) {
		$this->addLog('Getting Raw articles');
		$contents = $this->getElementsByName('article', $fileContents);
		array_shift($contents);
		return $contents;
	}
	
	protected function getHeadline(&$text) {
		$this->addLog('Get aeticle headline');
		return $this->textFixation($this->getElementByName('headline', $text));
	}
	
	
	protected function getArticleDate(&$text) {
		$this->addLog("Getting article date");
		$date = $this->textFixation($this->getElementByName('date', $text));
		$date = explode('-', $date);
		$date = $date[0] . '-' . $date[2] . '-' . $date[1];
		return $this->dateFormater($date);
	}
}