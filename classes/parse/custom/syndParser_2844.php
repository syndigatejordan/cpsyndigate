<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Arab Publisher House  [# publisher id = 1017]
//Title      : ForbesMiddleEast.com [ English ] 
//Created on : Aug 18, 2016, 11:29:42 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2844 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
