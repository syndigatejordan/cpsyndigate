<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Khaama Press News Agency  [# publisher id = 1667]
//Title      : The Khaama Press News Agency [ Persian ] 
//Created on : Oct 10, 2021, 10:16:27 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12229 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fa'); 
	} 
}