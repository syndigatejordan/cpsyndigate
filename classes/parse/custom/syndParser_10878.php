<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Corporación Medios Digitales del Sur Ltda  [# publisher id = 1590]
//Title      : Mundo de la Salud [ Spanish ] 
//Created on : Jun 09, 2021, 1:54:22 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10878 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}