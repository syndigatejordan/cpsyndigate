<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Hard Beat Communications  [# publisher id = 1394]
//Title      : News Americas News Network [ English ] 
//Created on : Sep 19, 2021, 10:20:41 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7445 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}