<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Gbich  [# publisher id = 1783]
//Title      : Gbich.net [ French ] 
//Created on : Mar 07, 2022, 1:33:00 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12511 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}