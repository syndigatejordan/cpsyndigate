<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Associated Newspapers of Ceylon Limited (via HT Media Ltd.)  [# publisher id = 98]
//Title      : Daily News (via HT Media Ltd.) [ English ] 
//Created on : Jan 22, 2020, 10:24:46 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_300 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}