<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Museum Associates, dba Los Angeles County Museum of Art   [# publisher id = 1309]
//Title      : Los Angeles County Museum of Art [ English ] 
//Created on : Feb 15, 2020, 7:04:05 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5930 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
