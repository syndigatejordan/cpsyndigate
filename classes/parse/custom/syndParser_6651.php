<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Saudi Press Agency (SPA)  [# publisher id = 133]
//Title      : Saudi Press Agency (SPA) [ Russian ] 
//Created on : Oct 12, 2020, 12:11:39 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6651 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}