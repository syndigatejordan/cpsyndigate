<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Global Data Point Ltd.  [# publisher id = 378]
//Title      : Legal Monitor Worldwide [ English ] 
//Created on : Aug 09, 2021, 12:37:02 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1258 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}