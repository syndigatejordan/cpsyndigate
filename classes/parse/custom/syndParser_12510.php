<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Visages du Benin  [# publisher id = 1782]
//Title      : Visages du Benin [ French ] 
//Created on : Mar 06, 2022, 11:51:39 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12510 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}