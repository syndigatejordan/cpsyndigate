<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : National News Agency Lebanon (NNA)  [# publisher id = 132]
//Title      : National News Agency Lebanon (NNA) [ English ] 
//Created on : Oct 02, 2019, 11:02:42 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5801 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}