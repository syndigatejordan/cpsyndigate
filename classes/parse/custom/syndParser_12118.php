<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Angola Press News Agency (ANGOP)  [# publisher id = 1613]
//Title      : Angola Press News Agency (ANGOP) [ Portuguese ] 
//Created on : Aug 10, 2021, 9:33:02 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12118 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}