<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence Togolaise de Presse (ATOP)  [# publisher id = 1367]
//Title      : Agence Togolaise de Presse (ATOP) [ French ] 
//Created on : Sep 28, 2021, 7:38:05 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6455 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}