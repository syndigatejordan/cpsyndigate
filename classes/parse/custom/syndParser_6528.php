<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The United Nations Department of Global Communications  [# publisher id = 1400]
//Title      : UN News [ Swahili ] 
//Created on : Oct 20, 2020, 1:42:21 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6528 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('sw'); 
	} 
}