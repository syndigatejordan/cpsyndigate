<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Web Middle East (Web Me) s.a.l.  [# publisher id = 19]
//Title      : The Daily Star - Business [ English ] 
//Created on : Apr 23, 2019, 11:52:44 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5551 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}