<?php

/**
 * Publisher : IRIN (United Nations Integrated Regional Information Networks)
 * Title     : PlusNews Arabic [Arabic]
 *
 * -----------------------------------------------------------------------------------------------------
 */
class syndParser_411 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
    $this->extensionFilter = 'xml';
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = $this->textFixation($this->getCData($this->getElementByName('content:encoded', $text)));
    return preg_replace("/<img[^>]+\>/i", '', $story);
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");

    return trim($this->textFixation($this->getElementByName('dc:creator', $text)));
  }

  public function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = trim($this->getElementByName('pubDate', $text));
    return $this->dateMapper($date);
  }

  protected function getImages(&$text) {
    $story = trim($this->textFixation($this->getCData($this->getElementByName('description', $text))));

    $imagesArray = array();
    preg_match_all("/<img[^>]+\>/i", $story, $imgs);

    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");

      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];

      if (!$imagePath) {
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        continue;
      }

      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);

      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  private function dateMapper($date) {

    $date = trim($date);
    $monthsEn = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',);
    $monthsAr = array('يناير', 'فبراير', 'مارس', 'أبريل', 'مايو', 'يونيو', 'يوليو', 'أغسطس', 'سبتمبر', 'أكتوبر', 'نوفمبر', 'ديسمبر',);

    $daysEn = array('Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri',);
    $daysAr = array('السبت', 'الأحد', 'الإثنين', 'الثلاثاء', 'الأربعاء', 'الخميس', 'الجمعة',);

    $date = str_replace($daysAr, $daysEn, $date);
    $date = str_replace($monthsAr, $monthsEn, $date);
    return $this->dateFormater($date);
  }

}
