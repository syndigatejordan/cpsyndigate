<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher  : Bloomberg  [# publisher id = 1202]
//Title      : Bloomberg Video + QuickTake Video [ English ]
//Created on : Jan 16, 2019, 7:20:21 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_6008 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('/<iframe(.*?)<\/iframe>/is', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }
  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $matches = null;
    $videos = array();
    $videoName = "";
    $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
    $fulltext=$this->textFixation($this->getElementByName('fulltext', $text));
    $videoname =null;
    if(preg_match('/<iframe src="(.*?)"><\/iframe>/is',$fulltext,$videoname)){
      $videoname=$videoname[1];
      if (!empty($videoname)) {
        $video = array();
        $video['video_name'] = $videoname;
        $video['original_name'] = $videoName;
        $video['video_caption'] = $videoName;
        $video['added_time'] = date('Y-m-d h:i:s', $this->getElementByName('date', $text));

        $videos[] = $video;
      }
    }
    return $videos;
  }

}
