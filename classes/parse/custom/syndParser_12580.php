<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Radiodiffusion télévision Guinéenne  [# publisher id = 1791]
//Title      : Radiodiffusion télévision Guinéenne [ French ] 
//Created on : Apr 11, 2022, 11:45:32 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12580 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}