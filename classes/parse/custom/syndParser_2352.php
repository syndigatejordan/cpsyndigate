<?php 
		///////////////////////////////////////////////////////////////////////////////////// 
		//Publisher : Royal Dutch Shell plc   
		//Title     : Shell Media Releases [ English ] 
		/////////////////////////////////////////////////////////////////////////////////////
		
		class syndParser_2352 extends syndParseCms { 
			 public function customInit() { 
				 parent::customInit(); 
				 $this->defaultLang = $this->model->getLanguageId('en'); 
			} 
		}