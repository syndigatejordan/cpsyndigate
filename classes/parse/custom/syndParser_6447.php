<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence Centrafricaine de Presse  [# publisher id = 1362]
//Title      : Agence Centrafricaine de Presse [ French ] 
//Created on : Sep 23, 2021, 8:04:29 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6447 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}