<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : KHOVAR (National information agency of Tajikistan)  [# publisher id = 1418]
//Title      : KHOVAR (National information agency of Tajikistan) [ Russian ] 
//Created on : Sep 19, 2021, 8:56:35 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6620 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}