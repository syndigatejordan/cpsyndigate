<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Questex Asia Ltd  [# publisher id =489 ]
// Titles    : CFO Innovation Asia [Enlish]
//Created on : Jul 23, 2018, 9:34:28 AM
//Author     : Eyad
///////////////////////////////////////////////////////////////////////////

class syndParser_1471 extends syndParseRss {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('item', $fileContents);
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return $this->textFixation($this->getCData($this->getElementByName('title', $text)));
  }

  protected function getAbstract(&$text) {
    $this->addLog('getting article summary');
    return $this->textFixation($this->getCData($this->getElementByName('summary', $text)));
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = trim(preg_replace('/\+(.*)/is', '', $this->getCData($this->getElementByName('date', $text))));
    return date('Y-m-d', strtotime($date));
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    return $this->story;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getCData($this->getElementByName('author', $text)));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    return $this->textFixation($this->getCData($this->getElementByName('type', $text)));
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $image_caption = "";
    $imagePath = $this->getCData($this->getElementByName('thumbnail', $text));
    $imagePath = trim(preg_replace('/\?(.*)/is', '', $imagePath));
    $image_caption = trim($this->getCData($this->getElementByName('title', $text)));
    if ($imagePath) {
      if (!$this->checkImageifCached($imagePath)) {
        $imagePath = str_replace(' ', '%20', $imagePath);
        $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
        if ($copiedImage) {
          $images = $this->getAndCopyImagesFromArray(array($copiedImage));
          if (!empty($image_caption)) {
            $images[0]['image_caption'] = $image_caption;
          }
          $name_image = explode('/images/', $copiedImage);
          if ($image_caption == $name_image[1]) {
            $image_caption = '';
          }
          $images[0]['image_caption'] = $image_caption;
          $images[0]['is_headline'] = TRUE;
          array_push($imagesArray, $images[0]);
        }
      }
    }


    $this->story = $this->getCData($this->getElementByName('content', $text));
    $this->story = str_replace("&nbsp;", " ", $this->story);
    $this->story = $this->textFixation($this->story);

    $imgs = NULL;
    preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
      if (!empty($image_caption[0])) {
        $image_caption = trim($image_caption[0]);
      } else {
        $image_caption = "";
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      $imagePath = trim(preg_replace('/\?(.*)/is', '', $imagePath));
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  public function getArticleReference(&$text) {
    $this->addLog("getting article reference");
    return $this->textFixation($this->getCData($this->getElementByName('url', $text)));
  }

}
