<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Hearst Newspapers  [# publisher id = 1600]
//Title      : ctpost (Connecticut Post) [ English ] 
//Created on : Jul 04, 2021, 12:35:08 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11935 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}