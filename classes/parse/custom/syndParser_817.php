<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: TBREAK MEDIA
	// Titles   : ME Movies[ English ]
	//////////////////////////////////////////////////////////////////////////////

class syndParser_817 extends syndParseRss {

	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('en');
	}

	public function getOriginalCategory(&$text) {
		$this -> addLog('getting article category');
		$cats = $this -> getElementsByName('category', $text);
		$originalCats = array();

		if (!empty($cats)) {
			foreach ($cats as $cat) {
				$originalCats[] = $this -> textFixation($this -> getCData($cat));
			}
		}
		return implode(', ', $originalCats);
	}

	protected function getAuthor(&$text) {
		$this -> addLog("getting article author");
		return $this -> getElementByName('dc:creator', $text);
	}

	public function getArticleDate(&$text) {
		$this -> addLog("getting article date");
		$date = $this -> getElementByName('pubDate', $text);
		$date = date('Y-m-d', strtotime($date));
		return $date;
	}

	protected function getStory(&$text) {
		$this -> addLog("getting article text");
		return $this -> getCData($this -> getElementByName('content:encoded', $text));
	} 

	protected function getAbstract(&$text) {
		$this -> addLog('getting article summary');
		$summary = $this -> textFixation($this -> getCData($this -> getElementByName('description', $text)));
		return $summary;
	}

	protected function getImages(&$text) {
		$story = trim($this -> textFixation($this -> getCData($this -> getElementByName('content:encoded', $text))));

		$imagesArray = array();
		preg_match_all("/<img[^>]+\>/i", $story, $imgs);

		foreach ($imgs[0] as $img) {
			$this -> addLog("getting article images");

			$imageInfo = syndParseHelper::getImgElements($img, 'img');
			$imagePath = $imageInfo[0];

			if (!$imagePath) {
				continue;
			}
			$imagePath = str_replace(' ', '%20', $imagePath);
      if($this->checkImageifCached($imagePath)){
        // Image already parsed..
        continue;
      }
			$copiedImage = $this -> copyUrlImgIfNotCached($imagePath);

			if (!$copiedImage) {
				echo "no pahr";
				continue;
			}
			$images = $this -> getAndCopyImagesFromArray(array($copiedImage));
			$images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
			$images[0]['is_headline'] = false;
			$new_img = str_replace($imagePath, $images[0]['img_name'], $img);
			$text = str_replace($img, $new_img, $text);
			array_push($imagesArray, $images[0]);
		}
		return $imagesArray;
	}

}
