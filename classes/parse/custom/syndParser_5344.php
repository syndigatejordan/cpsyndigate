<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Emirates News Agency (WAM)  [# publisher id = 32]
//Title      : Emirates News Agency (WAM) [ Turkish ] 
//Created on : Feb 10, 2019, 9:07:04 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5344 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('tr');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
