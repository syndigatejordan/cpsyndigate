<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bangladesh Sangsbad Sangstha (BSS)  [# publisher id = 1405]
//Title      : Bangladesh Sangsbad Sangstha (BSS) [ Bengali ] 
//Created on : Oct 20, 2020, 11:46:35 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6544 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('bn'); 
	} 
}