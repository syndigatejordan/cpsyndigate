<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : GlobalData  
//Title     : GlobalData [ English ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_848 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('EventSummary', $fileContents);
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = $this->getElementByName('Summary', $text);
    $this->story = strip_tags($this->story, '<p><br><strong><b><u><i>');
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  public function getAbstract(&$text) {
    $this->addLog('Getting article summary');
    $this->abs = strip_tags($this->getElementByName('EventInBrief', $text));
    return $this->abs;
  }

  public function getHeadline(&$text) {
    $headline = trim($this->getElementByName('EventHeadline', $text));
    return $headline;
  }

  public function getArticleDate(&$text) {
    $date = trim($this->getElementByName('StoryDate', $text));
    return date('Y-m-d', strtotime($date));
  }

  public function getOriginalCategory(&$text) {
     $NewsType = trim($this->getElementByName('NewsType', $text));
     return $NewsType;
  }

}