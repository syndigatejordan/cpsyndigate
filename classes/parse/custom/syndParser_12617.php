<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IFIS Japan Limited  [# publisher id = 1798]
//Title      : IFIS Japan [ Japanese ] 
//Created on : May 15, 2022, 6:53:02 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12617 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	} 
}