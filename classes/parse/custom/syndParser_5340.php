<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Emirates News Agency (WAM)  [# publisher id = 32]
//Title      : Emirates News Agency (WAM) [ Portuguese ] 
//Created on : Feb 10, 2019, 9:06:18 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5340 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}