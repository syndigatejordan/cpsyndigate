<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Industry Dive  [# publisher id = 1740]
//Title      : BioPharma Dive [ English ] 
//Created on : Jan 27, 2022, 8:21:53 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12357 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}