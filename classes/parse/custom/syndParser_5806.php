<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ma'an Network  [# publisher id = 501]
//Title      : Ma'an News Agency [ English ] 
//Created on : Oct 02, 2019, 11:38:35 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5806 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}