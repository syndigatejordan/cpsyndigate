<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Afghan Voice Agency (AVA)  [# publisher id = 1401]
//Title      : Afghan Voice Agency (AVA) [ English ] 
//Created on : Dec 08, 2020, 10:09:12 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6530 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}