<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Dutch ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_585 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority' 		=>	'Aanbestedende dienst', 	
										'Nature of Contract'		=>	'Aard van het Contract', 
										'Regulation of Procurement' =>	'Regulering van de Procurement', 
										'Type of bid required'		=> 	'Type bod dat nodig is', 
										'Award Criteria'			=>	'Gunningscriteria',
										'Original CPV'				=> 	'Originele CPV', 
										'Country'					=>	'Land', 
										'Document Id' 				=>	'Document ID', 
										'Type of Document'			=>	'Type document', 
										'Procedure'					=>	'Procedure', 
										'Original Language'			=>	'Oorspronkelijke taal', 
										'Current Language'			=>	'Huidige taal',
										'Address'					=>	'Adres');	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('nl');
		}
		
		protected function getStory(&$text) {			
			$this->addLog("getting article text");
			$story  	=  $this->textFixation($this->getElementByName('content', $text));
			$sector 	=  $this->textFixation($this->getElementByName("sector", $text));
			$market		=  $this->textFixation($this->getElementByName("market", $text));
			$marketOrg	=  $this->textFixation($this->getElementByName("marketorg", $text));
		
			$typeBid		=  $this->textFixation($this->getElementByName("typebid", $text));
			$awardCrit		=  $this->textFixation($this->getElementByName("awardcrit", $text));
			$originalCpv	=  $this->getElementsByName("originalcpv", $text);
		
			$isoCountry		=  $this->textFixation($this->getElementByName("isocountry", $text));
		
			$docId			=  $this->textFixation($this->getElementByName("nodocojs", $text));
			$proc			=  $this->textFixation($this->getElementByName("proc", $text));
			$docType		=  $this->textFixation($this->getElementByName("natnotice", $text));
		
		 
			preg_match_all("/<[^>]+lg=+[^>]+\>/i", $text, $matches);
			$lang = $matches[0];
			$lang =  explode(" ", $lang[0]);
		
			$langCriteria = new Criteria();
		
			$origLang	=  explode("\"", $lang[3]);
			$origLang	=  $origLang[1];
		
			$currLang	=  explode("\"", $lang[2]);
			$currLang	=	$currLang[1];
	
			$langCriteria->add(LanguagePeer::CODE , $origLang);
			$orig = LanguagePeer::doSelectOne($langCriteria);
		
			if($orig) {
				$origLang = $orig;
			}
		
			$currLangCriteria = new Criteria();
			$currLangCriteria->add(LanguagePeer::CODE , $currLang);
			$curr = LanguagePeer::doSelectOne($currLangCriteria);
		
			if($curr) {
				$currLang = $curr;
			}
		
			$countryCriteria = new Criteria();
			$countryCriteria->add(CountryPeer::ISO, $isoCountry, Criteria::EQUAL);
			$country = CountryPeer::doSelectOne($countryCriteria);
		
			if($country) {
       		    $isoCountry = $country->getPrintableName();
        	}
      	
      	  if($sector|| $market || $marketOrg || $typeBid || $awardCrit || $originalCpv || $isoCountry || $docId || $proc || $origLang || $currLang || $docType) {
       			$story .= "\n <p> <b>More info: </b> \n";
       		
       			$this->addToBody($this->translations["Awarding Authority"], $sector, $story);
				$this->addToBody($this->translations["Nature of Contract"], $market, $story);
				$this->addToBody($this->translations["Regulation of Procurement"], $marketOrg, $story);
				$this->addToBody($this->translations["Type of bid required"], $typeBid, $story);
		
				$this->addToBody($this->translations["Award Criteria"], $awardCrit, $story);
				$cpvs="";
				foreach ($originalCpv as $cpv) {
					$cpvs .= $cpv .", ";
				}
			
				$this->addToBody($this->translations["Original CPV"], $cpvs, $story);
				$this->addToBody($this->translations["Country"], $isoCountry, $story);
				$this->addToBody($this->translations["Document Id"], $docId, $story);
				$this->addToBody($this->translations["Type of Document"], $docType, $story);
				$this->addToBody($this->translations["Procedure"], $proc, $story);
				$this->addToBody($this->translations["Original Language"], $origLang, $story);
				$this->addToBody($this->translations["Current Language"], $currLang, $story);
       
				$story 	.="</p>";
    	    }
        
       		$story 	 = str_replace('<p>[...]</p>', "", $story);
       		$story	 = preg_replace("/<addr+\>/i", $this->translations['Address'].":", $story);
 	     	return $story;
		}
	}
