<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Fuseworks Limited  [# publisher id =517 ]  
// Titles    : Fuseworks [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1551 extends syndParseXmlContent {
	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('en');
		$this -> extensionFilter = 'xml';
	}

	protected function getRawArticles(&$fileContents) {
		//get articles
		$this -> addLog("getting articles raw text");
		return $this -> getElementsByName('nitf', $fileContents);
	}

	public function getStory(&$text) {
		$this -> addLog('Getting article story');
		$body = trim($this -> getElementByName('body.content', $text));
		$this -> story = $this -> textFixation($this -> getElementByName('body.content', $text));
		$this -> story = strip_tags($this -> story, '<p><br><strong><b><u><i>');
		if (empty($this -> story)) {
			return '';
		}
		return $this -> story;
	}

	public function getHeadline(&$text) {
		$head = trim($this -> getElementByName('body.head', $text));
		$headline = strip_tags($this -> getElementByName('hedline', $head));
		return $headline;
	}

	public function getArticleDate(&$text) {
		$head = trim($this -> getElementByName('body.head', $text));
		$dateline = trim($this -> getElementByName('dateline', $head));
		$dateInfo = syndParseHelper::getImgElements($dateline, 'story.date','norm');
		return date('Y-m-d', strtotime($dateInfo[0]));
	}

	protected function getAuthor(&$text) {
		$this -> addLog("getting article author");
		$head = trim($this -> getElementByName('body.head', $text));
		$b = trim($this -> textFixation($this -> getElementByName('byline', $head)));
		return trim($this -> textFixation($this -> getElementByName('byttl', $b)));
	}

}