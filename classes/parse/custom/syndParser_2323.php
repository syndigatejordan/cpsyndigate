<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : test for pdfs research   [# publisher id =742 ] 
// Title     : test title for pdf research
///////////////////////////////////////////////////////////////////////////
class syndParser_2323 extends syndParseCms {

  public $story;

  protected function getArticle(&$text) {
    try {

      //$article['iptc_id']     = $this->getIptcId($text);
      $article['title_id'] = $this->title->getId();
      $article['lang_id'] = $this->getLangId($text);
      $article['headline'] = strip_tags($this->getHeadline($text));
      $article['abstract'] = strip_tags($this->getAbstract($text));
      $article['author'] = strip_tags($this->getAuthor($text));
      $article['date'] = $this->getArticleDate($text);
      $article['has_time'] = $this->getHasTime();
      $article['parsed_at'] = time();
      $article['original_data'] = $this->getOriginalData($text);
      $article['images'] = $this->getImages($text);
      $article['videos'] = $this->getVideos($text);

      $issueNum = isset($article['original_data']['issue_number']) ? $article['original_data']['issue_number'] : 0;
      $article['original_data']['original_article_id'] = $this->getArticleOriginalId(array('text' => &$text, 'headline' => $article['headline'], 'articleDate' => $article['date'], 'issueNum' => $issueNum));
      $article['original_data']['old_original_article_id'] = syndParseContent::getArticleOriginalId(array('text' => &$text, 'headline' => $article['headline'], 'articleDate' => $article['date'], 'issueNum' => $issueNum));
      $article['original_data']['original_category'] = strip_tags($this->getOriginalCategory($text));
      $article['original_data']['reference'] = strip_tags($this->getArticleReference($text));

      /* 			
        Because getIptcId remove the category -in some parsers like AbText- the text and we do use this in
        other function we put it at last
       */
      $article['iptc_id'] = $this->getIptcId($text);

      $this->processExtraTags($text);
      $test = $this->getStory($text);
      $article['story'] = $this->getStory($text);
    } catch (Exception $e) {
      $this->addLog($e->getMessage(), ezcLog::ERROR);
      $this->errorMessage = $e->getMessage();
      $this->defaultProcessState = ezcLog::FAILED_AUDIT;
    }

    return $article;
  }

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    //var_dump($this->story);
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getImages(&$text) {
    $this->story = $this->textFixation($this->getCData($this->getElementByName('fulltext', $text)));
    $this->imagesArray = array();
    $images = $this->textFixation($this->getElementByName('images', $text));
    $images = str_replace('<img>', '<image>', $images);
    $images = str_replace('</img>', '</image>', $images);
    $images = $this->getElementsByName('image', $images);
    foreach ($images as $img) {
      $this->addLog("getting article images");
      $imagePath = $this->textFixation($this->getElementByName('img_path', $img));
      $imageDescription = $this->textFixation($this->getElementByName('img_description', $img));
      $imagePath = "/var/www/cms/web/upload/" . trim($imagePath);

      if (!$imagePath) {
        continue;
      }
      $baseNameold = basename($imagePath);
      $baseName = str_replace(' ', '_', $baseNameold);
      $copiedImage = $this->imgCacheDir . $baseName;

      if (!is_dir($this->imgCacheDir)) {
        mkdir($this->imgCacheDir, 0755, true);
      }
      if (!file_exists($copiedImage)) {
        $copyResult = copy($imagePath, $copiedImage);
      }
      if (!$copyResult) {
        echo "no path", PHP_EOL;
        continue;
      }
      $im = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (empty($imageDescription)) {
        $imageDescription = "";
      }
      $im[0]['image_caption'] = $imageDescription;
      $im[0]['is_headline'] = false;
      $this->story = str_replace($baseNameold, $im[0]["img_name"], $this->story);
      array_push($this->imagesArray, $im[0]);
    }
    return $this->imagesArray;
  }

}