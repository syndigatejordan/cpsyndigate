<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Haaretz Daily Newspaper Ltd.  [# publisher id = 937]
//Title      : Haaretz [ Hebrew ] 
//Created on : May 19, 2016, 11:02:50 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2733 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('he'); 
	} 
}