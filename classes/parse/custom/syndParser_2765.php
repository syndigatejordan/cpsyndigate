<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Media & Communications Center  [# publisher id = 964]
//Title      : The Reporter [ English ] 
//Created on : May 22, 2016, 9:00:53 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2765 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}
