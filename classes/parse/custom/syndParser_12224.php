<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Salam Watandar Media Organisation  [# publisher id = 1663]
//Title      : Salam Watandar [ Uzbek ] 
//Created on : Oct 10, 2021, 7:15:28 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12224 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('uz'); 
	} 
}