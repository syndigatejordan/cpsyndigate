<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : U.S. Department of State  [# publisher id = 1048]
//Title      : Share America [ Russian ] 
//Created on : Sep 13, 2021, 2:13:17 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2907 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}