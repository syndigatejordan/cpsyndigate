<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ukrainian National News Agency "Ukrinform"  [# publisher id = 1337]
//Title      : Ukrinform [ German ] 
//Created on : Sep 23, 2021, 8:00:55 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6037 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('de'); 
	} 
}