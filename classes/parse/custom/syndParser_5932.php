<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Nieman Foundation at Harvard  [# publisher id = 1310]
//Title      : Nieman Journalism Lab [ English ] 
//Created on : Sep 19, 2021, 8:53:28 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5932 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}