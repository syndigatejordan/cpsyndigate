<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BANG Media International  [# publisher id = 306]
//Title      : BANG Showbiz - Movies [ English ] 
//Created on : Sep 13, 2021, 1:38:45 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6331 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}