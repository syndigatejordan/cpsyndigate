<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Newsfile Corp.  [# publisher id = 502]
//Title      : Newsfile [ English ] 
//Created on : Sep 23, 2021, 7:47:53 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1502 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}