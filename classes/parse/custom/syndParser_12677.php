<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : FISCO Ltd.  [# publisher id = 1832]
//Title      : FISCO [ Japanese ] 
//Created on : Jul 31, 2022, 7:36:23 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12677 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	} 
}