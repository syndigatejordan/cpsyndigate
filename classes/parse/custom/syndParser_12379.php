<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Direct Actu  [# publisher id = 1750]
//Title      : Direct Actu [ French ] 
//Created on : Jan 30, 2022, 1:17:32 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12379 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}