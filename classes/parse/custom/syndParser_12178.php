<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Kebbi Daily Print  [# publisher id = 1630]
//Title      : Kebbi Daily News [ English ] 
//Created on : Sep 30, 2021, 7:38:34 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12178 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}