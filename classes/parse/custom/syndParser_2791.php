<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Islamic Republic News Agency  [# publisher id = 799]
//Title      : ParsToday [ Spanish ] 
//Created on : Jul 21, 2016, 12:52:09 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2791 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}