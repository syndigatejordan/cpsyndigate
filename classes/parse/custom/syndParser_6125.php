<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Namibia Press Agency  [# publisher id = 1346]
//Title      : Namibia Press Agency (NAMPA) - Text Radio Bulletins [ English ] 
//Created on : Sep 23, 2021, 8:18:02 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6125 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}