<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Galadari Printing & Publishing LLC  [# publisher id = 28]
//Title      : Khaleej Times - Saudi Arabia [ English ] 
//Created on : Sep 23, 2019, 7:37:12 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5770 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}