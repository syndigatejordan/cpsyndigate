<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sarl Interface rp  [# publisher id = 509]
//Title      : Radio-M.net [ Arabic ] 
//Created on : Jul 18, 2022, 12:38:58 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12672 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}