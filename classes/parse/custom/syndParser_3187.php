<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Source News Agency  [# publisher id = 1161]
//Title      : The Source [ English ] 
//Created on : Sep 23, 2018, 5:52:12 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3187 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}