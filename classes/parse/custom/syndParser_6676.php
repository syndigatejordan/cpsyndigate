<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Turkmenistan State News Agency (TDH)  [# publisher id = 1430]
//Title      : Turkmenistan State News Agency (TDH) [ Turkmen ] 
//Created on : Oct 22, 2020, 1:43:54 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6676 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('tk'); 
	} 
}