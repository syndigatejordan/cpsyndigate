<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : U.S. Department of State  [# publisher id = 1048]
//Title      : Share America [ Persian ] 
//Created on : Sep 13, 2021, 2:13:20 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2912 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fa'); 
	} 
}