<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Yemen News Agency (SABA)  [# publisher id = 143]
//Title      : Yemen News Agency (SABA) [ French ] 
//Created on : Nov 26, 2019, 8:55:31 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5872 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}