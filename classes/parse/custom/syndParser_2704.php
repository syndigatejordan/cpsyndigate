<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Ayyam Printing, Publishing, Advertising and Distribution House  [# publisher id = 911]
//Title      : Al Ayyam [ Arabic ] 
//Created on : Feb 02, 2021, 3:46:40 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2704 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}