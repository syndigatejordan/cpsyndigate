<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Alston Publishing House Pte Ltd  [# publisher id = 1329]
//Title      : Maths SMART [ English ] 
//Created on : Mar 31, 2020, 8:38:33 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_6002 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $images = $this->textFixation($this->getElementByName('images', $text));
    $images = str_replace('<img>', '<image>', $images);
    $images = str_replace('</img>', '</image>', $images);
    $images = $this->getElementsByName('image', $images);
    foreach ($images as $imgName) {
      $img = array();
      $this->addLog("getting image");
      $imageDescription = $this->textFixation($this->getElementByName('img_description', $imgName));
      $imagePath = trim($this->textFixation($this->getElementByName('img_path', $imgName)));
      $path = pathinfo($imagePath);
      $extension = $path["extension"];
      $imageExtension = array("gif", "png", "jpeg", "jpg", "pdf");
      if (!in_array($extension, $imageExtension)) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        $this->addLog("No Path");
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['image_caption'] = $imageDescription;
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    $this->story = $this->getCData($this->getElementByName('fulltext', $text));
    $this->story = str_replace("&amp;", "&", $this->story);
    $this->story = $this->textFixation($this->story);
    preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
      if (!empty($image_caption[0])) {
        $image_caption = $image_caption[0];
      } else {
        $image_caption = syndParseHelper::getImgElements($img, 'img', 'title');
        if (!empty($image_caption[0])) {
          $image_caption = $image_caption[0];
        } else {
          $image_caption = "";
        }
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePathold = $imageInfo[0];
      $imagePath = preg_replace("/\?(.*)/is", "", $imagePathold);
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        $this->addLog("No Path");
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePathold, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
