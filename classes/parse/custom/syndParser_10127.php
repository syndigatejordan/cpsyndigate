<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : African News Agency  [# publisher id = 1532]
//Title      : African News Agency [ English ] 
//Created on : Dec 01, 2021, 7:22:02 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10127 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}