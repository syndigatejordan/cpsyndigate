<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Arab Publisher House  [# publisher id = 1017]
//Title      : ForbesMiddleEast.com [ Arabic ] 
//Created on : Aug 18, 2016, 11:30:12 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2845 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }
}
