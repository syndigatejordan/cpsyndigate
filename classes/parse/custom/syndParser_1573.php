<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Breaking Media, Inc.  [# publisher id = 531]
//Title      : Breaking Defense [ English ] 
//Created on : Sep 19, 2021, 7:02:23 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1573 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}