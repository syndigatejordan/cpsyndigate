<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Financial Nigeria International Limited  [# publisher id = 1350]
//Title      : Financial Nigeria [ English ] 
//Created on : Sep 02, 2020, 6:34:05 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6437 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}