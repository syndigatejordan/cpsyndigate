<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dominicano Digital  [# publisher id = 1690]
//Title      : Digital Dominican [ Spanish ] 
//Created on : Dec 05, 2021, 8:49:30 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12285 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}