<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Inkitab  [# publisher id = 674]
//Title      : Inkitab [ Arabic ] 
//Created on : Aug 02, 2021, 12:21:50 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2108 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}