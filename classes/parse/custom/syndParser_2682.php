<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Thailand Ministry of Foreign Affairs  [# publisher id = 902]
//Title      : Thailand Ministry of Foreign Affairs - Press Releases [ English ] 
//Created on : Apr 07, 2016, 10:52:44 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2682 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}