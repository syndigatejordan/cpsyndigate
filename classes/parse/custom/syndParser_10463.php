<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Daylight Online Newspaper  [# publisher id = 1550]
//Title      : Daylight Online Newspaper [ English ] 
//Created on : Sep 19, 2021, 10:23:21 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10463 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}