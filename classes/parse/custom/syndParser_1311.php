<?php
///////////////////////////////////////////////////////////////////////////
// Publisher : Jenny White  [# publisher id =402 ] 
// Titles    : Kamil Pasha [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1311 extends syndParseRss {

	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('en');
	}
	
    protected function getAuthor (&$text) {
        $this->addLog("getting article author");
        return $this->textFixation( $this->getElementByName('dc:creator', $text) );                 
    }
	
    protected function getStory (&$text) {        
        $this->addLog("getting article body");
        return $this->textFixation( $this->getCData($this->getElementByName('content:encoded', $text)) );
    }

}