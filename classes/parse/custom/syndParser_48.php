<?php

	////////////////////////////////////////////////////////////////////////
	// Publisher : Dar Assabah
	// Titles    : Dar Assabah
	////////////////////////////////////////////////////////////////////////

	
	class syndParser_48 extends syndParseAbTxtSample
	{

		public function customInit()
		{
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('ar');
			$this->charEncoding = 'UTF-16';
			$this->extensionFilter = 'txt';
		}
		
		
		protected function getRawArticles(&$fileContents)
		{
			return array($fileContents);
		}
	}
?>