<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Nexus Communication S.A.  [# publisher id = 1720]
//Title      : Fleet Europe [ French ] 
//Created on : Jan 11, 2022, 2:24:19 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12325 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}