<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : Raxanreeb Online  [publisher id =566 ] 
//Title     : Raxanreeb Online [ English ]
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_1637 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('item', $fileContents);
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->getCData($this->getElementByName('title', $text));
    return $this->textFixation($headline);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    return date('Y-m-d', strtotime($date));
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = $this->getCData($this->getElementByName('content:encoded', $text));
    $story = preg_replace('#<h3>Use Facebook to Comment on this Post(.*)#s', "", $story);
    $story = preg_replace("/<style\\b[^>]*>(.*?)<\\/style>/s", "", $story);
    $story = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $story);
    $story = preg_replace('/<!--(.|\s)*?-->/', '', $story);
    return $this->textFixation($story);
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getElementsByName('category', $text);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = $this->textFixation($this->getCData($cat));
      }
    }
    return implode(', ', $originalCats);
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getCData($this->getElementByName('dc:creator', $text)));
  }

  protected function getImages(&$text) {
    $this->imagesArray = array();
    $story = $this->textFixation($this->getCData($this->getElementByName('content:encoded', $text)));
    $story = preg_replace('#<h3>Use Facebook to Comment on this Post(.*)#s', "", $story);
    preg_match_all(
            "/<img[^>]+\>/i", $story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      if (!$imagePath) {
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $text = str_replace($img, $new_img, $text);
      $text = str_replace($imagePath, $images[0]['img_name'], $text);
      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

}