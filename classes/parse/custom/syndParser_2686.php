<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Fadaat Media Ltd  [# publisher id = 904]
//Title      : TheNewArab [ Arabic ] 
//Created on : Aug 15, 2021, 9:00:56 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2686 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}