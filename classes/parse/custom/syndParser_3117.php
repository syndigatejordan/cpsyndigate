<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Associated Newspapers of Zimbabwe  [# publisher id = 1132]
//Title      : Daily News [ English ] 
//Created on : Aug 28, 2017, 8:38:52 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3117 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}