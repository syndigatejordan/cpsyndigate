<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : 	Agency Tunis Afrique Press 
// Titles    :  Agency Tunis Afrique Press [Arabic]
///////////////////////////////////////////////////////////////////////////

class syndParser_858 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }
    protected function getHeadline(&$text)
    {
        $this->addLog("getting article headline");
        $headLine= $this->textFixation($this->getElementByName('title', $text));
        if (strlen($headLine) > 250) {
            $headLine = substr($headLine, 0, strrpos(substr($headLine, 0, 250), ' ')) . " ...";
        }
        return $headLine;
    }
  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    $this->story = str_replace(". ", ".</p><p>", $this->story);
    $this->story = "<p>" . $this->story . "</p>";
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    sleep(1);
    $baseName = basename($imageUrl);
    $baseName = explode(".", $baseName);
    $baseNameUn = sha1($imageUrl) . "." . $baseName[1];
    $copiedImage = $this->imgCacheDir . $baseNameUn;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }
    if (!file_exists($copiedImage)) {
      $curl_handle = curl_init();
      curl_setopt($curl_handle, CURLOPT_URL, $imageUrl);
      curl_setopt($curl_handle, CURLOPT_VERBOSE, true);
      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($curl_handle, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($curl_handle, CURLOPT_ENCODING, "gzip,deflate");
      $imgContent = curl_exec($curl_handle);
      curl_close($curl_handle);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
