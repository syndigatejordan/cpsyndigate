<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Haaretz Daily Newspaper Ltd.  [# publisher id = 937]
//Title      : TheMarker Online [ Hebrew ] 
//Created on : Mar 04, 2021, 10:39:14 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10320 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('he'); 
	} 
}