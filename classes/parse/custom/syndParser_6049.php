<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : SourceMedia, LLC d/b/a Arizent  [# publisher id = 1338]
//Title      : Credit Union Journal [ English ] 
//Created on : Jun 18, 2020, 7:05:39 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6049 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}