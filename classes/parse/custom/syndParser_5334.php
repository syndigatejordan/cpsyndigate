<?php
/////////////////////////////////////////////////////////////////////////////////////
//Publisher  : Asianet-Pakistan (Pvt) Ltd.  [# publisher id = 83]
//Title      : Dawn [ English ]
//Created on : Aug 19, 2021, 10:15:36 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5334 extends syndParseCms {
    public function customInit() {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('en');
    }
}