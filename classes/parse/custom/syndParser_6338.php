<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BANG Media International  [# publisher id = 306]
//Title      : BANG Showbiz - Tech Video [ English ] 
//Created on : Aug 04, 2020, 11:16:59 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_6338 extends syndParseCms
{

//Handle empty body
    public function parse()
    {
        $articles = array();
        foreach ($this->files as $file) {
            if ($this->extensionFilter) {
                if (!syndParseHelper::isFileExtension($file, $this->extensionFilter)) {
                    $this->addLog("Wrong extension for file : $file)");
                    continue;
                }
            }
            if (!file_exists($file)) {
                $this->addLog("file dose not exist: $file)");
                continue;
            }

            $this->addLog("get file contents (file:$file)");
            $fileContents = $this->getFileContents($file);

            if (!$fileContents) {
                continue;
            }

            $this->currentlyParsedFile = $file;
            $this->loadCurrentDirectory();
            $this->loadUpperDir();
            $rawArticles = $this->getRawArticles($fileContents);
            foreach ($rawArticles as $rawArticle) {
                $article = $this->getArticle($rawArticle);
                $articles[] = $article;
            }
        }
        return $articles;
    }

    public function customInit()
    {
        parent::customInit();
        $this->charEncoding = 'UTF-8';
        $this->defaultLang = $this->model->getLanguageId('en');
    }


    protected function getVideos(&$text)
    {
        $this->addLog("Getting videos");
        $videos = array();
        $matches = null;
        $path = "/usr/local/syndigate/tmp/parser_6338_cache/images";
        $matches = $this->textFixation($this->getElementByName('fulltext', $text));
        $enclosure = syndParseHelper::getImgElements($matches, 'iframe', 'src');
        $link = html_entity_decode($enclosure[0], ENT_COMPAT);
        $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
        $extension = "mp4";
        $file = sha1($link) . ".$extension";
        $copiedImage = $this->copyUrlVideoIfNotCached($link);
        $video_name = $this->model->getVidReplacement($file, $path, 6338);
        $video_name = str_replace(VIDEOS_PATH, VIDEOS_HOST, $video_name);

        $video = array();
        $video['video_name'] = $video_name;
        $video['original_name'] = $link;
        $video['video_caption'] = $videoName;
        $video['mime_type'] = $extension;
        $video['video_type'] = $extension;
        $date = $this->textFixation($this->getCData($this->getElementByName('date', $text)));
        $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
        if (!isset($video['video_name']) && empty($video['video_name'])) {
            $video = array();
        }
        $videos[] = $video;
        return $videos;
    }

    public function copyUrlVideoIfNotCached($imageUrl) {
        $baseName = sha1($imageUrl) . ".mp4";
        $copiedImage = $this->imgCacheDir . $baseName;

        if (!is_dir($this->imgCacheDir)) {
            mkdir($this->imgCacheDir, 0755, true);
        }

        if (!file_exists($copiedImage)) {
            $options = array(
                CURLOPT_RETURNTRANSFER => true, // return web page
                CURLOPT_HEADER => false, // do not return headers
                CURLOPT_FOLLOWLOCATION => true, // follow redirects
                CURLOPT_USERAGENT => "spider", // who am i
                CURLOPT_AUTOREFERER => true, // set referer on redirect
                CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
                CURLOPT_TIMEOUT => 120, // timeout on response
                CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
            );

            $ch = curl_init($imageUrl);
            curl_setopt_array($ch, $options);
            $imgContent = curl_exec($ch);
            curl_close($ch);
            $myfile = fopen($copiedImage, "w");
            fwrite($myfile, $imgContent);
            fclose($myfile);
            if (!empty($imgContent)) {
                return $copiedImage;
            } else {
                return false;
            }
        } else {
            return $copiedImage;
        }
    }

}
