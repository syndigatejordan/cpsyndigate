<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher:  AmmanNet 
	// Titles   : Shabab [Arabic]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_756 extends syndParseRss
	{
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('ar');
		}				

		protected function getStory(&$text) {			
			$this->addLog("getting article text");			
			$text = $this->getCData($this->getElementByName('content:encoded', $text));			
			return $this->textFixation($text);
		}
	}
