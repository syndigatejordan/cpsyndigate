<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : China Economic Net  [# publisher id = 1797]
//Title      : Economic Daily News [ Spanish ] 
//Created on : May 10, 2022, 11:15:27 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12612 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}