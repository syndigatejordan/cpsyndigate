<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : China News Service (CNS)  [# publisher id = 1059]
//Title      : Chinanews - China News Service (CNS) [ Chinese ] 
//Created on : Oct 19, 2020, 9:43:06 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6550 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh'); 
	} 
}