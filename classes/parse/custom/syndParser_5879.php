<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Investigative Reporting Workshop  [# publisher id = 1277]
//Title      : Investigative Reporting Workshop [ Arabic ] 
//Created on : Dec 11, 2019, 1:20:28 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5879 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}