<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Crisp Network  [# publisher id = 1331]
//Title      : Crisp Network: Voice-17-October [ Arabic ] 
//Created on : Jul 09, 2020, 8:17:44 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6304 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}