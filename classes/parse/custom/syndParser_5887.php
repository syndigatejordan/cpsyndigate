<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ensia  [# publisher id = 1283]
//Title      : Ensia [ English ] 
//Created on : Dec 11, 2019, 1:59:18 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5887 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
