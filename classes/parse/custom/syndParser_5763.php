<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Altibbi Ltd.  [# publisher id = 1259]
//Title      : Altibbi - Labs [ Arabic ] 
//Created on : Sep 11, 2019, 11:41:57 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5763 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    $date = preg_replace('/ \+(.*)/is', '', $date);
    return date('Y-m-d', strtotime($date));
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) srcset=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) sizes=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) width=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) height=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getAbstract(&$text) {
    $this->addLog('getting article summary');
    return trim(strip_tags($this->textFixation($this->getElementByName('description', $text))));
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $this->story = $this->getCData($this->getElementByName('body', $text));
    $this->story = str_replace("&amp;", "&", $this->story);
    $this->story = $this->textFixation($this->story);
    preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
      if (!empty($image_caption[0])) {
        $image_caption = $image_caption[0];
      } else {
        $image_caption = syndParseHelper::getImgElements($img, 'img', 'title');
        if (!empty($image_caption[0])) {
          $image_caption = $image_caption[0];
        } else {
          $image_caption = "";
        }
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePathold = $imageInfo[0];
      $imagePath = preg_replace("/\?(.*)/is", "", $imagePathold);
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        $this->addLog("No Path");
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePathold, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = sha1($imageUrl) . ".jpeg";
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );

      $ch = curl_init($imageUrl);
      curl_setopt_array($ch, $options);
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
