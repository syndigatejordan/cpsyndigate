<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : Islamic Republic News Agency  [# publisher id =799 ] 
//Title     : Islamic Republic News Agency [ English ]
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2453 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $body = $this->getElementByName('fulltext', $text);
    $body = html_entity_decode($body);
    $body = str_replace("&amp;", "&", $body);
    $body = str_replace("&amp;nbsp;", "& ", $body);
    $body = str_replace("&;nbsp;", "", $body);
    $headline = $this->getCData($this->getElementByName('title', $text));
    $body = str_replace("<p><strong></strong></p>", "", $body);
    $body = str_replace("�", "", $body);
    $body = html_entity_decode($body, ENT_QUOTES, "UTF-8");
    $body = preg_replace('/<img[^>]+\>/i', '', $body);
    return $body;
  }


}
