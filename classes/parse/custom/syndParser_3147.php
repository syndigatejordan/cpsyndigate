<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : General Portfolio Ltd.  [# publisher id = 1151]
//Title      : The Ghanaian Chronicle [ English ] 
//Created on : Aug 03, 2021, 12:38:09 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3147 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}