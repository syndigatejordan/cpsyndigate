<?php

////////////////////////////////////////////////////////////////////////
// Publisher  : MBA (Management and Business for Africa)
// Titles     : African Manager [ English ]
// Created on : Oct 08, 2018, 5:33:03 AM
// Author     : eyad
////////////////////////////////////////////////////////////////////////

class syndParser_713 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->getCData($this->getElementByName('title', $text));
    return $this->textFixation($headline);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('PubDate', $text);
    return date('Y-m-d', strtotime($date));
  }

  /*
    protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return trim($this->textFixation($this->getElementByName('dc:creator', $text)));
    }

    public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getElementsByName('category', $text);
    $originalCats = array();

    if (!empty($cats)) {
    foreach ($cats as $cat) {
    $originalCats[] = $this->textFixation($this->getCData($cat));
    }
    }
    return implode(', ', $originalCats);
    }
   */

  protected function getAbstract(&$text) {
    $this->addLog("getting article summary");
    return $this->textFixation($this->getElementByName('description', $text));
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = trim(preg_replace('!\s+!', ' ', $this->story));
    return $this->story;
  }

  protected function getImages(&$text) {
      $this->story = $this->getCData($this->getElementByName('content', $text));
      $this->story = str_replace("&nbsp;", " ", $this->story);
      $this->story = $this->textFixation($this->story);

      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      return array();
    $imagesArray = array();
    $imagePath = $this->getCData($this->getElementByName('image', $text));
    if ($imagePath) {
      $this->addLog("getting article images");
      $image_caption = '';
      if (!empty($image_caption[0])) {
        $image_caption = $image_caption[0];
      } else {
        $image_caption = "";
      }
      if (!$this->checkImageifCached($imagePath)) {
        $imagePath = str_replace(' ', '%20', $imagePath);
        $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
        if ($copiedImage) {
          $images = $this->getAndCopyImagesFromArray(array($copiedImage));
          if (!empty($image_caption)) {
            $images[0]['image_caption'] = $image_caption;
          }
          $name_image = explode('/images/', $copiedImage);
          if ($images[0]['image_caption'] == $name_image[1]) {
            $images[0]['image_caption'] = '';
          }
          $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
          $images[0]['is_headline'] = TRUE;
          array_push($imagesArray, $images[0]);
        }
      }
    }

    $this->story = $this->getCData($this->getElementByName('content', $text));
    $this->story = str_replace("&nbsp;", " ", $this->story);
    $this->story = $this->textFixation($this->story);

    preg_match_all("/<img(.*?)>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
      if (!empty($image_caption[0])) {
        $image_caption = $image_caption[0];
      } else {
        $image_caption = "";
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      $imagePath = preg_replace("/\?(.*)/is", "", $imagePath);
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
