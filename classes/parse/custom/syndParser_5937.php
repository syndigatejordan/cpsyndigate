<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Human Rights Watch  [# publisher id = 1302]
//Title      : Human Rights Watch - News [ simplified Chinese ] 
//Created on : Feb 29, 2020, 1:56:40 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5937 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('zh-Hans');
  }
}
