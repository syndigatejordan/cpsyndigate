<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : NST Publishing LTD  [# publisher id = 1487]
//Title      : European Business Magazine [ English ] 
//Created on : Sep 19, 2021, 10:20:21 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7431 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}