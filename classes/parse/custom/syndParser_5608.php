<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Conversation Africa, Inc.  [# publisher id = 1236]
//Title      : The Conversation (Africa Edition) [ English ] 
//Created on : Sep 27, 2021, 7:41:55 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5608 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}