<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Tower Media Middle East FZ LLC  [# publisher id = 54]
//Title      : Al Aan TV [ Arabic ] 
//Created on : Feb 08, 2017, 10:12:25 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_160 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}