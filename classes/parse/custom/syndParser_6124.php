<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Namibia Press Agency  [# publisher id = 1346]
//Title      : Namibia Press Agency (NAMPA) - NAMPA TV [ English ] 
//Created on : Sep 23, 2021, 8:17:25 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6124 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
    protected function getVideos(&$text)
    {
        $this->addLog("Getting videos");
        $videos = array();
        $matches = null;
        $path = "/usr/local/syndigate/tmp/parser_6124_cache/images";
        $matches = $this->textFixation($this->getElementByName('fulltext', $text));
        $enclosure = syndParseHelper::getImgElements($matches, 'iframe', 'src');
        $link = html_entity_decode($enclosure[0], ENT_COMPAT);
        $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
        $extension = "mp4";
        $file = sha1($link) . ".$extension";
        $copiedImage = $this->copyUrlVideoIfNotCached($link);
        $video_name = $this->model->getVidReplacement($file, $path, 6124);
        $video_name = str_replace(VIDEOS_PATH, VIDEOS_HOST, $video_name);

        $video = array();
        $video['video_name'] = $video_name;
        $video['original_name'] = $link;
        $video['video_caption'] = $videoName;
        $video['mime_type'] = $extension;
        $video['video_type'] = $extension;
        $date = $this->textFixation($this->getCData($this->getElementByName('date', $text)));
        $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
        if (!isset($video['video_name']) && empty($video['video_name'])) {
            $video = array();
        }
        $videos[] = $video;
        return $videos;
    }

    public function copyUrlVideoIfNotCached($imageUrl) {
        $baseName = sha1($imageUrl) . ".mp4";
        $copiedImage = $this->imgCacheDir . $baseName;

        if (!is_dir($this->imgCacheDir)) {
            mkdir($this->imgCacheDir, 0755, true);
        }

        if (!file_exists($copiedImage)) {
            $options = array(
                CURLOPT_RETURNTRANSFER => true, // return web page
                CURLOPT_HEADER => false, // do not return headers
                CURLOPT_FOLLOWLOCATION => true, // follow redirects
                CURLOPT_USERAGENT => "spider", // who am i
                CURLOPT_AUTOREFERER => true, // set referer on redirect
                CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
                CURLOPT_TIMEOUT => 120, // timeout on response
                CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
            );

            $ch = curl_init($imageUrl);
            curl_setopt_array($ch, $options);
            $imgContent = curl_exec($ch);
            curl_close($ch);
            $myfile = fopen($copiedImage, "w");
            fwrite($myfile, $imgContent);
            fclose($myfile);
            if (!empty($imgContent)) {
                return $copiedImage;
            } else {
                return false;
            }
        } else {
            return $copiedImage;
        }
    }

}
