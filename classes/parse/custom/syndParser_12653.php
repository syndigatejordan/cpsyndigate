<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : MEI Global, LLC  [# publisher id = 1818]
//Title      : States News Service [ English ] 
//Created on : Jul 03, 2022, 12:31:57 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_12653 extends syndParseXmlContent
{

    public $story;

    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('en');
        $this->extensionFilter = 'xml';
    }

    public function getHeadline(&$text)
    {
        $this->addLog("getting article headline");
        return trim($this->textFixation($this->getElementByName('Headline1', $text)));
    }

    public function getArticleDate(&$text)
    {
        $this->addLog("getting article date");
        $date = $this->getElementByName('ContentDate', $text);
        if ($date) {
            return $this->dateFormater($date);
        }
        return parent::getArticleDate($text);
    }

    public function getStory(&$text)
    {
        $this->addLog('Getting article story');
        $this->story = $this->textFixation($this->getCData($this->getElementByName('BodyContent', $text)));
        $this->story = preg_replace("/\r\n|\r|\n/", "</p><p>", $this->story);
        if (!empty($this->story)) {
            $this->story = "<p>" . $this->story . "</p>";
        }
        $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) width=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) height=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) align=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
        $this->story = trim(preg_replace('!\s+!', ' ', $this->story));
        if (empty($this->story)) {
            return '';
        }
        return $this->story;
    }

    protected function getRawArticles(&$fileContents)
    {
        //get articles
        $this->addLog("getting articles raw text");
        $matchs = null;
        preg_match_all("/<Document(.*?)<\/Document>/is", $fileContents, $matchs);
        $matchs = $matchs[0];
        return $matchs;
    }

    protected function getImages(&$text)
    {
        return array();
    }
}
