<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : France Médias Monde (FMM)  [# publisher id = 383]
//Title      : FRANCE 24 Infographics [ French ] 
//Created on : Aug 07, 2018, 7:46:06 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3538 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('fr');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $matches = null;
    preg_match_all("/<item><nid>(.*?)<shares\/><\/item>/is", $fileContents, $matches);
    $articles = $matches[0];
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = trim($this->textFixation($this->getElementByName('title', $text)));
    return $this->textFixation($headline);
  }

  protected function getAbstract(&$text) {
    $this->addLog('getting article summary');
    return trim($this->textFixation($this->getElementByName('surtitle', $text)));
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('created', $text);
    $date =substr($date, 0, -3);
    return date('Y-m-d', $date);
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $intro = trim($this->textFixation($this->getCData($this->getElementByName('intro', $text))));
    if (!empty($intro)) {
      $intro = "<p>$intro</p>";
    }
    $body = trim($this->textFixation($this->getCData($this->getElementByName('body', $text))));
    if (!empty($body)) {
      $body = "<p>$body</p>";
    }
    $story = $intro . $body;
    if (empty($story)) {
      $story = trim($this->textFixation($this->getElementByName('title', $text)));
    }
    return $story;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getCData($this->getElementByName('source', $text)));
  }

  public function getArticleReference(&$text) {
    $this->addLog("getting article reference");
    return $this->textFixation($this->getCData($this->getElementByName('url', $text)));
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $slideshow_images = $this->getElementByName('slideshow_images', $text);
    $matches = null;
    preg_match_all("/<item><credit>(.*?)<\/crop><\/item>/is", $slideshow_images, $matches);
    foreach ($matches[0] as $img) {
      $this->addLog("getting article images");
      $credit = null;
      preg_match("/<credit>(.*?)<\/credit>/is", $img, $credit);
      $credit = $credit[1];

      $captiont = null;
      preg_match("/<caption>(.*?)<\/caption>/is", $img, $caption);
      $caption = $caption[1];
      $imageCaption = $credit . " / " . $caption;
      $url = null;
      preg_match("/<\/code><url>(.*)<\/url>/is", $img, $url);
      $imagePath = $url[1];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no path";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['image_caption'] = $imageCaption;
      $images[0]['is_headline'] = FALSE;
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
