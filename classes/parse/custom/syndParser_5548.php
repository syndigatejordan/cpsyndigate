<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Business News for Press and Distribution  [# publisher id = 9]
//Title      : Daily News Egypt - Business [ English ] 
//Created on : Aug 18, 2021, 6:16:31 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5548 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}