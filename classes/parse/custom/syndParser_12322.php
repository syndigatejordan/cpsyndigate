<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Mark Allen Group (MAG)  [# publisher id = 1304]
//Title      : Electric & Hybrid Vehicle Technology International [ English ] 
//Created on : Jan 11, 2022, 1:41:31 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12322 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}