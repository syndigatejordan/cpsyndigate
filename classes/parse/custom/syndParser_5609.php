<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Conversation Media Group Ltd  [# publisher id = 1237]
//Title      : The Conversation (Australia Edition) [ English ] 
//Created on : Sep 27, 2021, 7:43:03 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5609 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}