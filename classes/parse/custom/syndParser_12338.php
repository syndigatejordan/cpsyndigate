<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ESG Investor Ltd.  [# publisher id = 1735]
//Title      : ESG Investor [ English ] 
//Created on : Jan 16, 2022, 12:49:18 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12338 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}