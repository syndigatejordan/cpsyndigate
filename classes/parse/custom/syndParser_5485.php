<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Manhal FZ LLC (Arabic)  [# publisher id = 1184]
//Title      : Islamic Finance Hub (Theses) [ Arabic ] 
//Created on : Mar 27, 2019, 9:26:40 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

require_once '/usr/local/syndigate/classes/parse/syndParseAlManhal.php';

class syndParser_5485 extends syndParseAlManhal {

  protected function getArticleDate(&$text) {

    $this->addLog("getting articles date");
    $date = $this->textFixation($this->getCData($this->getElementByName('Date', $text)));
    $date = str_replace("<", "", $date);
    $date = str_replace(">", "", $date);
    $date = trim(preg_replace('!\s+!', ' ', $date));
    if (preg_match("/GMT/", $date)) {
      $date = trim(preg_replace("/GMT(.*)/is", "", $date));
      $date = date("Y-m-d", strtotime($date));
    } else {
      $date = explode('/', $date);
      $date = $date[2] . "-" . $date[1] . "-" . $date[0];
    }
    return $date;
  }

}
