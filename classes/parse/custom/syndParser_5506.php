<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The New York Times Company / nytLicensing  [# publisher id = 1197]
//Title      : Freakonomics [ English ] 
//Created on : Apr 01, 2019, 10:14:00 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5506 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}