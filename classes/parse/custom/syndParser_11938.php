<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Hearst Newspapers  [# publisher id = 1600]
//Title      : San Francisco Chronicle [ English ] 
//Created on : Jul 04, 2021, 12:46:35 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11938 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}