<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : EURL Millenium Press  [# publisher id = 1760]
//Title      : Le Courrier d'Algerie [ French ] 
//Created on : Feb 24, 2022, 7:13:35 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12392 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}