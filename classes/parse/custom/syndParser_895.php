<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Central Asian News Service PF  [# publisher id = 262]
//Title      : Central Asian News Service [ Russian ] 
//Created on : Mar 11, 2018, 1:51:31 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_895 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}