<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Breaking Media, Inc.  [# publisher id = 531]
//Title      : Dealbreaker [ English ] 
//Created on : Sep 26, 2021, 9:45:29 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1575 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}