<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : Bahrain News Agency (BNA)
//Title     : Bahrain News Agency (BNA) [ Arabic ]
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_854 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

}