<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CPI Construction  [# publisher id =307 ]    
//Title      : BuildGreen [ Arabic ] 
//Created on : Jul 20, 2016, 15:30:16 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1023 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
