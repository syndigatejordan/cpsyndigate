<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Emirates News Agency (WAM)
// Titles    : Emirates News Agency [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_55 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->charEncoding = 'WINDOWS-1256';
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting raw articles text");
    return $this->getElementsByName('NewsComponent', $fileContents);
  }

  protected function getStory(&$text) {
    //get body text
    $this->addLog("getting article text");
    return $this->textFixation($this->getElementByName('DataContent', $text));
  }

  protected function getHeadline(&$text) {
    //get headline
    $this->addLog("getting article headline");
    $headline = $this->getElementByName('HeadLine', $text);
    return $headline;
  }

  protected function getAbstract(&$text) {
    $this->addLog("getting article Summary");
    $summary = $this->getElementByName('SlugLine', $text);
    return $summary;
  }

  protected function getArticleDate(&$text) {
    //get article date
    $this->addLog("getting article date");
    $date = trim($this->getElementByName('dateline', $text));
    if ($date) {
      //DateLine Example:   21 April (WAM) -
      $date = explode("(", $date);
      $date = ltrim($date[0]) . date("Y");
      $date = date("Y-m-d", strtotime($date));
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  protected function getIptcId(&$text) {
    $this->addLog("getting category name");
    $originalCategoryName = $this->getElementByName('section', $text);
    $originalCategoryName = $this->getElementByName('s1', $originalCategoryName);
    $iptcId = $this->model->getIptcCategoryId($originalCategoryName);
    if (!$iptcId) {
      return parent::getIptcId($text);
    }
    return $iptcId;
  }

}

?>
