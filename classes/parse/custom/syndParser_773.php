<?php 
	////////////////////////////////////////////////////////////////////////
	// Publisher : IANS India Pvt. Ltd.
	// Titles    : Alhindelyom.com
	////////////////////////////////////////////////////////////////////////

	class syndParser_773 extends syndParseXmlContent {
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('ar');
		}
		
		protected function getRawArticles(&$fileContents) {
			$this->addLog("getting raw articles text");
			$rawArticles = $this->getElementsByName('article', $fileContents);
			return $rawArticles;
		}	
		
		protected function getHeadline(&$text) {
			$this->addLog("getting article headline");
			return $this->textFixation($this->getElementByName("headline", $text));
		}
		
		protected function getStory(&$text) {
			$this->addLog("getting article story");
			$story = $this->textFixation($this->getElementByName("content", $text));
			return str_replace("align='justify'", '', $story);
		}
		
		protected function getAbstract(&$text) {
			$this->addLog("getting article abstract");
			return $this->textFixation($this->getElementByName("summary", $text));
		}
		
		protected function getAuthor(&$text) {
			$this->addLog("getting artcile Author");
			return $this->textFixation($this->getElementByName("author", $text));
		}
		
		protected function getOriginalCategory(&$text) {
			$this->addLog("getting article original category");
			return $this->textFixation($this->getElementByName('cat', $text));
		}
		
		protected function getArticleDate(&$text) {
			$this->addLog("getting article date");
			$date = $this->getElementByName('date', $text);
			return $this->dateFormater($date);
		}
		
		protected function getIptcId(&$text) {
			//get category name from file name
			$this->addLog("getting category name");
			$originalCategoryName = $this->getElementByName('cat', $text);
			//if there is no category tag, read it from the file name
			if(!$originalCategoryName) {
				$originalCategoryName = syndParseHelper::getCategoryNameFromFileName($this->currentlyParsedFile);
			}
			$iptcId = $this->model->getIptcCategoryId($originalCategoryName);
			return ($iptcId ? $iptcId : parent::getIptcId($text));
		}
		
	}
