<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : Okaz Organization for Press & Publication
//Title     : The Saudi Gazette [ English ]
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1792 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
