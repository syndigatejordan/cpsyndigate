<?php

///////////////////////////////////////////////////////////////////////////
//Publisher : Cover Images Limited  [# publisher id =1257 ]  
//Titles    : COVER Images - Picture Features (International Showbiz)  [ English ]
//Created on : Dec 12, 2019, 10:48:41 AM
//Author     : eyad
///////////////////////////////////////////////////////////////////////////

class syndParser_5846 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xmp';
  }

  //Handle Empty body
  public function parse() {
    $articles = array();
    foreach ($this->files as $file) {
      if ($this->extensionFilter) {
        if (!syndParseHelper::isFileExtension($file, $this->extensionFilter)) {
          $this->addLog("Wrong extension for file : $file)");
          continue;
        }
      }
      if (!file_exists($file)) {
        $this->addLog("file dose not exist: $file)");
        continue;
      }

      $this->addLog("get file contents (file:$file)");
      $fileContents = $this->getFileContents($file);

      if (!$fileContents) {
        continue;
      }

      $this->currentlyParsedFile = $file;
      $this->loadCurrentDirectory();
      $this->loadUpperDir();
      $rawArticles = $this->getRawArticles($fileContents);
      foreach ($rawArticles as $rawArticle) {
        $article = $this->getArticle($rawArticle);
        $articles[] = $article;
      }
    }
    return $articles;
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('x:xmpmeta', $fileContents);
    return $articles;
  }

  protected function getStory(&$text) {
    if (!empty($story)) {
      $this->story = preg_replace("/\r\n|\r|\n/", "</p><p>", $this->story);
      $this->story = '<p>' . $this->story . '</p>';
    }
    $this->story = str_replace('<p></p>', '', $this->story);
    return $this->story;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $Headline = $this->textFixation($this->getCData($this->getElementByName('photoshop:Headline', $text)));
    $Headline = html_entity_decode($Headline, ENT_QUOTES);
    return $Headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('photoshop:DateCreated', $text);
    $date = trim(preg_replace('/T(.*)/is', '', $date));
    return date('Y-m-d', strtotime($date));
  }

  protected function getImages(&$text) {
    $this->addLog("getting article image");
    $this->story = "";
    $imagesArray = array();
    $body = $this->textFixation(strip_tags($this->getCData($this->getElementByName('dc:description', $text))));
    $image_Name = str_replace("xmp", "jpg", basename($this->currentlyParsedFile));
    $image_full_path = $this->currentDir . $image_Name;
    $image_full_path = str_replace($this->title->getParseDir(), $this->title->getHomeDir(), $image_full_path);
    $image_caption = html_entity_decode($body, ENT_QUOTES);
    $image_caption = trim(preg_replace('/\s\s+/', ' ', $image_caption));
    $image_caption = preg_replace('!\s+!', ' ', $image_caption);
    $currentDir = str_replace($image_Name, "", $image_full_path);
    if (file_exists($image_full_path)) {
      //the video name is same as the image name here  		
      $name = $this->model->getImgReplacement($image_Name, $currentDir, 5850);
      //$this->addLog("getting article image  image_Name $image_Name");
      //$this->addLog("getting article currentDir $currentDir");
      //$this->addLog("getting article image name $name");
      $img['img_name'] = str_replace(IMGS_PATH, "https://syndigateimages.s3.amazonaws.com/syndigate/imgs/", $name);

      $img['original_name'] = $image_Name;
      $img['image_caption'] = $image_caption;
      $img['is_headline'] = FALSE;
      $img['image_original_key'] = sha1($image_Name);
      array_push($imagesArray, $img);
      $body = '<p>' . trim(str_replace(array("\r\n", "\r", "\n"), '</p><p>', $body)) . '</p>';
      $body = str_replace("<p>   </p>", "", $body);
      $body = str_replace("<p></p>", "", $body);
      $this->story="<div><img src=\"{$img['img_name']}\" alt=\"$image_caption\"/><p>$body</p></div>";
    }

    return $imagesArray;
  }

  public function getArticleOriginalId($params = array()) {
    $articleOriginalId = basename($this->currentlyParsedFile);
    $this->articleOriginalId = $articleOriginalId;
    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }
    return $this->title->getId() . "_" . sha1($articleOriginalId);
  }

}

?>
