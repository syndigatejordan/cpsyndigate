<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Apex Press and Publishing  [# publisher id = 1]
//Title      : Muscat Daily [ English ] 
//Created on : Sep 11, 2017, 11:24:39 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_927 extends syndParseCms {
  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

}
