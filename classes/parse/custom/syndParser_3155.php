<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ministry of Information, Communication and Technology- Malawi Government  [# publisher id = 1159]
//Title      : Malawi News Agency [ English ] 
//Created on : Aug 30, 2017, 6:40:35 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3155 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}