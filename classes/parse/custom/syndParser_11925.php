<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : REFINITIV LIMTED  [# publisher id = 1597]
//Title      : ZAWYA by Refinitiv (Press Releases) [ Arabic ] 
//Created on : Nov 30, 2021, 7:34:02 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11925 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}