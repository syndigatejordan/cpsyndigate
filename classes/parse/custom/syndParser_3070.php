<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Abu Dhabi Media Company  [# publisher id = 233]
//Title      : The National - Personal Finance [ English ] 
//Created on : May 04, 2017, 11:47:04 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3070 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = $this->textFixation($this->getCData($this->getElementByName('fulltext', $text)));
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getArticle(&$text) {
    try {
      $allowedTags = '<h1><h2><h3><h4><h5><h6><b><i><u><p><hr><br><br/><br /><big><em><small><strong><sub><sup><ins><del><s><strike><code><kbd><q><img><BLOCKQUOTE><cite><dd><div><span><ul><li><ol><style><table><td><th><tr><tbody><thead><a><iframe><embed><video><source><object><param><script>';

      //$article['iptc_id']     = $this->getIptcId($text);
      $article['title_id'] = $this->title->getId();
      $article['lang_id'] = $this->getLangId($text);
      $article['headline'] = strip_tags($this->getHeadline($text));
      $article['abstract'] = strip_tags($this->getAbstract($text));
      $article['author'] = strip_tags($this->getAuthor($text));
      $article['date'] = $this->getArticleDate($text);
      $article['has_time'] = $this->getHasTime();
      $article['parsed_at'] = time();
      $article['original_data'] = $this->getOriginalData($text);
      $article['images'] = $this->getImages($text);
      $article['videos'] = $this->getVideos($text);

      $issueNum = isset($article['original_data']['issue_number']) ? $article['original_data']['issue_number'] : 0;
      $article['original_data']['original_article_id'] = $this->getArticleOriginalId(array('text' => &$text, 'headline' => $article['headline'], 'articleDate' => $article['date'], 'issueNum' => $issueNum));
      $article['original_data']['old_original_article_id'] = syndParseContent::getArticleOriginalId(array('text' => &$text, 'headline' => $article['headline'], 'articleDate' => $article['date'], 'issueNum' => $issueNum));
      $article['original_data']['original_category'] = strip_tags($this->getOriginalCategory($text));
      $article['original_data']['reference'] = strip_tags($this->getArticleReference($text));

      /* 			
        Because getIptcId remove the category -in some parsers like AbText- the text and we do use this in
        other function we put it at last
       */
      $article['iptc_id'] = $this->getIptcId($text);

      $this->processExtraTags($text);
      $article['story'] = strip_tags($this->getStory($text), $allowedTags);
    } catch (Exception $e) {
      $this->addLog($e->getMessage(), ezcLog::ERROR);
      $this->errorMessage = $e->getMessage();
      $this->defaultProcessState = ezcLog::FAILED_AUDIT;
    }

    return $article;
  }

}
