<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IPS - Inter Press Service  [# publisher id = 1436]
//Title      : IPS - Inter Press Service [ Italian ] 
//Created on : Sep 12, 2021, 7:27:19 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6712 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('it'); 
	} 
}