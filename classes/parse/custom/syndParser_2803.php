<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Reach MENA FZ LLC  [# publisher id = 983]
//Title      : HnaUAE.com [ Arabic ] 
//Created on : Sep 19, 2021, 7:04:54 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2803 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}