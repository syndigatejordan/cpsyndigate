<?php
////////////////////////////////////////////////////////////////////////
// Publisher : Euclid Infotech Pvt. Ltd
// Titles    : tendersinfo mena
////////////////////////////////////////////////////////////////////////
/*

The content will be moved to 193 so it will always be zero count
*/
class syndParser_785 extends syndParseXmlContent {
	
	public function customInit() {
		parent::customInit();
		$this->defaultLang = $this->model->getLanguageId('en');
	}
	
	
	public function beforeParse($params = array()) {
		
		parent::beforeParse($params = array());
		
		//
		//Getting all files and moving it to Albawaba tenders info mena
		//
		
		// getting target title
		$c = new Criteria();
		$c->add(TitlePeer::ID , 193);
		$targetTitle = TitlePeer::doSelectOne($c);
		
		if($targetTitle) {
			$files = scandir($this->title->getHomeDir());
			
			foreach ($files as $file) {
				if($file!= '.' && $file !='..') {
										
					if(!file_exists($targetTitle->getHomeDir() . $file)) {
						$this->addLog("moving " . $this->title->getHomeDir() . $file . " to " . $targetTitle->getHomeDir());
						copy($this->title->getHomeDir() . $file, $targetTitle->getHomeDir() . $file);
					}
				}
			}
		}
		
	}
	
	
	protected function getRawArticles(&$fileContents) {
		$this->addLog("getting raw articles text");
		return array();
		//return $this->getElementsByName('Row', $fileContents);
	}
	
	protected function getHeadline(&$text) {
		$this->addLog('getting article headline');
		return $this->textFixation($this->getElementByName('news_title', $text));
	}
	
	protected function getStory(&$text) {
		$this->addLog('getting article story');
		
		$story  ="<p>";
		$story .= $this->textFixation($this->getElementByName('news_details', $text));
		$story  = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "</p><p>", $story);
		$story .= "</p>"; 
		return $story;
	}
}
