<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Grupo Globo  [# publisher id = 1568]
//Title      : O Globo [ Portuguese ] 
//Created on : Apr 25, 2021, 11:30:46 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10812 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}