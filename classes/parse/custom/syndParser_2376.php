<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : NewsBites Pty Ltd.  [# publisher id =341 ] 
// Titles    : NewsBites Finance - Research Reports (Daily)
///////////////////////////////////////////////////////////////////////////

class syndParser_2376 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    $this->loadCurrentDirectory();
    $this->addLog("getting raw articles text");
    return $this->getElementsByName('story', $fileContents);
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return $this->textFixation($this->getElementByName('Headline', $text));
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('Date', $text);
    $date = explode(' ', $date);
    $date = explode('/', $date[0]);
    $date = "$date[2]-$date[1]-$date[0]";
    $date = preg_replace('!\s+!', '', $date);
    return date('Y-m-d', strtotime($date));
  }

  protected function getOriginalCategory(&$text) {
    $this->addLog("getting article category");
    $type=trim(trim($this->textFixation($this->getElementByName('type', $text))), ',');
    $id=trim(trim($this->textFixation($this->getElementByName('ID', $text))), ',').','.$type;
    return $id;
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $body = "";
    $article = trim($this->replaceEntity($this->textFixation($this->getElementByName('article', $text))));
    $ID = trim($this->replaceEntity($this->textFixation($this->getElementByName('ID', $text))));
    $Country = trim($this->replaceEntity($this->textFixation($this->getElementByName('country', $text))));
    $ASXcode = trim($this->replaceEntity($this->textFixation($this->getElementByName('ASXcode', $text))));
    $Company = trim($this->replaceEntity($this->textFixation($this->getElementByName('Company', $text))));
    $keyword = trim($this->replaceEntity($this->textFixation($this->getElementByName('keyword', $text))));

    if ($article) {
      $body .= "<p>$article </p>";
    }
    if ($ID) {
      $body .= "<p> ID : $ID </p>";
    }
    if ($Country) {
      $body .= "<p> Country : $Country </p>";
    }

    if ($ASXcode) {
      $body .= "<p> ASXcode : $ASXcode </p>";
    }
    if ($Company) {
      $body .= "<p> Company :  $Company </p>";
    }
    if ($keyword) {
      $body .= "<p> keyword : $keyword </p>";
    }
    return $body;
  }

  protected function getImages(&$text) {
    $this->addLog("getting article images");
    $image_Name = trim($this->replaceEntity($this->textFixation($this->getElementByName('link', $text))));
    if (!empty($image_Name)) {
      $image_full_path = $this->currentDir . $image_Name;
      $original_name = explode('.', $image_Name);
      if (file_exists($image_full_path)) {
        //the video name is same as the image name here  		
        $name = $this->model->getImgReplacement($image_Name, $this->currentDir, 2376);
        $img['img_name'] = str_replace(IMGS_PATH, IMGS_HOST, $name);
        $img['original_name'] = $original_name[0];
        $img['image_caption'] = $image_Name;
        $img['is_headline'] = true;
        $images[] = $img;
        return $images;
      } else {

        return false;
      }
    } else {

      return false;
    }
  }

  protected function getOriginalData(&$text) {
    $dir='341/2376/'.date('Y/m/d');
    $file = explode($dir, $this->currentlyParsedFile);
    $file = explode('.', $file[1]);
    $originalData['extras']['original_file'] = $file[0];
    return $originalData;
  }

}
