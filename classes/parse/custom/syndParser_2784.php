<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ALP Content Services operating as AL Publishing  [# publisher id = 979]
//Title      : Al-Business.net [ Arabic ] 
//Created on : Jul 13, 2016, 1:07:38 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2784 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}