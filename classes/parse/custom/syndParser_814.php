<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Future Publishing Limited Quay House  [# publisher id = 232]
//Title      : TechRadar [ English ] 
//Created on : Aug 30, 2021, 7:49:21 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_814 extends syndParseCms
{
    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('en');
    }

    protected function getStory(&$text)
    {
        $this->addLog("getting article body");
        $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
        $this->story = preg_replace('/<figcaption>(.*?)<\/figcaption>/i', '', $this->story);
        $this->story = preg_replace('/<figure>(.*?)<\/figure>/i', '', $this->story);
        $this->story = preg_replace('!\s+!', ' ', $this->story);
        if (empty($this->story))
            return $this->textFixation($this->getElementByName('fulltext', $text));
        else {
            return $this->story;
        }
    }
}

