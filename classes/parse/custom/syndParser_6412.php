<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : InSight Crime  [# publisher id = 1355]
//Title      : InSight Crime - Investigations (Full PDF Version) [ English ] 
//Created on : Aug 25, 2020, 12:46:40 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6412 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}