<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  :  The Saudi Arabian Oil Company  [# publisher id = 743]
//Title      : Saudi Aramco Environmental protection reports [ English ] 
//Created on : Feb 02, 2016, 12:05:12 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2331 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}