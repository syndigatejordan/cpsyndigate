<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Crisp Network  [# publisher id = 1331]
//Title      : Crisp Network: Work and Skills [ Arabic ] 
//Created on : Jul 09, 2020, 8:17:52 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6305 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}