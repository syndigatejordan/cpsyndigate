<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Foundation for Economic Education  [# publisher id = 1272]
//Title      : Foundation for Economic Education [ English ] 
//Created on : Dec 10, 2019, 10:21:46 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5874 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
