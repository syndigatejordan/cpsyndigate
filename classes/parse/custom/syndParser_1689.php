<?php 
		///////////////////////////////////////////////////////////////////////////////////// 
		//Publisher : Dubai Financial Market, PJSC.  
		//Title     : Dubai Financial Market (DFM) [ Arabic ] 
		/////////////////////////////////////////////////////////////////////////////////////
		
		class syndParser_1689 extends syndParseCms { 
			 public function customInit() { 
				 parent::customInit(); 
				 $this->defaultLang = $this->model->getLanguageId('ar'); 
			} 
		}