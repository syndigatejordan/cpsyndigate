<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : NewsBites Pty Ltd.  [# publisher id =341 ] 
// Titles    : NewsBites Finance - Stock Reports
///////////////////////////////////////////////////////////////////////////

class syndParser_2375 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $d = $this->getElementsByName('DigestName', $fileContents);
    return $d;
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $story = $this->textFixation($this->getElementByName('Abstract', $text));
    return $story;
  }

  public function getHeadline(&$text) {
    $headline = strip_tags($this->getElementByName('Headline', $text));
    return $headline;
  }

  public function getArticleDate(&$text) {
    $dateline = trim($this->getElementByName('Date', $text));
    return date('Y-m-d', strtotime($dateline));
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return trim($this->textFixation($this->getElementByName('Source', $text)));
  }

  protected function getOriginalData(&$text) {
    $dir='341/2375/'.date('Y/m/d');
    $file = explode($dir, $this->currentlyParsedFile);
    $file = explode('.', $file[1]);
    $originalData['extras']['original_file'] = $file[0];
    return $originalData;
  }

}
