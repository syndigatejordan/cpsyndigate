<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sharjah Media Center  [# publisher id = 1033]
//Title      : Sharjah 24 [ Arabic ] 
//Created on : Aug 21, 2016, 2:25:15 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2867 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}