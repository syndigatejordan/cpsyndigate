<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Iran Information & Communication News Agency  [# publisher id = 797]
//Title      : ICTna [ English ] 
//Created on : Jan 31, 2016, 5:45:17 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2450 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}