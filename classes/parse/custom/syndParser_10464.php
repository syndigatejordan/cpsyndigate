<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : SURMEDIOS S.A.  [# publisher id = 1551]
//Title      : Info Sur Diario [ Spanish ] 
//Created on : Sep 19, 2021, 10:23:38 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10464 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}