<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Shohob for Web Development  [# publisher id =670 ] 
// Titles    : Maqar [ Arabic ]
///////////////////////////////////////////////////////////////////////////

class syndParser_2153 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
    $this->extensionFilter = 'xml';

    require_once('/usr/local/syndigate/lib/getid3/getid3/getid3.php');
  }

  protected function getRawArticles(&$fileContents) {
    $this->loadCurrentDirectory();
    $this->addLog("getting raw articles text");
    $content = $this->getElementsByName('item', $fileContents);
    $content = array(trim($content [0]));
    return $content;
  }

  public function getStory(&$text) {
    $story = trim($this->getElementByName('headline', $text));
    return $story;
  }

  public function getHeadline(&$text) {
    $this->headline = trim($this->getElementByName('headline', $text));
    return $this->headline;
  }

  public function getArticleDate(&$text) {
    $date = trim($this->getElementByName('date', $text));
    return strtotime($date);
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting article video");
    $videoCaption = $this->headline;
    $videoName = trim($this->getElementByName('video', $text));
    if (empty($videoName)) {
      return array();
    }

    $videoName = $this->currentDir . $videoName;

    $videos = array();
    $videoInformation = array();
    $neededInfo = array('filesize', 'fileformat', 'encoding', 'mime_type', 'playtime_seconds', 'bitrate', 'playtime_string', 'video');

    $getID3 = new getID3;
    $realVideoPath = str_replace($this->title->getParseDir(), $this->title->getHomeDir(), $videoName);
    if (!file_exists($videoName)) {
      return array();
    }
    $videoInfo = $getID3->analyze($realVideoPath);

    foreach ($neededInfo as $key => $value) {
      $keysExist = array_keys($videoInfo);

      if (in_array($value, $keysExist)) {
        switch ($value) {
          case 'video' :
            $videoInformation['width'] = $videoInfo[$value]['resolution_x'];
            $videoInformation['height'] = $videoInfo[$value]['resolution_y'];
            break;

          case 'bitrate' :
            $videoInformation[$value] = floor($videoInfo[$value] / 1024);
            break;

          default :
            $videoInformation[$value] = $videoInfo[$value];
            break;
        }
      }
    }
    $video['video_name'] = $this->getAndCopyVideosFromArray($videoName);

    $mimeType = 'm2p';
    $video['original_name'] = $videoName;
    $video['video_caption'] = $videoCaption;
    //$video['bit_rate']       	= $this->getBitRateFromName($videoLable);
    $video['bit_rate'] = json_encode($videoInformation);
    //$bitRate . ' kbps';
    $video['mime_type'] = $mimeType;
    $video['video_type'] = $mimeType;
    $video['added_time'] = date('Y-m-d h:i:s', filemtime($video['original_name']));
    $videos[] = $video;
    return $videos;
  }

}