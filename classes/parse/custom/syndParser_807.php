<?php

////////////////////////////////////////////////////////////////////////
// Publisher : Mediaquest Corp  [# publisher id =84 ]  
// Titles    : Saneou Al Hadath [ Arabic ] 
//Created on : May 22, 2017, 10:44:25 AM
//Author     : eyad
////////////////////////////////////////////////////////////////////////

class syndParser_807 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('ar');
  }
}
