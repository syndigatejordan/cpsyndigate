<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Chamber of Commerce and Industry of Republic of Armenia  [# publisher id = 855]
//Title      : Chamber of Commerce and Industry of Republic of Armenia (CCIRA) - News [ English ] 
//Created on : Feb 08, 2016, 1:15:42 PM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2615 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}