<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : bizbahrain  [# publisher id = 1322]
//Title      : bizbahrain [ English ] 
//Created on : Mar 24, 2020, 12:06:51 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5966 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
