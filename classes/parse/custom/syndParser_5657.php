<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Nisr Publishing LLC  [# publisher id = 34]
//Title      : Friday [ English ] 
//Created on : Jun 24, 2019, 08:50:43 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5657 extends syndParseXmlContent {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $matchs = null;
    preg_match_all("/<gnarticle(.*?)<\/gnarticle>/is", $fileContents, $matchs);
    $matchs = $matchs[0];
    return $matchs;
  }

  public function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return trim($this->textFixation($this->getElementByName('Headline', $text)));
  }

  public function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = trim($this->getElementByName('PublishedDate', $text));
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $matche = null;
    if (preg_match('/<Byline>(.*?)<\/Byline>/is', $text, $matche)) {
      return trim($matche[1]);
    }
    return "";
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $matche = null;
    if (preg_match('/<Topics(.*?)<\/Topics>/is', $text, $matche)) {
      return trim($matche[1]);
    }
    return "";
  }

  public function getArticleOriginalId($params = array()) {
    $articleOriginalId = syndParseHelper::getImgElements($params['text'], 'gnarticle', 'id');
    $articleOriginalId = $articleOriginalId[0];

    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }

    return $this->title->getId() . '_' . sha1($articleOriginalId);
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = trim(preg_replace('!\s+!', ' ', $this->story));
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getImages(&$text) {
      $imagesArray = array();
      $this->story = $this->textFixation($this->getCData($this->getElementByName('body', $text)));
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return $imagesArray;
      
      $imgs = array();
      preg_match_all("/<img[^>]+\>/i", $this->story, $imgs);

      foreach ($imgs[0] as $img) {
          $this->addLog("getting article images");
          $image_caption = '';
          $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
          if (!empty($image_caption[0])) {
              $image_caption = $image_caption[0];
          } else {
        $image_caption = syndParseHelper::getImgElements($img, 'img', 'title');
        if (!empty($image_caption[0])) {
          $image_caption = $image_caption[0];
        } else {
          $image_caption = "";
        }
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePathold = $imageInfo[0];
      $imagePath = preg_replace("/\?(.*)/is", "", $imagePathold);
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        $this->addLog("No Path");
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePathold, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = sha1($imageUrl) . ".jpeg";
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );

      $ch = curl_init($imageUrl);
      curl_setopt_array($ch, $options);
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
