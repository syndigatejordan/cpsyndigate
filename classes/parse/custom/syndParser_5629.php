<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : openDemocracy Limited  [# publisher id = 1247]
//Title      : openDemocracy [ Arabic ] 
//Created on : May 21, 2019, 8:02:41 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5629 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}