<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bourse de Casablanca  [# publisher id = 595]
//Title      : Bourse de Casablanca [ French ] 
//Created on : Oct 16, 2019, 5:54:33 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1685 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}