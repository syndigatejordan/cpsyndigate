<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Digital Vibe Network  [# publisher id = 1848]
//Title      : The Digital Vibe Network [ English ] 
//Created on : Sep 20, 2022, 5:39:27 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12711 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}