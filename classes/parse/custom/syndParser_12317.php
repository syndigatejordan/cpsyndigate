<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : EV World  [# publisher id = 1714]
//Title      : EV World [ English ] 
//Created on : May 09, 2022, 2:33:44 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12317 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}