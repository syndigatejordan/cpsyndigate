<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : African Echo (Pty) Ltd.  [# publisher id = 967]
//Title      : Times of Swaziland [ English ] 
//Created on : May 22, 2016, 6:51:10 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2768 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}