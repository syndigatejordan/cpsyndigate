<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Reed Business Information Limited  [# publisher id =606 ]
// Titles    : FlightGlobal [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1748 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('fullArticle', $fileContents);
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = $this->getElementByName('bodySection', $text) ."<br><p>".$this->getElementByName('issueName', $text) ."</p>";
    $this->story = html_entity_decode($this->story);
    $this->story = strip_tags($this->story, '<p><br><strong><b><u><i><para>');
    $this->story = trim($this->story);
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  public function getHeadline(&$text) {
    $head = trim($this->getElementByName('titleBlock', $text));
    $head = trim($this->getElementByName('title', $head));
    $story = $this->getElementByName('bodySection', $text);
    $story = html_entity_decode($story);
    $story = strip_tags($story);
    $story = trim($story);
    if (empty($head)) {
      $head = substr($story, 0, 100);
    }
    return $head;
  }

  public function getArticleDate(&$text) {
    $date = trim($this->getElementByName('prism:releaseTime', $text));
    $date = explode('/', $date);
    $date = $date[2] . '-' . $date[1] . '-' . $date[0];
    return $date;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $author = trim($this->getElementByName('authorBlock', $text));
    $author = trim($this->textFixation($this->getElementByName('name', $author)));
    return trim($author);
  }

  protected function getOriginalCategory(&$text) {
    $this->addLog("getting article original category");
    $cat = trim($this->getElementByName('sectionName', $text));
    return $cat;
  }

}