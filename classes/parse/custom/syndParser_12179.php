<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Versia  [# publisher id = 1631]
//Title      : Versia [ Russian ] 
//Created on : Nov 03, 2021, 8:31:06 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12179 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}