<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IFIS Japan Limited  [# publisher id = 1798]
//Title      : IFIS Japan FTP [ Japanese ] 
//Created on : Jun 14, 2022, 12:41:59 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12640 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	} 
}