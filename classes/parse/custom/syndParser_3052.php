<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : DCube  [# publisher id = 1097]
//Title      : Apple World [ Arabic ] 
//Created on : Oct 07, 2021, 12:09:45 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3052 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}