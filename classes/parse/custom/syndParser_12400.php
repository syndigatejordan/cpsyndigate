<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Standard Group  [# publisher id = 1155]
//Title      : KTN News [ English ] 
//Created on : May 19, 2022, 7:55:16 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12400 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}