<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : NBK Capital  [# publisher id = 695]
//Title      : NBK Capital - The Insight [ English ] 
//Created on : Feb 02, 2016, 12:01:24 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2176 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}