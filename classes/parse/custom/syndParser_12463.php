<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : MM ACTIV  [# publisher id = 1766]
//Title      : NuFFooDS Spectrum [ English ] 
//Created on : Feb 20, 2022, 12:25:34 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12463 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}