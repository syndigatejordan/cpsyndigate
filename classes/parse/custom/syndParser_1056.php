<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Iran Cultural & Press Institute  
//Title     : Iran Daily [ English ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1056 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
