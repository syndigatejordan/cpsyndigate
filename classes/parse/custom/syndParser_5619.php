<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Democracy Now!  [# publisher id = 1245]
//Title      : Democracy Now! [ Spanish ] 
//Created on : May 21, 2019, 9:30:50 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5619 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}