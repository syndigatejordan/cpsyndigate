<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Welad ElBalad Media Services LTD  [# publisher id = 492]
//Title      : Heritage [ Arabic ] 
//Created on : Sep 08, 2021, 6:56:37 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3400 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}