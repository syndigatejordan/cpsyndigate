<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sarmady Communications  [# publisher id = 763]
//Title      : FilFan [ Arabic ] 
//Created on : Sep 09, 2020, 10:44:38 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2371 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}