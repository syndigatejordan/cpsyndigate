<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Tachad.com  [# publisher id = 1621]
//Title      : Tachad.com [ French ] 
//Created on : Sep 05, 2021, 11:53:18 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12165 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}