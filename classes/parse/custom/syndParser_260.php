<?php

	////////////////////////////////////////////////////////////////////////
	// Publisher : Independent Television News Limited  
	// Titles    : ITN- Independent [ English ]
	////////////////////////////////////////////////////////////////////////

	class syndParser_260 extends syndParseXmlContent
	{
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('en');
			$this->extensionFilter = 'xml';
		}

		protected function getRawArticles(&$fileContents) {			
			$this->addLog("getting articles raw text");
			return $this->getElementsByName('STORY', $fileContents);
		}

		protected function getStory(&$text) {			
			$this->addLog("getting article text");
			$story = $this->replaceAmp($this->textFixation($this->getElementByName('FULLTEXT', $text)));
			$story = str_replace('<PARA>', '<p>', $story);
			$story = str_replace('</PARA>', '</p>', $story);
			return $story;
		}

		protected function getHeadline(&$text) {
			$this->addLog("getting article headline");
			$head = $this->textFixation($this->getElementByName('TITLE', $text));
			return $head;
		}
		
		/*
		protected function getArticleDate(&$text)
		{			
			$this->addLog("getting article date");
			
			$date = $this->getElementByName('date', $text);
			$date = explode(' ', $date);
			return $date[0];
		}
		*/
		
		
		protected function getAbstract(&$text) {
			$this->addLog("getting article summary");
			return $this->textFixation($this->getElementByName('TEASER', $text));
		}
		
		protected function getImages(&$text) {			
			$image  = syndParseHelper::getImgElements($text, 'IMAGE');
			$images	= $this->getAndCopyImagesFromArray(array($this->currentDir . $image[0]));
			return $images;
		}
				
	}
