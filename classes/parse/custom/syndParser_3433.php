<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Media OutReach Newswire  [# publisher id = 1172]
//Title      : Media OutReach Newswire [ English ] 
//Created on : Sep 14, 2021, 8:28:06 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3433 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}