<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Federation of Chambers of Commerce and Industry of Sri Lanka.  [# publisher id = 850]
//Title      : Federation of Chambers of Commerce and Industry of Sri Lanka (FCCISL) - News [ English ] 
//Created on : Feb 09, 2016, 12:04:28 PM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2607 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}