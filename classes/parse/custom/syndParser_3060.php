<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Aqar Talk  [# publisher id = 1099]
//Title      : Aqar Talk [ Arabic ] 
//Created on : Oct 07, 2021, 12:12:57 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3060 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}