<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Philippine Chamber of Commerce and Industries  [# publisher id = 847]
//Title      : Philippine Chamber of Commerce and Industry (PCCI) - News [ English ] 
//Created on : Feb 15, 2016, 8:51:52 AM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2604 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}