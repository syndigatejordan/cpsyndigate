<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Eco-Business  [# publisher id = 1748]
//Title      : Eco-Business [ English ] 
//Created on : Jan 24, 2022, 11:54:56 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12355 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}