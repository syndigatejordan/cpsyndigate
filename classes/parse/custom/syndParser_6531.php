<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Afghan Voice Agency (AVA)  [# publisher id = 1401]
//Title      : Afghan Voice Agency (AVA) [ Pashto ] 
//Created on : Dec 08, 2020, 10:11:41 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6531 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ps'); 
	} 
}