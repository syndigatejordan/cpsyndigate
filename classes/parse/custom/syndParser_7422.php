<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Codesiconsulting S.A.  [# publisher id = 1484]
//Title      : El Jornal [ Spanish ] 
//Created on : Sep 19, 2021, 10:20:14 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7422 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}