<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : REFINITIV LIMTED  [# publisher id = 1597]
//Title      : ZAWYA by Refinitiv [ Arabic ] 
//Created on : Nov 30, 2021, 7:33:53 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11924 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}