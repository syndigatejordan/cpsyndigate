<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : World of Media JLT  [# publisher id = 434]
//Title      : MediAvataarME.com [ English ] 
//Created on : Sep 23, 2021, 7:45:23 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1381 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}