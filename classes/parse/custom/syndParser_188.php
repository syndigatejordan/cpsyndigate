<?php

////////////////////////////////////////////////////////////////////////
// Publisher : Société générale S.A.L 
// Titles    : L' Orient-Le Jour
////////////////////////////////////////////////////////////////////////

class syndParser_188 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('fr');
    //$this->charEncoding = 'ISO-8859-1';					
  }

  protected function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) width=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) height=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) align=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (!empty($this->story)) {
      $this->story = "<p>" . $this->story;
      $this->story = str_replace(". ", ".</p> <p>", $this->story);
      $this->story = str_replace("<p></p>", "", $this->story);
      $this->story = str_replace("<p> </p>", "", $this->story);
    }
    if (empty($this->story)) {
      return '';
    }
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    return $this->story;
  }

}
