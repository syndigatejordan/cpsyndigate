<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bloomberg L.P.  [# publisher id = 1202]
//Title      : Bloomberg Podcasts [ English ] 
//Created on : Mar 26, 2020, 8:22:06 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5995 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}