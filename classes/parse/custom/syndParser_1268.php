<?php

//////////////////////////////////////////////////////////////////////////////
// Publisher: FRANCE 24 / AEF  [# publisher id =383 ]
// Titles   : FRANCE 24 [French]
//////////////////////////////////////////////////////////////////////////////

class syndParser_1268 extends syndParseRss {

  private $titleInfoURL;
  private $titleInfoName;
  private $titleInfoLink;

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('fr');
  }

  public function getRawArticles(&$fileContents) {
    $this->addLog("getting articles info Logo & URL to add them to body in RawArticles function");

    $titleInfo = $this->getElementByName('image', $fileContents);
    $this->titleInfoURL = trim($this->textFixation($this->getElementByName('url', $titleInfo)));
    $this->titleInfoName = trim($this->textFixation($this->getElementByName('title', $titleInfo)));
    $this->titleInfoLink = trim($this->textFixation($this->getElementByName('link', $titleInfo)));

    return parent::getRawArticles($fileContents);
  }

  public function getStory(&$text) {
    $body = parent::getStory($text);
    $this->addLog('Adding Logo And links to the end of article body');
    $story = preg_replace('/<img[^>]+\>/i', '', $body);
    $story = preg_replace('!\s+!', ' ', $story);
    return $story;
  }

}
