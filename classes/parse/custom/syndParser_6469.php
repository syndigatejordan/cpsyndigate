<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Liberia News Agency (LINA)  [# publisher id = 1376]
//Title      : Liberia News Agency (LINA) [ English ] 
//Created on : Sep 19, 2021, 8:54:58 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6469 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}