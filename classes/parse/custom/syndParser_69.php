<?php

//////////////////////////////////////////////////////////////////////////////
// Publisher: AKIpress News Agency 
// Titles   : AKIpress News Agency English
//////////////////////////////////////////////////////////////////////////////

class syndParser_69 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->charEncoding = 'WINDOWS-1251';
  }

  public function getStory(&$text) {
    $this->addLog('getting article story');
    $story = $this->textFixation($this->getCData($this->getElementByName('description', $text)));
    if (!empty($story)) {
      $story = '<p>' . $story . '</p>';
      $story = preg_replace("/\r\n|\r|\n/", "</p><p>", $story);
    }
    return $story;
  }

}

?>