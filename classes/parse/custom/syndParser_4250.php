<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Lobster IT Limited  [# publisher id = 1199]
//Title      : LOBSTER - UGC Images [ English ] 
//Created on : Feb 21, 2021, 8:34:16 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_4250 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}