<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Creative Publishing  [# publisher id = 719]
//Title      : Alkhabar Aleqtesady [ Arabic ] 
//Created on : Sep 23, 2021, 7:50:12 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2269 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}