<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : SweetCrude Limited  [publisher id =480 ] 
//Title     : SweetCrude Reports [ English ]
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1450 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

}
