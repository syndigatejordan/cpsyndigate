<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : INFOPRO Digital  [# publisher id = 1716]
//Title      : L'Automobile & L'Entreprise [ French ] 
//Created on : Jan 11, 2022, 12:19:34 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12320 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}