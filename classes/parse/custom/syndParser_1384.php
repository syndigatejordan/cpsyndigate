<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Youssef Rakha  [# publisher id = 436]
//Title      : The Sultan’s Seal [ Arabic ] 
//Created on : Jul 29, 2021, 10:06:01 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1384 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}