<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : All-China Federation of Industry and Commerce  [# publisher id = 857]
//Title      : All-China Federation of Industry and Commerce (ACFIC) - News [ English ] 
//Created on : Feb 09, 2016, 12:05:25 PM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2617 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}