<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Syrian Arab News Agency (SANA)  [# publisher id = 134]
//Title      : Syrian Arab News Agency (SANA) [ Spanish ] 
//Created on : Oct 15, 2020, 12:18:35 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6656 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}