<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Seeking Alpha Ltd.  [# publisher id = 1675]
//Title      : Seeking Alpha - News [ English ] 
//Created on : Oct 26, 2021, 5:58:07 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12240 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}