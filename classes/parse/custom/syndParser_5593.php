<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Stichting Global Voices  [# publisher id = 1235]
//Title      : Global Voices [ Swedish ] 
//Created on : Feb 22, 2021, 8:00:54 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5593 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('sv'); 
	} 
}