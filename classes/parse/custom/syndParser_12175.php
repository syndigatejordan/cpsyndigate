<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Digital News Asia  [# publisher id = 1627]
//Title      : Digital News Asia [ English ] 
//Created on : Dec 14, 2021, 7:00:47 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12175 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}