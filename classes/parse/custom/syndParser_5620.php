<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Tasnim News Agency  [# publisher id = 1246]
//Title      : Tasnim News Agency [ English ] 
//Created on : May 22, 2019, 6:47:57 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5620 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}