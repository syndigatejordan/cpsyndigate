<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Nation Media Group Limited  [# publisher id = 338]
//Title      : Sunday Nation [ English ] 
//Created on : Jul 29, 2021, 9:52:49 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1383 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}