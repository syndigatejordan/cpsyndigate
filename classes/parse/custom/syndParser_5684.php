<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Vacca Foeda Media  [# publisher id = 1255]
//Title      : Today I Found Out [ English ] 
//Created on : Oct 05, 2021, 7:27:23 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5684 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	}
    protected function getVideos(&$text) {
        $this->addLog("Getting videos");
        $fulltext = $this->textFixation($this->getElementByName('fulltext', $text));
        $videos = array();
        $matches = null;
        preg_match_all('/<iframe(.*?)<\/iframe>/is', $fulltext, $matches);
        foreach ($matches[0] as $matche) {
            $video = array();
            $videoname = syndParseHelper::getImgElements($matche, 'iframe', 'src');

            $video['video_name'] = $videoname[0];
            $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
            $video['original_name'] = $videoName;
            $video['video_caption'] = $videoName;
            $date = $this->getElementByName('date', $text);
            $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
            $video['mime_type'] = "";
            $videos[] = $video;
        }
        return $videos;
    } 
}
