<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Groupe BUSINESS 221  [# publisher id = 1745]
//Title      : Business 221 [ French ] 
//Created on : Jan 19, 2022, 1:15:18 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12350 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}