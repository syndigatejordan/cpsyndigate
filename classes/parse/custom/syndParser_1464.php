<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : FocusAsia Media Ltd.  [# publisher id =487 ] 
// Titles    : GCTL8.com [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1464 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");

    $headline = $this->getCData($this->getElementByName('title', $text));
    return $headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    $date = preg_replace('/ \+(.*)/is', '', $date);
    $date = date('Y-m-d', strtotime($date));
    return $date;
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $body = $this->textFixation($this->getCData($this->getElementByName('description', $text)));
    $body = strip_tags($body, '<p><br><strong><b><u><i>');
    return $body;
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getElementByName('category', $text);
    return $cats;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->getElementByName('author', $text);
  }

}
