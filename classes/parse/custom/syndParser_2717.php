<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dar Al Sharq Press, Printing & Distribution  [# publisher id = 923]
//Title      : Al Sharq [ Arabic ] 
//Created on : May 19, 2016, 7:23:05 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2717 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}