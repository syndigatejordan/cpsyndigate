<?php
///////////////////////////////////////////////////////////////////////////
// Publisher : BANG Media International
// Titles    : BANG ShowBiz
///////////////////////////////////////////////////////////////////////////
class syndParser_1022 extends syndParseXmlContent
{
    public function customInit () {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('es');
        $this->extensionFilter = 'xml';
    }
    
    protected function getRawArticles (&$fileContents) {
        //get articles
        $this->addLog("getting articles raw text");
        
        $newsIdentifier = $this->getElementsByName('NewsIdentifier', $fileContents);
        $articles =  $this->getElementsByName('NewsComponent', $fileContents);
        
        foreach ($articles as $index => &$article) {
            $article .= $newsIdentifier[$index];
        }
        
        return $articles;
    }
            
    protected function getHeadline (&$text) {        
        $this->addLog("getting article headline");
        $headline = $this->getElementByName('HeadLine', $text);
        return $this->textFixation($headline);
    }
    
    protected function getAbstract (&$text) {                
        $this->addLog("getting article abstract");
        $abstract = $this->getCData($this->getElementByName('NewsLineText', $text));        
        return $this->textFixation($abstract);
    }
    
    
    protected function getAuthor (&$text) {
        $this->addLog("getting article author");
        $author = $this->getCData($this->getElementByName('ProviderId', $text));                 
        return $this->textFixation($author);
    }
    
    protected function getArticleDate (&$text) {       
        $this->addLog("getting article date");
        $date = $this->getElementByName('DateId', $text);        
        $date =  $this->dateFormater(substr($date, 0, 10));
        return $date;
    }
    
    protected function getOriginalCategory (&$text) {               
        $this->addLog("getting article category");
        $parts = explode('.xml', basename($this->currentlyParsedFile));
        $cat   = str_replace('_', ' ', $parts[0]);
        return $cat;
    }
    
    protected function getStory (&$text) {        
        $this->addLog("getting article text");
        $story = $this->getCData($this->getElementByName('body.content', $text));
        $story = html_entity_decode($story);        
        return $this->textFixation($story);        
    }
    
    public function getArticleOriginalId($params = array()) {			    	
        
        $articleOriginalId = $this->getElementByName('NewsItemId', $params['text']);	
        
	if(!$articleOriginalId ) {
            return parent::getArticleOriginalId($params);
	}			
		
	return $this->title->getId() . '_' . $articleOriginalId;
    }
        
    protected  function getImages(&$text) {
   
        $this->addLog("getting article images");
                
        $parts = explode('</ContentItem>', $text);
        if(count($parts) != 3) {
            return array();
        }
        
        $matches = array();        
        preg_match_all('/http:\/\/.*?"/i', $parts[1], $matches);
        if(empty($matches)) {
            return array();
        }
        $matches = explode('"', $matches[0][0]);
        
        $imagePath   = $matches[0];
	$imagesArray = array();
        
        $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
		
        if(!$copiedImage) {
            return array();
        }
        
        //getting the name of the image 
        $matches = array();        
        preg_match_all('/<Property FormalName="caption" value=".*?".*?>/i', $parts[1], $matches);
        $imgCap = explode('=', $matches[0][0]);
        $imgCap = str_replace(' />', '', $imgCap[2]);
        $imgCap = str_replace('"', '', $imgCap);         
        $imgCap = str_replace(array('?', ','), '', $imgCap);        
        if(strlen($imgCap) > 30) {
            $imgCap = substr($imgCap, 0, 30);            
        }
        
        $newName = str_replace(' ', '_', strtolower($imgCap));
        $newImage= dirname($copiedImage) . '/' . $newName . '.jpg';
        
        copy($copiedImage, $newImage);
        
            
        $images = $this->getAndCopyImagesFromArray(array($newImage));                
        $images[0]['image_caption'] = $imgCap;
                
        array_push($imagesArray, $images[0]);	
        return $imagesArray;
    }
    
    
    /*
    public function afterParse($params = array())  {
        
        //parsed articles 
        $articles = $params['articles'];
        
        $checked_sim = array();
        foreach ($articles as $article) {
            if(empty($checked_sim)) {
                $checked_sim[] = $article;
            } else {
                foreach ($checked_sim as $sim_article) {                    
                    
                    $sim = 0;
                    similar_text($article['story'], $sim_article['story'], $sim );                    
                    //echo $sim . PHP_EOL;                   
                    
                    if((int)$sim > 95) {
                      //unlink the artcire ;
                    }
                }
                $checked_sim[] =  $article;
            }
        }
        parent::afterParse();
    }
     * 
     */

     
}

