<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bourse de Casablanca  [# publisher id = 595]
//Title      : Bourse de Casablanca [ English ] 
//Created on : Oct 16, 2019, 5:53:23 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1684 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}