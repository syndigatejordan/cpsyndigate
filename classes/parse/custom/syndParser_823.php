<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sabq Group  [# publisher id = 236]
//Title      : Sabq News [ Arabic ] 
//Created on : Jan 20, 2016, 5:03:41 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////


class syndParser_823 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getImages(&$text) {
      $this->story = $this->getCData($this->getElementByName('fulltext', $text));
      $this->story = html_entity_decode($this->story, ENT_NOQUOTES, 'UTF-8');
      $this->story = $this->textFixation($this->story);
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();

      $this->imagesArray = array();
      preg_match_all("/<img[^>]+\>/i", $this->story, $imgs);

      foreach ($imgs[0] as $img) {
          $this->addLog("getting article images");
          $image_caption = '';
          $image_caption = explode('alt="', $img);
          if (!empty($image_caption[1])) {
              $image_caption = explode('"', $image_caption[1]);
              $image_caption = $image_caption[0];
      } else {
        $image_caption = "";
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = str_replace('%20', ' ', $image_caption);
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['is_headline'] = false;
      $this->story = str_replace($image_caption, "", $this->story);
      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = basename($imageUrl);
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $curl_handle = curl_init();
      curl_setopt($curl_handle, CURLOPT_URL, $imageUrl);
      curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl_handle, CURLOPT_USERAGENT, 'php');
      $imgContent = curl_exec($curl_handle);
      curl_close($curl_handle);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

  /*  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    preg_match_all('#<embed(.*)/>#si', $this->story, $cvideo);
    $videos = array();
    foreach ($cvideo[0] as $vsrc) {
    preg_match('/src="(.*?)"/is', $vsrc, $videoname);
    $link = $videoname[1];
    $videoName = $this->getElementByName('title', $text);

    $video['video_name'] = $link;
    $video['original_name'] = $videoName;
    $video['video_caption'] = $videoName;
    $date = $this->getElementByName('date', $text);
    $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
    $videos[] = $video;
    $this->story = str_replace($vsrc, "", $this->story);
    }
    return $videos;
    }
   */
}
