<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : RASANAH - International Institute for Iranian Studies  [# publisher id = 1264]
//Title      : RASANAH - Annual Strategic Reports [ Arabic ] 
//Created on : Sep 01, 2020, 8:48:45 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5808 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}