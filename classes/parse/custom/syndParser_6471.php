<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Maghreb Press Agency  [# publisher id = 1377]
//Title      : Maghreb Press Agency [ English ] 
//Created on : Oct 06, 2020, 1:07:32 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6471 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}