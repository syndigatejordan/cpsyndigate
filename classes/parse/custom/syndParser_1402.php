<?php
//////////////////////////////////////////////////////////////////////////////
// Publisher: The New Straits Times Press (M) Berhad  [# publisher id =445 ]
// Titles   : Business Times [ English ]
//////////////////////////////////////////////////////////////////////////////

class syndParser_1402 extends syndParseXmlContent {

	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('en');
	}

	public function getRawArticles(&$fileContents) {
		$this -> addLog("getting articles raw element");

		return $this -> getElementsByName('dcdossier', $fileContents);
	}

	protected function getHeadline(&$text) {
		$this -> addLog("getting article headline");

		return $this -> textFixation($this -> getElementByName('HL', $text));
	}

	protected function getAuthor(&$text) {
		$this -> addLog("getting article author");

		return $this -> textFixation($this -> getElementByName('BY', $text));
	}

	protected function getStory(&$text) {
		$this -> addLog("getting article text");

		$story = $this -> textFixation($this -> getElementByName('body', $text));
		$story = preg_replace('/<DCID>.*?<\/DCID>/i', '', $story);
		$story = preg_replace('/<HL>.*?<\/HL>/i', '', $story);
		$story = preg_replace('/<TX>/i', '<p>', $story);
		$story = preg_replace('/<\/TX>/i', '</p>', $story);

		return $story;
	}

	protected function getArticleDate(&$text) {
		$this -> addLog("getting article date");

		$date = $this -> textFixation($this -> getElementByName('DA', $text));
		$year = substr($date, 0, 4);
		$month = substr($date, 4, 2);
		$day = substr($date, 6, 2);

		$date = $year . '-' . $month . '-' . $day;
		return $date;
	}

}