<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Togo Top News  [# publisher id = 1803]
//Title      : Togo Top News [ French ] 
//Created on : May 17, 2022, 10:03:32 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12622 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}