<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Oman Establishment for Press, Publication and Advertising (OEPPA)  [# publisher id = 1071]
//Title      : Oman Daily Observer [ English ] 
//Created on : Apr 16, 2020, 6:07:20 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3055 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}