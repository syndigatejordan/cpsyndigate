<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Discovery, Inc.  [# publisher id = 1229]
//Title      : Eurosport [ English ] 
//Created on : Apr 13, 2019, 5:41:47 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5527 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}