<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Polish ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_586 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority' 			=> 'Zamawiającego', 
									'Nature of Contract'			=> 'Rodzaj umowy', 
									'Regulation of Procurement' 	=> 'Rozporządzenie zamówień',
									'Type of bid required'			=> 'Rodzaj oferty wymagane', 
									'Award Criteria'				=>	'Kryteria przyznawania', 
									'Original CPV'					=> 	'Original CPV', 
									'Country'						=>	'Kraj', 
									'Document Id' 					=>	'Dokumentu tożsamości', 
									'Type of Document'				=>	'Rodzaj dokumentu', 
									'Procedure'						=>	'Procedura', 
									'Original Language'				=>	'Oryginalny język', 
									'Current Language'				=> 	'Bieżący język');	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('pl');
		}
	}