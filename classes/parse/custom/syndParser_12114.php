<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : South Sudan News Now  [# publisher id = 1611]
//Title      : South Sudan News Now [ English ] 
//Created on : Aug 09, 2021, 8:36:20 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12114 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}