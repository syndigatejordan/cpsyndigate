<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : coindesk  [# publisher id = 1838]
//Title      : CoinDesk Japan [ Japanese ] 
//Created on : Aug 10, 2022, 12:24:20 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12697 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	} 
}