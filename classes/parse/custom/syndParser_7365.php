<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Emergence Togo  [# publisher id = 1470]
//Title      : Emergence Togo [ French ] 
//Created on : Oct 05, 2021, 8:03:05 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7365 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}