<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Searchlight Communications Incorporated  [# publisher id = 474]
//Title      : The New Dawn [ French ] 
//Created on : Oct 25, 2020, 1:48:24 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1444 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}