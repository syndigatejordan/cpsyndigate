<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Human Rights Watch  [# publisher id = 1302]
//Title      : Human Rights Watch - News [ Arabic ] 
//Created on : Feb 29, 2020, 1:54:25 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5935 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }
}
