<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Al-Ahram Publishing House  
//Title     : Al-Ahram Sports [ Arabic ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_909 extends syndParseRss {

  public $source;
  public $Third_patty = array("AFP", "DPA", "Reuters", "وكالات", "وكالات الأنباء", "لألمانية", "أ ف ب", "أ ش أ", "أ.ف.ب", "أ.ش.أ", "ا ش ا", "ا.ف.ب", "الفرنسية", "الألمانية", "الالمانية", "د.ب.ا", "رويترز", "ا.ش.ا ");

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('item', $fileContents);
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->getElementByName('title', $text);
    return $this->textFixation($headline);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubdate', $text);
    $date = explode('CMT', $date);
    $date = $date[0];
    return date('Y-m-d', strtotime($date));
  }

  protected function getStory(&$text) {
    $this->addLog("getting article story");
    if (in_array($this->source, $this->Third_patty)) {
      return"";
    }
    $story = $this->getCData($this->getElementByName('body', $text));
    $story = $this->textFixation($story);
    return $story;
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getElementsByName('category', $text);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = $this->textFixation($this->getCData($cat));
      }
    }
    return implode(', ', $originalCats);
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $this->source = trim($this->textFixation($this->getElementByName('source', $text)));
    return $this->source;
  }

  protected function getImages(&$text) {
    $this->addLog("getting article images");
    $imagesArray = array();
    if (in_array($this->source, $this->Third_patty)) {
      return $imagesArray;
    }
    $imagePath = $this->getElementByName('bigimage', $text);
    $imagePath = str_replace(' ', '%20', $imagePath);
    if (!$imagePath) {
      return;
    }
    if ($image = $this->checkImageifCached($imagePath)) {
      // Image already parsed..
      return;
    }
    $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

    if (!$copiedImage) {
      echo "no pahr";
      return;
    }
    $images = $this->getAndCopyImagesFromArray(array($copiedImage));
    if (!empty($image_caption)) {
      $images[0]['image_caption'] = $image_caption;
    }
    $name_image = explode('/images/', $copiedImage);
    if ($images[0]['image_caption'] == $name_image[1]) {
      $images[0]['image_caption'] = '';
    }
    $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
    $images[0]['is_headline'] = false;
    array_push($imagesArray, $images[0]);
    return $imagesArray;
  }

}
