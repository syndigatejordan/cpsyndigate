<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Philippine Information Agency  [# publisher id = 1427]
//Title      : Philippine Information Agency [ Tagalog ] 
//Created on : Oct 13, 2020, 11:42:04 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6647 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('tl'); 
	} 
}