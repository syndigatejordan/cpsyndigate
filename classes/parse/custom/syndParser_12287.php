<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Cristiana Invest Press SRL  [# publisher id = 1692]
//Title      : Nine O'Clock [ English ] 
//Created on : Dec 13, 2021, 8:03:19 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12287 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}