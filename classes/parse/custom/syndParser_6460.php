<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Algeria Press Service  [# publisher id = 136]
//Title      : Algeria Press Service [ Tamazight Arabic ] 
//Created on : Oct 05, 2020, 10:38:29 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6460 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zga'); 
	} 
}