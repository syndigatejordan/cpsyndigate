<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Welad ElBalad Media Services LTD  [# publisher id = 492]
//Title      : Sports [ Arabic ] 
//Created on : Sep 08, 2021, 6:56:38 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3401 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}