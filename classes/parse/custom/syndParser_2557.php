<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Spanish Chamber of Commerce in South Africa (SCCA) - News  [# publisher id = 833]
//Title      : Spanish Chamber of Commerce in South Africa (SCCA) - News [ English ] 
//Created on : Feb 02, 2016, 12:12:54 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2557 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}