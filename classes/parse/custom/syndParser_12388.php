<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Zocalo  [# publisher id = 1757]
//Title      : Zocalo [ Spanish ] 
//Created on : Feb 08, 2022, 2:37:26 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12388 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}