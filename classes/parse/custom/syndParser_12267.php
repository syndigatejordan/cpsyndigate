<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CryptoNews  [# publisher id = 1685]
//Title      : CryptoNews [ French ] 
//Created on : Nov 23, 2021, 10:07:16 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12267 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}