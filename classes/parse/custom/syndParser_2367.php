<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Afri-Can Publications  [# publisher id = 762]
//Title      : The Patriotic Vanguard [ English ] 
//Created on : Sep 19, 2021, 7:03:52 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2367 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}