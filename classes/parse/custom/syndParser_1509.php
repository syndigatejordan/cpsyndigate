<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Mobile88.com  [# publisher id =505 ]
// Titles    : Mobile88.com [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1509 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('item', $fileContents);
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->getCData($this->getElementByName('title', $text));
    return $this->textFixation($headline);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    $date = preg_replace('/ \+(.*)/is', '', $date);
    return date('Y-m-d', strtotime($date));
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = $this->getCData($this->getElementByName('content:encoded', $text));
    $story = preg_replace("/<style\\b[^>]*>(.*?)<\\/style>/s", "", $story);
    preg_match_all('#<iframe[^>]+>#i', $story, $videos);
    $videolink = "";
    foreach ($videos[0] as $video) {
      $video = explode('src="', $video);
      $video = $video[1];
      $video = explode('"', $video);
      $video = $video[0];
      $videolink .= '<p><a href="' . $video . '">' . $video . '</a></p>';
    }
    $story = $story . $videolink;
    $story = preg_replace('/<img[^>]+\>/i', '', $story);
    $story = preg_replace('!\s+!', ' ', $story);
    return $this->textFixation($story);
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = "Mobile phone news, Telco news , Mobile Technology News , Tablet news";
    return $cats;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getCData($this->getElementByName('dc:creator', $text)));
  }

  protected function getImages(&$text) {
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      return array();

    $this->imagesArray = array();
    $story = $this->textFixation($this->getCData($this->getElementByName('content:encoded', $text)));
    preg_match_all(
            "/<img[^>]+\>/i", $story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $text = str_replace($img, $new_img, $text);
      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

}
