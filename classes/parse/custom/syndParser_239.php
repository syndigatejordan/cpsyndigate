<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : ANI Media Pvt Ltd 
// Titles    : Asian News International (ANI)
///////////////////////////////////////////////////////////////////////////

class syndParser_239 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    //$this->charEncoding = 'ISO-8859-1';
  }

  protected function getRawArticles(&$fileContents) {
    $this->loadCurrentDirectory();

    $this->addLog("getting raw articles text");
    return $this->getElementsByName('article', $fileContents);
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    return $this->textFixation($this->getCData($this->getElementByName('content', $text)));
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return $this->textFixation($this->getElementByName('headline', $text));
  }

  /*
    protected function getIptcId(&$text)
    {
    }
   */

  protected function getArticleDate(&$text) {

    $this->addLog("getting articles date");
    $date = $this->getElementByName('date', $text);
    $date = explode("/", $date);
    $date = "{$date[2]}-{$date[1]}-{$date[0]}";
    if (strtotime($date)) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate(&$text);
  }

  protected function getAbstract(&$text) {
    $this->addLog("getting abstract");
    return $this->textFixation($this->getElementByName('summary', $text));
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getElementByName('author', $text));
  }

  protected function getOriginalCategory(&$text) {
    $this->addLog("getting Original Category");
    $cat = str_replace("\r\n", '', $this->textFixation($this->getElementByName('cats', $text)));
    $subCat = str_replace("\r\n", '', $this->textFixation($this->getElementByName('subcats', $text)));

    if ($subCat) {
      return $cat . '/' . $subCat;
    } else {
      return $cat;
    }
  }

}
