<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : SIERRA EXPRESS MEDIA  [# publisher id = 1149]
//Title      : Sierra Express Media [ English ] 
//Created on : Sep 04, 2017, 6:00:56 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3145 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}