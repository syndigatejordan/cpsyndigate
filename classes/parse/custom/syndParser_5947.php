<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Gottscho-Schleisner Collection  [# publisher id = 1313]
//Title      : Gottscho-Schleisner Collection [ English ] 
//Created on : Mar 03, 2020, 7:48:38 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5947 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
