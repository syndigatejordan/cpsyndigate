<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Infos Plus Gabon  [# publisher id = 1160]
//Title      : Infos Plus Gabon [ Portuguese ] 
//Created on : Sep 23, 2021, 7:55:53 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3174 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}