<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Hungarian ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_583 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority' 			=>	'Ajánlatkérő hatóság', 
										'Nature of Contract'			=>	'Jellege szerződés', 
										'Regulation of Procurement' 	=>	'Rendelete a Közbeszerzési', 
										'Type of bid required'			=> 	'Típusa szükséges ajánlatot', 
										'Award Criteria'				=>	'Díj kritériumai', 
										'Original CPV'					=> 	'Eredeti CPV', 
										'Country'						=>	'Ország', 
										'Document Id' 					=>	'Dokumentum Id', 
										'Type of Document'				=>	'Dokumentum típusa', 
										'Procedure'						=>	'Eljárás', 
										'Original Language'				=>	'Eredeti nyelv', 
										'Current Language'				=> 	'Aktuális nyelv');	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('hu');
		}
	}