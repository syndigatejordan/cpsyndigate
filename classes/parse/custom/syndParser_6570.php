<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Iranian Students News Agency (ISNA)  [# publisher id = 147]
//Title      : Iranian Students News Agency (ISNA) [ Persian ] 
//Created on : Oct 15, 2020, 8:51:24 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6570 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fa'); 
	} 
}