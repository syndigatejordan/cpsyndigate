<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Islamic Ideology Dissemination Organization (IIDO)  [# publisher id = 149]
//Title      : Mehr News Agency (MNA) [ Urdu ] 
//Created on : Oct 14, 2020, 11:15:05 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6588 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ur'); 
	} 
}