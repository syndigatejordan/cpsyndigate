<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Caixin Global Limited  [# publisher id = 1569]
//Title      : Caixin Global [ English ] 
//Created on : Apr 26, 2021, 11:56:15 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10818 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}