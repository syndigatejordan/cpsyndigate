<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Maghreb Press Agency  [# publisher id = 1377]
//Title      : Maghreb Press Agency [ Spanish ] 
//Created on : Oct 06, 2020, 1:22:12 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6474 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}