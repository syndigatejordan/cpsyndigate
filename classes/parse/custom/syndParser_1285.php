<?php

////////////////////////////////////////////////////////////////////////
// Publisher : Carol Fleming  [# publisher id =389 ]
// Title     : American Bedu [English]
////////////////////////////////////////////////////////////////////////

class syndParser_1285 extends syndParseRss {

  //private $headline;
  //private $search;

  public function customInit() {
    parent::customInit();
    $this->search = null;
    $this->headline = null;
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  /* protected function getAbstract(&$text) {
    $this -> addLog("getting article summary");

    $desc = $this -> textFixation($this -> getCData($this -> getElementByName('description', $text)));
    $abstract = preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i", '<$1$2>', $desc);
    $abstract = strip_tags($abstract, '<p><b><i><strong>');

    $this -> search = '<p>The post ' . $this -> headline . ' appeared first on Big Project Middle East.</p>';
    if (strpos($abstract, $this -> search)) {
    $abstract = str_replace($this -> search, '', $abstract);
    }
    return $abstract;
    } */

  /* public function getOriginalCategory(&$text) {
    $this -> addLog('getting article category');
    $cats = $this -> getElementsByName('category', $text);
    $originalCats = array();

    if (!empty($cats)) {
    foreach ($cats as $cat) {
    $originalCats[] = $this -> textFixation($this -> getCData($cat));
    }
    }
    return implode(', ', $originalCats);
    } */

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return $this->textFixation($this->getElementByName('title', $text));
  }

  public function getAuthor(&$text) {
    $this->addLog('getting article author');
    return $this->textFixation($this->getElementByName('dc:creator', $text)) . PHP_EOL;
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");

    $story = $this->textFixation($this->getCData($this->getElementByName('content:encoded', $text)));
    $story = strip_tags($story, '<p><b><strong><i><em><a><table><tbody><tr><td><th><u>');
    //$story = preg_replace('/<p>={2,}<\/p>/i', '', $story);
    //$story = preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i", '<$1$2>', $story);

    /* if (strpos($story, $this -> search)) {
      $story = str_replace($this -> search, '', $story);
      } */
    return $story;
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $imgs = array();

    //$regExp = '/<media:content url="[^>]+jpg" medium="image">/i';
    $regExp = '<media:content[\s]+.*url[\s]*=[\s]*([\"]([^>\"]*)[\"]|' . "[\\']([^>\\']*)[\\']|([^>\\s]*))([\\s]|[^>])*[\/]?>";
    preg_match_all("/$regExp/i", $text, $imgs);
    $imgs = $imgs[0];


    $counter = 0;
    foreach ($imgs as $img) {


      $this->addLog("getting article images");
      $start_from = strpos($img, 'url') + 5;

      $imagePath = substr($img, $start_from, strpos($img, '.jpg') + 4 - $start_from);

      if (false === strpos($img, ".jpg") /* || false === strpos($img, ".jpeg") || false === strpos($img, ".gif") */) {
        continue;
      }

      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if ($counter > 0) {
        $images[0]['is_headline'] = 0;
      }


      array_push($imagesArray, $images[0]);
      $counter++;
    }
    return $imagesArray;
  }

}