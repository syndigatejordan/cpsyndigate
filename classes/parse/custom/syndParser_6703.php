<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Belarusian Telegraph Agency (BelTA)  [# publisher id = 1434]
//Title      : Belarusian Telegraph Agency (BelTA) [ Spanish ] 
//Created on : Oct 22, 2020, 7:19:49 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6703 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}