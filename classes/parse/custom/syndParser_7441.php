<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Germán Alvarez Comunicación Corporativa  [# publisher id = 1492]
//Title      : Region Mar del Plata [ Spanish ] 
//Created on : Sep 19, 2021, 10:20:34 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7441 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}