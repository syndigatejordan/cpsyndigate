<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ACN Newswire Co Ltd  [# publisher id = 1174]
//Title      : JCN Newswire [ English ] 
//Created on : Sep 23, 2021, 7:56:32 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3439 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}