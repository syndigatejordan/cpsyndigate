<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : AD Communications  [# publisher id = 1758]
//Title      : AM Querétaro [ Spanish ] 
//Created on : Feb 08, 2022, 2:46:54 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12389 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}