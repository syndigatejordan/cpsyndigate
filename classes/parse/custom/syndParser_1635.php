<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Moby Group  [# publisher id =565 ] 
//Title      : TOLOnews [ English ] 
//Created on : May 03, 2017, 05:30:28 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_1635 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
