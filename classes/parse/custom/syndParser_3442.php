<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ACN Newswire Co Ltd  [# publisher id = 1174]
//Title      : ACN Newswire [ Korean ] 
//Created on : Oct 19, 2021, 6:59:12 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3442 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ko'); 
	} 
}