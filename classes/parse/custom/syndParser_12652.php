<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Boston Globe Media Partners, LLC  [# publisher id = 1817]
//Title      : The Boston Globe [ English ] 
//Created on : Jun 20, 2022, 9:43:13 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12652 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}