<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dubai Corporation of Tourism & Commerce Marketing (DCTCM)  [# publisher id = 1175]
//Title      : Dubai Tourism [ Arabic ] 
//Created on : Feb 18, 2018, 9:33:55 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3445 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
