<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CryptoNews  [# publisher id = 1685]
//Title      : CryptoNews [ Italian ] 
//Created on : Nov 23, 2021, 10:27:24 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12271 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('it'); 
	} 
}