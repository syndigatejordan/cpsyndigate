<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Conversation Spain  [# publisher id = 1242]
//Title      : The Conversation (España Edition) [ Spanish ] 
//Created on : Sep 27, 2021, 7:47:37 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5615 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}