<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : REFINITIV LIMTED  [# publisher id = 1597]
//Title      : ZAWYA by Refinitiv [ English ] 
//Created on : Nov 30, 2021, 7:33:39 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11922 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}