<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Erem Media FZ LLC  [# publisher id = 1015]
//Title      : Foochia [ Arabic ] 
//Created on : May 16, 2022, 1:27:16 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12621 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}