<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Newstex, LLC  [# publisher id = 1519]
//Title      : Blockchain News [ English ] 
//Created on : Nov 29, 2021, 2:24:17 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10643 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}