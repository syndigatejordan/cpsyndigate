<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Kaiser Family Foundation  [# publisher id = 1275]
//Title      : Kaiser Health News [ English ] 
//Created on : Aug 04, 2021, 7:07:43 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5877 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}