<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Slovenian ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_590 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority'			=>	'Koncedent', 
										'Nature of Contract'			=> 	'Vrsta pogodbe', 
										'Regulation of Procurement' 	=> 	'Uredba o javnem naročanju', 
										'Type of bid required'			=>	'Vrsta ponudbe zahteva', 
										'Award Criteria'				=>	'Merila za dodelitev', 
										'Original CPV'					=> 	'Original CPV', 
										'Country'						=>	'Country', 
										'Document Id' 					=>	'Potek Id', 
										'Type of Document'				=>	'Vrsta dokumenta', 
										'Procedure'						=>	'Postopek', 
										'Original Language'				=>	'Original Language', 
										'Current Language'				=> 	'Trenutni jezik');	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('sl');
		}
	}