<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ukrainian National News Agency "Ukrinform"  [# publisher id = 1337]
//Title      : Ukrinform [ French ] 
//Created on : Sep 23, 2021, 8:02:11 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6039 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}