<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Media OutReach Newswire  [# publisher id = 1172]
//Title      : Media OutReach Newswire [ Korean ] 
//Created on : Sep 14, 2021, 8:28:03 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7438 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ko'); 
	} 
}