<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: United Press International (UPI)
	// Titles   : UPI Reporte LatAm
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_359 extends syndParseXmlContent 
	{
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('es');
		}
		
		public function getRawArticles(&$fileContents) {	
			
			$this->addLog("Getting raw articles");
			$this->xml = @simplexml_load_string($fileContents);

			if($this->xml === FALSE) {
				return array();
			}
			return $this->getElementsByName('NewsML', $fileContents);
		}
						
		protected function getStory(&$text) {
			
			$this->addLog("getting article text");
			$story = @$this->xml->NewsItem->NewsComponent->NewsComponent->ContentItem->Encoding->DataContent->asXml();
			return $this->textFixation($this->getElementByName('DataContent', $story));
		}
		
		protected function getHeadline(&$text) {
			
			$this->addLog('Getting headline');
			$headline = $this->xml->NewsItem->NewsComponent->NewsLines->HeadLine;
			return $this->textFixation($headline);
		}
		
		protected function getArticleDate(&$text) {
			
			$this->addLog("getting article date");
			$dateId = $this->xml->NewsItem->Identification->NewsIdentifier->DateId;
			$date	= substr($dateId, 0, 4) . '-' . substr($dateId, 4, 2) . '-' . substr($dateId, 6, 2);
			
			if($date) {
				return $this->dateFormater($date);
			}
			return parent::getArticleDate($text);
		}

		
	} // End class 
