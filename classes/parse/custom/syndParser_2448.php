<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Behnam Ghasemi Co.  [# publisher id = 795]
//Title      : Kohan Journal News [ English ] 
//Created on : Jan 30, 2016, 8:20:24 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2448 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}