<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Endeavor Business Media, LLC  [# publisher id = 1712]
//Title      : Electronic Design - Automotive [ English ] 
//Created on : Jan 11, 2022, 11:19:46 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12315 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}