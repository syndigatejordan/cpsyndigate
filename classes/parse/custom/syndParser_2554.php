<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Italian-South African Chamber of Trade and Industries  [# publisher id = 831]
//Title      : Italian-South African Chamber of Trade and Industries (IACTI) - Newsletter [ English ] 
//Created on : Feb 02, 2016, 12:12:29 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2554 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}