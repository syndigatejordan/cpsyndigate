<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Muse Arabia  [# publisher id = 727]
//Title      : Muse Arabia [ English ] 
//Created on : Sep 19, 2021, 7:03:24 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2281 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}