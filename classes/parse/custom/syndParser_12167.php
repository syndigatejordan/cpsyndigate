<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CTN News  [# publisher id = 1623]
//Title      : CTN News l Chiang Rai Times [ English ] 
//Created on : Sep 06, 2021, 1:41:08 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12167 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}