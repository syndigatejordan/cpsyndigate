<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Estonian ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_577 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority' 		=>	'Abi andva ametiasutuse', 
										'Nature of Contract'		=>	'Lepingu olemuse', 
										'Regulation of Procurement' =>	'Määrus riigihangete', 
										'Type of bid required'		=>	'Pakkumise tüüp nõutud', 
										'Award Criteria'			=>	'Määramise kriteeriumid',
										'Original CPV'				=> 	'Originaal CPV', 
										'Country'					=>	'Riik', 
										'Document Id' 				=>	'Dokumendi Id', 
										'Type of Document'			=>	'Dokumendi liik', 
										'Procedure'					=>	'Menetlus', 
										'Original Language'			=>	'Originaal keel', 
										'Current Language'			=> 	'Aktiivne keel');	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('et');
		}
	}