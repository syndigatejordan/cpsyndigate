<?php

//////////////////////////////////////////////////////////////////////////////
// Publisher: AdCom Advertising & Publishing  [# publisher id =373 ] 
// Titles   : AutoMiddleEast.com [English]
//////////////////////////////////////////////////////////////////////////////

class syndParser_1250 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getAbstract(&$text) {
    $this->addLog("getting article summary");

    $abstract = $this->textFixation($this->getCData($this->getElementByName('description', $text)));
    if (strpos($abstract, 'Related posts:')) {
      $abstract = substr($abstract, 0, strpos($abstract, 'Related posts:')); //($this -> search, '', $abstract);
    }
    return $abstract;
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getElementsByName('category', $text);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = $this->textFixation($this->getCData($cat));
      }
    }
    return implode(', ', $originalCats);
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");

    $story = $this->textFixation($this->getCData($this->getElementByName('content:encoded', $text)));
    $story = preg_replace('/<img[^>]+\>/i', '', $story);
    $story = preg_replace('!\s+!', ' ', $story);
    //}
    return $story;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $auther = $this->textFixation($this->getCData($this->getElementByName('dc:creator', $text)));
    return $auther;
  }

  protected function getImages(&$text) {

    $this->story = trim($this->textFixation($this->getCData($this->getElementByName('content:encoded', $text))));
    $imagesArray = array();
    preg_match_all("/<img[^>]+\>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
