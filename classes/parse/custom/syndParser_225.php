<?php

//////////////////////////////////////////////////////////////////////////////
// Publisher: Cihan Haber Ajansı Reklâmcılık A.Ş.
// Title    : Cihan News Agency (CNA) Turkish 
//////////////////////////////////////////////////////////////////////////////
class syndParser_225 extends syndParseRss {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('tr');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('item', $fileContents);
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->getCData($this->getElementByName('title', $text));
    return $this->textFixation($headline);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    $date = trim(preg_replace('/GMT(.*)/is', '', $date));
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $this->story = trim($this->textFixation($this->getCData($this->getElementByName('description', $text))));
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    return $this->story;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    preg_match("/<dc:creator>(.*?)<\/dc:creator>/si", $text, $author);
    if (!empty($author[1])) {
      return trim($this->textFixation($this->getCData($author[1])));
    } else {
      return "";
    }
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getElementsByName('category', $text);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = $this->textFixation($this->getCData($cat));
      }
    }
    return implode(', ', $originalCats);
  }

  protected function getImages(&$text) {
    preg_match_all("/<media:content(.*?)\/media:content>/si", $text, $imgs);
    $imagesArray = array();
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      preg_match('/url="(.*?)"></si', $img, $imageInfo);
      $imagePath = $imageInfo[1];
      $image_caption = trim($this->textFixation($this->getCData($this->getElementByName('media:title', $img))));
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }

    return $imagesArray;
  }

}
