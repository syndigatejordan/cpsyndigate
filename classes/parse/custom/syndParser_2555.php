<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Italian-South African Chamber of Trade and Industries  [# publisher id = 831]
//Title      : Italian-South African Chamber of Trade and Industries (IACTI) - Press Releases [ English ] 
//Created on : Feb 02, 2016, 12:12:39 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2555 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}