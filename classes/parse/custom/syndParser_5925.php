<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Pressenza  [# publisher id = 1305]
//Title      : Pressenza [ French ] 
//Created on : Feb 15, 2020, 6:32:40 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5925 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('fr');
  }
}
