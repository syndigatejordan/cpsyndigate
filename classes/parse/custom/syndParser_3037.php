<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Wiser Hotel project  [# publisher id = 1096]
//Title      : Mandarin Oriental [ English ] 
//Created on : Jan 11, 2017, 2:02:14 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3037 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = $this->textFixation($this->getElementByName('fulltext', $text));
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    return $this->story;
  }

  protected function getImages(&$text) {
    
  }

}
