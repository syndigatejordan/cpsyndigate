<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Iranian Labour News Agency (ILNA)  [# publisher id = 1411]
//Title      : Iranian Labour News Agency (ILNA) [ English ] 
//Created on : Oct 20, 2020, 11:57:46 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6567 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}