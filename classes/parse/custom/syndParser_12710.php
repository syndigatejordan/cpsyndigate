<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Wagner J. Au  [# publisher id = 1847]
//Title      : New World Notes [ English ] 
//Created on : Sep 22, 2022, 5:42:49 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12710 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}