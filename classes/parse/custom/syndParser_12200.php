<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Azadi Radio  [# publisher id = 1646]
//Title      : Azadi Radio [ Pashto ] 
//Created on : Oct 04, 2021, 11:19:46 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12200 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ps'); 
	} 
}