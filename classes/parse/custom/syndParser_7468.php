<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : China Economic News  [# publisher id = 1507]
//Title      : China Economic Times [ simplified Chinese ] 
//Created on : Jan 19, 2021, 2:24:18 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7468 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh-Hans'); 
	} 
}