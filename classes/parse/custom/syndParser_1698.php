<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Tel Aviv Stock Exchange (TASE)  
//Title     : Tel Aviv Stock Exchange (TASE) [ English ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1698 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getStory(&$text) {
    $this->addLog('getting article story');
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', " ", $this->story);
    return $this->story;
  }

  protected function getImages(&$text) {
      $this->story = $this->textFixation($this->getCData($this->getElementByName('fulltext', $text)));
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();
      $this->imagesArray = array();
      preg_match_all("/<img[^>]+>/i", $this->story, $imgs);

      foreach ($imgs[0] as $img) {
          $this->addLog("getting article images");
          $imageInfo = syndParseHelper::getImgElements($img, 'img');
          $imagePath = $imageInfo[0];
          if (!$imagePath) {
              continue;
          }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);

      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

}
