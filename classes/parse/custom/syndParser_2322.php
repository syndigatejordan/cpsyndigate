<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Bawaba Middle East Limited  [# publisher id = 62]
//Title      : Oil & Gas Projects Tracker [ English ] 
//Created on : Aug 18, 2021, 10:59:59 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2322 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}