<?php
//////////////////////////////////////////////////////////////////////////////
// Publisher: India Business Insight Database  [# publisher id =1403 ]
// Titles   : New Straits Times [ English ]
//////////////////////////////////////////////////////////////////////////////

class syndParser_1403 extends syndParseXmlContent {// syndParseXmlContent

	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('en');
		$this -> extensionFilter = 'xml';
	}

	protected function getRawArticles(&$fileContents) {
		//get articles
		$this -> addLog("getting articles raw text");
		return $this -> getElementsByName('DC', $fileContents);
	}

	protected function getStory(&$text) {
        //get body text
        $this->addLog("getting article text");
        $body = $this->getCData($this->getElementByName('BI', $text));
        //This action depends on "Task #1064 Remove all the images from our clients feeds"
        $body = preg_replace('/<img[^>]+\>/i', '', $body);
        return $body;
    }

	protected function getHeadline(&$text) {
		$this -> addLog("getting article headline");

		$headline = $this -> getCData($this -> getElementByName('TI', $text));
		return $headline;
	}

	protected function getArticleDate(&$text) {
		$this -> addLog("getting article date");

		$date = $this -> getElementByName('PD', $text);
		$date = date('Y-m-d', strtotime($date));
		return $date;
	}

	public function getOriginalCategory(&$text) {
		$this -> addLog('getting article category');

		$cats = $this -> getElementsByName('BT', $text);
		$originalCats = array();

		if (!empty($cats)) {
			foreach ($cats as $cat) {
				if (empty($cat)) {
					continue;
				}
				$originalCats[] = $this -> textFixation($cat);
			}
		}
		return implode(', ', $originalCats);
	}

	protected function getImages(&$text)
    {
        //This action depends on "Task #1064 Remove all the images from our clients feeds"
        return array();
        $this->addLog("getting article images");

        //echo PHP_EOL . $this -> currentlyParsedFile . PHP_EOL;
        //echo PHP_EOL . PHP_EOL ; print_r($text); echo PHP_EOL . PHP_EOL;
        //exit;
        $images = $this->getElementsByName('images', $text);
        if (empty($images)) {
            return array();
        }

        $finalImages = array();
		foreach ($images as $image) {
			$image = $this -> getElementByName('image', $image);

			$image = trim($image);
			if (empty($image)) {
				return array();
			}

			if (file_exists($this -> currentDir . $image)) {
				return $this -> getAndCopyImagesFromArray(array($this -> currentDir . $image));
			}
			return array();
		}
	}

}