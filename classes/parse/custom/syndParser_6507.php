<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : AETOSWire  [# publisher id = 1389]
//Title      : AETOSWire [ Arabic ] 
//Created on : Sep 22, 2020, 11:44:31 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6507 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}