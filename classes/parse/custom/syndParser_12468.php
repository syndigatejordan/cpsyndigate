<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Cointelegraph  [# publisher id = 1767]
//Title      : Cointelegraph [ German ] 
//Created on : Feb 21, 2022, 11:17:22 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12468 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('de'); 
	} 
}