<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Shenzhen Securities Times Co., Ltd.  [# publisher id = 1557]
//Title      : Securities Times - Disclosures [ simplified Chinese ] 
//Created on : Mar 30, 2021, 2:39:04 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10694 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh-Hans'); 
	} 
}