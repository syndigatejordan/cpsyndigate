<?php
///////////////////////////////////////////////////////////////////////////
// Publisher : Community Media Network 
// Titles    : Eye on Media [Arabic]
///////////////////////////////////////////////////////////////////////////

class syndParser_758 extends syndParseCms {
	
	public function customInit() {
		parent::customInit();
		$this->defaultLang = $this->model->getLanguageId('ar');
	}

	public function getStory(&$text) {
		$story = '<p>';
		$story .= parent::getStory($text);
		$story = str_replace(". ", ".</p><p>", $story);
		$story .= '</p>';

		return $story;
	}


}
