<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : South African Chamber of Commerce and Industry  [# publisher id = 830]
//Title      : South African Chamber of Commerce and Industry (SACCI) - Press Releases [ English ] 
//Created on : Feb 02, 2016, 12:12:18 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2553 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}