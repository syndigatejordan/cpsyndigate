<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Imanuel Marcus/The Berlin Spectator  [# publisher id = 1559]
//Title      : The Berlin Spectator [ English ] 
//Created on : Sep 19, 2021, 10:24:13 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10794 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}