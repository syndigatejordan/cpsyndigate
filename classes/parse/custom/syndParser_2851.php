<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Gulf Medical University  [# publisher id = 1022]
//Title      : HealthMagazine.ae [ English ] 
//Created on : Aug 22, 2016, 8:36:35 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2851 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}