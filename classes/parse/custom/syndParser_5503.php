<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Manhal FZ LLC (French)  [# publisher id = 1186]
//Title      : Islamic Finance Hub (Articles) [ French ] 
//Created on : Apr 01, 2019, 5:59:48 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

require_once '/usr/local/syndigate/classes/parse/syndParseAlManhal.php';

class syndParser_5503 extends syndParseAlManhal {

  protected function getArticleDate(&$text) {

    $this->addLog("getting articles date");
    $date = $this->textFixation($this->getCData($this->getElementByName('Date', $text)));
    $date = str_replace("<", "", $date);
    $date = str_replace(">", "", $date);
    $date = trim(preg_replace('!\s+!', ' ', $date));
    if (preg_match("/GMT/", $date)) {
      $date = trim(preg_replace("/GMT(.*)/is", "", $date));
      $date = date("Y-m-d", strtotime($date));
      $date = explode('-', $date);
      if (checkdate((int) $date[2], (int) $date[1], (int) $date[0])) {
        $date = $date[0] . "-" . $date[2] . "-" . $date[1];
      } else {
        $date = $date[0] . "-" . $date[1] . "-" . $date[2];
      }
    } else {
      $date = explode('/', $date);
      $date = $date[2] . "-" . $date[1] . "-" . $date[0];
    }
    return $date;
  }

}
