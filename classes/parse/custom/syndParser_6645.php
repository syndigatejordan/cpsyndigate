<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : OANA - The Organization of Asia-Pacific News Agencies  [# publisher id = 1426]
//Title      : OANA - The Organization of Asia-Pacific News Agencies [ English ] 
//Created on : Oct 13, 2020, 8:48:52 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6645 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}