<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Elaph Publishing House Ltd.  [# publisher id = 453]
//Title      : Elaph [ Arabic ] 
//Created on : Aug 10, 2021, 7:59:26 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1414 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}