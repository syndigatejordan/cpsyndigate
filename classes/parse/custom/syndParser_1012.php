<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Fusion Media Limited  [# publisher id = 304]
//Title      : Investing.com Français (France Edition) [ French ] 
//Created on : May 12, 2022, 7:35:06 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1012 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}