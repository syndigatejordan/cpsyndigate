<?php

//////////////////////////////////////////////////////////////////////////////
// Publisher: Haykal Media
// Titles   : Aliqtisadi
//////////////////////////////////////////////////////////////////////////////

class syndParser_795 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->getCData($this->getElementByName('title', $text));
    return $headline;
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getElementsByName('category', $text);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = $this->textFixation($this->getCData($cat));
      }
    }
    return implode(', ', $originalCats);
  }

  public function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    $date = preg_replace('/ \+(.*)/is', '', $date);
    $date = date('Y-m-d', strtotime($date));
    return $date;
  }

  protected function getAbstract(&$text) {
    $this->addLog("getting article summary");
    $desc = $this->textFixation($this->getCData($this->getElementByName('description', $text)));
    $desc = str_replace('[...]', '', $desc);
    $desc = strip_tags($desc, '<p>');
    return $desc;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getCData($this->getElementByName('dc:creator', $text)));
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = $this->textFixation($this->getCData($this->getElementByName('content:encoded', $text)));
    $story = strip_tags($story, '<p>');
    return $story;
  }

}
