<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Republic of Turkey Ministry of Foreign Affairs  [# publisher id = 901]
//Title      : Republic Turkey Ministry of Foreign Affairs - Press Releases [ English ] 
//Created on : Apr 07, 2016, 10:48:43 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2679 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}