<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Islamic Republic of Afghanistan - Government Media & Information Center (GMIC)  [# publisher id = 1413]
//Title      : Islamic Republic of Afghanistan - Government Media & Information Center (GMIC) [ English ] 
//Created on : Oct 20, 2020, 6:00:15 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6597 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}