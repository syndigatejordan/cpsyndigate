<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: Apex
	// Title    : Oman Today
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_601 extends syndParseAbTxtSample
	{
		protected function getRawArticles(&$fileContents)
		{			
			$this->addLog('Get raw articles');
			$fileContents	= str_replace("", "'", $fileContents);
			$fileContents	= str_replace("", "\"", $fileContents);
			$fileContents	= str_replace("", "\"", $fileContents);
			$fileContents	= str_replace("Â", "", $fileContents);
			$fileContents	= str_replace("", "", $fileContents);
			$fileContents	= str_replace("", "'", $fileContents);

			$fileContents = str_replace("\n\r", "\n", $fileContents);
			$fileContents = str_replace("\r", "\n", $fileContents);
			return array($fileContents);
		}
		
		public function customInit() {
			$this->addLog('Custom customInit.');
			parent::customInit();
			$this->charEncoding = 'ISO-8859-1';
			$this->defaultLang = $this->model->getLanguageId('en');
		}
		
		public function getStory(&$text) {
			$story = parent::getStory($text);
			$story = str_replace("\n\n", "\n", $story);
			$story = str_replace("\n\n", "\n", $story);
			
			return str_replace("\n", "<br />\n", $story);
		}
	}
