<?php

//////////////////////////////////////////////////////////////////////////////
// Publisher: As-Safir News
// Titles   : As-Safir News
//////////////////////////////////////////////////////////////////////////////

class syndParser_45 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
    $this->extensionFilter = 'xml';
    $this->cat_translation['Front Page'] = 'Front Page';
    $this->cat_translation['Politics'] = 'سياسة';
    $this->cat_translation['Local'] = 'محليّات';
    $this->cat_translation['Economy'] = 'إقتصاد';
    $this->cat_translation['Arabic_International'] = 'عربي ودولي';
    $this->cat_translation['Sports_from_Lebanon'] = 'رياضة من لبنان';
    $this->cat_translation['International_SPorts'] = 'رياضة من العالم';
    $this->cat_translation['Culture'] = 'ثقافة';
    $this->cat_translation['Photos_Voices'] = 'صوت وصورة';
    $this->cat_translation['Opinions'] = 'قضايا وآراء';
    $this->cat_translation['Safir_Writers'] = 'كتّاب السفير';
    $this->cat_translation['Painting'] = 'رسم';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $d = $this->getElementsByName('item', $fileContents);
    return $d;
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $body = html_entity_decode(trim($this->getElementByName('description', $text)));
    $body = strip_tags($body, '<p><br><strong><b><u><i>');
    if (empty($body)) {
      return '';
    }
    return $body;
  }

  public function getHeadline(&$text) {
    $head = trim($this->getElementByName('title', $text));
    return $head;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $author = trim($this->getElementByName('a10:author', $text));
    $author = trim($this->getElementByName('a10:name', $author));
    return $author;
  }

  public function getArticleDate(&$text) {
    $date = trim($this->getElementByName('pubDate', $text));
    $date = preg_replace('/ \+(.*)/is', '', $date);
    return date('Y-m-d', strtotime($date));
  }

  public function getOriginalCategory(&$text) {

    $this->addLog('Getting article category');

    $filename = basename($this->currentlyParsedFile);
    $filename = str_replace('.xml', '', $filename);
    if (isset($this->cat_translation[$filename])) {
      return $this->cat_translation[$filename];
    } else {
      return false;
    }
  }

  public function getArticleReference(&$text) {
    preg_match('/link href="(.*?)\/RssFeed/si', $text, $link);
    $link = $link[1];
    return $link;
  }

  protected function getImages(&$text) {
    $this->imagesArray = array();
    preg_match_all('/<enclosure url="(.*?)"/si', $text, $imgs);

    foreach ($imgs[1] as $img) {
      $this->addLog("getting article images");
      $imagePath = '';
      $imagePath = $img;
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $name_image = explode('/images/', $copiedImage);
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if ($images[0]['image_caption'] == $name_image[1])
        $images[0]['image_caption'] = '';
      $images[0]['is_headline'] = false;
      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

  public function getArticleOriginalId($params = array()) {
    $defaultArray = array('text' => '', 'headline' => '', 'articleDate' => '', 'issueNum' => 0);
    $params = array_merge($defaultArray, $params);

    $articleOriginalId = trim($this->getElementByName('guid', $params['text']));
    if (!$articleOriginalId) {
      $articleOriginalId = trim($this->getElementByName('link', $params['text']));
    }

    $articleOriginalId = trim($this->getCData($articleOriginalId));

    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    } else {
      // same article published under many categories via many links
      // ex. http://www.assafir.com/article.aspx?EditionId=1960&ChannelId=46314&ArticleId=3050
      // all we have is to remove the ChannelId

      $newOriginal = array();
      $articleOriginalArray = explode("&", $articleOriginalId);

      foreach ($articleOriginalArray as $element) {
        if (false === strpos($element, 'ChannelId')) {
          $newOriginal[] = $element;
        }
      }

      return $this->title->getId() . "_" . sha1(implode('&', $newOriginal));
    }
  }

}
