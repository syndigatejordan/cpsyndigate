<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Frontiers Media SA  [# publisher id = 1278]
//Title      : Frontiers Science News [ English ] 
//Created on : Dec 11, 2019, 1:25:57 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5880 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
