<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Lithuanian ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_581 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority' 		=> 	'Suteikiančiosios institucijos', 
										'Nature of Contract'		=> 	'Sutarties pobūdį', 
										'Regulation of Procurement' =>	'Reglamento pirkimų', 
										'Type of bid required'		=> 	'Tipas pasiūlymas, reikalingas', 
										'Award Criteria'			=>	'Sutarčių skyrimo kriterijai', 
										'Original CPV'				=> 	'Originalus CPV', 
										'Country'					=>	'Šalis', 
										'Document Id' 				=>	'Dokumento ID',
										'Type of Document'			=>	'Dokumento tipas', 
										'Procedure'					=>	'Procedūra', 
										'Original Language'			=>	'Originalo kalba', 
										'Current Language'			=> 	'Dabartinis kalba');	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('lt');
		}
	}