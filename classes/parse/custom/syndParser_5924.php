<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Pressenza  [# publisher id = 1305]
//Title      : Pressenza [ Spanish ] 
//Created on : Feb 15, 2020, 6:32:24 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5924 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('es');
  }
}
