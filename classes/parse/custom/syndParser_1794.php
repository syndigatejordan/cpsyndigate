<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Sud Quotidien  
//Title     : Sud Quotidien [ French ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1794 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('fr');
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $title = strtolower(trim($this->textFixation($this->getElementByName('title', $text))));
    return $title;
  }

}
