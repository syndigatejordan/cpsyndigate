<?php

///////////////////////////////////////////////////////////////////////////
//Publisher : WENN Media Group  [# publisher id =1189 ] 
//Titles    : COVER IMAGES  [ English ]
//Created on : Mar 03, 2019, 08:48:41 AM
//Author     : eyad
///////////////////////////////////////////////////////////////////////////

class syndParser_3828 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  //Handle Empty body
  public function parse() {
    $articles = array();
    foreach ($this->files as $file) {
      if ($this->extensionFilter) {
        if (!syndParseHelper::isFileExtension($file, $this->extensionFilter)) {
          $this->addLog("Wrong extension for file : $file)");
          continue;
        }
      }
      if (!file_exists($file)) {
        $this->addLog("file dose not exist: $file)");
        continue;
      }

      $this->addLog("get file contents (file:$file)");
      $fileContents = $this->getFileContents($file);

      if (!$fileContents) {
        continue;
      }

      $this->currentlyParsedFile = $file;
      $this->loadCurrentDirectory();
      $this->loadUpperDir();
      $rawArticles = $this->getRawArticles($fileContents);
      foreach ($rawArticles as $rawArticle) {
        $article = $this->getArticle($rawArticle);
        $articles[] = $article;
      }
    }
    return $articles;
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('item', $fileContents);
    return $articles;
  }

  protected function getStory(&$text) {

    return "";
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $Headline = $this->textFixation($this->getCData($this->getElementByName('headline', $text)));
    $Headline = html_entity_decode($Headline, ENT_QUOTES);
    return $Headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('DateCreated', $text);
    $date = trim(preg_replace('/T(.*)/is', '', $date));
    return date('Y-m-d', strtotime($date));
  }

  protected function getImages(&$text) {
    $this->addLog("getting article image");
    $container_media = $this->getElementsByName('container_media', $text);
    $imagesArray = array();
    foreach ($container_media as $item) {
      $img = array();
      $image_Name = $this->textFixation($this->getElementByName('media', $item));
      $original_name = explode(".", $image_Name);

      $image_full_path = $this->currentDir . $image_Name;
      $image_full_path = str_replace($this->title->getParseDir(), $this->title->getHomeDir(), $image_full_path);
      $image_caption = $this->textFixation($this->getCData($this->getElementByName('media_caption', $item)));
      $image_caption = html_entity_decode($image_caption, ENT_QUOTES);
      $image_caption = trim(preg_replace('/\s\s+/', ' ', $image_caption));
      $currentDir = str_replace($image_Name, "", $image_full_path);
      if (file_exists($image_full_path)) {
        //the video name is same as the image name here  		
        $name = $this->model->getImgReplacement($image_Name, $currentDir, 3828);
        //$this->addLog("getting article image  image_Name $image_Name");
        //$this->addLog("getting article currentDir $currentDir");
        //$this->addLog("getting article image name $name");
        $img['img_name'] = str_replace(IMGS_PATH, IMGS_HOST, $name);
        $img['original_name'] = $original_name[0];
        $img['image_caption'] = $image_caption;
        $img['is_headline'] = FALSE;
        $img['image_original_key'] = sha1($image_Name);
        array_push($imagesArray, $img);
      }
    }
    return $imagesArray;
  }

  public function getArticleOriginalId($params = array()) {
    $articleOriginalId = $this->getElementByName('key', $params['text']);

    $this->articleOriginalId = $articleOriginalId;
    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }
    return $this->title->getId() . "_" . sha1($articleOriginalId);
  }

}

?>
