<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : nouvelledeguinee.com SA  [# publisher id = 1629]
//Title      : nouvelledeguinee.com [ French ] 
//Created on : Sep 30, 2021, 6:37:44 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12177 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}