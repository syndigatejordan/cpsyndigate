<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : South Africa - Nigeria Chamber of Commerce  [# publisher id = 834]
//Title      : South Africa - Nigeria Chamber of Commerce (SA-NCC) - Blog Spot [ English ] 
//Created on : Feb 02, 2016, 12:13:03 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2558 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}