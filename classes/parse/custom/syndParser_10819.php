<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Caixin Media  [# publisher id = 1570]
//Title      : Caixin [ simplified Chinese ] 
//Created on : Apr 26, 2021, 12:00:21 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10819 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh-Hans'); 
	} 
}