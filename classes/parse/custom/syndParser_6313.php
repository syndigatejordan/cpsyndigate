<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bloomberg L.P.  [# publisher id = 1202]
//Title      : Bloomberg Health & Science [ English ] 
//Created on : Aug 04, 2021, 12:15:30 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6313 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}