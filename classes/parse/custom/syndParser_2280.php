<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Muse Arabia  [# publisher id = 727]
//Title      : Muse Arabia [ Arabic ] 
//Created on : Sep 19, 2021, 7:03:17 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2280 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}