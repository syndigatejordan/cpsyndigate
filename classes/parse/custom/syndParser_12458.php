<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Seeking Alpha Ltd.  [# publisher id = 1675]
//Title      : Seeking Alpha [ English ] 
//Created on : Feb 20, 2022, 7:23:50 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12458 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}