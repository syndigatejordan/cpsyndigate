<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bourse de Casablanca  [# publisher id = 595]
//Title      : Bourse de Casablanca [ Arabic ] 
//Created on : Oct 16, 2019, 5:52:58 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1683 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}