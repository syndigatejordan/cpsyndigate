<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Communication Week Media Limited  [# publisher id = 568]
//Title      : Nigeria Communications Week [ English ] 
//Created on : Dec 15, 2019, 8:45:55 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1740 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}