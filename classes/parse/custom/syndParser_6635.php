<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Malaysia National News Agency (BERNAMA)  [# publisher id = 1421]
//Title      : Malaysia National News Agency (BERNAMA) [ Spanish ] 
//Created on : Oct 26, 2020, 7:26:50 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6635 extends syndParseCms
{
    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('es');
    }

    public function copyUrlImgIfNotCached($imageUrl)
    {
        if (preg_match("/.pdf/", strtolower($imageUrl))) {
            $baseName = sha1($imageUrl) . ".pdf";
        } else {
            $baseName = sha1($imageUrl) . ".jpeg";
        }
        $copiedImage = $this->imgCacheDir . $baseName;

        if (!is_dir($this->imgCacheDir)) {
            mkdir($this->imgCacheDir, 0755, true);
        }

        if (!file_exists($copiedImage)) {
            $opts = array();
            $http_headers = array();
            $http_headers[] = 'Expect:';

            $opts[CURLOPT_URL] = $imageUrl;
            $opts[CURLOPT_HTTPHEADER] = $http_headers;
            $opts[CURLOPT_CONNECTTIMEOUT] = 10;
            $opts[CURLOPT_TIMEOUT] = 60;
            $opts[CURLOPT_HEADER] = FALSE;
            $opts[CURLOPT_BINARYTRANSFER] = TRUE;
            $opts[CURLOPT_VERBOSE] = FALSE;
            $opts[CURLOPT_SSL_VERIFYPEER] = FALSE;
            $opts[CURLOPT_SSL_VERIFYHOST] = 2;
            $opts[CURLOPT_RETURNTRANSFER] = TRUE;
            $opts[CURLOPT_FOLLOWLOCATION] = TRUE;
            $opts[CURLOPT_MAXREDIRS] = 2;
            $opts[CURLOPT_IPRESOLVE] = CURL_IPRESOLVE_V4;

            # Initialize PHP/CURL handle
            $ch = curl_init();
            curl_setopt_array($ch, $opts);
            $imgContent = curl_exec($ch);

            # Close PHP/CURL handle
            curl_close($ch);
            $myfile = fopen($copiedImage, "w");
            fwrite($myfile, $imgContent);
            fclose($myfile);
            if (!empty($imgContent)) {
                return $copiedImage;
            } else {
                unlink($copiedImage);
                return false;
            }
        } else {
            return $copiedImage;
        }
    }

}