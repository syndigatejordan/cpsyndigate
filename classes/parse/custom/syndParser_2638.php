<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Kingdom of Saudi Arabia Ministry of Foreign Affairs  [# publisher id = 882]
//Title      : Kingdom of Saudi Arabia Ministry of Foreign Affairs - Important Issues [ English ] 
//Created on : Mar 01, 2016, 12:33:31 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2638 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

}
