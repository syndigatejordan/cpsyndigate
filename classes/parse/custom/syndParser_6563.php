<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Government of Singapore  [# publisher id = 894]
//Title      : Government of Singapore News [ English ] 
//Created on : Oct 15, 2020, 10:00:49 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6563 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}