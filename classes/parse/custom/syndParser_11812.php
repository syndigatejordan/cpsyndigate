<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ODO Septima  [# publisher id = 1594]
//Title      : Brestskii Kaleidoskop [ Russian ] 
//Created on : Jul 06, 2021, 8:06:47 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11812 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}