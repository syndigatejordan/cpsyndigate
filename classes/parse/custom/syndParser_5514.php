<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Middle East News Agency (MENA)  [# publisher id = 138]
//Title      : Middle East News AGency (MENA) - Economy [ English ] 
//Created on : Apr 01, 2019, 11:42:06 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5514 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}