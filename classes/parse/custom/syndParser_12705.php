<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Panchsheel News Private Limited  [# publisher id = 1844]
//Title      : India Press Agency (IPA) [ English ] 
//Created on : Sep 01, 2022, 2:13:32 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12705 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}