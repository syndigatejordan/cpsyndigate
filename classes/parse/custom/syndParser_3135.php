<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Africa Media Holdings  [# publisher id = 1142]
//Title      : Lesotho Times [ English ] 
//Created on : Aug 03, 2021, 12:30:02 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3135 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}