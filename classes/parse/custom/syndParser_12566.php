<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IDG Communications, Inc.  [# publisher id = 1790]
//Title      : TECHADVISOR FRANCE [ French ] 
//Created on : Apr 06, 2022, 10:23:13 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12566 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}