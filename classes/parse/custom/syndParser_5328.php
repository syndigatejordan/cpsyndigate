<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ​The Egyptian Center for Economic Studies (ECES)  [# publisher id = 1203]
//Title      : ECES Working Papers [ Arabic ] 
//Created on : Jan 03, 2019, 9:13:33 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5328 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}