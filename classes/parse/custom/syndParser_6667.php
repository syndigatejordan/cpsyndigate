<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Turkish Government  [# publisher id = 148]
//Title      : Anadolu Agency (AA) [ Kurdish ] 
//Created on : Oct 13, 2020, 9:37:34 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6667 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ku'); 
	} 
}