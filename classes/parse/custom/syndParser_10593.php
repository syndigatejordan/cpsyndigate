<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Xinhua News Agency  [# publisher id = 1555]
//Title      : Economic Information Daily [ simplified Chinese ] 
//Created on : Mar 28, 2021, 7:56:44 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10593 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh-Hans'); 
	} 
}