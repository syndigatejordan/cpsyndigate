<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence Haïtienne de Presse  [# publisher id = 1382]
//Title      : Agence Haïtienne de Presse [ French ] 
//Created on : Sep 28, 2021, 7:52:51 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6488 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}