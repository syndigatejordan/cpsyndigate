<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : RASANAH - International Institute for Iranian Studies  [# publisher id = 1264]
//Title      : RASANAH - Iran Case File [ English ] 
//Created on : May 31, 2020, 11:36:21 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5814 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}