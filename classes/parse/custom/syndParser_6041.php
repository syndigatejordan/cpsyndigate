<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ukrainian National News Agency "Ukrinform"  [# publisher id = 1337]
//Title      : Ukrinform [ Polish ] 
//Created on : Sep 23, 2021, 8:03:51 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6041 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pl'); 
	} 
}