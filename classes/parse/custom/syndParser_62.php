<?php

///////////////////////////////////////////////////////////////////////////
// Publisher :  Jordan Press Foundation 
// Titles    :  The Jordan Times [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_62 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->charEncoding = 'UTF-8';
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $allowedTags = '<h1><h2><h3><h4><h5><h6><b><i><u><p><hr><br><br/><br /><big><em><small><strong><sub><sup><ins><del><s><strike><code><kbd><q><BLOCKQUOTE><cite><dd><div><span><ul><li><ol><style><table><td><th><tr><tbody><thead><a><iframe><object><embed><param>';
    $this->story = strip_tags($this->story, $allowedTags);
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story)) {
      return '';
    }
    //var_dump($this->story);
    //$this->story = utf8_encode($this->story);
    return $this->story;
  }


  public function getArticleOriginalId($params = array()) {
    $defaultArray = array('text' => '', 'headline' => '', 'articleDate' => '', 'issueNum' => 0);
    $params = array_merge($defaultArray, $params);
    if (!$articleOriginalId = trim($this->getElementByName('id', $params['text']))) {
      return parent::getArticleOriginalId($params);
    }
    return $this->title->getId().'_'.sha1($articleOriginalId . trim($params['headline'])).'_'.sha1($params['text']);
  }
}
