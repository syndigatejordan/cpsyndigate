<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bloomberg L.P.  [# publisher id = 1202]
//Title      : Bloomberg Businessweek + Pursuits [ English ] 
//Created on : Mar 27, 2020, 3:28:42 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_6007 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getStory(&$text) {
    $story = trim($this->textFixation($this->getCData($this->getElementByName('description', $text))));
    if (!empty($story)) {
      $story = "<p>$story</p>";
    }
    $keywords = trim($this->textFixation($this->getCData($this->getElementByName('media:keywords', $text))));
    if (!empty($keywords)) {
      $keywords = "<p>keywords: $keywords</p>";
    }
    $credit = trim($this->textFixation($this->getCData($this->getElementByName('author', $text))));
    if (!empty($credit)) {
      $credit = "<p>By $credit</p>";
    }
    if (preg_match("/pursuits/", strtolower($this->currentlyParsedFile))) {
      if (preg_match("/pursuits/", strtolower($keywords))) {
        $story = $credit . $story . $keywords;
        return $story;
      } else {
        return "";
      }
    }
    $story = $credit . $story . $keywords;
    return $story;
  }

  public function getHeadline(&$text) {
    $this->headline = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
    return $this->headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    $date = trim(str_replace('/\+(.*)/is', '', $date));
    return date('Y-m-d', strtotime($date));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = trim($this->getElementByName('category', $text));
    return $cats;
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $image_caption = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
    $matches = null;
    preg_match_all('/<media:content medium="image"(.*?)<\/media:content>/is', $text, $matches);
    foreach ($matches[0] as $match) {
      $this->addLog("getting article images");
      $image_caption = trim($this->getElementByName('media:title', $match));
      $imageInfo = syndParseHelper::getImgElements($match, 'media:content', 'url');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        $this->addLog("No Path");
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $matches = null;
    $videos = array();
    preg_match('/<media:content(.*?)medium="video"(.*?)>/is', $text, $matches);
    $videoname = syndParseHelper::getImgElements($matches[0], 'media:content', 'url');
    $link = $videoname[0];
    if (!empty($link)) {
      $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
      $video['video_name'] = $link;
      $video['original_name'] = $videoName;
      $video['video_caption'] = $videoName;
      $mimeType = syndParseHelper::getImgElements($matches[0], 'media:content', 'type');
      $mimeType = $mimeType[0];
      $video['mime_type'] = $mimeType;
      $date = $this->getElementByName('pubDate', $text);
      $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
      $videos[] = $video;
    }
    return $videos;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    //sleep(1);
      $extension = basename($imageUrl);
    $extension = explode(".", $extension);
    if (isset($extension[1])) {
      $extension = ".{$extension[1]}";
    } else {
      $extension = ".jpeg";
    }
    $baseName = sha1($imageUrl) . $extension;
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $imageUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
