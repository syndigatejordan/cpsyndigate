<?php 
/**
 * IMPORTANT : 
 *  
 * This is the abstrct class for all Amcham Titles, the publisher Id is 204
 *
 */
class syndParserPublisher_204 extends syndParseContent 
{

	public function customInit() {
		parent::customInit();
		$this->defaultLang = $this->model->getLanguageId('en');
		$this->extensionFilter = 'xml';
		
		$this->sectors 	  = $this->getSectors();
		$this->subSectors = $this->getSubsectors();
		$this->sectorsARR = $this->getSectorsArr(); 		
	}	
	
	public function getRawArticles(&$fileContents) {		
		
		$this->addLog("getting raw articles text");
		$xml = simplexml_load_string($fileContents);
		
		$articles = array();
		foreach ($xml->Tender as $t) {			
			unset($article);

			$article['original_id'] 		= (int)$t->attributes()->Tender_ID;
			$article['headline'] 			= (string)$t->attributes()->Subject;
			$article['date']	 			= (string)$t->attributes()->Date_Published;
			$article['body']	 			= (string)$t->attributes()->Description;
									
			$article['extra']['Specs_Fees'] = (string)$t->attributes()->Specs_Fees;			
			$article['extra']['Bid_Bond']   = (string)$t->attributes()->Bid_Bond;
			$article['extra']['Country']   = (string)$t->attributes()->Country;
			$article['extra']['Client_ID']   = (string)$t->attributes()->Client_ID;			
			$article['extra']['Expiry_Date']   = (string)$t->attributes()->Expiry_Date;
			$article['extra']['Performance_Bond']   = (string)$t->attributes()->Performance_Bond;
			$article['extra']['Location']   = (string)$t->Location->attributes()->Location_Name;
			$article['extra']['Financing_Name']   = (string)$t->Tenders_Financing->attributes()->Financing_Name;
			$article['extra']['Currency_Name']   = (string)$t->Tenders_Currency->attributes()->Currency_Name;
			
			$clientInfo = $t->Tenders_Client_Info->attributes(); 
			$article['extra']['client']['Client_ID'] = (int)$clientInfo->Client_ID; 
			$article['extra']['client']['Client_Name'] = (string)$clientInfo->Client_Name;
			$article['extra']['client']['Address'] = (string)$clientInfo->Address;
			$article['extra']['client']['Phone'] = (string)$clientInfo->Phone;
			$article['extra']['client']['Fax'] = (string)$clientInfo->Fax;
			$article['extra']['client']['Email'] = (string)$clientInfo->Email;
			$article['extra']['client']['Client_Division'] = (string)$clientInfo->Client_Division;
			
			
			foreach ($t->Tendes_Tenders_Sub_Sectors_View_ISI as $attribute) {
								
				$article['extra']['sectors'][] = array( 'sector'=> $this->sectors[(int)$attribute->attributes()->Sector_ID]  , 
														'sub_sector'=> $this->subSectors[ (int)$attribute->attributes()->Sub_Sector_ID ]['Sub_Sector_Name']); 
			}
						
			$articles[] = $article;					
		}
		return $articles;
	}
	
	
	protected function getStory(&$text) {
		$this->addLog('Getting article story');				
		
		$story = $this->textFixation($text['body']);
		
		$story .= "<br /><br />\n";
		
		$this->addToBody('Specs Fees', $text['extra']['Specs_Fees'], &$story);
		$this->addToBody('Bid Bond', $text['extra']['Bid_Bond'], $story);
		$this->addToBody('Currency Name', $text['extra']['Currency_Name'], $story);
		$this->addToBody('Country', $text['extra']['Country'], $story);
		$this->addToBody('Expiry Date', $text['extra']['Expiry_Date'], $story);		
		$this->addToBody('Performance Bond', $text['extra']['Performance_Bond'], $story);
		$this->addToBody('Location', $text['extra']['Location'], $story);
		$this->addToBody('Financing Name', $text['extra']['Financing_Name'], $story);
		
		
		$story .= "<br /><b>Client : </b><br /><br />\n";		
		$this->addToBody('Client Name', $text['extra']['client']['Client_Name'], $story);
		$this->addToBody('Address', $text['extra']['client']['Address'], $story);
		$this->addToBody('Phone', $text['extra']['client']['Phone'], $story);
		$this->addToBody('Fax', $text['extra']['client']['Fax'], $story);
		$this->addToBody('Email', $text['extra']['client']['Email'], $story);
		$this->addToBody('Client Division', $text['extra']['client']['Client_Division'], $story);
		
		if($text['extra']['sectors']) {
			$story .= "<br /><b>Sectors : </b><br /><br />\n";
			foreach ($text['extra']['sectors'] as $sector) {
				$story.= $sector['sector'] . '/' . $sector['sub_sector'] . "<br />\n";
			}
			
		}
		
		return $story;
	}
	
	
	protected function getHeadline(&$text) {
		$this->addLog('Getting article headline');
		return $this->textFixation($text['headline']);
	}
	
	protected function getArticleDate(&$text) {		
		$this->addLog('Getting article date');
		return $this->dateFormater(trim($this->textFixation($text['date'])));
	}
	
	public function getArticleOriginalId($params = array()) {
		return $this->title->getId() . '_' . $params['text']['original_id'];
	}

	
	
	
	protected function getSectors() {
		$sectors[54] = 'Electromechanical Works';
		$sectors[56] = 'Construction Projects';
		$sectors[57] = 'Agriculture & Food';
		$sectors[58] = 'Water & Waste-Water Equipment & Works';
		$sectors[59] = 'Energy';
		$sectors[60] = 'Petroleum - Oil, Gas & Petrochemicals';
		$sectors[61] = 'Industry';
		$sectors[64] = 'Information & Communications Technology';
		$sectors[65] = 'Environment-Related Works';
		$sectors[66] = 'Automotive & Construction Equipment';
		$sectors[103] = 'Mining';
		$sectors[108] = 'Economy';
		$sectors[111] = 'Transport';
		$sectors[124] = 'Consultancy';
		$sectors[129] = 'Management & Operation';
		$sectors[131] = 'Medical - Pharmaceuticals & Laboratory';
		
		return $sectors;
	}
	
	protected function getSubsectors() {
		
		$subSectors[165] = array('Sector_ID'=>54, 'Sub_Sector_Name' => 'Electromechanical Works');
		$subSectors[269] = array('Sector_ID'=>54, 'Sub_Sector_Name' => 'Swimming Pools');
		$subSectors[42] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Integrated works');
		$subSectors[43] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Tunnels');
		$subSectors[44] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Administrative Buildings');
		$subSectors[45] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Residential Buildings');
		$subSectors[46] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Bridges');
		$subSectors[47] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Hospitals');
		$subSectors[48] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Hotels & Tourist Development');
		$subSectors[49] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Cultural Buildings & Clubs');
		$subSectors[50] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Renovation, Treatment & Paints');
		$subSectors[51] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Steel Structure, Possible Prefab');
		$subSectors[203] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Schools & Other Educational Buildings');
		$subSectors[206] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Irrigation & Drainage Canals');
		$subSectors[251] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Parks & Landscaping');
		$subSectors[258] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Telephone-Exchange Buildings');
		$subSectors[267] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Roads Construction');
		$subSectors[279] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Dry Ports');
		$subSectors[280] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Airports');
		$subSectors[293] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Sea Ports');
		$subSectors[357] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Earthmoving Works');
		$subSectors[392] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'River Ports');
		$subSectors[460] = array('Sector_ID'=>56, 'Sub_Sector_Name' => 'Integrated Projects');
		$subSectors[52] = array('Sector_ID'=>57, 'Sub_Sector_Name' => 'Agricultural Machinery');
		$subSectors[53] = array('Sector_ID'=>57, 'Sub_Sector_Name' => 'Agricultural & Animal Production');
		$subSectors[55] = array('Sector_ID'=>57, 'Sub_Sector_Name' => 'Food');
		$subSectors[79] = array('Sector_ID'=>57, 'Sub_Sector_Name' => 'Fertilizers & Agro Chemicals');
		$subSectors[121] = array('Sector_ID'=>57, 'Sub_Sector_Name' => 'Veterinary Medicines');
		$subSectors[163] = array('Sector_ID'=>57, 'Sub_Sector_Name' => 'Poultry & Animal Wealth Development');
		$subSectors[283] = array('Sector_ID'=>57, 'Sub_Sector_Name' => 'Fish Wealth Development');
		$subSectors[342] = array('Sector_ID'=>57, 'Sub_Sector_Name' => 'Agricultural Waste Recycling');
		$subSectors[57] = array('Sector_ID'=>58, 'Sub_Sector_Name' => 'Potable Water & Waste-Water Pumps');
		$subSectors[59] = array('Sector_ID'=>58, 'Sub_Sector_Name' => 'Potable Water & Waste-Water Pipelines');
		$subSectors[61] = array('Sector_ID'=>58, 'Sub_Sector_Name' => 'Irrigation & Drainage Networks');
		$subSectors[94] = array('Sector_ID'=>58, 'Sub_Sector_Name' => 'Earthmoving Equipment');
		$subSectors[206] = array('Sector_ID'=>58, 'Sub_Sector_Name' => 'Irrigation & Drainage Canals');
		$subSectors[237] = array('Sector_ID'=>58, 'Sub_Sector_Name' => 'Water Meters');
		$subSectors[303] = array('Sector_ID'=>58, 'Sub_Sector_Name' => 'Irrigation & Drainage Pumping Stations');
		$subSectors[349] = array('Sector_ID'=>58, 'Sub_Sector_Name' => 'Water Desalination Stations');
		$subSectors[449] = array('Sector_ID'=>58, 'Sub_Sector_Name' => 'Water Wells Drilling');
		$subSectors[62] = array('Sector_ID'=>59, 'Sub_Sector_Name' => 'Generation');
		$subSectors[63] = array('Sector_ID'=>59, 'Sub_Sector_Name' => 'Transformation');
		$subSectors[64] = array('Sector_ID'=>59, 'Sub_Sector_Name' => 'Transmission');
		$subSectors[65] = array('Sector_ID'=>59, 'Sub_Sector_Name' => 'Distribution');
		$subSectors[67] = array('Sector_ID'=>59, 'Sub_Sector_Name' => 'Lighting Equipment & Works');
		$subSectors[68] = array('Sector_ID'=>59, 'Sub_Sector_Name' => 'Other Electrical Equipment & Works');
		$subSectors[298] = array('Sector_ID'=>59, 'Sub_Sector_Name' => 'Untraditional Energy');
		$subSectors[335] = array('Sector_ID'=>59, 'Sub_Sector_Name' => 'Marine Engines');
		$subSectors[346] = array('Sector_ID'=>59, 'Sub_Sector_Name' => 'Diesel Engines');
		$subSectors[355] = array('Sector_ID'=>59, 'Sub_Sector_Name' => 'Electric Motors');
		$subSectors[445] = array('Sector_ID'=>59, 'Sub_Sector_Name' => 'Automotive Engines');
		$subSectors[448] = array('Sector_ID'=>59, 'Sub_Sector_Name' => 'Digital Motors');
		$subSectors[459] = array('Sector_ID'=>59, 'Sub_Sector_Name' => 'Public');
		$subSectors[69] = array('Sector_ID'=>60, 'Sub_Sector_Name' => 'Development');
		$subSectors[70] = array('Sector_ID'=>60, 'Sub_Sector_Name' => 'Production');
		$subSectors[71] = array('Sector_ID'=>60, 'Sub_Sector_Name' => 'Treatment');
		$subSectors[72] = array('Sector_ID'=>60, 'Sub_Sector_Name' => 'Transport');
		$subSectors[74] = array('Sector_ID'=>60, 'Sub_Sector_Name' => 'Distribution');
		$subSectors[160] = array('Sector_ID'=>60, 'Sub_Sector_Name' => 'Petrochemicals');
		$subSectors[286] = array('Sector_ID'=>60, 'Sub_Sector_Name' => 'Petroleum - Oil, Gas');
		$subSectors[330] = array('Sector_ID'=>60, 'Sub_Sector_Name' => 'Liquefication');
		$subSectors[341] = array('Sector_ID'=>60, 'Sub_Sector_Name' => 'Natural Gas Distribution');
		$subSectors[75] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Electrical & Household Appliances');
		$subSectors[76] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Refrigeration & Air Conditioning');
		$subSectors[77] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Plastics & Derivatives');
		$subSectors[78] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Packaging & Wrapping');
		$subSectors[79] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Fertilizers & Agro Chemicals');
		$subSectors[80] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Paper');
		$subSectors[81] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Steel');
		$subSectors[122] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Metallic');
		$subSectors[124] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Refractories & Ceramics');
		$subSectors[125] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Chemicals');
		$subSectors[127] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Food & Beverage');
		$subSectors[160] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Petrochemicals');
		$subSectors[244] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Wood');
		$subSectors[246] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Workshop Equipment');
		$subSectors[247] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Safety Equipment');
		$subSectors[253] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Engineering');
		$subSectors[254] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Textile');
		$subSectors[266] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Vessels Building');
		$subSectors[272] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Construction Materials');
		$subSectors[273] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Electronics');
		$subSectors[275] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Cement');
		$subSectors[291] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Leather');
		$subSectors[296] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Sports Equipment');
		$subSectors[318] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Printing');
		$subSectors[320] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Meteorology Equipment');
		$subSectors[367] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Elevators');
		$subSectors[404] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Optical');
		$subSectors[451] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Electrical Feeder Industries');
		$subSectors[461] = array('Sector_ID'=>61, 'Sub_Sector_Name' => 'Pharmaceuticals');
		$subSectors[22] = array('Sector_ID'=>64, 'Sub_Sector_Name' => 'Telecommunication Equipment & Works');
		$subSectors[85] = array('Sector_ID'=>64, 'Sub_Sector_Name' => 'Computers, Hardware & Software');
		$subSectors[87] = array('Sector_ID'=>64, 'Sub_Sector_Name' => 'Automation & Process Control');
		$subSectors[88] = array('Sector_ID'=>64, 'Sub_Sector_Name' => 'Internet');
		$subSectors[89] = array('Sector_ID'=>64, 'Sub_Sector_Name' => 'E-Commerce');
		$subSectors[255] = array('Sector_ID'=>64, 'Sub_Sector_Name' => 'Other Office Equipment');
		$subSectors[256] = array('Sector_ID'=>64, 'Sub_Sector_Name' => 'Microfilm');
		$subSectors[261] = array('Sector_ID'=>64, 'Sub_Sector_Name' => 'Audiovisual Equipment');
		$subSectors[327] = array('Sector_ID'=>64, 'Sub_Sector_Name' => 'Media');
		$subSectors[428] = array('Sector_ID'=>64, 'Sub_Sector_Name' => 'Tele Education');
		$subSectors[429] = array('Sector_ID'=>64, 'Sub_Sector_Name' => 'Training');
		$subSectors[438] = array('Sector_ID'=>64, 'Sub_Sector_Name' => 'Tele Medicine');
		$subSectors[91] = array('Sector_ID'=>65, 'Sub_Sector_Name' => 'Environment-Related Works');
		$subSectors[92] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Passenger vehicles');
		$subSectors[93] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Cranes & Forklifts');
		$subSectors[94] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Earthmoving Equipment');
		$subSectors[95] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Rubber Tyres & Batteries');
		$subSectors[96] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Tractors');
		$subSectors[97] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Pickups & Trucks');
		$subSectors[98] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Buses, Minibuses & Microbuses');
		$subSectors[99] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Automotive Spare Parts');
		$subSectors[206] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Irrigation & Drainage Canals');
		$subSectors[209] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Ambulance Vehicles');
		$subSectors[250] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Fire Trucks & Rescue vehicles');
		$subSectors[262] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Motorcycles');
		$subSectors[319] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Maintenance & Repair');
		$subSectors[322] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Workshop Equipment');
		$subSectors[323] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Vessels & Motor Boats');
		$subSectors[357] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Earthmoving Works');
		$subSectors[378] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Traction Equipment');
		$subSectors[436] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Mobile Clinics');
		$subSectors[449] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Water Wells Drilling');
		$subSectors[453] = array('Sector_ID'=>66, 'Sub_Sector_Name' => 'Mobile Workshops');
		$subSectors[416] = array('Sector_ID'=>103, 'Sub_Sector_Name' => 'Mining');
		$subSectors[276] = array('Sector_ID'=>108, 'Sub_Sector_Name' => 'Economy');
		$subSectors[466] = array('Sector_ID'=>108, 'Sub_Sector_Name' => 'Banking, Finance & Insurance');
		$subSectors[281] = array('Sector_ID'=>111, 'Sub_Sector_Name' => 'Transport');
		$subSectors[282] = array('Sector_ID'=>111, 'Sub_Sector_Name' => 'Maritime Transport');
		$subSectors[289] = array('Sector_ID'=>111, 'Sub_Sector_Name' => 'Elevated Transport');
		$subSectors[292] = array('Sector_ID'=>111, 'Sub_Sector_Name' => 'Traction');
		$subSectors[313] = array('Sector_ID'=>111, 'Sub_Sector_Name' => 'Mass Transport');
		$subSectors[380] = array('Sector_ID'=>111, 'Sub_Sector_Name' => 'River Transport');
		$subSectors[442] = array('Sector_ID'=>111, 'Sub_Sector_Name' => 'Air Transport');
		$subSectors[365] = array('Sector_ID'=>123, 'Sub_Sector_Name' => 'Public');
		$subSectors[379] = array('Sector_ID'=>124, 'Sub_Sector_Name' => 'Marketing Consultancy');
		$subSectors[386] = array('Sector_ID'=>124, 'Sub_Sector_Name' => 'Engineering Consultancy');
		$subSectors[411] = array('Sector_ID'=>124, 'Sub_Sector_Name' => 'Technical Consultancy');
		$subSectors[413] = array('Sector_ID'=>124, 'Sub_Sector_Name' => 'Educational Consultancy');
		$subSectors[444] = array('Sector_ID'=>124, 'Sub_Sector_Name' => 'Management Consultancy');
		$subSectors[456] = array('Sector_ID'=>124, 'Sub_Sector_Name' => 'Training Consultancy');
		$subSectors[384] = array('Sector_ID'=>129, 'Sub_Sector_Name' => 'Car Parks');
		$subSectors[396] = array('Sector_ID'=>129, 'Sub_Sector_Name' => 'Hospitals');
		$subSectors[397] = array('Sector_ID'=>129, 'Sub_Sector_Name' => 'Hotels');
		$subSectors[398] = array('Sector_ID'=>129, 'Sub_Sector_Name' => 'Public Utilities');
		$subSectors[399] = array('Sector_ID'=>129, 'Sub_Sector_Name' => 'Cultural & Recreational Facilities');
		$subSectors[417] = array('Sector_ID'=>129, 'Sub_Sector_Name' => 'Solid Waste');
		$subSectors[420] = array('Sector_ID'=>129, 'Sub_Sector_Name' => 'Airports');
		$subSectors[47] = array('Sector_ID'=>131, 'Sub_Sector_Name' => 'Hospitals');
		$subSectors[82] = array('Sector_ID'=>131, 'Sub_Sector_Name' => 'Medical & Laboratory Consumables');
		$subSectors[83] = array('Sector_ID'=>131, 'Sub_Sector_Name' => 'Medical & Hospital Equipment');
		$subSectors[84] = array('Sector_ID'=>131, 'Sub_Sector_Name' => 'Laboratory & Scientific Equipment');
		$subSectors[121] = array('Sector_ID'=>131, 'Sub_Sector_Name' => 'Veterinary Medicines');
		$subSectors[457] = array('Sector_ID'=>131, 'Sub_Sector_Name' => 'Human Medicines');

		return $subSectors;
	}
	
	
	protected function getSectorsArr() {
		$sectors 	= $this->getSectors();
		$subsectors = $this->getSubsectors();
		
		if($subsectors) {
			foreach ($subsectors as $id => &$value) {
				$value['Sector_Name'] = $sectors[$value['Sector_ID']];
			}
		}
		return $subsectors;
	}	
	
	protected function addToBody($caption, $value, &$body) {
		$value = trim($value);
		
		if($value && $value != 'N/A') {
			$body .= "$caption : $value <br />\n"; 
		}	
	}


}
