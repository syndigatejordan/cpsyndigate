<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : 24.com  [# publisher id = 1131]
//Title      : Port Elizabeth Express [ English ] 
//Created on : Aug 30, 2017, 8:41:27 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3131 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}