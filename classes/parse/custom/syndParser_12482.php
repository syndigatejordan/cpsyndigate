<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : FM Estacion 99.5  [# publisher id = 1769]
//Title      : FM Station 99.5 [ Spanish ] 
//Created on : Feb 24, 2022, 9:02:33 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12482 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}