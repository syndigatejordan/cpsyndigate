<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Eurasianet  [# publisher id = 1281]
//Title      : Eurasianet [ Russian ] 
//Created on : Dec 11, 2019, 1:41:35 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5884 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ru');
  }
}
