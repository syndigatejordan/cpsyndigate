<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Mongolian National Chamber of Commerce and Industry  [# publisher id = 854]
//Title      : Mongolian National Chamber of Commerce and Industry (MNCCI) - News [ English ] 
//Created on : Feb 09, 2016, 12:04:58 PM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2612 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}