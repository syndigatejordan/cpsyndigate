<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Iranian Students News Agency (ISNA)  [# publisher id = 147]
//Title      : Iranian Students News Agency (ISNA) [ Arabic ] 
//Created on : Jan 31, 2016, 3:04:37 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2445 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}