<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : FRANCE 24 / AEF  [# publisher id =383 ]  
//Title     : FRANCE 24 [English]
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_2187 extends syndParseCms
{

    public function customInit()
    {
        parent::customInit();
        $this->charEncoding = 'UTF-8';
        $this->defaultLang = $this->model->getLanguageId('en');
    }

    protected function getVideos(&$text)
    {
        $this->addLog("Getting videos");
        $matches = $this->textFixation($this->getElementByName('fulltext', $text));
        $videoInfo = syndParseHelper::getImgElements($matches, 'iframe', 'src');
        $link = $videoInfo[0];
        $videoName = $this->getElementByName('title', $text);
        $videos = array();
        $video['video_name'] = $link;
        $video['original_name'] = $videoName;
        $video['video_caption'] = $videoName;
        $video['mime_type'] = "";
        $date = $this->getElementByName('date', $text);
        $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
        $videos[] = $video;
        return $videos;
    }

}
