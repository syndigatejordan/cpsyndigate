<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Finch Computing  [# publisher id = 1511]
//Title      : ESG feed [ English ] 
//Created on : Mar 18, 2022, 1:00:55 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12525 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}