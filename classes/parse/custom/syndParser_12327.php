<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : TheDriven.io  [# publisher id = 1722]
//Title      : The Driven [ English ] 
//Created on : Jan 11, 2022, 2:31:33 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12327 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}