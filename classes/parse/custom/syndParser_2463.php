<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Damascus Chamber of Commerce  [# publisher id = 805]
//Title      : Damascus Chamber of Commerce [ Arabic ] 
//Created on : Feb 02, 2016, 12:09:30 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2463 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}