<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Indian Chamber of Commerce  [# publisher id = 844]
//Title      : Indian Chamber of Commerce (ICC) - Press Releases [ English ] 
//Created on : Feb 08, 2016, 1:01:45 PM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2601 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}