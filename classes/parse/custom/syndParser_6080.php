<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : QUOI Media Group Inc.  [# publisher id = 1341]
//Title      : QUOI Media - Health [ English ] 
//Created on : Aug 04, 2021, 7:32:11 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6080 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}