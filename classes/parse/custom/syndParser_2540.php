<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Eltas Enterprises INC Company  [# publisher id = 820]
//Title      : The Maravi Post [ English ] 
//Created on : Sep 19, 2021, 7:03:58 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2540 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}