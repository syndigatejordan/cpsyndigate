<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: Corporate Publishing International / IDG  
	// Titles   : Network World Middle East
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_242 extends syndParseRss
	{
		public function customInit()
		{
			parent::customInit();
			$this->defaultLang  = $this->model->getLanguageId('en');
		}
		

		/**
		 * main parsing processing method
		 *
		 * @return array of articles
		 */
		public function parse()
		{
			$articles = parent::parse();

			$artCount = count($articles);
			for($i=0; $i<$artCount; $i++) {
				$articles[$i]['story'] = $articles[$i]['story']; 
			}

			return $articles;
		}

		public function getStory(&$text) {
			$this->addLog("getting article text");
		
			$story = $this->textFixation($this->getCData($this->getElementByName('content:encoded', $text)));
			$story = explode('<div class="feedflare">', $story);
			$story = strip_tags($story[0], '<p><br><BR><strong>'); 
			return $story;

		}
		
	}
