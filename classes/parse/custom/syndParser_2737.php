<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Groupe Eco-Media  [# publisher id = 941]
//Title      : L'Economiste [ French ] 
//Created on : May 19, 2016, 10:33:40 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2737 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}