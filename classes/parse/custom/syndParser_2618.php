<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Vietnam Business Forum Magazine - VCCI  [# publisher id = 858]
//Title      : Vietnam Chamber of Commerce and Industry (VCCI) - News [ English ] 
//Created on : Feb 11, 2016, 10:58:51 AM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2618 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}