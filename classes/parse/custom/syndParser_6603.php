<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : KABAR News Agency  [# publisher id = 1415]
//Title      : KABAR News Agency [ Russian ] 
//Created on : Oct 22, 2020, 10:32:34 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6603 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}