<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Romanian ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_588 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority'			=>	'Autoritatea de atribuire', 
										'Nature of Contract'			=> 	'Natura contractului', 
										'Regulation of Procurement' 	=> 	'Regulamentul de achiziţie', 
										'Type of bid required'			=>	'Tipul de suma licitată necesară', 
										'Award Criteria'				=>	'Criterii de atribuire', 
										'Original CPV'					=> 	'Original CPV', 
										'Country'						=>	'Ţară', 
										'Document Id' 					=>	'Id-ul document', 
										'Type of Document'				=>	'Tip de document', 
										'Procedure'						=>	'Procedura de', 
										'Original Language'				=>	'Original în limba', 
										'Current Language'				=> 	'Limba curent');	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('ro');
		}
	}