<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: Hilal Publishing and Marketing Group
	// Titles   : Middle East Interiors
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_194 extends syndParseAbTxtSample
	{
		public function customInit()
		{
			parent::customInit();
			$this->charEncoding = 'WINDOWS-1256';
			$this->defaultLang = $this->model->getLanguageId('en');
		}

		protected function getAuthor(&$text)
		{
			//get author
			$this->addLog("getting article author");
			$author = $this->textFixation($this->getItemValue('Dateline', $text));
			$this->removeItem('Dateline', $text);
			return $author;
		}
		
		
		protected function getStory(&$text)
		{
			return strip_tags(parent::getStory($text), '<br><BR>');
		}

	}