<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : JSC “Teleradiocompany LUX”  [# publisher id = 1603]
//Title      : 24 News Channel [ Russian ] 
//Created on : Aug 09, 2021, 12:27:08 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11948 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}