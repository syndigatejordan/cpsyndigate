<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Mongabay  [# publisher id = 1294]
//Title      : Mongabay [ simplified Chinese ] 
//Created on : Feb 10, 2020, 7:41:00 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5909 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('zh-Hans');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
