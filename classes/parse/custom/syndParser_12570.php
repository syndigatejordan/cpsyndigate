<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Euronews  [# publisher id = 604]
//Title      : Euronews [ Spanish ] 
//Created on : Apr 10, 2022, 8:41:19 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12570 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}