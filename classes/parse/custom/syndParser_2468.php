<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Egyptian Ministry of Trade & Industry  [# publisher id = 809]
//Title      : Egyptian International Trade Point - EITP News [ English ] 
//Created on : Feb 02, 2016, 12:10:07 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2468 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}