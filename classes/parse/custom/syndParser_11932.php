<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Infomedia / Opoint Technology  [# publisher id = 1534]
//Title      : TopicFeed: Coffee [ English ] 
//Created on : Sep 23, 2021, 12:32:11 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11932 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}