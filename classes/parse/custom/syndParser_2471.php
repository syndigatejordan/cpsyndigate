<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Amman Chamber Of Commerce  [# publisher id = 810]
//Title      : Amman Chamber Of Commerce (ACC) News [ English ] 
//Created on : Feb 02, 2016, 12:10:22 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2471 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}