<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Benzinga  [# publisher id = 1596]
//Title      : Benzinga España [ Spanish ] 
//Created on : Aug 02, 2022, 7:18:17 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12682 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}