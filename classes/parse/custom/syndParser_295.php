<?php

	////////////////////////////////////////////////////////////////////////
	// Publisher : India Today Group  
	// Titles    : INDIA TODAY TRAVEL PLUS
	////////////////////////////////////////////////////////////////////////

	class syndParser_295 extends syndParseXmlContent
	{
		public function customInit() {
			parent::customInit();
			$this->defaultLang 		= $this->model->getLanguageId('en');
			$this->charEncoding 	= 'ISO-8859-1';
			$this->extensionFilter 	= 'xml';
		}	
		
		protected function getRawArticles(&$fileContents) {			
			$this->addLog("getting articles raw text");
			
			// The articles are not separated, so we will spareate them manually
			$fileContents = explode('<section>', $fileContents);
			array_shift($fileContents);
			
			foreach ($fileContents as &$contents) {
				$contents = '<section>' . $contents;
			}
			
			return $fileContents;
		}
		
		protected function getHeadline(&$text) {
			
			$this->addLog('Getting article headline');
			return $this->textFixation($this->getElementByName('headline', $text));
		}
		
		protected function getStory(&$text) {			
			
			$this->addLog('Getting article story');
			return $this->textFixation($this->getElementByName('bodytext', $text));
		}
		
		protected function getOriginalCategory(&$text) {
			
			$this->addLog("getting article original category");
			return $this->textFixation($this->getElementByName('section', $text));
		}
		
		protected function getAbstract(&$text) {
			
			$this->addLog("getting article summary");
			return $this->textFixation($this->getElementByName('introtext', $text));
		}
		
		protected function getAuthor(&$text) {
			$this->addLog("getting article author");
			return $this->textFixation($this->getElementByName('byline', $text));
		}
		
		protected function getArticleDate(&$text) {
			
			$this->addLog("getting article date");
			return  $this->dateFormater(date('Y-m-d', strtotime($this->getElementByName('edition', $text))));
		}
		
	}
