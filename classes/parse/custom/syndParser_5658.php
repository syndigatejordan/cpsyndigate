<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BBC World Service  [# publisher id = 1192]
//Title      : BBC Bengali - News [ Bengali ] 
//Created on : Sep 14, 2021, 1:27:15 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5658 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('bn'); 
	} 
}