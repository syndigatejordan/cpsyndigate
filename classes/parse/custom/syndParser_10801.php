<?php
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : LRM 955 Radio Maria Juana FM 101.9  [# publisher id = 1563]
//Title      : Radio Maria Juana 101.9 [ Spanish ] 
//Created on : Apr 20, 2021, 8:19:03 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_10801 extends syndParseCms
{
    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('es');
    }
    protected function getVideos(&$text) {
        $this->addLog("Getting videos");
        $videosItem = array();
        $videos = array();
        $videosArray = array();
        $date = $this->getElementByName('date', $text);
        $date = date('Y-m-d h:i:s', strtotime($date));
        $story = $this->getCData($this->getElementByName('fulltext', $text));
        $story = str_replace("&amp;", "&", $story);
        $story = $this->textFixation($story);
        $video_Name = syndParseHelper::getImgElements($story, 'audio', 'src');
        $video_Name = $video_Name[0];
        $videoDescription = $this->textFixation($this->getElementByName('title', $text));
        $mimeType = "";
        $video['video_name'] = $video_Name;
        $video['original_name'] = $video_Name;
        $video['video_caption'] = $videoDescription;
        $video['mime_type'] = $mimeType;
        $video['added_time'] = $date;
        $videosItem[] = $video;
        return $videosItem;
    }
}
