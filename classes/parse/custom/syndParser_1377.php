<?php 
		///////////////////////////////////////////////////////////////////////////////////// 
		//Publisher : Agence Mauritanienne d'Information (AMI)  
		//Title     : Agence Mauritanienne d'Information (AMI) [ Arabic ] 
		/////////////////////////////////////////////////////////////////////////////////////
		
		class syndParser_1377 extends syndParseCms { 
			 public function customInit() { 
				 parent::customInit(); 
				 $this->defaultLang = $this->model->getLanguageId('ar'); 
			} 
		}