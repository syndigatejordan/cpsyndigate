<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : CountryWatch Inc.  [# publisher id =549 ] 
//Title     : CountryWatch Country Profiles [ English ]
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_1655 extends syndParseXmlContent {

  public $countryISO = array('Afghanistan' => 'AFG', 'Albania' => 'ALB', 'Algeria' => 'DZA', 'Andorra' => 'AND', 'Angola' => 'AGO', 'Antigua' => 'ATG', 'Argentina' => 'ARG', 'Armenia' => 'ARM', 'Australia' => 'AUS', 'Austria' => 'AUT', 'Azerbaijan' => 'AZE', 'Bahamas' => 'BHS', 'Bahrain' => 'BHR', 'Bangladesh' => 'BGD', 'Barbados' => 'BDS', 'Belarus' => 'BLR'
      , 'Belgium' => 'BEL', 'Belize' => 'BLZ', 'Benin' => 'BEN', 'Bhutan' => 'BTN', 'Bolivia' => 'BOL', 'Bosnia-Herzegovina' => 'BIH', 'Botswana' => 'BWA'
      , 'Brazil' => 'BRA', 'Brunei' => 'BRN', 'Bulgaria' => 'BGR', 'Burkina-Faso' => 'BFA', 'Burma' => 'MMR', 'Burundi' => 'BDI', 'Cambodia' => 'KHM'
      , 'Cameroon' => 'CMR', 'Canada' => 'CAN', 'Cape-Verde' => 'CPV', 'Central-African-Republic' => 'CAF', 'Chad' => 'TCD', 'Chile' => 'CHL', 'China' => 'CHN'
      , 'China-Hong-Kong' => 'HKG', 'China-Taiwan' => 'TWN', 'Colombia' => 'COL', 'Comoros' => 'COM', 'Congo-DRC' => 'COD', 'Congo-RC' => 'COG', 'Costa-Rica' => 'CRI'
      , 'Cote-d-Ivoire' => 'CIV', 'Croatia' => 'HRV', 'Cuba' => 'CUB', 'Cyprus' => 'CYP', 'Czech-Republic' => 'CZE', 'Denmark' => 'DNK', 'Djibouti' => 'DJI'
      , 'Dominica' => 'DMA', 'Dominican-Republic' => 'DOM', 'East-Timor' => 'TLS', 'Ecuador' => 'ECU', 'Egypt' => 'EGY', 'El-Salvador' => 'SLV', 'Equatorial-Guinea' => 'GNQ'
      , 'Eritrea' => 'ERI', 'Estonia' => 'EST', 'Ethiopia' => 'ETH', 'Fiji' => 'FJI', 'Finland' => 'FIN', 'Former-Yugoslav-Rep-of-Macedonia' => 'MKD', 'France' => 'FRA'
      , 'Gabon' => 'GAB', 'Gambia' => 'GMB', 'Georgia' => 'GEO', 'Germany' => 'DEU', 'Ghana' => 'GHA', 'Greece' => 'GRC', 'Grenada' => 'GRD', 'Guatemala' => 'GTM', 'Guinea' => 'GIN'
      , 'Guinea-Bissau' => 'GNB', 'Guyana' => 'GUY', 'Haiti' => 'HTI', 'Holy-See' => 'VAT', 'Honduras' => 'HND', 'Hungary' => 'HUN', 'Iceland' => 'ISL', 'India' => 'IND'
      , 'Indonesia' => 'IDN', 'Iran' => 'IRN', 'Iraq' => 'IRQ', 'Ireland' => 'IRL', 'Israel' => 'ISR', 'Italy' => 'ITA', 'Jamaica' => 'JAM', 'Japan' => 'JPN', 'Jordan' => 'JOR'
      , 'Kazakhstan' => 'KAZ', 'Kenya' => 'KEN', 'Kiribati' => 'KIR', 'Korea-North' => 'PRK', 'Korea-South' => 'KOR', 'Kuwait' => 'KWT', 'Kyrgyzstan' => 'KGZ', 'Laos' => 'LAO', 'Latvia' => 'LVA'
      , 'Lebanon' => 'LBN', 'Lesotho' => 'LSO', 'Liberia' => 'LBR', 'Libya' => 'LBY', 'Liechtenstein' => 'LIE', 'Lithuania' => 'LTU', 'Luxembourg' => 'LUX', 'Madagascar' => 'MDG'
      , 'Malawi' => 'MWI', 'Malaysia' => 'MYS', 'Maldives' => 'MDV', 'Mali' => 'MLI', 'Malta' => 'MLT', 'Malta' => 'MLT', 'Marshall-Islands' => 'MHL', 'Mauritania' => 'MRT', 'Mauritius' => 'MUS'
      , 'Mexico' => 'MEX', 'Micronesia' => 'FSM', 'Moldova' => 'MDA', 'Monaco' => 'MCO', 'Mongolia' => 'MNG', 'Morocco' => 'MAR', 'Mozambique' => 'MOZ', 'Namibia' => 'NAM', 'Nauru' => 'NRU'
      , 'Nepal' => 'NPL', 'Netherlands' => 'NLD', 'New-Zealand' => 'NZL', 'Nicaragua' => 'NIC', 'Niger' => 'NER', 'Nigeria' => 'NGA', 'Norway' => 'NOR', 'Oman' => 'OMN'
      , 'Pakistan' => 'PAK', 'Palau' => 'PLW', 'Panama' => 'PAN', 'Papua-New-Guinea' => 'PNG', 'Paraguay' => 'PRY', 'Peru' => 'PER', 'Philippines' => 'PHL', 'Poland' => 'POL'
      , 'Portugal' => 'PRT', 'Qatar' => 'QAT', 'Romania' => 'ROU', 'Russia' => 'RUS', 'Rwanda' => 'RWA', 'Saint-Kitts-and-Nevis' => 'KNA', 'Saint-Lucia' => 'LCA'
      , 'Saint-Vincent-and-Grenadines' => 'VCT', 'Samoa' => 'WSM', 'San-Marino' => 'SMR', 'Sao-Tome-and-Principe' => 'STP', 'Saudi-Arabia' => 'SAU', 'Senegal' => 'SEN'
      , 'Serbia' => 'SRB', 'Seychelles' => 'SYC', 'Sierra-Leone' => 'SLE', 'Singapore' => 'SGP', 'Slovakia' => 'SVK', 'Slovenia' => 'SVN', 'Solomon-Islands' => 'SLB'
      , 'Somalia' => 'SOM', 'South-Africa' => 'ZAF', 'Spain' => 'ESP', 'Sri-Lanka' => 'LKA', 'Sudan' => 'SDN', 'Suriname' => 'SUR', 'Swaziland' => 'SWZ', 'Sweden' => 'SWE'
      , 'Switzerland' => 'CHE', 'Syria' => 'SYR', 'Tajikistan' => 'TJK', 'Tanzania' => 'TZA', 'Thailand' => 'THA', 'Togo' => 'TGO', 'Tonga' => 'TON', 'Trinidad-Tobago' => 'TTO'
      , 'Tunisia' => 'TUN', 'Turkey' => 'TUR', 'Turkmenistan' => 'TKM', 'Tuvalu' => 'TUV', 'Uganda' => 'UGA', 'Ukraine' => 'UKR', 'United-Arab-Emirates' => 'ARE'
      , 'United-Kingdom' => 'GBR', 'United-States' => 'USA', 'Uruguay' => 'URY', 'Uzbekistan' => 'UZB', 'Vanuatu' => 'VUT', 'Venezuela' => 'VEN', 'Vietnam' => 'VNM', 'Yemen' => 'YEM', 'Zambia' => 'ZMB', 'Zimbabwe' => 'ZWE');
  public $contant;
  public $tag;
  public $imagesArray = array();

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    return $this->getElementsByName('string', $fileContents);
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $filename = basename($this->currentlyParsedFile);
    $filename = str_replace('.xml', '', $filename);
    $filename = array_search($filename, $this->countryISO);
    $headline = $filename . " Country Overview";
    return $headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = date('Y-m-d');
    return $date;
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = $this->getCData($this->getElementByName('string', $text));
    $this->story = html_entity_decode($this->story, ENT_COMPAT, 'UTF-8');
    $first_message = 'A transport-level error has occurred when receiving results from the server. (provider: TCP Provider, error: 0 - An existing connection was forcibly closed by the remote host.)';
    $second_message = 'The service has encountered an error processing your request. Please try again. Error code 40143.
A severe error occurred on the current command.  The results, if any, should be discarded.';
    $third_message = 'Connection Timeout Expired.  The timeout period elapsed during the post-login phase.  The connection could have timed out while waiting for server to complete the login process and respond; Or it could have timed out while attempting to create multiple active connections.  The duration spent while attempting to connect to this server was - [Pre-Login] initialization=1; handshake=10; [Login] initialization=0; authentication=0; [Post-Login] complete=14002;';
    if ((stripos($this->story, $first_message) !== false) || (stripos($this->story, $second_message) !== false) || (stripos($this->story, $third_message) !== false))
      return"";
    return $this->story;
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = "Country Overview";
    return $cats;
  }

}