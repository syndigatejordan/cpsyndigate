<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Groupe Wal Fadjri  [# publisher id = 969]
//Title      : Walfadjri [ French ] 
//Created on : May 22, 2016, 6:54:39 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2770 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}