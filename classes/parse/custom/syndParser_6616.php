<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Khaosan Pathet Lao (KPL)  [# publisher id = 1417]
//Title      : Lao News Agency (KPL) [ French ] 
//Created on : Oct 13, 2020, 9:00:09 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6616 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}