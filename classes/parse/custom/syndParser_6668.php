<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Turkish Government  [# publisher id = 148]
//Title      : Anadolu Agency (AA) [ Kashmiri ] 
//Created on : Oct 13, 2020, 9:37:35 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6668 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ks'); 
	} 
}