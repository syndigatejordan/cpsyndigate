<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Highlights for Children, Inc.  [# publisher id = 1333]
//Title      : News-O-Matic [ French ] 
//Created on : Aug 10, 2021, 11:55:31 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6025 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}