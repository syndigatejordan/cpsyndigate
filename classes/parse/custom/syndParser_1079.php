<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : LinkonLine
// Title     : Jawhara [ Arabic ]
///////////////////////////////////////////////////////////////////////////

class syndParser_1079 extends syndParseRss {

  protected $cat_translation;
  protected $story = null;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');

    /* $this->cat_translation['syahawsafar'] = 'سياحة وسفر';
      $this->cat_translation['amalwamwal'] = 'أعمال وأموال';
      $this->cat_translation['mawadeerahenah'] = 'المواضيع الراهنة';
      $this->cat_translation['decore'] = 'ديكور';
      $this->cat_translation['modawanetjawharah'] = 'مدونة جوهرة';
      $this->cat_translation['nessamolhamat'] = 'نساء ملهمات';
      $this->cat_translation['technology'] = 'تكنولوجيا';
      $this->cat_translation['jamalwseha'] = 'جمال وصحة';
      $this->cat_translation['modda'] = 'الموضة';
      $this->cat_translation['matbakhjawharah'] = 'مطبخ جوهرة';
      $this->cat_translation['hamelwomomah'] = 'الحمل والامومة';
      $this->cat_translation['alakat'] = 'علاقات';
      $this->cat_translation['tarfeeh'] = 'الترفيه'; */

    /* 	$this->cat_translation['jamal'] 		 = 'الجمال';
      $this->cat_translation['seha'] 			 = 'الصحة';
      $this->cat_translation['moda'] 			 = 'الموضة';
      $this->cat_translation['matbakjawharah'] = 'مطبخ جوهرة';
      $this->cat_translation['hamelwomomah'] 	 = 'الحمل والامومة';
      $this->cat_translation['alakat'] 		 = 'علاقات';
      $this->cat_translation['mashaheer'] 	 = 'مشاهير';
      $this->cat_translation['syahawsafar'] 	 = 'سياحة وسفر';
      $this->cat_translation['mojtama'] 		 = 'المجتمع';
      $this->cat_translation['nessamolhamat']  = 'نساء ملهمات';
      $this->cat_translation['manzlwdecore'] 	 = 'المنزل والديكور';
     */
    $this->cat_translation['jamal'] = 'الجمال';
    $this->cat_translation['seha'] = 'الصحة';
    $this->cat_translation['moda'] = 'الموضة';
    $this->cat_translation['matbakjawharah'] = 'مطبخ جوهرة';
    $this->cat_translation['hamelwomomah'] = 'الحمل والامومة';
    $this->cat_translation['alakat'] = 'علاقات';
    $this->cat_translation['mashaheer'] = 'مشاهير';
    $this->cat_translation['sowarvideo'] = 'صور وفيديو';
    $this->cat_translation['decorefonoon'] = 'ديكور وفنون';
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = $this->textFixation($this->getCData($this->getElementByName('description', $text)));
    $this->story = strip_tags($this->story, '<p><br><strong><b><u><i>');

    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");

    $date = $this->getElementByName('a10:updated', $text);
    $date = date('Y-m-d', strtotime($date));
    return $date;
  }

  public function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getCData($this->getElementByName('Name', $text)));
  }

  /*
    public function getAbstract(&$text) {
    $this -> addLog('Getting article summary');
    $abstract = $this -> textFixation($this -> getCData($this -> getElementByName('description', $text)));
    $link_pos = strpos($abstract, '<a');

    if ($link_pos) {
    return substr($abstract, 0, strpos($abstract, '<a'));
    } else {
    return $abstract;
    }

    }
   */

  public function getOriginalCategory(&$text) {

    $this->addLog('Getting article category');

    $filename = basename($this->currentlyParsedFile);
    $filename = str_replace('.xml', '', $filename);
    if (isset($this->cat_translation[$filename])) {
      return $this->cat_translation[$filename];
    } else {
      return false;
    }
  }

  public function getImages(&$text) {

    /* if ( empty( $this->story ) ) {
      $this->addLog('Will not get article images because the budy is empty');
      return array();
      } */

    $this->addLog('Getting article images');

    $imgs_array = array();

    //
    // Getting headline image from enclosure
    //

		$regExp = '<enclosure[\s]+.*url[\s]*=[\s]*([\"]([^>\"]*)[\"]|' . "[\\']([^>\\']*)[\\']|([^>\\s]*))([\\s]|[^>])*[\/]?>";
    preg_match_all("/$regExp/i", $text, $matches);

    if ($matches[2]) {
      $img = $matches[2][0];
    }
    if ($this->checkImageifCached($img)) {
      // Image already parsed..
      return;
    }
    if ($img) {
      $localImg = $this->copyUrlImgIfNotCached($img);
      if ($localImg) {
        $imginx = $this->getAndCopyImagesFromArray(array($localImg));
        if (!empty($imginx)) {
          $imgs_array[] = $imginx[0];
        }
      }
    }

    //
    // Getting embeded images in text
    //
		$imgs = syndParseHelper::getImgElements($text);
    if (!empty($imgs)) {
      foreach ($imgs as $img) {
        $localImg = $this->copyUrlImgIfNotCached($img);
        $localImg = $this->getAndCopyImagesFromArray(array($localImg));
        if (!empty($localImg)) {
          $localImg[0]['is_headline'] = 0;
          $imgs_array[] = $localImg[0];
        }
      }
    }

    return $imgs_array;
  }

  public function getArticleOriginalId($params = array()) {
    $defaultArray = array('text' => '', 'headline' => '', 'articleDate' => '', 'issueNum' => 0);
    $params = array_merge($defaultArray, $params);
    if ($params['issueNum']) {
      return sha1(trim($params['headline']) . $params['issueNum']);
    } else {
      return $this->title->getId() . '_' . sha1(trim($params['headline']) . $params['articleDate'] . $this->story);
    }
  }

}
