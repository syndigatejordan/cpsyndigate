<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Oman Establishment for Press, Publication and Advertising (OEPPA)  [# publisher id = 1071]
//Title      : Oman Daily [ Arabic ] 
//Created on : Apr 20, 2020, 5:46:08 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2983 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}