<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sun (Fiji) News Ltd  [# publisher id = 1060]
//Title      : Fiji Sun [ English ] 
//Created on : Sep 19, 2021, 7:07:05 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2954 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}