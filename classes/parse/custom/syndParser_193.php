<?php

////////////////////////////////////////////////////////////////////////
// Publisher : Al Bawaba (Middle East) Ltd.
// Titles    : Mena Report [ English ]
////////////////////////////////////////////////////////////////////////

class syndParser_193 extends syndParseXmlContent {

  private $contentType;

  const TYPE_NEWS = 'News';
  const TYPE_PROJECT = 'Project';
  const TYPE_CONTRACT = 'Contract';
  const TYPE_TENDER = 'Tender';

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getRawArticles(&$fileContents) {

    $filename = strtolower(basename($this->currentlyParsedFile));

    switch ($filename) {
      case (strpos($filename, 'news') !== false):
        $this->contentType = self::TYPE_NEWS;
        break;
      case (strpos($filename, 'projects') !== false):
        $this->contentType = self::TYPE_PROJECT;
        break;
      case (strpos($filename, 'contract') !== false):
        $this->contentType = self::TYPE_CONTRACT;
        break;
      case (strpos($filename, 'tenders') !== false):
        $this->contentType = self::TYPE_TENDER;
        break;
    }

    $this->addLog("getting raw articles text for the file " . $filename);
    $rows = $this->getElementsByName('Row', $fileContents);
    echo "content type is " . $this->contentType . "\n";
    echo count($rows) . " found (code in the parser) \n";
    return $rows;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");

    switch ($this->contentType) {
      case ($this->contentType == self::TYPE_NEWS) :
        $headline = 'news_title';
        break;
      case ($this->contentType == self::TYPE_PROJECT) :
        $headline = 'project_name';
        break;
      case ($this->contentType == self::TYPE_CONTRACT) :
        $headline = 'Contract_title';
        break;
      case ($this->contentType == self::TYPE_TENDER) :
        $headline = 'short_desc';
        break;
    }
    return $this->textFixation($this->getElementByName($headline, $text));
  }

  protected function getStory(&$text) {
      $this->addLog("getting article text");
      $functionName = "getStory" . $this->contentType;

      $story = $this->$functionName($text);
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $story = preg_replace('/<img[^>]+\>/i', '', $story);
      return $story;
      //return call_user_func('getStory' . $this->contentType, $text);
  }

  private function getStoryNews(&$text) {
    /* $story  = "<p>";
      $story .= $this->textFixation($this->getElementByName('news_details', $text));
      $story  = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "</p><p>", $story);
      $story .= "</p>"; */
    return $this->replaceEntity($this->textFixation($this->getElementByName('news_details', $text)));
  }

  private function getStoryProject(&$text) {

    $body = $this->replaceEntity($this->textFixation($this->getElementByName('project_details', $text)));

    $project_completion_date = trim($this->replaceEntity($this->textFixation($this->getElementByName('project_completion_date', $text))));
    $address = trim($this->replaceEntity($this->textFixation($this->getElementByName('add1', $text))));
    $email_id = trim($this->replaceEntity($this->textFixation($this->getElementByName('email_id', $text))));
    $url = trim($this->replaceEntity($this->textFixation($this->getElementByName('url', $text))));
    $financier = trim($this->replaceEntity($this->textFixation($this->getElementByName('financier', $text))));
    $financier_address = trim($this->replaceEntity($this->textFixation($this->getElementByName('financier_address', $text))));
    $maj_org = trim($this->replaceEntity($this->textFixation($this->getElementByName('maj_org', $text))));
    $other_contacts = trim($this->replaceEntity($this->textFixation($this->getElementByName('other_contacts', $text))));

    $countryCod = $this->replaceEntity($this->textFixation($this->getElementByName('country', $text)));
    $c = new Criteria();
    $c->add(CountryPeer::ISO, $countryCod, Criteria::EQUAL);
    $country = CountryPeer::doSelectOne($c);
    //
    if ($project_completion_date) {
      $body .= "<br /> Project completion date : $project_completion_date ";
    }
    if ($maj_org) {
      $body .= "<br /> Major organization : $maj_org \n";
    }
    if ($address) {
      $body .= "<br /> Address : $address \n";
    }
    if ($country) {
      $body .= "\n<br />" . "Country :" . $country->getPrintableName();
    }
    if ($email_id) {
      $body .= "<br /> Email : $email_id \n";
    }
    if ($url) {
      $body .= "<br /> Url : $url \n";
    }
    if ($financier) {
      $body .= "<br /> Financier : $financier \n";
    }
    if ($financier_address) {
      $body .= "<br />  Financier address : $financier_address \n";
    }
    if ($other_contacts) {
      $body .= "<br />  Contacts : $other_contacts \n";
    }

    return $body;
  }

  private function getStoryContract(&$text) {
    $body = $this->replaceEntity($this->textFixation($this->getElementByName('Contract_details', $text)));

    $contractorName = trim($this->replaceEntity($this->textFixation($this->getElementByName('contractors_name', $text))));
    $contractorAdd = trim($this->replaceEntity($this->textFixation($this->getElementByName('contractors_address', $text))));
    $financier = trim($this->replaceEntity($this->textFixation($this->getElementByName('financier', $text))));
    $implementing_agency = trim($this->replaceEntity($this->textFixation($this->getElementByName('implementing_agency', $text))));
    $email_id = trim($this->replaceEntity($this->textFixation($this->getElementByName('email_id', $text))));
    $url = trim($this->replaceEntity($this->textFixation($this->getElementByName('url', $text))));
    if ($contractorName) {
      $body .= "<br /> Contractor name : $contractorName \n";
    }
    if ($contractorAdd) {
      $body .= "<br /> Contractor address : $contractorAdd \n";
    }
    if ($financier) {
      $body .= "<br /> Financier : $financier \n";
    }
    if ($implementing_agency) {
      $body .= "<br /> Implementing agency : $implementing_agency \n";
    }

    $countryCod = $this->replaceEntity($this->textFixation($this->getElementByName('country', $text)));

    $c = new Criteria();
    $c->add(CountryPeer::ISO, $countryCod, Criteria::EQUAL);
    $country = CountryPeer::doSelectOne($c);

    if ($country) {
      $body .= "<br />" . "Country :" . $country->getPrintableName();
    }
    if ($email_id) {
      $body .= "<br /> Email : $email_id \n";
    }
    if ($url) {
      $body .= "<br /> Url : $url \n";
    }
    return $body;
  }

  private function getStoryTender(&$text) {

    $body = $this->replaceEntity($this->textFixation($this->getElementByName('tenders_details', $text)));
    $noticeNo = $this->replaceEntity($this->textFixation($this->getElementByName('noticeNo', $text)));

    $attachments_names = $this->textFixation($this->getElementByName('doc_file_name', $text));
    $attachments = $this->textFixation($this->getElementByName('tender_doc_file', $text));

    $maj_org = trim($this->replaceEntity($this->textFixation($this->getElementByName('maj_org', $text))));
    $address = trim($this->replaceEntity($this->textFixation($this->getElementByName('add1', $text))));
    $countryCod = $this->replaceEntity($this->textFixation($this->getElementByName('country', $text)));
    $email_id = trim($this->replaceEntity($this->textFixation($this->getElementByName('email_id', $text))));
    $url = trim($this->replaceEntity($this->textFixation($this->getElementByName('url', $text))));
    $address2 = trim($this->replaceEntity($this->textFixation($this->getElementByName('add2', $text))));
    $tender_notice_no = trim($this->replaceEntity($this->textFixation($this->getElementByName('tender_notice_no', $text))));
    $notice_type = trim($this->replaceEntity($this->textFixation($this->getElementByName('notice_type', $text))));
    $open_date = trim($this->replaceEntity($this->textFixation($this->getElementByName('open_date', $text))));
    $project_name = trim($this->replaceEntity($this->textFixation($this->getElementByName('project_name', $text))));


    if ($maj_org) {
      $body .= "<br /> Major organization : $maj_org \n";
    }
    if ($address) {
      $body .= "<br /> Address : $address \n";
    }


    $c = new Criteria();
    $c->add(CountryPeer::ISO, $countryCod, Criteria::EQUAL);
    $country = CountryPeer::doSelectOne($c);

    if ($country) {
      $body .= "<br />" . "Country :" . $country->getPrintableName();
    }
    if ($email_id) {
      $body .= "<br /> Email : $email_id \n";
    }
    if ($url) {
      $body .= "<br /> Url : $url \n";
    }
    if ($address2) {
      $body .= "<br /> Address : $address2 \n";
    }
    if ($tender_notice_no) {
      $body .= "<br />  Tender notice number : $tender_notice_no \n";
    }
    if ($notice_type) {
      $body .= "<br />  Notice type : $notice_type \n";
    }
    if ($open_date) {
      $body .= "<br />  Open date : $open_date \n";
    }
    if ($project_name) {
      $body .= "<br />  Project name : $project_name \n";
    }
    $attachments_links = array();
    if ($attachments_names) {
      $attachments_names = explode(',', $attachments_names);
      $attachments = explode(',', $attachments);

      foreach ($attachments_names as $index => $attachment_name) {
        $lnk = trim($attachments[$index]);
        $attachments_links[] = "<a href=\"$lnk\" >$attachment_name</a>";
      }
    }

    if ($noticeNo) {
      $body .= PHP_EOL . "<br />" . "NoticeNo : " . $noticeNo;
    }

    if (!empty($attachments_links)) {
      $body .= PHP_EOL . "<br /> Tender documents : " . implode(', ', $attachments_links);
    }
    //echo $body ; die();
    return $body;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName("date_c", $text);
    $date = $this->dateFormater($date);
    return $date;
  }

  protected function getIptcId(&$text) {
    $this->addLog("getting category name");
    $originalCategoryName = $this->getElementByName('sector', $text);
    $iptcId = $this->model->getIptcCategoryId($originalCategoryName);

    if (!$iptcId) {
      return parent::getIptcId($text);
    }
    return $iptcId;
  }

  protected function getOriginalCategory(&$text) {

    $cat = $this->textFixation($this->getElementByName('sector', $text));
    $cat = preg_replace('/[0-9\\s]/i', '', $cat);
    return $cat;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $auth = $this->textFixation($this->getElementByName('entryby', $text));
    return $auth ? $auth : '';
  }

  public function getArticleOriginalId($params = array()) {
    $this->addLog("getting article orignial Id");
    //$articleOriginalId = $this->getElementByName("posting_id", $params['text']);
    if (!$articleOriginalId = (int) trim($this->getElementByName('posting_id', $params['text']))) {
      return parent::getArticleOriginalId($params);
    }
    // do not check dublication between this and the other titles
    return $this->title->getId() . "_" . date('Ymd') . "_" . $articleOriginalId;
  }

}
