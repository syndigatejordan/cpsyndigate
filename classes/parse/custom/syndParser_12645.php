<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : LVS Media Group  [# publisher id = 1813]
//Title      : La Vie Senegalaise [ French ] 
//Created on : Jun 16, 2022, 1:44:58 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12645 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}