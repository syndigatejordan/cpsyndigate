<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : African Press Organization (APO)  [# publisher id = 448]
//Title      : African Press Organization (APO) [ English ] 
//Created on : Oct 05, 2021, 7:57:35 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1405 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}