<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Qatar Information and Marketing  [# publisher id = 20]
//Title      : Al Watan [ Arabic ] 
//Created on : Feb 11, 2019, 12:30:46 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_24 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}