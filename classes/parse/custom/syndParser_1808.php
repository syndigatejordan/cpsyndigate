<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Newsprint Namibia  [# publisher id = 637]
//Title      : Namibian Sun [ English ] 
//Created on : Aug 10, 2021, 10:12:34 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1808 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}