<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dubai Chamber of Commerce and Industry  [# publisher id = 790]
//Title      : Dubai Chamber of Commerce and Industry News [ Arabic ] 
//Created on : Feb 02, 2016, 11:45:16 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2438 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}