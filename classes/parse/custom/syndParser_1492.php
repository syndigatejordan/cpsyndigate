<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Jerusalem Post  [# publisher id =497 ]
//Title      : Jpost.com (The Jerusalem Post online edition) [ English ] 
//Created on : Nov 20, 2019, 10:40:56 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1492 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

}
