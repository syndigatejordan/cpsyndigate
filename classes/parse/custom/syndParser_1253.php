<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : FooDiva FZ LLE  [# publisher id = 375]
//Title      : FooDiva [ English ] 
//Created on : Jul 28, 2021, 1:04:29 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1253 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}