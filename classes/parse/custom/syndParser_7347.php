<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : 4Semanas.com  [# publisher id = 1464]
//Title      : 4SEMANAS.COM [ Spanish ] 
//Created on : Sep 19, 2021, 10:18:08 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7347 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}