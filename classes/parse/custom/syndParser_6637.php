<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : MCOT Public Company Limited  [# publisher id = 1422]
//Title      : MCOT - Mass Communication Organization of Thailand [ Thai ] 
//Created on : Oct 15, 2020, 10:29:17 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6637 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('th'); 
	} 
}