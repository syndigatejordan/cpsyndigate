<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Volkov Law Group  [# publisher id = 1732]
//Title      : Corruption, Crime & Compliance (Volkov Law Group) [ English ] 
//Created on : Jan 16, 2022, 11:57:01 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12333 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}