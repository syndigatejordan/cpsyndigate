<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence Guinéenne de Presse (AGP)  [# publisher id = 1635]
//Title      : AGP Guinée [ French ] 
//Created on : Aug 31, 2022, 6:21:49 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12185 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}