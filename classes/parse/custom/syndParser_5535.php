<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Associated Newspapers UK Ltd.  [# publisher id = 605]
//Title      : The Daily Mail (MailOnline) - Sports [ English ] 
//Created on : Sep 15, 2019, 6:30:26 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5535 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('NewsML', $fileContents);
  }

  protected function getStory(&$text) {
      //get body text
      $this->addLog("getting article text");
      $story = $this->getElementByName('body', $text);
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $story = preg_replace('/<img[^>]+\>/i', '', $story);
      return $story;
  }

  protected function getOriginalCategory(&$text) {

    $this->addLog("getting article Category");
    $Categorys = $this->getElementsByName('KeywordLine', $text);
    $Categorys = explode(";", $Categorys[0]);
    return $Categorys[1] . "," . $Categorys[0];
  }

  protected function getHeadline(&$text) {
    //get headline
    $this->addLog("getting article headline");
    $item=$this->getElementsByName('NewsLines', $text);
    $item=  explode("</HeadLine>", $item[0]);
    $headline=  explode("<HeadLine>", $item[0]);
    return $headline[1];
  }

  protected function getArticleDate(&$text) {
    //get article date
    $this->addLog("getting article date");
    $date = $this->getElementsByName('ThisRevisionCreated', $text);
    $date = trim($date[0], "Z");
    return date("Y-m-d", strtotime($date));
  }

  protected function getAbstract(&$text) {
    //get abstract
    $this->addLog("getting article abstract");
    $summray = $this->textFixation($this->getElementByName('NewsLineText', $text));
    return $summray;
  }

  protected function getAuthor(&$text) {
    //get author
    $this->addLog("getting article author");
    $author = $this->getElementsByName('KeywordLine', $text);
    $author = explode(";", $author[0]);
    return $author[3];
  }
}
