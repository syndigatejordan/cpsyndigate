<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : MOLDPRES  [# publisher id = 1438]
//Title      : MOLDPRES [ Romanian ] 
//Created on : Oct 18, 2020, 8:06:00 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6722 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ro'); 
	} 
}