<?php
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dzayer info  [# publisher id = 1544]
//Title      : Dzayer info [ Arabic ] 
//Created on : Mar 16, 2021, 2:43:58 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_10347 extends syndParseRss
{

    public $author;

    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('ar');
    }

    public function getRawArticles(&$fileContents)
    {
        //get articles
        $this->addLog("getting articles raw text");
        $articles = $this->getElementsByName('item', $fileContents);
        return $articles;
    }

    public function getOriginalCategory(&$text)
    {
        $this->addLog('getting article category');
        $cats = $this->getElementsByName('category', $text);
        $originalCats = array();

        if (!empty($cats)) {
            foreach ($cats as $cat) {
                $originalCats[] = $this->textFixation($this->getCData($cat));
            }
        }
        return implode(', ', $originalCats);
    }

    protected function getHeadline(&$text)
    {
        $this->addLog("getting article headline");
        $headline = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
        return $headline;
    }

    protected function getArticleDate(&$text)
    {
        $this->addLog("getting article date");

        $date = trim(preg_replace('/\+(.*)/is', '', $this->getElementByName('pubDate', $text)));
        return date('Y-m-d', strtotime($date));
    }

    protected function getStory(&$text)
    {
        $this->addLog("getting article text");
        $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) srcset=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) sizes=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) width=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) height=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) link_thumbnail=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('!\s+!', ' ', $this->story);
        return $this->story;
    }

    protected function getAuthor(&$text)
    {
        $this->addLog("getting article author");
        $this->author = $this->textFixation($this->getCData($this->getElementByName('dc:creator', $text)));
        return $this->author;
    }

    protected function getImages(&$text)
    {
        $imagesArray = array();
        $this->story = str_replace('&nbsp;', '', $this->getElementByName('content:encoded', $text));
        $this->story = $this->textFixation($this->getCData($this->story));
        //This action depends on "Task #1064 Remove all the images from our clients feeds"
        $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
        return array();

        $imgs = NULL;
        preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
        foreach ($imgs[0] as $img) {
            $this->addLog("getting article images");
            $image_caption = '';
            $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
            if (!empty($image_caption[0])) {
                $image_caption = $image_caption[0];
            } else {
                $image_caption = syndParseHelper::getImgElements($img, 'img', 'title');
                if (!empty($image_caption[0])) {
                    $image_caption = $image_caption[0];
                } else {
                    $image_caption = "";
                }
            }
            $imageInfo = syndParseHelper::getImgElements($img, 'img');
            $imagePath = $imageInfo[0];
            if (!$imagePath) {
                continue;
            }
            if ($this->checkImageifCached($imagePath)) {
                // Image already parsed..
                continue;
            }
            $imagePath = str_replace(' ', '%20', $imagePath);
            $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
            if (!$copiedImage) {
                echo "no pahr";
                continue;
            }
            $images = $this->getAndCopyImagesFromArray(array($copiedImage));
            if (!empty($image_caption)) {
                $images[0]['image_caption'] = $image_caption;
            }
            $name_image = explode('/images/', $copiedImage);
            if ($images[0]['image_caption'] == $name_image[1]) {
                $images[0]['image_caption'] = '';
            }
            $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
            $images[0]['is_headline'] = false;
            $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
            $this->story = str_replace($img, $new_img, $this->story);
            array_push($imagesArray, $images[0]);
        }
        return $imagesArray;
    }

}
