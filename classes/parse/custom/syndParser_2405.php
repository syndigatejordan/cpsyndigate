<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Cairo Chamber of Commerce  [# publisher id =770 ]   
//Title     : Cairo Chamber of Commerce News [ English ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2405 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

}
