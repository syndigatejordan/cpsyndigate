<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Millas Comunicações  [# publisher id = 1579]
//Title      : MS NEWS [ Portuguese ] 
//Created on : Apr 26, 2022, 8:04:56 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10837 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}