<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Alpha Comunicaciones Integrales  [# publisher id = 1485]
//Title      : Bella Vista News [ Spanish ] 
//Created on : Mar 27, 2022, 9:26:14 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7427 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}