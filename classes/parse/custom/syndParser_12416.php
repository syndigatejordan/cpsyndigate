<?php
/////////////////////////////////////////////////////////////////////////////////////
//Publisher  : Gannett Co., Inc.  [# publisher id = 1763]
//Title      : Newsquest (News Portfolio) [ English ]
//Created on : Mar 02, 2022, 8:56:30 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_12416 extends syndParseXmlContent
{

    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('en');
        $this->extensionFilter = 'xml';
    }

    public function getHeadline(&$text)
    {
        $this->addLog("getting article headline");
        return trim($this->textFixation($this->getElementByName('headline', $text)));
    }

    public function getArticleDate(&$text)
    {
        $this->addLog("getting article date");
        $date = trim($this->getElementByName('published_date', $text));
        if ($date) {
            return $this->dateFormater($date);
        }
        return parent::getArticleDate($text);
    }

    public function getOriginalCategory(&$text)
    {
        $this->addLog('getting article category');
        return trim(strip_tags($this->textFixation($this->getElementByName('section', $text))));
    }

    public function getArticleOriginalId($params = array())
    {
        $articleOriginalId = $this->getElementByName('article_id', $params['text']);

        if (!$articleOriginalId) {
            return parent::getArticleOriginalId($params);
        }

        return $this->title->getId() . '_' . sha1($articleOriginalId);
    }

    public function getStory(&$text)
    {
        $this->addLog('Getting article story');
        $story = $this->textFixation($this->getCData($this->getElementByName('body', $text)));
        $story = preg_replace('/<img[^>]+\>/i', '', $story);
        $story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $story);
        $story = preg_replace('/(<[^>]+) width=".*?"/i', '$1', $story);
        $story = preg_replace('/(<[^>]+) height=".*?"/i', '$1', $story);
        $story = preg_replace('/(<[^>]+) align=".*?"/i', '$1', $story);
        $story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $story);
        $story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $story);
        $story = preg_replace('!\s+!', ' ', $story);
        if (empty($story)) {
            return '';
        }
        $publication_name = trim(strip_tags($this->textFixation($this->getElementByName('publication_name', $text))));
        $story = "$story <p>$publication_name</p>";
        return $story;
    }

    public function getArticleReference(&$text)
    {
        $this->addLog('Getting article Link');
        $link = trim($this->textFixation($this->getElementByName('origin_uri', $text)));
        if (empty($link)) {
            return '';
        }
        return $link;
    }

    protected function getRawArticles(&$fileContents)
    {
        //get articles
        $this->addLog("getting articles raw text");
        return $this->getElementsByName('article', $fileContents);
    }

    protected function getAbstract(&$text)
    {
        $this->addLog('getting article summary');
        return trim($this->textFixation($this->getElementByName('SubHeadLine', $text)));
    }

    protected function getAuthor(&$text)
    {
        $this->addLog("getting article author");
        return trim(strip_tags($this->textFixation($this->getElementByName('byline', $text))));
    }
}

