<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Editora 29 de maio Ltda.  [# publisher id = 1580]
//Title      : City Journal [ Portuguese ] 
//Created on : Sep 19, 2021, 10:24:52 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10838 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}