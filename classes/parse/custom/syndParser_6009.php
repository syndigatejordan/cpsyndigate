<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bloomberg L.P.  [# publisher id = 1202]
//Title      : Bloomberg Politics & Policy [ English ] 
//Created on : Aug 04, 2021, 12:09:34 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6009 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}