<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Business Wire, Inc. / Berkshire Hathaway  [# publisher id = 1251]
//Title      : Business Wire [ English ] 
//Created on : Jun 30, 2019, 8:56:21 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5646 extends syndParseXmlContent {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    $fileContents = preg_replace('!\s+!', ' ', $fileContents);
    $articles[] = $fileContents;
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->getContant($text, "HeadLine");
    if (is_array($headline)) {
      $headline = $headline[0];
      return trim(strip_tags($this->getCData($this->getElementByName('DataContent', $headline))));
    } else {
      return "";
    }
  }

  protected function getAbstract(&$text) {
    $this->addLog("getting article abstract");
    $abstract = $this->getContant($text, "SubHeadLine");
    if (is_array($abstract)) {
      $abstract = $abstract[0];
      return trim(strip_tags($this->getCData($this->getElementByName('DataContent', $abstract))));
    } else {
      return "";
    }
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $author = $this->getCData($this->getElementByName('ProviderId', $text));
    return $this->textFixation($author);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = trim(preg_replace('/\+(.*)/is', '', $this->getElementByName('DateAndTime', $text)));
    return date('Y-m-d', strtotime($date));
  }

  protected function getOriginalCategory(&$text) {
    $this->addLog("getting article category");
    $matches = null;
    if (preg_match_all("/<Property FormalName=\"BWCategoryKeywords\" Value=\"(.*?)\"\/>/", $text, $matches)) {
      return implode(",", $matches[1]);
    } else {
      return "";
    }
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    return $this->story;
  }

  protected function getImages(&$text) {
    $this->imagesArray = array();
    $this->story = $this->getContant($text, "Body");
    if (is_array($this->story)) {
      $this->story = $this->getCData($this->getElementByName('DataContent', $this->story[0]));
      $imgs = NULL;
      preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
      foreach ($imgs[0] as $img) {
        $this->addLog("getting article images");
        $image_caption = '';
        $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
        if (!empty($image_caption[0])) {
          $image_caption = $image_caption[0];
        } else {
          $image_caption = syndParseHelper::getImgElements($img, 'img', 'title');
          if (!empty($image_caption[0])) {
            $image_caption = $image_caption[0];
          } else {
            $image_caption = "";
          }
        }
        $imageInfo = syndParseHelper::getImgElements($img, 'img');
        $imagePath = $imageInfo[0];
        if (!$imagePath) {
          continue;
        }
        if ($this->checkImageifCached($imagePath)) {
          // Image already parsed..
          continue;
        }
        $imagePath = str_replace(' ', '%20', $imagePath);
        $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
        if (!$copiedImage) {
          echo "no pahr";
          continue;
        }
        $images = $this->getAndCopyImagesFromArray(array($copiedImage));
        if (!empty($image_caption)) {
          $images[0]['image_caption'] = $image_caption;
        }
        $name_image = explode('/images/', $copiedImage);
        if ($images[0]['image_caption'] == $name_image[1]) {
          $images[0]['image_caption'] = '';
        }
        $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
        $images[0]['is_headline'] = false;
        $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
        $this->story = str_replace($img, $new_img, $this->story);
        array_push($this->imagesArray, $images[0]);
      }
    } else {
      $this->story = "";
    }
    $Photo = $this->getContant($text, "Photo");
    $Photo = implode(" ", $Photo);
    $imgs = array();
    preg_match_all("/<ContentItem Duid=\"(.*?)\" Href=\"(.*?)\">/is", $Photo, $imgs);
    foreach ($imgs[2] as $imagePath) {
      $this->addLog("getting article images");
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no path" . PHP_EOL;
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = sha1($imageUrl) . ".jpeg";
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $curl_handle = curl_init();
      curl_setopt($curl_handle, CURLOPT_URL, $imageUrl);
      curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl_handle, CURLOPT_USERAGENT, 'php');
      $imgContent = curl_exec($curl_handle);
      curl_close($curl_handle);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

  public function getArticleOriginalId($params = array()) {
    $articleOriginalId = $this->getElementByName('NewsItemId', $params['text']);
    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }
    return $this->title->getId() . '_' . $articleOriginalId;
  }

  public function getContant($text, $role) {
    $matches = null;
    $pattern = "/<NewsComponent>(.*?)<Role FormalName=\"$role\"\/>(.*?)<\/NewsComponent>/";
    if (preg_match_all($pattern, $text, $matches)) {
      return $matches[2];
    } else {
      return "";
    }
  }

}
