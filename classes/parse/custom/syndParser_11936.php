<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Hearst Newspapers  [# publisher id = 1600]
//Title      : CHRON (Houston Chronicle) [ English ] 
//Created on : Jul 04, 2021, 12:38:34 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11936 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}