<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Global Crypto Press Association  [# publisher id = 1834]
//Title      : The Global Crypto Press Association [ German ] 
//Created on : Aug 07, 2022, 11:26:07 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12694 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('de'); 
	} 
}