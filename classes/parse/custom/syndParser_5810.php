<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : RASANAH - International Institute for Iranian Studies  [# publisher id = 1264]
//Title      : RASANAH - Iranian Issues Series [ English ] 
//Created on : Sep 01, 2020, 8:58:38 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5810 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}