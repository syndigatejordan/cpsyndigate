<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bulgarian News Agency (BTA)  [# publisher id = 1435]
//Title      : Bulgarian News Agency (BTA) [ bulgarian ] 
//Created on : Sep 23, 2021, 8:09:24 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6706 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('bg'); 
	} 
}