<?php

///////////////////////////////////////////////////////////////////////////
//Publisher : WENN Media Group  [# publisher id =1189 ] 
//Titles    : WENN Footage  [ English ]
//Created on : Mar 03, 2019, 08:48:41 AM
//Author     : eyad
///////////////////////////////////////////////////////////////////////////

class syndParser_3743 extends syndParseXmlContent {

  // Available video types
  private $videoTypes = array('mov' => '.mov', 'mp4' => '.mp4');

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xmp';
    require_once('/usr/local/syndigate/lib/getid3/getid3/getid3.php');
  }

  protected function getRawArticles(&$fileContents) {
    $this->loadCurrentDirectory();
    $this->addLog("getting raw articles text");
    $articles[] = $fileContents;
    return $articles;
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = $this->textFixation($this->getElementByName('dc:description', $text));
    $story = strip_tags($story);
    $story = trim(preg_replace('!\s+!', ' ', $story));
    $story = preg_replace('/[\n\r]/', "</p><p>", $story);
    if (!empty($story)) {
      $story = "<p>$story</p>";
    }
    return $story;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return $this->textFixation($this->getElementByName('photoshop:Headline', $text));
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('photoshop:DateCreated', $text);
    $date = trim(preg_replace('/\+(.*)/is', '', $date));
    return date('Y-m-d', strtotime($date));
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return trim($this->textFixation($this->getElementByName('photoshop:Source', $text)));
  }

  protected function getImages(&$text) {
    $this->addLog("getting article image");

    //$imageName = $this->currentDir . $this->videoName . '.jpg';
    $image_Name = $this->textFixation($this->getElementByName('photoshop:TransmissionReference', $text)) . ".jpg";
    $image_full_path = $this->currentDir . $image_Name;
    $original_name = explode('.', $image_Name);
    if (file_exists($image_full_path)) {
      //the video name is same as the image name here  		
      $name = $this->model->getImgReplacement($image_Name, $this->currentDir, 3743);
      $img['img_name'] = str_replace(IMGS_PATH, "https://syndigateimages.s3.amazonaws.com/syndigate/imgs/", $name);
      $img['original_name'] = $original_name[0];
      $img['image_caption'] = $image_Name;
      $img['is_headline'] = true;
      $images[] = $img;
      return $images;
    } else {

      return false;
    }
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $category = trim($this->textFixation($this->getElementByName('dc:subject', $text)));
    $cats = $this->getElementsByName('rdf:li', $category);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = $this->textFixation($this->getCData($cat));
      }
    }
    return implode(', ', $originalCats);
  }

  public function getArticleOriginalId($params = array()) {
    $articleOriginalId = $this->getElementByName('photoshop:TransmissionReference', $params['text']);

    $this->articleOriginalId = $articleOriginalId;
    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }
    return $this->title->getId() . "_" . sha1($articleOriginalId) . "_" . $articleOriginalId;
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $video_Name = basename($this->currentlyParsedFile);
    $video_Name=  preg_replace("/\.(.*)/is","",$video_Name);
    $exec = "find $this->currentDir -name \"$video_Name*\"";
    $comm = shell_exec($exec);
    $comm = explode($this->currentDir, $comm);
    $videostype = array();
    foreach ($comm as $com) {
      $com = explode('.', $com);

      //var_dump($com[1]);
      if (isset($com[1]) && $com[1] != "xmp") {
        $com[1] = trim($com[1]);
        if (!empty($com[1])) {
          if (in_array('.' . $com[1], $this->videoTypes)) {
            $videostype[] = $com[1];
          }
        }
      }
    }
    $video_Name = $video_Name . "." . $videostype[0];
    $videoName = $this->currentDir . $video_Name;
    $video_Name_arr = explode('.', $video_Name);
    $videos = array();
    $videoInformation = array();
    $neededInfo = array('filesize', 'fileformat', 'encoding', 'mime_type', 'playtime_seconds', 'bitrate', 'playtime_string', 'video');
    //foreach ($this->videoTypes as $videoLable => $videoType) {
    var_dump($videoName);
    if (in_array('.' . $videostype[0], $this->videoTypes)) {
      if (file_exists($videoName)) {

        var_dump($videoName);
        $getID3 = new getID3;
        $realVideoPath = str_replace($this->title->getParseDir(), $this->title->getHomeDir(), $videoName);
        $videoInfo = $getID3->analyze($realVideoPath);

        foreach ($neededInfo as $key => $value) {

          if (in_array($value, $videoInfo)) {
            switch ($value) {
              case 'video':
                $videoInformation['width'] = $videoInfo[$value]['resolution_x'];
                $videoInformation['height'] = $videoInfo[$value]['resolution_y'];
                break;
              case 'bitrate':
                $videoInformation[$value] = floor($videoInfo[$value] / 1024);
                break;

              default:
                $videoInformation[$value] = $videoInfo[$value];
                break;
            }
          }
        }


        //$name = $this->model->getVidReplacement($this->videoName . $videoType, $this->currentDir, 238) ;
        //$video['video_name']    	= str_replace(VIDEOS_PATH, VIDEOS_HOST, $name);

        $video['video_name'] = $this->getAndCopyVideosFromArray($videoName);

        //$bitRate 					= $videoInfo['bitrate'] / 1000;
        $mimeType = $videoInfo['mime_type'];
        $video['original_name'] = $videoName;
        $video['video_caption'] = $this->textFixation($this->getElementByName('photoshop:Headline', $text));
        //$video['bit_rate']       	= $this->getBitRateFromName($videoLable);
        $video['bit_rate'] = json_encode($videoInformation);    //$bitRate . ' kbps';
        $video['mime_type'] = $mimeType;
        $video['added_time'] = date('Y-m-d h:i:s', filemtime($video['original_name']));
        $videos[] = $video;
      }
    }
    return $videos;
  }

  private function getBitRateFromName($name) {
    $bit = explode("(", $name);

    if (is_array($bit) & count($bit) > 1) {
      $bit = $bit[1];
      $bit = explode(")", $bit);
      return $bit[0];
    } else {
      return '';
    }
  }

}

?>
