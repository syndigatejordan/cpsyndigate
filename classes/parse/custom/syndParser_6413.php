<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : InSight Crime  [# publisher id = 1355]
//Title      : InSight Crime [ Spanish ] 
//Created on : Sep 19, 2021, 8:54:22 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6413 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}