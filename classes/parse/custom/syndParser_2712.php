<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Mada Est. For Mass Media, Culture and Arts  [# publisher id = 918]
//Title      : Al Mada [ Arabic ] 
//Created on : Jun 23, 2019, 7:03:53 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2712 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}