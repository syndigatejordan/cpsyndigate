<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Infos Plus Gabon  [# publisher id = 1160]
//Title      : Infos Plus Gabon [ Arabic ] 
//Created on : Sep 26, 2021, 12:39:37 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3176 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}