<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Standard Group  [# publisher id = 1155]
//Title      : The Nairobian [ English ] 
//Created on : May 19, 2022, 7:54:47 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12398 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}