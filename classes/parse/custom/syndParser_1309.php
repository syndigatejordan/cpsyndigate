<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : 	 M. Lynx Qualey  [# publisher id =400 ]
// Titles    :   ArabLit [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1309 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
