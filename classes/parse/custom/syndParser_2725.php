<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : El Khabar  [# publisher id = 930]
//Title      : Elkhabar Erriadhi [ Arabic ] 
//Created on : Dec 18, 2017, 1:45:00 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2725 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('/<ins(.*?)<\/ins>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    $this->story = str_replace("div", "p", $this->story);

    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getImages(&$text) {
    $this->story = $this->getCData($this->getElementByName('fulltext', $text));
    $this->story = str_replace("&amp;", "&", $this->story);
    $this->story = $this->textFixation($this->story);
    $imagesArray = array();
    preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = explode('alt="', $img);
      $image_caption = explode('"', $image_caption[1]);
      $image_caption = $image_caption[0];
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      $oldimagePath = $imagePath;
      $imagePath = explode('?', $imagePath);
      $imagePath = $imagePath[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
