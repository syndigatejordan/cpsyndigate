<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : News Company Botswana (Pty) Ltd  [# publisher id = 954]
//Title      : The Botswana Gazette [ English ] 
//Created on : May 18, 2016, 11:08:00 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2754 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}