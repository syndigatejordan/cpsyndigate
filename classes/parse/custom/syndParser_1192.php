<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ArmInfo News Agency  [# publisher id = 358]
//Title      : ArmInfo - Newswire [ Russian ] 
//Created on : Apr 16, 2019, 12:44:08 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1192 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}