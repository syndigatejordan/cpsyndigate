<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : La Nouvelle Tribune  [# publisher id = 1546]
//Title      : La Nouvelle Tribune [ French ] 
//Created on : Sep 19, 2021, 10:23:04 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10349 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}