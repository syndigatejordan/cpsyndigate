<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : 3BL MEDIA, Inc.  [# publisher id = 1730]
//Title      : 3BL Media ESG News [ English ] 
//Created on : Jan 16, 2022, 11:46:08 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12331 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}