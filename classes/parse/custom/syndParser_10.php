<?php

////////////////////////////////////////////////////////////////////////
// Publisher : Sidra Media
// Title     : 7DAYS [English
////////////////////////////////////////////////////////////////////////

class syndParser_10 extends syndParseRss {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article story");
    $this->story = strip_tags($this->story, '<p><br><table><tr><td><span>');
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    return $this->story;
  }

  protected function getImages(&$text) {
    $this->story = trim($this->textFixation($this->getCData($this->getElementByName('content:encoded', $text))));
    $this->story = preg_replace('/<div data-object="thumbnav"><img(.*)<\/div>/', '', $this->story);
    $imagesArray = array();
    preg_match_all('/<figure id="attachment_(.*?)<\/figure>/is', $this->story, $imgs);
    foreach ($imgs[0] as $div) {
      $this->addLog("getting article images");
      preg_match("/<img[^>]+>/i", $div, $img);
      $img = $img[0];
      $image_caption = null;
      preg_match('/<figcaption class="wp-caption-text">(.*?)<\/figcaption>/is', $div, $image_caption);
      $image_caption = $image_caption[1];
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        $this->story = str_replace($div, "", $this->story);
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        $this->story = str_replace($div, "", $this->story);
        continue;
      }
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        $this->story = str_replace($div, "", $this->story);
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = str_replace('%20', ' ', $image_caption);
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
      $this->story = str_replace($div, "", $this->story);
    }
    preg_match_all("/<img[^>]+\>/i", $this->story, $imgs);

    foreach ($imgs[0] as $img) {

      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = explode('alt="', $img);
      $image_caption = explode('"', $image_caption[1]);
      $image_caption = trim($image_caption[0]);
      if (is_array($image_caption) === TRUE) {
        $image_caption = '';
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        $this->story = str_replace($img, "", $this->story);
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        $this->story = str_replace($img, "", $this->story);
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        $this->story = str_replace($img, "", $this->story);
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = str_replace('%20', ' ', $image_caption);
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);

      $this->story = str_replace($img, "", $this->story);
    }
    return $imagesArray;
  }

}
