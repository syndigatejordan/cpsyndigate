<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : iCrowdNewswire, LLC  [# publisher id = 1535]
//Title      : Gestion [ Spanish ] 
//Created on : Jul 01, 2021, 1:39:17 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11928 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}