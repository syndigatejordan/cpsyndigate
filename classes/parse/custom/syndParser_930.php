<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : United Media Services LLC  [# publisher id = 275]
//Title      : Al Mar'a [ Arabic ] 
//Created on : Jan 04, 2017, 10:23:30 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_930 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}