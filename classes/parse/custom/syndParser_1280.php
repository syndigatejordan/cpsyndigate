<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Nation Media Group Limited  [# publisher id = 338]
//Title      : Africa Review [ English ] 
//Created on : Sep 29, 2016, 7:34:28 AM
//Author     : khaled
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1280 extends syndParseRss {

  public $story;
  public $author;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $matches = NULL;
    preg_match_all("/<article>(.*?)<\/link>/is", $fileContents, $matches);
    return $matches[0];
  }

  protected function getAbstract(&$text) {

    $this->addLog("getting article summary");
    return trim($this->textFixation($this->getCData($this->getElementByName('description', $text))));
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
    return $headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('articleDate', $text);
    $date = preg_replace('/GMT(.*)/is', '', $date);
    return date('Y-m-d', strtotime($date));
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $this->author = $this->textFixation($this->getCData($this->getElementByName('author', $text)));
    if ($this->author == "AFP -1") {
      return "";
    }
    $this->story = preg_replace('/<style(.*?)<\/style>/is', '', $this->story);
    $this->story = preg_replace('/<figure(.*?)<\/figure>/is', '', $this->story);
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
    $this->story = trim(preg_replace('!\s+!', ' ', $this->story));
    return $this->story;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");

    return $this->author;
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    return $this->textFixation($this->getCData($this->getElementByName('section', $text)));
  }

  public function getArticleReference(&$text) {
    $this->addLog('Getting article Link');
    $link = trim($this->textFixation($this->getCData($this->getElementByName('link', $text))));
    if (empty($link)) {
      return '';
    }
    return $link;
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $image_caption = "";
    $media = $this->getElementByName('media', $text);
    $imagePath = $this->getCData($this->getElementByName('photo', $media));
    $image_caption = trim($this->getCData($this->getElementByName('caption', $media)));
    if ($imagePath) {
      if (!$this->checkImageifCached($imagePath)) {
        $imagePath = str_replace(' ', '%20', $imagePath);
        $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
        if ($copiedImage) {
          $images = $this->getAndCopyImagesFromArray(array($copiedImage));
          if (!empty($image_caption)) {
            $images[0]['image_caption'] = $image_caption;
          }
          $name_image = explode('/images/', $copiedImage);
          if ($image_caption == $name_image[1]) {
            $image_caption = '';
          }
          $images[0]['image_caption'] = $image_caption;
          $images[0]['is_headline'] = TRUE;
          array_push($imagesArray, $images[0]);
        }
      }
    }


    $this->story = $this->getCData($this->getElementByName('story', $text));
    $this->story = str_replace("&nbsp;", " ", $this->story);
    $this->story = $this->textFixation($this->story);

    preg_match_all("/<img(.*?)>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
      if (!empty($image_caption[0])) {
        $image_caption = $image_caption[0];
      } else {
        $image_caption = "";
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      $imagePath = preg_replace("/\?(.*)/is", "", $imagePath);
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
