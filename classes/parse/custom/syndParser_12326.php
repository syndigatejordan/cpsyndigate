<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Seth Weintraub  [# publisher id = 1721]
//Title      : Electrek [ English ] 
//Created on : Jan 11, 2022, 2:28:24 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12326 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}