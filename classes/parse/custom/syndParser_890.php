<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CNBC Arabia  [# publisher id =260 ] 
//Title      : CNBC Arabia [ CNBC Arabia ] 
//Created on : Jun 17, 2019, 05:16:03 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_890 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getStory(&$text) {
    $allowedTags = '<h1><h2><h3><h4><h5><h6><img><b><i><u><p><hr><br><br/><br /><strong><div><span><ul><li><ol><table><td><th><tr><tbody><thead><a><iframe>';

    $this->addLog("getting article body");
    if (empty($this->story)) {
      return strip_tags($this->textFixation($this->getElementByName('fulltext', $text)), $allowedTags);
    } else {
      return strip_tags($this->story, $allowedTags);
    }
  }

  protected function getImages(&$text) {
      $this->story = $this->textFixation($this->getCData($this->getElementByName('fulltext', $text)));
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();

      $imagesArray = array();
      preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
      foreach ($imgs[0] as $img) {
          $this->addLog("getting article images");
          $imageInfo = syndParseHelper::getImgElements($img, 'img');
          $imagePath = $imageInfo[0];
          if (!$imagePath) {
              continue;
          }
          if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $name_image = explode('/images/', $copiedImage);
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $videos = array();
    $title = $this->textFixation($this->getCData($this->getElementByName('title', $text)));

    $date = $this->getElementByName('date', $text);
    $date = date('Y-m-d h:i:s', strtotime($date));
    $story = $this->getCData($this->getElementByName('fulltext', $text));
    $story = str_replace("&nbsp;", " ", $story);
    $story = $this->textFixation($story);
    $story = trim(preg_replace('!\s+!', ' ', $story));
    $matches = null;
    preg_match_all('/<video(.*?)<\/video>/is', $story, $matches);
    foreach ($matches[0] as $match) {

      $sources = null;
      if (preg_match_all('/<source(.*?)>/is', $match, $sources)) {
        foreach ($sources[0] as $source) {
          $video = array();
          $videoname = syndParseHelper::getImgElements($source, 'source', 'src');
          if (isset($videoname[0])) {
            $videoname = $videoname[0];
          } else {
            $videoname = "";
          }
          if (empty($videoname)) {
            continue;
          }
          $mimeType = syndParseHelper::getImgElements($source, 'source', 'type');
          if (isset($mimeType[0])) {
            $mimeType = $mimeType[0];
          } else {
            $mimeType = "";
          }
          if (preg_match("/youtube.com/", $videoname)) {
            continue;
          }

          $video['video_name'] = $videoname;
          $video['original_name'] = $videoname;
          $video['video_caption'] = $title;
          $video['mime_type'] = $mimeType;
          $video['added_time'] = $date;
          $videos[] = $video;
        }
      }
    }
    return $videos;
  }

}
