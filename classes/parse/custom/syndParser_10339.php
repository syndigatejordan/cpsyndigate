<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Founder Apabi Technology Limited  [# publisher id = 1538]
//Title      : China Business News [ simplified Chinese ] 
//Created on : Mar 14, 2021, 1:56:29 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10339 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh-Hans'); 
	} 
}