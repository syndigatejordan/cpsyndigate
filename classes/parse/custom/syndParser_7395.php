<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Cynomedia Africa SARL  [# publisher id = 1481]
//Title      : Journal des Comores [ French ] 
//Created on : Sep 05, 2021, 1:34:35 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7395 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}