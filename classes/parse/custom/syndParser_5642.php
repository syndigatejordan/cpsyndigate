<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bizzy Baby Media  [# publisher id =1249 ] 
//Title      : Bizzy Baby Media [ English ] 
//Created on : Oct 14, 2019, 11:35:33 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5642 extends syndParseXmlContent {

  public $story;
  public $imagesArray = array();
  public $category;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('item', $fileContents);
    return $articles;
  }

  public function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return trim($this->textFixation($this->getElementByName('Title', $text)));
  }

  public function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = trim($this->getElementByName('Production_Date', $text));
    $date = trim(preg_replace('/GMT(.*)/is', '', $date));
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  public function getArticleOriginalId($params = array()) {
    $articleOriginalId = $this->getElementByName("Sr_No_", $params['text']);
    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }

    return $this->title->getId() . '_' . sha1($articleOriginalId);
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    return trim($this->textFixation($this->getElementByName('Sub_Category', $text)));
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = "";
    if (isset($this->imagesArray[0]["img_name"])) {
      $description = trim($this->textFixation($this->getElementByName('Description', $text)));
      $this->story .= "<p>$description</p>";
      $Channel = $this->getData($text, 'Channel', 'Channel:');
      $this->story .=$Channel;
      $Category = $this->getData($text, 'Category', 'Category');
      $this->story .= $Category;
      $Sub_Category = $this->getData($text, 'Sub_Category', 'Sub Category:');
      $this->story .= $Sub_Category;
      $Tags = $this->getData($text, 'Tags', 'Tags:');
      $this->story .= $Tags;
      $Duration = $this->getData($text, 'Duration', 'Duration:');
      $this->story .= $Duration;
      $Video_Resolution = $this->getData($text, 'Video_Resolution', 'Video Resolution:');
      $this->story .= $Video_Resolution;
      return $this->story;
    } else {
      return '';
    }
  }

  protected function getImages(&$text) {
    $s3 = "https://bizzy-baby-media.s3.amazonaws.com/Thumbnails/";
    $this->imagesArray = array();
    $imgs = array();
    $imgs = array();
    preg_match_all("/<Thumbnail_File_Name>(.*?)<\/Thumbnail_File_Name>/i", $text, $imgs);
    foreach ($imgs[0] as $img) {

      $this->addLog("getting article images");
      $image_caption = trim($this->getElementByName('Title', $text));
      $imagePath = $s3 . trim($this->textFixation($this->getElementByName('Thumbnail_File_Name', $img)));
      $images = array();
      $images['img_name'] = $imagePath;
      $images['original_name'] = $imagePath;
      $images['image_caption'] = $image_caption;
      $images['is_headline'] = false;
      $images['image_original_key'] = sha1($imagePath);
      array_push($this->imagesArray, $images);
    }
    return $this->imagesArray;
  }

  protected function getVideos(&$text) {
    $s3 = "https://bizzy-baby-media.s3.amazonaws.com/Videos/";

    $date = trim($this->getElementByName('Production_Date', $text));
    $videoName = trim($this->getElementByName('Title', $text));
    $date = date('Y-m-d h:i:s', strtotime($date));
    $this->videos = array();
    $imgs = array();
    preg_match_all("/<Video_File_Name>(.*?)<\/Video_File_Name>/i", $text, $imgs);

    foreach ($imgs[1] as $img) {
      $this->addLog("Getting video");
      $videoname = $s3 . $img;
      $mimeType = "";
      $video['video_name'] = $videoname;
      $video['original_name'] = $videoName;
      $video['video_caption'] = $videoName;
      $video['mime_type'] = $mimeType;
      $video['added_time'] = $date;
      $this->videos[] = $video;
    }
    return $this->videos;
  }

  private function getData($text, $from, $lable) {
    $story = "";
    $body = trim($this->textFixation($this->getElementByName($from, $text)));
    if (!empty($body)) {
      $story = "<div><p><strong>$lable</strong></p><p>$body</p></div>";
    } else {
      return "";
    }
    return $story;
  }

  private function getDataFromArray($text, $from, $lable, $sub_item) {
    $story = "";
    $element = array();
    $element_p = $this->getElementByName($from, $text);
    $items = $this->getElementsByName($sub_item, $element_p);
    foreach ($items as $item) {

      $element[$item] = $item;
    }
    $body = trim(implode(", ", $element));
    if (!empty($body)) {
      $story = "<div><p><strong>$lable</strong></p><p>$body</p></div>";
    } else {
      return "";
    }
    return $story;
  }

}
