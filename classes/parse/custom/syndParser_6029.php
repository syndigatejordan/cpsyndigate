<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Arcturus Publishing Limited  [# publisher id = 1334]
//Title      : Color by Number - The ‘Calm' Issue [ English ] 
//Created on : Apr 22, 2020, 8:10:57 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6029 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}