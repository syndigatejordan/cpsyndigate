<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Confidente Newspaper  [# publisher id = 1448]
//Title      : Confidente Newspaper [ English ] 
//Created on : Oct 28, 2020, 7:35:49 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6967 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}