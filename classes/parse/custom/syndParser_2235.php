<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : Assafir  [# publisher id =29 ]  
//Title     : As-Safir
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2235 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('item', $fileContents);
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->getElementByName('title', $text);
    $headline = preg_replace('!\s+!', ' ', $headline);
    return $this->textFixation($headline);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    $date = preg_replace('/ \+(.*)/is', '', $date);
    return date('Y-m-d', strtotime($date));
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = trim($this->textFixation($this->getCData($this->getElementByName('description', $text))));
    $story = preg_replace('/<img[^>]+\>/i', '', $story);
    $story = preg_replace('!\s+!', ' ', $story);
    return$story;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getCData($this->getElementByName('a10:name', $text)));
  }

  public function getArticleReference(&$text) {
    var_dump($text);
    $this->addLog("getting article reference");
    preg_match('/<a10:link href="(.*?)" \/>/si', $text, $link);
    return $link[1];
  }

  protected function getImages(&$text) {

    $imagesArray = array();
    $this->addLog("getting article images");
    preg_match('/<enclosure url="(.*?)" type/i', $text, $imgs);
    $imagePath = $imgs[1];
    if (!$imagePath) {
      return $imagesArray;
    }
    if ($this->checkImageifCached($imagePath)) {
      // Image already parsed..
      return $imagesArray;
    }
    $imagePath = str_replace(' ', '%20', $imagePath);
    $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

    if (!$copiedImage) {
      echo "no pahr";
      return $imagesArray;
    }
    $images = $this->getAndCopyImagesFromArray(array($copiedImage));

    $name_image = explode('/images/', $copiedImage);
    if ($images[0]['image_caption'] == $name_image[1]) {
      $images[0]['image_caption'] = '';
    }
    $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
    $images[0]['is_headline'] = false;
    array_push($imagesArray, $images[0]);
    return $imagesArray;
  }

}
