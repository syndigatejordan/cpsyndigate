<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Saudi Press Agency (SPA)  [# publisher id = 133]
//Title      : Saudi Press Agency (SPA) [ French ] 
//Created on : Oct 12, 2020, 12:11:42 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6653 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}