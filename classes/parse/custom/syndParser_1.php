<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Apex Press and Publishing  [# publisher id = 1]
//Title      : The Week [ English ] 
//Created on : Jan 30, 2016, 7:35:21 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}