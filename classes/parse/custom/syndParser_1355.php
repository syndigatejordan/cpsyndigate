<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Netilligence Business Systems LLC  [# publisher id = 418]
//Title      : Arabian Gazette [ English ] 
//Created on : Oct 19, 2021, 7:11:09 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1355 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}