<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IPE International Publishers Ltd  [# publisher id = 1779]
//Title      : IPE - ESG [ English ] 
//Created on : Feb 28, 2022, 1:14:41 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12501 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}