<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence de Presse Savoir News  [# publisher id = 1503]
//Title      : Savoir News [ French ] 
//Created on : Sep 19, 2021, 10:21:09 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7463 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}