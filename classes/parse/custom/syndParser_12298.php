<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : InfoBrisas  [# publisher id = 1698]
//Title      : InfoBrisas [ Spanish ] 
//Created on : Jan 03, 2022, 2:15:44 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12298 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}