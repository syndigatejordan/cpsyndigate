<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Motivate publishing  
//Title     : Campaign Middle East [ English ] 
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_324 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}


/*
//////////////////////////////////////////////////////////////////////////////////////
//  Title     : Campaign Middle East [English]
//  Publisher : Motivate publishing
//////////////////////////////////////////////////////////////////////////////////////

	class syndParser_324 extends syndParseAbTxtSample  
	{
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('en');
			$this->charEncoding= 'ISO-8859-1';
		}
		
		
		public function getRawArticles(&$fileContents) {	
			$this->addLog('Getting Raw article');
			return array($fileContents);
		}
						
		protected function getStory(&$text) {
			return nl2br(trim($this->textFixation($text)));
		}
		
		protected function getHeadline(&$text) {
			return trim(parent::getHeadline($text, 'Headline'));
		}
		
		protected function getArticleDate(&$text) {
			$tmp = parent::getArticleDate($text, 'Date'); // just to delete it 
			return $this->dateFormater('current date until they fix it');
		}
		
		protected function getAbstract(&$text) {
			return trim(parent::getAbstract($text, 'Summary'));
		}
		
		protected function getAuthor(&$text) {
			return trim(parent::getAuthor($text, 'Author'));
		}
		
		protected function getOriginalCategory(&$text) {
			
			$text = str_replace('(Sub category', '(Subcategory', $text);
			$text = str_replace('(Parent tag', '(Parenttag', $text);
			$text = str_replace('(Sub tags', '(Subtags', $text);
			$this->removeItem('Parenttag', $text);
			$this->removeItem('Subtags', $text);
			$this->removeItem('Subcategory', $text);
			return  trim(parent::getOriginalCategory($text, 'Category'));
		}

		
	} // End class 
	
*/	
