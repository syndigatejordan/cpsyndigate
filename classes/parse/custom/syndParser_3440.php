<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ACN Newswire Co Ltd  [# publisher id = 1174]
//Title      : JCN Newswire [ Japanese ] 
//Created on : Sep 23, 2021, 7:57:10 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3440 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	} 
}