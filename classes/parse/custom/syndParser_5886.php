<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Center for Biological Diversity  [# publisher id = 1282]
//Title      : The Revelator [ English ] 
//Created on : Dec 19, 2019, 9:44:00 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5886 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
