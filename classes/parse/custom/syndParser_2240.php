<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Daily Times (via HT Media Ltd.)  [# publisher id = 708]
//Title      : Daily Times(PK) [ English ] 
//Created on : Jan 22, 2020, 10:14:56 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2240 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}