<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CPI Media Group  [# publisher id =80 ] 
//Title      : Broadcast Pro Middle East [ English ] 
//Created on : Dec 26, 2017, 6:35:22 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_888 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
