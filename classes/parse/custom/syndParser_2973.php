<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Afghanistan Times  [# publisher id = 569]
//Title      : Afghanistan Times [ English ] 
//Created on : Aug 25, 2021, 6:50:11 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2973 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}