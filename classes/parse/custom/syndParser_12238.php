<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Caretti Media  [# publisher id = 1673]
//Title      : Whisper Eye [ English ] 
//Created on : Oct 24, 2021, 12:10:56 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12238 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}