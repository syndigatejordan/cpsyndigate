<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agencia STP Press  [# publisher id = 1368]
//Title      : Agencia STP Press [ Portuguese ] 
//Created on : Sep 19, 2021, 8:54:42 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6456 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}