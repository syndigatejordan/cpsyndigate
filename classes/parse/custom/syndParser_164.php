<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher   : Palestine News Network (PNN)  [# publisher id =58 ]   
//Title       : Palestine News Network (PNN) [ English ] 
// Created on : Mar 02, 2020, 12:33:03 PM
// Author     : eyad
////////////////////////////////////////////////////////////////////////

class syndParser_164 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
