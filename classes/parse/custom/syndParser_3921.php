<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Fashion Week Online  [# publisher id = 1198]
//Title      : FashionWeekOnline (FWO) - Interviews [ English ] 
//Created on : Oct 24, 2018, 9:09:13 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3921 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $this->story = preg_replace('/<style(.*?)<\/style>/is', '', $this->story);
    $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) srcset=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/<noscript(.*?)<\/noscript>/is', '', $this->story);
    $this->story = trim(preg_replace('!\s+!', ' ', $this->story));
    return $this->story;
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $videos = array();
    $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));

    $date = $this->getElementByName('date', $text);
    $date = date('Y-m-d h:i:s', strtotime($date));
    $story = $this->getCData($this->getElementByName('fulltext', $text));
    $story = str_replace("&nbsp;", " ", $story);
    $story = $this->textFixation($story);
    $story = trim(preg_replace('!\s+!', ' ', $story));
    $matches = null;
    preg_match_all('/<iframe(.*?)<\/iframe>/is', $story, $matches);
    foreach ($matches[0] as $match) {
      $video = array();
      $videoname = syndParseHelper::getImgElements($match, 'iframe', 'src');
      $videoname = $videoname[0];
      if (preg_match("/youtube.com/", $videoname)) {
        continue;
      }
      $mimeType = "";
      $video['video_name'] = $videoname;
      $video['original_name'] = $videoName;
      $video['video_caption'] = $videoName;
      $video['mime_type'] = $mimeType;
      $video['added_time'] = $date;
      $videos[] = $video;
    }
    return $videos;
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $this->story = $this->getCData($this->getElementByName('fulltext', $text));
    $this->story = str_replace("&nbsp;", " ", $this->story);
    $this->story = $this->textFixation($this->story);
    $this->story = trim(preg_replace('!\s+!', ' ', $this->story));

      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();

      preg_match_all("/<img(.*?)>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
      if (!empty($image_caption[0])) {
        $image_caption = $image_caption[0];
      } else {
        $image_caption = "";
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      var_dump($imagePath);
      $imagePath = preg_replace("/\?(.*)/is", "", $imagePath);
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $path = explode("fashionweekonline.com", $imageUrl);
    if (isset($path[1])) {
      $path = $path[1];
    } else {
      $path = '/wp-content/uploads/2018/02/2018.02.25-ANOUKI-MFW_LMM7845.jpg';
    }
    $baseName = sha1($imageUrl) . ".jpeg";
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $imageUrl);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // Skip SSL Verification
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(':authority: fashionweekonline.com',
          'method: GET',
          "path: $path",
          'scheme: https',
          'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
          'accept-encoding: gzip, deflate, br',
          'accept-language: en-US,en;q=0.9,ar;q=0.8,fr;q=0.7',
          'cache-control: no-cache',
          'pragma: no-cache',
          'upgrade-insecure-requests: 1',
          'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36'));
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        unlink($copiedImage);
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
