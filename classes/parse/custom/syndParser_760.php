<?php 

//////////////////////////////////////////////////////////////////////////////
// Publisher: TenderNews.com
// Titles   : TenderNews.com - Tenders [ English ]
//////////////////////////////////////////////////////////////////////////////

	
class syndParser_760 extends syndParseXmlContent {
	
	public function customInit() {
		parent::customInit();
		$this->defaultLang = $this->model->getLanguageId('en');
	}
	
		
	public function getRawArticles(&$fileContents) {			
		$this->addLog("getting raw articles text");
		$this->addLog("this will not loggin all operations due the huge amount of articles, this can be reactivated from the parser by removing the comments");
		$rawArticles = $this->getElementsByName('item', $fileContents);	
		return $rawArticles;
			

	}
	protected function getHeadline(&$text) {
		//$this->addLog("getting article headline");
		return $this->textFixation($this->getElementByName('Tender_Detail', $text));
	}
		
	protected function getArticleDate(&$text) {
		//$this->addLog("getting article date");
		$date = $this->getElementByName('Publication_date', $text);
		
		if($date) {
			return $this->dateFormater($date);
		}
		return parent::getArticleDate($text);
	}
		
	public  function  getStory(&$text) {
		
		//$this->addLog("getting article story");
		
	 	$story 	    	= $this->textFixation($this->getElementByName('Tender_Detail', $text))." \n";
		$location   	= $this->textFixation($this->getElementByName('Location', $text));
		$bidDate     	= $this->textFixation($this->getElementByName('BidDate', $text));
		$tenderAuth 	= $this->textFixation($this->getElementByName('TA', $text));
		
		$tendAuthAddress = $this->textFixation($this->getElementByName('TA_Address', $text));
		$financier   	 = $this->textFixation($this->getElementByName('Financier', $text));
		$country	     = $this->textFixation($this->getElementByName('Tender_Country', $text));
		$type		     = $this->textFixation($this->getElementByName('Tender_Type', $text));
		
		$tearnMoney	    = $this->textFixation($this->getElementByName('TEarnMoney', $text));
		$estimatedValue = $this->textFixation($this->getElementByName('TEMd', $text));
		$story .= " <b> more info : </b> </br> \n ";
		$this->addToBody('Location ', $location, $story);
		$this->addToBody('BidDate ', $bidDate, $story);
		$this->addToBody('Tender Country ', $country, $story);
		$this->addToBody('Tender Type ', $type, $story);
		$this->addToBody('Tendering Authority ', $tenderAuth, $story);
		$this->addToBody('Tendering Authority Address', $tendAuthAddress, $story);
		$this->addToBody('Financier ', $financier, $story);
		$this->addToBody('Tearn Money ', $tearnMoney, $story);
		$this->addToBody('Estimate Value ', $estimatedValue, $story);
        $story = preg_replace('/<img[^>]+\>/i', '', $story);

        return $story;
	}
	
	public function getArticleOriginalId($params = array()) {
		$bidDate 		= $this->getElementByName('BidDate', $params['text']);
		$tenderDetail 	= $this->getElementByName('Tender_Detail', $params['text']);			
		
		$articleOriginalId = $bidDate . $tenderDetail;
			
		if(!$articleOriginalId) {
			return parent::getArticleOriginalId($params);				
		} else {				
			return $this->title->getId() . "_" . sha1($articleOriginalId);
		}
	}

	protected function addToBody($caption, $value, &$body) {
		$value = trim($value);
		
		if($value && $value != 'N/A') {
			$body .= "$caption : $value <br />\n"; 
		}	
	}
	
  	protected function getOriginalCategory(&$text) {		
		//$this->addLog("getting article original category");
		return trim($this->textFixation($this->getElementByName('Sector_Category', $text)));
	}

}
