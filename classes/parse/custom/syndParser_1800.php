<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Business and Financial Times  [# publisher id = 630]
//Title      : Business and Financial Times [ English ] 
//Created on : Mar 11, 2018, 8:43:42 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1800 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}