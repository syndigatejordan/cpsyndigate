<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CryptoNews  [# publisher id = 1685]
//Title      : CryptoNews [ simplified Chinese ] 
//Created on : Nov 23, 2021, 10:38:45 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12274 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh-Hans'); 
	} 
}