<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Bahrain Chamber of Commerce & Industry  [# publisher id =768 ]  
//Title     : BCCI News [ English ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2401 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

}
