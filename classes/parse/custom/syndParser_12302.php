<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IFR / PFI  [# publisher id = 1700]
//Title      : IFR Ticker [ English ] 
//Created on : Jan 06, 2022, 11:56:35 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12302 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}