<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : ANI Media Pvt Ltd 
// Titles    : Asian News International (ANI) [Video feed]
///////////////////////////////////////////////////////////////////////////

class syndParser_238 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    $this->loadCurrentDirectory();
    $this->addLog("getting raw articles text");
    return $this->getElementsByName('article', $fileContents);
  }

  protected function getStory(&$text) {
    $this->loadVideoName($text);
    $this->originalCategory = $this->textFixation($this->getElementByName('cats', $text));
    $this->addLog("getting article text");
    $story = $this->textFixation($this->getElementByName('content', $text));
    // $story="$story update";
    //$videoImage = $this->getVideoImage($text);
    return $story;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return $this->textFixation($this->getElementByName('headline', $text));
  }

  protected function getAbstract(&$text) {
    $this->addLog("getting abstract");
    return $this->textFixation($this->getElementByName('summary', $text));
  }

  protected function getArticleDate(&$text) {

    $this->addLog("getting articles date");
    $date = $this->getElementByName('date', $text);
    $date = explode("/", $date);
    $date = "{$date[2]}-{$date[1]}-{$date[0]}";
    if (strtotime($date)) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate(&$text);
  }

  /* protected function getIptcId(&$text)
    {
    $this->addLog("getting category name");
    $originalCategoryName = $this->getElementByName('cats', $text);
    $iptcId = $this->model->getIptcCategoryId($originalCategoryName);
    if(!$iptcId) {
    return parent::getIptcId($text);
    }
    return $iptcId;
    } */

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return trim($this->textFixation($this->getElementByName('author', $text)));
  }

  private function loadVideoName($text) {
    $videoName = $this->textFixation($this->getElementByName('video', $text));
    $videoName = explode(".", $videoName);
    $this->videoName = $videoName[0];
  }

  protected function getImages(&$text) {
    // this contain just one image [ video image ]
    $this->addLog("getting video image");

    //$imageName = $this->currentDir . $this->videoName . '.jpg';
    $image_Name = $this->textFixation($this->getElementByName('image', $text));
    $image_full_path = $this->currentDir . $image_Name;
    $original_name = explode('.', $image_Name);
    if (file_exists($image_full_path)) {
      //the video name is same as the image name here  		
      $name = $this->model->getImgReplacement($image_Name, $this->currentDir, 238);
      $img['img_name'] = str_replace(IMGS_PATH, "https://syndigateimages.s3.amazonaws.com/syndigate/imgs/", $name);
      $img['original_name'] = $original_name[0];
      $img['image_caption'] = $image_Name;
      $img['is_headline'] = true;
      $images[] = $img;
      //var_dump($images);exit;
      return $images;
    } else {

      return false;
    }
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $video_Name = trim($this->textFixation($this->getElementByName('video', $text)));
    $videoName = "syndigate-ani-media-pvt-ltd.s3.amazonaws.com/" . $video_Name;
    $videoName="https://".str_replace("//","/",$videoName);
    $video_Name_arr = explode('.', $video_Name);
    $videos = array();
    $video['video_name'] = $videoName;
    $video['original_name'] = $videoName;
    $video['video_caption'] = $video_Name_arr[0];
    $video['added_time'] = date('Y-m-d h:i:s', filemtime($video['original_name']));
    $videos[] = $video;
    return $videos;
  }

  protected function getOriginalCategory(&$text) {
    return $this->originalCategory;
  }

  private function getBitRateFromName($name) {
    $bit = explode("(", $name);

    if (is_array($bit) & count($bit) > 1) {
      $bit = $bit[1];
      $bit = explode(")", $bit);
      return $bit[0];
    } else {
      return '';
    }
  }

}

?>
