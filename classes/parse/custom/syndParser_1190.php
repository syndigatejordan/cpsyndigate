<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : ArmInfo News Agency  [# publisher id =358 ] 
// Titles    : ArmInfo - Business Bulletin [Russian]
///////////////////////////////////////////////////////////////////////////

class syndParser_1190 extends syndParseXmlContent {

	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('ru');
		$this -> charEncoding = "cp-866";
		$this->extensionFilter = array( 'txt', 'TXT' );
	}

	protected function getFileContents(&$file) {
		if ($this -> charEncoding) {
			return mb_convert_encoding(file_get_contents($file), OUTPUT_CHARSET, $this -> charEncoding);
			//return iconv($this->charEncoding, OUTPUT_CHARSET, file_get_contents($file));
		} else {
			return file_get_contents($file);
		}
	}

	public function getRawArticles(&$fileContents) {
		return $this->getElementsByName('item', $fileContents);
	}

	public function getStory(&$text) {
		$this->addLog('Getting article story');

		$story = trim($this -> getElementByName('body', $text));
		$story = '<p>' . str_replace(".\r\n", ".\r\n" . "</p>\r\n<p>", $story) . '</p>';
		return $story;
	}

	public function getHeadline(&$text) {
		$this->addLog('Getting article headline');

		return trim($this -> getElementByName('headline', $text));
	}
	
	public function getOriginalCategory(&$text) {
		$this->addLog('Getting article category');

        return trim($this->getElementByName('category', $text));
	}

    public function getArticleDate(&$text) {
    	$this->addLog('Getting article date');

        $date = trim($this->getElementByName('pubdate', $text)); 
		list($d, $m, $y) = preg_split('/\//', $date);
		$date = "$y-$m-$d";
		$date =  date('Y-m-d', strtotime($date));
		if ( $date == '1970-01-01' ) {
			$date = date('Y-m-d');
		}
		return $date;
    }
}