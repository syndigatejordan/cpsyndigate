<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Lebanese InformationTechnology Services (LITS)  [# publisher id = 1345]
//Title      : Lebanese Forces [ Arabic ] 
//Created on : Jun 16, 2020, 7:41:54 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6120 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}