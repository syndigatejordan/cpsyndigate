<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : World Economic Forum  [# publisher id = 1274]
//Title      : World Economic Forum [ English ] 
//Created on : Dec 19, 2019, 12:56:09 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5876 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
