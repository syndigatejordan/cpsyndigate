<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Kuensel Corporation Ltd.  [# publisher id = 1092]
//Title      : Kuensel [ English ] 
//Created on : Sep 19, 2021, 7:05:34 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3015 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}