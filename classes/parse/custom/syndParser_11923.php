<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : REFINITIV LIMTED  [# publisher id = 1597]
//Title      : ZAWYA by Refinitiv (Press Releases) [ English ] 
//Created on : Nov 30, 2021, 7:33:46 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11923 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}