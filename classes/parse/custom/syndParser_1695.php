<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Palestine Exchange (PEX)  [# publisher id = 600]
//Title      : Palestine Exchange (PEX) [ Arabic ] 
//Created on : Jul 21, 2019, 5:27:22 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1695 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}