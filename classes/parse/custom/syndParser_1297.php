<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Independent Newspapers (Pty) Limited  [# publisher id = 392]
//Title      : Sunday Tribune [ English ] 
//Created on : Jul 29, 2021, 8:24:44 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1297 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}