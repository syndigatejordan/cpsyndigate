<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Jornal Grande Bahia (JGB)  [# publisher id = 1571]
//Title      : Jornal Grande Bahia (JGB) [ Portuguese ] 
//Created on : Sep 19, 2021, 10:24:45 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10829 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}