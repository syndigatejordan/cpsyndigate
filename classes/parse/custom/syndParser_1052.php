<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Two Way Communication Co WLL  [# publisher id =317 ] 
//Title      : 24X7 News Bahrain Online [ English ] 
//Created on : Dec 11, 2018, 10:20:17 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1052 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
