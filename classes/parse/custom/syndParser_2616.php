<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bhutan Chamber of Commerce & Industry (BCCI)  [# publisher id = 856]
//Title      : Bhutan Chamber of Commerce & Industry (BCCI) - News [ English ] 
//Created on : Feb 10, 2016, 12:45:33 PM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2616 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}