<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : News Express Nigeria  [# publisher id = 1841]
//Title      : News Express [ English ] 
//Created on : Aug 28, 2022, 5:57:17 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12701 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}