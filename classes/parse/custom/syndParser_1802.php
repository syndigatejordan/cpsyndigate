<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The News Chronicle  [# publisher id = 632]
//Title      : News Chronicle [ English ] 
//Created on : Jul 24, 2018, 9:49:03 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1802 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}