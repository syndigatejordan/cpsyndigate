<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CSRWIRE, LLC.  [# publisher id = 1724]
//Title      : 3BL CSR Wire [ English ] 
//Created on : Jan 16, 2022, 11:40:28 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12330 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}