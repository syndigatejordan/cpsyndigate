<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BANG Media International  [# publisher id = 306]
//Title      : BANG Showbiz - Picture Features [ English ] 
//Created on : Sep 13, 2021, 12:20:30 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3541 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}