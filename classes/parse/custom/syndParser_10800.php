<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : LRM 955 Radio Maria Juana FM 101.9  [# publisher id = 1563]
//Title      : Radio Maria Juana 101.9 [ Spanish ] 
//Created on : May 04, 2021, 8:14:18 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10800 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	}
}