<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Abu Dhabi Chamber of Commerce and Industry  [# publisher id = 789]
//Title      : Abu Dhabi Chamber of Commerce and Industry News [ Arabic ] 
//Created on : Jan 31, 2016, 1:43:20 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2482 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}