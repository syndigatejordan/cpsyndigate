<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Mongolian National Chamber of Commerce and Industry  [# publisher id = 854]
//Title      : Mongolian National Chamber of Commerce and Industry (MNCCI) - Intellectual Property Newsletter [ English ] 
//Created on : Feb 08, 2016, 1:16:59 PM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2613 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}