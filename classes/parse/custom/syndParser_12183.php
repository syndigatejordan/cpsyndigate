<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Benin Web TV  [# publisher id = 1634]
//Title      : Benin Web TV [ French ] 
//Created on : Sep 30, 2021, 12:57:25 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12183 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}