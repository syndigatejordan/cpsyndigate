<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agencia Informativa Latinoamericana Prensa Latina  [# publisher id = 1385]
//Title      : Prensa Latina [ Russian ] 
//Created on : Oct 19, 2020, 12:28:01 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6499 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}