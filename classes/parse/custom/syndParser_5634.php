<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agency Tunis Afrique Press  [# publisher id = 142]
//Title      : Agency Tunis Afrique Press [ French ] 
//Created on : May 21, 2019, 11:09:40 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5634 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}