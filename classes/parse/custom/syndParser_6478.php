<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sahara Press Service  [# publisher id = 1378]
//Title      : Sahara Press Service [ French ] 
//Created on : Oct 07, 2020, 12:48:02 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6478 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}