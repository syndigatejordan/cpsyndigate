<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Conversation US, Inc.  [# publisher id = 1244]
//Title      : The Conversation (US Edition) [ English ] 
//Created on : Sep 27, 2021, 7:49:50 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5617 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}