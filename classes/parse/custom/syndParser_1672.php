<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : Pivotal Sources 
//Title     : Pivotal Sources [ English ]
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_1672 extends syndParseXmlContent {

  public $imagesArray = array();

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('news', $fileContents);
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");

    $headline = trim($this->getCData($this->getElementByName('heading', $text)));
    return $headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getCData($this->getElementByName('date', $text));
    $date = explode("/", $date);
    $date = implode("-", $date);
    return $date;
  }

  public function getStory(&$text) {
      $this->addLog('Getting article story');
      $this->story = $this->textFixation($this->getCData($this->getElementByName('description', $text)));

      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);

      return $this->story;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return trim($this->textFixation($this->getCData($this->getElementByName('source', $text))));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getCData($this->getElementByName('sector', $text));
    $cats=rtrim($cats,',');
    return $cats;
  }

}