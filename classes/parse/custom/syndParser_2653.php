<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Jaras  [# publisher id = 890]
//Title      : Al Jaras [ Arabic ] 
//Created on : Sep 19, 2021, 7:04:05 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2653 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}