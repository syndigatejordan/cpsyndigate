<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Human Rights Watch  [# publisher id = 1302]
//Title      : Human Rights Watch - News [ English ] 
//Created on : Feb 10, 2020, 11:50:16 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5917 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
