<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : MONTSAME News Agency  [# publisher id = 1423]
//Title      : MONTSAME News Agency [ Russian ] 
//Created on : Oct 17, 2020, 2:37:26 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6639 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}