<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IRIB World Service  [# publisher id = 799]
//Title      : ParsToday [ Hebrew ] 
//Created on : Oct 22, 2020, 12:34:17 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6577 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('he'); 
	} 
}