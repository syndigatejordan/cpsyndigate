<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Nation Media Group Limited  [# publisher id = 338]
//Title      : 93.3 K-FM [ English ] 
//Created on : Sep 19, 2021, 8:51:25 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3485 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}