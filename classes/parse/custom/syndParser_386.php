<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Algeria Press Service  [# publisher id = 136]
//Title      : Algeria Press Service [ English ] 
//Created on : Apr 03, 2018, 1:38:28 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_386 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}