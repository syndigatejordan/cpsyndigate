<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Salam Watandar Media Organisation  [# publisher id = 1663]
//Title      : Salam Watandar [ English ] 
//Created on : Oct 10, 2021, 7:03:50 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12221 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}