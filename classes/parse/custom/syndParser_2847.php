<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Fujairah Culture & Media Authority  [# publisher id = 1019]
//Title      : Fujairah News [ Arabic ] 
//Created on : Aug 18, 2016, 11:34:21 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2847 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}