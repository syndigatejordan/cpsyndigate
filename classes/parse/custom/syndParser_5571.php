<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Stichting Global Voices  [# publisher id = 1235]
//Title      : Global Voices [ Dutch ] 
//Created on : Feb 22, 2021, 7:58:45 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5571 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('nl'); 
	} 
}