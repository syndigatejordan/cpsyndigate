<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Cynomedia Africa SARL  [# publisher id = 1481]
//Title      : Journal de Kinshasa [ French ] 
//Created on : Aug 15, 2021, 12:22:22 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7392 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}