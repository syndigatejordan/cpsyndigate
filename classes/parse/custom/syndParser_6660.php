<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Timor-Leste News Agency (ANTIL)  [# publisher id = 1429]
//Title      : Timor-Leste News Agency (ANTIL) [ English ] 
//Created on : Oct 13, 2020, 10:17:35 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6660 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}