<?php
///////////////////////////////////////////////////////////////////////////
// Publisher : Asianet-Pakistan (Pvt) Ltd.  [# publisher id =83 ] 
// Titles    : Pakistan Press International (PPI) Radio News
///////////////////////////////////////////////////////////////////////////
class syndParser_2072 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ur');
    $this->charEncoding = 'UTF-8';
    $this->extensionFilter = 'xml';
  }

  public function getRawArticles(&$fileContents) {
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('Article', $fileContents);
  }

  public function getStory(&$text) {

    $this->addLog("getting article text");
    $story = trim($this->getElementByName('Text', $text));
    $story = '<p>' . str_replace(".\r\n", ".\r\n" . "</p>\r\n<p>", $story) . '</p>';
    return $story;
  }

  public function getHeadline(&$text) {
    $this->addLog("getting article headline");

    return trim($this->getElementByName('Headline', $text));
  }

  public function getArticleDate(&$text) {
    $this->addLog("getting article date");

    $date = trim($this->getElementByName('Date', $text));
    return date('Y-m-d', strtotime($date));
  }

  public function getAuthor(&$text) {
    $this->addLog("getting article Source");

    return trim($this->getElementByName('Source', $text));
  }

}