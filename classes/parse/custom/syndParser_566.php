<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ German ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_566 extends syndParseWMCCM {
		
		protected $translations = array('Awarding Authority' 			=>	'Bewilligungsbehörde',
		 								'Nature of Contract'			=>	'Art des Auftrags', 
		 								'Regulation of Procurement' 	=>	'Verordnung der Beschaffung',
										'Type of bid required'			=>	'Art des Angebots erforderlich',
										'Award Criteria'				=>	'Zuschlagskriterien', 
										'Original CPV'					=> 	'Original CPV', 
										'Country'						=>	'Land', 
										'Document Id' 					=>	'Dokument-ID',
										'Type of Document'				=>	'Art des Dokuments',
										'Procedure'						=>	'Vorgehensweise', 
										'Original Language'				=>	'Ursprache', 
										'Current Language'				=> 	'Aktuelle Sprache'
										);	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('de');
		}
	}