<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Standard Investment Bank  [# publisher id = 736]
//Title      : Standard Investment Bank [ English ] 
//Created on : Feb 02, 2016, 12:04:53 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2298 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}