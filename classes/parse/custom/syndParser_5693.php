<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Cover Images Limited  [# publisher id =1257 ] 
//Title      : COVER VIDEO - Viral [ English ] 
//Created on : Aug 26, 2019, 11:33:05 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5693 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getStory(&$text) {
    $story = trim($this->textFixation($this->getCData($this->getElementByName('description', $text))));
    if (!empty($story)) {
      $story = "<p>$story</p>";
    }
    $keywords = trim($this->textFixation($this->getCData($this->getElementByName('media:keywords', $text))));
    if (!empty($keywords)) {
      $keywords = "<p>keywords: $keywords</p>";
    }
    $story = $story . $keywords;
    return $story;
  }

  public function getHeadline(&$text) {
    $this->headline = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
    return $this->headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    $date = trim(str_replace('/\+(.*)/is', '', $date));
    return date('Y-m-d', strtotime($date));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = trim($this->getElementByName('media:category', $text));
    return $cats;
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $image_caption = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
    $enclosure = null;
    preg_match("/<media:thumbnail[^>]+\>/is", $text, $enclosure);
    $imageInfo = syndParseHelper::getImgElements($enclosure[0], 'media:thumbnail', 'url');
    $imagePath = $imageInfo[0];
    if ($imagePath) {
      $this->addLog("getting article images");
      if (!$this->checkImageifCached($imagePath)) {
        $imagePath = str_replace(' ', '%20', $imagePath);
        $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
        if ($copiedImage) {
          $images = $this->getAndCopyImagesFromArray(array($copiedImage));
          if (!empty($image_caption)) {
            $images[0]['image_caption'] = $image_caption;
          }
          $name_image = explode('/images/', $copiedImage);
          if ($images[0]['image_caption'] == $name_image[1]) {
            $images[0]['image_caption'] = '';
          }
          $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
          $images[0]['is_headline'] = TRUE;
          array_push($imagesArray, $images[0]);
        }
      }
    }
    return $imagesArray;
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $matches = null;
    preg_match('/<media:content(.*?)>/is', $text, $matches);
    $videoname = syndParseHelper::getImgElements($matches[0], 'media:content', 'url');
    $link = $videoname[0];
    $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
    $videos = array();
    $video['video_name'] = $link;
    $video['original_name'] = $videoName;
    $video['video_caption'] = $videoName;
    $mimeType = syndParseHelper::getImgElements($matches[0], 'media:content', 'type');
    $mimeType = $mimeType[0];
    $video['mime_type'] = $mimeType;
    $date = $this->getElementByName('pubDate', $text);
    $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
    $videos[] = $video;
    return $videos;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    sleep(3);
    $baseName = basename($imageUrl);
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $imageUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
