<?php

////////////////////////////////////////////////////////////////////////
// Publisher :  Selig Multimedia, Inc.
// Title     :  PRNewsChannel [ English ]
////////////////////////////////////////////////////////////////////////
class syndParser_600 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getHeadline(&$text) {
    $headline = $this->getElementByName('title', $text);
    return $headline;
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    return $this->textFixation($this->getCData($this->getElementByName('content:encoded', $text)));
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");

    return trim($this->textFixation($this->getElementByName('dc:creator', $text)));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getElementsByName('category', $text);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = $this->textFixation($this->getCData($cat));
      }
    }
    return implode(', ', $originalCats);
  }

  protected function getImages(&$text) {
    $story = $this->getElementByName('content:encoded', $text);
    $imagesArray = array();
    preg_match_all("/<img[^>]+\>/i", $story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");

      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath_orginal = $imageInfo[0];
      $imagePath = explode('?', $imagePath_orginal);
      $imagePath = $imagePath[0];
      if (!$imagePath) {
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      if($this->checkImageifCached($imagePath)){
        // Image already parsed..
        continue;
      }
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath_orginal, $images[0]['img_name'], $img);
      $text = str_replace($img, $new_img, $text);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}