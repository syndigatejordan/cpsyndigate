<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Red Bull Media House GmbH  [# publisher id =1263 ] 
//Title      : Red Bull - Content Pool - Stories [ English ] 
//Created on : Oct 1, 2019, 11:35:33 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5793 extends syndParseXmlContent {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('fullArticle', $fileContents);
    return $articles;
  }

  public function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $article = $this->getElementByName("descriptiveProduct", $text);
    return trim($this->textFixation($this->getElementByName('productTitle', $article)));
  }

  public function getArticleDate(&$text) {
    $this->addLog("getting article date");

    $article = $this->getElementByName("descriptiveProduct", $text);
    $date = trim($this->getElementByName('activationDate', $text));
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  public function getArticleOriginalId($params = array()) {
    $articleOriginalId = $this->getElementByName("id", $params['text']);
    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }

    return $this->title->getId() . '_' . sha1($articleOriginalId);
  }

  public function getArticleReference(&$text) {
    $this->addLog('Getting article Link');
    $id = $this->getElementByName("id", $text);
    $url = "https://www.redbullcontentpool.com/international/$id";
    return $url;
  }

  public function getAbstract(&$text) {
    $this->addLog('Getting article summary');
    $article = $this->getElementByName("descriptiveProduct", $text);
    return trim($this->textFixation($this->getElementByName('description', $article)));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $category = array();
    $topics = $this->getElementsByName("topic", $text);
    foreach ($topics as $topic) {
      $cat = $this->getElementByName("item", $topic);
      $category[$cat] = $cat;
    }
    $category = trim(implode(", ", $category));
    return $category;
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $article = $this->getElementByName("descriptiveProduct", $text);
    $summary = trim($this->textFixation($this->getElementByName('description', $article)));
    if (!empty($summary)) {
      $summary = "<p>$summary</p>";
    }
    $this->story = $summary . $this->textFixation($this->getCData($this->getElementByName('text', $article)));
    $this->story = trim(preg_replace('!\s+!', ' ', $this->story));
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getImages(&$text) {

    $imagesArray = array();
    $imgs = array();
    preg_match_all("/<mediaProduct>(.*?)<\/mediaProduct>/i", $text, $imgs);
    foreach ($imgs[0] as $img) {

      $this->addLog("getting article images");
      if (preg_match("/<description>(.*?)<\/description>/i", $img, $image_caption)) {
        $image_caption = strip_tags($this->textFixation($image_caption[1]));
      } else {
        $image_caption = "";
      }

      $resources = $this->getElementByName('resources', $img);
      $resources = $this->getElementsByName('item', $resources);
      $resources = $resources[0];
      $imagePath = $this->getElementByName('url', $resources);
      $images = array();

      $images['img_name'] = $imagePath;
      $images['original_name']= $imagePath;
      $images['image_caption'] = $image_caption;
      $images['is_headline'] = true;
      $images['image_original_key'] = sha1($imagePath);
      array_push($imagesArray, $images);
    }
    $images = $this->getElementByName('images', $text);
    $images = $this->getElementByName('items', $images);
    $imgs = array();
    preg_match_all("/<item><_meta><links>(.*?)<\/resourceIds><\/item>/i", $images, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      if (preg_match("/<description>(.*?)<\/description>/i", $img, $image_caption)) {
        $image_caption = strip_tags($this->textFixation($image_caption[1]));
      } else {
        $image_caption = "";
      }

      $resources = $this->getElementByName('resources', $img);
      $resources = $this->getElementsByName('item', $resources);
      $resources = $resources[0];
      $imagePath = $this->getElementByName('url', $resources);
           $images = array();

      $images['img_name'] = $imagePath;
      $images['original_name']= $imagePath;
      $images['image_caption'] = $image_caption;
      $images['is_headline'] = false;
      $images['image_original_key'] = sha1($imagePath);
      array_push($imagesArray, $images);
    }
    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    sleep(1);
    $baseName = sha1($imageUrl) . ".jpeg";
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );

      $ch = curl_init($imageUrl);
      curl_setopt_array($ch, $options);
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $article = $this->getElementByName("descriptiveProduct", $text);
    $date = trim($this->getElementByName('activationDate', $text));
    $date = date('Y-m-d h:i:s', strtotime($date));
    $videos = array();
    $items = $this->getElementByName('videos', $text);
    $items = $this->getElementByName('items', $items);
    $imgs = array();
    preg_match_all("/<item><_meta><links>(.*?)<\/resourceIds><\/item>/i", $items, $imgs);

    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      if (preg_match("/<productTitle>(.*?)<\/productTitle>/i", $img, $image_caption)) {
        $videoName = strip_tags($this->textFixation($image_caption[1]));
      } else {
        $videoName = "";
      }

      $resources = $this->getElementByName('resources', $img);
      $resources = $this->getElementsByName('item', $resources);
      $resources = $resources[0];
      $videoname = $this->getElementByName('url', $resources);
      $mimeType = "";
      $video['video_name'] = $videoname;
      $video['original_name'] = $videoName;
      $video['video_caption'] = $videoName;
      $video['mime_type'] = $mimeType;
      $video['added_time'] = $date;
      $videos[] = $video;
    }
    return $videos;
  }

}
