<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Point Press  [# publisher id = 961]
//Title      : The Point Newspaper [ English ] 
//Created on : May 19, 2016, 11:01:27 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2762 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}