<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Pro Publica, Inc.  [# publisher id = 1289]
//Title      : ProPublica [ English ] 
//Created on : Jan 28, 2020, 1:03:44 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5903 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}