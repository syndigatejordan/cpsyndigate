<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Newstex, LLC  [# publisher id = 1519]
//Title      : ALJAZEERA Balkans [ Bosnian ] 
//Created on : Feb 23, 2022, 8:40:46 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12480 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('bs'); 
	} 
}