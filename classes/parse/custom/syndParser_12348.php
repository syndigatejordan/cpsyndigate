<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Radio Africa Group of companies  [# publisher id = 1744]
//Title      : The Star [ English ] 
//Created on : May 09, 2022, 2:33:59 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12348 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}