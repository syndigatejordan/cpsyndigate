<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : HT Media Ltd.  [# publisher id = 89]
//Title      : Hindustan Times Business [ English ] 
//Created on : Jan 04, 2022, 1:34:06 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12299 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}