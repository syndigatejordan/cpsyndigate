<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IFR / PFI  [# publisher id = 1700]
//Title      : PFI Daily News (Ticker) [ English ] 
//Created on : Jan 06, 2022, 11:56:24 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12301 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}