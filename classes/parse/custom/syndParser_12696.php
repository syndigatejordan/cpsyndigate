<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Observ Algérie  [# publisher id = 1837]
//Title      : ObservAlgerie.com [ French ] 
//Created on : Aug 10, 2022, 11:18:54 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12696 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}