<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Seeking Alpha Ltd.  [# publisher id = 1675]
//Title      : Seeking Alpha – Research/Analysis (Headlines + Summary) [ English ] 
//Created on : May 01, 2022, 7:49:10 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12591 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}