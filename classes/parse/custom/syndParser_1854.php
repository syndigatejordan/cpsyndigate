<?php

///////////////////////////////////////////////////////////////////////////
//  Please note to all the titles for publisher  Asianet-Pakistan (Pvt) Ltd.  [# publisher id =83 ] based on this title   
//  Be careful when you change                                                                                        
//////////////////////////////////////////////////////
//
// Publisher : Asianet-Pakistan (Pvt) Ltd.  [# publisher id =83 ] 
// Titles    : Accounting Today
///////////////////////////////////////////////////////////////////////////

class syndParser_1854 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->charEncoding = 'UTF-8';
    //$this->extensionFilter = 'xml';
  }

  public function getRawArticles(&$fileContents) {
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('DigestName', $fileContents);
  }

  public function getStory(&$text) {

      $this->addLog("getting article text");
      $story = trim($this->getElementByName('Text', $text));
      $story = '<p>' . str_replace(".\r\n", ".\r\n" . "</p>\r\n<p>", $story) . '</p>';
      $story = preg_replace('/<img[^>]+\>/i', '', $story);
      return $story;
  }

  public function getHeadline(&$text) {
    $this->addLog("getting article headline");

    return trim($this->getElementByName('Headline', $text));
  }

  public function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = trim($this->getElementByName('Date', $text));
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  public function getAuthor(&$text) {
    $this->addLog("getting article Source");
    return trim($this->getElementByName('Source', $text));
  }

  protected function getAbstract(&$text) {
    $this->addLog("getting article summary");
    return $this->textFixation($this->getElementByName('ByLine', $text));
  }

}
