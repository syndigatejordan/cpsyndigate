<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Le Quatrieme Pouvoir  [# publisher id = 1829]
//Title      : Le Quatrieme Pouvoir [ French ] 
//Created on : Jul 20, 2022, 12:35:41 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12674 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}