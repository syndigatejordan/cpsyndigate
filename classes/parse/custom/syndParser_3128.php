<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Kenya News Agency  [# publisher id = 1139]
//Title      : Kenya News Agency [ English ] 
//Created on : Sep 11, 2017, 11:09:29 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3128 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}