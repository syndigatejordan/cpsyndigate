<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : coindesk  [# publisher id = 1838]
//Title      : CoinDesk Korea [ Korean ] 
//Created on : Aug 10, 2022, 12:26:40 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12698 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ko'); 
	} 
}