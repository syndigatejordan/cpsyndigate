<?php

////////////////////////////////////////////////////////////////////////
// Publisher : India Today Group  
// Titles    : Business Today
////////////////////////////////////////////////////////////////////////

class syndParser_294 extends syndParseXmlContent {
  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->charEncoding = 'ISO-8859-1';
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    $this->addLog("getting articles raw text");
    $file = $this->getElementsByName('artical', $fileContents);
    $file = $file[0];
    $articles = explode('<section', $file);
    unset($articles[0]);

    foreach ($articles as &$article) {
      $article = '<section' . $article;
    }
    return $articles;
  }

  protected function getOriginalCategory(&$text) {
    $this->addLog("getting article original category");
    $category= $this->getElementsByName('section', $text);
    return trim($category[0]);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementsByName('edition', $text);
    return date('Y-m-d', strtotime($date[0]));
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return trim($this->textFixation($this->getElementByName('byline', $text)));
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return $this->textFixation($this->getElementByName('headline', $text));
  }

  protected function getAbstract(&$text) {
    $this->addLog("getting article Abstract");
    return $this->textFixation($this->getElementByName('introtext', $text));
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = $this->textFixation($this->getElementByName('bodytext', $text));

    $story = str_replace('<copyright>', '<p>', $story);
    $story = str_replace('</copyright>', '</p>', $story);
    return $story;
  }

}
