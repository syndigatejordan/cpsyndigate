<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Automobil KONSTRUKTION  [# publisher id = 1702]
//Title      : Automobil KONSTRUKTION [ German ] 
//Created on : Jan 11, 2022, 10:31:50 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12306 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('de'); 
	} 
}