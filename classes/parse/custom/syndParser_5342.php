<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Emirates News Agency (WAM)  [# publisher id = 32]
//Title      : Emirates News Agency (WAM) [ Farsi ] 
//Created on : Feb 10, 2019, 9:06:42 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5342 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fa'); 
	} 
}