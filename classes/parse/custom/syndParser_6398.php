<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Shephard Media  [# publisher id = 1352]
//Title      : Military Training Magazine - Shephard Media [ English ] 
//Created on : Aug 30, 2020, 8:58:32 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6398 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}