<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Motori News  [# publisher id = 1719]
//Title      : Motori News - Electric [ English ] 
//Created on : Jan 11, 2022, 2:18:54 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12324 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}