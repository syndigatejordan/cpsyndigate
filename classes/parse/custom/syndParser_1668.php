<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher :  	Gabonews publisher id =584
//Title     : Gabonews [ French ]
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_1668 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('fr');
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = strip_tags($this->story, '<table><tr><td><span><p><br><strong><b><u><i><a><sub><div>');
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getImages(&$text) {
    $this->story = $this->getCData($this->getElementByName('fulltext', $text));
    $this->story = str_replace("&amp;", "&", $this->story);
    $this->story = $this->textFixation($this->story);
    $this->imagesArray = array();
    preg_match_all("/<img[^>]+\>/i", $this->story, $imgs);

    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");

      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);

      $img = htmlspecialchars($img);
      $img = str_replace("&quot;", '"', $img);
      $new_img = htmlspecialchars($new_img);
      $new_img = str_replace("&quot;", '"', $new_img);

      $text = str_replace($img, $new_img, $text);

      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

}
