<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Madina Press Group  [# publisher id = 919]
//Title      : Al Madina [ Arabic ] 
//Created on : May 16, 2016, 9:29:52 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2713 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}