<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IISD International Institute for Sustainable Development  [# publisher id = 1777]
//Title      : International Institute for Sustainable Development (IISD) - News [ English ] 
//Created on : Feb 28, 2022, 12:58:03 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12499 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}