<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : iMENA Services  [# publisher id =705 ]  
//Title     : iMENA Perspectives 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2230 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

}
