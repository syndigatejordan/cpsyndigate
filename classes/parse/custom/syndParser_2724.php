<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Awareness Times Newspaper  [# publisher id = 929]
//Title      : Awareness Times [ English ] 
//Created on : May 16, 2016, 10:53:20 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2724 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}