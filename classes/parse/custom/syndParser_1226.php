<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Mehrdad Boostanchi  [# publisher id = 363]
//Title      : Iranian Advertising Journal - Danashe Tablighat [ English ] 
//Created on : Feb 02, 2016, 11:50:55 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1226 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}