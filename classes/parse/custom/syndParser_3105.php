<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Motivate publishing  [# publisher id = 116]
//Title      : Hype [ English ] 
//Created on : Sep 10, 2017, 11:22:19 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3105 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}