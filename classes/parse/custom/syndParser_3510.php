<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Turkish Radio and Television Corporation  [# publisher id = 1180]
//Title      : TRT World [ English ] 
//Created on : Mar 25, 2020, 1:13:38 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3510 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
