<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Cynomedia Africa SARL  [# publisher id = 1481]
//Title      : Journal du Gabon [ French ] 
//Created on : Sep 05, 2021, 1:34:33 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7390 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}