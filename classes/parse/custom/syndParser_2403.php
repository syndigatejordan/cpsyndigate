<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Alexandria Chamber of Commerce  [# publisher id =769 ] 
//Title     : Alexandria Chamber of Commerce News [ English ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2403 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

}
