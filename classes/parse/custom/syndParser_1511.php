<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : The Philippine Daily Inquirer, Inc.  [# publisher id =506 ] 
// Titles    : INQUIRER.net [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1511 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('nitf', $fileContents);
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = utf8_encode($this->getElementByName('body.content', $text));
    $this->story = strip_tags($this->story, '<p><br><strong><b><u><i>');
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  public function getAbstract(&$text) {
    $this->addLog('Getting article summary');
    $this->abs = utf8_encode(strip_tags($this->getElementByName('abstract', $text)));
    return $this->abs;
  }

  public function getHeadline(&$text) {
    $head = trim($this->getElementByName('body.head', $text));
    $headline = utf8_encode(strip_tags($this->getElementByName('hedline', $head)));
    return $headline;
  }

  public function getArticleDate(&$text) {
    $head = trim($this->getElementByName('body.head', $text));
    $dateline = trim($this->getElementByName('dateline', $head));
    $dateInfo = syndParseHelper::getImgElements($dateline, 'story.date', 'norm');
    return date('Y-m-d', strtotime($dateInfo[0]));
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $head = trim($this->getElementByName('body.head', $text));
    $b = trim($this->textFixation($this->getElementByName('byline', $head)));
    return trim($this->textFixation($this->getElementByName('person', $b)));
  }

  public function getOriginalCategory(&$text) {
    //$fixture = trim($this->getElementByName('fixture', $text));
    $category = syndParseHelper::getImgElements($text, 'fixture','fix-id');    
    return $category[0];
  }

}