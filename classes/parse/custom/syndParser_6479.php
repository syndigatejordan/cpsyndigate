<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sahara Press Service  [# publisher id = 1378]
//Title      : Sahara Press Service [ Russian ] 
//Created on : Oct 07, 2020, 12:48:15 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6479 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}