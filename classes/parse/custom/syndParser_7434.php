<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Media OutReach Newswire  [# publisher id = 1172]
//Title      : Media OutReach Newswire [ simplified Chinese ] 
//Created on : Sep 14, 2021, 8:27:55 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7434 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh-Hans'); 
	} 
}