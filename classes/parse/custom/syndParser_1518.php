<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Interface Médias Sarl  [# publisher id = 509]
//Title      : Maghreb Emergent [ French ] 
//Created on : Apr 07, 2019, 11:28:58 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1518 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('fr');
  }
}
