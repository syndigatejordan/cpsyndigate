<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Rysha Media LLC  [# publisher id = 1212]
//Title      : Blockchain Advisor [ English ] 
//Created on : Nov 29, 2021, 9:11:13 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5368 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}