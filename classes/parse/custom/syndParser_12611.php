<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : China Economic Net  [# publisher id = 1797]
//Title      : Economic Daily News [ Japanese ] 
//Created on : May 10, 2022, 11:13:20 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12611 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	} 
}