<?php
///////////////////////////////////////////////////////////////////////////
// Publisher : Press Services & Documentaiton Bureau
// Titles    : The Middle East Reporter (MER) [ English ] 
///////////////////////////////////////////////////////////////////////////


class syndParser_597 extends syndParseRss {
	
	public function customInit() {
		parent::customInit();
		$this->defaultLang 	= $this->model->getLanguageId('en');
	}

	public function getStory(&$text) {
		$this->addLog('Getting article story');
		$story = parent::getStory($text);
		
		$story = str_replace("\r", '', $story);
		$story = str_replace("\n", "<br />\n", $story);
		
		return $story;
	}
}
