<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Vibe Ghana  [# publisher id = 1157]
//Title      : Vibe Ghana [ English ] 
//Created on : Sep 04, 2017, 7:50:10 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3153 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}