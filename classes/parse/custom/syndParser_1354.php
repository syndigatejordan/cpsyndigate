<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Info-Prod Research (Middle East) Ltd.  [# publisher id =417 ]
// Titles    : IPR Strategic Information Database [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1354 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->charEncoding = "ISO-8859-1";
    $this->extensionFilter = array('txt', 'TXT');
  }

  protected function getFileContents(&$file) {
    $content = parent::getFileContents($file);
    $content = str_replace('', "'", stripcslashes($content));
    return $content;
  }

  public function getRawArticles(&$fileContents) {
    $content = explode("\n\n\n", $fileContents);
    $contents = array();
    foreach ($content as $id => $cont) {
      if (!empty($cont)) {
        $contents[] = $cont;
      }
    }/*
      $contt = explode("\n\n", $cont);

      foreach ( $contt as $item => $con ) {
      $contt[$item] = '<headline>' . trim($contt[0]) . '</headline>';

      $contt[$item] = '<pubdate>' . trim($contt[1]) . '</pubdate>';

      $contt[$item] = '<body>' . trim($contt[2]) . '</body>';

      var_dump($contt);exit;
      }
      $contt = implode('', $contt);
      $contents[]  = $contt;
      }
      } */
    return $contents;
  }

  public function getHeadline(&$text) {
    $this->addLog('Getting article headline');
    $headline = explode("\n", $text);
    return trim($headline[0]);
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
 $story = explode("\n", $text);
    $story = trim($story[4]);
    $story = '<p>' . str_replace(".\r\n", ".\r\n" . "</p>\r\n<p>", $story) . '</p>';
    return $story;
  }

  public function getArticleDate(&$text) {
    $this->addLog('Getting article date');
    $date = explode("\n", $text);
    $date = trim($date[2]);
     $date = explode("2015", $date);
    $date = $date[0] . "2015";
    return date('Y-m-d', strtotime($date));
  }

}
