<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Vacca Foeda Media  [# publisher id = 1255]
//Title      : Today I Found Out [ English ] 
//Created on : Sep 19, 2021, 8:52:29 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5670 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}