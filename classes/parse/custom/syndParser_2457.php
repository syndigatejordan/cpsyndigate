<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Mideast Time Group  [# publisher id = 800]
//Title      : Mideast Times [ English ] 
//Created on : Jan 30, 2016, 6:41:54 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2457 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}