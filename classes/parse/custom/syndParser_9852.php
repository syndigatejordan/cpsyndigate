<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Government of the Republic of Lithuania  [# publisher id = 1526]
//Title      : Government of Lithuania - News [ English ] 
//Created on : Feb 22, 2021, 7:26:07 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_9852 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}