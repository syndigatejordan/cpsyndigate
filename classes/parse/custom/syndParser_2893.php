<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : African Press Organization (APO)  [# publisher id = 448]
//Title      : MENA Wire® [ English ] 
//Created on : Oct 31, 2021, 10:24:20 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2893 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}