<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : BANG Media International  [# publisher id = 306]
//Title      : BANG ShowBiz [ Japanese ] 
//Created on : Sep 13, 2021, 12:04:04 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1020 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	} 
}