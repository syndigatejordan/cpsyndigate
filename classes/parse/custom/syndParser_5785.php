<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Elmahatta  [# publisher id = 1261]
//Title      : Elmahatta [ Arabic ] 
//Created on : Sep 27, 2021, 8:00:49 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5785 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}