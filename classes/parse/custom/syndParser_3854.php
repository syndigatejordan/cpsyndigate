<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : News Deeply  [# publisher id = 423]
//Title      : Women's Advancement Deeply [ English ] 
//Created on : Sep 16, 2018, 12:12:20 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3854 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    
    $this->story = preg_replace('/(<[^>]+) data-image-meta=".*?"/i', '$1', $this->story);
    
    $this->story = preg_replace('/<style(.*?)<\/style>/is', '', $this->story);
    $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) srcset=".*?"/i', '$1', $this->story);
    
    $this->story = preg_replace('/(<[^>]+) width=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) height=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) data-permalink=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) data-orig-file=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) data-orig-size=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) data-comments-opened=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) data-image-title=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) data-image-description=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) data-medium-file=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) data-large-file=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) data-attachment-id=".*?"/i', '$1', $this->story);
    
    $this->story = trim(preg_replace('!\s+!', ' ', $this->story));
    $this->story = str_replace("<aside>", "", $this->story);
    $this->story = str_replace("</aside>", "", $this->story);
   
    return $this->story;
  }

  protected function getImages(&$text) {
      $imagesArray = array();
      $this->story = $this->getCData($this->getElementByName('fulltext', $text));
      $this->story = str_replace("&nbsp;", " ", $this->story);
      $this->story = $this->textFixation($this->story);
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();

      preg_match_all("/<img(.*?)>/i", $this->story, $imgs);
      foreach ($imgs[0] as $img) {
          $this->addLog("getting article images");
          $image_caption = '';
          $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
          if (!empty($image_caption[0])) {
              $image_caption = $image_caption[0];
          } else {
              $image_caption = "";
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      $imagePath = preg_replace("/\?(.*)/is", "", $imagePath);
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = basename($imageUrl);
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $imageUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
