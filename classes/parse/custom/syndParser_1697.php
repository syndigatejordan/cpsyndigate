<?php 
		///////////////////////////////////////////////////////////////////////////////////// 
		//Publisher : Tel Aviv Stock Exchange (TASE)  
		//Title     : Tel Aviv Stock Exchange (TASE) [ Hebrew ] 
		/////////////////////////////////////////////////////////////////////////////////////
		
		class syndParser_1697 extends syndParseCms { 
			 public function customInit() { 
				 parent::customInit(); 
				 $this->defaultLang = $this->model->getLanguageId('he'); 
			} 
		}