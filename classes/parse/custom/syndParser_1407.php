<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : mid-east information  [# publisher id = 449]
//Title      : Mid-East.Info [ English ] 
//Created on : Aug 15, 2021, 7:15:32 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1407 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}