<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Télam S.E. Agencia Nacional de Noticias  [# publisher id = 1398]
//Title      : Télam newswire  [ Spanish ] 
//Created on : Sep 27, 2021, 7:06:54 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12172 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}