<?php
////////////////////////////////////////////////////////////////////////
// Publisher :  As'ad AbuKhalil
// Title     :  The Angry Arab News Service [Arabic]
////////////////////////////////////////////////////////////////////////

class syndParser_877 extends syndParseRss {
	private $category = 'اﻷخبار';
	
	public function customInit() {
		parent::customInit();
		$this->defaultLang = $this->model->getLanguageId('ar');	
	}
	
	public function getRawArticles(&$fileContents) {  
      		//get raw articles
		$this->addLog("getting raw articles text");
		$rawArticles = array();
		$allArticles = parent::getRawArticles($fileContents);
		foreach($allArticles as $article) {
			$articleDate = $this->getArticleDate($article);
			$articleDate = explode('-', $articleDate);
			
			if(mktime(0, 0, 0, 6, 18, 2011)<=mktime(0, 0, 0, $articleDate[1], $articleDate[2], $articleDate[0])) {
			    $rawArticles[] = $article;	
			}
		}
		return $rawArticles;
	}



	protected function getStory(&$text) {
		$this->addLog("getting article story");
		$story = $this->textFixation($this->getCData($this->getElementByName('content:encoded', $text)));
		$story = strip_tags($story, '<div><p><br>');
		$story = preg_replace("/<div[^>]+\>/i", "<div>", $story);

		return $story;
	}

	protected function getAuthor(&$text){
		$this->addLog("getting article author");
		return $this->textFixation($this->getCData($this->getElementByName('dc:creator', $text)));
	}
		
	
	protected function getIptcId(&$text) {
		$this->addLog("getting category name");

		$iptcId = $this->model->getIptcCategoryId($this->category);
		return ($iptcId ? $iptcId : parent::getIptcId($text));
	}


	protected function getOriginalCategory(&$text) {
		//get article text
		$this->addLog("getting article original category");
		return $this->category;
	}

	public function getArticleOriginalId($params = array()) {
		$articleOriginalId = $this->getElementByName('headline', $params['text']);
		$articleOriginalId .= $this->getElementByName('pubDate', $params['text']);
	
		$articleOriginalId = trim($this->getCData($articleOriginalId));
		if(!$articleOriginalId) {
			return parent::getArticleOriginalId($params);				
		} else {
			
			return $this->title->getId() . "_" . sha1($articleOriginalId);
		}
	}
}



