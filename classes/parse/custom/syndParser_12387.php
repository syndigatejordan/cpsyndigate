<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Hidrocalidodigital.com  [# publisher id = 1756]
//Title      : Hidrocalidodigital.com [ Spanish ] 
//Created on : Feb 08, 2022, 2:30:12 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12387 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}