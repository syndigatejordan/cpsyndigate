<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Coconuts Media Limited  [# publisher id = 1468]
//Title      : BK Magazine [ English ] 
//Created on : Aug 15, 2021, 12:04:14 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7362 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}