<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Assirou Global Media  [# publisher id = 1743]
//Title      : Assirou Global Media [ French ] 
//Created on : Jan 19, 2022, 8:34:18 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12347 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}