<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Danish ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_576 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority' 		=> 	'Ordregivende myndighed',
										'Nature of Contract'		=>	'Arten af kontrakten', 
										'Regulation of Procurement'	=>	'Regulering af indkøb', 
										'Type of bid required'		=> 	'Type bud, der kræves', 
										'Award Criteria'			=>	'Tildelingskriterier', 
										'Original CPV'				=> 	'Original CPV', 
										'Country'					=>	'Land', 
										'Document Id' 				=>	'Dokument Id', 
										'Type of Document'			=>	'Type dokument', 
										'Procedure'					=>	'Procedure', 
										'Original Language'			=>	'Original Sprog', 
										'Current Language'			=> 	'Nuværende Sprog');	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('da');
		}
	}