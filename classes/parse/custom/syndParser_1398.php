<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Globes Publisher Itonut (1983) Ltd.  [# publisher id = 443]
//Title      : Globes [ English ] 
//Created on : Sep 26, 2021, 8:21:42 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1398 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}