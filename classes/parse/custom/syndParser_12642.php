<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Tunisie News  [# publisher id = 1810]
//Title      : Tunisie News [ French ] 
//Created on : Jun 16, 2022, 11:52:03 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12642 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}