<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Pressenza  [# publisher id = 1305]
//Title      : Pressenza [ Turkish ] 
//Created on : Feb 15, 2020, 6:32:00 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5922 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('tr');
  }
}
