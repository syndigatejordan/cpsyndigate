<?php

///////////////////////////////////////////////////////////////////////////
//Publisher : DAFEDServ  [# publisher id =652 ]  
//Titles    : DAFE DATA - African Stock Exchange Data Feed
//Created on : Feb 16, 2017, 08:09:21 AM
//Author     : eyad
///////////////////////////////////////////////////////////////////////////
class syndParser_1840 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'htm';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = array();
    $article = NUll;
    preg_match('/<body>(.*?)<\/body>/is', $fileContents, $article);
    $article = trim(preg_replace('/<!--(.*?)-->/is', "", $article[1]));
    $article = preg_replace("/(<[^>]+) style='.*?'/i", '$1', $article);
    $article = preg_replace('/(<[^>]+) class=(.*?)>/i', '$1>', $article);
    $article = preg_replace('/(<[^>]+) width=(.*?)>/i', '$1>', $article);
    $article = preg_replace('/(<[^>]+) height=(.*?)>/i', '$1>', $article);
    $article = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $article);
    $article = trim(str_replace('<td></td>', "", $article));
    $article = trim(str_replace('<col>', "", $article));
    $article = trim(str_replace('<![if supportMisalignedColumns]>', "", $article));
    $article = trim(str_replace('<![endif]>', "", $article));
    $article = strip_tags($article, "<table><tr><td>");
    $article = preg_replace('!\s+!', ' ', $article);
    $article = preg_replace('/(<[^>]+) width=(.*?)>/i', '$1>', $article);
    array_push($articles, $article);
    return $articles;
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = $text;
    $story = preg_replace('/<table (.*?)>/is', '<table border="1">', $story);
    $story = trim(preg_replace('!\s+!', ' ', $story));
    return $story;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = null;
    preg_match_all('/<tr>(.*?)<\/tr>/is', $text, $headline);
    $headline = $headline[0][1];
    $text = str_replace($headline, "", $text);
    $headline = trim(strip_tags($headline));
    return $headline;
  }

  protected function getArticleDate(&$text) {
    //get article date
    $this->addLog("getting article date");
    $date = null;
    preg_match_all('/<tr>(.*?)<\/tr>/is', $text, $date);
    $date = $date[0][0];
    $text = str_replace($date, "", $text);
    $date = trim(strip_tags($date));
    $date = explode('-', $date);
    if (empty($date[1])) {
      $date = explode('/', $date[0]);
    }
    $date = $date[2] . "-" . $date[1] . "-" . $date[0];
    return $date;
  }

}
