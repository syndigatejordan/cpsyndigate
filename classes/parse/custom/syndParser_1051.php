<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al-Akhbar  [# publisher id = 316]
//Title      : Al-Akhbar [ Arabic ] 
//Created on : Mar 28, 2018, 2:41:32 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1051 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}