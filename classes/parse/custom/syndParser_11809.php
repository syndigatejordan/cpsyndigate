<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  :  Comunicação do Agreste e Sertão Ltda  [# publisher id = 1593]
//Title      : Tribuna do Sertao [ Portuguese ] 
//Created on : Jul 06, 2021, 8:14:18 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11809 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}