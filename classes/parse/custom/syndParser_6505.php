<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ANDINA - Agencia Peruana de Noticias  [# publisher id = 1388]
//Title      : ANDINA - Peru News Agency [ English ] 
//Created on : Oct 15, 2020, 11:48:37 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6505 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}