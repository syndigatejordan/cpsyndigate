<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Tripoli News Network (TNN)  [# publisher id = 1191]
//Title      : Tripoli News Network (TNN) [ Arabic ] 
//Created on : Aug 30, 2018, 7:10:13 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3821 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}