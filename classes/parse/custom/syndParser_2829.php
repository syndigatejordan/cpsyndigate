<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Eurabia Media Corporation FZ LLC  [# publisher id = 1000]
//Title      : Amwal-mag.com [ Arabic ] 
//Created on : Aug 22, 2016, 1:38:54 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2829 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}