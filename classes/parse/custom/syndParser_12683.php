<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Global Crypto Press Association  [# publisher id = 1834]
//Title      : The Global Crypto Press Association [ English ] 
//Created on : Aug 03, 2022, 9:03:07 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12683 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}