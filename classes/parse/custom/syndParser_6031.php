<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Arcturus Publishing Limited  [# publisher id = 1334]
//Title      : Color by Number - The 'Animal Kingdom' Issue [ English ] 
//Created on : Apr 22, 2020, 8:11:08 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6031 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}