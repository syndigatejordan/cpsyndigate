<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : La Nation  [# publisher id = 1141]
//Title      : La Nation [ French ] 
//Created on : Mar 13, 2019, 3:07:56 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3134 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}