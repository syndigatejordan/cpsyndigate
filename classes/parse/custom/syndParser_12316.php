<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : EuroTransportMedia Publishing and Events GmbH  [# publisher id = 1713]
//Title      : FIRMENAUTO [ German ] 
//Created on : Jan 11, 2022, 11:24:31 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12316 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('de'); 
	} 
}