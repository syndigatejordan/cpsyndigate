<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : 24.com  [# publisher id = 1131]
//Title      : Kouga Express [ English ] 
//Created on : Aug 29, 2017, 10:35:49 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3129 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}