<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Addy.media LLC  [# publisher id = 1270]
//Title      : Addy.media - Music [ English ] 
//Created on : Jan 07, 2020, 8:02:51 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5895 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}