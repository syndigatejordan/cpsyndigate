<?php
///////////////////////////////////////////////////////////////////////////
// Publisher : 	Bikya Masr 
// Titles    :  Bikya Masr [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_876 extends syndParseRss {
	
	private $story;

	public function customInit() {
		parent::customInit();
		$this->defaultLang = $this->model->getLanguageId('en');	
	}
	
	protected function getStory(&$text) {
		$this->addLog("getting article story");
		
		$story = strip_tags($this->story,'<p>,<br>');
		preg_replace("/<p[^>]+\>/i",'<p>', $story);

		return $story;
	}

	protected  function getImages(&$text) {
		$this->story = $this->textFixation($this->getElementByName('content:encoded', $text));
		
		$imagesArray = array();
		$imgs	     = array();
		
		preg_match("/<img[^>]+\>/i", $this->story, $imgs);
		foreach ($imgs as $img) {
			$this->addLog("getting article images");
			$imagePath = syndParseHelper::getImgElements($img);
			$imagePath = $imagePath[0];
				
			if(!$imagePath) {					
			   continue;
			}
		  if($this->checkImageifCached($imagePath)){
        // Image already parsed..
        continue;
      }	
			$copiedImage = $this->copyUrlImgIfNotCached($imagePath);
				
			if(!$copiedImage) {
			   continue;
			}		
			$images = $this->getAndCopyImagesFromArray(array($copiedImage));
			array_push($imagesArray, $images[0]);
		}
		
		return $imagesArray;
			
	}
	
}
