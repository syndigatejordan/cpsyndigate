<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Somali National News Agency (SONA)  [# publisher id = 1380]
//Title      : Somali National News Agency (SONA) [ Arabic ] 
//Created on : Sep 19, 2021, 8:55:16 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6485 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}