<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Mirror  [# publisher id = 1524]
//Title      : The Masvingo Mirror [ English ] 
//Created on : Feb 23, 2021, 12:27:39 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_9509 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}