<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Global Crypto Press Association  [# publisher id = 1834]
//Title      : The Global Crypto Press Association [ Swedish ] 
//Created on : Aug 07, 2022, 11:25:56 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12693 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('sv'); 
	} 
}