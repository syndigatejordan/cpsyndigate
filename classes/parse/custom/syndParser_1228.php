<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Sun Publishing Ltd.  [# publisher id =364 ] 
//Title      : The Sun [ English ] 
//Created on : Aug 20, 2019, 8:34:22 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_1228 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
