<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Shephard Media  [# publisher id = 1352]
//Title      : Naval Warfare Magazine - Shephard Media [ English ] 
//Created on : Aug 30, 2020, 8:58:29 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6401 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}