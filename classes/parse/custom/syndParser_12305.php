<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Autocar Professional  [# publisher id = 1701]
//Title      : Autocar Professional [ English ] 
//Created on : Jan 11, 2022, 10:27:52 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12305 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}