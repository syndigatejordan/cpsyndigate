<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Killid Group  [# publisher id = 1668]
//Title      : The Killid Group [ English ] 
//Created on : Oct 10, 2021, 10:44:25 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12231 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}