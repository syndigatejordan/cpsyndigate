<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : iCrowdNewswire, LLC  [# publisher id = 1535]
//Title      : La Republica [ Spanish ] 
//Created on : Mar 08, 2021, 9:10:23 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10326 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}