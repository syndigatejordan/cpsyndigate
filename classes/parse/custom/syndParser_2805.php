<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Aen Hail Newspaper  [# publisher id = 985]
//Title      : Aenhail.com [ Arabic ] 
//Created on : Aug 10, 2016, 6:56:06 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2805 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $body = $this->getElementByName('fulltext', $text);
    $body = html_entity_decode($body);
    $body = str_replace("&amp;", "&", $body);
    $body = str_replace("[embedded content]", "", $body);
    $body = str_replace("&amp;nbsp;", "& ", $body);
    $body = str_replace("&;nbsp;", "", $body);
    $body = html_entity_decode($body, ENT_QUOTES, "UTF-8");
    $body = preg_replace('/<img[^>]+\>/i', '', $body);

      $body = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $body);
      $body = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $body);
      $body = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $body);
      $body = preg_replace('/<a href="http:\/\/www.aenhail.com.sa(.*?)jpg"><\/a>/', "", $body);
      $body = preg_replace('/<a href="http:\/\/www.aenhail.com.sa(.*?)png"><\/a>/', "", $body);

      $body = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $body);
      $body = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $body);
      $body = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $body);
      $body = preg_replace("!\s+!", " ", $body);
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $body = preg_replace('/<img[^>]+\>/i', '', $body);

      return $body;
  }
}
