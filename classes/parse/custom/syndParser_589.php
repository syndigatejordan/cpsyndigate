<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Slovak ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_589 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority'			=>	'Obstarávateľa', 
										'Nature of Contract'			=> 	'Charakter zmluvy', 
										'Regulation of Procurement' 	=> 	'Nariadenie o verejnom obstarávaní', 
										'Type of bid required'			=>	'Typ ponuky požadovaných', 
										'Award Criteria'				=>	'Kritériá pre prideľovanie grantov', 
										'Original CPV'					=> 	'Pôvodná CPV', 
										'Country'						=>	'Krajina', 
										'Document Id' 					=>	'Dokument Id', 
										'Type of Document'				=>	'Druh dokumentu', 
										'Procedure'						=>	'Postup', 
										'Original Language'				=>	'Pôvodný jazyk', 
										'Current Language'				=> 	'Aktuálny jazyk');	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('sk');
		}
	}