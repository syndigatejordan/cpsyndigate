<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sultanate of Oman Ministry of Foreign Affairs  [# publisher id = 879]
//Title      : Sultanate of Oman Ministry of Foreign Affairs - News [ English ] 
//Created on : Feb 23, 2016, 3:41:38 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2627 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}