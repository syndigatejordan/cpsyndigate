<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Motivate publishing  [# publisher id = 116]
//Title      : Business traveller Middle East [ English ] 
//Created on : Nov 03, 2020, 2:30:32 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6359 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	}

    //Handle empty body
    public function parse() {
        $articles = array();
        foreach ($this->files as $file) {
            if ($this->extensionFilter) {
                if (!syndParseHelper::isFileExtension($file, $this->extensionFilter)) {
                    $this->addLog("Wrong extension for file : $file)");
                    continue;
                }
            }
            if (!file_exists($file)) {
                $this->addLog("file dose not exist: $file)");
                continue;
            }

            $this->addLog("get file contents (file:$file)");
            $fileContents = $this->getFileContents($file);

            if (!$fileContents) {
                continue;
            }

            $this->currentlyParsedFile = $file;
            $this->loadCurrentDirectory();
            $this->loadUpperDir();
            $rawArticles = $this->getRawArticles($fileContents);
            foreach ($rawArticles as $rawArticle) {
                $article = $this->getArticle($rawArticle);
                $articles[] = $article;
            }
        }
        return $articles;
    }

}