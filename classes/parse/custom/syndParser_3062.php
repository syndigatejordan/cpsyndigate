<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Omar AlArifi  [# publisher id = 1101]
//Title      : AlThawagah [ Arabic ] 
//Created on : Sep 26, 2021, 10:57:12 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3062 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}