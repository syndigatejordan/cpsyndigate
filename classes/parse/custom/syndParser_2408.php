<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Erbil Chamber of Commerce and Industry  [# publisher id =771 ]  
//Title     : Erbil Chamber of Commerce & Industry News  [ arabic ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2408 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

}
