<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : bne Intellinews  [# publisher id = 1624]
//Title      : bne Intellinews [ English ] 
//Created on : Sep 08, 2021, 12:31:02 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12169 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}