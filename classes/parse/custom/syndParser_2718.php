<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Wahda Foundation For Press, Printing and Publishing  [# publisher id = 917]
//Title      : Al Thawra [ Arabic ] 
//Created on : May 19, 2016, 7:26:11 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2718 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}