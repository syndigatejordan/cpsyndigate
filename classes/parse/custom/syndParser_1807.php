<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Namibia Economist  
//Title     : Namibia Economist [ English ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1807 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

}
