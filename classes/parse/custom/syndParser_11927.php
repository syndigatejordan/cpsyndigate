<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : iCrowdNewswire, LLC  [# publisher id = 1535]
//Title      : El Universal [ Spanish ] 
//Created on : Jul 01, 2021, 1:39:07 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11927 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}