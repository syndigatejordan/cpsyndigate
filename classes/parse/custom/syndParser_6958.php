<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Paradise Media Venture  [# publisher id = 1444]
//Title      : Paradise News [ English ] 
//Created on : Jul 06, 2021, 7:12:27 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_6958 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
