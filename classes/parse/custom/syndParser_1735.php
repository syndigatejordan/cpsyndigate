<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Mediaquest Corp  [# publisher id = 84]
//Title      : AMEinfo.com [ English ] 
//Created on : Aug 10, 2021, 10:01:49 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1735 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}