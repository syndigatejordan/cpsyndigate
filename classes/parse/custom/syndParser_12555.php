<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IDG Communications, Inc.  [# publisher id = 1790]
//Title      : MACWORLD UK [ English ] 
//Created on : Apr 05, 2022, 11:52:53 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12555 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}