<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : First Reports  [# publisher id = 1672]
//Title      : First Reports [ English ] 
//Created on : Nov 03, 2021, 8:29:55 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12237 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}