<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : ITP Business Publishing Ltd.
// Titles    : ITP.net [Arabic]
///////////////////////////////////////////////////////////////////////////
class syndParser_905 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('NewsComponent', $fileContents);
  }

  protected function getStory(&$text) {
    //get body text
    $this->addLog("getting article text");
    $blocks = $this->getElementsByName('DataContent', $text);
    $pattern = '\<!--\[if gte mso \d+\]\>.*\<!\[endif\]--\>';
    //echo preg_replace("/$pattern/", '', $this->getCData($blocks[2]));
    $body = html_entity_decode($this->textFixation($this->getCData($blocks[2])), ENT_QUOTES, 'UTF-8');
    return $body;
  }

  protected function getOriginalCategory(&$text) {

    $this->addLog("getting article Category");
    $blocks = $this->getElementsByName('DataContent', $text);
    return $this->textFixation($blocks[0]);
  }

  protected function getHeadline(&$text) {
    //get headline
    $this->addLog("getting article headline");
    $headline = $this->getElementsByName('Headline', $text);
    return $this->textFixation($headline[0]);
  }

  protected function getArticleDate(&$text) {
    //get article date
    $this->addLog("getting article date");
    $date = $this->getElementsByName('DateLine', $text);
    return $this->dateFormater($date[0]);
  }

  protected function getIptcId(&$text) {
    $this->addLog("getting category name");
    $originalCategoryName = $this->getElementsByName('DataContent', $text);
    $iptcId = $this->model->getIptcCategoryId($originalCategoryName[0]);
    if (!$iptcId) {
      return parent::getIptcId(&$text);
    }
    return $iptcId;
  }

  protected function getOriginalData(&$text) {
    $originalData['extras']['distributor'] = $this->getElementByName('ProviderId', $text);
    $originalData['extras']['location'] = $this->getElementByName('location', $text);
    return $originalData;
  }

  protected function getAbstract(&$text) {
    //get abstract
    $this->addLog("getting article abstract");
    $bodyHead = $this->getElementByName('body\.head', $text);
    return $this->textFixation($this->getElementByName('abstract', $bodyHead));
  }

  protected function getAuthor(&$text) {
    //get author
    $this->addLog("getting article author");
    $creator = $this->getElementsByName('Creator', $text);
    $matches = array();
    preg_match('/Contribution FormalName="(.*)"/', $creator[0], $matches);
    $author = $this->textFixation($matches[1]);
    return $author;
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $images = $text;
    $this->addLog("getting article images");
    $imageInfo = syndParseHelper::getImgElements($images, 'ContentItem', 'Href');
    $imageInfo = str_replace("http://itp.nethttp://www.itp.net", "http://itp.net", $imageInfo[0]);
    $imagePath = str_replace(' ', '%20', $imageInfo);
    if (!$imagePath) {
      return;
    }
    if ($image = $this->checkImageifCached($imagePath)) {
      // Image already parsed..
      return;
    }
    $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
    if (!$copiedImage) {
      echo "no path" . PHP_EOL;
      return;
    }
    $images = $this->getAndCopyImagesFromArray(array($copiedImage));
    $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
    $images[0]['is_headline'] = false;
    array_push($imagesArray, $images[0]);
    return $imagesArray;
  }

  public function getArticleOriginalId($params = array()) {
    $articleOriginalId = (int) $this->getElementByName('NewsItemId', $params['text']);

    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }

    return $this->title->getId() . '_' . sha1($articleOriginalId) . '_' . $articleOriginalId;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = basename($imageUrl);
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );
      $ch = curl_init($imageUrl);
      curl_setopt_array($ch, $options);
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
