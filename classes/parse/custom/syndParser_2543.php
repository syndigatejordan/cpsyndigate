<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Galadari Printing & Publishing LLC  [# publisher id = 28]
//Title      : WKND [ English ] 
//Created on : Jan 30, 2016, 8:56:57 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2543 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
