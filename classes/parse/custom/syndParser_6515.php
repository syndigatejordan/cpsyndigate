<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Kabulpress.org  [# publisher id = 75]
//Title      : KabulPress.org [ Persian ] 
//Created on : Aug 19, 2021, 9:24:02 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6515 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fa'); 
	} 
}