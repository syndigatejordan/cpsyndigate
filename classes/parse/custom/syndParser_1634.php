<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Moby Group  [# publisher id =565 ] 
//Title      : TOLOnews [ Farsi ] 
//Created on : May 03, 2017, 05:30:28 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_1634 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('fa');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    return $this->story;
  }

  protected function getImages(&$text) {
      $this->story = $this->textFixation($this->getCData($this->getElementByName('fulltext', $text)));
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();

      $imagesArray = array();
      preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
      foreach ($imgs[0] as $img) {
          $this->addLog("getting article images");
          $image_caption = '';
          $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
          if (!empty($image_caption[0])) {
              $image_caption = $image_caption[0];
          } else {
              $image_caption = "";
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      $imagePath = preg_replace("/\?(.*)/is", "", $imagePath);
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
