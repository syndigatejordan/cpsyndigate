<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Euronews SA  [# publisher id = 604]
//Title      : euronews [ French ] 
//Created on : May 21, 2019, 11:36:01 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1733 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}