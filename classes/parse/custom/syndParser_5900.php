<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Kataeb.org  [# publisher id = 1288]
//Title      : Kataeb.org [ English ] 
//Created on : Dec 03, 2020, 11:41:21 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5900 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}