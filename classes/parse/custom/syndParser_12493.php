<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IFC - International Finance Corporation  [# publisher id = 1773]
//Title      : IFC (World Bank Group) [ Russian ] 
//Created on : Feb 28, 2022, 12:29:50 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12493 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}