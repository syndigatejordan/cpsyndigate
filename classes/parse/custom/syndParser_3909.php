<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The New York Times Company / nytLicensing  [# publisher id = 1197]
//Title      : T (The New York Times Style Magazine) - Video [ English ] 
//Created on : Oct 19, 2018, 4:51:30 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3909 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getStory(&$text) {
    $story = trim($this->textFixation($this->getCData($this->getElementByName('media:description', $text))));
    if (!empty($story)) {
      $story = "<p>$story</p>";
    }
    $keywords = trim($this->textFixation($this->getCData($this->getElementByName('media:keywords', $text))));
    if (!empty($keywords)) {
      $keywords = "<p>$keywords</p>";
    }
    $tags = trim($this->textFixation($this->getCData($this->getElementByName('media:tags', $text))));
    if (!empty($tags)) {
      $tags = "<p>$tags</p>";
    }
    $story = $story . $keywords . $tags;
    return $story;
  }

  public function getHeadline(&$text) {
    $this->headline = trim($this->getElementByName('media:title', $text));
    return $this->headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    $date = trim(str_replace('/\+(.*)/is', '', $date));
    return date('Y-m-d', strtotime($date));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = trim($this->getElementByName('media:category', $text));
    return $cats;
  }

  protected function getImages(&$text) {
    $imagesArray = array();
    $image_caption = $this->textFixation($this->getCData($this->getElementByName('media:title', $text)));
    $enclosure = null;
    preg_match("/<media:thumbnail[^>]+\>/is", $text, $enclosure);
    $imageInfo = syndParseHelper::getImgElements($enclosure[0], 'media:thumbnail', 'url');
    $imagePath = $imageInfo[0];
    if ($imagePath) {
      $this->addLog("getting article images");
      if (!$this->checkImageifCached($imagePath)) {
        $imagePath = str_replace(' ', '%20', $imagePath);
        $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
        if ($copiedImage) {
          $images = $this->getAndCopyImagesFromArray(array($copiedImage));
          if (!empty($image_caption)) {
            $images[0]['image_caption'] = $image_caption;
          }
          $name_image = explode('/images/', $copiedImage);
          if ($images[0]['image_caption'] == $name_image[1]) {
            $images[0]['image_caption'] = '';
          }
          $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
          $images[0]['is_headline'] = TRUE;
          array_push($imagesArray, $images[0]);
        }
      }
    }
    return $imagesArray;
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $matches = null;
    $videos = array();
    preg_match_all('/<media:content(.*?)>/is', $text, $matches);
    foreach ($matches[0] as $match) {
      $video = array();
      $videoname = syndParseHelper::getImgElements($match, 'media:content', 'url');
      $link = $videoname[0];
      $videoName = $this->textFixation($this->getCData($this->getElementByName('media:title', $text)));
      $video['video_name'] = $link;
      $video['original_name'] = $videoName;
      $video['video_caption'] = $videoName;

      $type = syndParseHelper::getImgElements($match, 'media:content', 'type');
      $type = $type[0];
      $video['mime_type'] = $type;
      $videoInformation = array();
      $videoInformation["type"] = $type;
      //////////////////bit_rate////////////////////////
      $lang = syndParseHelper::getImgElements($match, 'media:content', 'lang');
      if (isset($lang[0])) {
        $videoInformation["lang"] = $lang[0];
      }
      $duration = syndParseHelper::getImgElements($match, 'media:content', 'duration');
      if (isset($duration[0])) {
        $videoInformation["duration"] = $duration[0];
      }
      $medium = syndParseHelper::getImgElements($match, 'media:content', 'medium');
      if (isset($medium[0])) {
        $videoInformation["medium"] = $medium[0];
      }
      $framerate = syndParseHelper::getImgElements($match, 'media:content', 'framerate');
      if (isset($framerate[0])) {
        $videoInformation["framerate"] = $framerate[0];
      }
      $fileSize = syndParseHelper::getImgElements($match, 'media:content', 'fileSize');
      if (isset($fileSize[0])) {
        $videoInformation["fileSize"] = $fileSize[0];
      }
      $height = syndParseHelper::getImgElements($match, 'media:content', 'height');
      if (isset($height[0])) {
        $videoInformation["height"] = $height[0];
      }
      $width = syndParseHelper::getImgElements($match, 'media:content', 'width');
      if (isset($width[0])) {
        $videoInformation["width"] = $width[0];
      }
      $bitrate = syndParseHelper::getImgElements($match, 'media:content', 'bitrate');
      if (isset($bitrate[0])) {
        $videoInformation["bitrate"] = $bitrate[0];
      }
      $samplingrate = syndParseHelper::getImgElements($match, 'media:content', 'samplingrate');
      if (isset($samplingrate[0])) {
        $videoInformation["samplingrate"] = $samplingrate[0];
      }
      //////////////////bit_rate////////////////////////
      $video['bit_rate'] = json_encode($videoInformation);
      $date = $this->getElementByName('pubDate', $text);
      $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
      $videos[] = $video;
    }
    return $videos;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = sha1($imageUrl) . ".jpeg";
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $opts = array();
      $http_headers = array();
      $http_headers[] = 'Expect:';

      $opts[CURLOPT_URL] = $imageUrl;
      $opts[CURLOPT_HTTPHEADER] = $http_headers;
      $opts[CURLOPT_CONNECTTIMEOUT] = 10;
      $opts[CURLOPT_TIMEOUT] = 60;
      $opts[CURLOPT_HEADER] = FALSE;
      $opts[CURLOPT_BINARYTRANSFER] = TRUE;
      $opts[CURLOPT_VERBOSE] = FALSE;
      $opts[CURLOPT_SSL_VERIFYPEER] = FALSE;
      $opts[CURLOPT_SSL_VERIFYHOST] = 2;
      $opts[CURLOPT_RETURNTRANSFER] = TRUE;
      $opts[CURLOPT_FOLLOWLOCATION] = TRUE;
      $opts[CURLOPT_MAXREDIRS] = 2;
      $opts[CURLOPT_IPRESOLVE] = CURL_IPRESOLVE_V4;

      # Initialize PHP/CURL handle
      $ch = curl_init();
      curl_setopt_array($ch, $opts);
      $imgContent = curl_exec($ch);

      # Close PHP/CURL handle
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        //unlink($copiedImage);
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
