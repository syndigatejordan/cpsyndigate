<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Quest FZE  [# publisher id = 388]
//Title      : The Turbulent World of Middle East Soccer [ English ] 
//Created on : Sep 23, 2021, 7:44:04 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1284 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}