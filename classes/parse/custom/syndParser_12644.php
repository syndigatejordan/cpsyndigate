<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : INFORMACIEL COMMUNICATION  [# publisher id = 1812]
//Title      : TogoFoot [ French ] 
//Created on : Jun 16, 2022, 12:48:50 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12644 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}