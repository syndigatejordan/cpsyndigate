<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : BBC   [# publisher id =1192 ] 
// Titles    : BBC Arabic - Videos [ Arabic ]
//Created on : Sep 17, 2018, 11:35:22 AM
//Author     : eyad
///////////////////////////////////////////////////////////////////////////

class syndParser_3838 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
    $this->extensionFilter = 'xml';
  }

  public function getArticleReference(&$text) {
    $this->addLog('Getting article Link');
    return '';
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = preg_replace("/<img[^>]+>/i", '', $this->story);
    $this->story = preg_replace("/<video(.*?)<\/video>/is", '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getImages(&$text) {
    $this->imagesArray = array();
    $this->story = $this->getCData($this->getElementByName('fulltext', $text));
    $this->story = str_replace("&amp;", "&", $this->story);
    $this->story = $this->textFixation($this->story);
    $imgs = array();
    preg_match_all("/<img[^>]+>/i", $this->story, $imgs);

    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
      if (!empty($image_caption[0])) {
        $image_caption = $image_caption[0];
      } else {
        $image_caption = "";
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $title = $this->textFixation($this->getElementByName('title', $text));
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath, $title);

      if (!$copiedImage) {
        echo "no path" . PHP_EOL;
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

  protected function getVideos(&$text) {

    $videos = array();
    $story = $this->getCData($this->getElementByName('fulltext', $text));
    $story = str_replace("&nbsp;", " ", $story);
    $story = $this->textFixation($story);
    $story = trim(preg_replace('!\s+!', ' ', $story));
    $matches = null;
    preg_match_all('/<video(.*?)<\/video>/is', $story, $matches);
    foreach ($matches[0] as $match) {
      $this->addLog("Getting videos");
      $video = array();
      $videoInfo = syndParseHelper::getImgElements($match, 'source', 'src');
      $videoInfo = $videoInfo[0];
      if (preg_match("/youtube.com/", $videoInfo)) {
        continue;
      }

      $link = $this->copyUrlImgIfNotCached($videoInfo);
      if (file_exists($link)) {
        $name = basename($link);
        $video_name = $this->model->getVidReplacement($name, $this->imgCacheDir, 3838);
        $video_name = str_replace(VIDEOS_PATH, VIDEOS_HOST, $video_name);
        $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
        $video = array();
        $video['video_name'] = $video_name;
        $video['original_name'] = $videoName;
        $video['video_caption'] = $videoName;
        $mimeType = "";
        $video['mime_type'] = $mimeType;
        $video['video_type'] = $mimeType;
        $date = $this->getElementByName('date', $text);
        $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
      } else {
        $video = array();
      }
      $videos[] = $video;
    }
    return $videos;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = basename($imageUrl);
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $imageUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
