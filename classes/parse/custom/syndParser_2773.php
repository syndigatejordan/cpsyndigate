<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dar Alyaum Press & Publishing  [# publisher id = 971]
//Title      : Alyaum [ Arabic ] 
//Created on : May 01, 2018, 11:21:03 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2773 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');

    $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('/<td id="photocap">(.*?)<\/td>/si', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getImages(&$text) {
      $this->story = $this->getCData($this->getElementByName('fulltext', $text));
      $this->story = str_replace("&amp;", "&", $this->story);
      $this->story = $this->textFixation($this->story);
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();
      
      $imagesArray = array();
      preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
      foreach ($imgs[0] as $img) {
          $img = str_replace("onerror=\"this.src='images/no-image.png'\"", "", $img);
          $this->addLog("getting article images");
          $image_caption = '';
          $image_caption = explode('caption="', $img);
          $image_caption = explode('"', $image_caption[1]);
          $image_caption = $image_caption[0];
          $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    $baseName = basename($imageUrl);
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $imageUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
