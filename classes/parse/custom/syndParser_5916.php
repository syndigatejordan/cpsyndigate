<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Wellcome Trust  [# publisher id = 1301]
//Title      : Mosaic [ English ] 
//Created on : Feb 10, 2020, 11:46:50 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5916 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
