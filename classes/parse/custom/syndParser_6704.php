<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Belarusian Telegraph Agency (BelTA)  [# publisher id = 1434]
//Title      : Belarusian Telegraph Agency (BelTA) [ Chinese ] 
//Created on : Oct 22, 2020, 7:19:52 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6704 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('zh'); 
	} 
}