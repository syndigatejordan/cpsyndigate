<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Massif Society  [# publisher id = 488]
//Title      : Yerepouni Daily News [ Arabic ] 
//Created on : Sep 23, 2021, 7:46:46 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1470 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}