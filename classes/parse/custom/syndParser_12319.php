<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : INFOPRO Digital  [# publisher id = 1716]
//Title      : Autos Infos - Energy Transition [ French ] 
//Created on : Jan 11, 2022, 12:16:25 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12319 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}