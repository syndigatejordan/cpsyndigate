<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Turkmenistan State News Agency (TDH)  [# publisher id = 1430]
//Title      : Turkmenistan State News Agency (TDH) [ English ] 
//Created on : Oct 22, 2020, 1:42:59 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6674 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}