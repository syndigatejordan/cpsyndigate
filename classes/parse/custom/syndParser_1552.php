<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Fuseworks Limited  [# publisher id =517 ]  
// Titles    : Fuseworks [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1552 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('nitf', $fileContents);
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $body = trim($this->getElementByName('body.content', $text));
    $this->story = $this->textFixation($this->getElementByName('body.content', $text));
    $this->story = strip_tags($this->story, '<p><br><strong><b><u><i>');
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  public function getHeadline(&$text) {
    $head = trim($this->getElementByName('body.head', $text));
    $headline = strip_tags($this->getElementByName('hedline', $head));
    return $headline;
  }

  public function getArticleDate(&$text) {
    $head = trim($this->getElementByName('head', $text));
    $dateline = trim($this->getElementByName('docdata', $head));
    $dateInfo = syndParseHelper::getImgElements($dateline, 'date.issue', 'norm');
    return date('Y-m-d', strtotime($dateInfo[0]));
  }

  public function getOriginalCategory(&$text) {
    $DataContent = trim($this->getElementByName('head', $text));
    $cat = syndParseHelper::getImgElements($DataContent, 'meta name ="subcategory"', 'content');
    $cat = $cat[0];
    return $cat;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $DataContent = trim($this->getElementByName('head', $text));
    $auther = syndParseHelper::getImgElements($DataContent, 'meta name ="author"', 'content');
    return $auther[0];
  }

  public function getAbstract(&$text) {
    $this->addLog('Getting article summary');
    $this->abs = trim(strip_tags($this->getElementByName('byline', $text)));
    return $this->abs;
  }

}
