<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Harayer Publishing Group  [# publisher id = 1021]
//Title      : HarayerMagazine.com [ Arabic ] 
//Created on : Aug 22, 2016, 8:34:12 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2850 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}