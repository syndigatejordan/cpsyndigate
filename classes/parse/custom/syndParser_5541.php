<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : CG Arabia SPC.  [# publisher id = 366]
//Title      : Bahrain Confidential [ English ] 
//Created on : Apr 17, 2019, 8:13:00 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5541 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) srcset=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);

      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);

      if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
