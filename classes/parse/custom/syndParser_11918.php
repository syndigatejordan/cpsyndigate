<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : iCrowdNewswire, LLC  [# publisher id = 1535]
//Title      : El Comercio [ Spanish ] 
//Created on : Jul 01, 2021, 1:29:55 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11918 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('es'); 
	} 
}