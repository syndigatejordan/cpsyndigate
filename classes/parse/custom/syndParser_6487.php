<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : GroupWeb Media Network  [# publisher id = 1381]
//Title      : ArabNewsWire [ English ] 
//Created on : Sep 20, 2020, 7:53:42 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_6487 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
