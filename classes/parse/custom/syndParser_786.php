<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Universal Solutions S.A.E.  [# publisher id = 224]
//Title      : Gulf Oil & Gas [ English ] 
//Created on : Aug 09, 2021, 10:04:33 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_786 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}