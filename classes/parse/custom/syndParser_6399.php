<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Shephard Media  [# publisher id = 1352]
//Title      : Naval Warfare - Shephard Media [ English ] 
//Created on : Sep 19, 2021, 8:54:09 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6399 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}