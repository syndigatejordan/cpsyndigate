<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence Malienne de Presse et de Publicité  [# publisher id = 1344]
//Title      : Agence Malienne de Presse et de Publicité (AMAP) [ French ] 
//Created on : Aug 15, 2021, 10:08:15 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6077 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}