<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : RFE/RL, Inc.  [# publisher id = 1651]
//Title      : Gandhara [ English ] 
//Created on : Oct 04, 2021, 1:38:55 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12205 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}