<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Standard Group  [# publisher id = 1155]
//Title      : Money Maker [ English ] 
//Created on : May 19, 2022, 7:57:42 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12409 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}