<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Middle East Monitor  [# publisher id = 1266]
//Title      : The Middle East Monitor - Infographics [ English ] 
//Created on : Oct 13, 2019, 10:40:02 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5841 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}