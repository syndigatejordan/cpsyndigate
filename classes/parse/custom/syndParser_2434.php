<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : Hebron Chamber of Commerce and Industry  [# publisher id =787 ] 
//Title     : Hebron Chamber of Commerce and Industry News [ arabic ]
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2434 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = trim($this->textFixation($this->getCData($this->getElementByName('fulltext', $text))));
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    $this->story = html_entity_decode($this->story, ENT_QUOTES, "UTF-8");
    if (empty($this->story)) {
      return "";
    } else {
      return $this->story;
    }
  }

}
