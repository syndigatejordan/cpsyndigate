<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Lebanese InformationTechnology Services (LITS)  [# publisher id = 1345]
//Title      : RLL (Radio Liban Libre) [ Arabic ] 
//Created on : Feb 24, 2022, 7:11:50 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7030 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}