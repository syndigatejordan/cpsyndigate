<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Finnish ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_591 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority'			=>	'Hankintaviranomaisen', 
										'Nature of Contract'			=> 	'Sopimuksen luonne', 
										'Regulation of Procurement' 	=> 	'Asetus hankinnat', 
										'Type of bid required'			=>	'Tyyppi vaadittavan', 
										'Award Criteria'				=>	'Tarjouskilpailun ratkaisuperusteet', 
										'Original CPV'					=> 	'Alkuperäinen CPV', 
										'Country'						=>	'Maa', 
										'Document Id' 					=>	'Asiakirjan tunnus', 
										'Type of Document'				=>	'Asiakirjalaji', 
										'Procedure'						=>	'Menettely', 
										'Original Language'				=>	'Alkuperäinen kieli', 
										'Current Language'				=> 	'Nykyinen kieli');	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('fi');
		}
	}