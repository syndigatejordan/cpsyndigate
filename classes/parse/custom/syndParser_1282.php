<?php

///////////////////////////////////////////////////////////////////////////
// Publisher :   Qatar News Agency (QNA)  [# publisher id =386 ] 
// Titles    :   Qatar News Agency (QNA) [Arabic]
///////////////////////////////////////////////////////////////////////////

class syndParser_1282 extends syndParseRss {

  public $story;
  protected $cat_translation;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
    $this->cat_translation['recent_news'] = 'اخر الاخبار';
    $this->cat_translation['economy'] = 'اقتصاد';
    $this->cat_translation['sports'] = 'رياضة';
    $this->cat_translation['miscellaneous'] = 'متفرقات';
    $this->cat_translation['general'] = 'عامة';
    $this->cat_translation['bulletin_economy'] = 'اقتصاد';
    $this->cat_translation['bulletin_culture'] = 'ثقافة';
    $this->cat_translation['bulletin_sports'] = 'رياضة';
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return $this->textFixation($this->getElementByName('title', $text));
  }

  public function getStory(&$text) {
    $this->addLog('getting article summary');
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = str_replace(']]>', '', $this->story);
    $this->story = html_entity_decode($this->story, ENT_QUOTES, "UTF-8");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    return $this->story;
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('Getting article category');
    $filename = basename($this->currentlyParsedFile);
    $filename = str_replace('.xml', '', $filename);
    if (isset($this->cat_translation[$filename])) {
      return $this->cat_translation[$filename];
    } else {
      return false;
    }
  }

  public function getArticleOriginalId($params = array()) {
    $this->addLog('getting article original Id');
    return $this->title->getId() . '_' . sha1($params['articleDate']) . '_' . sha1($params['headline']) . '_' . sha1($params['text']);
  }

  protected function getImages(&$text) {
    $this->story = $this->textFixation($this->getCData($this->getElementByName('description', $text)));
    $imagesArray = array();
    preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = explode('alt="', $img);
      $image_caption = explode('"', $image_caption[1]);
      $image_caption = $image_caption[0];
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['image_caption'] = str_replace('-', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}

?>