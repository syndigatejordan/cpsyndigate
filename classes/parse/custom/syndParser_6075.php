<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sanskriti Media  [# publisher id = 1343]
//Title      : Sanskriti Media - Archive Photos [ English ] 
//Created on : Jul 23, 2020, 1:34:05 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6075 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}