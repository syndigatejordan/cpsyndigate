<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : test publisher  [# publisher id = 50]
//Title      : Mariam Jallouq (UGC Images) [ English ] 
//Created on : Jul 09, 2019, 1:11:10 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5672 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}