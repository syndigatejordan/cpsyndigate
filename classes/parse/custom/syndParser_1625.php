<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Jagran Prakashan Ltd  [# publisher id =564 ] 
// Titles    : MiD-DAY [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1625 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  public function getRawArticles(&$fileContents) {
    $fileContents = str_replace('<article>', '', $fileContents);
    $fileContents = str_replace('</article>', '', $fileContents);
    $exploded = explode('<storyid>', $fileContents);
    $contents = array();

    foreach ($exploded as $record) {
      if ($record) {
        $contents[] = '<storyid>' . $record;
      }
    }
    unset($contents[0]);
    return $contents;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->textFixation($this->getCData($this->getElementByName('headline', $text)));
    $headline = trim(strip_tags($headline));
    return $headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getCData($this->getElementByName('postdate', $text));
    $date = date('Y-m-d', strtotime($date));
    return $date;
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $story = $this->getCData($this->getElementByName('CONTENT', $text));
    $story = preg_replace('/<img[^>]+\>/i', '', $story);
    $story = preg_replace('!\s+!', ' ', $story);
    return $story;
  }

  public function getOriginalCategory(&$text) {
    $cat = $this->getCData(trim($this->getElementByName('category', $text)));
    return $cat;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $auther = $this->getCData(trim($this->getElementByName('author', $text)));
    return $auther;
  }

  protected function getImages(&$text) {

    $story = trim($this->textFixation($this->getCData($this->getElementByName('CONTENT', $text))));
    $imagesArray = array();
    preg_match_all("/<img[^>]+\>/i", $story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");

      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];

      if (!$imagePath) {
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $text = str_replace($img, $new_img, $text);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
