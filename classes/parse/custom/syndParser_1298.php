<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Independent Newspapers (Pty) Limited  [# publisher id = 392]
//Title      : The Sunday Independent [ English ] 
//Created on : Jul 29, 2021, 8:35:36 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1298 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}