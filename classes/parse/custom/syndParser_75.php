<?php

///////////////////////////////////////////////////////////////////////////
// Publisher :  12 B.I.S SARL  [# publisher id =43 ] 
// Titles    :  tayyar.org [Arabic]
///////////////////////////////////////////////////////////////////////////

class syndParser_75 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('ar');
  }
}
