<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Infos Plus Gabon  [# publisher id = 1160]
//Title      : Infos Plus Gabon [ English ] 
//Created on : Sep 23, 2021, 7:54:38 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3172 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}