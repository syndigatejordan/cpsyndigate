<?php

///////////////////////////////////////////////////////////////////////////
// Publisher :   Qatar News Agency (QNA)  [# publisher id =386 ] 
// Titles    :   Qatar News Agency (QNA) [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1281 extends syndParseRss  {
        
    public function customInit() {
        parent::customInit();
        $this -> defaultLang = $this -> model -> getLanguageId('en');
        }
    
    public function getStory(&$text) {
        $this -> addLog('getting article summary');
        $summary = $this -> textFixation($this -> getElementByName('description', $text));
        return $summary;
    }    
    public function getOriginalCategory(&$text) {

    $this -> addLog('Getting article category');

    $filename = basename($this -> currentlyParsedFile);
    $filename = str_replace('.xml', '', $filename);
    return $filename;
    }
}
?>