<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Bendel Newspapers Company Limited (BNCL)  [# publisher id = 960]
//Title      : The Nigerian Observer [ English ] 
//Created on : Jul 14, 2016, 8:42:44 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2761 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}