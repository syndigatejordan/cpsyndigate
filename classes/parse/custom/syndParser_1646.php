<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : Cyber Media (India) Ltd
//Title     : Globalservicesmedia.com [ English ]
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1646 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('item', $fileContents);
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->getElementByName('title', $text);
    return $this->textFixation($headline);
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('pubDate', $text);
    //$date =  $this->dateFormater(substr($date, 5, 11));
    return date('Y-m-d', strtotime($date));
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = $this->getCData($this->getElementByName('content', $text));
    return $this->textFixation($story);
  }

  protected function getOriginalCategory(&$text) {
    $this->addLog("getting article category");
    $category = $this->getElementByName('category', $text);
    return $category;
  }

  public function getAbstract(&$text) {
    $this->addLog('Getting article summary');
    $this->abs = strip_tags($this->getElementByName('description', $text));
    return $this->abs;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getCData($this->getElementByName('source', $text)));
  }

  protected function getImages(&$text) {

    $story = trim($this->textFixation($this->getCData($this->getElementByName('content', $text))));
    $imagesArray = array();
    preg_match_all("/<img[^>]+\>/i", $story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");

      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];

      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $text = str_replace($img, $new_img, $text);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}
