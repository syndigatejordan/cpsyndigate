<?php

////////////////////////////////////////////////////////////////////////
// Publisher : India Today Group
// Titles    : Mail Today
////////////////////////////////////////////////////////////////////////

class syndParser_331 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    $this->addLog("getting articles raw text");
    $Article = null;
    preg_match_all('/<Article>(.*?)<\/Article>/is', $fileContents, $Article);
    return $Article[1];
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $Headline = trim($this->textFixation($this->getElementByName('Headline', $text)));
    return $Headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");

    $date = date('Y-m-d', strtotime($this->getElementByName('Publishdate', $text)));
    return $this->dateFormater($date);
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    return $this->textFixation($this->getElementByName('Bodytext', $text));
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getElementByName('Byline', $text));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog("getting original category");
    return $this->textFixation($this->getElementByName('ArticleName', $text));
  }

}

//End class
