<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ​The Egyptian Center for Economic Studies (ECES)  [# publisher id = 1203]
//Title      : ECES Economic Literature Review [ Arabic ] 
//Created on : Jan 03, 2019, 9:10:06 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5311 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}