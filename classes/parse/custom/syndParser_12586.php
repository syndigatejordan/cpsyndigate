<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Industry Dive  [# publisher id = 1740]
//Title      : Multifamily Dive [ English ] 
//Created on : Apr 27, 2022, 1:38:29 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12586 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}