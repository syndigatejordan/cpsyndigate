<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Central News Agency (CNA)  [# publisher id = 1406]
//Title      : Central News Agency (CNA) [ Japanese ] 
//Created on : Oct 19, 2020, 11:53:06 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6547 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	} 
}