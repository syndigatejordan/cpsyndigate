<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher  : France Médias Monde (FMM)  [# publisher id =383 ]   
//Title      : FRANCE 24  [Arabic]
//Created on : Oct 17, 2018, 14:48:41 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
class syndParser_3899 extends syndParseCms
{

    public function customInit()
    {
        parent::customInit();
        $this->charEncoding = 'UTF-8';
        $this->defaultLang = $this->model->getLanguageId('ar');
    }

//Handle empty body
    public function parse()
    {
        $articles = array();
    foreach ($this->files as $file) {
      if ($this->extensionFilter) {
        if (!syndParseHelper::isFileExtension($file, $this->extensionFilter)) {
          $this->addLog("Wrong extension for file : $file)");
          continue;
        }
      }
      if (!file_exists($file)) {
        $this->addLog("file dose not exist: $file)");
        continue;
      }

      $this->addLog("get file contents (file:$file)");
      $fileContents = $this->getFileContents($file);

      if (!$fileContents) {
        continue;
      }

      $this->currentlyParsedFile = $file;
      $this->loadCurrentDirectory();
        $this->loadUpperDir();
        $rawArticles = $this->getRawArticles($fileContents);
        foreach ($rawArticles as $rawArticle) {
            $article = $this->getArticle($rawArticle);
            $articles[] = $article;
        }
    }
        return $articles;
    }

    protected function getVideos(&$text)
    {
        $this->addLog("Getting videos");
        $matches = $this->textFixation($this->getElementByName('fulltext', $text));
        $videoInfo = syndParseHelper::getImgElements($matches, 'iframe', 'src');
        $link = $videoInfo[0];
        $videoName = $this->getElementByName('title', $text);
        $videos = array();
        $video['video_name'] = $link;
        $video['original_name'] = $videoName;
        $video['video_caption'] = $videoName;
        $video['mime_type'] = "";
        $date = $this->getElementByName('date', $text);
        $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
        $videos[] = $video;
        return $videos;
    }
  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('/<iframe(.*?)<\/iframe>/is', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
