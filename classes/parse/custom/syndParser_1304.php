<?php
///////////////////////////////////////////////////////////////////////////
// Publisher : Contify  [# publisher id =394 ] 
// Titles    : India Investment News [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1304 extends syndParseRss {
	public function customInit() {
		parent::customInit();
		$this -> charEncoding = 'UTF-8';
		$this -> defaultLang  = $this -> model -> getLanguageId('en');
	}
	
	protected function getAuthor(&$text) {
		$this -> addLog("getting article author");

		$author = $this -> getElementByName('dc:creator', $text);
		return $this -> textFixation($author);
	}

	protected function getOriginalCategory(&$text) {
		$this -> addLog("getting article original category");

		return 'India Investment';
	}

}