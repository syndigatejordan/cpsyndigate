<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sonapresse  [# publisher id = 1140]
//Title      : L' Union [ French ] 
//Created on : Jan 14, 2020, 9:10:39 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3133 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}