<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence Gabonaise de Presse  [# publisher id = 1137]
//Title      : Agence Gabonaise de Presse [ French ] 
//Created on : Sep 20, 2020, 9:39:17 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6451 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}