<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : AfreePress  [# publisher id = 1359]
//Title      : AfreePress [ French ] 
//Created on : Sep 19, 2021, 8:54:30 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6444 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}