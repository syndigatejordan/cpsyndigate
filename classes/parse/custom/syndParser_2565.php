<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ministry of Foreign Affairs - Arab Republic of Egypt  [# publisher id = 836]
//Title      : Ministry of Foreign Affairs of the Arab Republic of Egypt - Articles and Interviews [ Arabic ] 
//Created on : Feb 02, 2016, 12:13:57 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2565 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}