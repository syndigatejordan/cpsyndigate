<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Iran Chamber of Commerce, Industries, Mines & Agriculture  [# publisher id = 814]
//Title      : ICCIMA News Bulletin [ English ] 
//Created on : Feb 02, 2016, 11:46:07 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2496 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}