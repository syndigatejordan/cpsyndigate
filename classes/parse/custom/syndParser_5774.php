<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Hilal Publishing and Marketing Group  [# publisher id = 4]
//Title      : Gulf Daily News - Saudi Arabia News [ English ] 
//Created on : Sep 23, 2019, 11:01:28 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5774 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}