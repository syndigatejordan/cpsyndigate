<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IDG Communications, Inc.  [# publisher id = 1790]
//Title      : CSO GERMANY [ German ] 
//Created on : Apr 19, 2022, 11:15:10 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12545 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('de'); 
	} 
}