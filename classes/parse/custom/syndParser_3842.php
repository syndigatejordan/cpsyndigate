<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al Bawaba Middle East Limited  [# publisher id = 62]
//Title      : The Gateway - A Podcast from the Middle East (by albawaba.com) [ English ] 
//Created on : Aug 03, 2021, 1:54:12 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3842 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}