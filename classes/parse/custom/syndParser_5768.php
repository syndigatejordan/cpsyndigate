<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : China Daily Information Co.  [# publisher id = 498]
//Title      : China Daily - Business [ English ] 
//Created on : Sep 09, 2019, 11:22:54 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5768 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $this->story = trim(preg_replace('!\s+!', ' ', $this->story));
    $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) srcset=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) sizes=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) src-gif=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) width=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) height=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) data-lazy-sizes=".*?"/i', '$1', $this->story);
      return $this->story;
  }

    protected function getImages(&$text)
    {
        $imagesArray = array();
        $this->story = $this->getCData($this->getElementByName('fulltext', $text));
        $this->story = str_replace("&amp;", "&", $this->story);
        $this->story = $this->textFixation($this->story);

        //This action depends on "Task #1064 Remove all the images from our clients feeds"
        $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
        return array();

        preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
        foreach ($imgs[0] as $img) {
            $this->addLog("getting article images");
            $image_caption = '';
            $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
            if (!empty($image_caption[0])) {
                $image_caption = $image_caption[0];
            } else {
                $image_caption = syndParseHelper::getImgElements($img, 'img', 'title');
                if (!empty($image_caption[0])) {
                    $image_caption = $image_caption[0];
                } else {
                    $image_caption = "";
                }
            }
            $imageInfo = syndParseHelper::getImgElements($img, 'img');
            $imagePathold = $imageInfo[0];
            $imagePath = preg_replace("/\?(.*)/is", "", $imagePathold);
            if (!$imagePath) {
                continue;
            }
            $imagePath = str_replace(' ', '%20', $imagePath);
            $new_img = array();
            $new_img['img_name'] = $imagePath;
            $new_img['original_name'] = $imagePath;
            $new_img['image_caption'] = $image_caption;
            $new_img['is_headline'] = FALSE;
            $new_img['image_original_key'] = sha1($imagePath);
            array_push($imagesArray, $new_img);
        }
        return $imagesArray;
    }

    public function copyUrlImgIfNotCached($imageUrl)
    {
        sleep(1);
        if (!preg_match("/(https|http)/", $imageUrl)) {
            $imageUrl = "https:" . $imageUrl;
        }
        $baseName = sha1($imageUrl) . ".jpeg";
        $copiedImage = $this->imgCacheDir . $baseName;

        if (!is_dir($this->imgCacheDir)) {
            mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );

      $ch = curl_init($imageUrl);
      curl_setopt_array($ch, $options);
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
