<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Thies Info Thies TV  [# publisher id = 1742]
//Title      : Thies Info [ French ] 
//Created on : Jan 19, 2022, 7:55:26 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12346 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}