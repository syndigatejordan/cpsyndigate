<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : AgênciaBrasil (ABR)  [# publisher id = 1387]
//Title      : AgênciaBrasil (ABR) [ Portuguese ] 
//Created on : Sep 24, 2020, 12:46:39 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_6504 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('pt');
  }

}
