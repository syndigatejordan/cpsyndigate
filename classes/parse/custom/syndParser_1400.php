<?php
//////////////////////////////////////////////////////////////////////////////
// Publisher: Thai News Service  [# publisher id =444 ] 
// Titles   : Thai News Service [ English ]
//////////////////////////////////////////////////////////////////////////////

class syndParser_1400 extends syndParseXmlContent {

	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('en');
	}

	public function getRawArticles(&$fileContents) {
		$this -> addLog("getting articles raw element");

		return $this -> getElementsByName('nitf', $fileContents);
	}

	protected function getHeadline(&$text) {
		$this -> addLog("getting article headline");

		return $this -> textFixation($this -> getElementByName('hl1', $text));
	}

	protected function getStory(&$text) {
		$this -> addLog("getting article text");

		return $this -> textFixation($this -> getElementByName('body.content', $text));
	}

	protected function getArticleDate(&$text) {
		$this -> addLog("getting article date");

		$regExp = '<pubdata date.publication*=[\s]*([\"]([^>\"]*)[\"]|' . "[\\']([^>\\']*)[\\']|([^>\\s]*))([\\s]|[^>])*[\/]?>";
		preg_match_all("/$regExp/i", $text, $m);

		return $this -> dateFormater($m[2][0]);
	}

}
