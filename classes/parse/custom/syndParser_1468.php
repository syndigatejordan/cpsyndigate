<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Massif Society  [# publisher id = 488]
//Title      : Yerepouni Daily News [ English ] 
//Created on : Aug 15, 2021, 7:21:07 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1468 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}