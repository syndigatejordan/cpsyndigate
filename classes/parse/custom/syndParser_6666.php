<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Turkish Government  [# publisher id = 148]
//Title      : Anadolu Agency (AA) [ French ] 
//Created on : Oct 13, 2020, 9:37:32 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6666 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}