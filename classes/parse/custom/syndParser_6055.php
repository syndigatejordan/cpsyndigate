<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : SourceMedia, LLC d/b/a Arizent  [# publisher id = 1338]
//Title      : The Bond Buyer [ English ] 
//Created on : Jun 18, 2020, 7:06:14 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6055 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('nitf', $fileContents);
  }

  protected function getStory(&$text) {
    //get body text
    $this->addLog("getting article text");
    return $this->textFixation($this->getElementByName('body\.content', $text));
  }

  protected function getHeadline(&$text) {
    //get headline
    $this->addLog("getting article headline");
    $headline = $this->getElementByName('hedline', $text);
    return $this->textFixation($this->getElementByName('hl1', $text));
  }

  protected function getArticleDate(&$text) {
    //get article date
    $this->addLog("getting article date");
    $date = syndParseHelper::getImgElements($text, 'pubdata', 'date.publication');
    if ($date[0]) {
      return $this->dateFormater($date[0]);
    }
    return parent::getArticleDate($text);
  }

  protected function getOriginalCategory(&$text) {
    $this->addLog("getting article Category");
    $cat = syndParseHelper::getImgElements($text, 'tobject', 'tobject.type');
    if (isset($cat[0])) {
      return $cat[0];
    }
    return "";
  }

  protected function getAbstract(&$text) {
    //get abstract
    $this->addLog("getting article abstract");
    $bodyHead = $this->getElementByName('body\.head', $text);
    return $this->textFixation($this->getElementByName('abstract', $bodyHead));
  }

  protected function getAuthor(&$text) {
    //get author
    $this->addLog("getting article author");
    $bodyHead = $this->getElementByName('body\.head', $text);
    return strip_tags($this->textFixation($this->getElementByName('byline', $bodyHead)));
  }

  public function getArticleReference(&$text) {
    $link = syndParseHelper::getImgElements($text, 'pubdata', 'ex-ref');
    return $link[0];
  }

}

?>