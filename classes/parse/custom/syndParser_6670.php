<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Turkish Government  [# publisher id = 148]
//Title      : Anadolu Agency (AA) [ Persian ] 
//Created on : Oct 13, 2020, 9:37:39 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6670 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fa'); 
	} 
}