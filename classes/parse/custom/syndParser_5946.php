<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Yeni Gün Haber Ajansi Basin Ve Yayincilik A.Ş.   [# publisher id = 1312]
//Title      : Cumhuriyet Photos [ Turkish ] 
//Created on : Mar 01, 2020, 10:17:50 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5946 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('tr');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
