<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Centre for African Journalists (CAJ) News Africa  [# publisher id = 1370]
//Title      : Centre for African Journalists (CAJ) News Africa [ English ] 
//Created on : Sep 19, 2021, 8:54:51 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6462 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}