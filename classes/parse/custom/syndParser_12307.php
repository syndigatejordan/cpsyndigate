<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Autovia Ltd  [# publisher id = 1703]
//Title      : Driving Electric [ English ] 
//Created on : Jan 11, 2022, 10:37:31 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12307 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}