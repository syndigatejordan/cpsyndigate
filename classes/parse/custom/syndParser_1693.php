<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Muscat Securities Market (MSN)  [# publisher id =599 ] 
//Title      : Muscat Securities Market (MSN) [ Arabic ]
//Created on : Aug 22, 2017, 06:16:03 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_1693 extends syndParseCms
{

    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('ar');
    }


    protected function getImages(&$text)
    {
        $this->addLog("getting article image");
        $fulltext = $this->textFixation($this->getElementByName('fulltext', $text));
        if (preg_match("/<a(.*?)<\/a>/is", $fulltext, $match)) {
            $imageInfo = syndParseHelper::getImgElements($match[0], 'a', 'href');
            $imageInfo = $imageInfo[0];
        } else {
            return array();
        }
        $imagesArray = array();

        $imageInfo = str_replace(' ', '%20', $imageInfo);
        $image_caption = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
        $img['img_name'] = $imageInfo;
        $img['original_name'] = $imageInfo;
        $img['image_caption'] = $image_caption;
        $img['is_headline'] = FALSE;
        $img['image_original_key'] = sha1($imageInfo);
        array_push($imagesArray, $img);
        return $imagesArray;
    }

}
