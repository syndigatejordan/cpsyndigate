<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : SENENEWS  [# publisher id = 1781]
//Title      : Senenews.com [ French ] 
//Created on : Mar 06, 2022, 11:30:42 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12509 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}