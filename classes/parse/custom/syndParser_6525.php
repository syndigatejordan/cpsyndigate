<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The United Nations Department of Global Communications  [# publisher id = 1400]
//Title      : UN News [ Russian ] 
//Created on : Oct 20, 2020, 1:42:15 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6525 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}