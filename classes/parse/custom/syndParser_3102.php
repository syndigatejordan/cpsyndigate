<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Motivate publishing  [# publisher id = 116]
//Title      : Open Skies [ English ] 
//Created on : Jan 04, 2018, 9:43:56 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3102 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}