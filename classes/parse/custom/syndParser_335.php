<?php

////////////////////////////////////////////////////////////////////////
// Publisher : AllAfrica Global Media 
// Titles    : allAfrica.com [ French ]
////////////////////////////////////////////////////////////////////////

class syndParser_335 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('fr');
    //	$this->charEncoding		= 'ISO-8859-1';
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    $this->addLog("getting articles raw text");
    $data = $this->getElementsByName('AllAfrica_NewsStory', $fileContents);
    $data = str_replace("<publisher>", "<name_of_the_publisher>", $data);
    $data = str_replace("</publisher>", "</name_of_the_publisher>", $data);
    return $data;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->textFixation($this->getElementByName('publisher_headline', $text));
    return $headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('publication_date', $text);
    $date = substr($date, 0, 4) . '-' . substr($date, 4, 2) . '-' . substr($date, 6, 2);
    return $this->dateFormater($date);
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $headline = $this->textFixation($this->getElementByName('publisher_headline', $text));
    $publisher = $this->textFixation($this->getElementByName('publisher', $text));
    $publisherLocation = $this->textFixation($this->getElementByName('publisher_location', $text));
    $dateline = $this->textFixation($this->getElementByName('dateline', $text));
    $reporter = $this->textFixation($this->getElementByName('reporter', $text));
    $author = $publisher . " " . $publisherLocation . " " . $dateline . " " . $reporter;
    $author = str_replace($headline, "", $author);
    $author = trim(preg_replace('!\s+!', ' ', $author));
    return $author;
  }

  protected function getOriginalCategory(&$text) {
    $this->addLog("getting article original category");
    $kind_of_story = trim($this->textFixation($this->getElementByName('kind_of_story', $text)));
    $name_of_publisher = trim($this->textFixation($this->getElementByName('name_of_the_publisher', $text)));
    $cats = $this->getElementsByName('category', $text);
    $originalCats = array();
    if (!empty($cats)) {
      foreach ($cats as $cat) {
        if (empty($cat)) {
          continue;
        }
        $originalCats[] = $this->textFixation($cat);
      }
    }
    if (!empty($kind_of_story)) {
      $originalCats[] = $kind_of_story;
    }
    if (!empty($name_of_publisher)) {
      $originalCats[] = $name_of_publisher;
    }
    return implode(', ', $originalCats);
  }

  protected function getStory(&$text) {
      $this->addLog("getting article text");
      $dateline = $this->textFixation($this->getElementByName('dateline', $text));
      $story = $this->textFixation($this->getElementByName('body', $text));
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $story = $this->textFixation($dateline . " — " . $story);
      return $story;
  }

  public function getArticleReference(&$text) {
    $link = trim($this->textFixation($this->getCData($this->getElementByName('URL', $text))));
    return $link;
  }

}

//End class