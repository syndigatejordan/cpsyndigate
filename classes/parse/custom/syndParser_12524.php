<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : National Daily Newspaper L'Express  [# publisher id = 1787]
//Title      : National Daily Newspaper L'Express [ French ] 
//Created on : Mar 17, 2022, 10:55:15 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12524 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}