<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Citizens Union Foundation  [# publisher id = 1273]
//Title      : The Gotham Gazette [ English ] 
//Created on : Dec 16, 2019, 7:42:48 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5875 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }
}
