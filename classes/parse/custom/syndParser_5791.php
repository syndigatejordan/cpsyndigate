<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Red Bull Media House GmbH  [# publisher id =1263 ] 
//Title      : Red Bull - Content Pool - Photos [ English ] 
//Created on : Oct 1, 2019, 11:35:33 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5791 extends syndParseXmlContent {

  public $story;
  public $imagesArray = array();
  public $category;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('fullArticle', $fileContents);
    return $articles;
  }

  public function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return trim($this->textFixation($this->getElementByName('productTitle', $text)));
  }

  public function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = trim($this->getElementByName('firstActivationTimestamp', $text));
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  public function getArticleOriginalId($params = array()) {
    $articleOriginalId = $this->getElementByName("id", $params['text']);
    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }

    return $this->title->getId() . '_' . sha1($articleOriginalId);
  }

  public function getArticleReference(&$text) {
    $this->addLog('Getting article Link');
    $id = $this->getElementByName("id", $text);
    $url = "https://www.redbullcontentpool.com/international/$id";
    return $url;
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $category = array();
    $topics = $this->getElementByName("topic", $text);
    $items = $this->getElementsByName("item", $topics);
    foreach ($items as $item) {

      $category[$item] = $item;
    }
    $this->category = trim(implode(", ", $category));
    return $this->category;
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = "";
    if (isset($this->imagesArray[0]["img_name"])) {
      $description = trim($this->textFixation($this->getElementByName('description', $text)));
      $this->story .= "<p>$description</p>";
      $source = $this->getData($text, 'source', 'Project / Event:');
      $this->story .=$source;
      $length = $this->getData($text, 'length', 'Length:');
      $this->story .= $length;
      $subtitles = $this->getData($text, 'subtitles', 'Subtitles:');
      $this->story .= $subtitles;
      $version = $this->getData($text, 'version', 'Version:');
      $this->story .= $version;
      $aspectRatio = $this->getData($text, 'aspectRatio', 'Aspect Ratio:');
      $this->story .= $aspectRatio;
      $geolocation = $this->getDataFromArray($text, 'geolocation', 'Location:', 'item');
      $this->story .= $geolocation;
      $language = $this->getDataFromArray($text, 'language', 'Language:', 'item');
      $this->story .= $language;
      $attributes = $this->getDataFromArray($text, 'attributes', 'Photo Type:', 'type');
      $this->story .= $attributes;
      $productionDate = $this->getData($text, 'productionDate', 'Production Date:');
      $this->story .= $productionDate;
      $person = $this->getDataFromArray($text, 'person', 'Person / Team:', 'item');
      $this->story .= $person;
      $photographerCredit = $this->getData($text, 'photographerCredit', 'Photographer Credit:');
      $this->story .= $photographerCredit;
      $fileSize = $this->getData($text, 'fileSize', 'File Size:');
      $this->story .= $fileSize;
      $essenceType = $this->getData($text, 'essenceType', 'Format:');
      $this->story .= $essenceType;

      $this->story .= "<div><p><strong>Topic:</strong></p><p> $this->category </p></div>";
      return $this->story;
    } else {
      return '';
    }
  }

  protected function getImages(&$text) {
    $this->imagesArray = array();
    $imgs = array();
    $imgs = array();
    preg_match_all("/<resources>(.*?)<\/resources>/i", $text, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      if (preg_match("/<productTitle>(.*?)<\/productTitle>/i", $text, $image_caption)) {
        $image_caption = strip_tags($this->textFixation($image_caption[1]));
      } else {
        $image_caption = "";
      }
      $resources = $this->getElementByName('resources', $img);
      $resources = $this->getElementsByName('item', $resources);
      $resources = $resources[0];
      $imagePath = $this->getElementByName('url', $resources);
      $images = array();

      $images['img_name'] = $imagePath;
      $images['original_name'] = $imagePath;
      $images['image_caption'] = $image_caption;
      $images['is_headline'] = false;
      $images['image_original_key'] = sha1($imagePath);
      array_push($this->imagesArray, $images);
    }
    return $this->imagesArray;
  }

  private function getData($text, $from, $lable) {
    $story = "";
    $body = trim($this->textFixation($this->getElementByName($from, $text)));
    if (!empty($body)) {
      $story = "<div><p><strong>$lable</strong></p><p>$body</p></div>";
    } else {
      return "";
    }
    return $story;
  }

  private function getDataFromArray($text, $from, $lable, $sub_item) {
    $story = "";
    $element = array();
    $element_p = $this->getElementByName($from, $text);
    $items = $this->getElementsByName($sub_item, $element_p);
    foreach ($items as $item) {

      $element[$item] = $item;
    }
    $body = trim(implode(", ", $element));
    if (!empty($body)) {
      $story = "<div><p><strong>$lable</strong></p><p>$body</p></div>";
    } else {
      return "";
    }
    return $story;
  }

}
