<?php

/**
 * Publisher : IRIN (United Nations Integrated Regional Information Networks)
 * Title     : PlusNewse English service [ English ]
 * 
 */
class syndParser_407 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->charEncoding = 'ISO-8859-1';
    $this->extensionFilter = 'xml';
  }

  public function getRawArticles(&$fileContents) {
    $fileContents = str_replace("", "'", $fileContents);
    $fileContents = str_replace("", "\"", $fileContents);
    $fileContents = str_replace("", "\"", $fileContents);
    $fileContents = str_replace("", "", $fileContents);
    $fileContents = str_replace("", "'", $fileContents);
    $fileContents = str_replace("Â", "", $fileContents);
    $fileContents = str_replace("", "", $fileContents);

    return parent::getRawArticles($fileContents);
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    return $this->textFixation($this->getCData($this->getElementByName('body', $text)));
  }

  public function getAbstract(&$text) {
    $this->addLog('Getting article summary');
    return $this->textFixation($this->getCData($this->getElementByName('description', $text)));
  }

  public function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getCData($this->getElementByName('date', $text));
    $date = substr($date, strpos($date, ',') + 2);
    $date = date('Y-m-d', strtotime($date));
    return $this->dateFormater($date);
  }

}
