<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ansar Press  [# publisher id = 1643]
//Title      : Ansar Press [ Persian ] 
//Created on : Oct 04, 2021, 7:50:21 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12195 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fa'); 
	} 
}