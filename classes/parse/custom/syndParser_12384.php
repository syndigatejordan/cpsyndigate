<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Kanal FM  [# publisher id = 1752]
//Title      : Kanal FM Radio [ French ] 
//Created on : Feb 02, 2022, 2:28:49 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12384 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}