<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dar Al Baath for Press, Printing, Publishing and Distribution  [# publisher id = 912]
//Title      : Al Baath [ Arabic ] 
//Created on : May 16, 2016, 10:39:40 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2705 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}