<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : NBK Capital  [# publisher id = 695]
//Title      : NBK Capital - MENA In Focus [ English ] 
//Created on : Feb 02, 2016, 12:00:32 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2174 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}