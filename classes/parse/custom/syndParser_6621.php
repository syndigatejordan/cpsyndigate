<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : KHOVAR (National information agency of Tajikistan)  [# publisher id = 1418]
//Title      : KHOVAR (National information agency of Tajikistan) [ Arabic ] 
//Created on : Sep 19, 2021, 8:56:42 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6621 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}