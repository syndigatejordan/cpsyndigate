<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Agence France Presse  [# publisher id =716 ]  
//Title      : AFP - International News [ English ] 
//Created on : Mar 23, 2020, 10:05:58 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5968 extends syndParseXmlContent {

  public $story;
  public $imagesArray = array();
  public $category;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('fullArticle', $fileContents);
    return $articles;
  }

  public function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return trim($this->textFixation($this->getElementByName('title', $text)));
  }

  public function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = trim($this->getElementByName('published', $text));
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  public function getArticleOriginalId($params = array()) {
    $articleOriginalId = $this->getElementByName("newsItemID", $params['text']);
    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }

    return $this->title->getId() . '_' . sha1($articleOriginalId);
  }

  protected function getAbstract(&$text) {
    $this->addLog("getting article summary");
    return trim($this->getElementByName('jsondata', $text));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $slug = $this->getElementByName('slug', $text);
    $cats = $this->getElementsByName('item', $slug);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = $this->textFixation($this->getCData($cat));
      }
    }
    return implode(', ', $originalCats);
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $news = trim($this->textFixation($this->getElementByName('news', $text)));
    $news = str_replace("<item>", "<p>", $news);
    $news = str_replace("</item>", "</p>", $news);
    return $news;
  }

}
