<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Business Publishing Services Kft  [# publisher id = 1625]
//Title      : Budapest Business Journal [ English ] 
//Created on : Sep 27, 2021, 1:40:11 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12173 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}