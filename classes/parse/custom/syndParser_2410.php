<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : The Federation of Israeli Chambers of Commerce  [# publisher id =773 ] 
//Title     : The Federation of Israeli Chambers of Commerce News [ English ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2410 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

}
