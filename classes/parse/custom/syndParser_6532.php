<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Afghan Voice Agency (AVA)  [# publisher id = 1401]
//Title      : Afghan Voice Agency (AVA) [ Persian ] 
//Created on : Dec 08, 2020, 10:12:30 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6532 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fa'); 
	} 
}