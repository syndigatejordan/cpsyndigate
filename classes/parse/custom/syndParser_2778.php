<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Okaz Organization for Press & Publication  [# publisher id = 511]
//Title      : Okaz [ Arabic ] 
//Created on : May 26, 2016, 9:02:25 AM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2778 extends syndParseCms
{

    public $story;

    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('ar');
    }

    protected function getHeadline(&$text)
    {
        $this->addLog("getting article headline");
        $headLine= $this->textFixation($this->getElementByName('title', $text));
        if (strlen($headLine) > 250) {
            $headLine = substr($headLine, 0, strrpos(substr($headLine, 0, 250), ' ')) . " ...";
        }
        return $headLine;
    }

    protected function getStory(&$text)
    {
        $this->addLog("getting article body");
        $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
        $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) width=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) height=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) align=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
        $this->story = preg_replace('!\s+!', ' ', $this->story);
    $this->story = str_replace("<div/> </div>", "", $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    $this->story = str_replace(".", ".</p><p>", $this->story);
    if (!empty($this->story)) {
      $this->story = "<p>" . $this->story . "</p>";
    }
    $this->story = str_replace("</p><p> </div></p>", "</p></div>", $this->story);
    return $this->story;
  }

  protected function getImages(&$text) {
      $this->story = $this->textFixation($this->getCData($this->getElementByName('fulltext', $text)));
      $this->story = str_replace("onerror=\"this.src='images/no-image.png'\"", "", $this->story);
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
      return array();

      $imagesArray = array();
      preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
      foreach ($imgs[0] as $img) {
          $this->addLog("getting article images");
          $image_caption = '';
          $image_caption = explode('alt="', $img);
          $image_caption = explode('"', $image_caption[1]);
          $image_caption = $image_caption[0];
          $imageInfo = syndParseHelper::getImgElements($img, 'img');
          $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $headline = trim($this->textFixation($this->getElementByName('title', $text)));
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath, $headline);
      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl, $headline) {
    sleep(5);
    $baseName = basename($imageUrl);
    $baseName = sha1($headline) . '_' . $baseName;
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }
    if (!file_exists($copiedImage)) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $imageUrl);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
