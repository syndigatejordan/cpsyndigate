<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Mirror  [# publisher id = 1524]
//Title      : The Masvingo Mirror [ English ] 
//Created on : Feb 16, 2021, 9:49:32 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_9508 extends syndParseCms {

    public function customInit() {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('en');
    }
}
