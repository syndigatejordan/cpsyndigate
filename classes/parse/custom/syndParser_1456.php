<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : N2V   [# publisher id =484 ] 
// Titles    : Electrony [Arabic]
///////////////////////////////////////////////////////////////////////////

class syndParser_1456 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");

    $headline = $this->getCData($this->getElementByName('title', $text));
    return $headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");

    $date = $this->getElementByName('pubDate', $text);
    $date = preg_replace('/ \+(.*)/is', '', $date);
    $date = date('Y-m-d', strtotime($date));
    return $date;
  }

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $body = $this->textFixation($this->getCData($this->getElementByName('content:encoded', $text)));
    $body = strip_tags($body, '<p><br><strong><b><u><i>');
    return $body;
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    $cats = $this->getElementsByName('category', $text);
    $originalCats = array();

    if (!empty($cats)) {
      foreach ($cats as $cat) {
        $originalCats[] = $this->textFixation($this->getCData($cat));
      }
    }
    return implode(', ', $originalCats);
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->getElementByName('dc:creator', $text);
  }

  /*
    protected function getAbstract(&$text) {
    $this -> addLog('getting article summary');
    $summary = $this -> textFixation($this -> getCData($this -> getElementByName('description', $text)));
    $summary= str_replace('[...]', '', $summary);
    return $summary;
    }
   */

  protected function getImages(&$text) {
    $story = trim($this->textFixation($this->getCData($this->getElementByName('content:encoded', $text))));

    $imagesArray = array();
    preg_match_all("/<img[^>]+\>/i", $story, $imgs);

    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");

      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];

      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

}

/*
 class syndParser_1 extends syndParseXmlContent {
 public function customInit() {
 parent::customInit();
 $this -> defaultLang = $this -> model -> getLanguageId('en');
 $this -> extensionFilter = 'xml';
 }

 protected function getRawArticles(&$fileContents) {
 //get articles
 $this -> addLog("getting articles raw text");
 return $this -> getElementsByName('item', $fileContents);
 }

 protected function getHeadline(&$text) {
 $this -> addLog("getting article headline");

 $headline = $this -> getCData($this -> getElementByName('title', $text));
 return $headline;
 }

 protected function getArticleDate(&$text) {
 $this -> addLog("getting article date");

 $date = $this -> getElementByName('pubDate', $text);
 $date = date('Y-m-d', strtotime($date));
 return $date;
 }

 public function getStory(&$text) {
 $this -> addLog('Getting article story');
 $this -> story = $this -> textFixation($this -> getCData($this -> getElementByName('content', $text)));
 $this -> story = strip_tags($this -> story, '<p><br><strong><b><u><i>');
 if (empty($this -> story)) {
 return '';
 }
 return $this -> story;
 }

 public function getOriginalCategory(&$text) {
 $this -> addLog('getting article category');
 $cats = $this -> getElementsByName('category', $text);
 $originalCats = array();

 if (!empty($cats)) {
 foreach ($cats as $cat) {
 $originalCats[] = $this -> textFixation($this -> getCData($cat));
 }
 }
 return implode(', ', $originalCats);
 }

 protected function getAuthor(&$text) {
 $this -> addLog("getting article author");
 return $this -> textFixation($this -> getCData($this -> getElementByName('AuthorName', $text)));
 }

 protected function getAbstract(&$text) {
 $this -> addLog('getting article summary');
 $summary = $this -> textFixation($this -> getCData($this -> getElementByName('description', $text)));
 return $summary;
 }

 }

 class syndParser_1 extends syndParseRss {

 protected $cat_translation;
 protected $story = null;

 public function customInit() {
 parent::customInit();
 $this -> defaultLang = $this -> model -> getLanguageId('en');

 $this -> cat_translation['national'] = 'National';
 $this -> cat_translation['world'] = 'World';
 $this -> cat_translation['business'] = 'Business';
 $this -> cat_translation['opinion'] = 'Opinion';
 $this -> cat_translation['sport'] = 'Sport';
 $this -> cat_translation['health'] = 'Health';
 $this -> cat_translation['lifestyle'] = 'Life Style';
 $this -> cat_translation['entertainment'] = 'Entertainment';
 $this -> cat_translation['editorial'] = 'Editorial';
 $this -> cat_translation['specialnews'] = 'Special News';
 $this -> cat_translation['goldenjubileeuganda'] = 'Golden Jubilee Uganda';
 $this -> cat_translation['supplements'] = 'Supplements';
 $this -> cat_translation['archive'] = 'Archive';
 $this -> cat_translation['specialfeatures'] = 'Special Features';
 $this -> cat_translation['todayspick'] = 'todays Pick';
 $this -> cat_translation['sitessoundsuganda'] = 'Sites and Sounds of Uganda';
 $this -> cat_translation['specialsuppliment'] = 'Special Suppliment';

 }

 public function getStory(&$text) {
 $this -> addLog('Getting article story');
 $this -> story = $this -> textFixation($this -> getCData($this -> getElementByName('description', $text)));
 $this -> story = strip_tags($this -> story, '<p><br><strong><b><u><i>');

 if (empty($this -> story)) {
 return '';
 }
 return $this -> story;
 }

 protected function getArticleDate(&$text) {
 $this -> addLog("getting article date");

 $date = $this -> getElementByName('pubDate', $text);
 $date = date('Y-m-d', strtotime($date));
 return $date;
 }

 public function getOriginalCategory(&$text) {

 $this -> addLog('Getting article category');

 $filename = basename($this -> currentlyParsedFile);
 $filename = str_replace('.xml', '', $filename);
 if (isset($this -> cat_translation[$filename])) {
 return $this -> cat_translation[$filename];
 } else {
 return false;
 }
 }

 protected function getImages(&$text) {
 $story = trim($this -> textFixation($this -> getCData($this -> getElementByName('description', $text))));
 $header = trim($this -> textFixation($this -> getCData($this -> getElementByName('title', $text))));

 $imagesArray = array();
 preg_match_all("/<img[^>]+\>/i", $story, $imgs);

 foreach ($imgs[0] as $img) {
 $this -> addLog("getting article images");

 $imageInfo = syndParseHelper::getImgElements($img, 'img');
 $imagePath = $imageInfo[0];

 if (!$imagePath) {
 continue;
 }
 $imagePath = str_replace(' ', '%20', $imagePath);
 $imagePath = "http://www.newvision.co.ug" . $imagePath;
 $copiedImage = $this -> copyUrlImgIfNotCached($imagePath);

 if (!$copiedImage) {
 echo "no pahr";
 continue;
 }
 $images = $this -> getAndCopyImagesFromArray(array($copiedImage));
 $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
 $images[0]['is_headline'] = false;
 array_push($imagesArray, $images[0]);
 }
 return $imagesArray;
 }

 /*protected function getRawArticles(&$fileContents) {
 //get articles
 $this -> addLog("getting articles raw text");
 return $this -> getElementsByName('DC', $fileContents);
 }

 protected function getStory(&$text) {
 //get body text
 $this -> addLog("getting article text");
 $body = $this -> getCData($this -> getElementByName('BI', $text));
 return $body;
 }

 protected function getHeadline(&$text) {
 $this -> addLog("getting article headline");

 $headline = $this -> getCData($this -> getElementByName('TI', $text));
 return $headline;
 }

 protected function getArticleDate(&$text) {
 $this -> addLog("getting article date");

 $date = $this -> getElementByName('PD', $text);
 $date = date('Y-m-d', strtotime($date));
 return $date;
 }

 public function getOriginalCategory(&$text) {
 $this -> addLog('getting article category');

 $cats = $this -> getElementsByName('BT', $text);
 $originalCats = array();

 if (!empty($cats)) {
 foreach ($cats as $cat) {
 if (empty($cat)) {
 continue;
 }
 $originalCats[] = $this -> textFixation($cat);
 }
 }
 return implode(', ', $originalCats);
 }

 protected function getImages(&$text) {
 $this -> addLog("getting article images");

 //echo PHP_EOL . $this -> currentlyParsedFile . PHP_EOL;
 //echo PHP_EOL . PHP_EOL ; print_r($text); echo PHP_EOL . PHP_EOL;
 //exit;
 $images = $this -> getElementsByName('images', $text);
 if (empty($images)) {
 return array();
 }

 $finalImages = array();
 foreach ($images as $image) {
 $image = $this -> getElementByName('image', $image);

 $image = trim($image);
 if (empty($image)) {
 return array();
 }

 if (file_exists($this -> currentDir . $image)) {
 return $this -> getAndCopyImagesFromArray(array($this -> currentDir . $image));
 }
 return array();
 }
 }

 }*/

/*
 protected function getRawArticles(&$fileContents)
 {
 $this->loadCurrentDirectory();

 $this->addLog("getting raw articles text");
 return $this->getElementsByName('article', $fileContents);
 }

 protected function getStory(&$text)
 {
 $this->addLog("getting article text");
 return $this->textFixation( $this -> getCData ( $this->getElementByName('content', $text) ) );
 }

 protected function getHeadline(&$text)
 {
 $this->addLog("getting article headline");
 return $this->textFixation($this->getElementByName('headline', $text));
 }

 protected function getAuthor(&$text)
 {
 $this->addLog("getting article author");
 return $this->textFixation($this->getElementByName('author', $text));
 }

 protected function getOriginalCategory(&$text)
 {
 $this->addLog("getting Original Category");
 $cat    = str_replace("\r\n",'', $this->textFixation($this->getElementByName('cats', $text)));
 $subCat = str_replace("\r\n",'', $this->textFixation($this->getElementByName('subcats', $text)));

 if($subCat) {
 return $cat . '/' . $subCat;
 } else {
 return $cat;
 }

 }*/

/*protected function getStory(&$text) {
 $this -> addLog("getting article text");
 return $this -> textFixation($this -> getCData($this -> getElementByName('content:encoded', $text)));
 }

 protected function getAuthor(&$text) {
 $this -> addLog("getting article author");

 return trim($this -> textFixation($this -> getElementByName('dc:creator', $text)));
 }

 public function getOriginalCategory(&$text) {
 $this -> addLog('getting article category');
 $cats = $this -> getElementsByName('category', $text);
 $originalCats = array();

 if (!empty($cats)) {
 foreach ($cats as $cat) {
 $originalCats[] = $this -> textFixation($this -> getCData($cat));
 }
 }
 return implode(', ', $originalCats);
 }

 protected function getImages(&$text) {
 $body = $this -> textFixation($this -> getCData($this -> getElementByName('content:encoded', $text)));
 $images = $this -> getElementsByName('a', $body);

 $imagesArray = array();
 foreach ($images as $img) {
 $this -> addLog("getting article images");

 $imagePath = syndParseHelper::getImgElements($img, 'img');
 if (is_array($imagePath) && $imagePath) {
 $imagePath = $imagePath[0];
 }

 if (!$imagePath) {
 continue;
 }
 $copiedImage = $this -> copyUrlImgIfNotCached($imagePath);
 if (!$copiedImage) {
 continue;
 }

 $images = $this -> getAndCopyImagesFromArray(array($copiedImage));
 array_push($imagesArray, $images[0]);

 $text = str_replace($img, '', $text);
 }
 return $imagesArray;
 }*/

/*public function customInit() {
 parent::customInit();
 $this -> defaultLang = $this -> model -> getLanguageId('en');
 }

 public function getRawArticles(&$fileContents) {
 $this -> addLog("getting articles raw element");

 return $this -> getElementsByName('news', $fileContents);
 }

 protected function getHeadline(&$text) {
 $this -> addLog("getting article headline");

 return $this -> textFixation($this -> getElementByName('title', $text));
 }

 protected function getStory(&$text) {
 $this -> addLog("getting article text");

 $story = $this -> textFixation($this -> getElementByName('content', $text));
 $story = preg_replace('/<paragraph>/i', '<p>', $story);
 $story = preg_replace('/<\/paragraph>/i', '</p>', $story);
 return $story;
 }

 protected function getArticleDate(&$text) {
 $this -> addLog("getting article date");

 $date = $this -> textFixation($this -> getElementByName('publish_date', $text));
 return $this->dateFormater( $date );
 }

 protected function getOriginalCategory(&$text) {
 $this -> addLog("getting article original category");

 return $this -> textFixation($this -> getElementByName('category', $text));
 }*/

/*public function getOriginalCategory(&$text) {
 $this -> addLog('getting article category');
 $cats = $this -> getElementsByName('category', $text);
 $originalCats = array();

 if (!empty($cats)) {
 foreach ($cats as $cat) {
 $originalCats[] = $this -> textFixation($this -> getCData($cat));
 }
 }
 return implode(', ', $originalCats);
 }

 protected function getStory(&$text) {
 $this -> addLog("getting article text");
 return $this -> textFixation($this -> getCData($this -> getElementByName('content:encoded', $text)));
 }

 protected function getAuthor(&$text) {
 $this -> addLog("getting article author");

 return trim($this -> textFixation($this -> getElementByName('dc:creator', $text)));
 }

 protected function getImages(&$text) {
 $body = $this -> textFixation($this -> getCData($this -> getElementByName('content:encoded', $text)));
 $images = $this -> getElementsByName('a', $body);

 $imagesArray = array();
 foreach ($images as $img) {
 $this -> addLog("getting article images");

 $imagePath = syndParseHelper::getImgElements($img, 'img');
 if (is_array($imagePath) && $imagePath) {
 $imagePath = $imagePath[0];
 }

 if (!$imagePath) {
 continue;
 }
 $copiedImage = $this -> copyUrlImgIfNotCached($imagePath);
 if (!$copiedImage) {
 continue;
 }

 $images = $this -> getAndCopyImagesFromArray(array($copiedImage));
 array_push($imagesArray, $images[0]);

 $text = str_replace($img, '', $text);
 }
 return $imagesArray;
 }*/

/*public function customInit() {
 parent::customInit();
 $this -> defaultLang = $this -> model -> getLanguageId('en');
 }

 public function getRawArticles(&$fileContents) {
 $this -> addLog('getting row articles');
 $rowArticles = parent::getRawArticles($fileContents);
 return $rowArticles;
 }

 protected function getAbstract(&$text) {
 $this -> addLog('getting article summary');
 $summary = $this -> textFixation($this -> getCData($this -> getElementByName('description', $text)));
 return $summary;
 }

 public function getStory(&$text) {
 $this -> addLog('getting article story');
 $story = $this -> textFixation($this -> getCData($this -> getElementByName('content:encoded', $text)));
 $story = str_replace("style=\"text-align: justify;\"", '', $story);
 $story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $story);
 return $story;
 }

 protected function getAuthor(&$text) {
 $this -> addLog("getting article author");

 return trim($this -> textFixation($this -> getElementByName('dc:creator', $text)));
 }

 public function getOriginalCategory(&$text) {
 $this -> addLog('getting article category');
 $cats = $this -> getElementsByName('category', $text);
 $originalCats = array();

 if (!empty($cats)) {
 foreach ($cats as $cat) {
 $originalCats[] = $this -> textFixation($this -> getCData($cat));
 }
 }
 return implode(', ', $originalCats);
 }

 protected function getImages(&$text) {
 $body = $this -> textFixation($this -> getCData($this -> getElementByName('content:encoded', $text)));
 $images = $this -> getElementsByName('a', $body);

 $imagesArray = array();
 foreach ($images as $img) {
 $this -> addLog("getting article images");

 $imagePath = syndParseHelper::getImgElements($img, 'img');

 if (is_array($imagePath) && $imagePath) {
 $imagePath = $imagePath[0];
 }

 if (!$imagePath) {
 continue;
 }

 $copiedImage = $this -> copyUrlImgIfNotCached($imagePath);
 if (!$copiedImage) {
 continue;
 }

 $images = $this -> getAndCopyImagesFromArray(array($copiedImage));
 array_push($imagesArray, $images[0]);

 $text = str_replace($img, '', $text);
 }

 return $imagesArray;
 }*/

/*public function getRawArticles(&$fileContents) {
 $this -> addLog("getting articles raw text");
 return $this -> getElementsByName('nitf:nitf', $fileContents);
 }

 protected function getHeadline(&$text) {
 $this -> addLog('getting article headline');
 $header1 = $this -> textFixation($this -> getElementByName('nitf:hl1', $text));
 $header2 = $this -> textFixation($this -> getElementByName('nitf:hl2', $text));
 $header = $header1 . '. ' . $header2;
 return $header;
 }

 public function getAbstract(&$text) {
 $this -> addLog('Getting article summary');
 $abstract = $this -> textFixation($this -> getCData($this -> getElementByName('nitf:abstract', $text)));
 $abstract = str_ireplace('nitf:', '', $abstract);
 return $abstract;
 }

 protected function getAuthor (&$text) {
 $this->addLog("getting article author");
 $author = $this -> textFixation( $this->getCData( $this->getElementByName('nitf:byline', $text) ) );
 return $author;
 }

 protected function getArticleDate(&$text) {
 $this -> addLog("getting article date");

 $date 	=  $this -> textFixation( $this -> getElementByName('nitf:docdata', $text ));

 $regExp = '<nitf:date.release[\s]+.*norm[\s]*=[\s]*([\"]([^>\"]*)[\"]|' . "[\\']([^>\\']*)[\\']|([^>\\s]*))([\\s]|[^>])*[\/]?>";
 preg_match_all("/$regExp/i", $date, $m);
 $date = $m[2][0];

 $date = explode('T', $date);
 $date = trim( $date[0] );

 $year  = substr($date, 0, 4);
 $month = substr($date, 4, 2);
 $day   = substr($date, 6, 2);

 $date = $year . '-' . $month . '-' . $day;
 return $date;
 }

 protected function getOriginalCategory(&$text) {
 $this -> addLog('getting article category');
 $cats = $this -> getElementByName('nitf:key-list', $text);

 if (!empty($cats)) {
 $regExp = '<nitf:keyword[\s]+.*key[\s]*=[\s]*([\"]([^>\"]*)[\"]|' . "[\\']([^>\\']*)[\\']|([^>\\s]*))([\\s]|[^>])*[\/]?>";
 preg_match_all("/$regExp/i", $cats, $m);
 $categories = $m[2];

 if ( ! empty( $categories ) ) {
 return implode(', ', $categories);
 }
 }
 return '';
 }

 protected function getStory(&$text) {
 $this -> addLog('getting article story');

 $body = $this -> getElementByName('nitf:block', $text );
 if ( strpos($body, '<nitf:media') ) {
 //echo strpos($body, '<nitf:media');
 $body = substr($body, 0, strpos($body, '<nitf:media'));
 $body = strip_tags($body);
 }

 return $body;
 }

 public function getCData($text)
 {
 $text = str_replace( array('<![CDATA[', ']]>') , '', $text);
 return $text;
 }

 protected function getImages(&$text) {
 $this -> addLog('getting article image');

 //echo PHP_EOL . $this->binTmpDir . PHP_EOL;
 //$copiedImage = $this->copyUrlImgIfNotCached($imagePath);
 //exit;

 $regExp = '<nitf:media-metadata name="IPTC_Caption_Abstract"[\s]+.*value[\s]*=[\s]*([\"]([^>\"]*)[\"]|' . "[\\']([^>\\']*)[\\']|([^>\\s]*))([\\s]|[^>])*[\/]?>";
 preg_match_all("/$regExp/i", $text, $m);
 $caption = $m[2][0];
 $caption = trim($caption);

 $obj = $this -> getElementByName('nitf:media-object', $text );

 $imageObj = $this -> getCData($obj);
 $imageObj = base64_decode($imageObj);

 if ( empty( $imageObj ) ) {
 return array();
 }

 $imageName = time();
 $imagePath = $this -> currentDir . $imageName . '.jpg';

 if ( empty($caption) ) {
 $caption = $imageName;
 } else {

 if ( strlen($caption) > 250 ) {
 $caption = substr($caption, 0, 250);
 }

 }

 touch($imagePath);
 chmod($imagePath, 0777);

 file_put_contents($imagePath, $imageObj);

 if(file_exists($imagePath)) {

 $name =  $this->model->getImgReplacement($imageName.'.jpg', $this->currentDir, $this-> title -> getId() );
 $img['img_name']		= str_replace(IMGS_PATH, IMGS_HOST, $name);
 $img['original_name']	= $imagePath;
 $img['image_caption']	= $caption . '.jpg';  //$imageName . '.jpg';
 $img['is_headline']     = false;
 $images[] = $img;
 return $images;
 } else {
 return array();
 }

 }*/

/*protected function getStory(&$text) {
 //$story = parent::getStory($text);
 $story = trim( $this -> textFixation( $this -> getCData( $this -> getElementByName('description', $text) ) ) );
 return preg_replace("/<img[^>]+\>/i", '', $story);
 }

 protected function getImages(&$text) {
 $story = trim($this -> textFixation($this -> getCData($this -> getElementByName('description', $text))));

 $imagesArray = array();
 preg_match_all("/<img[^>]+\>/i", $story, $imgs);

 foreach ($imgs[0] as $img) {
 $this -> addLog("getting article images");

 $imageInfo = syndParseHelper::getImgElements($img, 'img');
 //$imageInfo = syndParseHelper::getImgElements($img);
 $imagePath = $imageInfo[0];

 if (!$imagePath) {
 continue;
 }
 $imagePath   = str_replace(' ', '%20', $imagePath);
 $copiedImage = $this -> copyUrlImgIfNotCached($imagePath);
 if (!$copiedImage) {
 continue;
 }

 $images = $this -> getAndCopyImagesFromArray(array($copiedImage));
 $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);

 array_push($imagesArray, $images[0]);
 }
 return $imagesArray;
 }*/

/*protected function getStory(&$text) {
 $this->addLog("getting article text");
 $story = $this->textFixation($this->getCData( $this->getElementByName('content:encoded', $text) ) );

 echo PHP_EOL . $story . PHP_EOL;
 exit;

 return preg_replace("/<img[^>]+\>/i", "", $story);
 }

 protected function getAuthor(&$text) {
 $this -> addLog("getting article author");

 return trim( $this -> textFixation( $this -> getElementByName('dc:creator', $text) ) );
 }

 public function getArticleDate(&$text) {
 $this->addLog("getting article date");
 $date = $this->getElementByName('pubDate', $text);
 return  $this->dateMapper($date);
 }

 protected function getImages(&$text) {
 $story = trim($this -> textFixation($this -> getCData($this -> getElementByName('content:encoded', $text))));

 $imagesArray = array();
 preg_match_all("/<img[^>]+\>/i", $story, $imgs);

 foreach ($imgs[0] as $img) {
 $this -> addLog("getting article images");

 $imageInfo = syndParseHelper::getImgElements($img, 'img');
 //$imageInfo = syndParseHelper::getImgElements($img);
 $imagePath = $imageInfo[0];

 if (!$imagePath) {
 continue;
 }

 $copiedImage = $this -> copyUrlImgIfNotCached($imagePath);
 if (!$copiedImage) {
 continue;
 }

 $images = $this -> getAndCopyImagesFromArray(array($copiedImage));
 array_push($imagesArray, $images[0]);
 }
 return $imagesArray;
 }

 private function dateMapper ($date) {

 $date = trim($date);
 $monthsEn = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',);
 $monthsAr = array('يناير', 'فبراير', 'مارس', 'أبريل', 'مايو', 'يونيو', 'يوليو', 'أغسطس', 'سبتمبر', 'أكتوبر', 'نوفمبر', 'ديسمبر',);

 $daysEn = array ('Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri',);
 $daysAr = array ('السبت', 'الأحد', 'الإثنين', 'الثلاثاء', 'الأربعاء', 'الخميس', 'الجمعة',);

 $date = str_replace($daysAr, $daysEn, $date);
 $date = str_replace($monthsAr, $monthsEn, $date);
 return $this->dateFormater($date);
 }*/

/*public function customInit() {
 parent::customInit();
 $this -> extensionFilter = 'xml';
 $this -> defaultLang = $this -> model -> getLanguageId('en');

 require_once ('/usr/local/syndigate/lib/getid3/getid3/getid3.php');
 }

 public function getRawArticles(&$fileContents) {
 $this -> addLog("getting articles raw element");

 $fileContents = html_entity_decode($fileContents, ENT_QUOTES, 'UTF-8');
 return $this -> getElementsByName('Workbook', $fileContents);
 }

 protected function getHeadline(&$text) {
 $this -> addLog("getting article headline");

 return $this -> textFixation($this -> getElementByName('TITLE', $text));
 }

 protected function getArticleDate(&$text) {
 $this -> addLog("getting article date");

 $date = $this -> textFixation($this -> getElementByName('LastSaved', $text));
 $date = explode('T', $date);
 $date = $date[0];

 if ( ! $date ) {
 $date = parent::getCurrentParsedFileDateFromFolder();
 }
 return $date;
 }

 protected function getStory(&$text) {
 $this -> addLog("getting article text");

 return $this -> textFixation($this -> getElementByName('DESCRIPTION', $text));
 }

 protected function getVideos(&$text) {
 $this -> addLog("Getting article video");

 $videoName = $videoCaption = $this -> textFixation($this -> getElementByName('videoSrc', $text));
 if ( empty($videoName) ) {
 return array();
 }

 $videoName = $this -> currentDir . $videoName;

 $videos = array();
 $videoInformation = array();
 $neededInfo = array('filesize', 'fileformat', 'encoding', 'mime_type', 'playtime_seconds', 'bitrate', 'playtime_string', 'video');

 $getID3 = new getID3;
 $realVideoPath = str_replace( $this -> title -> getParseDir(), $this -> title -> getHomeDir(), $videoName );

 if ( ! file_exists($videoName) ) {
 return array();
 }

 $videoInfo = $getID3 -> analyze($realVideoPath);

 foreach ( $neededInfo as $key => $value ) {
 $keysExist = array_keys($videoInfo);

 if ( in_array($value, $keysExist) ) {
 switch ( $value ) {
 case 'video' :
 $videoInformation['width'] = $videoInfo[$value]['resolution_x'];
 $videoInformation['height'] = $videoInfo[$value]['resolution_y'];
 break;

 case 'bitrate' :
 $videoInformation[$value] = floor($videoInfo[$value] / 1024);
 break;

 default :
 $videoInformation[$value] = $videoInfo[$value];
 break;
 }
 }

 }

 //$name = $this->model->getVidReplacement($this->videoName . $videoType, $this->currentDir, 238) ;
 //$video['video_name']    	= str_replace(VIDEOS_PATH, VIDEOS_HOST, $name);

 $video['video_name'] = $this -> getAndCopyVideosFromArray($videoName);

 //$bitRate 					= $videoInfo['bitrate'] / 1000;
 $mimeType = $videoInfo['mime_type'];
 $video['original_name'] = $videoCaption;
 $video['video_caption'] = substr( $videoCaption, 0, strrpos($videoCaption, '.') );
 //$video['bit_rate']       	= $this->getBitRateFromName($videoLable);
 $video['bit_rate'] = json_encode($videoInformation);
 //$bitRate . ' kbps';
 $video['mime_type'] = $mimeType;
 $video['added_time'] = date('Y-m-d h:i:s', filemtime($video['original_name']));
 $videos[] = $video;

 return $videos;
 }*/

/*protected function getImages(&$text) {
 $body   = $this->textFixation($this->getCData($this->getElementByName('description', $text)));
 $images = $this->getElementsByName('a',$body);

 $imagesArray = array();
 foreach ($images as $img) {
 $this->addLog("getting article images");

 $imagePath = syndParseHelper::getImgElements($img, 'img');

 if(is_array($imagePath)&& $imagePath) {
 $imagePath = $imagePath[0];
 }

 if(!$imagePath) {
 continue;
 }

 $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
 if(!$copiedImage) {
 continue;
 }

 $images 	= $this->getAndCopyImagesFromArray(array($copiedImage));
 array_push($imagesArray, $images[0]);

 $text = str_replace($img, '', $text);
 }

 return $imagesArray;
 }*/

/*protected function getAuthor(&$text) {
 $this -> addLog("getting article author");

 $author = $this -> getElementByName('dc:creator', $text);
 return $this -> textFixation($author);
 }

 protected function getOriginalCategory(&$text) {
 $this -> addLog("getting article original category");

 return 'India Patents';
 }*/

/*protected function getImages(&$text) {
 $this -> addLog("getting article images");

 $imagePath = $this -> getCData($this -> getElementByName('photo', $text));

 $imagesArray = array();

 if ($imagePath) {
 $copiedImage = $this -> copyUrlImgIfNotCached($imagePath);
 if ($copiedImage) {
 $images = $this -> getAndCopyImagesFromArray(array($copiedImage));
 array_push($imagesArray, $images[0]);
 return $imagesArray;
 }
 } else {
 return $imagesArray;
 }
 }*/

/*public function getRawArticles(&$fileContents) {
 $this -> addLog("getting articles info Logo & URL to add them to body in RawArticles function");

 $titleInfo = $this -> getElementByName('image', $fileContents);
 $this -> titleInfoURL  = trim( $this -> textFixation( $this -> getElementByName('url'	, $titleInfo) ) );
 $this -> titleInfoName = trim( $this -> textFixation( $this -> getElementByName('title'	, $titleInfo) ) );
 $this -> titleInfoLink = trim( $this -> textFixation( $this -> getElementByName('link'	, $titleInfo) ) );

 return parent::getRawArticles($fileContents);
 }

 public function getStory(&$text) {
 $body = parent::getStory($text);

 $this -> addLog('Adding Logo And links to the end of article body');

 $logo 	= "<img src='{$this->titleInfoURL}' />";
 $source = "<b>More videos available on <a href='$this->titleInfoLink' target='_blank'>$this->titleInfoLink</a></b>";
 $story 	= $body . ' <br /><p>' . $logo . '  ' . $source .'</p>';
 return $story;
 }*/

/*protected function getAuthor(&$text) {
 $this -> addLog("getting article author");
 return trim($this -> textFixation($this -> getElementByName('dc:creator', $text)));
 }

 public function getStory(&$text) {
 $this -> addLog('getting article text');

 $story = trim($this -> textFixation($this -> getCData($this -> getElementByName('content:encoded', $text))));
 //return $story;
 return preg_replace("/<img[^>]+\>/i", "", $story);
 }*/

/*protected function getImages(&$text) {
 $story = trim($this -> textFixation($this -> getCData($this -> getElementByName('content:encoded', $text))));

 $imagesArray = array();
 preg_match_all("/<img[^>]+\>/i", $story, $imgs);

 foreach ($imgs[0] as $img) {
 $this -> addLog("getting article images");

 $imageInfo = syndParseHelper::getImgElements($img, 'img');
 //$imageInfo = syndParseHelper::getImgElements($img);
 $imagePath = $imageInfo[0];

 if (!$imagePath) {
 continue;
 }

 $copiedImage = $this -> copyUrlImgIfNotCached($imagePath);
 if (!$copiedImage) {
 continue;
 }

 $images = $this -> getAndCopyImagesFromArray(array($copiedImage));
 array_push($imagesArray, $images[0]);
 }
 return $imagesArray;
 }*/

/*public function getRawArticles(&$fileContents) {
 $this->addLog('Getting article story');

 return $this->getElementsByName('ITEM', $fileContents);
 }

 public function getArticleDate(&$text) {
 $this->addLog('Getting article date');

 $date 		= trim($this->getElementByName('DATE', $text));
 $finalDate 	= date('Y-m-d', strtotime($date));

 if ( $finalDate == '1970-01-01' ) {
 $date 		= explode('/', $date);
 $finalDate	= $date[2] . '-' . $date[1] . '-' . $date[0];
 }
 return $finalDate;
 }

 public function getHeadline(&$text) {
 $this->addLog('Getting article headline');

 return trim($this->getElementByName('HEADLINE', $text));
 }

 public function getStory(&$text) {
 $this->addLog('Getting article body');

 $story = trim($this->getElementByName('TEXT', $text));
 return '<p>' . $story .'</p>';
 }

 protected function getImages(&$text) {

 //$imageGroup = $this->getElementByName('imgs', $text);
 $images = $this->getElementsByName('IMAGE', $text);

 $imgs = array();
 $count=count($images);
 for($i=0; $i<$count; $i++) {

 $imagePath = $this->currentDir . trim($images[$i]);
 if( file_exists( $imagePath ) ) {
 $this->addLog('Getting article image');
 $imgs[$i] = $imagePath;
 }
 }

 if ( !empty($imgs) ) {
 return $this->getAndCopyImagesFromArray($imgs);
 } else {
 return array();
 }

 }*/

//}

/*protected function getAuthor(&$text) {
 $this -> addLog('getting article author');
 return $this -> getCData (($this -> getElementByName('dc:creator', $text)) );
 }

 public function getStory(&$text) {
 $this -> addLog('getting article text');

 $story = trim($this -> textFixation($this -> getCData($this -> getElementByName('description', $text))));
 return preg_replace("/<img[^>]+\>/i", "", $story);
 }

 public function getOriginalCategory(&$text) {
 $this -> addLog('Getting article original category');

 $parts = explode('.xml', basename($this -> currentlyParsedFile));
 $category = str_replace('_', ' ', $parts[0]);
 return $category;
 }

 protected function getImages(&$text) {
 $story = trim($this -> textFixation($this -> getCData($this -> getElementByName('description', $text))));

 $imagesArray = array();
 preg_match_all("/<img[^>]+\>/i", $story, $imgs);

 foreach ($imgs[0] as $img) {
 $this -> addLog("getting article images");

 $imageInfo = syndParseHelper::getImgElements($img);
 $imagePath = $imageInfo[0];

 if (!$imagePath) {
 continue;
 }

 $copiedImage = $this -> copyUrlImgIfNotCached($imagePath);
 if (!$copiedImage) {
 continue;
 }

 $images = $this -> getAndCopyImagesFromArray(array($copiedImage));
 array_push($imagesArray, $images[0]);
 }
 return $imagesArray;
 }*/

/*protected function getHeadline(&$text) {
 $this -> addLog("getting article headline");

 return trim($this -> textFixation($this -> getCData($this -> getElementByName('title', $text))));
 }

 public function getAbstract(&$text) {
 $this -> addLog('Getting article summary');

 return trim($this -> textFixation($this -> getCData($this -> getElementByName('description', $text))));
 }

 public function getStory(&$text) {
 $this -> addLog('getting article text');

 // 		return preg_replace("/<img[^>]+\>/i", "", $text);
 return trim($this -> textFixation($this -> getCData($this -> getElementByName('story', $text))));
 }

 protected function getArticleDate(&$text) {
 $this -> addLog("getting article date");

 $date = trim($this -> textFixation($this -> getCData($this -> getElementByName('articleDate', $text))));

 //if ( strcasecmp( date('l', strtotime($date) ), 'Sunday') === 0 ) {  // date('l', strtotime($date) ) == 'Sunday'
 //echo 'This aticle posted on Sunday';
 //exit;
 //}

 return parent::dateFormater(date('Y-m-d', strtotime($date)));
 }

 protected function getImages(&$text) {
 $story = parent::getStory($text);

 $imagesArray = array();
 preg_match_all("/<img[^>]+\>/i", $story, $imgs);

 foreach ($imgs[0] as $img) {
 $this -> addLog("getting article images");

 $imageInfo = syndParseHelper::getImgElements($img);
 $imagePath = $imageInfo[0];

 if (!$imagePath) {
 continue;
 }

 $copiedImage = $this -> copyUrlImgIfNotCached($imagePath);
 if (!$copiedImage) {
 continue;
 }

 $images = $this -> getAndCopyImagesFromArray(array($copiedImage));
 array_push($imagesArray, $images[0]);
 }
 return $imagesArray;
 }*/
