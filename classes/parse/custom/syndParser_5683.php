<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Vacca Foeda Media  [# publisher id = 1255]
//Title      : Today I Found Out [ English ] 
//Created on : Sep 19, 2021, 8:52:46 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5683 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}