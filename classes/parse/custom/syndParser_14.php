<?php
/////////////////////////////////////////////////////////////////////////////////////
//Publisher  : EURL Horizons  [# publisher id =12 ]
//Title      : Horizons [ French ]
//Created on : Mar 28, 2021, 6:48:09 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_14 extends  syndParseCms
{
    public function customInit()
    {
        parent::customInit();
        $this->defaultLang = $this->model->getLanguageId('fr');
    }

}
