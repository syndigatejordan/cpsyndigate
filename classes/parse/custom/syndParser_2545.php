<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Durban Chamber of Commerce and Industry  [# publisher id = 823]
//Title      : Durban Chamber of Commerce and Industry News [ English ] 
//Created on : Feb 02, 2016, 12:10:49 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2545 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}