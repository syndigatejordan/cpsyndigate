<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : AGERPRES (Romanian National News Agency)  [# publisher id = 1433]
//Title      : AGERPRES (Romanian National News Agency) [ Hungarian ] 
//Created on : Dec 14, 2021, 12:46:49 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6698 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('hu'); 
	} 
}