<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : SME Publishing under SME Rebuilders  [# publisher id =379 ] 
// Titles    : SME World
///////////////////////////////////////////////////////////////////////////

class syndParser_1259 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

}
