<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Africa News Agency (ANA)  [# publisher id = 1432]
//Title      : Africa News Agency (ANA) [ English ] 
//Created on : Oct 05, 2020, 11:15:34 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6693 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}