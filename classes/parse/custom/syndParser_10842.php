<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Media 24  [# publisher id = 1582]
//Title      : News24 [ English ] 
//Created on : May 16, 2021, 9:40:17 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10842 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}