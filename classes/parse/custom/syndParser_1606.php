<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Global Data Point Ltd.  [# publisher id = 378]
//Title      : Energy Monitor Worldwide [ English ] 
//Created on : Aug 10, 2021, 1:52:35 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1606 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}