<?php

////////////////////////////////////////////////////////////////////////
// Publisher : AllAfrica Global Media 
// Titles    : allAfrica.com [ English ]
////////////////////////////////////////////////////////////////////////

class syndParser_334 extends syndParseXmlContent {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
    $this->charEncoding = 'ISO-8859-1';
    $this->extensionFilter = 'xml';
  }

  protected function getRawArticles(&$fileContents) {
    $this->addLog("getting articles raw text");
    $data = $this->getElementsByName('AllAfrica_NewsStory', $fileContents);
    $data = str_replace("<publisher>", "<name_of_the_publisher>", $data);
    $data = str_replace("</publisher>", "</name_of_the_publisher>", $data);
    return $data;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return $this->textFixation($this->getElementByName('publisher_headline', $text));
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");

    $date = $this->getElementByName('publication_date', $text);
    $date = substr($date, 0, 4) . '-' . substr($date, 4, 2) . '-' . substr($date, 6, 2);
    return $this->dateFormater($date);
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $headline = $this->textFixation($this->getElementByName('publisher_headline', $text));
    $publisher = $this->textFixation($this->getElementByName('publisher', $text));
    $publisherLocation = $this->textFixation($this->getElementByName('publisher_location', $text));
    $dateline = $this->textFixation($this->getElementByName('dateline', $text));
    $reporter = $this->textFixation($this->getElementByName('reporter', $text));
    $author = $publisher . " " . $publisherLocation . " " . $dateline . " " . $reporter;
    $author = str_replace($headline, "", $author);
    $author = trim(preg_replace('!\s+!', ' ', $author));
    return $author;
  }

  protected function getOriginalCategory(&$text) {
    $this->addLog("getting article original category");
    $kind_of_story = trim($this->textFixation($this->getElementByName('kind_of_story', $text)));
    $name_of_publisher = trim($this->textFixation($this->getElementByName('name_of_the_publisher', $text)));
    $cats = $this->getElementsByName('category', $text);
    $originalCats = array();
    if (!empty($cats)) {
      foreach ($cats as $cat) {
        if (empty($cat)) {
          continue;
        }
        $originalCats[] = $this->textFixation($cat);
      }
    }
    if (!empty($kind_of_story)) {
      $originalCats[] = $kind_of_story;
    }
    if (!empty($name_of_publisher)) {
      $originalCats[] = $name_of_publisher;
    }
    return implode(', ', $originalCats);
  }

  public function getArticleReference(&$text) {
    $link = trim($this->textFixation($this->getCData($this->getElementByName('URL', $text))));
    return $link;
  }

  protected function getStory(&$text) {
      $dateline = $this->textFixation($this->getElementByName('dateline', $text));
      $this->addLog("getting article text");
      $story = $this->getCData($this->getElementByName('body', $text));
      $arr_xml_special_char = array("&#x80;", "&#x81;", "&#x82;", "&#x83;", "&#x84;", "&#x85;", "&#x86;", "&#x87;", "&#x88;", "&#x89;", "&#x8A;", "&#x8B;", "&#x8C;", "&#x8D;", "&#x8E;", "&#x8F;", "&#x90;", "&#x91;", "&#x92;", "&#x93;", "&#x94;", "&#x95;", "&#x96;", "&#x97;", "&#x98;", "&#x99;", "&#x9A;", "&#x9B;", "&#x9C;", "&#x9D;", "&#x9E;", "&#x9F;", "&#xA0;", "&#xA1;", "&#xA2;", "&#xA3;", "&#xA4;", "&#xA5;", "&#xA6;", "&#xA7;", "&#xA8;", "&#xA9;", "&#xAA;", "&#xAB;", "&#xAC;", "&#xAD;", "&#xAE;", "&#xAF;", "&#xB0;", "&#xB1;", "&#xB2;", "&#xB3;", "&#xB4;", "&#xB5;", "&#xB6;", "&#xB7;", "&#xB8;", "&#xB9;", "&#xBA;", "&#xBB;", "&#xBC;", "&#xBD;", "&#xBE;", "&#xBF;", "&#xC0;", "&#xC1;", "&#xC2;", "&#xC3;", "&#xC4;", "&#xC5;", "&#xC6;", "&#xC7;", "&#xC8;", "&#xC9;", "&#xCA;", "&#xCB;", "&#xCC;", "&#xCD;", "&#xCE;", "&#xCF;", "&#xD0;", "&#xD1;", "&#xD2;", "&#xD3;", "&#xD4;", "&#xD5;", "&#xD6;", "&#xD7;", "&#xD8;", "&#xD9;", "&#xDA;", "&#xDB;", "&#xDC;", "&#xDD;", "&#xDE;", "&#xDF;", "&#xE0;", "&#xE1;", "&#xE2;", "&#xE3;", "&#xE4;", "&#xE5;", "&#xE6;", "&#xE7;", "&#xE8;", "&#xE9;", "&#xEA;", "&#xEB;", "&#xEC;", "&#xED;", "&#xEE;", "&#xEF;", "&#xF0;", "&#xF1;", "&#xF2;", "&#xF3;", "&#xF4;", "&#xF5;", "&#xF6;", "&#xF7;", "&#xF8;", "&#xF9;", "&#xFA;", "&#xFB;", "&#xFC;", "&#xFD;", "&#xFE;", "&#xFF;");
      $arr_xml_special_replace = array("€", " ", "‚", "ƒ", "„", "…", "†", "‡", "ˆ", "‰", "Š", "‹", "Œ", " ", "Ž", "x", " ", "‘", "’", "“", "”", "•", "–", "—", "˜", "™", "š", "›", "œ", " ", "ž", "Ÿ'", " ", "¡", "¢", "£", "¤", "¥", "¦", "§", "¨", "©", "ª", "«", "¬", " ", "®", "¯", "°", "±", "²", "³", "´", "µ", "¶", "·", "¸", "¹", "º", "»", "¼", "½", "¾", "¿", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "×", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "Þ", "ß", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï", "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "÷", "ø", "ù", "ú", "û", "ü", "ý", "þ", "ÿ");
      $story = str_replace($arr_xml_special_char, $arr_xml_special_replace, $story);
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $story = $this->textFixation($dateline . " — " . $story);
      return $story;
  }

}

//End class