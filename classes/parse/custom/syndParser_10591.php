<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : O Movimento Jornal e Editora  [# publisher id = 1554]
//Title      : Jornal O Movimento [ Portuguese ] 
//Created on : Sep 23, 2021, 8:16:05 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10591 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}