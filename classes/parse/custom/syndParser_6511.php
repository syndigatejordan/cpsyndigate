<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Government of St. Eustatius  [# publisher id = 1392]
//Title      : St. Eustatius News [ English ] 
//Created on : Oct 20, 2020, 12:26:19 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6511 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}