<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : IFC - International Finance Corporation  [# publisher id = 1773]
//Title      : IFC (World Bank Group) [ Arabic ] 
//Created on : Feb 28, 2022, 12:33:48 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12495 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}