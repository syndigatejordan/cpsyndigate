<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Seeking Alpha Ltd.  [# publisher id = 1675]
//Title      : Seeking Alpha – Analysis  [ English ] 
//Created on : Oct 26, 2021, 5:59:34 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12242 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}