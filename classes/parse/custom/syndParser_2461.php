<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Emad Honarparvar  [# publisher id = 803]
//Title      : Digital Spirit [ English ] 
//Created on : Feb 02, 2016, 12:09:11 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2461 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}