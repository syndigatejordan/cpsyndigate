<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Sahara Press Service  [# publisher id = 1378]
//Title      : Sahara Press Service [ English ] 
//Created on : Oct 07, 2020, 12:47:33 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6476 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}