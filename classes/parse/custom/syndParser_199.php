<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : 	Palestine News & Information Agency (WAFA) 
// Titles    :  Palestine News & Information Agency (WAFA) [Arabic]
///////////////////////////////////////////////////////////////////////////

	class syndParser_199 extends syndParseCms{
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('ar');
		}

		public function getStory(&$text) {
	           $story = '<p>';
		   $story = parent::getStory($text);
		   $story = str_replace(". ", ".</p><p>", $story);
		   $story .= '</p>';

		   return $story;
	       }
		
	}
