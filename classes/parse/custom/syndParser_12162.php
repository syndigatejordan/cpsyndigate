<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Le Potentiel  [# publisher id = 1617]
//Title      : Le Potentiel [ French ] 
//Created on : Aug 31, 2021, 8:27:43 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12162 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}