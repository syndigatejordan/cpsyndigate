<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Human Rights Watch  [# publisher id = 1302]
//Title      : Human Rights Watch - News [ Spanish ] 
//Created on : Feb 29, 2020, 1:56:49 PM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5936 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('es');
  }
}
