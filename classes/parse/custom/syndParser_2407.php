<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher : Erbil Chamber of Commerce and Industry  [# publisher id =771 ]  
//Title     : Erbil Chamber of Commerce & Industry News [ English ] 
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2407 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

}
