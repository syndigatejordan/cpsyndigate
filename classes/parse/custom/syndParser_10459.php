<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : EBR Media Limited  [# publisher id = 1547]
//Title      : The European Business Review [ English ] 
//Created on : Dec 14, 2021, 7:47:43 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_10459 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}