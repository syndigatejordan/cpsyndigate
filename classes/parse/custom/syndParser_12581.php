<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Société générale de Presse et d’Edition S.A.L  [# publisher id = 69]
//Title      : L' Orient Today [ English ] 
//Created on : Apr 12, 2022, 7:02:28 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12581 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}