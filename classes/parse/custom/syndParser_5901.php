<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Kataeb.org  [# publisher id = 1288]
//Title      : Kataeb.org [ Arabic ] 
//Created on : Dec 03, 2020, 11:41:05 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5901 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}