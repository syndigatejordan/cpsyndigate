<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : Jagran Prakashan Ltd  [# publisher id =564 ]
// Titles    : Only My Health [English]
///////////////////////////////////////////////////////////////////////////

class syndParser_1628 extends syndParseXmlContent {

  public $imagesArray = null;

  public function customInit() {
    parent::customInit();
    $this->extensionFilter = 'xml';
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getRawArticles(&$fileContents) {
    $this->addLog("getting articles raw text");
    return $this->getElementsByName('article', $fileContents);
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->getElementByName('headline', $text);
    $headline = trim(strip_tags($headline));
    return $headline;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('date', $text);
    $time = $this->getElementByName('time', $text);
    $date = date('Y-m-d', strtotime($date . " " . $time));
    return $date;
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $story = $this->getElementByName('content', $text);
    $story = trim(strip_tags($story, '<p><span><table><tr><td>'));
    return $story;
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    $auther = trim($this->getElementByName('author', $text));
    return $auther;
  }

  protected function getImages(&$text) {
    $this->addLog("getting article images");
    $text = html_entity_decode($text);
    $story = $this->getElementByName('content', $text);
    if (!empty($story)) {
      $imagesArray = array();
      preg_match_all("/<img[^>]+\>/i", $story, $imgs);
      foreach ($imgs[0] as $img) {
        $imageInfo = syndParseHelper::getImgElements($img, 'img');
        $imagePath = $imageInfo[0];

        if (!$imagePath) {
          continue;
        }
        $imagePath_original = $imagePath;
        $imagePath = str_replace(' ', '%20', $imagePath);
        //$imagePath = "http://www.onlymyhealth.com" . $imagePath;
        $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

        if (!$copiedImage) {
          echo "no pahr";
          continue;
        }
        $images = $this->getAndCopyImagesFromArray(array($copiedImage));
        $name_image = explode('/images/', $copiedImage);
        if ($images[0]['image_caption'] == $name_image[1]) {
          $images[0]['image_caption'] = '';
        }
        $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
        $images[0]['is_headline'] = false;

        $new_img = str_replace($imagePath_original, $images[0]['img_name'], $img);
        $text = str_replace($img, $new_img, $text);

        array_push($imagesArray, $images[0]);
      }
      return $imagesArray;
    }
  }

}
