<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Pro Event Co.  [# publisher id =1218 ] 
//Title      : APAimages [ English ] 
//Created on : Apr 28, 2019, 09:39:06 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5488 extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getRawArticles(&$fileContents) {
    //get raw articles
    $this->addLog("getting raw articles text");
    $rawArticles = $this->getElementsByName('GalleryImage', $fileContents);
    return $rawArticles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->textFixation($this->getCData($this->getElementByName('headline', $text)));
    if (empty($headline)) {
      $headline = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
    }
    $image_id=$this->textFixation($this->getElementByName('image_id', $text));
    return $headline." #$image_id";
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $body = "";
    $caption = $this->getCData($this->getElementByName('caption', $text));
    $keyword = $this->textFixation($this->getCData($this->getElementByName('keyword', $text)));
    $body = "<p>$caption</p><p>Keyword: $keyword</p>";
    return $body;
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = trim(preg_replace('/GMT(.*)/is', '', $this->getElementByName('ctime', $text)));
    return date('Y-m-d', strtotime($date));
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getCData($this->getElementByName('author', $text)));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog('getting article category');
    return $this->textFixation($this->getCData($this->getElementByName('keyword', $text)));
  }

  protected function getImages(&$text) {
    // this contain just one image [ video image ]
    $this->addLog("getting video image");
    $file_name = $this->textFixation($this->getElementByName('file_name', $text));
    $file_name = explode(".", $file_name);
    $image_Name = $this->textFixation($this->getElementByName('image_id', $text)) . ".{$file_name[1]}";
    $image_full_path = $this->currentDir . $image_Name;
    $original_name = explode('.', $image_Name);
    if (file_exists($image_full_path)) {
      //the video name is same as the image name here  		
      $name = $this->model->getImgReplacement($image_Name, $this->currentDir, 5488);
      $img['img_name'] = str_replace(IMGS_PATH, IMGS_HOST, $name);
      $img['original_name'] = $original_name[0];
      $img['image_caption'] = "";
      $img['is_headline'] = true;
      $images[] = $img;
      //var_dump($images);exit;
      return $images;
    } else {

      return false;
    }
  }

  public function getArticleOriginalId($params = array()) {
    $articleOriginalId = sha1($this->currentlyParsedFile);
    $this->addLog($this->title->getId() . '_' . $articleOriginalId);
    return $this->title->getId() . '_' . $articleOriginalId;
  }

}
