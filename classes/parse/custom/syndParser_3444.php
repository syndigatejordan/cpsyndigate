<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dubai Corporation of Tourism & Commerce Marketing (DCTCM)  [# publisher id = 1175]
//Title      : Dubai Tourism [ English ] 
//Created on : Feb 18, 2018, 9:34:44 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_3444 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
