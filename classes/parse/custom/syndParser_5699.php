<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Muse Arabia  [# publisher id = 727]
//Title      : Muse Arabia - Travel [ English ] 
//Created on : Sep 19, 2021, 8:52:52 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5699 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}