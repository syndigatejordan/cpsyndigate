<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : Customized Feeds for Yahoo
//Title     : Layalina - Custom [ Arabic ]
/////////////////////////////////////////////////////////////////////////////////////


class syndParser_1729 extends syndParseCms {
  public function customInit() {
    parent::customInit();
    $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('ar');
  }
public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = $this->textFixation($this->getCData($this->getElementByName('fulltext', $text)));
    $this->story = str_replace("<strong><br />", "<strong>", $this->story);
    $this->story = str_replace("<br /><strong>", "<strong>", $this->story);
    $this->story = str_replace("</strong><br />", "</strong>", $this->story);
    $this->story = str_replace("<br /></strong>", "</strong>", $this->story);
    $this->story = str_replace("<strong><br /></strong>", "", $this->story);
    $this->story = str_replace("<br /><br />", "<br />", $this->story);
    $this->story = str_replace("<br />", "</p><p>", $this->story);
    $this->story = str_replace("<p>\n</p>", "", $this->story);
    $this->story = str_replace("<p>\n", "<p>", $this->story);
    $this->story = str_replace("\n<p>", "<p>", $this->story);
    $this->story = str_replace("\n</p>", "</p>", $this->story);
    $this->story = str_replace("</p>\n", "</p>", $this->story);
    $this->story = str_replace("\n&nbsp;", " ", $this->story);
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

}