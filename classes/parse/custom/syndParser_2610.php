<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Singapore International Chamber of Commerce  [# publisher id = 853]
//Title      : Singapore International Chamber of Commerce (SICC) - News [ English ] 
//Created on : Feb 08, 2016, 1:12:37 PM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2610 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}