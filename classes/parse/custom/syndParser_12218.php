<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dawat Media  [# publisher id = 1637]
//Title      : Dawat Media [ English ] 
//Created on : Oct 07, 2021, 12:30:22 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12218 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}