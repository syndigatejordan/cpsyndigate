<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : KABAR News Agency  [# publisher id = 1415]
//Title      : KABAR News Agency [ English ] 
//Created on : Oct 22, 2020, 10:32:32 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6602 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}