<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Swedish ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_592 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority'			=>	'Hankintaviranomaisen', 
										'Nature of Contract'			=> 	'Typ av kontrakt', 
										'Regulation of Procurement' 	=> 	'Reglering av upphandling', 
										'Type of bid required'			=>	'Typ av budet som krävs', 
										'Award Criteria'				=>	'Tilldelningskriterier', 
										'Original CPV'					=> 	'Original CPV', 
										'Country'						=>	'Land', 
										'Document Id' 					=>	'Dokument ID', 
										'Type of Document'				=>	'Typ av dokument', 
										'Procedure'						=>	'Förfarande', 
										'Original Language'				=>	'Originalspråk', 
										'Current Language'				=> 	'Aktuellt språk');	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('sv');
		}
	}