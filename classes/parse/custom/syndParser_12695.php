<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Malay Post  [# publisher id = 1836]
//Title      : The Malay Post [ English ] 
//Created on : Aug 08, 2022, 9:54:13 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12695 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}