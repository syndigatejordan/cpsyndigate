<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ZAPress.com  [# publisher id = 815]
//Title      : ZAPress.com [ Arabic ] 
//Created on : Nov 30, 2015, 12:48:23 PM
//Author     : safaa
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2492 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('ar');
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story)) {
      return '';
    }
    return $this->story;
  }

  protected function getImages(&$text) {
    $this->story = $this->getCData($this->getElementByName('fulltext', $text));
    $this->story = html_entity_decode($this->story, ENT_NOQUOTES, 'UTF-8');
    $this->story = $this->textFixation($this->story);
    $this->imagesArray = array();
    preg_match_all("/<img[^>]+\>/i", $this->story, $imgs);

    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = explode('alt="', $img);
      if (!empty($image_caption[1])) {
        $image_caption = explode('"', $image_caption[1]);
        $image_caption = $image_caption[0];
      } else {
        $image_caption = "";
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      $imagePath = explode('?', $imagePath);
      if (count($imagePath) > 1) {
        $imagePath = $imagePath[0];
      } else {
        $imagePath = $imageInfo[0];
      }
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = str_replace('%20', ' ', $image_caption);
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['is_headline'] = false;
      $this->story = str_replace($image_caption, "", $this->story);
      array_push($this->imagesArray, $images[0]);
    }
    return $this->imagesArray;
  }

}
