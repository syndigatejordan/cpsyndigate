<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Engloba Ldª  [# publisher id = 1768]
//Title      : Jornal Tropical [ Portuguese ] 
//Created on : Apr 05, 2022, 6:48:46 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12481 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('pt'); 
	} 
}