<?php

///////////////////////////////////////////////////////////////////////////
// Publisher : ETCN International Inc.  [# publisher id =507 ]
// Titles    : ETCN China Import & Export Information [Chinese]
///////////////////////////////////////////////////////////////////////////

class syndParser_1514 extends syndParseXmlContent {
	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('zh');
		$this -> extensionFilter = 'xml';
	}

	protected function getRawArticles(&$fileContents) {
		//get articles
		$this -> addLog("getting articles raw text");
		return $this -> getElementsByName('item', $fileContents);
	}

	protected function getHeadline(&$text) {
		$this -> addLog("getting article headline");

		$headline = $this -> getElementByName('headline', $text);
		return $headline;
	}

	protected function getAbstract(&$text) {
		$this -> addLog('getting article summary');
		$summary = $this -> textFixation($this -> getElementByName('sub-headline', $text));
		return $summary;
	}

	protected function getArticleDate(&$text) {
		$this -> addLog("getting article date");

		$date = $this -> getElementByName('date', $text);
		$date = date('Y-m-d', strtotime($date));
		return $date;
	}

	protected function getStory(&$text) {
		$this -> addLog("getting article text");
		$body = $this -> getCData($this -> getElementByName('content', $text));
		$body = strip_tags($body, '<p><br><strong><b><u><i>');
		return $body;
	}

	public function getOriginalCategory(&$text) {

		$this -> addLog('Getting article category');
		$category = $this -> getCData($this -> getElementByName('category', $text));
		return $category;
	}

	protected function getAuthor(&$text) {
		$this -> addLog("getting article author");
		return $this -> textFixation($this -> getCData($this -> getElementByName('publication', $text)));
	}

}
