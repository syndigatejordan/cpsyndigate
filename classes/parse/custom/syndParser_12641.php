<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Tunisie News  [# publisher id = 1810]
//Title      : Tunisie News [ Arabic ] 
//Created on : Jun 16, 2022, 11:27:22 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12641 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}