<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Congolese Press Agency (ACP)  [# publisher id = 1371]
//Title      : Congolese Press Agency (ACP) [ French ] 
//Created on : Oct 06, 2020, 12:54:25 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6463 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}