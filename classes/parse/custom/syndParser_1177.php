<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Armenpress News Agency  [# publisher id = 354]
//Title      : Armenpress News Agency [ Russian ] 
//Created on : Aug 09, 2021, 12:14:23 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_1177 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}