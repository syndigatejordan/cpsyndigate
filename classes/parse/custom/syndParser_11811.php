<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ODO Septima  [# publisher id = 1594]
//Title      : Brestskii Kaleidoskop [ Russian ] 
//Created on : Jun 21, 2021, 2:05:50 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11811 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ru'); 
	} 
}