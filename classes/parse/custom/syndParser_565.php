<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Spanish ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_565 extends syndParseWMCCM {
		
		protected $translations = array('Awarding Authority'		=>	'Adjudicación Autoridad', 
										'Nature of Contract'		=>	'Naturaleza del contrato', 
										'Regulation of Procurement' => 	'Reglamento de Adquisiciones', 
										'Type of bid required'		=>	'Tipo de oferta solicitada',
										'Award Criteria'			=>	'Criterios de adjudicación', 
										'Original CPV'				=>	'Original CPV', 
										'Country'					=>	'País', 
										'Document Id' 				=>	'Documento Id', 
										'Type of Document'			=>	'Tipo de Documento',
										'Procedure'					=>	'Procedimiento',
										'Original Language'			=>	'Lengua original', 
										'Current Language'			=> 	'Idioma Actual');	

		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('es');
			
		}
	}