<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Togo-Presse  [# publisher id = 1156]
//Title      : Togo-Presse [ French ] 
//Created on : Sep 04, 2017, 7:34:33 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3152 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}