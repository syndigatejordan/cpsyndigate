<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Haaretz Daily Newspaper Ltd.  [# publisher id = 937]
//Title      : Haaretz [ English ] 
//Created on : May 19, 2016, 7:56:27 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2732 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}