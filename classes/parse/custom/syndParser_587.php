<?php
	//////////////////////////////////////////////////////////////////////////////
	// Publisher: WMCCM
	// Titles   : WMCCM Tenders [ Portuguese  ]
	//////////////////////////////////////////////////////////////////////////////

	class syndParser_587 extends syndParseWMCCM {
		protected $translations = array('Awarding Authority'			=>	'Entidade adjudicante', 
									'Nature of Contract'			=> 	'Natureza do contrato', 
									'Regulation of Procurement' 	=> 	'Regulamento para as aquisições', 
									'Type of bid required'			=>	'Tipo de proposta necessário', 
									'Award Criteria'				=>	'Critérios de adjudicação', 
									'Original CPV'					=> 	'Original CPV', 
									'Country'						=>	'País', 
									'Document Id' 					=>	'Documento de Identificação', 
									'Type of Document'				=>	'Tipo de Documento', 
									'Procedure'						=>	'Processo', 
									'Original Language'				=>	'Língua Original', 
									'Current Language'				=> 	'Idioma atual'
									);	
		
		public function customInit() {
			parent::customInit();
			$this->defaultLang = $this->model->getLanguageId('pt');
		}
	}