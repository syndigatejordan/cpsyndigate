<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Neotrope  [# publisher id = 841]
//Title      : Florida Newswire [ English ] 
//Created on : Sep 19, 2021, 10:18:39 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7351 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}