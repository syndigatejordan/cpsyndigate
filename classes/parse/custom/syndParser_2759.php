<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Dikgang Publishing Company (DPC)  [# publisher id = 948]
//Title      : The Monitor [ English ] 
//Created on : May 18, 2016, 12:49:58 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2759 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}