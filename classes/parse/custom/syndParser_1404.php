<?php
//////////////////////////////////////////////////////////////////////////////
// Publisher: Sinocast LLC  [# publisher id =447 ]
// Titles   : Sinocast China Business Beat [ English ]
//////////////////////////////////////////////////////////////////////////////

class syndParser_1404 extends syndParseXmlContent {

	public function customInit() {
		parent::customInit();
		$this -> defaultLang = $this -> model -> getLanguageId('en');
	}

	public function getRawArticles(&$fileContents) {
		$this -> addLog("getting articles raw element");

		return $this -> getElementsByName('news', $fileContents);
	}

	protected function getHeadline(&$text) {
		$this -> addLog("getting article headline");

		return $this -> textFixation($this -> getElementByName('title', $text));
	}

	protected function getStory(&$text) {
		$this -> addLog("getting article text");

		$story = $this -> textFixation($this -> getElementByName('content', $text));
		$story = preg_replace('/<paragraph>/i', '<p>', $story);
		$story = preg_replace('/<\/paragraph>/i', '</p>', $story);
		return $story;
	}

	protected function getArticleDate(&$text) {
		$this -> addLog("getting article date");

		$date = $this -> textFixation($this -> getElementByName('publish_date', $text));
		return $this -> dateFormater($date);
	}

	protected function getOriginalCategory(&$text) {
		$this -> addLog("getting article original category");

		return $this -> textFixation($this -> getElementByName('category', $text));
	}

}