<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : ACN Newswire Co Ltd  [# publisher id = 1174]
//Title      : ACN Newswire [ English ] 
//Created on : Sep 23, 2021, 7:57:48 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_3441 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}