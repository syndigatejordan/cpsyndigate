<?php

/////////////////////////////////////////////////////////////////////////////////////
//Publisher : Agency Tunis Afrique Press  [# publisher id =142 ] 
//Title     : Agency Tunis Afrique Press
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_392 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    // $this->charEncoding = 'UTF-8';
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getStory(&$text) {
    $this->addLog("getting article text");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    $this->story = html_entity_decode($this->story, ENT_QUOTES, "UTF-8");
    return $this->story;
  }

  protected function getImages(&$text) {

    $this->story = trim($this->textFixation($this->getCData($this->getElementByName('fulltext', $text))));
    $imagesArray = array();
    preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePath = $imageInfo[0];
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $title = $this->textFixation($this->getElementByName('title', $text));
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath, $title);

      if (!$copiedImage) {
        echo "no pahr";
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePath, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl, $title) {
    sleep(1);
    $baseName = basename($imageUrl);
    $baseName = explode(".", $baseName);
    $baseNameUn = sha1($title . $imageUrl) . "." . $baseName[1];
    $copiedImage = $this->imgCacheDir . $baseNameUn;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }
    if (!file_exists($copiedImage)) {
      $curl_handle = curl_init();
      curl_setopt($curl_handle, CURLOPT_URL, $imageUrl);
      curl_setopt($curl_handle, CURLOPT_VERBOSE, true);
      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
      $useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0";
      curl_setopt($curl_handle, CURLOPT_USERAGENT, $useragent);
      // This is what solved the issue (Accepting gzip encoding)
      curl_setopt($curl_handle, CURLOPT_ENCODING, "gzip,deflate");
      $imgContent = curl_exec($curl_handle);
      curl_close($curl_handle);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

}
