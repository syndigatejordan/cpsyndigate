<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Peoples Media Ltd  [# publisher id = 949]
//Title      : Peoples Daily [ English ] 
//Created on : Sep 19, 2021, 7:04:37 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2746 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}