<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Public Domain Review  [# publisher id = 1298]
//Title      : The Public Domain Review - Images [ English ] 
//Created on : Feb 10, 2020, 8:57:29 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5914 extends syndParseCms {

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

}
