<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Toyo Keizai Co., Ltd.  [# publisher id = 1806]
//Title      : Shikiho [ Japanese ] 
//Created on : Jun 06, 2022, 11:56:59 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12635 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	} 
}