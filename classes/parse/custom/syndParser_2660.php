<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Government of Singapore  [# publisher id = 894]
//Title      : Singapore Ministry of Foreign Affairs - Press Statements [ English ] 
//Created on : Apr 07, 2016, 10:07:19 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2660 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}