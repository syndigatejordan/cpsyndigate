<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Fast Company, Inc  [# publisher id = 1252]
//Title      : Fast Company [ English ] 
//Created on : Jun 11, 2019, 9:08:15 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5650 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}