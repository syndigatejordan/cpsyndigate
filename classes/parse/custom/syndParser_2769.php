<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Vanguard Media Limited  [# publisher id = 968]
//Title      : Vanguard  [ English ] 
//Created on : May 22, 2016, 6:53:06 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_2769 extends syndParseCms {

  public $story;

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  public function getStory(&$text) {
    $this->addLog('Getting article story');
    $this->story = str_replace('figure', 'div', $this->story);
    $this->story = str_replace('figcaption', 'div', $this->story);
    $this->story = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) srcset=".*?"/i', '$1', $this->story);
    $this->story = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) height=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) width=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) fit-vids=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) itemprop=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) onerror=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) data-srcset=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) data-src=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('/(<[^>]+) sizes=".*?"/i', '$1', $this->story);
      $this->story = preg_replace('!\s+!', ' ', $this->story);
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);

      if (empty($this->story)) {
          return '';
      }
      return $this->story;
  }

}
