<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ministry of Foreign Affairs - Arab Republic of Egypt  [# publisher id = 836]
//Title      : Ministry of Foreign Affairs of the Arab Republic of Egypt - News [ Arabic ] 
//Created on : Jan 30, 2016, 8:28:03 PM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2562 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}