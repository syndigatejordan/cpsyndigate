<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : 7 Dimensions Media FZE  [# publisher id = 1216]
//Title      : Air Cargo Update [ English ] 
//Created on : Jul 15, 2019, 7:17:29 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_5459 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}