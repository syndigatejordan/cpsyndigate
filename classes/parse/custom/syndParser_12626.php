<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Toyo Keizai Co., Ltd.  [# publisher id = 1806]
//Title      : Toyo Keizai Online [ Japanese ] 
//Created on : May 29, 2022, 10:07:52 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12626 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ja'); 
	} 
}