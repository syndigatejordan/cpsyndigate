<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : EPE-SPA El Moudjahid  [# publisher id = 931]
//Title      : El Moudjahid [ French ] 
//Created on : May 16, 2016, 11:37:38 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2726 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}