<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Al-Alam News Network  [# publisher id = 199]
//Title      : ALALAM (Photo Service) [ Arabic ] 
//Created on : Jan 24, 2019, 7:08:42 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_705 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}