<?php

///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The White House  [# publisher id = 1318]
//Title      : The Obama White House [ English ] 
//Created on : Mar 03, 2020, 9:23:55 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////

class syndParser_5952 extends syndParseCms {

  public $videosIds = array();

  public function customInit() {
    parent::customInit();
    $this->defaultLang = $this->model->getLanguageId('en');
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('/<iframe(.*?)<\/iframe>/is', '', $this->story);
    foreach ($this->videosIds as $id) {
      $this->story .="<p><iframe width=\"600\" height=\"400\" src=\"https://www.youtube.com/embed/$id\"></iframe></p>";
    }
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $this->videosIds=array();
    $fulltext = $this->textFixation($this->getElementByName('fulltext', $text));
    $videos = array();
    $matches = null;
    preg_match_all('/<iframe(.*?)<\/iframe>/is', $fulltext, $matches);
    foreach ($matches[0] as $matche) {
      $video = array();
      $videoname = syndParseHelper::getImgElements($matche, 'iframe', 'src');
      if (preg_match("/youtube/", $videoname[0])) {
        $id = null;
        preg_match("/embed\/(.*)/", $videoname[0], $id);
        $id = preg_replace("/\?(.*)/is", "", $id[1]);
        $videoname[0] = "https://www.youtube.com/watch?v=$id";
        array_push($this->videosIds, $id);
      }
      $video['video_name'] = $videoname[0];
      $videoName = $this->textFixation($this->getCData($this->getElementByName('title', $text)));
      $video['original_name'] = $videoname[0];
      $video['video_caption'] = $videoName;
      $date = $this->getElementByName('date', $text);
      $video['added_time'] = date('Y-m-d h:i:s', strtotime($date));
      $video['mime_type'] = "";
      $videos[] = $video;
    }
    return $videos;
  }

}
