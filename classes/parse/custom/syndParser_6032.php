<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Arcturus Publishing Limited  [# publisher id = 1334]
//Title      : Color by Number - The ‘Flowers’ Issue [ English ] 
//Created on : Apr 22, 2020, 8:11:14 AM
//Author     : mariam
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6032 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}