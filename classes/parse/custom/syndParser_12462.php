<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : MM ACTIV  [# publisher id = 1766]
//Title      : BioSpectrum India [ English ] 
//Created on : Feb 20, 2022, 11:53:11 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12462 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}