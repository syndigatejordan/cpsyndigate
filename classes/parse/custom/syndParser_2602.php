<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : The Federation of Pakistan Chambers of Commerce & Industry  [# publisher id = 845]
//Title      : The Federation of Pakistan Chambers of Commerce & Industry (FPCCI) - Press Releases [ English ] 
//Created on : Feb 09, 2016, 8:06:56 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2602 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}