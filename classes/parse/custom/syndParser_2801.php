<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Fikra for Publishing & Printing Ltd.  [# publisher id = 981]
//Title      : Swift Newz [ Arabic ] 
//Created on : Sep 19, 2021, 7:04:45 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2801 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}