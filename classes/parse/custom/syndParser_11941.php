<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Rainbow Newspaper Zambia Limited  [# publisher id = 1601]
//Title      : The Rainbow Newspaper [ English ] 
//Created on : Jul 05, 2021, 8:37:01 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_11941 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}