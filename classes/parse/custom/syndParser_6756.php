<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Rossiya Segodnya  [# publisher id = 1441]
//Title      : SPUTNIK [ Serbian (Cyrillic) ] 
//Created on : Oct 18, 2020, 10:11:10 AM
//Author     : eyad
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_6756 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('sr-Cyrl'); 
	} 
}