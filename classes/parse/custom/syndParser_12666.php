<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Les Editions Yassine  [# publisher id = 1823]
//Title      : L'Info Express [ French ] 
//Created on : Jul 13, 2022, 12:55:44 PM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12666 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}