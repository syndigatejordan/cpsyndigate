<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Ministry of Foreign Affairs of the Palestinain State  [# publisher id = 880]
//Title      : Ministry of Foreign Affairs of the Palestinain State - Missions News  [ Arabic ] 
//Created on : Feb 16, 2016, 9:33:10 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_2633 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('ar'); 
	} 
}