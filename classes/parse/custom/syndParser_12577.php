<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Euronews  [# publisher id = 604]
//Title      : Euronews [ Hungarian ] 
//Created on : Apr 10, 2022, 8:55:08 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12577 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('hu'); 
	} 
}