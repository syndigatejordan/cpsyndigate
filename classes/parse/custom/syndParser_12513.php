<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : HCH24  [# publisher id = 1784]
//Title      : HCH24 [ French ] 
//Created on : Mar 08, 2022, 6:41:53 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_12513 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('fr'); 
	} 
}