<?php 
///////////////////////////////////////////////////////////////////////////////////// 
//Publisher  : Coconuts Media Limited  [# publisher id = 1468]
//Title      : BK Magazine [ English ] 
//Created on : Dec 14, 2020, 9:27:21 AM
//Author     : majdi
/////////////////////////////////////////////////////////////////////////////////////
		
class syndParser_7369 extends syndParseCms { 
	 public function customInit() { 
		 parent::customInit(); 
		 $this->defaultLang = $this->model->getLanguageId('en'); 
	} 
}