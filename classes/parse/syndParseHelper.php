<?php
	final class syndParseHelper
	{
		private function __construct(){}
		/**
		* getting category name from the file name by returning the file basename without extension
		*
		* @param String $path file Path
		* @return string
		*/
		public static function getCategoryNameFromFileName($path)
		{
			$path_parts = pathInfo($path);
			$path_parts['basename'] = substr($path_parts['basename'], 0, strlen($path_parts['basename'])-(strlen($path_parts['extension'])+1)); 
			return $path_parts['basename'];
		}

		/**
	 	* getting all images sources from HTML text
	 	*
	 	* @param String $text HTML Text
	 	* @return array of strings ( images sources )
	 	*/
		public static function getImgElements($text, $tagName = 'img', $src = 'src')
		{
			$regExp = '<'.$tagName.'[\s]+.*'.$src.'[\s]*=[\s]*([\"]([^>\"]*)[\"]|'."[\\']([^>\\']*)[\\']|([^>\\s]*))([\\s]|[^>])*[\/]?>";
			preg_match_all("/$regExp/i", $text, $matches);
			$sources = array();
			foreach($matches[1] as $src) {
				$sources[] = trim($src,'"\'');
			}
			return $sources;
		}
		
		
		public static function getElements($text, $tagName = 'video')
		{
			
			$regExp = '<'.$tagName."*?\w+((\s+(\w|\w[\w-]*\w)(\s*=\s*(?:\".*?\"|'.*?'|[^'\">\s]+))?)+\s*|\s*)\/?>";  
			preg_match_all("/$regExp/i", $text, $matches);
			$sources = array();
			foreach($matches[1] as $src) {
				$sources[] = trim($src,'"\'');
				
			}
			return $sources;
		}
		

		/**
		 * getting the date from the file path EX:(/path/to/file/YYYY/MM/DD/file.xml)
		 *
		 * @return string date
		 */
		public static function getDateFromPath($path)
		{
			$dirName = dirname($path);
			$dir = explode('/', $dirName);
			$lastIndex = count($dir)-1;

			if($lastIndex>=2) {
				$day   = (int)$dir[$lastIndex];
				$month = (int)$dir[$lastIndex-1];
				$year  = (int)$dir[$lastIndex-2];
				return "$year-$month-$day";
			}
			return '';
		}

		/**
		 * replace images sourses with custom format after copying it, to the current host
		 *
		 * @param string $text HTML text
		 * @param string $host
		 * @param array &$images array with images sources with it's replacements
		 * @return String fixed HTML text

		public static function fixingImagesSource($model, $text, $host, &$images)
		{
			//get text images
			$textImgs = self::getImgElements($text);
			$imgsReplacements = $model->getImgsReplacements($textImgs, $host, $this->titleId);
			$images = $imgsReplacements;
			//replace images src with images replacements
			foreach($imgsReplacements as $originalPath => $replacementsPath) {
				$text = str_replace($originalPath, $replacementsPath, $text);
			}

			return $text;
		}*/


		/**
		 * checking for file extension
		 *
		 * @param String $filePath file path
		 * @param String $extension file extension to be matched
		 * @return Boolean true if matched and false otherwise
		 */
		public static function isFileExtension($filePath, $extension)
		{
			$path_parts = pathinfo($filePath);
			if(is_array($extension)) {
				return in_array($path_parts['extension'], $extension);
			}
			if ($extension == $path_parts['extension']) {
				return true;
			}
			return false;
		}
		
		/**
		 * Get File extention
		 *
		 * @param String $filePath file path
		 * @return String file extention if true or false if error accessing file
		 */
		public static function getFileExtension($filePath)
		{
			$path_parts = pathinfo($filePath);
			if(is_array($path_parts)) {
				return $path_parts['extension'];
			} else {
				return false;
			}
		}
		

		public static function getStringsBetween($start, $end, $string)
		{
			$splitByEnd = split($end, $string);
			$returnValue = array();
			foreach($splitByEnd as $split) {
				$part = split($start, $split);
				if(count($part)>1) {
					$returnValue[] = $part[1];
				}
			}
			return $returnValue;
		}

		public static function getStringBefore($delimiter, $string, $defaultReturn = '')
		{
			$parts = split($delimiter, $string);
			if(count($parts) > 1) {
				return $parts[0];
			}
			return $defaultReturn;
		}

		public static function getStringAfter($delimiter, $string, $defaultReturn = '')
		{
			$parts = split($delimiter, $string);
			if(count($parts) > 1) {
				return $parts[1];
			}
			return $defaultReturn;
		}
		
		
		public static function getDirectoryFiles($dir)
		{
			$files = array();
			
			if ($handle = opendir($dir)) {
				while (false !== ($file = readdir($handle))) {
					
					if(is_file($dir . $file)) {
						$files[] = $dir . $file; 
					}
				}
				closedir($handle);
				return $files;
			} else {
				return false;
			}
		}
}
?>
