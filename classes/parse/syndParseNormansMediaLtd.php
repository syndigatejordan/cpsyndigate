<?php

/**
 * Description of syndParseNormansMediaLtd
 *
 * @author eyad
 */
class syndParseNormansMediaLtd extends syndParseXmlContent
{

    public $languageTitle;

    public function customInit()
    {
        parent::customInit();
        $languageId = $this->title->getLanguageId();
        switch ($languageId) {
            case 1:
                $this->languageTitle = "Arabic";
                $this->defaultLang = $this->model->getLanguageId('ar');
                break;
            case 2:
                $this->languageTitle = "English";
                $this->defaultLang = $this->model->getLanguageId('en');
                break;
            case 4:
                $this->languageTitle = "French";
                $this->defaultLang = $this->model->getLanguageId('fr');
                break;
        }
        $this->extensionFilter = '';
    }


    public function getRawArticles(&$fileContents)
    {
        //get raw articles
        $this->addLog("getting raw articles text");
        $rawArticles = $this->getElementsByName('Story', $fileContents);
        return $rawArticles;
    }

    protected function getHeadline(&$text)
    {
        $this->addLog("getting article headline");
        return trim($this->textFixation($this->getCData($this->getElementByName('Headline', $text))));
    }

    protected function getStory(&$text)
    {
        $this->addLog("getting article body");
        return $this->textFixation($this->getCData($this->getElementByName('Text', $text)));
    }

    protected function getArticleDate(&$text)
    {
        $this->addLog("getting article date");
        $date = trim($this->getCData($this->getElementByName('Date', $text)));
        $date = date('Y-m-d', strtotime($date));
        return $date;
    }
    public function getArticleOriginalId($params = array())
    {
        $Headline = trim($this->getCData($this->getElementByName('Headline', $params['text'])));
        $Text = trim($this->getCData($this->getElementByName('Text', $params['text'])));
        $Date = trim($this->getCData($this->getElementByName('Date', $params['text'])));
        $transmissionID=$Headline.$Text.$Date;
        if ($transmissionID) {
            return $this->title->getId() . '_' . sha1($transmissionID);
        } else {
            return parent::getArticleOriginalId($params);
        }
    }
    public function getArticleReference(&$text)
    {
        $this->addLog("getting article reference");
        return "";
    }
    protected function getImages(&$text)
    {
        $imagesArray = array();
        return $imagesArray;
    }


}