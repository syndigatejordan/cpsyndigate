<?php
////////////////////////////////////////////////////////////////////////
// Publisher : WMCCM 188
////////////////////////////////////////////////////////////////////////
class syndParseWMCCM extends syndParseXmlContent {
	protected $translations;	
	
	public function customInit() {
		parent::customInit();
	}
			
	public function getRawArticles(&$fileContents) {
		$this->addLog("getting raw articles text");
		return $this->getElementsByName("doc", $fileContents);
	}

	protected function getStory(&$text) {				
		$this->addLog("getting article text");
		$story  	=  $this->textFixation($this->getElementByName('content', $text));
		
		$sector 	=  $this->textFixation($this->getElementByName("sector", $text));
		$market		=  $this->textFixation($this->getElementByName("market", $text));
		$marketOrg	=  $this->textFixation($this->getElementByName("marketorg", $text));
		
		$typeBid		=  $this->textFixation($this->getElementByName("typebid", $text));
		$awardCrit		=  $this->textFixation($this->getElementByName("awardcrit", $text));
		$originalCpv	=  $this->getElementsByName("originalcpv", $text);
	
		$isoCountry		=  $this->textFixation($this->getElementByName("isocountry", $text));
		
		$docId			=  $this->textFixation($this->getElementByName("nodocojs", $text));
		$proc			=  $this->textFixation($this->getElementByName("proc", $text));
		$docType		=  $this->textFixation($this->getElementByName("natnotice", $text));
		
		$lang		=  syndParseHelper::getElements($text, "OTH_NOT");
		
		if(!$lang) {
			$lang = syndParseHelper::getElements($text, "CONTRACT");
		}
		
		$lang		=  explode(" ", $lang[0]);
		
		$langCriteria = new Criteria();
		
		$origLang	=  explode("\"", $lang[3]);
		$origLang	=  $origLang[1];
		
		$currLang	=  explode("\"", $lang[2]);
		$currLang	=	$currLang[1];
		
		$langCriteria->add(LanguagePeer::CODE , $origLang);
		$orig = LanguagePeer::doSelectOne($langCriteria);

		if($orig) {
			$origLang = $orig;
		}
		
		$currLangCriteria = new Criteria();
		$currLangCriteria->add(LanguagePeer::CODE , $currLang);
		$curr = LanguagePeer::doSelectOne($currLangCriteria);
		
		if($curr) {
			$currLang = $curr;
		}
		
		$countryCriteria = new Criteria();
		$countryCriteria->add(CountryPeer::ISO, $isoCountry, Criteria::EQUAL);
		$country = CountryPeer::doSelectOne($countryCriteria);
		
		if($country) {
           $isoCountry = $country->getPrintableName();
        }
      	
        if($sector|| $market || $marketOrg || $typeBid || $awardCrit || $originalCpv || $isoCountry || $docId || $proc || $origLang || $currLang || $docType) {
       		$story .= "\n <p> <b>More info: </b> \n";
       		
       		$this->addToBody($this->translations["Awarding Authority"], $sector, $story);
			$this->addToBody($this->translations["Nature of Contract"], $market, $story);
			$this->addToBody($this->translations["Regulation of Procurement"], $marketOrg, $story);
			$this->addToBody($this->translations["Type of bid required"], $typeBid, $story);
		
			$this->addToBody($this->translations["Award Criteria"], $awardCrit, $story);
			$cpvs="";
			foreach ($originalCpv as $cpv) {
				$cpvs .= $cpv .", ";
			}
			
			$this->addToBody($this->translations["Original CPV"], $cpvs, $story);
			$this->addToBody($this->translations["Country"], $isoCountry, $story);
			$this->addToBody($this->translations["Document Id"], $docId, $story);
			$this->addToBody($this->translations["Type of Document"], $docType, $story);
			$this->addToBody($this->translations["Procedure"], $proc, $story);
			$this->addToBody($this->translations["Original Language"], $origLang, $story);
			$this->addToBody($this->translations["Current Language"], $currLang, $story);
       
			$story 	.="</p>";
        }
        
        $story 	 = str_replace('<p>[...]</p>', "", $story);
        $story	 = preg_replace("/<addr+\>/i", "Address: ", $story);
        
		return $story;
	}

	protected function getHeadline(&$text) {
		$this->addLog("getting article headline");
		return $this->textFixation($this->getElementByName('tidoc', $text));
	}

	protected function getArticleDate(&$text) {		
		$this->addLog("getting article headline");
		$date = $this->textFixation($this->getElementByName('datepub', $text)); 
		$date = substr($date, 0, 4) . '-' . substr($date, 4, 2) . '-' . substr($date, 6, 2);
		return $this->dateFormater($date);
	}
	
	public function getArticleOriginalId($params = array())
		{
			$defaultArray = array('text' => '', 'headline' =>'', 'articleDate' => '', 'issueNum' => 0);
			$params =  array_merge($defaultArray, $params);
			if(!$articleOriginalId = $this->getElementByName('nodocojs', $params['text'])) {
				return parent::getArticleOriginalId($params);
			}
			return $this->title->getId().'_' .$articleOriginalId;
		}
		
	
	protected function addToBody($caption, $value, &$body) {
		$value = trim($value);
		
		if($value && $value != 'N/A') {
			$body .= "$caption : $value .<br />\n"; 
		}	
	}

}
