<?php

/**
 * Description of syndParseInfomedia
 *
 * @author eyad
 */
class syndParseInfomedia extends syndParseXmlContent
{

    public $languageTitle;

    public function customInit()
    {
        parent::customInit();
        $languageId = $this->title->getLanguageId();//
        $language = new Criteria();
        $language->add(LanguagePeer::ID, $languageId);
        $currentLanguage = LanguagePeer::doSelectOne($language);
      
        $this->languageTitle = $currentLanguage->getName();
        $this->defaultLang = $this->model->getLanguageId($currentLanguage->getCode());
        $this->extensionFilter = 'xml';
    }


    public function getRawArticles(&$fileContents)
    {
        //get raw articles
        $this->addLog("getting raw articles text");
        $matches = NULL;
        preg_match_all("/<document(.*?)<\/document>/is", $fileContents, $matches);
        return $matches[0];
    }

    public function getArticleReference(&$text)
    {
        $this->addLog("getting article reference");
        $match = null;
        preg_match("/<direct_url>(.*?)<\/direct_url>/is", $text, $match);
        if (isset($match[1])) {
            return trim($this->textFixation($this->getCData(strip_tags($match[1]))));
        } else {
            return "";
        }
    }

    public function getArticleOriginalId($params = array())
    {
        /*  $id_site = syndParseHelper::getImgElements($params['text'], 'document', 'id_site');
          if (!empty($id_site[0])) {
              $id_site = trim($id_site[0]);
          } else {
              $id_site = "";
          }
          $id_article = syndParseHelper::getImgElements($params['text'], 'document', 'id_article');
          if (!empty($id_article[0])) {
              $id_article = trim($id_article[0]);
          } else {
              $id_article = "";
          }*/
        $match = null;
        preg_match("/<header(.*?)<\/header>/is", $params['text'], $match);
        if (isset($match[0])) {
            $header = trim($this->textFixation($this->getCData(strip_tags($match[0]))));
        } else {
            $header = "";
        }
        $date = trim($this->getCData($this->getElementByName('unix_timestamp', $params['text'])));
        $date = date('Y-m-d', $date);
        $transmissionID = "{ $header}_{$date}";
        if ($transmissionID) {
            return $this->title->getId() . '_' . sha1($transmissionID);
        } else {
            return parent::getArticleOriginalId($params);
        }
    }

    public function getOriginalCategory(&$text)
    {
        $this->addLog('getting article category');
        $cats = $this->getElementsByName('topic', $text);
        $originalCats = array();

        if (!empty($cats)) {
            foreach ($cats as $cat) {
                $originalCats[] = trim($this->textFixation($this->getCData($cat)));
            }
        }

        return trim(implode(', ', $originalCats),",");
    }

    protected function getHeadline(&$text)
    {
        $this->addLog("getting article headline");
        $match = null;
        preg_match("/<header(.*?)<\/header>/is", $text, $match);
        if (isset($match[0])) {
            return trim($this->textFixation($this->getCData(strip_tags($match[0]))));
        } else {
            return "";
        }
    }

    protected function getAbstract(&$text)
    {
        $this->addLog("getting article summary");
        $match = null;
        preg_match("/<summary(.*?)<\/summary>/is", $text, $match);
        if (isset($match[0])) {
            return trim($this->textFixation($this->getCData(strip_tags($match[0]))));
        } else {
            return "";
        }
    }

    protected function getStory(&$text)
    {
        $countrycode = trim($this->getCData($this->getElementByName('countrycode', $text)));
        $this->addLog("getting countrycode $countrycode");
        $country = new Criteria();
        $country->add(CountryPeer::ISO, $countrycode);
        $currentCountry = CountryPeer::doSelectOne($country);
        $this->addLog("getting currentCountry $currentCountry");
        if(!empty($currentCountry)) {
            $countryName = $currentCountry->getPrintableName();
            if (isset($countryName)) {
                $countryName = "<p>Country: $countryName</p>";
            } else {
                $countryName = "";
            }
        }else {
            $countryName = "";
        }
        $this->addLog("getting countryName $countryName");
        $matches = null;
        preg_match_all("/<main_source(.*?)<\/main_source>/is", $text, $matches);
        $originalSources = array();
        if (!empty($matches[0])) {

            foreach ($matches[0] as $source) {
                $originalSources[] = trim($this->getCData($this->getElementByName('name', $source)));
            }
            $originalSources = "<p>Source: " . implode(', ', $originalSources) . "</p>";
        } else {
            $originalSources = "";
        }
        $this->addLog("getting originalSources $originalSources");

        $this->addLog("getting article body");
        $body = $this->getElementByName('body', $text);
        preg_match("/<body(.*?)<\/body>/is", $text, $body);
        if (isset($body[0])) {
            $body = trim($body[0]);
        }
        return $body . $countryName . $originalSources;
    }

    protected function getArticleDate(&$text)
    {
        $this->addLog("getting article date");
        $date = trim($this->getCData($this->getElementByName('unix_timestamp', $text)));
        $date = date('Y-m-d', $date);
        return $date;
    }

    protected function getAuthor(&$text)
    {
        $this->addLog("getting article author");
        $match = null;
        preg_match("/<author>(.*?)<\/author>/is", $text, $match);
        if (isset($match[1])) {
            return trim($match[1]);
        } else {
            return "";
        }
    }

    protected function getImages(&$text)
    {
        $imagesArray = array();
        return $imagesArray;
    }

    //Handle empty body
    public function parse()
    {
        $articles = array();
        foreach ($this->files as $file) {
            if ($this->extensionFilter) {
                if (!syndParseHelper::isFileExtension($file, $this->extensionFilter)) {
                    $this->addLog("Wrong extension for file : $file)");
                    continue;
                }
            }
            if (!file_exists($file)) {
                $this->addLog("file dose not exist: $file)");
                continue;
            }

            $this->addLog("get file contents (file:$file)");
            $fileContents = $this->getFileContents($file);

            if (!$fileContents) {
                continue;
            }

            $this->currentlyParsedFile = $file;
            $this->loadCurrentDirectory();
            $this->loadUpperDir();
            $rawArticles = $this->getRawArticles($fileContents);
            foreach ($rawArticles as $rawArticle) {
                $article = $this->getArticle($rawArticle);
                $articles[] = $article;
            }
        }
        return $articles;
    }
}