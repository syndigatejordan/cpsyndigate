<?php

class syndParseCms extends syndParseXmlContent
{

  public $story;
  public $photoSyndicationRights = array(1, 6, 21, 24, 44, 62, 80, 148, 151, 152, 156, 157, 160, 167, 168, 170, 172,
      173, 174, 177, 178, 179, 186, 188, 190, 233, 255, 300, 314, 323, 324, 325, 326, 353, 395, 398, 424, 427, 461, 496,
      599, 704, 732, 746, 780, 790, 793, 870, 887, 895, 917, 918, 922, 925, 929, 930, 932, 933, 936, 937, 940, 941, 943, 957,
      959, 960, 1025, 1048, 1051, 1056, 1059, 1081, 1082, 1083, 1084, 1085, 1127, 1152, 1153, 1157, 1158, 1161, 1162,
      1163, 1164, 1165, 1166, 1217, 1219, 1223, 1231, 1233, 1244, 1245, 1246, 1254, 1256, 1262, 1277, 1279, 1338, 1347,
      1351, 1352, 1378, 1379, 1396, 1397, 1432, 1436, 1444, 1446, 1450, 1461, 1474, 1475, 1491, 1492, 1498, 1517, 1518,
      1526, 1592, 1595, 1598, 1601, 1611, 1612, 1613, 1614, 1615, 1616, 1617, 1618, 1619, 1620, 1621, 1622, 1623, 1635,
      1636, 1651, 1673, 1678, 1679, 1683, 1684, 1685, 1689, 1690, 1695, 1696, 1697, 1736, 1739, 1740, 1793, 1794, 1799,
      1800, 1801, 1802, 1803, 1804, 1805, 1806, 1807, 1809, 1811, 1842, 1920, 1934, 2075, 2095, 2240, 2257, 2268, 2274,
      2283, 2325, 2326, 2327, 2328, 2329, 2330, 2332, 2344, 2346, 2348, 2351, 2353, 2355, 2356, 2358, 2359, 2360, 2361,
      2362, 2363, 2364, 2368, 2401, 2402, 2404, 2405, 2406, 2407, 2408, 2410, 2412, 2415, 2416, 2423, 2432, 2434, 2437,
      2438, 2439, 2441, 2443, 2444, 2445, 2448, 2450, 2452, 2459, 2460, 2461, 2462, 2466, 2468, 2469, 2470, 2471, 2472,
      2482, 2484, 2493, 2496, 2510, 2517, 2543, 2546, 2547, 2548, 2549, 2550, 2551, 2552, 2554, 2557, 2558, 2560, 2562,
      2566, 2567, 2568, 2569, 2570, 2571, 2572, 2573, 2574, 2575, 2584, 2585, 2587, 2588, 2589, 2590, 2591, 2592, 2600,
      2603, 2604, 2605, 2606, 2607, 2608, 2609, 2610, 2611, 2612, 2615, 2617, 2618, 2627, 2628, 2629, 2630, 2631, 2632,
      2633, 2636, 2637, 2642, 2643, 2652, 2658, 2660, 2661, 2662, 2663, 2664, 2667, 2668, 2669, 2670, 2673, 2674, 2676,
      2677, 2678, 2683, 2684, 2701, 2702, 2703, 2705, 2706, 2710, 2711, 2712, 2713, 2714, 2715, 2716, 2717, 2718, 2719,
      2720, 2721, 2722, 2723, 2724, 2726, 2727, 2728, 2729, 2730, 2732, 2733, 2734, 2735, 2737, 2738, 2739, 2740, 2741,
      2742, 2745, 2747, 2749, 2751, 2754, 2756, 2757, 2759, 2760, 2761, 2762, 2764, 2765, 2766, 2767, 2768, 2769, 2770,
      2774, 2775, 2777, 2779, 2780, 2781, 2782, 2785, 2799, 2804, 2805, 2809, 2810, 2811, 2812, 2813, 2814, 2815, 2816,
      2818, 2819, 2822, 2823, 2824, 2827, 2828, 2829, 2831, 2832, 2841, 2842, 2843, 2844, 2845, 2846, 2847, 2850, 2851,
      2852, 2853, 2854, 2855, 2858, 2859, 2861, 2862, 2864, 2867, 2870, 2871, 2872, 2873, 2874, 2885, 2983, 3055, 3073,
      3080, 3087, 3090, 3091, 3094, 3097, 3112, 3113, 3115, 3117, 3118, 3119, 3120, 3121, 3122, 3125, 3126, 3129, 3130,
      3131, 3132, 3133, 3134, 3137, 3139, 3140, 3141, 3142, 3143, 3144, 3145, 3148, 3149, 3152, 3154, 3188, 3189, 3379,
      3381, 3423, 3424, 3427, 3428, 3429, 3510, 3514, 3811, 3821, 5303, 5304, 5305, 5306, 5307, 5308, 5309, 5310, 5311,
      5312, 5313, 5314, 5315, 5316, 5317, 5318, 5319, 5320, 5321, 5322, 5323, 5324, 5325, 5326, 5327, 5328, 5329, 5330,
      5331, 5332, 5333, 5459, 5460, 5462, 5492, 5499, 5500, 5508, 5512, 5516, 5518, 5520, 5522, 5526, 5528, 5534, 5536,
      5541, 5543, 5544, 5547, 5550, 5612, 5614, 5618, 5619, 5625, 5626, 5627, 5628, 5629, 5638, 5639, 5640, 5644, 5671,
      5677, 5678, 5682, 5686, 5688, 5695, 5766, 5767, 5770, 5771, 5772, 5775, 5780, 5781, 5786, 5787, 5788, 5789, 5809,
      5810, 5811, 5812, 5813, 5815, 5817, 5831, 5832, 5833, 5834, 5835, 5836, 5837, 5838, 5842, 5874, 5875, 5876, 5880,
      5881, 5882, 5883, 5884, 5885, 5886, 5887, 5890, 5905, 5910, 5913, 5916, 5917, 5922, 5923, 5924, 5925, 5926, 5935,
      5936, 5937, 5938, 5939, 5940, 5941, 5942, 5960, 5966, 6120, 6132, 6311, 6321, 6511, 6570, 6967, 7372, 7468, 10873);

  public function customInit()
  {
    parent::customInit();
    $this->extensionFilter = 'xml';
  }

  public function getRawArticles(&$fileContents)
  { //get raw articles
    $this->addLog("getting raw articles text");
    return $this->getElementsByName('topic', $fileContents);
  }

  protected function getIptcId(&$text)
  {
    //get category name from file name
    $this->addLog("getting category name");
    $category = $this->getElementByName('category', $text);
    if ($category) {
      return $this->model->getIptcCategoryId($category);
    }
    return parent::getIptcId($text);
  }

  protected function getLangId(&$text)
  {
    //get language
    $this->addLog("getting article language id");
    if ($this->defaultLang) {
      return $this->defaultLang;
    }
    $langCode = strtolower(substr($this->getElementByName('language', $text), 0, 2));
    $lang = $this->model->getLanguageId($langCode);
    if ($lang) {
      return $lang;
    }
    return parent::getLangId($text);
  }

  protected function getArticleDate(&$text) {
    //get article date
    $this->addLog("getting article date");
    $date = $this->getElementByName('date', $text);
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return $this->textFixation($this->getElementByName('title', $text));
  }

  protected function getAbstract(&$text) {
    //get article summary
    $this->addLog("getting article summary");
    return $this->textFixation($this->getCData($this->getElementByName('summary', $text)));
  }

  protected function getStory(&$text) {
    $this->addLog("getting article body");
    $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
    $this->story = preg_replace('!\s+!', ' ', $this->story);
    if (empty($this->story))
      return $this->textFixation($this->getElementByName('fulltext', $text));
    else {
      return $this->story;
    }
  }

  protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getElementByName('author', $text));
  }

  public function textFixation($text) {
    $text = parent::textFixation($text);
    return html_entity_decode($text);
  }

  public function getArticleOriginalId($params = array()) {
    $defaultArray = array('text' => '', 'headline' => '', 'articleDate' => '', 'issueNum' => 0);
    $params = array_merge($defaultArray, $params);
    if (!$articleOriginalId = trim($this->getElementByName('id', $params['text']))) {
      return parent::getArticleOriginalId($params);
    }
    return sha1($articleOriginalId . trim($params['headline']));
  }

  public function getOriginalCategory(&$text) {
    $this->addLog("getting original category");
    $cat = $this->textFixation($this->getElementByName('category', $text));
    if (!$cat) {
      return parent::getOriginalCategory($text);
    }
    return $cat;
  }

  protected function getVideos(&$text) {
    $this->addLog("Getting videos");
    $videosItem = array();
    $videos = array();
    $videosArray = array();
    $videos = $this->textFixation($this->getElementByName('images', $text));
    $videos = str_replace('<img>', '<image>', $videos);
    $videos = str_replace('</img>', '</image>', $videos);
    $videos = $this->getElementsByName('image', $videos);
    $date = $this->getElementByName('date', $text);
    $date = date('Y-m-d h:i:s', strtotime($date));
    foreach ($videos as $imgName) {
      $video = array();
      $this->addLog("getting video");
      $videoDescription = $this->textFixation($this->getElementByName('img_description', $imgName));
      $video_Name = trim($this->textFixation($this->getElementByName('img_path', $imgName)));
      $mimeType = "";
      $path = pathinfo($video_Name);
      $extension = strtolower($path["extension"]);
      $videoExtension = array("mp4", "mov", "flv","mp3","m4v");
      if (!in_array($extension, $videoExtension)) {
        continue;
      }
      if (preg_match("/google.com\/maps/", $video_Name)) {
        continue;
      }
      $video['video_name'] = $video_Name;
      $video['original_name'] = $video_Name;
      $video['video_caption'] = $videoDescription;
      $video['mime_type'] = $mimeType;
      $video['added_time'] = $date;
      $videosItem[] = $video;
    }
    return $videosItem;
  }

  protected function getImages(&$text) {
    $title_id = $this->title->getId();
    $imagesArray = array();
    $images = $this->textFixation($this->getElementByName('images', $text));
    $images = str_replace('<img>', '<image>', $images);
    $images = str_replace('</img>', '</image>', $images);
    $images = $this->getElementsByName('image', $images);
    foreach ($images as $imgName) {
      $img = array();
      $this->addLog("getting image");
      $imageDescription = $this->textFixation($this->getElementByName('img_description', $imgName));
      $image_Name = trim($this->textFixation($this->getElementByName('img_path', $imgName)));
      $path = pathinfo($image_Name);
      $extension = strtolower($path["extension"]);
      if (in_array($title_id, $this->photoSyndicationRights)) {
        $imageExtension = array("pdf", "zip");
      } else {
        $imageExtension = array("gif", "png", "jpeg", "jpg", "pdf", "zip");
      }

      if (!in_array($extension, $imageExtension)) {
        continue;
      }
      $img['img_name'] = $image_Name;
      $img['original_name'] = $image_Name;
      $img['image_caption'] = $imageDescription;
      $img['is_headline'] = FALSE;
      $img['image_original_key'] = sha1($image_Name);
      array_push($imagesArray, $img);
    }
    $this->story = $this->getCData($this->getElementByName('fulltext', $text));
    $this->story = str_replace("&amp;", "&", $this->story);
    $this->story = $this->textFixation($this->story);
    if (in_array($title_id, $this->photoSyndicationRights)) {
     return  $imagesArray ;
    }
    preg_match_all("/<img[^>]+>/i", $this->story, $imgs);
    foreach ($imgs[0] as $img) {
      $this->addLog("getting article images");
      $image_caption = '';
      $image_caption = syndParseHelper::getImgElements($img, 'img', 'alt');
      if (!empty($image_caption[0])) {
        $image_caption = $image_caption[0];
      } else {
        $image_caption = syndParseHelper::getImgElements($img, 'img', 'title');
        if (!empty($image_caption[0])) {
          $image_caption = $image_caption[0];
        } else {
          $image_caption = "";
        }
      }
      $imageInfo = syndParseHelper::getImgElements($img, 'img');
      $imagePathold = $imageInfo[0];
      $imagePath = preg_replace("/\?(.*)/is", "", $imagePathold);
      if (!$imagePath) {
        continue;
      }
      if ($this->checkImageifCached($imagePath)) {
        // Image already parsed..
        continue;
      }
      $imagePath = str_replace(' ', '%20', $imagePath);
      $copiedImage = $this->copyUrlImgIfNotCached($imagePath);
      if (!$copiedImage) {
        $this->addLog("No Path");
        continue;
      }
      $images = $this->getAndCopyImagesFromArray(array($copiedImage));
      if (!empty($image_caption)) {
        $images[0]['image_caption'] = $image_caption;
      }
      $name_image = explode('/images/', $copiedImage);
      if ($images[0]['image_caption'] == $name_image[1]) {
        $images[0]['image_caption'] = '';
      }
      $images[0]['image_caption'] = str_replace('%20', ' ', $images[0]['image_caption']);
      $images[0]['is_headline'] = false;
      $new_img = str_replace($imagePathold, $images[0]['img_name'], $img);
      $this->story = str_replace($img, $new_img, $this->story);
      array_push($imagesArray, $images[0]);
    }
    return $imagesArray;
  }

  public function copyUrlImgIfNotCached($imageUrl) {
    if (preg_match("/.pdf/", strtolower($imageUrl))) {
      $baseName = sha1($imageUrl) . ".pdf";
    } else {
      $baseName = sha1($imageUrl) . ".jpeg";
    }
    $copiedImage = $this->imgCacheDir . $baseName;

    if (!is_dir($this->imgCacheDir)) {
      mkdir($this->imgCacheDir, 0755, true);
    }

    if (!file_exists($copiedImage)) {
      $options = array(
          CURLOPT_RETURNTRANSFER => true, // return web page
          CURLOPT_HEADER => false, // do not return headers
          CURLOPT_FOLLOWLOCATION => true, // follow redirects
          CURLOPT_USERAGENT => "spider", // who am i
          CURLOPT_AUTOREFERER => true, // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
          CURLOPT_TIMEOUT => 120, // timeout on response
          CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
      );

      $ch = curl_init($imageUrl);
      curl_setopt_array($ch, $options);
      $imgContent = curl_exec($ch);
      curl_close($ch);
      $myfile = fopen($copiedImage, "w");
      fwrite($myfile, $imgContent);
      fclose($myfile);
      if (!empty($imgContent)) {
        return $copiedImage;
      } else {
        return false;
      }
    } else {
      return $copiedImage;
    }
  }

  public function getArticleReference(&$text) {
    $this->addLog('Getting article Link');
    $link = $this->getElementByName('link', $text);
    if (empty($link)) {
      return '';
    }
    return $link;
  }

}

?>
