<?php

////////////////////////////////////////////////////////////////////////
// Publisher : Fusion Media Limited 
// Publisher id : 304
////////////////////////////////////////////////////////////////////////

class syndParser_FusionMedia extends syndParseRss {

  public function customInit() {
    parent::customInit();
    $this->extensionFilter = 'xml';
    $this->defaultLang = $this->title->getLanguageId();
  }

  public function getRawArticles(&$fileContents) {

    //get raw articles
    $this->addLog("getting raw articles text");
    $rawArticles = $this->getElementsByName('Item', $fileContents);
    return $rawArticles;
  }

  protected function getOriginalCategory(&$text) {
    //get article text
    $this->addLog("getting article original category");
    return $this->textFixation($this->getCData($this->getElementByName('Category', $text)));
  }

  protected function getStory(&$text) {
      $this->addLog("getting article text");
      $story = $this->textFixation($this->getCData($this->getElementByName('body', $text)));
      $story = str_replace(']]>', '', $story);
      //This action depends on "Task #1064 Remove all the images from our clients feeds"
      $story = preg_replace('/<img[^>]+\>/i', '', $story);
      return $story;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return $this->textFixation($this->getCData($this->getElementByName('title', $text)));
  }

  protected function getArticleDate(&$text) {
    $this->addLog("getting article date");
    $date = $this->getElementByName('NewsDate', $text);
    if ($date) {
      return $this->dateFormater($date);
    }
    return parent::getArticleDate($text);
  }

  public function getArticleOriginalId($params = array()) {
    $defaultArray = array('text' => '', 'headline' => '', 'articleDate' => '', 'issueNum' => 0);
    $params = array_merge($defaultArray, $params);

      $articleOriginalId = trim($this->getElementByName('NewsID', $params['text']));
      $articleOriginalId = trim($this->getCData($articleOriginalId));

      if (!$articleOriginalId) {
          return parent::getArticleOriginalId($params);
      } else {
          return $this->title->getId() . "_" . sha1($articleOriginalId) . "_" . $articleOriginalId;
      }
  }

    public function getArticleReference(&$text)
    {
        $this->addLog("getting article reference");
        $link = syndParseHelper::getImgElements($text, 'link', 'href');
        if (isset($link[0])) {
            $link = $link[0];
        } else {
            $link = "";
        }
        return $link;
    }

    protected function getImages(&$text)
    {
        //This action depends on "Task #1064 Remove all the images from our clients feeds"
        return array();
    }
}
