<?php

	abstract class syndParseXmlContent extends syndParseContent
	{
		protected $hasCdata;

		/**
		 * convert SimpleXml Object to tree
		 *
		 * @param String $filePath xml file location
		 * @return Array
		 */
        public function xmlToTree($filePath)
		{
			if(is_file($filePath)) {
				$xml = simplexml_load_file($filePath);
				return $this->object2array($xml);
			}
		}

		/**
		 * get all xml elements contents
		 *
		 * @param String $name element name
		 * @param String $text all xml text
		 * @return array of elements contents
		 */
		public function getElementsByName($name, $text)
		{
			$openTagRegExp = '<'.$name.'([^\>]|[\s])*>';
			$closeTagRegExp = '<\/'.$name.'[\s]*>';
			$elements  = preg_split("/$openTagRegExp/i", $text);

			$elementsContents = array();
			$elementsCount = count($elements);
			for($i=1; $i<$elementsCount; $i++) {
				$element = preg_split("/$closeTagRegExp/i", $elements[$i]);
				$elementsContents[] = $element[0];
			}
			return $elementsContents;
		}

		/**
         * get first xml element contents
         *
         * @param String $name element name
         * @param String $text all xml text
		 * @return string
         */
		public function getElementByName($name, $text)
		{
			$element = $this->getElementsByName($name, $text);
			return (isset($element[0]) ? $element[0] : null);
		}

		protected function getElementByNameRecursivly($name, $text) {
			$names   = explode('/', $name);
			$subText = $text;
			
			
			echo $this->getElementByName('NewsItem', $text); die();	
			for($counter=0; $counter<count($names)-1; $counter++) {
				
				$tmp = $this->getElementsByName($names[$counter], $subText);
				print_r($tmp); die();
				echo $subText; die();
			}
			die();
			return $this->getElementByName($names[count($names)-1], $subText);
		}
		
		protected function getElementsByNameRecursivly($name, $text) {
			
			$names   = explode('/', $name);
			$subText = $text;
						
			for($counter=0; $counter<count($names)-1; $counter++) {
				$subText = $this->getElementByName($names[$counter], $subText);
			}
			return $this->getElementsByName($names[count($names)-1], $subText);
		}
		
		
		private function object2array($object)
		{
			$return = NULL;
			if(is_array($object)) {
				foreach($object as $key => $value){
					$return[$key] = $this->object2array($value);
				}
			} else {
				$var = get_object_vars($object);
				if($var) {
					foreach($var as $key => $value) {
						$return[$key] = ($key && !$value) ? NULL : $this->object2array($value);
					}
				} else {
					return $object;
				}
			}
			return $return;
		}
		public function getCData($text)
		{
                        ini_set("pcre.recursion_limit", "10000");
			$regExp = '<!\[CDATA\[((.*|\s*)*)\]\]>';
			if(preg_match("/$regExp/i", $text, $matches)) {
				$this->hasCdata = true;
				echo "has cData \n";
				return $matches[1];
			} else {
				return $text;
			}
		}

		public function textFixation($text)
		{
			$text = parent::textFixation($text);
			if($this->hasCdata) {
				return $text;
			} else {
				return html_entity_decode($this->getCData($text),  ENT_QUOTES, 'UTF-8');
			}
			
		}
		
		public function replaceAmp($text) 
		{
			$text = str_replace("&amp;", "&", $text);
			return $text;
		}
		
		public function replaceEntity($text, $symbols = array())
		{
			if(!is_array($symbols) || !count($symbols)) {
				$symbols = array("&#34;" => "\"", 
								 "&#38;" => "&",
								 "&#39;" => "'",
								 "&#60;" => "<",
								 "&#62;" => ">",
								 "&#63"  =>" ",
								 "&#174" => "®",
								 "&#180;" => "´",
								 "&#8216;" => "‘",
								 "&#8217;" => "’",
								 "&#8218;" => "‚",
								 "&#8220;" => "“",
								 "&#8221;" => "”",
								 "&#8222;" => "„"
							);
			}
			
			foreach ($symbols as $symbol => $char) {
				$text = str_replace($symbol, $char, $text);
			}
			return $text;
		}
		
	}
?>
