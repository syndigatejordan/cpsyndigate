<?php

/**
 * Description of syndParseAlManhal
 *
 * @author eyad
 */
class syndParseAlManhal extends syndParseXmlContent {

  public $languageTitle;

  public function customInit() {
    parent::customInit();
    $languageId = $this->title->getLanguageId();
    switch ($languageId) {
      case 1:
        $this->languageTitle = "Arabic";
        $this->defaultLang = $this->model->getLanguageId('ar');
        break;
      case 2:
        $this->languageTitle = "English";
        $this->defaultLang = $this->model->getLanguageId('en');
        break;
      case 4:
        $this->languageTitle = "French";
        $this->defaultLang = $this->model->getLanguageId('fr');
        break;
    }
    $this->extensionFilter = 'xml';
  }

  Public function getRawArticles(&$fileContents) {
    //get articles
    $this->addLog("getting articles raw text");
    $articles = $this->getElementsByName('item', $fileContents);
    return $articles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    return trim($this->textFixation($this->getElementByName('Title', $text)));
  }

  protected function getArticleDate(&$text) {

    $this->addLog("getting articles date");
    $date = $this->textFixation($this->getCData($this->getElementByName('Date', $text)));
    $date = trim(preg_replace('!\s+!', ' ', $date));
    if (preg_match("/GMT/", $date)) {
      $date = trim(preg_replace("/GMT(.*)/is", "", $date));
      $date = date("Y-m-d", strtotime($date));
    } else {
      $date = explode('-', $date);
      $date = $date[0] . "-" . $date[1] . "-" . $date[2];
    }
    return $date;
  }

  /*
    protected function getAuthor(&$text) {
    $this->addLog("getting article author");
    return $this->textFixation($this->getElementByName('Author_Authors', $text));
    }
   */

  protected function getStory(&$text) {
    $this->addLog("getting article text");
    $story = trim($this->textFixation($this->getElementByName('Body', $text)));
    $story = "<p>" . str_replace("/#/#/", "</p><p>", $story) . "</p>";
    return $story;
  }

  /*
    protected function getOriginalCategory(&$text) {
    $industry = array();
    $industry[0] = trim($this->textFixation($this->getCData($this->getElementByName('Document_Type', $text))));
    $industry[1] = trim($this->textFixation($this->getCData($this->getElementByName('Main_Topics', $text))));
    $industry = implode(" ", $industry);
    return $industry;
    }
   */

  protected function getImages(&$text) {
    $this->addLog("getting image");
    $titleId = $this->title->getId();
    $imagesArray = array();
    $image_Name = trim($this->textFixation($this->getElementByName('MLT', $text))) . ".pdf";
    $image_full_path = $this->currentDir . $image_Name;
    $original_name = explode('.', $image_Name);
    if (file_exists($image_full_path)) {
      $name = $this->model->getImgReplacement($image_Name, $this->currentDir, $titleId);
      if ($this->languageTitle == "French") {

        $img['img_name'] = str_replace(IMGS_PATH, IMGS_HOST, $name);
      } else {

        $img['img_name'] = str_replace(IMGS_PATH, "https://s3.amazonaws.com/syndigateimages/syndigate/imgs/", $name);
      }
      $img['original_name'] = $image_Name;
      $img['image_caption'] = $image_Name;
      $img['is_headline'] = FALSE;
      array_push($imagesArray, $img);
    }
    $image_Name = trim($this->textFixation($this->getElementByName('MLT', $text))) . ".jpg";

    $image_full_path = $this->currentDir . $image_Name;
    if (!file_exists($image_full_path)) {
      $image_full_path = $this->currentDir . "default.jpg";
    }
    $original_name = explode('.', $image_Name);
    if (file_exists($image_full_path)) {
      $name = $this->model->getImgReplacement($image_Name, $this->currentDir, $titleId);
      $img['img_name'] = str_replace(IMGS_PATH, IMGS_HOST, $name);
      $img['original_name'] = $image_Name;
      $img['image_caption'] = $image_Name;
      $img['is_headline'] = FALSE;
      array_push($imagesArray, $img);
    }
    return $imagesArray;
  }

  public function getArticleOriginalId($params = array()) {
    $articleOriginalId = trim($this->getElementByName('ORG_No', $params['text']));
    $this->articleOriginalId = $articleOriginalId;
    if (!$articleOriginalId) {
      return parent::getArticleOriginalId($params);
    }
    return $this->title->getId() . "_" . sha1($articleOriginalId) . "_" . $articleOriginalId;
  }

}
