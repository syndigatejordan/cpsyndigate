<?php
	abstract class syndParseParser
	{
		////////////////////////////////////////////////////////////////////
		// Main abstract functions
		// All prsers must have these functions
		protected abstract function getRawArticles(&$fileContents);
		protected abstract function getStory(&$text);
		protected abstract function getHeadline(&$text);
		protected abstract function getArticleDate(&$text);

		////////////////////////////////////////////////////////////////////
		// Overwritten functions
		public abstract function beforeParse($params = array());
		public abstract function afterParse($params = array());
		protected abstract function getArticle(&$text);
		protected abstract function getImages(&$text);
		protected abstract function getOriginalData(&$text);
		protected abstract function getIptcId(&$text);
		protected abstract function getLangId(&$text);
		protected abstract function getAbstract(&$text);
		protected abstract function getAuthor(&$text);

		public abstract function getArticleOriginalId($params = array());

		public abstract function parse();
		protected abstract function getHasTime();
	}
?>