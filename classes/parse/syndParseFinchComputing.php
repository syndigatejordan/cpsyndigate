<?php


class syndParseFinchComputing extends syndParseXmlContent
{

    public $languageTitle;
    public $story;
    public function customInit()
    {
        parent::customInit();
        $languageId = $this->title->getLanguageId();//
        $language = new Criteria();
        $language->add(LanguagePeer::ID, $languageId);
        $currentLanguage = LanguagePeer::doSelectOne($language);

        $this->languageTitle = $currentLanguage->getName();
        $this->defaultLang = $this->model->getLanguageId($currentLanguage->getCode());
        $this->extensionFilter = 'xml';
    }

    public function getRawArticles(&$fileContents)
    { //get raw articles
        $this->addLog("getting raw articles text");
        return $this->getElementsByName('topic', $fileContents);
    }

    protected function getIptcId(&$text)
    {
        //get category name from file name
        $this->addLog("getting category name");
        $category = $this->getElementByName('category', $text);
        if ($category) {
            return $this->model->getIptcCategoryId($category);
        }
        return parent::getIptcId($text);
    }

    protected function getLangId(&$text)
    {
        //get language
        $this->addLog("getting article language id");
        if ($this->defaultLang) {
            return $this->defaultLang;
        }
        $langCode = strtolower(substr($this->getElementByName('language', $text), 0, 2));
        $lang = $this->model->getLanguageId($langCode);
        if ($lang) {
            return $lang;
        }
        return parent::getLangId($text);
    }

    protected function getArticleDate(&$text)
    {
        //get article date
        $this->addLog("getting article date");
        $date = $this->getElementByName('date', $text);
        if ($date) {
            return $this->dateFormater($date);
        }
        return parent::getArticleDate($text);
    }

    protected function getHeadline(&$text)
    {
        $this->addLog("getting article headline");
        return $this->textFixation($this->getElementByName('title', $text));
    }

    protected function getAbstract(&$text)
    {
        //get article summary
        $this->addLog("getting article summary");
        return $this->textFixation($this->getCData($this->getElementByName('summary', $text)));
    }

    protected function getStory(&$text)
    {
        $this->addLog("getting article body");
        $this->story = $this->getCData($this->getElementByName('fulltext', $text));
        $this->story = str_replace("&amp;", "&", $this->story);
        $this->story = $this->textFixation($this->story);
        $this->story = preg_replace('/<img[^>]+\>/i', '', $this->story);
        $this->story = preg_replace('!\s+!', ' ', $this->story);
        if (empty($this->story))
            return $this->textFixation($this->getElementByName('fulltext', $text));
        else {
            return $this->story;
        }
    }

    protected function getAuthor(&$text)
    {
        $this->addLog("getting article author");
        return $this->textFixation($this->getElementByName('author', $text));
    }

    public function textFixation($text)
    {
        $text = parent::textFixation($text);
        return html_entity_decode($text);
    }

    public function getArticleOriginalId($params = array())
    {
        $defaultArray = array('text' => '', 'headline' => '', 'articleDate' => '', 'issueNum' => 0);
        $params = array_merge($defaultArray, $params);
        if (!$articleOriginalId = trim($this->getElementByName('id', $params['text']))) {
            return parent::getArticleOriginalId($params);
        }
        return sha1($articleOriginalId . trim($params['headline']));
    }

    public function getOriginalCategory(&$text)
    {
        return "";
        $this->addLog("getting original category");
        $cat = $this->textFixation($this->getElementByName('category', $text));
        if (!$cat) {
            return parent::getOriginalCategory($text);
        }
        return $cat;
    }

    public function getArticleReference(&$text)
    {
        $this->addLog('Getting article Link');
        $link = $this->getElementByName('link', $text);
        if (empty($link)) {
            return '';
        }
        return $link;
    }

    protected function getImages(&$text)
    {
        $imagesArray = array();
        return $imagesArray;
    }
}