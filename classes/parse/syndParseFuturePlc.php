<?php

/**
 * Description of syndParseFuturePlc
 *
 * @author eyad
 */
class syndParseFuturePlc extends syndParseXmlContent {

  public $languageTitle;

  public function customInit() {
    parent::customInit();
    $languageId = $this->title->getLanguageId();
    switch ($languageId) {
      case 1:
        $this->languageTitle = "Arabic";
        $this->defaultLang = $this->model->getLanguageId('ar');
        break;
      case 2:
        $this->languageTitle = "English";
        $this->defaultLang = $this->model->getLanguageId('en');
        break;
      case 4:
        $this->languageTitle = "French";
        $this->defaultLang = $this->model->getLanguageId('fr');
        break;
    }
    $this->extensionFilter = 'pdf';
  }

  public function getRawArticles(&$fileContents) {
    $this->addLog("getting raw articles text");
    if (!preg_match("/pdf/", $this->currentlyParsedFile)) {
      return array();
    }
    $rawArticles = array("file" => $this->currentlyParsedFile);
    return $rawArticles;
  }

  protected function getHeadline(&$text) {
    $this->addLog("getting article headline");
    $headline = $this->title->getName();
    return $headline;
  }

  //Return empty body for the article by Mariam request
  protected function getStory(&$text) {
    $this->addLog("getting article body");
    return "";
  }

  protected function getImages(&$text) {
    $this->addLog("getting image");
    $images = array();
    $titleId = $this->title->getId();
    foreach ($this->files as $image_Name) {
      $image_Name = basename($image_Name);
      $image_full_path = $this->currentDir . $image_Name;
      $original_name = pathinfo($image_Name);
      if (file_exists($image_full_path)) {
        //the video name is same as the image name here  		
        $name = $this->model->getImgReplacement($image_Name, $this->currentDir, $titleId);
        $img['img_name'] = str_replace(IMGS_PATH, IMGS_HOST, $name);
        $img['original_name'] = basename($image_Name);
        $img['image_caption'] = $original_name["filename"];
        $img['is_headline'] = false;
        $img['image_original_key'] = sha1($image_Name);
        $images[] = $img;
      }
    }

    return $images;
  }

  //Handle empty body
  public function parse() {
    $articles = array();
    foreach ($this->files as $file) {
      if ($this->extensionFilter) {
        if (!syndParseHelper::isFileExtension($file, $this->extensionFilter)) {
          $this->addLog("Wrong extension for file : $file)");
          continue;
        }
      }
      if (!file_exists($file)) {
        $this->addLog("file dose not exist: $file)");
        continue;
      }

      $this->addLog("get file contents (file:$file)");
      $fileContents = $this->getFileContents($file);

      if (!$fileContents) {
        continue;
      }

      $this->currentlyParsedFile = $file;
      $this->loadCurrentDirectory();
      $this->loadUpperDir();
      $rawArticles = $this->getRawArticles($fileContents);
      foreach ($rawArticles as $rawArticle) {
        $article = $this->getArticle($rawArticle);
        $articles[] = $article;
      }
    }
    return $articles;
  }

}
