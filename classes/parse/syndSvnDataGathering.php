<?php
	class syndSvnDataGathering
	{
		private $titleId;
		private $workingCopyPath;
		private $svnRepoUrl;

		/**
		 * class constructor
		 * 
		 * @param int $titleId Title id
		 */
		public function __construct($titleId)
		{
			$model = syndModel::getModelInstance();
			$this->titleId = (int)$titleId;
			$this->workingCopyPath = $model->getParseWorkDir($titleId);
			$titleObj = $model->getTitleById($this->titleId);
			$this->svnRepoUrl = sprintf('svn://localhost/%d/%d', $titleObj->getPublisherId(), $this->titleId);
		}

		/**
		 * perform svn update
		 */
		public function update($revision)
		{
			$updateCmd = 'svn update ';
			if($revision) {
				$updateCmd .= "-r$revision ";
			}
			$updateCmd .= $this->workingCopyPath;

			$log = ezcLog::getInstance();
            $log->log("Parse svn up [{$updateCmd}]", ezcLog::DEBUG, array('source' => 'parse') );
			exec($updateCmd, $output);
			
			// get update and add operations
			$addUpdateOp = array();
			foreach($output as $line) {
			    if(preg_match('/^([au])[\s]+(.*)$/i', $line, $matches)) {
			        $operation = isset($matches[1]) ? strtoupper($matches[1]) : '';
			        $filePath  = isset($matches[2]) ? $matches[2] : '';
			        if($operation && $filePath && is_file($filePath)) {
			            $addUpdateOp[] = $filePath;
			        }
			    }
			}
			return $addUpdateOp;
		}
		

		/**
		 * Get Revision log for a revision
		 *
		 * @param Integer $revision
		 * @return array
		 */
		public function getRevLog($revision) {
			
			if(! $revision || !is_int($revision)) {
                return array();
            }

            $updateCmd =  "svn log -r $revision -v " . $this->workingCopyPath;

            $log = ezcLog::getInstance();
            $log->log("Getting SVN log ( Re processing ) for revision $revision [{$updateCmd}]", ezcLog::DEBUG, array('source' => 'parse') );
            exec($updateCmd, $output);
            
            // get update and add operations
            $addUpdateOp = array();
            foreach($output as $line) {
            	
                if(preg_match('/^([aum])[\s]+(.*)$/i', trim($line), $matches)) {

                    $operation = isset($matches[1]) ? strtoupper($matches[1]) : '';
                    $filePath  = isset($matches[2]) ? $matches[2] : '';
					if($operation && $filePath) {
                    	if(is_file($filePath)) {
                    		$addUpdateOp[] = $filePath;
                    	} elseif (is_file($this->workingCopyPath . $filePath)) {
                    		$addUpdateOp[] = $this->workingCopyPath . $filePath;
                    	}
                    }
                }
            }
            return $addUpdateOp;
        }


		
			/**
		 * Returns the last revision in the SVN repo
		 *
		 * @return int Last revision number
		 */
		public function getLastRevision() {
			
			$lastRevision = 0;
			
			$command = "svn info $this->svnRepoUrl";
			exec($command, $output);
			foreach ($output as $line) {
				if (false !== strstr($line, 'Last Changed Rev:')) {
					if (preg_match('/\d+/', $line, $matches)) {						
						$lastRevision = $matches[0];
						break;						
					}
				}

			}
			
			return $lastRevision;			
			
		}
	}
?>
