<?php

require_once dirname(__FILE__) . '/../ez_autoload.php';

////////////////////////////////////////////////////////////////////////////////////////
// configuraion
///////////////////////////////////////////////////////////////////////////////////////
defined('COPYRIGHT') OR define('COPYRIGHT', " Provided by SyndiGate Media Inc. (<a href='http://syndigate.info'>Syndigate.info</a>).");
defined('STRIPPING_TAGS') OR define('STRIPPING_TAGS', false);
defined('TMP_DIR') OR define('TMP_DIR', APP_PATH . 'tmp/');

class syndGenerateRevMicrosoft {

  static public $DB_NAME;
  static public $DB_USER;
  static public $DB_PASS;
  static public $DB_HOST;
  private $model;
  private $tmpFiles;
  private $timeStamp, $version;
  public $clientsIds = array(), $force_resend = 0;

  public function __construct($timeStamp = '', $reportId = 0) {
    $this->model = syndModel::getModelInstance();

    $propelConf = Propel::getConfiguration();
    $dbConf = $propelConf['datasources']['propel']['connection'];

    self::$DB_USER = $dbConf['username'];
    self::$DB_PASS = $dbConf['password'];
    self::$DB_HOST = $dbConf['hostspec'];
    self::$DB_NAME = $dbConf['database'];

    if ($timeStamp) {
      $this->timeStamp = $timeStamp;
    } else {
      $this->timeStamp = time();
    }

    $this->version = date('dhis');
    $this->clientsIds = array(85, 95);
  }

  public function generateAll($titleId) {

    $this->titleId = $titleId;
    $conn = mysql_connect(self::$DB_HOST, self::$DB_USER, self::$DB_PASS, true) or die("\n\nCannot connect to database\n\n");
    mysql_select_db(self::$DB_NAME) or die("\n\nDatabase doesn't exists\n\n");

    $c = new Criteria();
    $c->add(TitlePeer::ID, $this->titleId);
    $titleObj = TitlePeer::doSelectOne($c);
    $titles[$titleObj->getId()]['id'] = $titleObj->getId();
    $titles[$titleObj->getId()]['providerName'] = $titleObj->getName();
    $titles[$titleObj->getId()]['copyright'] = $titleObj->getCopyRight();
    $titles[$titleObj->getId()]['webSite'] = $titleObj->getWebsite();

    $date = date('Y-m-d', $this->timeStamp);
    $minDate = date('Y-m-d H:i', strtotime("$date - 6 hours"));
    $maxDate = date('Y-m-d H:i', strtotime($date . ' 23:59'));  // get client article date filter
    $articlesFilters[] = ArticlePeer::PARSED_AT . " >= '$minDate'";
    $articlesFilters[] = ArticlePeer::PARSED_AT . " <= '$maxDate'";
    // get title's articles
    $sql = 'SELECT article.*, original_article_category.cat_name, article_original_data.reference '
            . 'FROM article left join article_original_data on article_original_data.id = article.article_original_data_id '
            . 'left join original_article_category on article_original_data.original_cat_id = original_article_category.id '
            . 'WHERE ' . ArticlePeer::TITLE_ID . ' = ' . $titleObj->getId() . ' and ((article.PARSED_AT >= "' . $minDate . '"'
            . 'AND article.PARSED_AT <= "' . $maxDate . '"))';
    $articlesResult = mysql_query($sql, $conn);
    $titles[$titleObj->getId()]['articles'] = new syndGenerateMySqlIterator($articlesResult, new ArticleArrayFormaterMicrosoft($this->model));
    $article_count = (int) $titles[$titleObj->getId()]['articles']->getCount();
    $mic_template = syndGenerateTemplatesManager::factory('NewsMlMicrosoft');

    $microsoft_file = $mic_template->generate($titles);

    $this->tmpFiles['microsoft_all'] = $microsoft_file;
    $this->tmpFiles['article_count'] = $article_count;
    chmod($microsoft_file, 0775);
    $this->copyFilesToClientsDir();
    $this->clearTmp();
  }

  private function clearTmp() {

    if (is_array($this->tmpFiles)) {
      foreach ($this->tmpFiles as $file) {
        unlink($file);
      }
    }
  }

  /**
   * Prepares the dir to write files to, and returns the full path
   *
   * @param string $clientHomeDir
   */
  private function prepareDir($clientHomeDir) {

    if ($this->timeStamp) {
      $ftpDir = rtrim($clientHomeDir, '/') . '/' . date('Y/m/d/', $this->timeStamp);
    } else {
      $ftpDir = rtrim($clientHomeDir, '/') . '/' . date('Y/m/d/');
    }

    if (!is_dir($ftpDir)) {
      $rez = mkdir($ftpDir, 0755, true);
    }
    return $ftpDir;
  }

  /**
   * Coping the tmp files to clients directories
   *
   * @return true if every thing goes right false otherwise
   */
  protected function copyFilesToClientsDir() {
    // $this->clientsIds = array(85, 95); // FOR TESTING SHOULD REMOVED.
    $c = new Criteria();
    $c->add(TitlesClientsRulesPeer::TITLE_ID, $this->titleId);
    $c->add(TitlesClientsRulesPeer::CLIENT_ID, $this->clientsIds, Criteria::IN);
    $clients = TitlesClientsRulesPeer::doSelect($c);
    $hasErrors = 0;

    foreach ($clients as $clientRule) {
      $c = new Criteria();
      $client = ClientPeer::retrieveByPK($clientRule->getClientId());
      if (!$client->getIsActive()) {
        continue;
      }
      $ftpDir = $this->prepareDir($client->getHomeDir());

      $fromFile = $this->tmpFiles['microsoft_all'];
      $targetFile = $ftpDir . $this->titleId . '.xml';
      if ((int) $this->tmpFiles['article_count'] == 0) {
        continue;
      }
      $clientPullFile = explode($client->getHomeDir(), $targetFile);
      $clientPullFile = $clientPullFile[1];
      $rez = copy($fromFile, $targetFile);
      chown($targetFile, $client->getFtpUsername());
      chgrp($targetFile, $client->getFtpUsername());
      chmod($targetFile, 0777);
      $afterGenerateFile = dirname(__FILE__) . '/generate/customClient/syndGenerate_' . $client->getId() . '.php';
      if (file_exists($afterGenerateFile)) {
        require_once $afterGenerateFile;
        $params = array("ftpDir" => $ftpDir, "generateType" => $client->getGenerateType());
        $className = "syndGenerate_" . $client->getId();
        $obj = new $className($params);
        $obj->afterGenerate();
      }
    }

    return $hasErrors ? 0 : 1;
  }

}

// End class syndGenerate

class ArticleArrayFormaterMicrosoft implements syndGenerateArrayFormater {

  private $model;

  public function __construct($model) {
    $this->model = $model;
  }

  private function getLanguage($langId) {
    static $languages = array();
    if (isset($languages[$langId])) {
      return $languages[$langId];
    }
    $lang = $this->model->getLanguage($langId);
    if ($lang) {
      $languages[$lang->getId()] = $lang->getName();
    }
    return ($lang ? $lang->getName() : '');
  }

  public function format($articleRow) {
    $artId = $articleRow['id'];

    $returnVal['id'] = $artId;
    $returnVal['headline'] = $articleRow['headline'];
    $returnVal['byLine'] = $articleRow['author'];
    $returnVal['language'] = $this->getLanguage($articleRow['language_id']);
    $returnVal['category'] = $articleRow['iptc_id'];
    $returnVal['abstract'] = $articleRow['summary'];
    $returnVal['text'] = & $articleRow['body'];
    $returnVal['date'] = trim($articleRow['date']) ? $articleRow['date'] : $articleRow['parsed_at'];
    $returnVal['parsed_at'] = $articleRow['parsed_at'];
    $returnVal['originalCategory'] = trim($articleRow['cat_name']);
    $returnVal['reference'] = trim($articleRow['reference']);

    $headlineImage = $this->model->getArticleHeadlineImage($artId);
    $returnVal['headlineImageCaption'] = $headlineImage ? $headlineImage->getImageCaption() : '';
    $returnVal['headlineImage'] = $headlineImage ? $headlineImage->getImgName() : '';
    $returnVal['videos'] = $this->model->getArticleVideos($artId);
    $returnVal['images'] = $this->model->getArticleImages($artId);
    return $returnVal;
  }

}

// End class 
