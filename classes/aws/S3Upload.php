#!/usr/local/bin/php
<?php
require 'S3.php';
require 's3_config.php';

class awsSyndigate {

  public $S3;

  public function __construct() {
    if (!extension_loaded('curl') && !@dl(PHP_SHLIB_SUFFIX == 'so' ? 'curl.so' : 'php_curl.dll'))
      exit("\nERROR: CURL extension not loaded\n\n");

    $this->S3 = new S3(awsAccessKey, awsSecretKey);
  }

  public function checkImage($argv) {
    $response = $this->S3->getBucket(awsBucketImages);
    $file = trim($argv[0], '/');
    if (!empty($response[$file])) {
      return syndigateImagesUrl . $file;
    } else {
      return FALSE;
    }
  }

  public function uploadImage($argv) {
    $file = trim($argv[0], '/');
    if ($this->S3->putObjectFile($argv[1], awsBucketImages, $file, S3::ACL_PUBLIC_READ)) {
      return syndigateImagesUrl . $file;
    } else {
      return "";
    }
  }
    public function uploadVideo($argv) {
    $file = trim($argv[0], '/');
    if ($this->S3->putObjectFile($argv[1], awsBucketVideos, $file, S3::ACL_PUBLIC_READ)) {
      return syndigateVideosUrl . $file;
    } else {
      return "";
    }
  }

}
