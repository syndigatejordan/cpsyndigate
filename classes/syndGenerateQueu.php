<?php 

require_once dirname(__FILE__) . '/../ez_autoload.php';
define('CONCURRENT_NUM', 5);

$output = shell_exec("ps aux | grep /usr/local/syndigate/classes/syndGenerateRevDriver.php");
$lines  = explode(PHP_EOL, $result);

$results = array();
foreach ($lines as $line) {
	$tabs = explode(" ", $line);
	
	$tmp = array();
	foreach ($tabs as &$tab) {
		if($tab) {
			$tmp[] = $tab;
		}
	}
	$results[] = $tmp;
}


$count = 0;
foreach ($results as $result ) {
	
	if( ( trim($result[10]) == 'php' ) ) {
		$count++;
	}
}

while ($count<=CONCURRENT_NUM) {
	$count++;
	$generation = GenerationQueuePeer::doSelectOne(new Criteria());
	if($generation) {
		$report = ReportPeer::retrieveByPK($generation->getReportId());
		exec( 'nohup /usr/bin/php /usr/local/syndigate/classes/syndGenerateRevDriver.php ' . $report->getTitleId() . ' ' . $report->getRevisionNum()  . ' ' . $report->getId() . '  > /dev/null &');
	}
}