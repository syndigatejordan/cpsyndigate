<?php
class syndGenerateMySqlIterator implements Iterator
{
	private $result;
	private $counter;
	private $rowsCount;
	private $formater;

	public function __construct($result, syndGenerateArrayFormater $formater)
	{
		$this->result = $result;
		$this->counter = 0;
		$this->rowsCount = mysql_num_rows($result);
		$this->formater = $formater;
	}
	public function rewind()
	{
		mysql_data_seek($this->result, 0);
		$this->counter = 0;
	}
	public function current()
	{
		$return = $this->formater->format(mysql_fetch_assoc($this->result));
		mysql_data_seek($this->result, $this->counter);

		return $return;
	}
	public function key()
	{
		return $this->counter;
	}
	public function next()
	{
		$this->counter++;
		return $this->formater->format(mysql_fetch_assoc($this->result));
	}
	public function valid()
	{
		return ($this->counter < $this->rowsCount);
	}

	public function getCount()
	{
		return $this->rowsCount;
	}
	
}
?>