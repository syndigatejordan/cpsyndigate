<?php
//require_once 'XML/FastCreate.php';
abstract class syndGenerateTemplate
{
	protected $tmpFile;
	private $writeOnly = true;

	function __construct($tmpFile = '')
	{
	}

	public function writeToTmpFile(&$text)
	{

		$dir = dirname($this->tmpFile);
                if(!is_dir($dir)) {
                        mkdir($dir, 0777, true);
                }

		if($this->writeOnly) {
			$fp = fopen($this->tmpFile, 'w');
			$this->writeOnly = false;
		} else {
			$fp = fopen($this->tmpFile, 'a');
		}
		fwrite($fp, $text);
		$text = '';
		fclose($fp);
	}


	
	public abstract function generate($titles);
	protected abstract function getTitle($title, $extras = array());
	protected abstract function getArticle($article, $extras = array());
}
?>
