<?php

class MRSS extends syndGenerateTemplate {

  private $XMLWriter = null;

  function __construct() {
    parent::__construct();

    @date_default_timezone_set("GMT");

    $this->XMLWriter = new XMLWriter();
    $this->XMLWriter->openMemory();
  }

  public function generate($titles) {

    $this->tmpFile = TMP_DIR . 'xml/' . date('Y/m/d/') . (microtime(1) * 100) . rand(1, 100000) . '.tmp';

    $this->XMLWriter->startDocument('1.0', 'UTF-8');
    $this->XMLWriter->setIndent(true);
    $this->XMLWriter->setIndentString(' ');

    $this->XMLWriter->startElement('rss');
    $this->XMLWriter->writeAttribute('version', '2.0');
    $this->XMLWriter->writeAttributeNS('xmlns', 'media', null, 'http://search.yahoo.com/mrss/');
    $this->XMLWriter->writeAttributeNS('xmlns', 'content', null, 'http://purl.org/rss/1.0/modules/content/');
    $this->XMLWriter->writeAttributeNS('xmlns', 'dc', null, 'http://purl.org/dc/elements/1.1/');

    $this->XMLWriter->startElement('channel');

    foreach ($titles as $title) {
      $this->getTitle($title);
    }

    // End channel
    $this->XMLWriter->endElement();
    // End rss
    $this->XMLWriter->endElement();
    // End document
    $this->XMLWriter->endDocument();

    $MRSS = $this->XMLWriter->outputMemory();

    $this->writeToTmpFile($MRSS);

    return $this->tmpFile;
  }

  protected function getTitle($title, $extras = array()) {
    echo "\n\nTitle: {$title['providerName']}\n";
    $providerName = $title['providerName'];
    $copyRight = $title['copyright'];
    $webSite = syndGenerateTemplatesManager::stringFixation($title['webSite']);
    $dateId = date('Ymd');
    $newsItemId = md5($title['id']);
    //$firstCreatedDate = $revisionCreatedDate = date('Ymd').'T'.date('His').'Z';
    $firstCreatedDate = $revisionCreatedDate = date("D, d M Y H:i:s e");

    $this->XMLWriter->startElement('title');
    $this->XMLWriter->text($providerName);
    $this->XMLWriter->endElement();

    $this->XMLWriter->startElement('description');
    $this->XMLWriter->text('MRSS feed from ' . $providerName);
    $this->XMLWriter->endElement();

    $this->XMLWriter->startElement('copyright');
    $this->XMLWriter->text($copyRight);
    $this->XMLWriter->endElement();

    $this->XMLWriter->startElement('link');
    $this->XMLWriter->text($webSite);
    $this->XMLWriter->endElement();

    $this->XMLWriter->startElement('guid');
    $this->XMLWriter->text($title['id']);
    $this->XMLWriter->endElement();

    $this->XMLWriter->startElement('pubDate');
    $this->XMLWriter->text($firstCreatedDate);
    $this->XMLWriter->endElement();

    $this->XMLWriter->startElement('lastBuildDate');
    $this->XMLWriter->text($revisionCreatedDate);
    $this->XMLWriter->endElement();

    $artCount = $title['articles']->getCount();

    $counter = 0;
    foreach ($title['articles'] as $article) {
      $counter++;
      echo "\r$counter/$artCount";
      $newsItem = $this->getArticle($article, array('providerName' => $title['providerName'], 'copyright' => $title['copyright']));
    }

    echo "\n\n";
    return $newsItem;
  }

  protected function getArticle($article, $extras = array()) {


    $this->XMLWriter->startElement('item');

    $this->XMLWriter->startElement('title');
    $this->XMLWriter->text(syndGenerateTemplatesManager::stringFixation($article['headline']));
    $this->XMLWriter->endElement();

    $this->XMLWriter->startElement('link');
    $this->XMLWriter->text(syndGenerateTemplatesManager::stringFixation($article['reference']));
    $this->XMLWriter->endElement();

    $this->XMLWriter->startElement('description');
    //Task #1339 Update the RSS feed format for client 97 
    $this->XMLWriter->text(syndGenerateTemplatesManager::stringFixation($article['abstract']));//. ' ' . $extras['providerName']
    $this->XMLWriter->endElement();

    $this->XMLWriter->writeElementNS('content', 'encoded', null, syndGenerateTemplatesManager::stringFixation($article['text'] . "\n{$extras['copyright']}\n") . COPYRIGHT);

    $this->XMLWriter->startElement('pubDate');
    $this->XMLWriter->text(syndGenerateTemplatesManager::stringFixation(date("D, d M Y H:i:s e", strtotime($article['date']))));
    $this->XMLWriter->endElement();

    $this->XMLWriter->writeElementNS('dc', 'creator', null, syndGenerateTemplatesManager::stringFixation($article['byLine']));

    $this->XMLWriter->startElement('lang');
    $this->XMLWriter->text(syndGenerateTemplatesManager::stringFixation($article['language']));
    $this->XMLWriter->endElement();

    $this->XMLWriter->startElement('guid');
    $this->XMLWriter->text(syndGenerateTemplatesManager::stringFixation($article['id']));
    $this->XMLWriter->endElement();

    if ((!empty($article['videos']) && is_array($article['videos']))) {

      $videoSpecs = array();
      $this->XMLWriter->startElementNS('media', 'group', null);

      foreach ($article['videos'] as $video) {

        $videoSpecs = json_decode($video->getBitRate(), true);
        $videoSpecs['url'] = syndGenerateTemplatesManager::stringFixation($video->getVideoName());
        $videoSpecs['medium'] = 'video';

        $this->XMLWriter->startElementNS('media', 'content', null);

        foreach ($videoSpecs as $specName => $specValue) {
          $this->XMLWriter->writeAttribute($specName, $specValue);
        }

        $this->XMLWriter->startElementNS('media', 'title', null);
        $this->XMLWriter->text(syndGenerateTemplatesManager::stringFixation($video->getOriginalName()));
        $this->XMLWriter->endElement();

        $this->XMLWriter->startElementNS('media', 'description', null);
        $this->XMLWriter->text(syndGenerateTemplatesManager::stringFixation($video->getVideoCaption()));
        $this->XMLWriter->endElement();

        // end media:content
        $this->XMLWriter->endElement();
      }
      // end media:group
      $this->XMLWriter->endElement();
    }

    if ((!empty($article['images']) && is_array($article['images']))) {

      foreach ($article['images'] as $image) {
        $this->XMLWriter->startElementNS('media', 'content', null);
        $this->XMLWriter->writeAttribute('url', syndGenerateTemplatesManager::stringFixation($image->getImgName()));
        $this->XMLWriter->writeAttribute('type', syndGenerateTemplatesManager::stringFixation($image->getImageType()));
        $this->XMLWriter->writeAttribute('caption', syndGenerateTemplatesManager::stringFixation($image->getImageCaption()));
        $this->XMLWriter->writeAttribute('medium', 'image');
        $this->XMLWriter->endElement();
      }
    }

    if (!empty($article['headlineImage'])) {
      $type = explode('.', $article['headlineImage']);
      $type = end($type);

      $this->XMLWriter->startElementNS('media', 'content', null);
      $this->XMLWriter->writeAttribute('url', syndGenerateTemplatesManager::stringFixation($article['headlineImage']));
      $this->XMLWriter->writeAttribute('type', syndGenerateTemplatesManager::stringFixation($type));
      $this->XMLWriter->writeAttribute('caption', syndGenerateTemplatesManager::stringFixation($article['headlineImageCaption']));
      $this->XMLWriter->writeAttribute('medium', 'image');
      $this->XMLWriter->endElement();
    }

    $this->XMLWriter->endElement();
  }

}

?>
