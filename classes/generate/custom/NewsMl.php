<?php
class NewsMl extends syndGenerateTemplate
{

	function __construct()
    {
    	parent::__construct();
    }

    public function generate($titles)
	{
		 
		$this->tmpFile = TMP_DIR . 'xml/' .date('Y/m/d/')  . (microtime(1)*100).rand(1, 100000).'.tmp';
		//$keys = array_keys($titles);


		$newsMl = '<?xml version="1.0" encoding="UTF-8"?>'.
			  '<NewsML>'.
				'<NewsEnvelope>'.
					'<DateAndTime>'.
						date('Ymd').'T'.date('His').'Z'.
					'</DateAndTime>'.
				'</NewsEnvelope>';
		$this->writeToTmpFile($newsMl);
		foreach($titles as $title) {
			$newsMl = $this->getTitle($title);
		}

		$newsMl = '</NewsML>';
		$this->writeToTmpFile($newsMl);
		
		return $this->tmpFile;
	}

	protected function getTitle($title, $extras = array())
	{ 
		echo "\n\nTitle: {$title['providerName']}\n";
		$webSite = syndGenerateTemplatesManager::stringFixation($title['webSite']);
		$dateId  = date('Ymd');
		$newsItemId = md5($title['id']);
		$firstCreatedDate = $revisionCreatedDate = date('Ymd').'T'.date('His').'Z';

		
		$newsItem = '<NewsItem>'.
				'<Identification>'.
					'<NewsIdentifier>'.
						'<ProviderId>'.
							$webSite.
						'</ProviderId>'.
						'<DateId>'.
							$dateId.
						'</DateId>'.
						'<NewsItemId>'.
							$newsItemId.
						'</NewsItemId>'.
						'<RevisionId PreviousRevision="0" Update="N">1</RevisionId>'.
						'<PublicIdentifier>'.
							"urn:newsml:$webSite:$dateId:$newsItemId:1".
						'</PublicIdentifier>'.
					'</NewsIdentifier>'.
				'</Identification>';

		$newsItem .= 	'<NewsManagement>'.
					'<NewsItemType FormalName="News" />'.
					'<FirstCreated>'.$firstCreatedDate.'</FirstCreated>'.
					'<ThisRevisionCreated>'.$revisionCreatedDate.'</ThisRevisionCreated>'.
					'<Status FormalName="Usable" />'.
				'</NewsManagement>';
	


		$this->writeToTmpFile($newsItem);
		$artCount = $title['articles']->getCount();
		$counter  = 0;
		
		foreach($title['articles'] as $article) { 
			$counter++;
			echo "\r$counter/$artCount";
			$newsItem = $this->getArticle($article, array('providerName'=>$title['providerName'], 'copyright'=>$title['copyright']));
			$this->writeToTmpFile($newsItem);
		}

		$newsItem = '</NewsItem>';
		$this->writeToTmpFile($newsItem);
		echo "\n\n";
		return $newsItem;
	}

	
	protected function getArticle($article, $extras = array())
	{ 	
		$newsComponent = '<NewsComponent>'.
					'<NewsLines>'.
						'<HeadLine>'.
							 syndGenerateTemplatesManager::stringFixation($article['headline']) .
						'</HeadLine>'.
						'<ByLine>'.
							syndGenerateTemplatesManager::stringFixation($article['byLine']).
						'</ByLine>'.
					'</NewsLines>'.
					'<DescriptiveMetadata>'.
						'<Language FormalName="'.syndGenerateTemplatesManager::stringFixation($article['language']).'" />'.
						'<Property FormalName="providerName" Value="'.syndGenerateTemplatesManager::stringFixation($extras['providerName']).'" />'.
						'<Property FormalName="categoryName" Value="'.syndGenerateTemplatesManager::stringFixation($article['category']).'" />'.
						'<Property FormalName="originalCategory" Value="'.syndGenerateTemplatesManager::stringFixation($article['originalCategory']).'" />'.
						'<Property FormalName="headlineImage" Value="'.syndGenerateTemplatesManager::stringFixation($article['headlineImage']).'" />'.
						'<Property FormalName="headlineImageCaption" Value="'.syndGenerateTemplatesManager::stringFixation($article['headlineImageCaption']).'" />'.
						'<Property FormalName="articleDate" Value="'.syndGenerateTemplatesManager::stringFixation($article['date']).'" />'.
						'<Property FormalName="articleId" Value="'.syndGenerateTemplatesManager::stringFixation($article['id']).'" />'.
					'</DescriptiveMetadata>'.
					'<NewsComponent>'.
						'<ContentItem>'.
							'<MediaType FormalName="Text" />'.
							'<Format FormalName="HTML" />'.
							'<DataContent>'.
								 syndGenerateTemplatesManager::stringFixation($article['abstract']) .
							'</DataContent>'.
						'</ContentItem>'.
						'<ContentItem>'.
							'<MediaType FormalName="Text" />'.
							'<Format FormalName="HTML" />'.
							'<DataContent>'.
								 syndGenerateTemplatesManager::stringFixation($article['text'])."<p class='syndigate_disclaimer'>{$extras['copyright']}<span>".COPYRIGHT."</span></p>".
							'</DataContent>'.
						'</ContentItem>';
										
					foreach ($article['videos'] as $video) {
						$newsComponent.=
						'<ContentItem Href="' . syndGenerateTemplatesManager::stringFixation($video->getVideoName()) .'">'.
							'<MediaType FormalName = "'. syndGenerateTemplatesManager::stringFixation($video->getVideoType()). '"/>'.
							'<MimeType FormalName = "'. syndGenerateTemplatesManager::stringFixation($video->getMimeType()). '"/>'.
							'<DataContent>'.
								 
							'</DataContent>'.
						'</ContentItem>';	
					} 
					
					foreach ($article['images'] as $image) {
						
						//headline Image is in another place 
						if($image->getisHeadline()) {
							continue;
						}
						$newsComponent.=
						'<ContentItem Href="' . syndGenerateTemplatesManager::stringFixation($image->getImgName()) .'">'.
							'<MediaType FormalName = "'. syndGenerateTemplatesManager::stringFixation($image->getImageType()). '"/>'.
							'<MimeType FormalName = "'. syndGenerateTemplatesManager::stringFixation($image->getMimeType()). '"/>'.
							'<ImageCaption Value = "'. syndGenerateTemplatesManager::stringFixation($image->getImageCaption()). '"/>'.
							'<DataContent>'.
								 
							'</DataContent>'.
						'</ContentItem>';	
					}	
									
				$newsComponent.=		
					'</NewsComponent>'.
				'</NewsComponent>';
		
		return $newsComponent;
	}

}
?>
