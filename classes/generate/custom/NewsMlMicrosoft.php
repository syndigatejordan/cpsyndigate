<?php

class NewsMlMicrosoft extends syndGenerateTemplate {

  function __construct() {
    parent::__construct();
  }

  public function generate($titles) {

    $this->tmpFile = TMP_DIR . 'xml/' . date('Y/m/d/') . (microtime(1) * 100) . rand(1, 100000) . '.tmp';
    //$keys = array_keys($titles);


    $newsMl = '<?xml version="1.0" encoding="UTF-8"?>' .
            '<NewsML  xmlns:mi="http://schemas.ingestion.microsoft.com/common/">' .
            '<Catalog href="http://www.iptc.org/std/catalog/catalog.IPTC-G2-Standards_17.xml"/>' .
            '<NewsEnvelope>' .
            '<DateAndTime>' .
            date('c') .
            '</DateAndTime>' .
            '</NewsEnvelope>';
    $this->writeToTmpFile($newsMl);
    foreach ($titles as $title) {
      $newsMl = $this->getTitle($title);
    }

    $newsMl = '</NewsML>';
    $this->writeToTmpFile($newsMl);

    return $this->tmpFile;
  }

  protected function getTitle($title, $extras = array()) {
    echo "\n\nTitle: {$title['providerName']}\n";
    $webSite = syndGenerateTemplatesManager::stringFixation($title['webSite']);
    $dateId = date('Ymd');
    $newsItemId = md5($title['id']);
    $revisionCreatedDate = date('c');

//		$this->writeToTmpFile($newsItem);
    $artCount = $title['articles']->getCount();
    $counter = 0;
    foreach ($title['articles'] as $article) {
      $counter++;
      echo "\r$counter/$artCount";
      $newsItem = '<NewsItem>' .
              '<Identification>' .
              '<NewsIdentifier>' .
              '<ProviderId>' .
              $webSite .
              '</ProviderId>' .
              '<NewsItemId>' .
              syndGenerateTemplatesManager::stringFixation($article['id']) .
              '</NewsItemId>' .
              '</NewsIdentifier>' .
              '</Identification>';
      ///////////Slideshow/////////
      if ($title['id'] == 2089 || $title['id'] == 2090 || $title['id'] == 2094 || $title['id'] == 2321 || $title['id'] == 3107) {
        $date = date('c', strtotime($article['parsed_at']));
        $author = syndGenerateTemplatesManager::stringFixation($article['author']);
        if (empty($author)) {
          $author = syndGenerateTemplatesManager::stringFixation($title['providerName']);
        }
        $newsItem .= '<NewsComponent Duid="' . syndGenerateTemplatesManager::stringFixation($article['id']) . '">
            <DateLine>' . $date . '</DateLine>
            <Author>' . $author . '</Author>';
      }
      $first_created = date("c", strtotime($article['parsed_at']) + $counter);
      $expirationDate = date('c', strtotime('+1 years'));
      $newsItem .= '<NewsManagement>' .
              '<NewsItemType FormalName="News" />';

      if ($title['id'] == 57) {
        $newsItem .='<FirstCreated>' . $first_created . '</FirstCreated>';
      } else {
        $newsItem .='<FirstCreated>' . date("c", strtotime(syndGenerateTemplatesManager::stringFixation($article['date']))) . '</FirstCreated>';
      }

      $newsItem .= '<ThisRevisionCreated>' . $first_created . '</ThisRevisionCreated>';

      if ($title['id'] == 57) {
        $newsItem .='<mi:dateTimeWritten>' . $first_created . '</mi:dateTimeWritten>';
      } else {
        $newsItem .='<mi:dateTimeWritten>' . date("c", strtotime(syndGenerateTemplatesManager::stringFixation($article['date']))) . '</mi:dateTimeWritten>';
      }

      $newsItem .= '<ExpirationDate>' . $expirationDate . '</ExpirationDate>' .
              '<Status FormalName="Published" />' .
              '</NewsManagement>';
      if ($title['id'] == 2089 || $title['id'] == 2090 || $title['id'] == 2094 || $title['id'] == 2321 || $title['id'] == 3107) {
        $newsItem .= $this->getSlideShow($article, $title['id'], array('providerName' => $title['providerName'], 'copyright' => $title['copyright']));
      } else {
        $title_id = $title['id'];
        $newsItem .= $this->getArticle($article, array('providerName' => $title['providerName'], 'copyright' => $title['copyright'], 'title_id' => $title['id']));
      }
      $newsItem .= '</NewsItem>';
      $this->writeToTmpFile($newsItem);

      $this->writeToTmpFile($newsItem);
    }

    echo "\n\n";
    return $newsItem;
  }

  protected function getArticle($article, $extras = array()) {
    $image_copyright = $article['language'] == 'English' ? "Provided by " . syndGenerateTemplatesManager::stringFixation($extras['providerName']) : syndGenerateTemplatesManager::stringFixation($extras['providerName']) . " قدمت بواسطة";
    if ($article['language'] == 'French') {
      $image_copyright = 'Présenté par';
    }
    $byLine = syndGenerateTemplatesManager::stringFixation($article['byLine']);
    if (empty($byLine)) {
      $byLine = syndGenerateTemplatesManager::stringFixation($extras['providerName']);
    }
    $newsComponent = '<NewsLines>' .
            '<CopyrightLine>' . "{$extras['copyright']} " . COPYRIGHT .
            '</CopyrightLine>' .
            '<HeadLine>' .
            syndGenerateTemplatesManager::stringFixation($article['headline']) .
            '</HeadLine>' .
            '<Slugline>' .
            '</Slugline>' .
            '<ByLine>' .
            $byLine .
            '</ByLine>' .
            '<Description>';
    if (!empty($article['summary'])) {
      $newsComponent.=syndGenerateTemplatesManager::stringFixation($article['summary']);
    } else {
      $summary = html_entity_decode(syndGenerateTemplatesManager::stringFixation($article['text']));
      $summary = strip_tags($summary);
      $summary = preg_replace('/\s+?(\S+)?$/', '', substr($summary, 0, 500));
      $newsComponent.= '<![CDATA[';
      $summary = str_replace("&nbsp;", " ", $summary);
      $newsComponent.=$summary
              . ']]>';
    }

    $newsComponent.='</Description>' .
            '<DerivedFrom>' .
            syndGenerateTemplatesManager::stringFixation($article['reference']) .
            '</DerivedFrom>' .
            '<mi:shortTitle>N/A</mi:shortTitle>' .
            '<mi:deepLink url="N/A"></mi:deepLink>' .
            '</NewsLines>' .
            /*  '<DescriptiveMetadata>' .
              '<Language FormalName="' . syndGenerateTemplatesManager::stringFixation($article['language']) . '" />' .
              '<Property FormalName="providerName" Value="' . syndGenerateTemplatesManager::stringFixation($extras['providerName']) . '" />' .
              '<Property FormalName="categoryName" Value="' . syndGenerateTemplatesManager::stringFixation($article['category']) . '" />' .
              '<Property FormalName="originalCategory" Value="' . syndGenerateTemplatesManager::stringFixation($article['originalCategory']) . '" />' .
              '<Property FormalName="articleDate" Value="' . syndGenerateTemplatesManager::stringFixation($article['date']) . '" />' .
              '</DescriptiveMetadata>' . */
            '<NewsComponent Duid="' . syndGenerateTemplatesManager::stringFixation($article['id']) . '">' .
            '<NewsComponent>' .
            '<Role FormalName="Main"/>' .
            '<ContentItem>' .
            '<MediaType FormalName="ComplexData" />' .
            '<MimeType FormalName="text/vnd.IPTC.NITF"/>' .
            '<DataContent>' .
            '<nitf><body><body.content>';
    $titleIdArray = array(
        23, 25, 34, 46, 54, 55, 57, 61, 62, 79, 87, 171, 183, 185, 186, 299, 300, 303, 305, 312, 314, 363, 364, 392, 424, 425, 426, 427, 732, 746, 793, 855, 858, 860,
        862, 906, 907, 909, 932, 977, 1056, 1069, 1149, 1184, 1187, 1222, 1312, 1341, 1355, 1373, 1374, 1406, 1428, 1492, 1515, 1518, 1526, 1738, 1743, 1794,
        1799, 1804, 1805, 1890, 1943, 2080, 2093, 2150, 2151, 2152, 2237, 2240, 2369, 2382, 2510, 2516, 2517, 2575, 2586, 2689, 2722, 2726, 2729, 2731, 2734,
        2735, 2737, 2739, 2742, 2744, 2764, 2777, 2778, 2803, 2805, 2818, 2821, 2902, 2983, 3094, 3111, 3121, 3123, 3126, 3136, 3175, 3179, 3188, 3190, 3486, 5333, 5334
    );
    if (!empty($article['headlineImage'])) {
      $newsComponent.= '<media media-type="image">' .
              '<media-reference mime-type="image/jpg"' .
              ' source="' . syndGenerateTemplatesManager::stringFixation($article['headlineImage']) .
              '" copyright="' . $image_copyright . '" />' .
              '<media-caption>' .
              syndGenerateTemplatesManager::stringFixation($article['headlineImageCaption']) .
              '</media-caption>' .
              '<mi:focalRegion>' .
              '<mi:x1>N/A</mi:x1>' .
              '<mi:y1>N/A</mi:y1>' .
              '<mi:x2>N/A</mi:x2>' .
              '<mi:y2>N/A</mi:y2>' .
              '</mi:focalRegion>' .
              '<mi:hasSyndicationRights>1</mi:hasSyndicationRights>' .
              '<mi:licenseId>' .
              hexdec(substr(sha1(syndGenerateTemplatesManager::stringFixation($article['headlineImage'])), 0, 15)) .
              '</mi:licenseId>' .
              '<mi:licensorName>' .
              syndGenerateTemplatesManager::stringFixation($extras['providerName']) .
              '</mi:licensorName>' .
              '</media>';
    } elseif (empty($article['images'])) {
      /* if (in_array($extras["title_id"], $titleIdArray)) {
        $newsComponent.= '<media media-type="image">' .
        '<media-reference mime-type="image/png"' .
        ' source="http://pub.syndigate.info/logos/default/' . $extras["title_id"] . '.png" ' .
        'copyright="' . $image_copyright . '" />' .
        '<media-caption>' .
        '</media-caption>' .
        '<mi:focalRegion>' .
        '<mi:x1>N/A</mi:x1>' .
        '<mi:y1>N/A</mi:y1>' .
        '<mi:x2>N/A</mi:x2>' .
        '<mi:y2>N/A</mi:y2>' .
        '</mi:focalRegion>' .
        '<mi:hasSyndicationRights>' .
        '</mi:hasSyndicationRights>' .
        '<mi:licenseId>' .
        '</mi:licenseId>' .
        '<mi:licensorName>' .
        '</mi:licensorName>' .
        '</media>';
        } */
    }
    foreach ($article['images'] as $image) {
      $newsComponent.= '<media media-type="image">' .
              '<media-reference mime-type="image/' . $image->getImageType() .
              '" source="' . syndGenerateTemplatesManager::stringFixation($image->getImgName()) .
              '" copyright="' . $image_copyright . '" />' .
              '<media-caption>' .
              syndGenerateTemplatesManager::stringFixation($image->getImageCaption()) .
              '</media-caption>' .
              '<mi:focalRegion>' .
              '<mi:x1>N/A</mi:x1>' .
              '<mi:y1>N/A</mi:y1>' .
              '<mi:x2>N/A</mi:x2>' .
              '<mi:y2>N/A</mi:y2>' .
              '</mi:focalRegion>' .
              '<mi:hasSyndicationRights>1</mi:hasSyndicationRights>' .
              '<mi:licenseId>' .
              hexdec(substr(md5(syndGenerateTemplatesManager::stringFixation($image->getImgName())), 0, 15)) .
              '</mi:licenseId>' .
              '<mi:licensorName>' .
              syndGenerateTemplatesManager::stringFixation($extras['providerName']) .
              '</mi:licensorName>' .
              '</media>';
    }

    foreach ($article['videos'] as $video) {
      $newsComponent.= '<media media-type="video">' .
              '<media-reference mime-type="video/' . $video->getVideoType() .
              '" source="' . syndGenerateTemplatesManager::stringFixation($video->getVideoName()) .
              '" copyright="' . $image_copyright . '"/>' .
              '<media-caption>' . syndGenerateTemplatesManager::stringFixation($video->getVideoCaption()) .
              '</media-caption>' .
              '</media>';
    }
    $newsComponent.= '<![CDATA[';
    $bodyhtml = html_entity_decode(syndGenerateTemplatesManager::stringFixation($article['text']));
    if ($extras["title_id"] == 3494 || $extras["title_id"] == 3495) {
      $bodyhtml = preg_replace("!<iframe(.*?)src=\"https://www.google.com/maps/embed/(.*?)</iframe>!is", "", $bodyhtml);
    }
    //The below line for Disco Marketplace, remove images from the body.
    $titleIdImagesArray = array(9, 57, 171, 175, 176, 185, 392, 701, 814, 858, 862, 890, 917, 1069, 1081, 1288, 1290, 1291, 1292,
        1294, 1295, 1296, 1297, 1298, 1299, 1300, 1301, 1303, 2283, 2284, 1312, 1680, 2321, 2374, 2731, 2734, 3126, 2369,
        3108, 3109, 3110, 3156, 3157, 3158, 3159, 3160, 3162, 3163, 3164, 3165, 3166, 3167, 3168, 3169, 3170, 3444, 2445,
        3486, 3377, 1738, 2080, 2686, 2687, 1164, 1284, 1647, 1491, 2764, 1217, 1526, 1152, 1160, 2646, 1286);
    if (in_array($extras["title_id"], $titleIdImagesArray)) {
      $bodyhtml = preg_replace('/<img[^>]+>/i', '', $bodyhtml);
    }
    $bodyhtml = str_replace("&nbsp;", " ", $bodyhtml);
    $newsComponent.=$bodyhtml
            . ']]>' .
            '</body.content></body></nitf></DataContent>' .
            '</ContentItem>' .
            '<item_keywords>' .
            '<item_keyword>' .
            syndGenerateTemplatesManager::stringFixation($article['originalCategory']) .
            '</item_keyword>' .
            '</item_keywords>' .
            '</NewsComponent>' .
            '</NewsComponent>';

    return $newsComponent;
  }

  protected function getSlideShow($article, $title_id, $extras = array()) {
    $image_copyright = $article['language'] == 'English' ? "Provided by " . syndGenerateTemplatesManager::stringFixation($extras['providerName']) : syndGenerateTemplatesManager::stringFixation($extras['providerName']) . " قدمت بواسطة";
    if ($article['language'] == 'French') {
      $image_copyright = 'Présenté par';
    }
    $newsComponent = '<NewsComponent>' .
            '<Role FormalName="Main"/>' .
            '<NewsLines>' .
            '<HeadLine>' .
            syndGenerateTemplatesManager::stringFixation($article['headline']) .
            '</HeadLine>' .
            '<SlugLine>';
    if (!empty($article['summary'])) {
      $newsComponent.=syndGenerateTemplatesManager::stringFixation($article['summary']);
    } else {
      $summary = html_entity_decode(syndGenerateTemplatesManager::stringFixation($article['text']));
      $summary = strip_tags($summary);
      $summary = preg_replace('/\s+?(\S+)?$/', '', substr($summary, 0, 500));
      $newsComponent.= '<![CDATA[';
      $summary = str_replace("&nbsp;", " ", $summary);
      $newsComponent.=$summary
              . ']]>';
    }
    $newsComponent.= '</SlugLine>' .
            '<DerivedFrom>' .
            syndGenerateTemplatesManager::stringFixation($article['reference']) .
            '</DerivedFrom>' .
            '</NewsLines>' .
            '<ContentItem>' .
            '<MediaType FormalName="ComplexData" />' .
            '<MimeType FormalName="text/vnd.IPTC.NITF"/>' .
            '<DataContent>' .
            '<nitf><body><body.content><p>';
    $newsComponent.= '<![CDATA[' . html_entity_decode(syndGenerateTemplatesManager::stringFixation($article['text'])) . ']]>' .
            '</p></body.content></body></nitf>' .
            '</DataContent>' .
            '</ContentItem>' .
            '<item_keywords>' .
            '<item_keyword>' .
            syndGenerateTemplatesManager::stringFixation($article['originalCategory']) .
            '</item_keyword>' .
            '</item_keywords>' .
            '</NewsComponent>' .
            '<NewsComponent>' .
            '<Role FormalName="Supporting"/>';

    $slideshowimages = array();
    $slideshowimages = $this->mergeImageAndThumb($article['images'], $title_id);
    foreach ($slideshowimages as $slideshowimage) {
      $newsComponent.= '<NewsComponent>' .
              '<Role FormalName="Main"/>' .
              '<ContentItem Href="' . syndGenerateTemplatesManager::stringFixation($slideshowimage["img_name"]) . '" title="' .
              str_replace("&quot;", "", syndGenerateTemplatesManager::stringFixation($slideshowimage["image_caption"]))
              . '" imageabstract="" copyright="' . $image_copyright . '">' .
              '<MediaType FormalName="Photo">' .
              '<Thumbnail>' .
              '<ContentItem Href="' . syndGenerateTemplatesManager::stringFixation($slideshowimage["image_thumb"]) . '"/>' .
              '</Thumbnail>' .
              '</MediaType>' .
              '<MimeType FormalName="image/' . $slideshowimage["image_type"] . '"/>' .
              '<Characteristics>' .
              '<SizeInBytes>' .
              strval($this->remote_file_size(syndGenerateTemplatesManager::stringFixation($slideshowimage["img_name"]))) .
              '</SizeInBytes>' .
              '</Characteristics>' .
              '<mi:focalRegion>' .
              '<mi:x1>N/A</mi:x1>' .
              '<mi:y1>N/A</mi:y1>' .
              '<mi:x2>N/A</mi:x2>' .
              '<mi:y2>N/A</mi:y2>' .
              '</mi:focalRegion>' .
              '<mi:hasSyndicationRights>1</mi:hasSyndicationRights>' .
              '<mi:licenseId>' .
              hexdec(substr(md5(syndGenerateTemplatesManager::stringFixation(syndGenerateTemplatesManager::stringFixation($slideshowimage["img_name"]))), 0, 15)) .
              '</mi:licenseId>' .
              '<mi:licensorName>' .
              syndGenerateTemplatesManager::stringFixation($extras['providerName']) .
              '</mi:licensorName>' .
              '</ContentItem>' .
              '</NewsComponent>' .
              '<NewsComponent>' .
              '<Role FormalName="Caption"/>' .
              '<ContentItem>' .
              '<MediaType FormalName="Text"/>' .
              '<MimeType FormalName="text/plain"/>' .
              '<DataContent>' .
              syndGenerateTemplatesManager::stringFixation($slideshowimage["image_caption"]) .
              '</DataContent>' .
              '</ContentItem>' .
              '</NewsComponent>';
    }
    $newsComponent.='</NewsComponent></NewsComponent>';

    return $newsComponent;
  }

  function remote_file_size($url) {
    # Get all header information
    $data = get_headers($url, true);
    # Look up validity
    if (isset($data['Content-Length']))
    # Return file size
      return (int) $data['Content-Length'];
  }

  function mergeImageAndThumb($images, $title_id) {
    $newImages = array();
    foreach ($images as $image) {
      $originalName = $image->getOriginalName();
      if (strpos($originalName, 'thumb_') === false) {
        $newImages[0]['img_name'] = $image->getImgName();
        $newImages[0]['image_caption'] = $image->getImageCaption();
        $newImages[0]['image_type'] = $image->getImageType();
        foreach ($images as $thumbimage) {
          $originalNameThumb = str_replace('/images/', '/images/thumb_', $originalName);
          if ($title_id == 2094) {
            $type = $image->getImageType();
            $originalNameThumb = str_replace('.' . $type, '-160x160.' . $type, $originalNameThumb);
          }
          $original_name = $thumbimage->getOriginalName();
          if ($title_id == 2321 || $title_id == 3107) {
            $original_name = $image->getOriginalName();
            $original_name = explode('/', $original_name);
            $original_name = explode('.', end($original_name));
            $original_name = $original_name[0];
            $originalNameThumb = $thumbimage->getOriginalName();
            $originalNameThumb = explode('/', $originalNameThumb);
            $originalNameThumb = explode('.', end($originalNameThumb));
            $originalNameThumb = substr(str_replace('thumb_', '', $originalNameThumb[0]), 0, strlen($original_name));
          }
          if ($originalNameThumb == $original_name) {
            $newImages[0]['image_thumb'] = $thumbimage->getImgName();
          }
        }
        array_push($newImages, $newImages[0]);
      }
    }
    return $newImages;
  }

}

?>
