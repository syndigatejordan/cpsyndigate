<?php
/*
 * Test Root layout for Test Client #49
 * This should be removed when every things goes well :)
 */
 
class syndGenerate_49 //extends syndGenerate 
{
	
	public function __construct($params)
	{
		$this->files = array();
		
		foreach ($params as $paramName => $paramValue) {
			$this->_vars[$paramName] = $paramValue;
		}

		$id = explode("_", __CLASS__); 
		$this->id = $id[1];
		
	}
	
	public function afterGenerate()
	{
		$this->moveFilesIntoFolders();
	}
	
	
	private function moveFilesIntoFolders()
	{

		$client = ClientPeer::retrieveByPK( $this -> id );

		if ($handle = opendir($this->_vars['ftpDir'])) {
			while (false !== ($file = readdir($handle))) {
				
				if($file != '.' && $file != '..' && is_file($this->_vars['ftpDir'] . $file)) {
					$newFileName = substr($file, 0, -4);
					$newFileName = $newFileName . '_' . date('Ymd') . '_' . date('His') . '_' . mt_rand(1, 10000000) . '.xml'; 
					rename( $this->_vars['ftpDir'] . $file, $client->getHomeDir() . $newFileName);
					$this->files[] = $client->getHomeDir() . $newFileName;
				}
			}
			closedir($handle);
		}
	}
	
}
