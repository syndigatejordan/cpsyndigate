<?php
class syndGenerate_7 //extends syndGenerate 
{
	public function __construct($params)
	{		
		$this->files = array();
		
		foreach ($params as $paramName => $paramValue) {
			$this->_vars[$paramName] = $paramValue;
		}

		$id = explode("_", __CLASS__); 
		$this->id = $id[1];
		
	}
	
	public function afterGenerate()
	{
		$this->moveFilesIntoFolders();
	}
	
	
	private function moveFilesIntoFolders()
	{
		
		if ($handle = opendir($this->_vars['ftpDir'])) {
			while (false !== ($file = readdir($handle))) {
				
				if($file != '.' && $file != '..' && is_file($this->_vars['ftpDir'] . $file)) {
										
					$folderToMove;
					if($this->_vars['generateType'] == 'updates') {
						$folderToMove = explode('_', $file);
					} else {
						$folderToMove = explode('.', $file);
					}
					
					$folderToMove = $folderToMove[0]; // title id 
					$folderToMove = TitlePeer::retrieveByPK($folderToMove)->getFtpUsername();
					$dirToMove    = $this->_vars['ftpDir'] . $folderToMove .'/';
					
					if(!is_dir($dirToMove)) {
						mkdir($dirToMove);
					}
					
					rename($this->_vars['ftpDir'].$file, $dirToMove.$file);
					$this->files[] = $dirToMove . $file;
				}
			}
			closedir($handle);
		}
	}
	
}
