<?php
/*
 * Put the files on the root to make http link on the files as client request
 * link to the file
 */

class syndGenerate_85 {

	public function __construct($params) {
		$this -> files = array();

		foreach ($params as $paramName => $paramValue) {
			$this -> _vars[$paramName] = $paramValue;
		}

		$id = explode("_", __CLASS__);
		$this -> id = $id[1];

	}

	public function afterGenerate() {
		$this -> moveFilesIntoFolders();
	}

	private function moveFilesIntoFolders() {

		$client = ClientPeer::retrieveByPK($this -> id);

		if ($handle = opendir($this -> _vars['ftpDir'])) {
			while (false !== ($file = readdir($handle))) {

				if ($file != '.' && $file != '..' && is_file($this -> _vars['ftpDir'] . $file)) {
					copy($this -> _vars['ftpDir'] . $file, $client -> getHomeDir() . $file);
					$this -> files[] = $client -> getHomeDir() . $file;
				}

			}
			closedir($handle);
		}
	}

}
