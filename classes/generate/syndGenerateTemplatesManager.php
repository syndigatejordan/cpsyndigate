<?php
class syndGenerateTemplatesManager
{
	private function __construct(){}

	public static function factory($name)
	{
		require_once APP_PATH."classes/generate/custom/$name.php";
		return new $name();
	}

	public static function stringFixation($text)
	{
		$text = htmlspecialchars(str_replace(chr(11), '', $text));
		return self::strippingTags($text);
	}

	public static function strippingTags($text)
	{
		if(STRIPPING_TAGS) {
			return strip_tags($text);
		}
		return $text;
	}
}
?>