<?php
require_once '/usr/local/syndigate/scripts/propel_include_config.php';

class FixtClientsTitleLogoCustom {

	public function __construct($ClientID) {
		$this -> ClientID = $ClientID;
		if (!$this -> ClientID) {echo "please provide the Client ID.";
			exit ;
		}
		$this -> processJob();
	}

	public function processJob() {

		$clientTitles = TitlesClientsRulesPeer::getClientTitles($this -> ClientID);
		var_dump( count($clientTitles)).chr(10);
		foreach ($clientTitles as $title) {
			$logoPath = $title -> getLogoPath();
			echo $logoPath . chr(10);
			if ($logoPath) {
				$title_name = str_replace(' ', '_', $title->getName()).'Logo';
				$logoPath = explode('/', $logoPath);
				$logo = $logoPath[count($logoPath) - 1];
				$logoPath = sfConfig::get('sf_upload_dir') . '/' . $logo;
				echo $title -> getId() . chr(10);
				$oldLogoPath = '/syndigate/pub/logos/' . $this -> ClientID . '/' . $title -> getId() . '.png';
				$newLogoPath = '/syndigate/pub/logos/new/' . $this -> ClientID . '/' . $title -> getId() . '.png';
				$newLogoPathGIF = '/syndigate/pub/logos/newGif/' . $this -> ClientID . '/' . $title_name . '.gif';
				if (is_file($logoPath)) {
					copy($logoPath, $oldLogoPath);
					$this -> saveResizedImage($oldLogoPath, $newLogoPath, 400, 40);
					 imagegif(imagecreatefromstring(file_get_contents($newLogoPath)), $newLogoPathGIF);
					chmod($newLogoPath, 0777);
				}
			}
		}
	}

	public function saveResizedImage($srcfile, $dstfile, $maxw = '', $maxh = '') {
		$settings = new ezcImageConverterSettings( array(new ezcImageHandlerSettings('GD', 'ezcImageGdHandler'),
		//new ezcImageHandlerSettings( 'ImageMagick', 'ezcImageImagemagickHandler' ),
		));
		$converter = new ezcImageConverter($settings);
		if ($maxh || $maxw) {
			$scaleFilters = array(new ezcImageFilter('scale', array('width' => $maxw, 'height' => $maxh, 'direction' => ezcImageGeometryFilters::SCALE_DOWN)));
		} else {
			$scaleFilters = array();
		}
		// Which MIME types the conversion may output
		$mimeTypes = array('image/png');

		// Create the transformation inside the manager, this can be re-used
		$converter -> createTransformation('png', $scaleFilters, $mimeTypes);

		try {
			echo $srcfile.' '.$dstfile.chr(10);
			$converter -> transform('png', $srcfile, $dstfile);
		} catch ( ezcImageTransformationException $e) {
			trigger_error("Exception: {$e->getMessage()} File:'$srcfile' ", E_USER_WARNING);
			return false;
		}

		return true;
	}

}// End class

$ClientID = $argv[1];
$obj = new FixtClientsTitleLogoCustom($ClientID);
