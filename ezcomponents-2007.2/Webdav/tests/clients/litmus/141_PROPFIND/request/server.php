<?php

return array (
  'PATH' => '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/sbin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/opt/bin',
  'PWD' => '/home/dotxp',
  'HOME' => '/home/dotxp',
  '_' => '/sbin/start-stop-daemon',
  'SERVER_SOFTWARE' => 'lighttpd/1.4.18',
  'SERVER_NAME' => 'webdav',
  'GATEWAY_INTERFACE' => 'CGI/1.1',
  'SERVER_PORT' => '80',
  'SERVER_ADDR' => '127.0.0.1',
  'REMOTE_PORT' => '45210',
  'REMOTE_ADDR' => '127.0.0.1',
  'CONTENT_LENGTH' => '345',
  'SCRIPT_NAME' => '/index.php',
  'PATH_INFO' => '/litmus/prop',
  'PATH_TRANSLATED' => '/home/dotxp/web/webdav/htdocs//litmus/prop',
  'SCRIPT_FILENAME' => '/home/dotxp/web/webdav/htdocs/index.php',
  'DOCUMENT_ROOT' => '/home/dotxp/web/webdav/htdocs/',
  'REQUEST_URI' => '/litmus/prop',
  'REDIRECT_URI' => '/index.php/litmus/prop',
  'QUERY_STRING' => '',
  'REQUEST_METHOD' => 'PROPFIND',
  'REDIRECT_STATUS' => '200',
  'SERVER_PROTOCOL' => 'HTTP/1.1',
  'HTTP_HOST' => 'webdav',
  'HTTP_USER_AGENT' => 'litmus/0.11 neon/0.26.3',
  'HTTP_CONNECTION' => 'TE',
  'HTTP_TE' => 'trailers',
  'HTTP_DEPTH' => '0',
  'HTTP_CONTENT_LENGTH' => '345',
  'CONTENT_TYPE' => 'application/xml',
  'HTTP_X_LITMUS' => 'props: 27 (propget)',
  'PHP_SELF' => '/index.php/litmus/prop',
  'REQUEST_TIME' => 1192631988,
);

?>