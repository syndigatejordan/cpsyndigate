<?php

return array (
  'REQUEST_METHOD' => 'LOCK',
  'REQUEST_URI' => '/webdav/',
  'SERVER_PROTOCOL' => 'HTTP/1.1',
  'HTTP_HOST' => 'webdav.sb.aol.com',
  'HTTP_TIMEOUT' => 'Infinite, Second-4100000000',
  'HTTP_DEPTH' => 'infinity',
  'CONTENT_TYPE' => 'text/xml; charset="utf-8"',
  'HTTP_CONTENT_LENGTH' => '1234',
  'HTTP_AUTH' => 'Digest username="ejw", realm="ejw@webdav.sb.aol.com", nonce="...", uri="/workspace/webdav/proposal.doc", response="...", opaque="..."',
);

?>