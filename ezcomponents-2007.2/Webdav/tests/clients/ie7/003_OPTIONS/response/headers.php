<?php

return array (
  'DAV' => '1, 2',
  'Allow' => 'GET, HEAD, PROPFIND, PROPPATCH, OPTIONS, DELETE, COPY, MOVE, MKCOL, PUT',
  'Server' => 'lighttpd/1.4.18/eZComponents/dev/ezcWebdavMicrosoftCompatibleTransportMock',
  'MS-Author-Via' => 'DAV',
);

?>