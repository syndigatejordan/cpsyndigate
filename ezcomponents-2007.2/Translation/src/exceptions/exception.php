<?php
/**
 * @copyright Copyright (C) 2005-2007 eZ systems as. All rights reserved.
 * @license http://ez.no/licenses/new_bsd New BSD License
 * @version 1.1.6
 * @filesource
 * @package Translation
 */

/**
 * The general exception class as used by the Translator Classes.
 *
 * @package Translation
 * @version 1.1.6
 */
abstract class ezcTranslationException extends ezcBaseException
{
}
?>
