<?php
/**
 * Base exception for the ImageConversion package.
 *
 * @package ImageConversion
 * @version 1.3.3
 * @copyright Copyright (C) 2005-2007 eZ systems as. All rights reserved.
 * @license http://ez.no/licenses/new_bsd New BSD License
 */

/**
 * General exception container for the ImageConversion component.
 *
 * @package ImageConversion
 * @version 1.3.3
 */
abstract class ezcImageException extends ezcBaseException
{
}
?>
