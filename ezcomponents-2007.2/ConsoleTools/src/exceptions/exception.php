<?php
/**
 * Base exception for the ConsoleTools package.
 *
 * @package ConsoleTools
 * @version 1.3.1
 * @copyright Copyright (C) 2005-2007 eZ systems as. All rights reserved.
 * @license http://ez.no/licenses/new_bsd New BSD License
 */

/**
 * General exception container for the ConsoleTools component.
 *
 * @package ConsoleTools
 * @version 1.3.1
 */
abstract class ezcConsoleException extends ezcBaseException
{
}
?>
