eZ Components - Cache
~~~~~~~~~~~~~~~~~~~~~

.. contents:: Table of Contents

Introduction
============

The Cache package provides a collection of lightweight classes to cache
different kinds of data. It provides a manager class, which takes
care of instantiating and reusing caches.

Class overview
==============

This section gives you an overview of the most important classes.

ezcCacheManager
  This is the optional manager, which is recommended if your application needs
  to cache different data for different purposes. It allows you to configure
  all caches in a central place and to retrieve them through ezcCacheManager.
  The cache manager will store only the configurations by default, only
  instantiating the cache object when requested.

ezcCacheStorage
  This is the base class for all cache storage (the cache classes themselves).
  All cache classes inherit from this base class.

ezcCacheStorageFilePlain
  Cache objects of this class are capable of storing plain text data on the file
  system. It utilizes the file_get_contents() and file_put_contents() PHP
  functions.

ezcCacheStorageFileArray
  In contrast to ezcCacheStorageFilePlain, objects of this class can store
  array structures and will keep PHP data types intact.
  The ezcCacheStorageFileArray class generates PHP code, which will be stored
  on the file system. Restoring data from the cache uses the require()
  construct.

ezcCacheStorageFileEvalArray
  Objects of this storage class follow a similar approach to
  ezcCacheStorageFileArray; they are also capable of storing array
  structures. The major difference between both classes is that
  ezcCacheStorageFileEvalArray will use PHP's eval() method instead of
  require() to restore the cached data. As a result,
  the stored data will not be cached again in PHP accelerators like APC_.
  This might be desirable if you store large amounts of data at once.

ezcCacheStorageMemcachePlain
  Uses the PHP Memcache_ extension to store cache objects in memory between
  requests, thus improving speed. The zlib extension is also required by
  Memcache for on-the-fly compression.

ezcCacheStorageApcPlain
  Uses the PHP APC_ extension to store cache objects in memory between
  requests, thus improving speed.

ezcCacheStorageFileApcArray
  Uses the PHP APC_ extension to store array structures in memory between
  requests, thus improving speed. It is basically a replacement for
  ezcCacheStorageFileArray, if the APC_ extension is installed.


Usage
=====

Terminology
-----------

A `cache`_ is identified by a `cache identifier`_ and contains a number of
`cache item`_ s.

_`Cache`
  A location that stores `cache item`_ s. A cache is created with
  ezcCacheManager::createCache(), preferrably at the beginning of your script.
  A cache with its `cache identifier`_ - the first parameter to
  ezcCacheManager::createCache() - can only be created once. After it has been
  created, you can reference it by calling ezcCacheManager::getCache(), using
  the corresponding $id as the first parameter.

_`Cache identifier`
  An identifier that uniquely identifies a `cache`_.

_`Cache item`
  One single entry stored in a `cache`_. Cache items can be stored and
  retrieved by using the ezcCacheStorage object that is returned by 
  calling ezcCacheManager::getCache(). Cache items use a `cache key`_ to
  identify specific entries.

_`Cache key`
  A string representing a single `cache item`_ in a `cache`_.

_`Attribute`
  Additional information to complement the `cache key`_ in
  `cache item`_ s.

A simple example
----------------

This example shows how to create and use a simple cache with
ezcCacheManager:

.. include:: tutorial_example_01.php
   :literal:

Time-to-live is defined as 30 seconds in this case; if left
out, the cache will have a lifespan of 24 hours. On line 9, the cache
configuration is stored in the cache
manager. The created `cache`_ uses the `cache identifier`_ "simple" and will reside in the directory
/tmp/cache/plain. (Note: This directory must exist and must be writable!) To store a `cache item`_, the storage
class ezcCacheStorageFilePlain will be used.

Line 11 defines a `cache key`_ for a cache item. The next line defines a
second unique `cache key`_ for a second cache item. After that (line 14), the
newly-created `cache`_ object is retrieved from ezcCacheManager. Lines 16 and
22 show how to check for cached data: ezcCacheStorage::restore()
will return bool false if no valid cache data is found for the given ID.

If no valid data is found, the data will be generated
and stored in the cache later (lines 17-18 and 24-25). The last line outputs the
data, so you can follow how it's cached for 30 seconds by running the
example multiple times in a short time frame. After 30 seconds, the cache data
will become invalid and will be regenerated.

Delayed initialization
----------------------

Instead of calling the ezcCacheManager::getCache() method yourself it is also
possible to use delayed initialization. When using this it is not required to
configure all the caches first by calling the ezcCacheManager::createCache()
method. Instead it would allow you to configure/create caches on demand, this
is called lazy, or delayed initialization. You can find a description how you
can use it for your own components and how it works in the `ezcBase
tutorial`__. The keyword for the cache component is
*ezcInitConfigurationManager*.

__ introduction_Base.html#lazy-initialization

.. include:: tutorial_lazy_initialization.php
	:literal:

Differences with the previous example can be seen in lines 5 to 23. In lines 20
to 23 you tell the delayed initialization mechanism to use the
customLazyCacheConfiguration as lazy initialization provider for the
*ezcInitConfigurationManager* context. The customLazyCacheConfiguration class
implements the configureObject() method that will automatically be called when
ezcCacheManager::getCache() is called with a cache identifier that has not been
created yet through ezcCacheManager::createCache(), as you can see in line 14.

Using multiple caches
---------------------

The following example shows how the cache manager deals with multiple caches:

.. include:: tutorial_example_02.php
   :literal:

In lines 12 and 13, two caches are created. Each `cache`_ must
reside in its own location and must have a different `cache identifier`_. We
use two different options for the lifetime of the caches to show how they act
independently.

Since the first `cache`_ reuses the location already used in example 1, 
we use a different `cache key`_ here.
Lines 15 to 25 are almost identical to the code from example 1, 
except that the program will pause for two seconds when generating 
the plain cache, in order to show different generation times for the two
caches.

On line 30, the second `cache`_ object is retrieved, which is capable of storing
arrays.  Therefore, we store the data from the plain cache here and
generate some additional data to be stored in an array. Running this
example multiple times will give you different results
since the second cache has a longer lifetime and will therefore hold its data
longer than the first one.

Complex caching
---------------

As the next example shows, the ezcCacheStorage class is capable of more advanced
features. This example uses extra attributes in addition to the `cache key`_:

.. include:: tutorial_example_03.php
   :literal:

After the creation of an array `cache`_, some sample data is created (lines
11-16). Data is identified by `cache key`_ s, which are associated with
arrays. Each array will be used to store the content and the attributes
together. `Attribute`_ s describe a `cache item`_ s in further detail.

In line 20, a foreach loop starts, which stores all example data in the cache.
After that, the method ezcCacheStorageFile::countDataItems() is used to count
cache items that meet certain criteria. The first parameter here would be a
`cache key`_.
When this is set, the method should always return 1 or 0, because only one cache
item per `cache key`_ may exist. In this example, the cache items with the specified attribute are
counted. The attributes to match are supplied as the second parameter. The
first method call will return 3 (line 28), since we have three cache items that
have the attribute "section" set to "articles". The second call (line 32)
should return 2, because two data items have the attribute "language" set to the
value "de".

On line 36 the storage object is told to delete all cache items that have
the `attribute`_ "language" set to "de". Therefore, the next calls to
ezcCacheStorageFile::countDataItems() will return 2 and 0.

Memory caching
--------------

If either of Memcache_ or APC_ PHP extensions is installed, then the caching
performance can be improved considerably by storing the cache data in memory
between requests.


APC
```

There are 2 implementations of APC provided: ezcCacheStorageApcPlain for
storing cache objects in memory, and ezcCacheStorageFileApcArray for storing
arrays in memory with file fall-back. The ezcCacheStorageFileApcArray basically
replaces the ezcCacheStorageFileArray if the APC_ PHP extension is installed.

The following example shows how to create and use a plain APC storage. It is
the same as the simple example above, but using the ezcCacheStorageApcPlain
class instead of ezcCacheStorageFilePlain. The second parameter of the
createCache() method is null, because an existing path is not needed.

.. include:: tutorial_apc_plain.php
   :literal:


The following example shows how to create and use an APC array storage. It is
the same as the second example above, but using the ezcCacheStorageFileApcArray
class instead of ezcCacheStorageFileArray. The second parameter of the
createCache() method must be an existing writeable path.

.. include:: tutorial_apc_array.php
   :literal:

Memcache
````````

Certain options must be set before creating a Memcache cache storage. See
ezcCacheStorageMemcacheOptions for more information.

host
  The name of the host running Memcache. Normally this is localhost.

port
  The port on which to connect to the Memcache host. Normally it is 11211.

persistent
  If creating a persistent connection to the Memcache server or not. A
  persistent connection does not close when the script ends. By default this is
  false.

compressed
  If the data is to be compressed in the cache. The zlib extension is required.

The following example shows how to create and use a plain Memcache storage. It
is the same as the simple example above, but using the
ezcCacheStorageMemcachePlain class instead of ezcCacheStorageFilePlain, and
setting the required options for the storage. The second parameter of the
createCache() method is null, because an existing path is not needed.

.. include:: tutorial_memcache_plain.php
   :literal:

More Information
================

For more information, see the ezcCacheManager, ezcCacheStorageFile and
ezcCacheStorageMemory API documentation.


.. _APC: http://pecl.php.net/package/APC
.. _Memcache: http://pecl.php.net/package/Memcache



..
   Local Variables:
   mode: rst
   fill-column: 79
   End: 
   vim: et syn=rst tw=79
