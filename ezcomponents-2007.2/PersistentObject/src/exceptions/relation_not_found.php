<?php
/**
 * File containing the ezcPersistentRelationNotFoundException class
 *
 * @package PersistentObject
 * @version 1.3.3
 * @copyright Copyright (C) 2005-2007 eZ systems as. All rights reserved.
 * @license http://ez.no/licenses/new_bsd New BSD License
 */

/**
 * Exception thrown, if a desired relation between 2 classes was not found.
 *
 * @package PersistentObject
 * @version 1.3.3
 */
class ezcPersistentRelationNotFoundException extends ezcPersistentObjectException
{

    /**
     * Constructs a new ezcPersistentRelationNotFoundException for the class $class
     * which does not have a relation for $relatedClass.
     *
     * @param string $class
     * @param string $relatedClass
     * @return void
     */
    public function __construct( $class, $relatedClass )
    {
        parent::__construct( "Class '{$class}' does not have a relation to '{$relatedClass}'" );
    }
}
?>
