<?php
/**
 * File containing the ezcTemplateLessEqualOperatorAstNode class
 *
 * @package Template
 * @version 1.2
 * @copyright Copyright (C) 2005-2007 eZ systems as. All rights reserved.
 * @license http://ez.no/licenses/new_bsd New BSD License
 * @access private
 */
/**
 * Represents the PHP less than or equal operator <=
 *
 * @package Template
 * @version 1.2
 * @access private
 */
class ezcTemplateLessEqualOperatorAstNode extends ezcTemplateBinaryOperatorAstNode
{
    /**
     * Returns a text string representing the PHP operator.
     * @return string
     */
    public function getOperatorPHPSymbol()
    {
        return '<=';
    }
}
?>
