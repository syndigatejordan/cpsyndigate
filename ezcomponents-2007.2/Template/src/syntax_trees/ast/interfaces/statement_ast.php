<?php
/**
 * File containing the ezcTemplateStatementAstNode class
 *
 * @package Template
 * @version 1.2
 * @copyright Copyright (C) 2005-2007 eZ systems as. All rights reserved.
 * @license http://ez.no/licenses/new_bsd New BSD License
 * @access private
 */
/**
 * Represents a statement of PHP code.
 *
 * @package Template
 * @version 1.2
 * @access private
 */
abstract class ezcTemplateStatementAstNode extends ezcTemplateAstNode
{
    /**
     * Constructs a ezcTemplateStatementAstNode
     */
    public function __construct()
    {
    }
}
?>
