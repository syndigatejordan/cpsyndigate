<?php
require_once( "debug_delayed_init_test.php");
require_once( "debug_test.php");
require_once( "debug_timer_test.php");
require_once( "writers/memory_writer_test.php");
require_once( "formatters/html_formatter_test.php");
    
class ezcDebugSuite extends PHPUnit_Framework_TestSuite
{
	public function __construct()
	{
		parent::__construct();
        $this->setName("Debug");
        
		$this->addTest( ezcDebugDelayedInitTest::suite() );
		$this->addTest( ezcDebugMemoryWriterTest::suite() );
		$this->addTest( ezcDebugTimerTest::suite() );
		$this->addTest( ezcDebugTest::suite() );
		$this->addTest( ezcDebugHtmlFormatterTest::suite() );
	}

    public static function suite()
    {
        return new ezcDebugSuite();
    }
}

?>
