<?php
/**
 * File containing the ezcTreeDbDataStore interface.
 *
 * @copyright Copyright (C) 2005-2007 eZ systems as. All rights reserved.
 * @license http://ez.no/licenses/new_bsd New BSD License
 * @version 1.0
 * @filesource
 * @package TreeDatabaseTiein
 */

/**
 * ezcTreeDbDataStore is an interface defining methods for database based
 * data stores.
 *
 * @package TreeDatabaseTiein
 * @version 1.0
 */
interface ezcTreeDbDataStore extends ezcTreeDataStore
{
}
?>
