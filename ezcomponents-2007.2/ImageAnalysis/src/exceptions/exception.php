<?php
/**
 * Base exception for the ImageAnalysis package.
 *
 * @package ImageAnalysis
 * @version 1.1.2
 * @copyright Copyright (C) 2005-2007 eZ systems as. All rights reserved.
 * @license http://ez.no/licenses/new_bsd New BSD License
 */

/**
 * General exception container for the ImageAnalysis component.
 *
 * @package ImageAnalysis
 * @version 1.1.2
 */
abstract class ezcImageAnalyzerException extends ezcBaseException
{
}
?>
