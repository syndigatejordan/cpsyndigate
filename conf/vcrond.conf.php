<?php

//Load Syndigate commons
require_once dirname(__FILE__) . '/../ez_autoload.php';

define ('MAX_WORKERS',		15);
define ('MAX_IDLE',			180);
define ('SLEEP_INTERVAL',	180);
define ('FS_START',			dirname(__FILE__) . '/');
define ('FS_LOG',			'/var/log/syndigate/cronjob/cron.log');
define ('FS_KILL',			FS_START . 'kill/daemon');

//Get Database Conf from Propel
$propelConf = Propel::getConfiguration();
$dbConf = $propelConf['datasources']['propel']['connection'];
define ('DB_HOST',			$dbConf['hostspec']);
define ('DB_USER',			$dbConf['username']);
define ('DB_PASS',			$dbConf['password']);
define ('DB_NAME',			$dbConf['database']);
unset($dbConf);

define ('TABLE_CRONTAB',	'gather_crontab');
define ('TABLE_CRONJOB',	'gather_cronjob');

