<?php

/**
 * UNIX username, if empty userID and groupID will be used
 * 
 * @var string
 */
$unixUsername = 'root';

/**
* Path to PID file
*
* @var string
*/
$pidFileLocation = '/var/run/shepherd.pid';

/**
* Home path
*
* @var string
*/
$homePath = '/usr/local/syndigate/shepherd/';

/**
 * Max number of children to launch 
 * 
 * @var int
 */
$maxChildren = 15;

#### Do not touch anything below unless you know what you're doing ####

##################### DB  Conf #############################	

	//get DB conf from Propel
	$propelConf = Propel::getConfiguration();
	$dbConf = $propelConf['datasources']['propel']['connection'];

	define('SHEPHERD_DB_USER', $dbConf['username']);
	define('SHEPHERD_DB_PASS', $dbConf['password']);
	define('SHEPHERD_DB_HOST', $dbConf['hostspec']);
	define('SHEPHERD_DB_DATABASE', $dbConf['database']);
	
	
/**
 * Seconds to sleep after spawning a child
 * 
 * @var int
 */
$sleepAfterSpawn = 2;

/**
 * Number of seconds to sleep if Max children is reached
 * 
 * @var int
 */
$maxChildrenWait = 5;

/**
 * Number of seconds to sleep when there's nothing to do
 * consider raising for better performance
 * 
 * @var int
 */
$idleWait = 60;

/**
 * Amount of seconds to timeout the check for dead children
 * 
 * @var int
 */
$checkDeadAlarm = 60;

/**
* Terminate daemon when set identity failure ?
*
* @var bool
*/
$requireSetIdentity = true;
