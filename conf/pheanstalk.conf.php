<?php
require_once dirname(__FILE__) . '/../ez_autoload.php';
require_once APP_PATH . 'lib/pheanstalk/pheanstalk_init.php';

define('PHEANSTALK_HOST_TWO', 				'10.200.200.196');
define('PHEANSTALK_HOST',                              '10.200.200.58');
define('PHEANSTALK_PORT', 				'11300');
define('PHEANSTALK_SLEEP_INTERVAL', 	5);
define('PHEANSTALK_TUBE', 				'default');
define('PHEANSTALK_IGNORE', 			'default');

define('GATHER_TUBE', 'syndGatherBeanstalkd');
define('GATHER_SLEEP_INTERVAL', 5);
define('GATHER_MAX_CONCURRENT_JOBS', 5);
define('GATHER_TIME_TO_RUN', 3600);

define('LOGO_TUBE', 'title-fix-logo');
define('LOGO_SLEEP_INTERVAL', 60);

define('MEMCACHE_HOST', '10.200.200.212');
define('MEMCACHE_PORT', 11211);


define('POST_PROCESS_ARTICLES_TUBE', 'post-process-articles');
