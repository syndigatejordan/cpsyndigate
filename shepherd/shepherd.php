#!/usr/bin/env php
<?php
/**
* Shepherd Daemon 
* 
* Slightly based on the work of Michal 'Seth' Golebiowski 
* @see http://www.phpclasses.org/browse/package/2197.html
* 
* 
* Requirements:
* Unix like operating system
* PHP 5
* PHP compiled with:
* --enable-sigchild
* --enable-pcntl
* 
* @author Ammar Ibrahim <ammar dot ibrahim at gmail dot com>
* 
*/

	require_once dirname(__FILE__) .  '/../ez_autoload.php';

	
##################### Globals ###########################

	/**
    * Current process ID
    *
    * @var int
    */
	$pid = 0;

	/**
    * Is daemon running
    *
    * @var boolean
    */
	$isRunning = false;

	/**
	 * Log object instance
	 *
	 * @var ezcLog
	 */
	$log;

	/**
    * Children PIDs to keep track of them
    *
    * @var array
    */
	$children = array();
	
	/**
	 * Indicates whether this process is a child or not
	 * 
	 * @var bool
	 */
	$isChild = false;
	
	/**
	 * A flag to tell if the child process is still processing
	 * 
	 * @var bool
	 */
	$isProcessing = false;
	
	/**
	 * A flag to tell if the daemon should stop executing
	 * used as a work around to not screw signal handling
	 * 
	 * @var bool
	 */	
	$allowExit = false;
	
	
	$logParams = array();
	
	
#####################   Tests  ############################# 

runTests();

##################### CLI Args #############################

$input = new ezcConsoleInput();

$helpOption = $input->registerOption( new ezcConsoleOption( 'h', 'help') );
$configOption = $input->registerOption( new ezcConsoleOption( 'c', 'config', ezcConsoleInput::TYPE_STRING) );

$input->process();

//check if help is requested
if ($helpOption->value) {
	usage();
}

//config file is mandatory
if (!$configOption->value) {
	usage();
}

if (!is_file($configOption->value)) {
	echo "[$configOption->value] is not a file" . PHP_EOL;
	exit;
}

if (!is_readable($configOption->value)) {
	echo "[$configOption->value] is not readable. Check permissions." . PHP_EOL;
	exit;
}

//load the config file
require $configOption->value;

##################### Start Daemon #########################

declare(ticks=1);
pcntl_signal(SIGCHLD, "sigHandler");
pcntl_signal(SIGTERM, "sigHandler");
pcntl_signal(SIGHUP, "sigHandler");
pcntl_signal(SIGINT, "sigHandler");
pcntl_signal(SIGALRM, "sigHandler");

//log instance
$log = ezcLog::getInstance();
$log->source = "shepherd";
$logParams = array('source' => "shepherd", 'categry' => 'sheperd');

if (!empty($unixUsername)) {
	//change the user
	$pw = posix_getpwnam($unixUsername);
	$userID = $pw['uid'];
	$groupID = $pw['gid'];			
}

$log->log('Starting daemon', ezcLog::DEBUG, $logParams);

if (!daemonize()) {
	$log->log('Could not start daemon', ezcLog::ERROR, $logParams);
	return false;
}

register_shutdown_function('stop');

$log->log('Running...', ezcLog::DEBUG, $logParams);

main();


/**
 * Main parent logic
 *
 * @return void
 */
function main() {
	global $checkDeadAlarm, $allowExit, $children, $maxChildren,
		$sleepAfterSpawn, $idleWait, $maxChildrenWait, $log, $parentLink, $logParams;
	while (true && !$allowExit)
	{

		
		//recreate log instance
		$log = ezcLog::getInstance();
		$log->source = "shepherd";
		
		//open database connection for parent
		$parentLink = mysqli_connect(SHEPHERD_DB_HOST, SHEPHERD_DB_USER, SHEPHERD_DB_PASS);

		if (!$parentLink) {
			$log->log("Couldn't open Parent DB connection", ezcLog::FATAL, $logParams );
			stop();
		}
		
		//SELECT DB
		mysqli_select_db($parentLink, SHEPHERD_DB_DATABASE);
		
		//parent main		
		if (count($children) < $maxChildren) {
				//see if there's any content to be processed
				$rowId = getNextParser();
				if ($rowId) {
	
					mysqli_query($parentLink, "UPDATE parsing_state SET locked=1 WHERE id = $rowId");
					
					//Spawn a parser and send some data to it
					forkChild(array('rowId' => $rowId));
					
					if ($sleepAfterSpawn) {
						sleep($sleepAfterSpawn);
					}
						
				} else {
					//there's nothing to do
					sleep($idleWait);
					continue;
				}
	
			
		} else {
			//We reached max children, try agaiin
			$log->log("max children reached", ezcLog::DEBUG, $logParams );
			sleep($maxChildrenWait);
			continue;
		}
	}

}

################ Functions ###################

/**
 * Detect dead children, and collect them
 *
 */
function detecetDeadChildren() {
	global $children, $log, $parentLink, $logParams;
	
	
	$alert = syndAlert::getInstance();
	
	//go over all children
	foreach ($children as $childPid) {
		if (!posix_kill($childPid, 0)) {
			//couldn't send a signal to the process, it must be dead
			/*we check to see if it completed the process or not
			$sql = "SELECT * FROM parsing_state WHERE pid = '$childPid' ORDER BY  "*/
			
			$alert->sendSynd("Detected dead worker. PID [{$childPid}]", syndAlert::CRITICAL, "shepherd" );
			$log->log("Detected dead worker. PID [{$childPid}]", ezcLog::ERROR, $logParams );
			
			//collect dead child PID
			$children = array_diff($children, array($childPid));
		}
	}
}


/**
 * Figures out which record needs to be parsed next
 *
 * @return mixed (returns false if there's nothing to parse, and an int that represents the ID of the record to be parsed)
 */
function getNextParser() {
	global $log, $parentLink, $logParams;

	//Get all titles that need some parsing
	$sql = "
		SELECT * 
		FROM `parsing_state` 
		WHERE completed = 0
		   AND title_id NOT IN (SELECT title_id FROM parsing_state WHERE locked=1)
		ORDER BY created_at ASC";
	$result = mysqli_query($parentLink, $sql);

	if (!mysqli_num_rows($result)) {
		//$log->log("No new records to launch parsers", ezcLog::DEBUG);
		return false;
	}
	
	while ($row = mysqli_fetch_assoc($result)) {
		$lowestRevQuery = "
			SELECT * 
			FROM parsing_state 
			WHERE title_id={$row['title_id']} 
				AND revision_num <=  {$row['revision_num']}
				AND completed = 0
			ORDER BY revision_num ASC
			LIMIT 1";

		$lowestRevResult = mysqli_query($parentLink, $lowestRevQuery);
		$lowestRevRow = mysqli_fetch_assoc($lowestRevResult);

		/**
		 * @todo Add check to see if the previous rev number was parsed correctly
		 */
		return $lowestRevRow['id'];
		
		
	}

}


/**
 * Main process that the children will execute
 *
 * @return int
 */
function childMain($data = array())
{
	global $isProcessing;
	
	/* 
	For testing
	
	sleep(15);
	$isProcessing = false;
	stop();
	*/
	
	//Spawn a parser and pass the data to it
	$parser = new syndParse($data);
	$parser->process();
	
	$isProcessing = false;
	
	stop();
}



	/**
    * Daemonize
    *
    * Several rules or characteristics that most daemons possess:
    * 1) Check is daemon already running
    * 2) Fork child process (removed)
    * 3) Sets identity
    * 4) Make current process a session laeder
    * 5) Write process ID to file
    * 6) Change home path
    * 7) umask(0)
    *
    * @return void
    * 
    */
	function daemonize()
	{
		global $log, $requireSetIdentity, $pidFileLocation, $pid, $homePath, $logParams;

		if (isDaemonRunning())
		{
			
			$log->log("Deamon already running", ezcLog::FATAL, $logParams );			
			// Deamon is already running. Exiting
			return false;
		}
		/*
		if (!$this->_fork())
		{
		// Couldn't fork. Exiting.
		return false;
		}
		*/
		if (!setIdentity() && $requireSetIdentity)
		{
			$log->log("Could not set identity. Exiting", ezcLog::FATAL, $logParams);
			return false;
		}

		if (!posix_setsid())
		{
			
			$log->log('Could not make the current process a session leader', ezcLog::FATAL, $logParams);
			return false;
		}

		$pid = getmypid();	
		
		if (!file_put_contents($pidFileLocation, $pid)) {
			$log->log('Could not write to PID file', ezcLog::FATAL, $logParams);
			return false;
		}


		@chdir($homePath);
		umask(0);

		return true;
	}
	
	/**
	 * Spawn a child process
	 *
	 * @param array $data data to be passed to child process
	 */
	function forkChild($data = array())
	{
		global $children, $log, $isChild, $isProcessing, $parentLink, $logParams;		
		
		//close the fucking connection so the child won't fuck it
		mysqli_close($parentLink);
		unset($parentLink);

		//remove the log for now.
		unset($log);
		
		if($pid  == -1) {
			
			// Falid to fork 
			$log = ezcLog::getInstance();
			$log->source = "shepherd";
			
			$isChild = false;
			$isProcessing = false;
			
			$log->log("Faild to fork child", ezcLog::FATAL, $logParams);
			
		} else if (($pid = pcntl_fork()) == 0) {
			
			//recreate log instance
			$log = ezcLog::getInstance();
			$log->source = "shepherd";
			
			$isChild = true;
			$isProcessing = true;
			$log->log("im a child", ezcLog::FATAL, $logParams);
			childMain($data);
		} else {

			//recreate log instance
			$log = ezcLog::getInstance();
			$log->source = "shepherd";			
			
			//reopen the connection
			//open database connection for parent
			$parentLink = mysqli_connect(SHEPHERD_DB_HOST, SHEPHERD_DB_USER, SHEPHERD_DB_PASS);
			if (!$parentLink) {
				$log->log("Couldn't open Parent DB connection", ezcLog::FATAL, $logParams);
				stop();
			}
			
			//SELECT DB
			mysqli_select_db($parentLink, SHEPHERD_DB_DATABASE);
						
			$isChild = false;
			$log->log("Spawned child with PID $pid - Record {$data['rowId']}", ezcLog::DEBUG, $logParams);
			$children[] = $pid;
			
			//store the PID of this Child
			mysqli_query($parentLink, "UPDATE parsing_state SET pid='$pid' WHERE id={$data['rowId']}");
		
		}

	}
	
	/**
    * Sets identity of a daemon and returns result
    *
    * @return bool
    */
	function setIdentity()
	{
		global $log, $groupID, $userID, $logParams;
		if (!posix_setgid($groupID) || !posix_setuid($userID))
		{
			$log->log('Could not set identity', ezcLog::FATAL, $logParams);
			return false;
		}

		return true;
	}	

	/**
    * Cheks is daemon already running
    *
    * @return bool
    */
	function isDaemonRunning()
	{
		global $pidFileLocation, $log, $logParams;	
		
		$oldPid = @file_get_contents($pidFileLocation);

		if (is_int($oldPid) && $oldPid && posix_kill(trim($oldPid),0))
		{
			//Another check
			$result = shell_exec('ps x | grep "' . $oldPid . '" | grep "' . $_SERVER['argv'][0] . '" | grep -v "grep"');
			if ($result) {
				$log->log('Daemon already running with PID: '.$oldPid, ezcLog::INFO, $logParams);
				return true;	
			}
		}

		return false;
		
	}
	
	

/**
 * SIGCHLD signal handler
 *
 */
function sigChild()
{
	global $children, $log, $logParams;

	$alert = syndAlert::getInstance();
	
	while(($pid = pcntl_wait($status, WNOHANG)) > 0) {
		$children = array_diff($children, array($pid));
		if(!pcntl_wifexited($status)) {
			$log->log("Collected killed pid $pid\n", ezcLog::INFO, $logParams);
			$alert->sendSynd("Detected dead worker. PID [{$pid}]", syndAlert::CRITICAL, "shepherd" );
		} else {
			$log->log( "Collected exited pid $pid\n", ezcLog::DEBUG, $logParams );
		}
	}
}

/**
 * Captures SIGALRM, the use is to check timeouts for 
 * spawned children
 *
 */
function sigAlarm() {
	global $log, $checkDeadAlarm, $logParams;
	
	$log->log("Checking running children", ezcLog::DEBUG, $logParams);
	
	//Get All Running children
	$c = new Criteria();
	$c->add(ParsingStatePeer::LOCKED, 1);
	
	$runningChildren = ParsingStatePeer::doSelect($c);
	
	
	//pcntl_alarm($checkDeadAlarm);
}

/**
 * Signal handler
 *
 * @param int $signal
 */
function sigHandler($signal)
{
	global $log, $allowExit, $isChild, $logParams;
	
	switch($signal) {
		case SIGALRM:
			$log->log("Caught SIGALRM", ezcLog::DEBUG, $logParams);
			sigAlarm();
			exit;
		case SIGINT:
			$log->log("Caught SIGINT " . ($isChild ? 'in child' : ''), ezcLog::DEBUG, $logParams);
			$allowExit = true;
			break;
		case SIGHUP:
			$log->log("Caught SIGHUP", ezcLog::DEBUG, $logParams);
			$allowExit = true;
			break;			
		case SIGTERM:
			$log->log("Caught SIGTERM", ezcLog::DEBUG, $logParams);
			$allowExit = true;
			break;
		case SIGCHLD:
			$log->log("Caught SIGCHLD", ezcLog::DEBUG, $logParams);
			sigChild();
			break;
		
	}
	
}

	
	
	/**
	 * Run tests to see whether the Env can run this daemon
	 *
	 */
	function runTests() {
		//check the PHP configuration
		if (!defined('SIGHUP')){
			trigger_error('The Daemon needs PHP compiled with --enable-pcntl directive',E_USER_ERROR);
			exit;
		}
		
		if (version_compare('5', phpversion(), '>')) {
			trigger_error('The Daemon requires PHP5', E_USER_ERROR);
			exit;
		}
	}

	/**
	 * Stop the daemon
	 *
	 */
	function stop() {
		global $isChild, $log, $logParams;
		
		if ($isChild) {
			global $isProcessing;
			
			while($isProcessing){}
			
			//wait until we finish processing
			exit(1);
			
		} else {
			
			global $children;
			
			$numChildren = count($children);
			
			//wait until children finish processing
			if ($numChildren) {
				$log->log("Waiting for [$numChildren] children to finish processing", ezcLog::INFO, $logParams);
				
				while(count($children)) {}
			}

			$log->log("Daemon exiting", ezcLog::INFO, $logParams);
			exit(0);
		}
		
	}
	
	
	/**
	 * Print help info
	 *
	 */
	function usage() {
$help="
Usage: ./shepherd [options]

shepherd supports the following command line options :

--help                                -h : this help screen
--config=/path/to/shepherd.conf       -c : configuration file
		
";	
		echo $help;
		exit;
	}
